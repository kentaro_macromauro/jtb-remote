<? 
include("include/setting.php");
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'class/c_service.php');
require_once($path."class/phpmailer/class.phpmailer.php");

$page_next = $_REQUEST[page];
$check_policy = 0; 
	
if (empty( $_SESSION[checkout] ))
{
	$check_policy++;
}

if (empty($page_next))
{
	$check_policy++;
}
	
if ($check_policy == 0)
{	
	$product_type = 'OPT';
			
	function zero_fill($data)
	{
		if (($data) < 10)
		{
			$data = '0'.$data;
		}
			
		return $data;	
	}	
				
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
		
		
		
	$date_inp    = $_SESSION[checkout][booking_date];
	$charge_type = $_SESSION[checkout][charge_type];
	if ($charge_type == "Per Pax")
	{
		$service_count ="";
		$service_type  ="";
	}
	else
	{
		$service_count = $_SESSION[checkout][qty_adult]+$_SESSION[checkout][qty_child]+$_SESSION[checkout][qty_infan];
		$service_type  = $_SESSION[checkout][charge_desc];
	}
		
	$product_code   = $db->get_product_code($_SESSION[checkout][product_id]);
		
	$result  = $db->view_product($_SESSION[checkout][product_id]);		
		
	$status_book = $result[status_book];
		
	$product_name = $result[product_name_jp];
		
	$currency_rate  = $result[rate];
	$currency_code  = $result[code];
	$currency_sign  = $result[sign];
	$short_desc     = $result[short_desc];
				
	$product_id   = $_SESSION[checkout][product_id];	
	$result       = $db->view_product($product_id);
	$product_code = $result[product_code];
			
	$price_adult  = $_SESSION[checkout][price_adult];
	$price_child  = $_SESSION[checkout][price_child];
	$price_infant = $_SESSION[checkout][price_infant];
					
	if ($_SESSION[checkout][allot] == "true")
	{
		$allot    = 1;
	}
	else
	{		
		$allot    = 0;
	}
			
	$arr_hotel = array('id' => '','name' => '' ,'tel'=>'', 'addr' => '');
		
	if ($_SESSION[register][reg][type_hotel] == 1)
	{
		$arr_hotel = array( 'id'   => $_SESSION[register][reg][hotel_id] ,
							'name' => '',
							'tel'  => '',
							'addr' => '');
	}
	else
	{
		$arr_hotel = array( 'id'   => '' ,
							'name' => $_SESSION[register][reg][hotel_name] ,
							'tel'  => $_SESSION[register][reg][hotel_tel],
							'addr' => $_SESSION[register][reg][hotel_addr]);
	}
		
	$arr_address = array('jp_zip1','jp_zip2','jp_pref','jp_city','jp_area','jp_building','ovs1','ovs2');
	
	if ($_SESSION[register][reg][type_addr] == true)
	{
			
		$type_addr = 2;
		$arr_address = array('jp_zip1' => '',
							 'jp_zip2' => '',
							 'jp_pref' => '',
							 'jp_city' => '',
							 'jp_area' => '',
							 'jp_building'=> '',
							 'ovs1'=> $_SESSION[register][reg][addrovs1],
							 'ovs2'=> $_SESSION[register][reg][addrovs2] );
	}
	else
	{
		$type_addr = 1;
		$arr_address = array('jp_zip1' => $_SESSION[register][reg][addrjp_zip1],
						 	 'jp_zip2' => $_SESSION[register][reg][addrjp_zip2],
							 'jp_pref' => $_SESSION[register][reg][addrjp_pref],
							 'jp_city' => $_SESSION[register][reg][addrjp_city],
							 'jp_area' => $_SESSION[register][reg][addrjp_area],
							 'jp_building' => $_SESSION[register][reg][addrjp_building],
							 'ovs1'=> '',
							 'ovs2'=> '');
	}
		
	$qty_amount = $_SESSION['checkout'][qty_adult] + $_SESSION['checkout'][qty_child] + $_SESSION['checkout'][qty_infant] ;
			
	$total_adult  = 0;
	$total_child  = 0;		
	$total_infant = 0;
			
	$total_adult  = $_SESSION[checkout][price_adult]  * $_SESSION[checkout][qty_adult];
		
	$total_child  = $_SESSION[checkout][price_child]  * $_SESSION[checkout][qty_child];
		
	$total_infant = $_SESSION[checkout][price_infant] * $_SESSION[checkout][qty_infant];
		
	if ( $_SESSION[checkout][price_adult] == 1 )
	{
		$total_adult = 0;
	}
	
	if ( $_SESSION[checkout][price_child] == 1)
	{
		$total_child = 0;
	}
		
	if ($_SESSION[checkout][price_infant]  == 1)
	{
		$total_infant = 0;
	}
		
	/*-------total adult-------*/
	if ( empty($_SESSION[checkout][price_adult]) || empty($_SESSION[checkout][qty_adult]) )
	{
		$total_adult = 0;
	}	
	/*-------total adult-------*/
			 
	/*-------total child-------*/
	if ( empty($_SESSION[checkout][price_child]) || empty($_SESSION[checkout][qty_child]) )
	{
		$total_child = 0;
	}
	/*-------total child-------*/
			
	/*-------total infant-------*/
	if ( empty($_SESSION[checkout][price_infant]) || empty($_SESSION[checkout][qty_infant]) )
	{
		$total_infant = 0;
	}		
	/*-------total infant-------*/
		
	$amount = ($total_adult+$total_child+$total_infant);	
		
		
	
	/* 2013 01 08 */
	if ($charge_type == "Per Service"){
		
		$total_adult  = $_SESSION[checkout][price_adult];
		$total_child  = 0;
		$total_infant = 0;
		$amount       = $_SESSION[checkout][price_adult];
		
		if ( $_SESSION[checkout][price_adult] == 1 )
		{
			$total_adult  = 0;
			$total_child  = 0;
			$total_infant = 0;
			$amount       = 0;
		}

		if ( empty($_SESSION[checkout][price_adult]) )
		{
			$total_adult  = 0;
			$total_child  = 0;
			$total_infant = 0;
			$amount       = 0;
		}
	}
	/* 2013 01 08 */
	

	if ( !empty($_SESSION[register][reg][birth_day]) && 
		 !empty($_SESSION[register][reg][birth_month]) &&
		 !empty($_SESSION[register][reg][birth_year]))
	{
		$pax_birthday = zero_fill($_SESSION[register][reg][birth_year]).'-'.
						zero_fill($_SESSION[register][reg][birth_month]).'-'.
						zero_fill($_SESSION[register][reg][birth_day]);
	} 
			
	if (!empty($_REQUEST[transection]))
	{
		$sql = 'SELECT book_id,session_status FROM '._DB_PREFIX_TABLE.'booking WHERE md5(book_id) = "'.trim($_REQUEST[transection]).'" ';	
	}
	
	$result = $db->db_query($sql);
		
	while ($record = mysql_fetch_array($result))
	{
		$cbook_id = $record[book_id];	
		$session_status = $record[session_status];
	}
		
	$ref_bookinit = 0;
		
	if (($session_status == 0)&&($cbook_id != 0))
	{
		$ref_bookinit = 0;
		$book_id = $cbook_id;
		
		$sql = 'DELETE FROM '._DB_PREFIX_TABLE.'booking WHERE book_id = "'.$book_id.'" ';
		$db->db_query($sql);
			
		$sql = 'DELETE FROM '._DB_PREFIX_TABLE.'booking_detail WHERE book_id = "'.$book_id.'" ';
		$db->db_query($sql);
			
		$sql = 'DELETE FROM '._DB_PREFIX_TABLE.'booking_guest WHERE book_id = "'.$book_id.'" ';
		$db->db_query($sql);
			
	}
	else
	{
		$ref_bookinit = 1;
		$db->db_start();
		$book_id = $db->db_newid('booking');	
	}

	$tb_booking = array('book_id',
						'session_status',
						'country_iso3',
						'book_type',
						'book_date',
						'jtb_bookind_id',
						'jtb_cfmcode',
						'jtb_statuscode',
						'transection_id',
						'allot',
						'charge_type',
						'charge_desc',
						'amount',
						'currency',
						'paid_type',
						'paid_status',
						'qty_adults',
						'qty_child',
						'qty_infant',
						'qty_amount',
						'reg_firstname',
						'reg_lastname',
						'reg_furi_firstname',
						'reg_furi_lastname',
						'reg_sex',
						'reg_email',
						'reg_tel1',
						'reg_tel2',
						'reg_tel3',
						'reg_mobile1',
						'reg_mobile2',
						'reg_mobile3',
						'reg_addrtype',
						'reg_addrjp_zip1',
						'reg_addrjp_zip2',
						'reg_addrjp_pref',
						'reg_addrjp_city',
						'reg_addrjp_area',
						'reg_addrjp_building',
						'reg_addrovs1',
						'reg_addrovs2',
						'reg_backup_tel1',
						'reg_backup_tel2',
						'reg_backup_tel3',
						'reg_backup_addrtype',
						'reg_backup_zip1',
						'reg_backup_zip2',
						'reg_backup_pref',
						'reg_backup_addrjp_city',
						'reg_backup_addrjp_area',
						'reg_backup_addrjp_building',
						'reg_backup_addrovs1',
						'reg_backup_addrovs2',
						'reg_hoteltype',
						'reg_hotel_id',
						'reg_hotel_name',
						'reg_hotel_tel',
						'reg_hotel_addr1',
						'reg_hotel_addr2',
						'reg_arrfrm',
						'reg_arrto',
						'reg_arrgo',
						'reg_arrgo_time',
						'reg_airno',
						'reg_info',
						'remark',
						'update_date',
						'update_by',
						'reg_birthday',
						'allot_cfm');
	$tb_booking_data = array( $book_id,
							0,
	/*'country_iso3' =>*/   $site_country,
	   /*'book_type' =>*/   $product_type,
	   /*'book_date' =>*/   $_SESSION[checkout][booking_date],
  /*'jtb_bookind_id' =>*/   '',  // return from api
							'', // return from api
							'',
  /*'transection_id' =>*/   session_id(),					
							$allot,
							$charge_type,
							$service_type,
		 /*'amount' =>*/    $amount,
		/*'currency' =>*/   $currency_rate,
	   /*'paid_type' =>*/   '0',
	 /*'paid_status' =>*/   '0',
	 /*'qty_adults' =>*/    $_SESSION['checkout'][qty_adult],
	 /*'qty_child'  =>*/    $_SESSION['checkout'][qty_child],
	 /*'qty_infant' =>*/    $_SESSION['checkout'][qty_infant],
	/*'qty_amount'	 =>*/   $qty_amount,
   /*'reg_firstname' =>*/   $_SESSION[register][reg][firstname],
   /*'reg_lastname'  =>*/   $_SESSION[register][reg][lastname],
/*'reg_furi_firstname' =>*/ $_SESSION[register][reg][furi_firstname],
/*'reg_furi_lastname'  =>*/ $_SESSION[register][reg][furi_lastname],
	/*'reg_sex'   =>*/ 	    gender_toint( $_SESSION[register][reg][sex] ),
	/*'reg_email' =>*/      $_SESSION[register][reg][email],
	  /*'reg_tel1'  =>*/    $_SESSION[register][reg][tel1],
	  /*'reg_tel2'  =>*/    $_SESSION[register][reg][tel2],
	  /*'reg_tel3'  =>*/    $_SESSION[register][reg][tel3],
	  /*'reg_mobile1' =>*/  $_SESSION[register][reg][mobile1],
	  /*'reg_mobile2' =>*/  $_SESSION[register][reg][mobile2],
	  /*'reg_mobile3' =>*/  $_SESSION[register][reg][mobile3],
	  /*'reg_addrtype' =>*/ 	 $type_addr,
	  /*'reg_addrjp_zip1' =>*/ 	 $arr_address[jp_zip1],
	/*'reg_addrjp_zip2' =>*/ 	 $arr_address[jp_zip2],
	/*'reg_addrjp_pref' =>*/ 	 $arr_address[jp_pref],
	/*'reg_addrjp_city' =>*/ 	 $arr_address[jp_city],
	/*'reg_addrjp_area' =>*/ 	 $arr_address[jp_area],
	/*'reg_addrjp_building' =>*/ $arr_address[jp_building],
		  /*'reg_addrovs1' 	=>*/ $arr_address[ovs1],
	   /*'reg_addrovs2' 	=>*/ $arr_address[ovs2],
		/*'reg_backup_tel1' =>*/ $_SESSION[register][reg][backup_tel1],
		/*'reg_backup_tel2' =>*/ $_SESSION[register][reg][backup_tel2],
		/*'reg_backup_tel3' =>*/ $_SESSION[register][reg][backup_tel3],
	/*'reg_backup_addrtype' =>*/ '1',
		/*'reg_backup_zip1' =>*/ '',
		/*'reg_backup_zip2' =>*/ '',
		/*'reg_backup_pref' =>*/ '',
 /*'reg_backup_addrjp_city' =>*/ '',
 /*'reg_backup_addrjp_area' =>*/ '',
 /*'reg_backup_addrjp_building' =>*/ '',
	/*'reg_backup_addrovs1' =>*/ $_SESSION[register][reg][backup_addr1],
	/*'reg_backup_addrovs2' =>*/ '',
	/*'reg_hoteltype' =>*/ $_SESSION[register][reg][type_hotel] ,
	/*'reg_hotel_id' =>*/ $arr_hotel[id],
	/*'reg_hotel_name' =>*/ $arr_hotel[name],
	/*'reg_hotel_tel'  =>*/ $arr_hotel[tel],
	/*'reg_hotel_addr1' =>*/ $arr_hotel[addr],
	/*'reg_hotel_addr2' =>*/ '',
	/*'reg_arrfrm'=>*/ $_SESSION[register][reg][arrfrm_year].'-'.$_SESSION[register][reg][arrfrm_month].'-'.$_SESSION[register][reg][arrfrm_day],
	/*'reg_arrto' =>*/ $_SESSION[register][reg][arrto_year].'-'.$_SESSION[register][reg][arrto_month].'-'.$_SESSION[register][reg][arrto_day],
	/*'reg_arrgo' =>*/ $_SESSION[register][reg][arrgo_year].'-'.$_SESSION[register][reg][arrgo_month].'-'.$_SESSION[register][reg][arrgo_day],
	/*'reg_arrgo_time' =>*/ $_SESSION[register][reg][arrgo_hh].':'.$_SESSION[register][reg][arrgo_mm],
	/*'reg_airno' =>*/ $_SESSION[register][reg][air_no],
	/*'reg_info' =>*/ $_SESSION[register][reg][sel_info],
	/*'remark' =>*/  $_SESSION[register][reg][remark_html],
	/*'update_date' =>*/ 'datetime',
	/*'update_by' =>*/ '0',
						$pax_birthday,
						$_SESSION[checkout][cfm_code]);
		
		
	$booking_detail = array('book_id' ,
							'book_idx',
							'product_id',
							'product_code',
							'hotel_id',
							'hotel_sub_id',
							'price_adult',
							'price_child',
							'price_infant',
							'qty_adult',
							'qty_child',
							'qty_infant',
							'qty_amount',
							'amount_adult',
							'amount_child',
							'amount_infant',
							'amount');
		
	$booking_detail_data = array(/*'book_id'=>*/	 $book_id,
								/*'book_idx'=>*/	 '1',
								/*'product_id'=>*/	 $product_id,
								/*'product_code'=>*/ $product_code,
								/*'hotel_id'=>*/ 	 '',
								/*'hotel_sub_id'=>*/ '',
								/*'price_adult'=>*/  $_SESSION['checkout'][price_adult],
								/*'price_child'=>*/  $_SESSION['checkout'][price_child],
								/*'price_infant'=>*/ $_SESSION['checkout'][price_infant],
								
								/*'qty_adult'=>*/  	$_SESSION['checkout'][qty_adult],
								/*'qty_child'=>*/  	$_SESSION['checkout'][qty_child],
								/*'qty_infant'=>*/ 	$_SESSION['checkout'][qty_infant],
								
								/*'qty_amount'=>*/  ($_SESSION['checkout'][qty_adult] + 
													 $_SESSION['checkout'][qty_child] + 
													 $_SESSION['checkout'][qty_infant]) ,
								
													($total_adult),
													($total_child),
													($total_infant),
													
								/*'amount' =>*/   	$amount
										);
		
	$productr_guest  = array('book_id',
							 'guest_id',
							 'guest_firstname',
							 'guest_lastname',
							 'guest_age',
							 'guest_sex',
							 'guest_birthday',
							 'guest_no',
							 'guest_noexpire');
			
	for ($i =0 ; $i < $qty_amount ; $i++)
	{	
		$productr_guest_data[$i]  = array(/*'book_id'=>*/ $book_id,
										  /*'guest_id'=>*/ ($i+1),
										  /*'guest_firstname'=>*/ $_SESSION[register][guest][$i][firstname],
										  /*'guest_lastname'=>*/  $_SESSION[register][guest][$i][lastname],
										  /*'guest_age'=>*/ 	  $_SESSION[register][guest][$i][age],
										  /*'guest_sex'=>*/ 	  gender_toint($_SESSION[register][guest][$i][sex]),
				  /*'guest_birthday'=>*/  $_SESSION[register][guest][$i][birth_year].'-'.$_SESSION[register][guest][$i][birth_month].'-'.
															  	  $_SESSION[register][guest][$i][birth_day],
										  /*'guest_no'=>*/ 		  $_SESSION[register][guest][$i][sn],
										  /*'guest_noexpire'=>*/  $_SESSION[register][guest][$i][expire_year].'-'.
																  $_SESSION[register][guest][$i][expire_month].'-'.
																  $_SESSION[register][guest][$i][expire_day]);
	}
	
	$db->set_insert(_DB_PREFIX_TABLE.'booking',$tb_booking,$tb_booking_data);
		
		
	if ( $ref_bookinit == 1)
	{
		$db->db_updateid($book_id, 'booking');	
	}
		
	$db->db_commit();
		
		
	$db->db_start();
	
	$db->set_insert(_DB_PREFIX_TABLE.'booking_detail',$booking_detail,$booking_detail_data);
	$db->db_commit();
	
	$db->db_start();
	
	for ($i=0; $i < $qty_amount; $i++)
	{
		$db->set_insert(_DB_PREFIX_TABLE.'booking_guest',$productr_guest,$productr_guest_data[$i]);
	}
	$db->db_commit();
		
	if ($page_next == 'payment')
	{
	
	  	header('Location: booking_payment.php?transection='.md5($book_id) );
	}
}
else
{
	header( "HTTP/1.1 301 Moved Permanently" );
  	header('Location: index.php');
}

?>