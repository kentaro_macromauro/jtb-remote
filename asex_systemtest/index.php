<?
include("include/setting.php");
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);



if ( strpos($_SERVER[REQUEST_URI],'index.php') )
{
	
  $url  = substr($_SERVER[REQUEST_URI] ,0, strpos($_SERVER[REQUEST_URI],'index.php') );
   
  
  //$url = str_replace('mybus-asia.sakura.ne.jp','http://www.mybus-asia.com/',$url);
  
  header( "HTTP/1.1 301 Moved Permanently" );
  header('Location: '. $url);
}



require_once($path."include/config.php");

$banner_link_lp  = 'images/banner/';
$banner_link_opt = 'images/banner_opt/';
$breadcamp    	 = '<ul class="bread-camp"><li><a href="../">TOP</a><span>&gt;</span></li><li>'.$site_name.'TOP</li></ul>';

/* produuct path setting   */
$img  = '../product/images/product/';
$link = 'product.php?product_id=';
/* produuct path setting   */

$entry_year = Date('Y');  $entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}


/* product slider1 */

//$product_slider1 = $db->front_show_product('OPT',$site_country,$prv_year,$prv_month,"%",0,19); 
$product_slider1 = $db->front_show_photosnap_slider('OPT',$site_country);

$slider1 = '';


for ($i=0;$i<count($product_slider1['product_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="product.php?product_id='.$product_slider1['product_id'][$i].'">';

	$imgcheck =  trim( '../product/images/product/'.$product_slider1['product_id'][$i].'-1.jpg' ) ;
	
	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media" alt="" src="../images/img_notfound.jpg" width="170" height="70" />';
	}
	else
	{
		$product_img = '../product/images/index.php?root=product&amp;width=170&amp;name='.trim( $product_slider1['product_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media" alt="'.$product_img.'" src="'.$product_img.'" width="170" height="70" />';
	}
	
	
	$product_text = str_len( $product_slider1['product_name_jp'][$i],27 ) ;

	
	
	$rate[0][show] = $db->show_rate($product_slider1['country_iso3'][$i]);
	
	$tempgae .= '</a>
				<div class="slider-media-text">
				<p>
				<a href="product.php?product_id='.$product_slider1['product_id'][$i].'"><strong>'.$product_text.'</strong></a>
				<br/>
				'.str_len($product_slider1['short_desc'][$i],70).'</p>';
	
	if ($product_slider1['price_min'][$i] > 0)
	{
		$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">'.show_price($product_slider1['price_min'][$i],$rate[0]).'
				</span>
				</div>';
		
	}
	
				
	$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider1 .= $tempgae;
}
/* product slider1 */


/* product slider2 */

$product_slider2 = $db->front_show_photosnap_slider('PKG',$site_country);
$slider2 = '';

for ($i=0;$i<count($product_slider2['product_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="product.php?product_id='.$product_slider2['product_id'][$i].'">';			

	$imgcheck =  trim( '../product/images/product/'.$product_slider2['product_id'][$i].'-1.jpg' ) ;

	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media" alt="" src="../images/img_notfound.jpg" width="170" height="70" />';
		
	}
	else
	{
		$product_img = '../product/images/index.php?root=product&amp;width=170&amp;name='.trim( $product_slider2['product_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media" alt="'.$product_img.'" src="'.$product_img.'" width="170" height="70" />';
	}
	
	$product_text = str_len( $product_slider2['product_name_jp'][$i],27) ;
		
	$tempgae .= '</a>
				<div class="slider-media-text">
				<p>
				<a href="product.php?product_id='.$product_slider2['product_id'][$i].'"><strong>'.$product_text.'</strong></a>
				<br/>
				'. str_len($product_slider2['short_desc'][$i] ,70) .'
				</p>';
				
	if ($product_slider2['price_min'][$i] > 0)
	{
		$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">'.
				show_price($product_slider2['price_min'][$i],$rate[0]).'
				</span>
				</div>';
				
	}
	
	$tempgae .= '</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider2 .= $tempgae;
}
/* product slider2 */

/* news */
$root_spcontent = '../article_img/index.php?root='.$countryname.'-content&amp;width=170&amp;height=70&amp;name=';
$country_news = $db->fetch_news(0,4,$site_country,"yes");
$news = array();

for ($i = 0; $i < count($country_news[id]) ;$i++)
{
	$news[ $country_news['id'][$i] ] 	= array( 'id' => $country_news['id'][$i],
												 'date' =>  $country_news['date'][$i], 
												 'subject' => $country_news['subject'][$i], 
												 'icon' => $country_news['icon'][$i] );
}
/* news */


/* branch sp content */
$branch_content = $db->fetch_branch_content(0,3,$site_country,"yes");
$sp_content = array();

for ($i = 0; $i < count($branch_content[id]); $i++)
{
	$sp_content[ $branch_content['id'][$i] ]   = array( 'id' => $branch_content['id'][$i],
												        'subject' => $branch_content['subject'][$i],
												        'short_content' => nl2br($branch_content['short_content'][$i]),
												        'image' => $root_spcontent.$branch_content['id'][$i].'-1.jpg' );
}
/* branch sp content */


/* pkg and opt at path right menu*/


/* template setting */

$smarty = new Smarty;

/* banner */
$banner = $db->fetch_banner_opt_country($site_country,0,2);
$banner_lp = $db->fetch_banner_lp_country($site_country,0,1);
/* banner */

include("../include/country_right_menu.php");


$smarty->assign("config",$config);

/* banner */
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("news",$news);
$smarty->assign("sp_content",$sp_content);

$smarty->assign("banner_link1",$link.$banner['product_id'][0]);
$smarty->assign("banner_img1", $banner_link_opt.$banner['banner_id'][0].'-1');

$smarty->assign("banner_link2",$link.$banner['product_id'][1]);
$smarty->assign("banner_img2", $banner_link_opt.$banner['banner_id'][1].'-1');

$smarty->assign("banner_link3",$link.$banner_lp['product_id'][0]);
$smarty->assign("banner_img3", $banner_link_lp.$banner_lp['banner_id'][0].'-1');

$smarty->assign("productslider1",$slider1);
$smarty->assign("productslider2",$slider2);
/* banner */

$smarty->display('country_index.tpl');
/* template setting */
?>