<? 
define('_WEBSERVICE_URL_'  , 'https://www.ap-otms.com/API/Client.asmx/Call');
			
define('_WEBSERVICE_TIME_', 30);

class c_websv
{
	protected $data_params;

	public function __construct($params)
	{
		$this->data_params = $params;
	}
	
	public function getall()
	{
		$ch = curl_init(_WEBSERVICE_URL_);
		curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
		curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
		curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
		curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
		curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
		curl_setopt($ch,    CURLOPT_HEADER,             true);
		curl_setopt($ch,    CURLOPT_POST,               true);
		curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
		curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     _WEBSERVICE_TIME_);
		curl_setopt($ch,    CURLOPT_POSTFIELDS,         /*json_decode(*/ $this->data_params/*)*/);
		curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf8",'Authorization: Basic '.base64_encode("MBC101C1:MBC101C1") ));
		$result = curl_exec($ch);
		
		$first = strpos( $result ,'{"d":');
		$result = substr($result,$first, strlen($result));
        //$raw  = json_decode($result, true);
        //return $raw;
	  	return $result;
	  
	}
}

/* sample Get Product Price Details */

$text = '{
  "cmd":"HOLDALLOTMENT",
  "params":{
    "productid":"1204149",
    "producttype":"OPT",
    "date":"17/08/2012",  
    "adults":1,
    "child":0,
    "infant":0,    
    "servicetype":"Flat",
    "servicecount":"1",
    "customerid":"AGT01",
    "sessionid":"TP453F",
    "transactionid":"V345Rt533bfrt4"
  }
}';


echo "productid:1204149";

$c_websv = new c_websv($text);
$result  = $c_websv->getall();

var_dump($result);

?>