<? 
define('_WEBSERVICE_URL_'  , 'http://123.100.239.36/Test/API/Client.asmx/Call');
			
define('_WEBSERVICE_TIME_', 30);

class c_websv
{
	protected $data_params;

	public function __construct($params)
	{
		$this->data_params = $params;
	}
	
	public function getall()
	{
		$ch = curl_init(_WEBSERVICE_URL_);
		curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
		curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
		curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
		curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
		curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
		curl_setopt($ch,    CURLOPT_HEADER,             true);
		curl_setopt($ch,    CURLOPT_POST,               true);
		curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
		curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     _WEBSERVICE_TIME_);
		curl_setopt($ch,    CURLOPT_POSTFIELDS,         /*json_decode(*/ $this->data_params/*)*/);
		curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf8",'Authorization: Basic '.base64_encode("agt01:pass123") ));
		$result = curl_exec($ch);
		
		$first = strpos( $result ,'{"d":');
		$result = substr($result,$first, strlen($result));
        //$raw  = json_decode($result, true);
        //return $raw;
	  	return $result;
	  
	}
}

/* sample ADDPRODUCTBOOKING */

$text = '{
  "cmd":"ADDPRODUCTBOOKING",
  "params":{
    "productid":"1205011",
    "producttype":"OPT",
    "datefrom":"28/05/2012",
    "dateto":"30/05/2012",
    "sessionid":"TEST",
    "arrivalcity":"SIN",
    "arrivaldate":"28/05/2012",
    "arrivaltime":"1010",
    "arvdepremarks":"None",
    "dropoff":"None",
    "departuredate":"30/05/2012",
    "departuretime":"1111",
    "pickup":"None",
    "customerid":"AGT01",
    "transactionid":"V345Rt533bfrt4",
    "CurrencyCode":"SGD",
    "AllotmentConfCode":"DJ966188G",
    "adults":1,
    "child":0,
    "infant":0,
    "service type":"Flat",
    "service count":"2",    	
    "bookingremark":"Nothing",
    "paxlist":[
      {
        "paxtitle":"Mr.",
        "paxname":"Jogh",
        "paxpassport":"IO9090",
        "passportcoutnry":"Singapore",
        "passportexpiry":"30/11/2019",
        "dob":"30/11/1970",
        "paxtype":"Adult",
        "paxroomline":"1"
      }
    ],
    "hotellist":[
      
    ],
    "addlservicelist":[
      
    ]
  }
}
';


$c_websv = new c_websv($text);
$result  = $c_websv->getall();

var_dump($result);

?>