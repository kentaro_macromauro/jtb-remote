<? 
define('_WEBSERVICE_URL_'  , 'http://123.100.239.36/JTB.ClientAPI/Client.asmx/Call');
			
define('_WEBSERVICE_TIME_', 30);

class c_websv
{
	protected $data_params;

	public function __construct($params)
	{
		$this->data_params = $params;
	}
	
	public function getall()
	{
		$ch = curl_init(_WEBSERVICE_URL_);
		curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
		curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
		curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
		curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
		curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
		curl_setopt($ch,    CURLOPT_HEADER,             true);
		curl_setopt($ch,    CURLOPT_POST,               true);
		curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
		curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     _WEBSERVICE_TIME_);
		curl_setopt($ch,    CURLOPT_POSTFIELDS,         /*json_decode(*/ $this->data_params/*)*/);
		curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf8",'Authorization: Basic '.base64_encode("agt01:pass123") ));
		$result = curl_exec($ch);
		
		$first = strpos( $result ,'{"d":');
		$result = substr($result,$first, strlen($result));
      //$raw  = json_decode($result, true);
	  	return $result;
	  //
	}
}

/* sample Get Product Price Details */
echo "Get Product Price Details";
$text = '{
  "cmd":"getproductpricedetails",
  "params":{
    "productid":"1203001",    	
    "producttype":"OPT",
    "date":"20/03/2012",
    "hotelgroup":13,
    "customerid":"SGP101",
    "sessionid":"TP453F",
    "transactionid":"V345Rt533bfrt4"
  }
}';



/* sample Get Product Availability */
echo "\n\nGet Product Availability";
$text = '{
  "cmd":"GETPRODUCTAVAILABILITY",
  "params":{
    "productid":"1203001",
    "producttype":"OPT",
    "date":"20/03/2012",
    "hotelid":"ALB-SIN-21",
    "roomcategory":"ROH",
    "roomcount":2,
    "customerid":"SGP101",
    "sessionid":"TP453F",
    "transactionid":"V345Rt533bfrt4"
  }
}';



$c_websv = new c_websv($text);
$result  = $c_websv->getall();

var_dump($result);

?>