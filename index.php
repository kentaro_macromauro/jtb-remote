<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH;
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');


$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$data = $db->db_connect();

$rate = $db->get_currency_rate();

require_once($path."include/config.php");

$breadcamp = output_breadcamp(array('TOP'),array(''));

/* For slide banner */
$top_banner_path  = $path.'images/top_banner/';
$record = $db->fetch_top_banner(0,20, 'ALL','yes');
$top_banner_img = '';
for ($i=0; $i< count( $record['top_banner_id']) ;$i++)
{
    if($record['jump'][$i] == 1){
        $target = '';
        if($record['new_window'][$i] == 1){
            $target = 'target="_blank"';
        }
        $custom_id = 'TOP_#'.($i+1);
        $top_banner_img .= '<a href="'.$record['link'][$i].'" '.$target.' id="'.$custom_id.'" class="slide"><img src="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg" data-thumb="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg"></a>';
    }else{
        $top_banner_img .= '<img src="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg" data-thumb="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg">';
    }
}

/* For select part on header */
$record = $db->get_country();
$sql = 'SELECT city_id,city_name_jp,city_iso3 FROM '._DB_PREFIX_TABLE.'city WHERE country_iso3 = UPPER("'.$_GET[inp_country].'") ORDER BY city_idx,city_id ';

$result = $db->db_query($sql); $i =0 ;
while ( $record1 = mysql_fetch_array($result))
{
    $arr_city[0][$i] = $record1[city_iso3];
    $arr_city[1][$i] = $record1[city_name_jp];
    $i++;
}

$inp_country = input_selectbox('inp_country',$record[1],$record[0],$_GET[inp_country],'-Please Select-','form-control top') ;
$select_city = input_selectbox('inp_city',$arr_city[1],$arr_city[0],$_GET[inp_city],'-Please Select-','form-control') ;

$inp_search_keyword = input_textbox('inp_keyword',$_GET[inp_keyword],'inp_width130');

$record = $db->view_theme_all();
$select_category = input_option($record['data'], $record['value'], $_GET[inp_category]);

if (is_array($_GET[inp_category]))
{
    for ($i = 0; $i < count($_GET[inp_category]) ; $i++)
    {
        $select_category .= input_hiddenbox('inp_category[]',$_GET[inp_category][$i]) ;
    }
}

$arr_option = $db->view_option_all();
$select_option = input_option($arr_option['data'], $arr_option['value'], $_GET[inp_option]);


$record = $db->viw_time_all();
$select_time = input_option($record['data'], $record['value'], $_GET[inp_time]);
/* For select part on header */

$smarty = new Smarty;
$smarty->template_dir = 'webapp/templates';
$smarty->compile_dir = 'webapp/templates_c';
$smarty->assign("config",$config);
$smarty->assign("breadcamp",$breadcamp);

/* For Top slide */
$smarty->assign("top_banner_img",$top_banner_img);

/* Search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_city",$select_city);
$smarty->assign("select_category",$select_category);
$smarty->assign("select_option",$select_option);
$smarty->assign("select_time",$select_time);

//path by each env
$image_path = $path.'/images';
$smarty->assign("image_path",$image_path);
$smarty->display('jtb_index.tpl');
/* template setting */
?>