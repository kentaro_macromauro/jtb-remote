<? 
	$code = $_REQUEST[code];
	
	switch ($code){
		case 'privacy-sg' :  $code = 'sg'; break;	
		case 'privacy-th'  :  $code = 'th'; break;
		case 'privacy-tw' :  $code = 'tw'; break;
	}
	
	switch ($code){
		case 'sg' : $site =  'JTB Pte. Ltd.'; break;
		case 'th'  : $site = 'JTB(Thailand)Limited'; break;
		case 'tw' : $site = '世帝喜旅行社股份有限公司'; break;
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>プライバシーポリシー</title>
<link type="text/css" href="common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="common/raku.css" rel="stylesheet" media="all" />
<style type="text/css">
.faq{padding:5px 0px 0px;}
.faq-q{background:url(images/lookjtb/faqq.jpg) left top no-repeat; padding-left:25px; line-height:22px; color:#C40000; font-weight:bold; cursor:pointer; padding-bottom:3px;}
.faq-a{background:url(images/lookjtb/faqa.jpg) left top no-repeat; padding-left:25px; line-height:22px; display:none;  }
.height10{height:10px; margin:0px; padding:0px; line-height:10px; font-size:10px;}
.border-bottom{border-bottom:#e7e7e7 solid 1px;}
</style>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(
			function(){
					$('.faq-q').toggle(
						 function(){
							$(this).parent().find('.faq-a').show();	
						},
						function(){
							$(this).parent().find('.faq-a').hide();	
						});
		});
</script>

</head>
<body>

<!--conent-->
<div class="content">
	<!--header-->
	<div class="header">
		<h1 class="txtHeaderTop">プライバシーポリシー</h1>
		<a href="#" class="logoTop"><img src="images/lookjtb/logo.jpg" alt="ルックJTB×Mybusアジア・パシフィック"  /></a>
	</div>
	<!--header-->
	
	
	<!--content -->
	<div class="txtContent">
		
		<!--content form step1-->
		<div class="breakLine"></div>		
		<!--contentbottom-->
		<div class="txtContentBottom border-bottom">
		
			<div class="txtContentNoteHeader">プライバシーポリシー</div>
			
			<div class="height10"></div>
			
			<p>この度は「ルックＪＴＢ　ＲＡＫＵなびサポート」をご利用いただきありがとうございます。<br/>
本サービスは(株)JTBワールドバケーションズが企画、実施する旅行（ルックJTB）にお申し込みを完了されたお客様を対象に、「現地法人<?=$site ?>」に業務委託をしてご提供するものです。<br/>
ご利用にあたっては、以下の規約をご参照の上、 お問合せフォームのプライバシーポリシーに同意をするチェックボックスにチェックを入れてください。
			</p>
			<br/>
			
			<p>
			当社は、お客様の個人情報を以下のようにお取扱し、保護に努めております。お客様におかれましては、予めこれらにご同意の上、個人情報をご提供いただけますようお願いいたします。</p><br/>

<p>1.当社の保有するお客様の個人情報<br/>
当社は、お客様がご旅行の申込等にあたり当社に提供いただいた個人情報の一部を個人データとして保有しております。</p><br/>

<p>2.お客様個人情報の利用目的<br/>
 当社は、ご旅行の申込みの際に提出された個人情報について、お客様との連絡のために利用させていただくほか、お客様がお申込みいただいた旅行において運送・宿泊機関等(主要な運送・宿泊機関等については契約書面に記載されています)の提供する旅行サービスの手配及びそれらのサービスの受領のための手続に必要な範囲内で利用させていただきます。</p><br/>

<p>※その他、当社は、</p>
<br/>

<p>(1) 当社及び当社の提携する企業の商品やサービス、キャンペーンのご案内<br/>
(2) 旅行参加後のご意見やご感想の提供のお願い<br/>
(3) アンケートのお願い<br/>
(4) 特典サービスの提供<br/>
(5) 統計資料の作成<br/>
にお客様の個人情報を利用させていただくことがあります。</p>
<br/>

<p>3.お客様個人情報の第三者への提供<br/>
当社は、お申込みいただいた旅行サービスの手配及びそれらのサービスの受領のための手続に必要な範囲内、または当社の旅行契約上の責任、事故時の費用等を担保する保険の手続き上必要な範囲内で、それら運送・宿泊機関、保険会社等及び手配代行者に対し、お客様の氏名、性別、年齢、住所、電話番号またはメールアドレス、パスポート番号、クレジットカード番号を電子的方法等で送付することにより提供いたします。 なお、特定した利用目的の範囲を超えた取扱いの必要性が生じた場合は、個人情報保護管理者による承認を得た後、改めてご本人様に利用目的などを通知し、同意を得るようにし、目的外利用はいたしません。</p>
<br/>
<p>4.お客様個人情報の収集・利用について<br/>
当社は、お客様の個人情報を収集、利用するにあたり、以下の取扱いをしておりますことを予めご承知おき願います。</p>

<br/>
<p>(1) 収集目的、利用範囲を明示し、同意を得ます。<br/>
(2) お客様の同意がない限り、収集目的以外に使用いたしません。<br/>
(3) 預託、第三者提供する場合は、予めその旨をお知らせし、同意を得ます。<br/>
(4) お申込、資料のご請求等において、お客様が当社にご提供いただく個人情報の項目をご自分で選択することはお客様の任意ですが、全部または一部の個人情報を提供いただけない場合であって、お客様との連絡、あるいは旅行サービスの手配及びそれらのサービスの受領のために必要な手続きがとれない場合、お客様のお申込、ご依頼をお引受できないことがあります。<br/>
(5) 今後のお客様のご旅行申し込みを簡素化するために、当社及び下記に記載された当社のグループ企業とお客様情報を共有する場合がありますが、厳重に管理・保管いたします。</p><br/>

<p>JTBアジアパシフィックグループ各社 </p><br/>
<p>シンガポール地域 JTB Pte. Ltd. </p><br/>

<p>タイ地域   JTB(Thailand)Limited </p><br/>

<p>インドネシア地域 PT.JTB INDONESIA </p><br/>

<p>マレーシア地域 JAPAN TRAVEL BUREAU(MALAYSIA)SDN.BHD.<br/>
    （Co.No.200323-V,KKKP/PL:1878) </p><br/>

<p>台湾地域   世帝喜旅行社股份有限公司 </p><br/>

<p>ベトナム・カンボジア地域 JTB-TNT Co., Ltd. </p><br/>

<p>オーストラリア地域 JTB Australia Pty. Ltd. </p><br/>

<p>ニュージーランド地域 JTB NEW ZEALAND LTD. </p><br/>



<p>5.お客様個人情報の共同利用<br/>
 当社は、当社が保有するお客様個人データのうち、氏名、住所、電話番号またはメールアドレスなどのお客様への連絡にあたり必要となる最小限の範囲のものについてグループ企業との間で共同して利用させていただきます。 当該企業グループは、それぞれの企業の営業案内、お客様のお申し込みの簡素化、催物内容等のご案内、ご購入いただいた商品の発送のために、これを利用させていただくことがあります。</p>
<br/>
<p>6.漏洩した際の対応<br/>
当社が保有するお客様の個人データの開示、削除もしくは消去、内容の訂正、その利用の停止または第三者への提供の停止をご希望の方は、お申し込み頂いた国のお問い合わせ窓口までお申し出ください。</p><br/>

<p>7.個人情報に関する問い合わせ、開示、削除、訂正<br/>
万一、当社の個人情報の流出等の問題が発生した場合には、直ちにお客様にご連絡をいたします。安全の確保を図ります。</p>
			
			
			<div class="height10"></div>
		</div>
		<!--contentbottom-->
		
	</div>
	<!--content -->
	
</div>
	
	
<!--footer-->
<div class="footer">
	<div class="footerContent">
		<p>Copyright © 2013 MyBus-Asia.com All rights reserved</p>
	</div>
</div>
<!--footer-->
</body>
</html>