﻿{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        <ul class="booking-nav-process {if $config.allot != 'false'}limit4{/if}">
          
            <li id="step1">希望日選択</li>
            <li id="step2" class="active_last">カート</li>
            
            <li id="step3" class="active">予約情報入力</li>
            <li id="step4" class="active_first">予約情報確認</li>
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step5">お支払い方法の選択</li>
            {/if}
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step6">決済情報確認</li>
            <li id="step7">決済情報確認</li>
            {/if}
            
            <li id="step8">完了</li>
        </ul>
        <div class="clear"></div>
        <span class="texttittle" >予約情報入力</span>
        <div class="text-policy">
          <p>個人情報保護方針</p>
          <p>入力されました個人情報は、ご本人の承諾なしに第三者（業務委託先を除く）に開示するようなことはいたしません。
            JTBの商品・情報提供のため利用させていただきます。詳しくは個人情報保護方針をご覧下さい。</p>
          <p>お申し込みの際のご注意（必ずお読み下さい）</p>
          <p>オプショナルツアー及び送迎手配など、本サイト内商品のお申し込みに際し、<a href="../page.php?id=term.html" target="_blank">『旅行条件』</a>を必ずご確認いただきます
            よう、お願い致します。　『旅行条件』のページへ</p>
          <div class="height10"></div>
          <p>
            <input type="checkbox" value="true" name="frm_step3_checkview" {if $booking.his.view == "on"}checked="checked"{/if}   />
            上記の内容に同意する</p>
        </div>
        <div class="form3-content">
          <!--<form action="booking_infoinput_confirm.php" name="gotonext" method="post">-->
          <form action="booking_infoinput_action.php" name="gotonext" method="post">
            <!-- form Register-->
            <div class="height10"></div>
            <p>ツアー名：{$booking.product.name}
              <input type="hidden" name="product_id" value="{$booking.product.id}" />
            <p>日付：{$booking.date}
              <input type="hidden" name="booking_date" value="{$booking.date}" />
            </p>
            <div class="height10"></div>
            <p>■申込情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>申込者名（漢字） <span class="text-small-red">※必須</span></th>
                <td>{$error.name}
                <span class="text-firstname" style="width:55px;">姓</span>
                  
                  <input type="text" class="inp-name"  value="{$booking.his.reg.firstname}" name="reg_firstname" />
                  <span class="text-lastname" style="width:65px;">名</span>
                  <input type="text" class="inp-name"  value="{$booking.his.reg.lastname}" name="reg_lastname" />
                  <span class="text-small">例） 山田 太郎</span></td>
              </tr>
              <tr>
                <th>申込者名（ローマ字） <span class="text-small-red">※必須</span></th>
                <td>
                  {$error.name_furi}
                  <span class="text-firstname" style="width:55px;">Surname</span>
                  <input type="text" class="inp-name" value="{$booking.his.reg.furi_firstname}" name="reg_firstname_furi" />
                  <span class="text-lastname" style="width:65px;">Firstname</span>
                  <input type="text" class="inp-name" value="{$booking.his.reg.furi_lastname}" name="reg_lastname_furi" />
                  <span class="text-small">例） Yamada Taro</span></td>
              </tr>
              <tr>
                <th>生年月日 <span class="text-small-red">※必須</span></th>
                <td>
                  {$error.birth_date}
                	<select name="year">
                    <option selected="selected" value="" label="----">----</option>
                    
                    
                    {foreach from=$form.birthyear item=year}
                    	{if $booking.his.reg.birth_year eq $year   }
                        	
                    
                    <option selected="selected" value="{$year}">{$year}</option>
                    
                    
                        {else}                    
                    		
                    
                    <option value="{$year}">{$year}</option>
                    
                    
                    	{/if}
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="month">
                    <option selected="selected" value="" >--</option>
                    <option value="1"  {if $booking.his.reg.birth_month eq "1"} selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.birth_month eq "2"} selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.birth_month eq "3"} selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.birth_month eq "4"} selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.birth_month eq "5"} selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.birth_month eq "6"} selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.birth_month eq "7"} selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.birth_month eq "8"} selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.birth_month eq "9"} selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.birth_month eq "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.birth_month eq "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.birth_month eq "12"}selected="selected"{/if} >12</option>
                  </select>
                  月
                  <select style="" name="day">
                    <option selected="selected" value="" >--</option>
                    <option value="1"  {if $booking.his.reg.birth_day eq "1"} selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.birth_day eq "2"} selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.birth_day eq "3"} selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.birth_day eq "4"} selected="selected"{/if}>4</option>
                    <option value="5"  {if $booking.his.reg.birth_day eq "5"} selected="selected"{/if}>5</option>
                    <option value="6"  {if $booking.his.reg.birth_day eq "6"} selected="selected"{/if}>6</option>
                    <option value="7"  {if $booking.his.reg.birth_day eq "7"} selected="selected"{/if}>7</option>
                    <option value="8"  {if $booking.his.reg.birth_day eq "8"} selected="selected"{/if}>8</option>
                    <option value="9"  {if $booking.his.reg.birth_day eq "9"} selected="selected"{/if}>9</option>
                    <option value="10" {if $booking.his.reg.birth_day eq "10"} selected="selected"{/if}>10</option>
                    <option value="11" {if $booking.his.reg.birth_day eq "11"} selected="selected"{/if}>11</option>
                    <option value="12" {if $booking.his.reg.birth_day eq "12"} selected="selected"{/if}>12</option>
                    <option value="13" {if $booking.his.reg.birth_day eq "13"} selected="selected"{/if}>13</option>
                    <option value="14" {if $booking.his.reg.birth_day eq "14"} selected="selected"{/if}>14</option>
                    <option value="15" {if $booking.his.reg.birth_day eq "15"} selected="selected"{/if}>15</option>
                    <option value="16" {if $booking.his.reg.birth_day eq "16"} selected="selected"{/if}>16</option>
                    <option value="17" {if $booking.his.reg.birth_day eq "17"} selected="selected"{/if}>17</option>
                    <option value="18" {if $booking.his.reg.birth_day eq "18"} selected="selected"{/if}>18</option>
                    <option value="19" {if $booking.his.reg.birth_day eq "19"} selected="selected"{/if}>19</option>
                    <option value="20" {if $booking.his.reg.birth_day eq "20"} selected="selected"{/if}>20</option>
                    <option value="21" {if $booking.his.reg.birth_day eq "21"} selected="selected"{/if}>21</option>
                    <option value="22" {if $booking.his.reg.birth_day eq "22"} selected="selected"{/if}>22</option>
                    <option value="23" {if $booking.his.reg.birth_day eq "23"} selected="selected"{/if}>23</option>
                    <option value="24" {if $booking.his.reg.birth_day eq "24"} selected="selected"{/if}>24</option>
                    <option value="25" {if $booking.his.reg.birth_day eq "25"} selected="selected"{/if}>25</option>
                    <option value="26" {if $booking.his.reg.birth_day eq "26"} selected="selected"{/if}>26</option>
                    <option value="27" {if $booking.his.reg.birth_day eq "27"} selected="selected"{/if}>27</option>
                    <option value="28" {if $booking.his.reg.birth_day eq "28"} selected="selected"{/if}>28</option>
                    <option value="29" {if $booking.his.reg.birth_day eq "29"} selected="selected"{/if}>29</option>
                    <option value="30" {if $booking.his.reg.birth_day eq "30"} selected="selected"{/if}>30</option>
                    <option value="31" {if $booking.his.reg.birth_day eq "31"} selected="selected"{/if}>31</option>
                  </select>
                  日 </td>
              </tr>
              <tr>
                <th>性別 <span class="text-small-red">※必須</span></th>
                <td>
                  {$error.sex}
                  <select name="reg_sex">
                    <option value="">---</option>
                    <option value="男性" {if $booking.his.reg.sex eq "男性"}selected="selected"{/if} >男性</option>
                    <option value="女性" {if $booking.his.reg.sex eq "女性"}selected="selected"{/if} >女性</option>
                  </select></td>
              </tr>
              <tr>
                <th>メールアドレス（PC） <span class="text-small-red">※必須</span></th>
                <td>
                  {$error.mail}                 
                  <input type="text" class="width250"  name="reg_mail" value="{$booking.his.reg.email}" />
                  <span class="text-small">例） yamada@●●●.co.jp</span></td>
              </tr>
              <tr>
                <th>確認用メールアドレス（PC） <br/>
                  <span class="text-small-red">※必須</span></th>
                <td>
                 
                  <input type="text" class="width250" name="reg_mail_cfm" value="{if $error.mail == ""}{$booking.his.reg.email}{/if}" />
                  <span class="text-small">例） yamada@●●●.co.jp</span></td>
              </tr>
              <tr >
                <th>&nbsp;</th>
                <td> {if $booking.his.reg.type_addr eq "true"}
                  <input type="checkbox" name="type_addr" value="true" checked="checked" />
                  {else}
                  <input type="checkbox" name="type_addr" value="true"  />
                  {/if}
                  
                  海外在住の方 </td>
              </tr>
              <tr>
                <th>住所</th>
                <td><!--Address JP-->
                  
                  
				  
                  
                  <table cellspacing="0" class="regis_address_local">
                    <tr >
                      <td>郵便番号 <span class="text-small-red">※必須</span></td>
                      <td>
                      	{$error.addr_type1_zip} 
                        〒
                        <input type="text" class="width40"  maxlength="3" value="{$booking.his.reg.addrjp_zip1}" name="reg_addrjp_zip1"/>
                        -
                        <input type="text"  class="width40"  maxlength="4" value="{$booking.his.reg.addrjp_zip2}" name="reg_addrjp_zip2"/>
                        <a target="_blank" href="http://search.post.japanpost.jp/zipcode/"><span class="mini">郵便番号検索</span></a></td>
                    </tr>
                    <tr>
                      <td>都道府県 <span class="text-small-red">※必須</span></td>
                      <td>
                          {$error.addr_type1_pref}              
                        <select name="reg_addrjp_pref" >
                          <option value="">都道府県を選択</option>
                          
                          
                        {foreach from=$form.city item=arr_city}
                    		
                          
                          <option value="{$arr_city.id}"  {if $booking.his.reg.addrjp_pref eq $arr_city.id}selected="selected"{/if}  >{$arr_city.name}</option>
                          
                          
                    	{/foreach} 
                      
                        
                        </select></td>
                    </tr>
                    <tr>
                      <td>市区町村 </td>
                      <td>
                      	
                        <input type="text" name="reg_addrjp_city" class="width350" value="{$booking.his.reg.addrjp_city}"  />
                        <span class="text-small">例） 品川区</span></td>
                    </tr>
                    <tr>
                      <td>
                      	
                      	町域 <span class="text-small-red">※必須</span></td>
                      <td>
                      	{$error.addr_type1_area}
                        <input type="text" name="reg_addrjp_area" class="width350" value="{$booking.his.reg.addrjp_area}" />
                        <span class="text-small">例） 東品川2-3-11</span></td>
                    </tr>
                    <tr>
                      <td>建物名<br/>
                        部屋番号 </td>
                      <td><input type="text" name="reg_addrjp_building" class="width350" value="{$booking.his.reg.addrjp_building}" />
                        <span class="text-small">例） ＪＴＢビル</span></td>
                    </tr>
                  </table>
                  <!--Address JP-->
                  <!--Address EN-->
                  
                  <table  class="regis_address_oversea">
                    <tr>
                      <td>住所 <span class="text-small-red">※必須</span></td>
                      <td>
                      	{$error.addr_type2}
                      	<input type="text" name="reg_addrovs1" class="width300"  style="width:300px;"  value="{$booking.his.reg.addrovs1}" />
                        <span class="text-small">例)　79 Anson Road #12-01 Singapore079906</span></td>
                    </tr>
                    <tr>
                      <td>国 <span class="text-small-red">※必須</span></td>
                      <td><input type="text" name="reg_addrovs2" class="width300" style="width:300px;" value="{$booking.his.reg.addrovs2}" />
                      	<span class="text-small">例)　Singapore</span>
                      </td>
                    </tr>
                  </table>
                  <!--Address EN--></td>
              </tr>
              <tr>
                <th>連絡先（電話）</th>
                <td>
                 <input type="text" name="reg_tel01" class="width40" value="{$booking.his.reg.tel1}" />
                  -
                  <input type="text" name="reg_tel02"  class="width40" value="{$booking.his.reg.tel2}" />
                  -
                  <input type="text" name="reg_tel03"  class="width40" value="{$booking.his.reg.tel3}" />
                  <span class="text-small">例） 03-1111-1111</span></td>
              </tr>
              <tr>
                <th>連絡先（携帯）<span class="text-small-red">※必須</span></th>
                <td>
                  {$error.mobile}
                  <input type="text" name="reg_mobile01" class="width40" value="{$booking.his.reg.mobile1}" />
                  -
                  <input type="text" name="reg_mobile02" class="width40" value="{$booking.his.reg.mobile2}" />
                  -
                  <input type="text" name="reg_mobile03" class="width40" value="{$booking.his.reg.mobile3}"  />
                  <span class="text-small">例） 090-1111-1111</span></td>
              </tr>
              <tr>
                <th>住所（緊急連絡先）<span class="text-small-red">※必須</span></th>
                <td>
                {$error.addr_backup}
                <input type="text" name="reg_backup_addr1" class="width350" value="{$booking.his.reg.backup_addr1}" /></td>
              </tr>
              <tr>
                <th>電話番号（緊急連絡先）<span class="text-small-red" >※必須</span></th>
                <td>
                  {$error.mobile_backup}
                  <input type="text" name="reg_backup_tel1" class="width40"  value="{$booking.his.reg.backup_tel1}" />
                  -
                  <input type="text" name="reg_backup_tel2" class="width40 "   value="{$booking.his.reg.backup_tel2}"/>
                  -
                  <input type="text" name="reg_backup_tel3" class="width40"    value="{$booking.his.reg.backup_tel3}"  />
                  <span class="text-small">例） 090-1111-1111</span></td>
              </tr>
            </table>
            <div class="height10"></div>
            <p>■参加情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>滞在先</th>
                <td><div class="hotel_auto">
                    {$error.hotel_type1_id}
                    
                    <select name="reg_hotel_id">
                      <option value="">滞在先</option>
                      
                      
                    
                    {foreach from=$form.hotel item=hotel}
                    	
                      
                      <option value="{$hotel.id}" {if $booking.his.reg.hotel_id == $hotel.id } selected="selected" {/if} >{$hotel.name}</option>
                      
                      
                    {/foreach}
                    
                  
                    
                    </select>
                  </div>
                  <div class="hotel_manual">
                    <table cellspacing="0">
                      <tr>
                        <td>滞在先名 <span class="text-small-red">※必須</span></td>
                        <td>{$error.hotel_type2_name}<input type="text" class="width350"  name="reg_hotel_name" value="{$booking.his.reg.hotel_name}" /></td>
                      </tr>
                      <tr>
                        <td>滞在先TEL</td>
                        <td><input type="text" name="reg_hotel_tel" class="width120" value="{$booking.his.reg.hotel_tel}" />
                          <span class="text-small">例） 090-1111-1111</span></td>
                      </tr>
                      <tr>
                        <td>滞在先住所</td>
                        <td> <input type="text"class="width350"  name="reg_hotel_addr" value="{$booking.his.reg.hotel_addr}"  /></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              <tr style="display:none;">
                <th>&nbsp;</th>
                <td><input type="checkbox" name="reg_hotel_select" value="1" {if $booking.his.reg.hotel_select == "1" }checked="checked"{/if}  />
                  リストにない場合 
              </tr>
              <tr>
                <th>滞在期間 <span class="text-small-red">※必須</span></th>
                <td><select name="reg_arrfrm_year">
                    <option selected="selected" value="" label="----">----</option>
                    
                    
                    {foreach from=$form.bookyear item=year}
                    	
                    
                    <option value="{$year}"  {if $booking.his.reg.arrfrm_year =="$year" }selected="selected"{/if}>{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="reg_arrfrm_month">
                    <option value=""  {if $booking.his.reg.arrfrm_month ==""  }selected="selected"{/if} label="--">--</option>
                    <option value="1" {if $booking.his.reg.arrfrm_month =="1" }selected="selected"{/if}>1</option>
                    <option value="2" {if $booking.his.reg.arrfrm_month =="2" }selected="selected"{/if}>2</option>
                    <option value="3" {if $booking.his.reg.arrfrm_month =="3" }selected="selected"{/if}>3</option>
                    <option value="4" {if $booking.his.reg.arrfrm_month =="4" }selected="selected"{/if}>4</option>
                    <option value="5" {if $booking.his.reg.arrfrm_month =="5" }selected="selected"{/if}>5</option>
                    <option value="6" {if $booking.his.reg.arrfrm_month =="6" }selected="selected"{/if}>6</option>
                    <option value="7" {if $booking.his.reg.arrfrm_month =="7" }selected="selected"{/if}>7</option>
                    <option value="8" {if $booking.his.reg.arrfrm_month =="8" }selected="selected"{/if}>8</option>
                    <option value="9"  {if $booking.his.reg.arrfrm_month =="9" }selected="selected"{/if}>9</option>
                    <option value="10" {if $booking.his.reg.arrfrm_month =="10" }selected="selected"{/if}>10</option>
                    <option value="11" {if $booking.his.reg.arrfrm_month =="11" }selected="selected"{/if}>11</option>
                    <option value="12" {if $booking.his.reg.arrfrm_month =="12" }selected="selected"{/if}>12</option>
                  </select>
                  月
                  <select style="" name="reg_arrfrm_day">
                    <option {if $booking.his.reg.arrform_day ==""  }selected="selected"{/if} value="" >--</option>
                    <option value="1"  {if $booking.his.reg.arrfrm_day =="1"  }selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.arrfrm_day =="2"  }selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.arrfrm_day =="3"  }selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.arrfrm_day =="4"  }selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.arrfrm_day =="5"  }selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.arrfrm_day =="6"  }selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.arrfrm_day =="7"  }selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.arrfrm_day =="8"  }selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.arrfrm_day =="9"  }selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.arrfrm_day =="10"  }selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.arrfrm_day =="11"  }selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.arrfrm_day =="12"  }selected="selected"{/if} >12</option>
                    <option value="13" {if $booking.his.reg.arrfrm_day =="13"  }selected="selected"{/if} >13</option>
                    <option value="14" {if $booking.his.reg.arrfrm_day =="14"  }selected="selected"{/if} >14</option>
                    <option value="15" {if $booking.his.reg.arrfrm_day =="15"  }selected="selected"{/if} >15</option>
                    <option value="16" {if $booking.his.reg.arrfrm_day =="16"  }selected="selected"{/if} >16</option>
                    <option value="17" {if $booking.his.reg.arrfrm_day =="17"  }selected="selected"{/if} >17</option>
                    <option value="18" {if $booking.his.reg.arrfrm_day =="18"  }selected="selected"{/if} >18</option>
                    <option value="19" {if $booking.his.reg.arrfrm_day =="19"  }selected="selected"{/if} >19</option>
                    <option value="20" {if $booking.his.reg.arrfrm_day =="20"  }selected="selected"{/if} >20</option>
                    <option value="21" {if $booking.his.reg.arrfrm_day =="21"  }selected="selected"{/if} >21</option>
                    <option value="22" {if $booking.his.reg.arrfrm_day =="22"  }selected="selected"{/if} >22</option>
                    <option value="23" {if $booking.his.reg.arrfrm_day =="23"  }selected="selected"{/if} >23</option>
                    <option value="24" {if $booking.his.reg.arrfrm_day =="24"  }selected="selected"{/if} >24</option>
                    <option value="25" {if $booking.his.reg.arrfrm_day =="25"  }selected="selected"{/if} >25</option>
                    <option value="26" {if $booking.his.reg.arrfrm_day =="26"  }selected="selected"{/if} >26</option>
                    <option value="27" {if $booking.his.reg.arrfrm_day =="27"  }selected="selected"{/if} >27</option>
                    <option value="28" {if $booking.his.reg.arrfrm_day =="28"  }selected="selected"{/if} >28</option>
                    <option value="29" {if $booking.his.reg.arrfrm_day =="29"  }selected="selected"{/if} >29</option>
                    <option value="30" {if $booking.his.reg.arrfrm_day =="30"  }selected="selected"{/if} >30</option>
                    <option value="31" {if $booking.his.reg.arrfrm_day =="31"  }selected="selected"{/if} >31</option>
                  </select>
                  日  
                  ～
                  <select name="reg_arrto_year">
                    <option value="" {if $booking.reg.to_year == ""}selected="selected"{/if} label="----">----</option>
                    
                    
                    {foreach from=$form.bookyear item=year}
                    	
                    
                    <option value="{$year}" {if $booking.his.reg.arrto_year == $year}selected="selected"{/if}>{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="reg_arrto_month">
                    <option value=""   {if $booking.his.reg.arrto_month == ""}selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.reg.arrto_month == "1"}selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.arrto_month == "2"}selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.arrto_month == "3"}selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.arrto_month == "4"}selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.arrto_month == "5"}selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.arrto_month == "6"}selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.arrto_month == "7"}selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.arrto_month == "8"}selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.arrto_month == "9"}selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.arrto_month == "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.arrto_month == "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.arrto_month == "12"}selected="selected"{/if} >12</option>
                  </select>
                  月
                  <select style="" name="reg_arrto_day">
                    <option value=""   {if $booking.his.reg.arrto_day == ""}selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.reg.arrto_day == "1"}selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.arrto_day == "2"}selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.arrto_day == "3"}selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.arrto_day == "4"}selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.arrto_day == "5"}selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.arrto_day == "6"}selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.arrto_day == "7"}selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.arrto_day == "8"}selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.arrto_day == "9"}selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.arrto_day == "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.arrto_day == "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.arrto_day == "12"}selected="selected"{/if} >12</option>
                    <option value="13" {if $booking.his.reg.arrto_day == "13"}selected="selected"{/if} >13</option>
                    <option value="14" {if $booking.his.reg.arrto_day == "14"}selected="selected"{/if} >14</option>
                    <option value="15" {if $booking.his.reg.arrto_day == "15"}selected="selected"{/if} >15</option>
                    <option value="16" {if $booking.his.reg.arrto_day == "16"}selected="selected"{/if} >16</option>
                    <option value="17" {if $booking.his.reg.arrto_day == "17"}selected="selected"{/if} >17</option>
                    <option value="18" {if $booking.his.reg.arrto_day == "18"}selected="selected"{/if} >18</option>
                    <option value="19" {if $booking.his.reg.arrto_day == "19"}selected="selected"{/if} >19</option>
                    <option value="20" {if $booking.his.reg.arrto_day == "20"}selected="selected"{/if} >20</option>
                    <option value="21" {if $booking.his.reg.arrto_day == "21"}selected="selected"{/if} >21</option>
                    <option value="22" {if $booking.his.reg.arrto_day == "22"}selected="selected"{/if} >22</option>
                    <option value="23" {if $booking.his.reg.arrto_day == "23"}selected="selected"{/if} >23</option>
                    <option value="24" {if $booking.his.reg.arrto_day == "24"}selected="selected"{/if} >24</option>
                    <option value="25" {if $booking.his.reg.arrto_day == "25"}selected="selected"{/if} >25</option>
                    <option value="26" {if $booking.his.reg.arrto_day == "26"}selected="selected"{/if} >26</option>
                    <option value="27" {if $booking.his.reg.arrto_day == "27"}selected="selected"{/if} >27</option>
                    <option value="28" {if $booking.his.reg.arrto_day == "28"}selected="selected"{/if} >28</option>
                    <option value="29" {if $booking.his.reg.arrto_day == "29"}selected="selected"{/if} >29</option>
                    <option value="30" {if $booking.his.reg.arrto_day == "30"}selected="selected"{/if} >30</option>
                    <option value="31" {if $booking.his.reg.arrto_day == "31"}selected="selected"{/if} >31</option>
                  </select>
                  日 
                  
                  {if $error.hotel_stay1 != ""}
                  	<br/>{$error.hotel_stay1}
                  {/if}
              </tr>
              <tr>
                <th>到着日フライト <span class="text-small-red">※必須</span></th>
                <td><select name="reg_arrgo_year">
                    <option value="" {if $booking.his.reg.arrgo_year == ""}selected="selected"{/if}>----</option>
                    
                    
                    {foreach from=$form.bookyear item=year}
                                        
                    	
                    
                    <option value="{$year}" {if $booking.his.reg.arrgo_year == $year}selected="selected"{/if} >{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="reg_arrgo_month">
                    <option value=""   {if $booking.his.reg.arrgo_month == ""}selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.reg.arrgo_month == "1"}selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.arrgo_month == "2"}selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.arrgo_month == "3"}selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.arrgo_month == "4"}selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.arrgo_month == "5"}selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.arrgo_month == "6"}selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.arrgo_month == "7"}selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.arrgo_month == "8"}selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.arrgo_month == "9"}selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.arrgo_month == "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.arrgo_month == "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.arrgo_month == "12"}selected="selected"{/if} >12</option>
                  </select>
                  月
                  <select style="" name="reg_arrgo_day">
                    <option value=""   {if $booking.his.reg.arrgo_day == ""  }selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.reg.arrgo_day == "1" }selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.reg.arrgo_day == "2" }selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.reg.arrgo_day == "3" }selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.reg.arrgo_day == "4" }selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.reg.arrgo_day == "5" }selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.reg.arrgo_day == "6" }selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.reg.arrgo_day == "7" }selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.reg.arrgo_day == "8" }selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.reg.arrgo_day == "9" }selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.reg.arrgo_day == "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.reg.arrgo_day == "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.reg.arrgo_day == "12"}selected="selected"{/if} >12</option>
                    <option value="13" {if $booking.his.reg.arrgo_day == "13"}selected="selected"{/if} >13</option>
                    <option value="14" {if $booking.his.reg.arrgo_day == "14"}selected="selected"{/if} >14</option>
                    <option value="15" {if $booking.his.reg.arrgo_day == "15"}selected="selected"{/if} >15</option>
                    <option value="16" {if $booking.his.reg.arrgo_day == "16"}selected="selected"{/if} >16</option>
                    <option value="17" {if $booking.his.reg.arrgo_day == "17"}selected="selected"{/if} >17</option>
                    <option value="18" {if $booking.his.reg.arrgo_day == "18"}selected="selected"{/if} >18</option>
                    <option value="19" {if $booking.his.reg.arrgo_day == "19"}selected="selected"{/if} >19</option>
                    <option value="20" {if $booking.his.reg.arrgo_day == "20"}selected="selected"{/if} >20</option>
                    <option value="21" {if $booking.his.reg.arrgo_day == "21"}selected="selected"{/if} >21</option>
                    <option value="22" {if $booking.his.reg.arrgo_day == "22"}selected="selected"{/if} >22</option>
                    <option value="23" {if $booking.his.reg.arrgo_day == "23"}selected="selected"{/if} >23</option>
                    <option value="24" {if $booking.his.reg.arrgo_day == "24"}selected="selected"{/if} >24</option>
                    <option value="25" {if $booking.his.reg.arrgo_day == "25"}selected="selected"{/if} >25</option>
                    <option value="26" {if $booking.his.reg.arrgo_day == "26"}selected="selected"{/if} >26</option>
                    <option value="27" {if $booking.his.reg.arrgo_day == "27"}selected="selected"{/if} >27</option>
                    <option value="28" {if $booking.his.reg.arrgo_day == "28"}selected="selected"{/if} >28</option>
                    <option value="29" {if $booking.his.reg.arrgo_day == "29"}selected="selected"{/if} >29</option>
                    <option value="30" {if $booking.his.reg.arrgo_day == "30"}selected="selected"{/if} >30</option>
                    <option value="31" {if $booking.his.reg.arrgo_day == "31"}selected="selected"{/if} >31</option>
                  </select>
                  日
                  <input name="reg_arrgo_hh" type="text" class="width40" style="margin-left:15px;" value="{$booking.his.reg.arrgo_hh}" maxlength="2"  />
                  :
                  <input name="reg_arrgo_mm" type="text" class="width40" value="{$booking.his.reg.arrgo_mm}" maxlength="2"  />
                  
                  {if $error.hotel_stay2 != ""}
                  	<br/>{$error.hotel_stay2}
                  {/if}
                  
                  <br/>
                  <span class="text-small-red">※日をまたぐフライトの場合、出発日と到着日の日付は異なりますのでご注意ください。</span></td>
              </tr>
              <tr>
                <th>&nbsp;</th>
                <td>フライトナンバー
                  <input type="text" name="air_no" class="width80" value="{$booking.his.reg.air_no}" />
                  <span class="text-small">例) SQ20、NA774等</span>
              </tr>
              <tr>
                <th>ご旅行形態：<span class="text-small-red">※必須</span></th>
                <td><select name="sel_info" >
                    <option value="ルックJTB" {if $booking.his.reg.info == "ルックJTB"}selected="selected"{/if} >ルックJTB</option>
                    <option value="その他JTB" {if $booking.his.reg.info == "その他JTB"}selected="selected"{/if}>その他JTB</option>
                    <option value="JTB以外"   {if $booking.his.reg.info == "JTB以外"}selected="selected"{/if}>JTB以外</option>
                  </select></td>
              </tr>
            </table>
            <div class="height10"></div>
            <p>■参加者情報</p>
            <p>※参加者全員の情報をご記入ください</p>
            <input type="hidden" name="qty_adult"  value="{$form.qty_adult}"  />
            <input type="hidden" name="qty_child"  value="{$form.qty_child}" />
            <input type="hidden" name="qty_infant" value="{$form.qty_infant}" />
            {foreach from=$form.arr_guest item=rec_guest}
            
            {if $rec_guest != 0}
            <div class="heigth5"></div>
            {/if}
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>参加者名（ローマ字）<span class="text-small-red">※必須</span></th>
                <td>
                  {$error.guest[$rec_guest].name} 
                  <input type="text" name="inp_guest_firstname[]" class="inp-name" value="{$booking.his.guest.$rec_guest.firstname}" />
                  <input type="text" name="inp_guest_lastname[]" class="inp-name"  value="{$booking.his.guest.$rec_guest.lastname}" />
                  
                  <span class="text-small-red">※パスポート記載の氏名をご記入ください</span>
                  </td>
              </tr>
              <tr>
                <th>参加者性別 <span class="text-small-red">※必須</span></th>
                <td>
                  {$error.guest[$rec_guest].sex}
                  <select name="inp_guest_sex[]">
                    <option value="">---</option>
                    <option value="男性" {if $booking.his.guest.$rec_guest.sex  == "男性" }selected="selected"{/if} >男性</option>
                    <option value="女性" {if $booking.his.guest.$rec_guest.sex  == "女性" }selected="selected"{/if} >女性</option>
                  </select></td>
              </tr>
              <tr>
                <th>参加者年齢 <span class="text-small-red">※必須</span></th>
                <td>
                   
                  <select name="inp_guest_age[]">
                    
                    
                  {foreach from=$form.age item=age}
                  	
                    
                    <option value="{$age}" {if $booking.his.guest.$rec_guest.age == $age }selected="selected"{/if} >{$age}歳</option>
                    
                    
                  {/foreach}
               
                  
                  </select></td>
              </tr>
              <tr>
                <th>参加者生年月日</th>
                <td><select name="inp_guest_birth_year[]">
                    <option selected="selected" value=""  {if $booking.his.guest.$rec_guest.birth_year == "" }selected="selected"{/if} >----</option>
                    
                    
                    {foreach from=$form.guest_birth_year item=guest_birth_year}
                    	
                    
                    <option value="{$guest_birth_year}"  {if $booking.his.guest.$rec_guest.birth_year == $guest_birth_year }selected="selected"{/if} > {$guest_birth_year} </option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select name="inp_guest_birth_month[]">
                    <option value=""   {if $booking.his.guest.$rec_guest.birth_month == ""  }selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.guest.$rec_guest.birth_month == "1" }selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.guest.$rec_guest.birth_month == "2" }selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.guest.$rec_guest.birth_month == "3" }selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.guest.$rec_guest.birth_month == "4" }selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.guest.$rec_guest.birth_month == "5" }selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.guest.$rec_guest.birth_month == "6" }selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.guest.$rec_guest.birth_month == "7" }selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.guest.$rec_guest.birth_month == "8" }selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.guest.$rec_guest.birth_month == "9" }selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.guest.$rec_guest.birth_month == "10" }selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.guest.$rec_guest.birth_month == "11" }selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.guest.$rec_guest.birth_month == "12" }selected="selected"{/if} >12</option>
                  </select>
                  月
                  <select name="inp_guest_birth_day[]">
                    <option value=""   {if $booking.his.guest.$rec_guest.birth_day == ""  }selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.guest.$rec_guest.birth_day == "1"  }selected="selected"{/if}>1</option>
                    <option value="2"  {if $booking.his.guest.$rec_guest.birth_day == "2"  }selected="selected"{/if}>2</option>
                    <option value="3"  {if $booking.his.guest.$rec_guest.birth_day == "3"  }selected="selected"{/if}>3</option>
                    <option value="4"  {if $booking.his.guest.$rec_guest.birth_day == "4"  }selected="selected"{/if}>4</option>
                    <option value="5"  {if $booking.his.guest.$rec_guest.birth_day == "5"  }selected="selected"{/if}>5</option>
                    <option value="6"  {if $booking.his.guest.$rec_guest.birth_day == "6"  }selected="selected"{/if}>6</option>
                    <option value="7"  {if $booking.his.guest.$rec_guest.birth_day == "7"  }selected="selected"{/if}>7</option>
                    <option value="8"  {if $booking.his.guest.$rec_guest.birth_day == "8"  }selected="selected"{/if}>8</option>
                    <option value="9"  {if $booking.his.guest.$rec_guest.birth_day == "9"  }selected="selected"{/if}>9</option>
                    <option value="10" {if $booking.his.guest.$rec_guest.birth_day == "10" }selected="selected"{/if}>10</option>
                    <option value="11" {if $booking.his.guest.$rec_guest.birth_day == "11" }selected="selected"{/if}>11</option>
                    <option value="12" {if $booking.his.guest.$rec_guest.birth_day == "12" }selected="selected"{/if}>12</option>
                    <option value="13" {if $booking.his.guest.$rec_guest.birth_day == "13" }selected="selected"{/if}>13</option>
                    <option value="14" {if $booking.his.guest.$rec_guest.birth_day == "14" }selected="selected"{/if}>14</option>
                    <option value="15" {if $booking.his.guest.$rec_guest.birth_day == "15" }selected="selected"{/if}>15</option>
                    <option value="16" {if $booking.his.guest.$rec_guest.birth_day == "16" }selected="selected"{/if}>16</option>
                    <option value="17" {if $booking.his.guest.$rec_guest.birth_day == "17" }selected="selected"{/if}>17</option>
                    <option value="18" {if $booking.his.guest.$rec_guest.birth_day == "18" }selected="selected"{/if}>18</option>
                    <option value="19" {if $booking.his.guest.$rec_guest.birth_day == "19" }selected="selected"{/if}>19</option>
                    <option value="20" {if $booking.his.guest.$rec_guest.birth_day == "20" }selected="selected"{/if}>20</option>
                    <option value="21" {if $booking.his.guest.$rec_guest.birth_day == "21" }selected="selected"{/if}>21</option>
                    <option value="22" {if $booking.his.guest.$rec_guest.birth_day == "22" }selected="selected"{/if}>22</option>
                    <option value="23" {if $booking.his.guest.$rec_guest.birth_day == "23" }selected="selected"{/if}>23</option>
                    <option value="24" {if $booking.his.guest.$rec_guest.birth_day == "24" }selected="selected"{/if}>24</option>
                    <option value="25" {if $booking.his.guest.$rec_guest.birth_day == "25" }selected="selected"{/if}>25</option>
                    <option value="26" {if $booking.his.guest.$rec_guest.birth_day == "26" }selected="selected"{/if}>26</option>
                    <option value="27" {if $booking.his.guest.$rec_guest.birth_day == "27" }selected="selected"{/if}>27</option>
                    <option value="28" {if $booking.his.guest.$rec_guest.birth_day == "28" }selected="selected"{/if}>28</option>
                    <option value="29" {if $booking.his.guest.$rec_guest.birth_day == "29" }selected="selected"{/if}>29</option>
                    <option value="30" {if $booking.his.guest.$rec_guest.birth_day == "30" }selected="selected"{/if}>30</option>
                    <option value="31" {if $booking.his.guest.$rec_guest.birth_day == "31" }selected="selected"{/if}>31</option>
                  </select>
                  日 </td>
              </tr>
              <tr>
                <th>参加者パスポート番号 </th>
                <td><input type="text" name="inp_guest_sn[]" value="{$booking.his.guest.$rec_guest.sn}" /></td>
              </tr>
              <tr>
                <th>参加者パスポート<br/>
                  Expirity date</th>
                <td><select name="inp_guest_expire_year[]">
                    <option  value="" {if $booking.his.guest.$rec_guest.expire_year == ""  }selected="selected"{/if} >----</option>
                    
                    
                    {foreach from=$form.guest_sn_year item=guest_sn_year}
                    	
                    
                    <option value="{$guest_sn_year}" {if $booking.his.guest.$rec_guest.expire_year == $guest_sn_year  }selected="selected"{/if}>{$guest_sn_year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select name="inp_guest_expire_month[]">
                    <option {if $booking.his.guest.$rec_guest.expire_month == ""  }selected="selected"{/if} value="">--</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "1" }selected="selected"{/if} value="1" >1</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "2" }selected="selected"{/if} value="2" >2</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "3" }selected="selected"{/if} value="3" >3</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "4" }selected="selected"{/if} value="4" >4</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "5" }selected="selected"{/if} value="5" >5</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "6" }selected="selected"{/if} value="6" >6</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "7" }selected="selected"{/if} value="7" >7</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "8" }selected="selected"{/if} value="8" >8</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "9" }selected="selected"{/if} value="9" >9</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "10" }selected="selected"{/if} value="10" >10</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "11" }selected="selected"{/if} value="11" >11</option>
                    <option {if $booking.his.guest.$rec_guest.expire_month == "12" }selected="selected"{/if} value="12" >12</option>
                  </select>
                  月
                  <select name="inp_guest_expire_day[]">
                    <option value=""   {if $booking.his.guest.$rec_guest.expire_day == ""  }selected="selected"{/if} >--</option>
                    <option value="1"  {if $booking.his.guest.$rec_guest.expire_day == "1"  }selected="selected"{/if} >1</option>
                    <option value="2"  {if $booking.his.guest.$rec_guest.expire_day == "2"  }selected="selected"{/if} >2</option>
                    <option value="3"  {if $booking.his.guest.$rec_guest.expire_day == "3"  }selected="selected"{/if} >3</option>
                    <option value="4"  {if $booking.his.guest.$rec_guest.expire_day == "4"  }selected="selected"{/if} >4</option>
                    <option value="5"  {if $booking.his.guest.$rec_guest.expire_day == "5"  }selected="selected"{/if} >5</option>
                    <option value="6"  {if $booking.his.guest.$rec_guest.expire_day == "6"  }selected="selected"{/if} >6</option>
                    <option value="7"  {if $booking.his.guest.$rec_guest.expire_day == "7"  }selected="selected"{/if} >7</option>
                    <option value="8"  {if $booking.his.guest.$rec_guest.expire_day == "8"  }selected="selected"{/if} >8</option>
                    <option value="9"  {if $booking.his.guest.$rec_guest.expire_day == "9"  }selected="selected"{/if} >9</option>
                    <option value="10" {if $booking.his.guest.$rec_guest.expire_day == "10" }selected="selected"{/if} >10</option>
                    <option value="11" {if $booking.his.guest.$rec_guest.expire_day == "11" }selected="selected"{/if} >11</option>
                    <option value="12" {if $booking.his.guest.$rec_guest.expire_day == "12" }selected="selected"{/if} >12</option>
                    <option value="13" {if $booking.his.guest.$rec_guest.expire_day == "13" }selected="selected"{/if} >13</option>
                    <option value="14" {if $booking.his.guest.$rec_guest.expire_day == "14" }selected="selected"{/if} >14</option>
                    <option value="15" {if $booking.his.guest.$rec_guest.expire_day == "15" }selected="selected"{/if} >15</option>
                    <option value="16" {if $booking.his.guest.$rec_guest.expire_day == "16" }selected="selected"{/if} >16</option>
                    <option value="17" {if $booking.his.guest.$rec_guest.expire_day == "17" }selected="selected"{/if} >17</option>
                    <option value="18" {if $booking.his.guest.$rec_guest.expire_day == "18" }selected="selected"{/if} >18</option>
                    <option value="19" {if $booking.his.guest.$rec_guest.expire_day == "19" }selected="selected"{/if} >19</option>
                    <option value="20" {if $booking.his.guest.$rec_guest.expire_day == "20" }selected="selected"{/if} >20</option>
                    <option value="21" {if $booking.his.guest.$rec_guest.expire_day == "21" }selected="selected"{/if} >21</option>
                    <option value="22" {if $booking.his.guest.$rec_guest.expire_day == "22" }selected="selected"{/if} >22</option>
                    <option value="23" {if $booking.his.guest.$rec_guest.expire_day == "23" }selected="selected"{/if} >23</option>
                    <option value="24" {if $booking.his.guest.$rec_guest.expire_day == "24" }selected="selected"{/if} >24</option>
                    <option value="25" {if $booking.his.guest.$rec_guest.expire_day == "25" }selected="selected"{/if} >25</option>
                    <option value="26" {if $booking.his.guest.$rec_guest.expire_day == "26" }selected="selected"{/if} >26</option>
                    <option value="27" {if $booking.his.guest.$rec_guest.expire_day == "27" }selected="selected"{/if} >27</option>
                    <option value="28" {if $booking.his.guest.$rec_guest.expire_day == "28" }selected="selected"{/if} >28</option>
                    <option value="29" {if $booking.his.guest.$rec_guest.expire_day == "29" }selected="selected"{/if} >29</option>
                    <option value="30" {if $booking.his.guest.$rec_guest.expire_day == "30" }selected="selected"{/if} >30</option>
                    <option value="31" {if $booking.his.guest.$rec_guest.expire_day == "31" }selected="selected"{/if} >31</option>
                  </select>
                  日 </td>
              </tr>
            </table>
            {/foreach}
            <div class="height10"></div>
            <p>■その他の情報</p>
            <table cellspacing="1" class="tbform">
              <tr>
                <th >&nbsp;</th>
                <td><textarea name="remark" rows="5" class="width350">{$booking.his.reg.remark}</textarea></td>
              </tr>
            </table>
            <!-- form Register-->
          </form>
          <span style="padding:5px 150px 5px 165px; display:block;">
          <input type="button" class="btn_submit gotoback" value="戻る" style="width:70px;" />
          <input type="button" class="btn_submit gotonext" value="次へ" style="width:70px;" />
          </span> 
				
                 {literal}
                  <div style="height:100px;"></div>
                    <div style="float:right; margin-right:5px;">
                    
                    <span id="ss_gmo_img_wrapper_100-50_image_ja">
                    <a href="https://jp.globalsign.com/" target="_blank">
                    <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
                    </a>
                    </span>
                    <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
                  </div>
                  {/literal}
          </div>
      </div>
    </div>
    {if $config.allot != 'false'}
    <form action="booking_cart.php" name="gotoback" method="post">
    </form>
    {else}
    <form action="booking.php?product_id={$booking.product.id}" name="gotoback" method="post">
    </form>
    {/if}
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>