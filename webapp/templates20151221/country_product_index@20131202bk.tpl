{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear  "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <h1 class="header-text-tittle">{$product.product_name_jp}</h1>
    <div class="product-top-image"> 
		{$product_top}    
     </div>
     
    <div class="product-button">
      <ul class="product-button-list">
      	{$product_temp_bottom}   
        
      </ul>
      <div class="clear"></div>
      <div class="product-btncat-box"> {$product.category} </div>
      
      {if $product.rev_status == 1}
      <div class="product-rew-box"> <a class="btn-review" href="review.php?product_id={$product.product_id}">ツアー体験記はこちら </a> </div>
      {/if}
      <div class="clear"></div>
    </div>
    
    
    <div class="product-detail">
      <p>{$product.short_desc}</p>
      <p><strong>ツアー料金</strong>： {$product.price_min} {if $product.show_rate eq 1}( 目安：\{$product.price_yen} ){/if} </p>
      <p><strong>最少催行人員</strong>：{$product.pax_min}名様</p>
      <p><strong>申込可能日　（催行日　{$product.day_cutoff+1}日　前まで）</strong></p>
      
      
      <p>{$productdetail}</p>
      
     
    </div>
    <div class="product-information">
      <div class="product-info"> 
      	{if $product.template_id == 1}
        
        {if $product.text_top != ""}
        <p>{$product.text_top}</p>
        {/if}
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
          	<tr>
            <th colspan="2" class="table-header">スケジュール</th></tr>
            {$product.text_table}
          </table>
        </div>
        
        {if $product.text_bottom != ""}<p>{$product.text_bottom}</p>{/if}
        {/if}
        
        {if $product.template_id == 2}
        <div class="product-table"> <strong>スケジュール</strong>
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            {$product.text_table}
          </table>
        </div>
        {/if} 
      <div class="clear"></div>
      {if $product.template_id == 3}
      <div class="product_text">{$product.text_top}</div>
      
      
      <div class="height10"></div>
      <div class="clear"></div>
      {/if}
      
      {if $product.remark != ""}
       <p>{$product.remark}</p>
      {/if}
      
      {if $product.itiner != ""}
      
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            <tr>
              <th colspan="3" class="table-header">Itinerary</th>
            </tr>
            {$product.itiner}
          </table>
        </div>
      
    <!--product itiner-->
    {/if}
    
    </div>
      <div class="clear"></div>
    </div>
    
    {if $product.policy != ""}
    <!--product policy-->
    <div class="product-policy">
      <div class="product-info">
        <strong>Policy</strong>
          <div>{$product.policy}</div>
        
      </div>
      <div class="clear"></div>
    </div>
    <!--product policy-->
    {/if}
    
    <div class="height10"></div>
   
    <div class="height10"></div>
    <a href="https://{$smarty.server.HTTP_HOST}/{$config.countryname}/booking.php?product_id={$product.product_id}" class="product-btn-booking">Booking</a>
    <div class="height10"></div>
    
    <!--banner footer-->
    <!--banner footer-->
     
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>