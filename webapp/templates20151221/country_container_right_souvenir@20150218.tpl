{if $config.country eq "TWN"}
<div class="container-right">
	<div class="container-right-tittle">
		<p class=" souvenir_tour_ranking_head">海外おみやげ</p>
	</div>
	
	<ul class="container-right-list">
		<li class="ptop10">
			<div class="right-list-product-subject icontop1"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=870"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=870-1.jpg" alt="" class="right-list-product-img" width="90"   /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=870">台湾限定　小籠包キューピー</a></div>
			<div class="right-list-product-txt">
				<p >みんなが大好きな“小籠包”がキューピーさんになって台湾に登場！ 「アイラブ台湾」の文字を蒸篭に添えて、「台湾推し」の皆様にお届け。マイバスデスク限定販売商品です。</p>
				<span class="txt-red-bold-price">NT$250</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
		<li>
			<div class="right-list-product-subject icontop2"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=403"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=403-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=403">【2014年】台湾フレッシュ愛文マンゴー　5Kg</a></div>
			<div class="right-list-product-txt">
				<p >今年も日本の皆様に、台湾の幸せをお届けします！ 待ちに待った台湾愛文マンゴーの季節が到来！ JTB台湾の契約農家が丹精こめて育てたアップルマンゴーを、 糖度、見た目も粒よりを厳選して、航空＆クー...</p>
				<span class="txt-red-bold-price">NT$2,800</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
		<li>
			<div class="right-list-product-subject icontop3"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=405"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=405-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=405">【2014年】台湾フレッシュ玉荷包ライチ　3Kg　</a></div>
			<div class="right-list-product-txt">
				<p>昨年も大好評！翡翠のような最高級ライチ。ジューシーでフレッシュな台湾＜玉荷包ライチ＞を航空＆クール便で日本へお届けします！</p>
				<span class="txt-red-bold-price">NT$2,400</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
		<li>
			<div class="right-list-product-subject icontop4"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=871"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=871-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=871">阿里山無農薬コーヒー ドリップ5袋</a></div>
			<div class="right-list-product-txt">
				<p>最高級の阿里山にこだわった、日本スペシャルティーコーヒー協会認定コーヒースペシャリストのロースターが焼く特別なコーヒー豆。 オフィスや旅先でも気軽に美味しい一杯を楽しめる便利なドリップ式を、おしゃれ...</p>
				<span class="txt-red-bold-price">NT$450</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
		<li >
			<div class="right-list-product-subject icontop5"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=406"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=406-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=406">【2014年】台湾フレッシュ黒葉ライチ　3Kg</a></div>
			<div class="right-list-product-txt">
				<p>昨年も大好評！ジューシーでフレッシュな台湾＜黒葉ライチ＞を航空＆クール便で日本へお届けします！ </p>
				<span class="txt-red-bold-price">NT$2,000</span> </div>
			<div class="clear"></div>
		</li>
	</ul>
	
	<div class="ptop10"></div>
	<a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank"><img src="/images/btn_opt_taiwan.jpg" alt="" /></a> <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank">日本発の台湾ツアーをお探しのお客様は</a>
	<div class="ptop10"></div>
	<a href="https://www.facebook.com/pages/JTB%E5%8F%B0%E6%B9%BE/258090714317394" target="_blank"><img src="/images/findusonfacebook.jpg" alt="" /></a>

	<!-- <div class="ptop10"></div>
    <a  onclick="_gaq.push(['_trackEvent', 'docomo_banner_in_tw', 'click']);" href="http://id-credit.com/idpp14first/index.html"  target="_blank"><img src="/images/btn_idpaypasscp.jpg" alt="" /></a>-->

	<div style="height:10px;"></div>
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html"><img src="/images/btn_opttwn_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html">オプショナルツアーオンラインカタログ 2015年3月まで</a>

<div style="height:10px;"></div>
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html"><img src="/images/btn_opttwn_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html">オプショナルツアーオンラインカタログ 2015年4月以降</a>


	<div class="ptop10"></div>
	<a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a> </div>
<div class="clear"></div>

{else}

<div class="container-right">
    <div class="container-right-tittle">
      <p class=" souvenir_tour_ranking_head">海外おみやげ</p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_1}"><img src="{$opt_top5_pic_1}" alt="" class="right-list-product-img" width="90"   /></a>      
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_1}">{$opt_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_1}</p>
          {$opt_top5_price_1}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_2}"><img src="{$opt_top5_pic_2}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_2}">{$opt_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_2}</p>
          {$opt_top5_price_2}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_3}"><img src="{$opt_top5_pic_3}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_3}">{$opt_top5_name_3}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_3}</p>
          {$opt_top5_price_3}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop4">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_4}"><img src="{$opt_top5_pic_4}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_4}">{$opt_top5_name_4}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_4}</p>
          {$opt_top5_price_4}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li > 
      	<div class="right-list-product-subject icontop5">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_5}"><img src="{$opt_top5_pic_5}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_5}">{$opt_top5_name_5}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_5}</p>
          {$opt_top5_price_5}
        </div>
        <div class="clear"></div>
        
      </li>
    </ul>
    
    {if $config.country eq "NZL"}
    <div class="ptop10"></div>
    <a href="http://www.jtb.co.nz/souvenir/"  target="_blank"><img src="/images/btn_souvenir_link.gif" alt="souvenir_banner" /></a>
    
    <a href="http://www.jtb.co.nz/souvenir/"  target="_blank" class="text_link">更に日本へ発送できる商品をもっと見たいお客様は、こちら。</a>

    {/if}
    
    <div class="ptop10"></div>
     {if $config.country eq "TWN"}
    <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank"><img src="/images/btn_opt_taiwan.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank">日本発の台湾ツアーをお探しのお客様は</a>
    
    <div class="ptop10"></div>
    <a href="https://www.facebook.com/pages/JTB%E5%8F%B0%E6%B9%BE/258090714317394" target="_blank"><img src="/images/findusonfacebook.jpg" alt="" /></a>
    	
      <div class="ptop10"></div>
    <a href=" http://id-credit.com/idpp14first/

" target="_blank"><img src="/images/btn_idpaypasscp.jpg" alt="" /></a>
   


	<div class="ptop10"></div>
	 <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" ><img src="/images/btn_opttwn_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" >オプショナルツアーオンラインカタログ</a>
	
	
	
	
	
    
    {elseif $config.country eq "IDN"}
    <a href="http://www.jtb.co.jp/kaigai/asia_resort/index.asp" target="_blank"><img src="/images/btn_opt_indonesia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia_resort/index.asp" target="_blank">日本発のバリ島ツアーをお探しのお客様は</a>
  	<div class="ptop10"></div>
    <a href="http://ameblo.jp/apa-saja/" target="_blank"><img src="/images/btn_apa_indonesia.jpg" alt="" /></a>
    <a href="http://ameblo.jp/apa-saja/" target="_blank">【バリ島】現地スタッフブログはこちら</a>
  	
    
    
   
       <div class="ptop10"></div>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_optidn_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
    
   <div class="ptop10"></div>
    <a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a>
  
    {elseif $config.country eq "THA"}
    <a href="http://www.jtb.co.jp/kaigai/asia/thailand/index.asp" target="_blank"><img src="/images/btn_opt_thailand.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/thailand/index.asp" target="_blank">日本発のタイツアーをお探しのお客様は</a>
    
    <div class="ptop10"></div>
   <!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_opttha_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->
   
    

    {elseif $config.country eq "VNM"}
    <a href="http://www.jtb.co.jp/kaigai/asia/vietnam/index.asp" target="_blank"><img src="/images/btn_opt_vietnam.jpg" alt="" /></a>	
    <a href="http://www.jtb.co.jp/kaigai/asia/vietnam/index.asp" target="_blank">日本発のベトナムツアーをお探しのお客様は</a>
    
	
	<div class="ptop10"></div>
	<!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_optvet_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->
	
	
    {elseif $config.country eq "KHM"}
    <a href="http://www.jtb.co.jp/kaigai/asia/cambodia/index.asp" target="_blank"><img src="/images/btn_opt_cambodia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/cambodia/index.asp" target="_blank">日本発のカンボジアツアーをお探しのお客様は</a>
   
   <div class="ptop10"></div>
	<!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_optcam_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->   
   
   
    {elseif $config.country eq "MYS"}
    <a href="http://www.jtb.co.jp/kaigai/asia/malaysia/index.asp" target="_blank"><img src="/images/btn_opt_malayasia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/malaysia/index.asp" target="_blank">日本発のマレーシアツアーをお探しのお客様は</a>
    
	<div class="ptop10"></div>
	<!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_optmys_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->
	

    {elseif $config.country eq "SGP"}
    <a href="http://www.jtb.co.jp/kaigai/asia/singapore/index.asp" target="_blank"><img src="/images/btn_opt_singapore.jpg" alt="" /></a>
  	<a href="http://www.jtb.co.jp/kaigai/asia/singapore/index.asp" target="_blank">日本発のシンガポールツアーをお探しのお客様は</a>
    
	 
	

    
    
    <div style="height:10px;"></div>
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html"><img src="/images/btn_optsga_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html">オプショナルツアーオンラインカタログ</a>
	
    
    
    
    
    {elseif $config.country eq "HKG"}
   
    {elseif $config.country eq "AUS"}
    <a href="http://www.jtb.co.jp/kaigai/oceania/australia/index.asp" target="_blank"><img src="/images/btn_opt_australia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/oceania/australia/index.asp" target="_blank">日本発のオーストラリアツアーをお探しのお客様は</a>

	
	<div class="ptop10"></div>
	<!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" ><img src="/images/btn_optaus_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->


    {elseif $config.country eq "NZL"}    
    <a href="http://www.jtb.co.jp/kaigai/oceania/newzealand/index.asp" target="_blank"><img src="/images/btn_opt_newzealand.jpg" alt="" /></a>
	<a href="http://www.jtb.co.jp/kaigai/oceania/newzealand/index.asp" target="_blank">日本発のニュージーランドツアーをお探しのお客様は</a>
    
    
    <div class="ptop10"></div>
    <a href="http://blog.jtb.co.nz/" target="_blank"><img src="/images/btn_nzl_blog.jpg" alt="" /></a>

	<div class="ptop10"></div>
    
	
	<!--online catalog-->
	
	
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html"><img src="/images/btn_optnzl_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html">オプショナルツアーオンラインカタログ</a>
    <!--online catalog-->
    
    
    {/if}
    {if $config.country <> "IDN"}
    <div class="ptop10"></div>
    <a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a>
    {/if}
    
    
  </div>
  <div class="clear"></div>





{/if}