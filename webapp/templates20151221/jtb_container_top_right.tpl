<div class="container-right">
    <div class="container-right-tittle">
      <p class=" optional_tour_ranking_head">オプショナルツアー </p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="{$opt_top5_link_1}"><img src="{$opt_top5_pic_1}" alt="" class="right-list-product-img" width="112" height="46"  /></a>      
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_1}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_1}">{$opt_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_1}</p>
          {$opt_top5_price_1}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="{$opt_top5_link_2}"><img src="{$opt_top5_pic_2}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_2}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_2}">{$opt_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_2}</p>
          {$opt_top5_price_2}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="{$opt_top5_link_3}"><img src="{$opt_top5_pic_3}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_3}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_3}">{$opt_top5_name_3}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_3}</p>
          {$opt_top5_price_3}
        </div>
        <div class="clear"></div>
      </li>
    </ul>
    
    <div style="height:10px; line-height:10px; display:block;"></div>
   
    <div class="container-right-tittle">
      <p class="souvenir_tour_ranking_head">海外おみやげ</p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="{$lp_top5_link_1}"><img src="{$lp_top5_pic_1}" alt="" class="right-list-product-img" width="90"/></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-txt">{$lp_top5_country_1}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_1}">{$lp_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_1}</p>
          {$lp_top5_price_1}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="{$lp_top5_link_2}"><img src="{$lp_top5_pic_2}" alt="" class="right-list-product-img" width="90" /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-txt">{$lp_top5_country_2}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_2}">{$lp_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_2}</p>
          {$lp_top5_price_2}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="{$lp_top5_link_3}"><img src="{$lp_top5_pic_3}" alt="" class="right-list-product-img" width="90" /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-txt">{$lp_top5_country_3}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_3}">{$lp_top5_name_3}</a></div>        
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_3}</p>
          {$lp_top5_price_3}
        </div>
        <div class="clear"></div>
      </li>
    </ul>
    
  </div>
  <div class="clear"></div>