{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider"> <img src="images/country/001.jpg" alt="" />
    <img src="images/country/002.jpg" alt=""  class="hide" />
    <img src="images/country/003.jpg" alt=""  class="hide" />
    <img src="images/country/004.jpg" alt=""  class="hide" />
    <img src="images/country/005.jpg" alt=""  class="hide" />
    <img src="images/country/006.jpg" alt=""  class="hide" />
    <img src="images/country/007.jpg" alt=""  class="hide" />
    <img src="images/country/008.jpg" alt=""  class="hide" />
    <img src="images/country/009.jpg" alt=""  class="hide" />
    <img src="images/country/010.jpg" alt="" class="hide" /> </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="text-content">
     
    {if $config.country eq "TWN"}
    <h1 class="font_red">台湾観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "IDN"}
    <h1 class="font_red">バリ島/インドネシア観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "THA"}
    <h1 class="font_red">タイ観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "VNM"}
    <h1 class="font_red">ベトナム観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "KHM"}
    <h1 class="font_red">カンボジア観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "MYS"}
    <h1 class="font_red">マレーシア観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "SGP"}
    <h1 class="font_red">シンガポール観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "HKG"}
    <h1 class="font_red">香港観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "AUS"}
    <h1 class="font_red">オーストラリア観光・オプショナルツアー「マイバス」</h1>
    {elseif $config.country eq "NZL"}
    <h1 class="font_red"></h1>
    {/if}
     
    {if $config.country eq "TWN"}
    <p>歓迎光臨！マイバス台湾では、初めての方にも、もっと台湾を知りたい方にもお楽しみいただけるオプショナルツアーをご用意しています。固定古蹟の「総統府」、ノスタルジック雰囲気に包まれる街「九份」、新幹線で行く「水の都高雄」を訪れるコースや、願い事を書いて飛ばす「天燈あげ」も大好評。旅先ならではのリラックスに「スパ・マッサージ」、言葉やチップの心配無用な「各種ミールクーポン」では家庭的なお食事から宮廷料理まで豊富なメニューを取り揃えました。旅の思い出に、台湾観光に彩りを添える、マイバスオプショナルツアーを是非ご利用ください。</p>
    {elseif $config.country eq "IDN"}
    <p>インドネシア・バリ島のご旅行は当島で開業２０年の実績のあるJTBバリ支店にお任せください。観光、各種アクティビティ、ディナー、スパ等等、バリ島の魅力を取り上げた多彩なオプショナルツアーをご用意しております。楽しく忘れられない旅の思い出をつくりましょう。</p>
    {elseif $config.country eq "THA"}
    <p>サワディーカッ！観るも食べるも癒されるも、全てにおいて充実したタイランド。世界遺産に古を学び、寺院で地元の人々の信仰心に触れ・・・ビーチではじけたあとは、美味しいタイフードに舌鼓。大都会バンコクでは買物三昧に食い倒れ。そんなAmazing Thailand があなたをお待ちしております。タイ観光・オプショナルツアーは「マイバス」タイランドにお任せください。</p>
    {elseif $config.country eq "VNM"}
    <p>弊社はホーチミン及びハノイに支店をもち日本からのお客様並びにベトナム在住の皆様に対し旅行業のあらゆるサービスをご提供させて頂いております。特にホーチミンにおいては市内中心部にデスク機能をドンコイデスク店/マジェスティックホテル隣、に設置しホーチミンご到着後のお客様に対し万全な対応を行える体制を整えております。また、それらのデスクでは、現地ご到着後の追加オプション申し込みやあらゆる旅のご相談に対しお客様のご満足を頂ける各種情報も整えております。ベトナム観光・オプショナルツアーのご用命は弊社デスクへ是非お立ち寄り下さい。</p>
    {elseif $config.country eq "KHM"}
    <p>JTBが自信を持ってお届けする、カンボジアをはじめとしたアジア・オセアニア専門の旅行商品販売サイトです。カンボジア現地オプショナルツアー「マイバス」・飛行機なしの旅行パッケージ「ランドパッケージ」を中心に商品をご提供しております。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。お客様の「感動のそばに、いつも」寄添いたい。そんな気持ちで皆様のお越しを心よりお待ちしております。</p>
    {elseif $config.country eq "MYS"}
    <p>自然豊かなボルネオ島のコタキナバルやランカウイ島、世界遺産の街マラッカやペナンのジョージタウン、長期滞在先としても人気のクアラルンプールなどマレーシアの旅のことならホテルや、日本語ガイドがご案内する定期観光・オプショナルツアーだけでなく、レストランやスパのご予約までなんでもお任せください！思い出に残る旅のお手伝いをいたします！</p>
    {elseif $config.country eq "SGP"}
    <p>マイバスシンガポールへようこそ！シンガポールは前代未聞の大変革中！今なお進化し続けていて、めまぐるしい変貌は絶対に見逃せません。小さいけれど緑豊かな南の島にはおもちゃ箱のように夢がいっぱい詰まっているんです。度肝を抜く奇抜さと驚きの大建造物、動物や自然と触れ合える都会のオアシス、異文化のユニークちゃんぽん、グルメにはたまらない美食探訪、可愛いアジアン雑貨から最旬のブランドまでの買い物天国、子どもも大人も大興奮のエンターテインメントの数々、一流ホテルでゴージャスな紳士・淑女気分…古きよき伝統と新しいトレンドの楽しみ方は十人十色。あなた流の豊かな時間を演出してみてください。シンガポール観光に彩を添える魅力的なオプショナルツアーをご用意してお待ちしております。</p>
    {elseif $config.country eq "HKG"}
    <p>JTBが自信を持ってお届けする、香港をはじめとしたアジア・オセアニア専門の旅行商品販売サイトです。香港現地オプショナルツアー「マイバス」・飛行機なしの旅行パッケージ「ランドパッケージ」を中心に商品をご提供しております。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。お客様の「感動のそばに、いつも」寄添いたい。そんな気持ちで皆様のお越しを心よりお待ちしております。</p>
    {elseif $config.country eq "AUS"}
    <p>オーストラリアは、日本の約20倍の国土を持ち、19の世界遺産がある南半球の国。雄大な自然の中、コアラやカンガルーなどの野生動物との触れ合い、グレートバリアリーフでのダイビングやマリンスポーツ、ロマンチックなクルーズ、ワインと食の旅など、ひとり旅からカップル、家族・グループの方々に広くお楽しみいただける観光・オプショナルツアーをご用意しております。トロピカルなケアンズ・ゴールドコースト、温暖なシドニー・メルボルン、乾燥した内陸のエアーズロックなど、スケールの大きな体験があなたを待っています。</p>
    {elseif $config.country eq "NZL"}
    <p>ニュージーランドには、皆様にご利用いただけるマイバスセンター、ツアーデスクが、オークランド、クライストチャーチ（2011年12月現在　一時閉鎖中）、クイーンズタウンの3ヶ所にございます。現地オプショナルツアーや人気レストランのお食事券（ミールクーポン）など、多数の商品を取り扱っております。スタッフ全員が日本語を話しますので、お気軽にお立ち寄りください。
        主要クレジットカード、Ji保険の現地対応窓口として無料サポートもいたします。“地球の箱庭”ニュージーランドへお越しの際は、真っ先にマイバスセンター、ツアーデスクへどうぞ！
    </p>
    {/if}
    
      
    </div>
    <div class="header-text-tittle">おすすめオプショナルツアー　MyBus</div>
    <div class="image-slider">
      <div class="slider-left"></div>
      <div class="slider-mid">
        <ul class="slider-content-index">
          {$productslider1}
        </ul>
      </div>
      <div class="slider-right"></div>
    </div>
    
       {if $config.country eq 'TWN' ||
      $config.country eq 'SGP' || 
      $config.country eq 'NZL' || 
      $config.country eq 'MYS' || 
      $config.country eq 'IDN' ||
      $config.country eq 'AUS' || 
      $config.country eq 'VNM' || 
      $config.country eq 'THA' }  
    
    <div class="header-text-tittle" >おすすめ{if $config.country eq "TWN"}
台湾{elseif $config.country eq "IDN"}
インドネシア{elseif $config.country eq "THA"}
タイ{elseif $config.country eq "VNM"}
ベトナム{elseif $config.country eq "KHM"}
カンボジア{elseif $config.country eq "MYS"}
マレーシア{elseif $config.country eq "SGP"}
シンガポール{elseif $config.country eq "HKG"}
香港{elseif $config.country eq "AUS"}
オーストラリア{elseif $config.country eq "NZL"}
ニュージーランド{/if}おみやげ</div>
    <div class="image-slider"  >
      <div class="slider-left2"></div>
      <div class="slider-mid2">
        <ul class="slider-content-index2">
          {$productslider2}
        </ul>
      </div>
      <div class="slider-right2"></div>
    </div>
    
    {/if}
    
    <!--banner footer-->
    <!--banner footer-->
    <!--news-->
    <div class="news">
      <div class="news_header"></div>
      <div class="news_middle"> {foreach from=$news item=items}
        <div class="news_box">
          <div class="news_icon"> {if $items.icon eq "1"} <img src="../images/icon_new.gif" alt="new" /> {/if} </div>
          <div class="news_date">{$items.date}</div>
          <div class="news_title"> <a href="news_detail.php?id={$items.id}">{$items.subject}</a> </div>
          <div class="clear"></div>
        </div>
        <div class="news_separate"></div>
        {/foreach} </div>
      <div class="news_bottom"></div>
    </div>
    <!--end news-->
    <!--special content-->
    <div class="special_content">
      <div class="special_content_top"></div>
      <div class="special_content_middle"> {foreach from=$sp_content item=content}
        <!--sp content-->
        <div class="special_content_box">
        
         {if $config.country eq "TWN"  &&   ($content.id  eq "101")}    
          <!--image-->
          <div class="image"><a  {literal}onclick="trackOutboundLink('branch_content_detail.php?id={/literal}{$content.id}{literal}'); return false;"{/literal} href="branch_content_detail.php?id={$content.id}"   ><img  alt="" src="{$content.image}" width="170"  /></a></div>
          <!--end image-->
          <!--content-->
          <div class="special_content_text">
            <h4 class="special_content_title"><a {literal}onclick="trackOutboundLink('branch_content_detail.php?id={/literal}{$content.id}{literal}'); return false;"{/literal} href="branch_content_detail.php?id={$content.id}">{$content.subject}</a></h4>
            <p class="special_content_description">{$content.short_content}</p>
          </div>
          <!--end content-->
          {else}
          <!--image-->
          <div class="image"><a href="branch_content_detail.php?id={$content.id}"><img  alt="" src="{$content.image}" width="170"  /></a></div>
          <!--end image-->
          <!--content-->
          <div class="special_content_text">
            <h4 class="special_content_title"><a href="branch_content_detail.php?id={$content.id}">{$content.subject}</a></h4>
            <p class="special_content_description">{$content.short_content}</p>
          </div>
          <!--end content-->          
          {/if}
          
          <div class="clear"></div>
          <div class="special_content_separate"></div>
        </div>
        <!--sp content-->
        {/foreach} </div>
        
      <div class="special_content_bottom"></div>
    </div>
    <!--end special content-->
    {if $config.country eq "THA" || $config.country eq "TWN" || $config.country eq "SGP" || $config.country eq "IDN" || $config.country eq "NZL" || $config.country eq "MYS" }
    <!--twitter-->
    <div class="twitter">
      <script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
      <script type="text/javascript" src="{$config.documentroot}common/js/twitter{$config.country}.js"></script>
    </div>
    <!--end twitter-->
    {/if}
    {if $config.openkeyword eq "true"}
    {if $config.country eq "TWN"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "IDN"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "THA"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "VNM"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "KHM"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "MYS"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "SGP"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "HKG"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "AUS"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "NZL"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {/if} 
    {/if}
    </div>
  <!--container-left-->
  
{if $config.country eq 'TWN'}
{literal}
<script type="text/javascript">
var trackOutboundLink = function(url) {
  ga('send', 'event', 'URL', 'click', url, {'hitCallback':
     function () {		 
     document.location = url;
     }
   });
}
</script>
{/literal}
{/if}
   
  <!--container-right-->
  {if $config.country eq 'TWN' ||
      $config.country eq 'SGP' || 
      $config.country eq 'NZL' || 
      $config.country eq 'MYS' || 
      $config.country eq 'IDN' ||
      $config.country eq 'AUS' || 
      $config.country eq 'VNM' || 
      $config.country eq 'THA' }  
  	{include file="country_container_top_right.tpl"}
  {else}
  	{include file="country_container_right.tpl"}
  {/if}
  <!--container-right-->
  
  
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>