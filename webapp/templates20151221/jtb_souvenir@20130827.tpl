{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider"> <img src="public/images/index.php?root=country&amp;width=967&amp;name=001.jpg" alt="" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=002.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=003.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=004.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=005.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=006.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=007.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=008.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=009.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=010.jpg" alt="" class="hide" /> </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> 
  	<ul class="bread-camp">    	
    	<li><a href="/">TOP</a><span>&gt;</span></li>
        <li>海外おみやげ</li>
    </ul>
    <div class="text-content">
      <h1 class="font_red">アジア・オセアニア各国の海外おみやげを販売</h1>  
     <p>JTBが厳選した海外のおみやげをオンラインで販売。お客様の素敵な旅の思い出や記念に、また大切な方に旅のおすそ分けにいかがでしょうか。<br/>
JTBだからこその品揃えと品質と共にお客様や大切な方のお手元まで自信を持ってお届けいたします。</p>
    </div>
    
    
   
   <div class="clear"></div>
   <div class="height10"></div>
   
   <!-- box search -->
        <form action="souvenir_search.php" method="get" name="frmSearch">
        	<div class="box-search-souvenir-header">Search</div>
			<div class="box-search-souvenir">
      	   
            
            <div class="inp-inputsearch">
            	<div class="inp-inputsearch-inp1"><p>国名</p> {$inp_country}</div>
                <div class="inp-inputsearch-inp2"><p>受取場所</p> {$inp_location} </div>
                <div class="inp-inputsearch-inp3"><p>カテゴリー</p>{$inp_theme}</div>
                
                
                <div class="clear"></div>
                
                <div class="inp-inputsearch-inp4"><p>フリーワード</p><input type="text" name="keyword" value=""  class="w210" /></div>
        
                
            </div>
                       
           <div class="box-search4">
             <input name="inp_search" class="btn_search" type="submit" value="検索" />
           </div>
           
      		</div>
        <!-- box search -->
   
   
    
    <!--Souveiner Photosnap1-->
    <div class="header-text-tittle">おすすめ海外おみやげ</div>
    <div class="image-slider">
      <div class="slider-left"></div>
      <div class="slider-mid">
        <ul class="slider-content-index">
          {$productslider1}
        </ul>
      </div>
      <div class="slider-right"></div>
    </div>
    <!--Souveiner Photosnap1-->
    
    
    
    {if $banner_name1 != "" || $banner_name2 != "" || $banner_name3 != ""}    
    <!--banner-navigation-->
    
    <!--Banner List-->
     <div class="clear"></div>
    <div class="height10"></div>
    
    <div class="banner-navigation">
      <div class="banner-navigation-main"> {if $banner_name1 != ""} <a href="{$banner_link1}" title="menu1" ><img src="{$banner_img1}" alt="menu1" height="168" width="767"  /></a> {/if}
        {if $banner_name2 != ""} <a href="{$banner_link2}" title="menu2" class="hide"><img src="{$banner_img2}" alt="menu2" height="168"  width="767" /></a> {/if}
        {if $banner_name3 != ""} <a href="{$banner_link3}" title="menu3" class="hide"><img src="{$banner_img3}" alt="menu3" height="168" width="767" /></a> {/if} </div>
      <ul class="banner-navigation-btn">
        {if $banner_name1 != ""}
        <li class="menu1 p-right3 active" id="banner-nav1">{$banner_name1}</li>
        {/if}
        {if $banner_name2 != ""}
        <li class="menu2 p-right3"  id="banner-nav2">{$banner_name2}</li>
        {/if}
        {if $banner_name3 != ""}
        <li class="menu3"  id="banner-nav3">{$banner_name3}</li>
        {/if}
      </ul>
    </div>
    <!--Banner List-->
    <div class="clear"></div> 
    <div class="height5"></div>  
     
    <!--banner-navigation-->
    {/if}
    
    
   
    
    <!--Souveiner Photosnap2-->
     <div class="header-text-tittle">お得海外おみやげ</div>
    <div class="image-slider" >
      <div class="slider-left2"></div>
      <div class="slider-mid2">
        <ul class="slider-content-index2">
          {$productslider2}
        </ul>
      </div>
      <div class="slider-right2"></div>
    </div>
    <!--Souveiner Photosnap2-->
    
        
  
    <!--banner footer-->
    </div>
  <!--container-left-->
  <!--container-right-->
  {include file="jtb_souvenir_container_right.tpl"}
  {$inc_flash}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>