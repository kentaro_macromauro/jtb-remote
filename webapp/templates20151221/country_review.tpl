{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  
  {include file="country_navigation_bar.tpl" }
  <div class="clear p-bottom8 "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left">
  	{$breadcamp}
      <div class="clear"></div>
    	
      <div class="header-text-tittle">{$review.product_name_jp}</div>
      
      <div class="text-content">
       <div class="height5"></div>
      	<p><strong>{$review.rev_tittle}</strong></p>
        <div class="height5"></div>
      	<p>{$review.rev_desc}</p>
      </div>
      <div class="clear"></div>
      <div class="height5"></div>
      
      <ul class="product_review_gallery">
      	{$review.gallery} 
      </ul>

      <div class="clear"></div>
   
      <!--banner footer-->
      <!--banner footer-->
    </div>
    <!--container-left-->
    <!--container-right-->
   {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>