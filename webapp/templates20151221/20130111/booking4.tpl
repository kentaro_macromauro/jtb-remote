{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        <ul class="booking-nav-process {if $config.allot != 'false'}limit4{/if}">
          
            <li id="step1">希望日選択</li>
            <li id="step2">カート</li>
            
            <li id="step3" class="active_last">予約情報入力</li>
            <li id="step4" class="active">予約情報確認</li>
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step5" class="active_first">お支払い方法の選択</li>
            {/if}
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step6" 	
            	{if $status_book == '1'}
            		class="active_first" 
                {/if}
            >決済情報確認</li>
            <li id="step7">決済情報確認</li>
            {/if}
            
            <li id="step8">完了</li>
          
        </ul>
        <div class="clear"></div>
        <span class="texttittle" >予約情報確認</span>
        <p>以下の情報でお間違いないでしょうか？</p>
        <div class="form3-content">
    		
          
            <!-- form Register-->
            <div class="height10"></div>
            			 
            <p>ツアー名：{$booking.product.name}
           
            </p>
            <p>日付：{$booking.date}
              
            </p>
            <div class="height10"></div>
            <p>■申込情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>申込者名（漢字） </th>
                <td> {$booking.his.reg.firstname} {$booking.his.reg.lastname}
                	
                </td>
              </tr>
              <tr>
                <th>申込者名（ローマ字）</th>
                <td>{$booking.his.reg.furi_firstname} {$booking.his.reg.furi_lastname}
                	
                </td>
              </tr>
              <tr>
                <th>生年月日 </th>
                <td>{$booking.his.reg.birth_year}年{$booking.his.reg.birth_month}月{$booking.his.reg.birth_day}日
                	
                </td>
              </tr>
              <tr>
                <th height="25">性別 </th>
                <td>{$booking.his.reg.sex}
                	
                </td>
              </tr>
              <tr>
                <th>メールアドレス（PC） </th>
                <td>{$booking.his.reg.email}
                	
                </td>
              </tr>
              <tr>
                <th>住所</th>
                <td> 
                  
            		
                  {if $booking.his.reg.type_addr eq 'true'}
                  		{$booking.his.reg.addrovs1} {$booking.his.reg.addrovs2}
     
                  {else} 
                  
                  <table cellspacing="0" >
                    <tr >
                      <td>郵便番号</td>
                      <td>〒{$booking.his.reg.addrjp_zip1}-{$booking.his.reg.addrjp_zip2}
                      	
                      </td>
                    </tr>
                    <tr>
                      <td>都道府県</td>
                      <td>{$booking.his.reg.addrjp_pref_name}
                    
                      </td>
                    </tr>
                    <tr>
                      <td>市区町村</td>
                      <td>{$booking.his.reg.addrjp_city}
                      	
                      </td>
                    </tr>
                    <tr>
                      <td>町域</td>
                      <td>{$booking.his.reg.addrjp_area}
                      	
                      </td>
                    </tr>
                    <tr>
                      <td>建物名<br/>
                        部屋番号</td>
                      <td>{$booking.his.reg.addrjp_building}
                      	
                      </td>
                    </tr>
                  </table>
                  {/if}
                  
                 </td>
              </tr>
              <tr>
                <th>連絡先（電話）</th>
                <td>{$booking.his.reg.tel}
                	
                </td>
              </tr>
              <tr>
                <th>連絡先（携帯）</th>
                <td>{$booking.his.reg.mobile}
                	
                </td>
              </tr>
              <tr>
                <th>住所（緊急連絡先）</th>
                <td>{$booking.his.reg.backup_addr1}
                	
                </td>
              </tr>
              <tr>
                <th>電話番号（緊急連絡先）</th>
                <td>{$booking.his.reg.backup_tel}
                	
                </td>
              </tr>
            </table>
            <div class="height10"></div>
            <p>■参加情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>宿泊ホテル</th>
                <td> {if $booking.his.reg.type_hotel eq '1'}
                  <div class=""> {$booking.his.reg.hotel_name}
                  	
                  </div>
                  {/if}
                  
                  {if $booking.his.reg.type_hotel eq '2'}
                  <div class="">
                    <table cellspacing="0">
                      <tr>
                        <td>滞在先名 </td>
                        <td>{$booking.his.reg.hotel_name}
                        	
                        </td>
                      </tr>
                      <tr>
                        <td>滞在先TEL</td>
                        <td>{$booking.his.reg.hotel_tel}
                        	
                        </td>
                      </tr>
                      <tr>
                        <td>滞在先住所</td>
                        <td>{$booking.his.reg.hotel_addr}
                        	
                        </td>
                      </tr>
                    </table>
                  </div>
                  {/if} </td>
              </tr>
              <tr>
                <th>宿泊期間</th>
                <td> {$booking.his.reg.arrfrm_year}年{$booking.his.reg.arrfrm_month}月{$booking.his.reg.arrfrm_day}日 ～ {$booking.his.reg.arrto_year}年{$booking.his.reg.arrto_month}月{$booking.his.reg.arrto_day}日    	
              </tr>
              <tr>
                <th>到着日フライト</th>
                <td>{$booking.his.reg.arrgo_year}年{$booking.his.reg.arrgo_month}月{$booking.his.reg.arrgo_day}日  
                {if ($booking.his.reg.arrgo_hh < 10)}0{$booking.his.reg.arrgo_hh}
                {else}{$booking.his.reg.arrgo_hh}{/if}:{if ($booking.his.reg.arrgo_mm < 10)}0{$booking.his.reg.arrgo_mm}
                {else}{$booking.his.reg.arrgo_mm}{/if}
                </td>
              </tr>
              <tr>
                <th>&nbsp;</th>
                <td>フライトナンバー {$booking.his.reg.air_no}</td>
                
              </tr>
              <tr>
                <th>ご旅行形態</th>
                <td>{$booking.his.reg.sel_info}
                	
                </td>
              </tr>
            </table>    
            <div class="height10"></div>
            <p>■参加者情報</p>
            {foreach from=$form.arr_guest item=rec_guest}
            
            {if $rec_guest != 0}
            <div class="heigth5"></div>
            {/if}
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>参加者名（ローマ字）</th>
                <td>{$booking.his.guest.$rec_guest.firstname} {$booking.his.guest.$rec_guest.lastname}</td>
              </tr>
              <tr>
                <th>参加者性別 </th>
                <td>{$booking.his.guest.$rec_guest.sex}</td>
              </tr>
              <tr>
                <th>参加者年齢 </th>
                <td>{$booking.his.guest.$rec_guest.age}歳</td>
              </tr>
              <tr>
                <th>参加者生年月日</th>
                <td>{$booking.his.guest.$rec_guest.birth_year}年{$booking.his.guest.$rec_guest.birth_month}月{$booking.his.guest.$rec_guest.birth_day}日 </td>
              </tr>
              <tr>
                <th>参加者パスポート番号 </th>
                <td>{$booking.his.guest.$rec_guest.sn}</td>
              </tr>
              <tr>
                <th>参加者パスポート<br/>
                  Expirity date</th>
                <td>{$booking.his.guest.$rec_guest.expire_year}年{$booking.his.guest.$rec_guest.expire_month}月{$booking.his.guest.$rec_guest.expire_day}日</td>
              </tr>
            </table>
            {/foreach}

            <div class="height10"></div>
            <p>■その他の情報</p>
            <table cellspacing="1" class="tbform">
              <tr>
                <th>&nbsp;</th>
                <td>{$booking.his.reg.remark_html}
                	
                </td>
              </tr>
            </table>
            
           
            
            
        {$booking.his.reg_hidden}	
            <!-- form Register-->

          <form action="booking_payment_cache.php?page=payment{if $transection != ""}&transection={$transection}{/if}" name="gotonext" method="post"></form>
          
          <span style="padding:5px 150px 5px 165px; display:block;">
          <input type="button" class="btn_submit gotoback" value="戻る" style="width:70px;" />
          <input type="button" class="btn_submit gotonext" value="次へ" style="width:70px;" />
          </span> </div>
      </div>
    </div>
    <form action="booking_infoinput.php" name="gotoback" method="post">
        {$booking.his.reg_hidden}    
    </form>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>