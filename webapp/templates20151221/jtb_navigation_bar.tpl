<!--navigation bar-->
<!--navigation-top-->
<ul class="navigation-top">
  <li id="navtop-menu1"><a href="index.php">ＴＯＰ</a></li>
  <li id="navtop-menu2" class="noarrow"><a href="opt.php">オプショナルツアー</a>
    <ul class="submenu hide">
      {foreach from=$config.city item=city}
      <li><a href="{$config.documentroot}{$config.countryname}/search.php?inp_type=OPT&amp;inp_city={$city.id}">{$city.name}</a> {/foreach}
    </ul>
  </li>
  <li id="navtop-menu7" class="noarrow" ><a href="souvenir.php">海外おみやげ</a></li>
  <li id="navtop-menu8" class="noarrow" ><a href="/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/" target="_blank">ホテル</a></li>
  <li id="navtop-menu4"><a href="#">キャンペーン･特集</a>
  	<ul class="submenu ">
   	 <li><a href="special_campaign.php">オプショナルツアー</a></li>
	 <li><a href="souvenir_special_campaign.php">海外おみやげ</a></li>
    </ul>
  </li>
  <li id="navtop-menu5"><a href="#">JTB各国支店</a>
    <ul class="submenu ">
      <li><a href="taiwan/">JTB台湾支店</a></li>
      <li><a href="indonesia/" >JTBバリ島/インドネシア</a></li>
      <li><a href="thailand/">JTBタイ支店</a></li>
      <li><a href="vietnam/">JTBベトナム支店</a></li>
      <li><a href="cambodia/">カンボジア（JTBベトナム支店）</a></li>
      <li><a href="malaysia/">JTBマレーシア支店</a></li>
      <li><a href="singapore/">JTBシンガポール支店</a></li>
      
      <li><a href="australia/">JTBオーストラリア支店</a></li>
      <li><a href="newzealand/">JTBニュージーランド支店</a></li>
    </ul>
  </li>
</ul>
<!--navigation-top-->
<div class="clear"></div>
<!--navigation-bottom-->
<ul class="navigation-bottom-toppage" >
  {foreach from=$jtb_nav item=nav}	
  	 
  
  <li id="navbottom-menu{$nav.id+1}" class="arrow1"><a href="{ if $nav.path != 'hongkong'}{$config.documentroot}{$nav.path}/{/if}">
  {if $nav.path eq 'indonesia'}バリ島/インドネシア
{else}{ if $nav.path != 'hongkong'}{$nav.name}{/if}{/if}</a>
    {if $nav.path != 'hongkong'}
    <ul class="submenu hide">
      <li><a href="{$config.documentroot}{$nav.path}/opt.php"> オプショナルツアー </a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}"  >
          {foreach from=$nav.city item=nav2}
          <li> <a href="{$config.documentroot}{$nav.path}/search_opt.php?inp_country={$nav.country_iso3}&amp;inp_city={$nav2.city_iso3}">
          	{$nav2.city_name}</a>
          </li>
          {/foreach}
        </ul>
      </li>
      {if $nav.path eq 'taiwan' || $nav.path eq 'singapore' || $nav.path eq 'newzealand' || $nav.path eq 'malaysia' || $nav.path eq 'indonesia' || $nav.path eq 'vietnam' || $nav.path eq 'australia' || $nav.path eq 'thailand'}
      <li >
         <a href="{$config.documentroot}{$nav.path}/souvenir.php" >海外おみやげ</a>
         <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}"> 
         	{if $nav.path eq 'singapore' || $nav.path eq 'newzealand' || $nav.path eq 'thailand' || $nav.path eq 'australia' || $nav.path eq 'taiwan'}
         	<li><a href="{$config.documentroot}{$nav.path}/souvenir_search.php?inp_country={$nav.country_iso3}&inp_location=1" >現地受取</a></li>
            {/if}
            
           {if $nav.path eq 'singapore' || $nav.path eq 'australia' || $nav.path eq 'taiwan'}
            <li><a href="{$config.documentroot}{$nav.path}/souvenir_search.php?inp_country={$nav.country_iso3}&inp_location=2" >日本受取</a></li>
            {/if}
            <li><a href="{$config.documentroot}{$nav.path}/spage.php?id=user_guide.html" >利用ガイド</a></li>
         </ul>
      </li>
      {/if}
      {if $nav.path eq 'taiwan'}
      <li>
      	<a href="/taiwan/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/TWN/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/taiwan/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/TPE/index.html" target="_blank" >台北</a></li>
            <li><a href="/taiwan/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/KHH/index.html" target="_blank" >高雄</a></li>
            <li><a href="/taiwan/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/HUN/index.html" target="_blank" >花連</a></li>
            <li><a href="/taiwan/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/TWN/index.html" target="_blank" >その他の地域</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'indonesia'}
      <li>
      	<a href="/indonesia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/INA/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/indonesia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/DPS/index.html" target="_blank" >バリ</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'thailand'}
      <li>
      	<a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/THA/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/BKK/index.html" target="_blank" >バンコク</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/CNX/index.html" target="_blank" >チェンマイ</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/CEI/index.html" target="_blank" >チェンライ</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/HKT/index.html" target="_blank" >プーケット</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/USM/index.html" target="_blank" >サムイ</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/KBV/index.html" target="_blank" >クラビ</a></li>
            <li><a href="/thailand/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/THA/index.html" target="_blank" >その他の地域</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'vietnam'}
      <li>
      	<a href="/vietnam/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/VIE/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/vietnam/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/SGN/index.html" target="_blank" >ホーチミン</a></li>
			<li><a href="/vietnam/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/HAN/index.html" target="_blank" >ハノイ</a></li>
			<li><a href="/vietnam/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/VIE/index.html"  target="_blank" >中部ベトナム・その他</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'cambodia'}
      <li>
      	<a href="/cambodia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/CAM/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/cambodia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/REP/index.html" target="_blank" >シェムリアップ</a></li>
            <li><a href="/cambodia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/CAM/index.html" target="_blank" >その他</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'malaysia'}
      <li>
      	<a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/MAS/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/KUL/index.html" target="_blank" >クアラルンプール</a></li>
            <li><a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/PEN/index.html" target="_blank" >ペナン</a></li>
            <li><a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/LGK/index.html" target="_blank" >ランカウイ</a></li>
            <li><a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/BKI/index.html" target="_blank" >コタキナバル</a></li>
            <li><a href="/malaysia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/MKZ/index.html" target="_blank" >マラッカ</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'australia'}
      <li>
      	<a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/AUS/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/CNS/index.html" target="_blank" >ケアンズ</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/OOL/index.html" target="_blank" >ゴールドコースト</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/SYD/index.html" target="_blank" >シドニー</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/MEL/index.html" target="_blank" >メルボルン</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/PER/index.html" target="_blank" >パース</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/AYQ/index.html" target="_blank" >エアーズロック</a></li>
            <li><a href="/australia/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/AUS/index.html" target="_blank" >その他の地域</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'singapore'}
      <li>
      	<a href="/singapore/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/SIN/index.html" target="_blank" >ホテル</a>
        <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
        	<li><a href="/singapore/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/SIN/index.html" target="_blank" >シンガポール</a></li>
			<li><a href="/singapore/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/SIN/index.html" target="_blank" >その他の地域</a></li>
        </ul>
      </li>
      {/if}
      {if $nav.path eq 'newzealand'}
      <li>
      	<a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/NZL/index.html" target="_blank" >ホテル</a>
         <ul class="{if $nav.id > 6}submenu2a{else}submenu2{/if}">
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/AKL/index.html" target="_blank" >オークランド</a></li>
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/ROT/index.html" target="_blank" >ロトルア</a></li>
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/CHC/index.html" target="_blank" >クライストチャーチ</a></li>
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/MON/index.html" target="_blank" >マウントクック</a></li>
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/ZQN/index.html" target="_blank" >クイーンズタウン</a></li>
			<li><a href="/newzealand/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/NZL/index.html" target="_blank" >その他の地域</a></li>
         </ul>
      </li>
      {/if}
    </ul>
    {/if}
   
    
    
  </li>
  
  {/foreach}
</ul>
<div class="clear"></div>
<!--navigation-bottom-->
<!--navigation bar-->