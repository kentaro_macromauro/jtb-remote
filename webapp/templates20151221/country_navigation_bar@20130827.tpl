<!--navigation bar-->
<!--navigation-top-->
<ul class="navigation-top">
  <li id="navtop-menu1"><a href="index.php">ＴＯＰ</a></li>
  <li id="navtop-menu2"><a href="opt.php">オプショナルツアー</a>
    <ul class="submenu hide">
      {foreach from=$config.city item=city}
      <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/search_opt.php?inp_country={$config.country}&amp;inp_city={$city.id}">{$city.name}</a>
      {/foreach}
    </ul>
  </li>
  
  {if $config.country eq 'TWN' ||
      $config.country eq 'SGP' || 
      $config.country eq 'NZL' || 
      $config.country eq 'MYS' || 
      $config.country eq 'IDN' ||
      $config.country eq 'AUS' || 
      $config.country eq 'VNM' ||
      $config.country eq 'THA'}  
  <li id="navtop-menu7" class="arrow"><a href="souvenir.php" >海外おみやげ</a>
  	<ul class="submenu ">
  		
        {if  $config.country eq 'SGP' || $config.country eq 'NZL' || $config.country eq 'THA' || $config.country eq 'AUS'}
        <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=1">現地受取</a></li>
        {/if}
        <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=2">日本受取</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/spage.php?id=user_guide.html">利用ガイド</a></li>
 	 </ul>
  </li>
  <li id="navtop-menu4"><a href="#">キャンペーン･特集</a>
  	<ul class="submenu ">
   	 <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/special_campaign.php">オプショナルツアー</a></li>
	 <li><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_special_campaign.php">海外おみやげ</a></li>
    </ul>
  </li>
  {else}
  <li id="navtop-menu4"  class="noarrow"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/special_campaign.php">キャンペーン･特集</a>
  {/if}
  <li id="navtop-menu5"><a href="#">JTB各国支店</a>
  	<ul class="submenu">   
        <li><a href="http://{$smarty.server.HTTP_HOST}/taiwan/">JTB台湾支店</a></li>  
        <li><a href="http://{$smarty.server.HTTP_HOST}/indonesia/">JTBバリ島/インドネシア</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/">JTBタイ支店</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/vietnam/">JTBベトナム支店</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/cambodia/">カンボジア（JTBベトナム支店）</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/">JTBマレーシア支店</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/singapore/">JTBシンガポール支店</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/hongkong/">マイバス香港</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/australia/">JTBオーストラリア支店</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/">JTBニュージーランド支店</a></li>
    </ul>
  </li>
</ul>
<!--navigation-top-->
<div class="clear"></div>
<!--navigation-bottom-->
{if $config.country eq "TWN"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/taiwan/page.php?id=info.html">台湾国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="#">台湾都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/taiwan/page.php?id=taipei.html">台北</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/taiwan/page.php?id=kaohsiung.html">高雄</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/taiwan/news.php">台湾最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/taiwan/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/taiwan/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "IDN"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/indonesia/page.php?id=info.html">インドネシア国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/indonesia/#">インドネシア都市情報</a><ul class="submenu hide"><li><a href="http://{$smarty.server.HTTP_HOST}/indonesia/page.php?id=bali.html">バリ</a></li><li><a href="http://{$smarty.server.HTTP_HOST}/indonesia/page.php?id=jakarta.html">ジャカルタ</a></li></ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/indonesia/news.php">バリ島最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/indonesia/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/indonesia/page.php?id=tourist_desk.html">現地マイバスデスク</a></li>
</ul>
{elseif $config.country eq "THA"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="page.php?id=info.html">タイ国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="#">タイ都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=bangkok.html">バンコク</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=phuket.html">プーケット</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=chiang_mai.html">チェンマイ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=samui.html">サムイ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=krabi.html">クラビ</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/thailand/news.php">タイ最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/thailand/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "VNM"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/vietnam/page.php?id=info.html">ベトナム国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/vietnam/#">ベトナム都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/vietnam/page.php?id=ho_chi_minh_city_saigon.html">ホーチミン</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/vietnam/page.php?id=hanoi.html">ハノイ</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/vietnam/news.php">ベトナム最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/vietnam/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/vietnam/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "KHM"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/cambodia/page.php?id=info.html">カンボジア国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/cambodia/#">カンボジア都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/cambodia/page.php?id=siem_reap.html">シェムリアップ</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/cambodia/news.php">カンボジア最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/cambodia/page.php?id=qa.html">Q&amp;A</a></li>
</ul>
{elseif $config.country eq "MYS"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=info.html">マレーシア国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/malaysia/#">マレーシア都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=kuala_lumpur.html">クアラルンプール</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=penang.html">ペナン</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=kota_kinabalu.html">コタキナバル</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=langkawi.html">ランカウイ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=malacca.html">マラッカ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=cameron_highlands.html">キャメロンハイランド</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=pangkor.html">パンコール</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=terengganu.html">トレンガヌ</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/malaysia/news.php">マレーシア最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/malaysia/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "SGP"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/singapore/page.php?id=info.html">シンガポール国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/singapore/#">シンガポール都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/singapore/page.php?id=singapore.html">シンガポール</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/singapore/news.php">シンガポール最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/singapore/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/singapore/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "HKG"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/hongkong/page.php?id=info.html">香港国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/hongkong/#">香港都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/hongkong/page.php?id=hongkong.html">香港</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/hongkong/page.php?id=macau.html">マカオ</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/hongkong/news.php">香港最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/hongkong/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/hongkong/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "AUS"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=info.html">オーストラリア国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/australia/#">オーストラリア都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=sydney.html">シドニー</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=gold_coast.html">ゴールドコースト</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=cairns.html">ケアンズ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=melbourne.html">メルボルン</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=perth.html">パース</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=ayersrock.html">エアーズロック</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=adelaide.html">アデレード</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=tasmania.html">タスマニア</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=brisbane.html">ブリスベン</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=darwin.html">ダーウィン</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=hamilton_island.html">ハミルトン島</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/australia/news.php">オーストラリア最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/australia/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{elseif $config.country eq "NZL"}
<ul class="navigation-bottom">
  <li id="navbottom-menu1"><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=info.html">ニュージーランド国情報</a></li>
  <li id="navbottom-menu2" class="arrow1"><a href="http://{$smarty.server.HTTP_HOST}/newzealand/#">ニュージーランド都市情報</a>
    <ul class="submenu hide">
      <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=auckland.html">オークランド</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=rotorua.html">ロトルア</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=christchurch.html">クライストチャーチ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=mount_cook.html">マウントクック</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=queenstown.html">クイーンズタウン</a></li>
    </ul>
  </li>
  <li id="navbottom-menu3"><a href="http://{$smarty.server.HTTP_HOST}/newzealand/news.php">ニュージーランド最新情報</a></li>
  <li id="navbottom-menu4"><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=qa.html">Q&amp;A</a></li>
  <li id="navbottom-menu5"><a href="http://{$smarty.server.HTTP_HOST}/newzealand/page.php?id=tourist_desk.html">現地トラベルデスク</a></li>
</ul>
{/if}
<div class="clear"></div>
<!--navigation-bottom-->
<!--navigation bar-->