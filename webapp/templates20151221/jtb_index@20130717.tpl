{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider"> <img src="public/images/index.php?root=country&amp;width=967&amp;name=001.jpg" alt="" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=002.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=003.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=004.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=005.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=006.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=007.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=008.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=009.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=010.jpg" alt="" class="hide" /> </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> 
  	<ul class="bread-camp"><li>TOP</li></ul>
    <div class="text-content">
      <h1 class="font_red">海外観光・オプショナルツアー「マイバス」、海外おみやげ販売　-アジア・オセアニア専門の旅行商品販売サイト</h1>
     
     <p>JTBが自信を持ってお届けする、アジア・オセアニア専門の旅行商品販売サイトです。
 現地オプショナルツアー「マイバス」・海外おみやげ販売を中心に商品をご提供しております。
 JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。
 お客様の「感動のそばに、いつも」寄添いたい。そんな気持ちで皆様のお越しを心よりお待ちしております。</p>
    </div>
    
    <!-- OPT Tour-->
    <div class="header-text-tittle">おすすめオプショナルツアー　MyBus</div>
    <div class="image-slider">
      <div class="slider-left"></div>
      <div class="slider-mid">
        <ul class="slider-content-index">
          {$productslider1}
        </ul>
      </div>
      <div class="slider-right"></div>
    </div>
    <!-- OPT Tour-->
    
    
    <!--Souveiner-->
     <div class="header-text-tittle">お得海外おみやげ</div>
    <div class="image-slider" >
      <div class="slider-left2"></div>
      <div class="slider-mid2">
        <ul class="slider-content-index2">
          {$productslider2}
        </ul>
      </div>
      <div class="slider-right2"></div>
    </div>
    <!--Souveiner-->
    
    
    
    <!--banner footer-->
    <!--OPT banner-->
    <div class="p-bottom8"><a href="{$banner_link1}"  ><img src="{$banner_img1}" alt="" width="767" height="168"  /></a></div>
    <!--<div class="p-bottom8"><a href="{$banner_link2}" title="banner2" ><img src="{$banner_img2}" alt="banner2" width="767" height="168"  /></a></div>-->
    
    <!--OPT banner-->
    
    <!--Souvenir banner-->
    <div class="p-bottom8"><a href="/taiwan/souvenir_campaign.php?id=4">
<img height="168" width="767" alt="季節限定！台湾フルーツ特集！" src="http://www.mybus-asia.com/taiwan/images/souvenir_banner/4-1.jpg" />
</a></div>
	<!--Souvenir banner-->

    
    
    
    <div class="p-bottom8"><a href="http://www.mybus.com.cn" target="_blank" ><img src="images/jtb_460_60.gif" alt="マイバス(日本語定期観光)" width="468" height="60" border="0" /></a>
    <p style="padding-top:2px;"><a href="http://www.mybus.com.cn" target="_blank">中国観光オプショナルツアー</a></p></div>
    
    <!--<div class="p-bottom8"><a href="{$banner_link3}" title="banner3" ><img src="{$banner_img3}" alt="banner3" width="767" height="168"  /></a></div>-->
    <!--banner footer-->
    </div>
  <!--container-left-->
  <!--container-right-->
  {include file="jtb_container_top_right.tpl"}
  {$inc_flash}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>