{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear  "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <h1 class="header-text-tittle">{$product.product_name_jp}</h1>
    <div class="product-top-image"> 
		{$product_top}    
     </div>
     
    <div class="product-button">
      <ul class="product-button-list">
      	{$product_temp_bottom}   
        
      </ul>
      <div class="clear"></div>
      <div class="product-btncat-box"> {$product.category} </div>
      
      {if $product.rev_status == 1}
      <div class="product-rew-box"> <a class="btn-review" href="review.php?product_id={$product.product_id}">ツアー体験記はこちら </a> </div>
      {/if}
      <div class="clear"></div>
    </div>
    
     {if $product.product_id eq 519 }
     	<div class="product-detail">
      <p>台湾高速鉄道の普通車指定席及び自由席が連続した３日間何度でも乗り放題。
        高鉄パスとパスポートを新幹線改札口で提示、お好きな時間にお好きな列車に乗車可能、便利なフリーパスです。 </p>
      <p><strong>ツアー料金</strong>： NT$2,400</p>
      <p><strong>最少催行人員</strong>：1名様</p>
      <p><strong>申込可能日</strong>： 制限なし</p>
      <p></p>
    </div>
    <div class="product-information">
      <div class="product-info">
        <p>■ご購入方法　本サイトやお電話、メールでのご予約は承っておりません。<br />
          台湾到着後、「マイバスデスク」で直接ご購入ください。<br />
          <br />
          ■販売種目　  台湾高速鉄路（台湾新幹線）大人３日間周遊券引換証（「高鐵周遊券兌憑證」）　<br />
          <br />
          ■販売対象　  中華民国以外のパスポートを所持する外国人旅客。<br />
          <br />
          ■販売金額　　ＮＴ＄２,４００（大人用１枚）　※子供用パスは販売しておりません。 <br />
          <br />
          ■お支払い方法　マイバスデスクにて台湾ドル現金またはクレジットカードでお支払ください。「高鉄パス引換証」をお渡しします。<br />
          <br />
          ■購入方法<br />
          マイバスデスクにて台湾ドル現金またはクレジットカードでお払いください。<br />
          「高鉄パス引換証」をお渡しします。 <br />
          <br />
          ■事前予約 事前のご予約は承っておりません。<br />
          「JTB台湾　マイバスデスク」へご来店、ご購入下さい。<br />
          ※５枚以上の大量購入の場合は、在庫確認のため事前に<a href="mailto:&#111;&#112;&#116;&#105;&#111;&#110;&#095;&#121;&#111;&#121;&#097;&#107;&#117;&#046;&#116;&#119;&#064;&#106;&#116;&#098;&#097;&#112;&#046;&#099;&#111;&#109;" target="_blank">option_yoyaku.tw@jtbap.com</a>　までお知らせ下さい。<br />
          <br />
          ■払い戻し方法：引換証の原本をお持ちの上、有効期限内で未使用に限り、10％の手数料で払い戻し可能です。<br />
          <br />
          ■高鉄パスまたは高鉄パス引換証の紛失、汚損、盗難等の場合の再発行、払い戻し手続きはできません。<br />
          <br />
          ■ご利用の流れ<br />
          １．マイバスデスクで引換証を購入 高鉄パス引換証ご購入時は、必ずパスポートをご提示下さい。 <br />
          マイバスデスクで引換証にお客様の氏名・国籍・パスポートナンバーを記載して、お渡しいたします。 <br />
          引換証の有効期限内に、駅窓口で高鉄パスにお引換ください。 <br />
          <br />
          ２．駅窓口で高鉄パスに引換え <br />
          台湾新幹線の乗車券販売窓口にて引換証とパスポートをご提示の上、ご利用開始日を指定してください。 <br />
          高鉄パスはご利用開始日から３日間自由席が乗り放題です。 <br />
          指定席を希望の場合はご利用日、ご乗車予定の新幹線を窓口で指定してください。 <br />
          <br />
          ３．いざ、台湾新幹線に乗車！ 新幹線にご乗車の際は必ず高鉄パスとパスポートを係員のいる改札口でご提示の上、専用改札口をご利用ください。 <br />
          パスポートの提示がない場合や、パスポートと高鉄パスの照合ができない場合はご利用いただけません。 <br />
          <br />
          【ご注意】 ご旅行中はパスポートの管理に十分ご注意ください 。<br />
          高鉄パス・引換証は「マイバスデスク」での現地販売のみとなります。 <br />
          <br />
        </p>
        <div class="product-table">
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            <tr>
              <th colspan="2" class="table-header">備考</th>
            </tr>
            <tr>
              <th>■設定除外日 </th>
              <td>なし（旧正月や国慶節等の繁忙期も利用可能。）※但し座席の確保を保証するものではありません。年末年始や台湾の祝祭日は大変混みあいますので、ご注意下さい。マイバスデスクでは指定席のご予約は承っておりません。 </td>
            </tr>
            <tr>
              <th>■払い戻し方法</th>
              <td>：引換証の原本をお持ちの上、有効期限内で未使用に限り、10％の手数料で払い戻し可能です。</td>
            </tr>
            <tr>
              <th>ご注意ください</th>
              <td>■高鉄パスまたは高鉄パス引換証の紛失、汚損、盗難等の場合の再発行、払い戻し手続きはできません。</td>
            </tr>
            <tr>
              <th>&nbsp;</th>
              <td>■マイバスデスクでは、65歳以上の方、身長150ｃｍ以下で12歳未満のお子様用の半額優待券の取り扱いはございません。<br/>
                駅窓口にて、身分証明をご提示の上ご自身でお求め下さい。</td>
            </tr>
            <tr>
              <th>【オンライン販売はありません】</th>
              <td>本サイトはご案内のみで、オンライン販売は承っておりません。<br/>
                パスポートをご持参の上でマイバスデスクまでおこしください。 <br/>
                <br/>
                <strong>台北デスク</strong> 所在地: 台北市中山北路二段56号1F（アンバサダーホテル向い）<br/>
                営業時間: 09：00～17：30（年中無休）<br/>
                電話番号: 886-（0）2-2521-7315<br/>
                <a href="http://www.mybus-asia.com/taiwan/page.php?id=tourist_desk.html"  target="_blank">http://www.mybus-asia.com/taiwan/page.php?id=tourist_desk.html</a></td>
            </tr>
          </table>
        </div>
        <div class="clear"></div>
        <p>【2013.9.2追記】<br />
          2013.10.8～新幹線料金が値上げになりますが、<br />
          周遊券は料金据え置きのまま。<br />
          ますますお得になります。<br />
          料金表（中国語）をご参照下さい。<br />
          <a href="http://www.thsrc.com.tw/download/20131008Fare_01.pdf" target="_blank">http://www.thsrc.com.tw/download/20131008Fare_01.pdf</a></p>
      </div>
      <div class="clear"></div>
    </div>
     {else}
    
    
    <div class="product-detail">
      <p>{$product.short_desc}</p>
      <p><strong>ツアー料金</strong>： {$product.price_min} {if $product.show_rate eq 1}( 目安：\{$product.price_yen} ){/if} </p>
      <p><strong>最少催行人員</strong>：{$product.pax_min}名様</p>
      <p><strong>申込可能日　（催行日　{$product.day_cutoff+1}日　前まで）</strong></p>
      
      
      <p>{$productdetail}</p>
      
     
    </div>
    <div class="product-information">
      <div class="product-info"> 
      	{if $product.template_id == 1}
        
        {if $product.text_top != ""}
        <p>{$product.text_top}</p>
        {/if}
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
          	<tr>
            <th colspan="2" class="table-header">スケジュール</th></tr>
            {$product.text_table}
          </table>
        </div>
        
        {if $product.text_bottom != ""}<p>{$product.text_bottom}</p>{/if}
        {/if}
        
        {if $product.template_id == 2}
        <div class="product-table"> <strong>スケジュール</strong>
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            {$product.text_table}
          </table>
        </div>
        {/if} 
      <div class="clear"></div>
      {if $product.template_id == 3}
      <div class="product_text">{$product.text_top}</div>
      
      
      <div class="height10"></div>
      <div class="clear"></div>
      {/if}
      
      {if $product.remark != ""}
       <p>{$product.remark}</p>
      {/if}
      
      {if $product.itiner != ""}
      
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            <tr>
              <th colspan="3" class="table-header">Itinerary</th>
            </tr>
            {$product.itiner}
          </table>
        </div>
      
    <!--product itiner-->
    {/if}
	
	
	{if $config.country eq 'TWN'}
<p>******************************************************************************************<br/>
ご予約・購入までの流れ<br/>
<br/>
My Busウェブサイトページからお好きな商品の検索<br/>
↓<br/>
商品を選択し、催行日/ご参加人数選択などの予約画面へ<br/>
↓<br/>
お客様情報入力<br/>
↓<br/>
受付/精算<br/>
*クレジットカード精算となります。（*現地精算商品は除く）<br/>
*ご予約いただいた商品のご提供が可能かどうかを確認させていただいてから精算の案内をさせていただきます。<br/>
その場合、受付完了のEメールを先に送付し、商品手配完了後、手配ご回答書および精算のご案内（Eメール）を送付します。<br/>
（商品のご提供の確認が必要のない商品は、受付と精算が同時に行われます。その場合、クレジットカード精算完了後、受付、精算完了のEメールが流れます。 ）<br/>
↓<br/>
購入完了<br/>
*お送りする手配ご回答書をご自身で印刷の上、現地にお持ちいただくことをお勧めいたします。<br/>
*****************************************************************************************</p>

	{/if}
	    </div>
          <div class="clear"></div>
        </div>
    
        {if $product.policy != ""}
        <!--product policy-->
        <div class="product-policy">
          <div class="product-info">
            <strong>Policy</strong>
              <div>{$product.policy}</div>
            
          </div>
          <div class="clear"></div>
        </div>
        <!--product policy-->
        {/if}
        <div class="height10"></div>
        <div class="height10"></div>
        <a href="https://{$smarty.server.HTTP_HOST}/{$config.countryname}/booking.php?product_id={$product.product_id}" class="product-btn-booking">Booking</a>
        <div class="height10"></div>
    {/if}
    <!--banner footer-->
    <!--banner footer-->
     
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>