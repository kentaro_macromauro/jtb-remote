{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider"> <img src="/public/images/country/001.jpg" width="967" alt="" />
    <img src="/public/images/country/002.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/003.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/004.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/005.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/006.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/007.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/008.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/009.jpg" width="967" alt=""  class="hide" />
    <img src="/public/images/country/010.jpg" width="967" alt="" class="hide" /> </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <div class="text-content">
      <h1 class="font_red">海外観光・オプショナルツアー「マイバス」</h1>
      <p>JTBの現地オプショナルツアーブランドである「マイバス」のアジア・オセアニア専門販売サイトです。<br/>
JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。<br/>
お客様の「感動のそばに、いつも」寄添いたい。そんな気持ちで皆様のお越しを心よりお待ちしております。</p>
    </div>
    <form action="search_opt.php">
      <div class="serach-menu-left">
      	
        <div class="serach-menu-left-tittle">検索</div>
        <div class="height5"></div>
        <div class="search-menu-subject1">目的地</div>
        {$inp_country}
        <div class="height5"></div>
        <div class="height5"></div>
        <span id="select_city">
        <select class="search-left-selectbox " name="inp_city">
          <option value="0">-------Please Select-------</option>
        </select>
        </span>
        <div class="height5"></div>
        
        <div class="search-menu-subject1" style="display:none;">日にち </div>
        <div class="search-center"  style="display:none;"> {$select_yearmonth}
          {$select_day} </div>
        <div class="p_ddpicker" style="padding-left:12px; margin-top:5px; display:none;""> <span id="id_cal">カレンダーから選ぶ</span> </div>
        <div class="height5"></div>
        <p>テーマ</p>
        <select class="search-left-selectbox" name="inp_category">
          <option value="0">---Please Select---</option>
          
        {$select_category}
      
        </select>
        <div class="height5"></div>
        <div class="height5"></div>
        <p>条件検索</p>
        <select class="search-left-selectbox" name="inp_option">
          <option value="0">---Please Select---</option>
          
        {$select_option}
      
        </select>
        <div class="height5"></div>
        <div class="height5"></div>
        <p>時間帯</p>
        <select class="search-left-selectbox" name="inp_time">
          <option value="0">---Please Select---</option>
          
        {$select_time}
      
        </select>
        <div class="height5"></div>
        <div class="height5"></div>
        <div class="search-menu-subject1">フリーワード検索</div>
        <input type="text" class="search_left_inp" name="inp_keyword" value="" />
        <div class="height10"></div>
        <div class="height5"></div>
        <input type="submit" value="検索" class="search-left-button" />
        <div class="height5"></div>
        <div class="search-bg-footer"></div>
      </div>
    </form>
    <div class="container-wsearch-left">
      <div class="container-wsearch-tittle">おすすめツアー</div>
      <div class="content">
        <div class="slider-box-top"></div>
        <div class="slider-content">
          <ul class="slider-content-mid">
            {$productslider}
          </ul>
        </div>
        <div class="slider-box-bottom"></div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="height10"></div>
    <!--banner-navigation-->
    <div class="banner-navigation">
   	<div class="banner-navigation-main">
    	{if $banner_name1 != ""}
    	<a href="{$banner_link1}" title="menu1" ><img src="{$banner_img1}" alt="menu1" height="168" width="767"  /></a>
        {/if}
        {if $banner_name2 != ""}
        <a href="{$banner_link2}" title="menu2" class="hide"><img src="{$banner_img2}" alt="menu2" height="168"  width="767" /></a>
        {/if}
        {if $banner_name3 != ""}
        <a href="{$banner_link3}" title="menu3" class="hide"><img src="{$banner_img3}" alt="menu3" height="168" width="767" /></a>
        {/if} 
    </div>
   	<ul class="banner-navigation-btn">
      {if $banner_name1 != ""}
   	  <li class="menu1 p-right3 active" id="banner-nav1">{$banner_name1}</li>
      {/if}
      {if $banner_name2 != ""}
      <li class="menu2 p-right3"  id="banner-nav2">{$banner_name2}</li>
      {/if}
      {if $banner_name3 != ""}
      <li class="menu3"  id="banner-nav3">{$banner_name3}</li>
      {/if}
    </ul>
    </div>
    <div class="clear"></div>
    <!--banner-navigation-->
    <div class="height10"></div>
    <div class="border-b4"></div>
    <div class="height10"></div>
    <div class="height5"></div>
    <!--category-->
    <div class="category">
      <div class="category_box">
        <div class="category_tittle"> 観る・見る </div>
        <div class="category_image"> <img src="{$catimg1}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg1}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category1}
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category[]=001&inp_category[]=002" >「観る・見る」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> お得！</div>
        <div class="category_image"> <img src="{$catimg2}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg2}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category2}
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_option=4" >「お得！」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> 遊ぶ </div>
        <div class="category_image"> <img src="{$catimg3}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg3}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category3}
          
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category[]=006&inp_category[]=009&inp_category[]=0018" >「遊ぶ」検索結果一覧へ</a></div>
      </div>
    </div>
    <!--category-->
    <!--category-->
    <div class="category">
      <div class="category_box">
        <div class="category_tittle"> 癒される </div>
        <div class="category_image"> <img src="{$catimg4}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg4}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category4} 
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category=007" >「 癒される」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> 食べる </div>
        <div class="category_image"> <img src="{$catimg5}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg5}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category5}
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category[]=008&inp_category[]=0017" >「 食べる・飲む 」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> 文化体験 </div>
        <div class="category_image"> <img src="{$catimg6}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg6}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category6}

        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category=013" >「文化体験」検索結果一覧へ</a></div>
      </div>
    </div>
    <!--category-->
    <!--category-->
    <div class="category">
      <div class="category_box">
        <div class="category_tittle"> アクティブ・スポーツ </div>
        <div class="category_image"> <img src="{$catimg7}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg7}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category7}
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category=003" >「アクティブ・スポーツ」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> 自然・動物</div>
        <div class="category_image"> <img src="{$catimg8}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg8}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category8}
        </ul>
        <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category[]=010&inp_category[]=011" >「自然・動物」検索結果一覧へ</a></div>
      </div>
      <div class="category_box">
        <div class="category_tittle"> 歴史・世界遺産 </div>
        <div class="category_image"> <img src="{$catimg9}" alt="" width="230" height="94" class="org_img" /> <img src="{$catimg9}" alt="" width="230" height="94" class="hide now" /></div>
        <ul class="category_text">
          {$category9}
        </ul>
         <div class="category_link2"><a href="search_opt.php?inp_country=&inp_category=012" >「歴史・世界遺産」検索結果一覧へ</a></div>
      </div>
    </div>
    <!--category-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="jtb_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>