<div class="bg_footer">
  <!--FOOTER CONTAINER-->
  <div class="footer_container">
    <div class="footer_link">
     <ul>
        <li><a href="http://{$smarty.server.HTTP_HOST}/page.php?id=branch.html">会社概要</a></li>

        <li><a href="http://{$smarty.server.HTTP_HOST}/page.php?id=term.html">旅行約款</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/page.php?id=privacy.html">個人情報保護方針</a></li>
      </ul>
    </div>
    <div class="copyrights">Copyright &copy; 2013 My Bus. All rights reserved </div>
    <div class="clear"></div>
  </div>
  <!--END FOOTER CONTAINER-->
</div>

{literal} 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32237848-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}
