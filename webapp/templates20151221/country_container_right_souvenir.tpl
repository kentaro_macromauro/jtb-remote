{if $config.country eq "TWN"}
<div class="container-right">
	<div class="container-right-tittle">
		<p class=" souvenir_tour_ranking_head">海外おみやげ</p>
	</div>
    
    
    
	
	<ul class="container-right-list">
    
     <li class="ptop10">
			<div class="right-list-product-subject icontop1"> <a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=870"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=870-1.jpg" alt="" class="right-list-product-img" width="90"   /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=870">台湾限定　小籠包キューピー</a></div>
			<div class="right-list-product-txt">
				<p >みんなが大好きな“小籠包”がキューピーさんになって台湾に登場！ 「アイラブ台湾」の文字を蒸篭に添えて、「台湾推し」の皆様にお届け。マイバスデスク限定販売商品です。</p>
				<span class="txt-red-bold-price">NT$250</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
      
    	
        <li>
			<div class="right-list-product-subject icontop2"> <a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=959"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=959-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=959">【日本郵送専用】「台南 廣富號帆布包」X「JTB台湾」オリジナルトートバッグ</a></div>
			<div class="right-list-product-txt">
				<p>台湾で大人気のバッグブランドと夢のコラボがついに実現<br/> </p>
				<span class="txt-red-bold-price">NT$1,000</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
        
        
        <li>
			<div class="right-list-product-subject icontop3"> <a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=929"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=929-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://www.mybus-asia.com/taiwan/souvenir_product.php?souvenir_id=929">【新登場！】パイナップルケーキのドリームチーム「わがままプリンセス」</a></div>
			<div class="right-list-product-txt">
				<p>
                あれも食べたい、これも食べたい！
そんな願いを叶えるパイナップルケーキの詰め合わせ
「わがままプリンセス」がJTB台湾マイバスに誕生しました！
                </p>
				<span class="txt-red-bold-price">NT$250</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
        
		<li>
			<div class="right-list-product-subject icontop4"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=909"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=909-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=909">ミニチュア天燈（LED付き・アクリルケース入り）</a></div>
			<div class="right-list-product-txt">
				<p>願いを書いて、空高く飛ばす台湾の名物「天燈（ランタン）」のミニチュアが登場。交通安全や健康祈願の言葉入りなので、お守りとして大切な方やご自分へ、お土産にどうぞ。</p>
				<span class="txt-red-bold-price">NT$200</span> </div>
			<div class="clear"></div>
			<div class="right-list-rank-border"></div>
		</li>
		<li >
			<div class="right-list-product-subject icontop5"> <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=950"><img src="../product/images/index.php?root=souvenir&amp;width=112&amp;name=950-1.jpg" alt="" class="right-list-product-img" width="90"  /></a> </div>
			<div class="clear"></div>
			<div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_product.php?souvenir_id=950">【漢方系美容クリーム】護膚霜　（Hu Fu Cream)　50ｇ</a></div>
			<div class="right-list-product-txt">
				<p>「台湾へ行くなら買ってきて！」
口コミで評判の漢方系美容クリーム「護膚霜（フーフークリーム）」
足マッサージや漢方の老舗・滋和堂の人気アイテムがマイバスに登場！</p>
				<span class="txt-red-bold-price">NT$1,000</span> </div>
			<div class="clear"></div>
		</li>
     
     
    </ul>
	
	<div class="ptop10"></div>
	<a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank"><img src="/images/btn_opt_taiwan.jpg" alt="" /></a> <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank">日本発の台湾ツアーをお探しのお客様は</a>
	<div class="ptop10"></div>
	<a href="https://www.facebook.com/pages/JTB%E5%8F%B0%E6%B9%BE/258090714317394" target="_blank"><img src="/images/findusonfacebook.jpg" alt="" /></a>

	<!-- <div class="ptop10"></div>
    <a  onclick="_gaq.push(['_trackEvent', 'docomo_banner_in_tw', 'click']);" href="http://id-credit.com/idpp14first/index.html"  target="_blank"><img src="/images/btn_idpaypasscp.jpg" alt="" /></a>-->


<div style="height:10px;"></div>
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html"><img src="/images/btn_opttwn_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html">オプショナルツアーオンラインカタログ</a>


	<div class="ptop10"></div>
	<a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a> </div>
<div class="clear"></div>

{else}







<div class="container-right">
	
     {if $config.country eq "SGP"}
     <div class="ptop10"></div>
    <a href="/singapore/cpn_osechi2015/" ><img src="/images/osechi_web_banner2.jpg" alt="" /></a>
    <div class="ptop10"></div>
    {/if}


    <div class="container-right-tittle">
      <p class=" souvenir_tour_ranking_head">海外おみやげ</p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_1}"><img src="{$opt_top5_pic_1}" alt="" class="right-list-product-img" width="90"   /></a>      
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_1}">{$opt_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_1}</p>
          {$opt_top5_price_1}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_2}"><img src="{$opt_top5_pic_2}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_2}">{$opt_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_2}</p>
          {$opt_top5_price_2}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_3}"><img src="{$opt_top5_pic_3}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_3}">{$opt_top5_name_3}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_3}</p>
          {$opt_top5_price_3}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop4">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_4}"><img src="{$opt_top5_pic_4}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_4}">{$opt_top5_name_4}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_4}</p>
          {$opt_top5_price_4}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li > 
      	<div class="right-list-product-subject icontop5">
        <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_5}"><img src="{$opt_top5_pic_5}" alt="" class="right-list-product-img" width="90"  /></a>
        </div>
        <div class="clear"></div>
        <div class="right-list-product-tittle"><a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/{$opt_top5_link_5}">{$opt_top5_name_5}</a></div>
        <div class="right-list-product-txt">
          <p>{$opt_top5_detail_5}</p>
          {$opt_top5_price_5}
        </div>
        <div class="clear"></div>
        
      </li>
    </ul>
    
    {if $config.country eq "NZL"}
    <div class="ptop10"></div>
    <a href="http://www.jtb.co.nz/souvenir/"  target="_blank"><img src="/images/btn_souvenir_link.gif" alt="souvenir_banner" /></a>  
    <a href="http://www.jtb.co.nz/souvenir/"  target="_blank" class="text_link">更に日本へ発送できる商品をもっと見たいお客様は、こちら。</a>



    {/if}
    
    <div class="ptop10"></div>
     {if $config.country eq "TWN"}
    <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank"><img src="/images/btn_opt_taiwan.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/taiwan/index.asp" target="_blank">日本発の台湾ツアーをお探しのお客様は</a>
    
    <div class="ptop10"></div>
    <a href="https://www.facebook.com/pages/JTB%E5%8F%B0%E6%B9%BE/258090714317394" target="_blank"><img src="/images/findusonfacebook.jpg" alt="" /></a>
    	
      <div class="ptop10"></div>
    <a href=" http://id-credit.com/idpp14first/

" target="_blank"><img src="/images/btn_idpaypasscp.jpg" alt="" /></a>
   


	<div class="ptop10"></div>
	 <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" ><img src="/images/btn_opttwn_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" >オプショナルツアーオンラインカタログ</a>
	
	
	
	
	
    
    {elseif $config.country eq "IDN"}
    <a href="http://www.jtb.co.jp/kaigai/asia_resort/index.asp" target="_blank"><img src="/images/btn_opt_indonesia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia_resort/index.asp" target="_blank">日本発のバリ島ツアーをお探しのお客様は</a>
  
  
    
    
    <div class="ptop10"></div>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" ><img src="/images/btn_optidn_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" >オプショナルツアーオンラインカタログ</a>
    
    
    
    
   <div class="ptop10"></div>
    <a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a>
  
    {elseif $config.country eq "THA"}
    <a href="http://www.jtb.co.jp/kaigai/asia/thailand/index.asp" target="_blank"><img src="/images/btn_opt_thailand.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/thailand/index.asp" target="_blank">日本発のタイツアーをお探しのお客様は</a>
    

      <div class="ptop10"></div>
   <!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" ><img src="/images/btn_opttha_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->
   
    

    {elseif $config.country eq "VNM"}
    <a href="http://www.jtb.co.jp/kaigai/asia/vietnam/index.asp" target="_blank"><img src="/images/btn_opt_vietnam.jpg" alt="" /></a>	
    <a href="http://www.jtb.co.jp/kaigai/asia/vietnam/index.asp" target="_blank">日本発のベトナムツアーをお探しのお客様は</a>
    
	
    
	   <div class="ptop10"></div>
	<!--online catalog-->
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" ><img src="/images/btn_optvet_brochure2.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog2.html" >オプショナルツアーオンラインカタログ</a>
	<!--online catalog-->
    
    
    
    
	
    {elseif $config.country eq "KHM"}
    <a href="http://www.jtb.co.jp/kaigai/asia/cambodia/index.asp" target="_blank"><img src="/images/btn_opt_cambodia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/cambodia/index.asp" target="_blank">日本発のカンボジアツアーをお探しのお客様は</a>
   

   
   
    {elseif $config.country eq "MYS"}
    <a href="http://www.jtb.co.jp/kaigai/asia/malaysia/index.asp" target="_blank"><img src="/images/btn_opt_malayasia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/asia/malaysia/index.asp" target="_blank">日本発のマレーシアツアーをお探しのお客様は</a>
    

	

    {elseif $config.country eq "SGP"}
    <a href="http://www.jtb.co.jp/kaigai/asia/singapore/index.asp" target="_blank"><img src="/images/btn_opt_singapore.jpg" alt="" /></a>
  	<a href="http://www.jtb.co.jp/kaigai/asia/singapore/index.asp" target="_blank">日本発のシンガポールツアーをお探しのお客様は</a>
    
	 	
    
    <div style="height:10px;"></div>
	<a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html"><img src="/images/btn_optsga_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html">オプショナルツアーオンラインカタログ</a>
	
    
    
    
    
    {elseif $config.country eq "HKG"}
   
    {elseif $config.country eq "AUS"}
    <a href="http://www.jtb.co.jp/kaigai/oceania/australia/index.asp" target="_blank"><img src="/images/btn_opt_australia.jpg" alt="" /></a>
    <a href="http://www.jtb.co.jp/kaigai/oceania/australia/index.asp" target="_blank">日本発のオーストラリアツアーをお探しのお客様は</a>

	



    {elseif $config.country eq "NZL"}    
    <a href="http://www.jtb.co.jp/kaigai/oceania/newzealand/index.asp" target="_blank"><img src="/images/btn_opt_newzealand.jpg" alt="" /></a>
	<a href="http://www.jtb.co.jp/kaigai/oceania/newzealand/index.asp" target="_blank">日本発のニュージーランドツアーをお探しのお客様は</a>
    
    
    <div class="ptop10"></div>
    <a href="http://blog.jtb.co.nz/" target="_blank"><img src="/images/btn_nzl_blog.jpg" alt="" /></a>
	  <div class="ptop10"></div>
    <!--online catalog-->
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html"><img src="/images/btn_optnzl_brochure.jpg" alt="" /></a>
    <a href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/page.php?id=online_catalog.html">オプショナルツアーオンラインカタログ</a>
    <!--online catalog-->
	
    
    
    {/if}
    {if $config.country <> "IDN"}
    <div class="ptop10"></div>
    <a href="http://www.jtb-pst.com" target="_blank"><img src="/images/banner_pst_180.gif" alt="" /></a>
    {/if}
    
    
  </div>
  <div class="clear"></div>





{/if}