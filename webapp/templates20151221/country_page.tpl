{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->

  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
    {$breadcamp}
    {$data_tempage} 
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
{if $config.country eq 'TWN'}
{literal}
<script type="text/javascript">
var trackOutboundLink = function(url) {
   ga('send', 'event', 'URL', 'click', url, {'hitCallback':
     function () {		 
     document.location = url;
     }
   });
}
</script>
{/literal}
{/if}
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>