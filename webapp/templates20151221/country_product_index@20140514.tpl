{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear  "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <h1 class="header-text-tittle">{$product.product_name_jp}</h1>
    <div class="product-top-image"> 
		{$product_top}    
     </div>
     
    <div class="product-button">
      <ul class="product-button-list">
      	{$product_temp_bottom}   
        
      </ul>
      <div class="clear"></div>
      <div class="product-btncat-box"> {$product.category} </div>
      
      {if $product.rev_status == 1}
      <div class="product-rew-box"> <a class="btn-review" href="review.php?product_id={$product.product_id}">ツアー体験記はこちら </a> </div>
      {/if}
      <div class="clear"></div>
    </div>
    
    
    <div class="product-detail">
      <p>{$product.short_desc}</p>
      <p><strong>ツアー料金</strong>： {$product.price_min} {if $product.show_rate eq 1}( 目安：\{$product.price_yen} ){/if} </p>
      <p><strong>最少催行人員</strong>：{$product.pax_min}名様</p>
      <p><strong>申込可能日　（催行日　{$product.day_cutoff+1}日　前まで）</strong></p>
      
      
      <p>{$productdetail}</p>
      
     
    </div>
    <div class="product-information">
      <div class="product-info"> 
      	{if $product.template_id == 1}
        
        {if $product.text_top != ""}
        <p>{$product.text_top}</p>
        {/if}
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
          	<tr>
            <th colspan="2" class="table-header">スケジュール</th></tr>
            {$product.text_table}
          </table>
        </div>
        
        {if $product.text_bottom != ""}<p>{$product.text_bottom}</p>{/if}
        {/if}
        
        {if $product.template_id == 2}
        <div class="product-table"> <strong>スケジュール</strong>
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            {$product.text_table}
          </table>
        </div>
        {/if} 
      <div class="clear"></div>
      {if $product.template_id == 3}
      <div class="product_text">{$product.text_top}</div>
      
      
      <div class="height10"></div>
      <div class="clear"></div>
      {/if}
      
      {if $product.remark != ""}
       <p>{$product.remark}</p>
      {/if}
      
      {if $product.itiner != ""}
      
        <div class="product-table"> 
          <table border="0" cellpadding="0" cellspacing="1" class="product-time-tb">
            <tr>
              <th colspan="3" class="table-header">Itinerary</th>
            </tr>
            {$product.itiner}
          </table>
        </div>
      
    <!--product itiner-->
    {/if}
	
	
	{if $config.country eq 'TWN'}
<p>******************************************************************************************<br/>
ご予約・購入までの流れ<br/>
<br/>
My Busウェブサイトページからお好きな商品の検索<br/>
↓<br/>
商品を選択し、催行日/ご参加人数選択などの予約画面へ<br/>
↓<br/>
お客様情報入力<br/>
↓<br/>
受付/精算<br/>
*クレジットカード精算となります。（*現地精算商品は除く）<br/>
*ご予約いただいた商品のご提供が可能かどうかを確認させていただいてから精算の案内をさせていただきます。<br/>
その場合、受付完了のEメールを先に送付し、商品手配完了後、手配ご回答書および精算のご案内（Eメール）を送付します。<br/>
（商品のご提供の確認が必要のない商品は、受付と精算が同時に行われます。その場合、クレジットカード精算完了後、受付、精算完了のEメールが流れます。 ）<br/>
↓<br/>
購入完了<br/>
*お送りする手配ご回答書をご自身で印刷の上、現地にお持ちいただくことをお勧めいたします。<br/>
*****************************************************************************************</p>

	{/if}
	
	
    
    </div>
      <div class="clear"></div>
    </div>
    
    {if $product.policy != ""}
    <!--product policy-->
    <div class="product-policy">
      <div class="product-info">
        <strong>Policy</strong>
          <div>{$product.policy}</div>
        
      </div>
      <div class="clear"></div>
    </div>
    <!--product policy-->
    {/if}
	
	
    
    <div class="height10"></div>
   
    <div class="height10"></div>
    <a href="https://{$smarty.server.HTTP_HOST}/{$config.countryname}/booking.php?product_id={$product.product_id}" class="product-btn-booking">Booking</a>
    <div class="height10"></div>
    
    <!--banner footer-->
    <!--banner footer-->
     
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>