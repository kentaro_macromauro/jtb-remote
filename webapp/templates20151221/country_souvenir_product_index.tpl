{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear  "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <h1 class="header-text-tittle">{$product.souvenir_name}</h1>
    <div class="souvenir_product_image"> 
		
        <div class="souvenir_product_main">
        	<div class="souvenir_product_show">
            	{$souvenir_product_img1}
            </div>
            
            <ul class="souvenir_product_list">
            	{$souvenir_product_image_list}
            </ul>        
        </div>      
        
     	<div class="clear"></div>
     	<div class="souvenir_theme">{$souvenir_product_theme}</div>
     </div>
         
    
    <div class="product-information">
      <div class="souvenir-info"> 
      	<div class="height10"></div>
        
        {if $product.text_top != ""}
        <p>{$product.text_top}</p>
        {/if}
        
        
         {if $product.souvenir_price != ""}
        <p class="souvenir_price"><strong>価格：{$product.souvenir_show_price}</strong></p>
      	{/if}
        
        {if $product.affiliate}
         <p style="color:#C41010;">※この商品はJTB商事が運営する「世界のおみやげ屋さん」からご購入いただきます。購入ボタンをクリックすると「世界の<br/>
おみやげ屋さん」に接続されます。</p> 
		{/if}
        
        
        {if $product.text_table != ""}
        
          <table border="0" cellpadding="0" cellspacing="0" class="product-souvenir-tb">
          	
           { $product.text_table}
            
          </table>
        
        {/if}
        
        {if $product.text_bottom != ""}<p>{$product.text_bottom}</p>{/if}
       
        
      <div class="clear"></div>
      {if $product.template_id == 3}
      <div class="product_text">{$product.text_top}</div>
      
      <div class="height10"></div>
      <div class="clear"></div>
      
      
      {/if}
      
      {if $product.remark != ""}
       <p>{$product.remark}</p>
      {/if}
      
     
	 {if $config.country eq 'TWN'}
	 <p>
	 ******************************************************************************************<br/>
ご購入までの流れ<br/>
<br/>
My Busウェブサイトページからお好きな商品の検索<br/>
↓<br/>
商品を選択し、購買数の選択などのショッピングカート画面へ<br/>
↓<br/>
お客様情報入力<br/>
↓<br/>
受付/精算<br/>
*クレジットカード精算となります。（*現地精算商品は除く）<br/>
*クレジットカード精算完了後、受付、精算完了のEメールが流れます。 <br/>
*受付と、精算が同時に行われない商品もございます。<br/>
（ご予約いただいた商品のご提供が可能かどうかを確認させていただいてから精算の案内をさせていただく場合がご<br/>
ざいます。その場合、受付完了のEメールを先に送付し、商品手配完了後、精算のご案内（Eメール）を送付します。）<br/>
↓<br/>
商品のご発送/お受取り<br/>
***************************************************************************************** </p>
	 {/if}
    
    </div>
      <div class="clear"></div>
    </div>
    
   
    
    <div class="height10"></div>
   
    <div class="height10"></div>
     {if $status_soldout == '1'}
     <div style="height:10px; line-height:10px;"></div>
    <p style="font-size:18px; font-weight:bold; color:#BF0000; text-align:center;">ご好評につきご予約受付終了となりました。</p>
    {else}
    <a href="{$product.submiturl}" {if $product.affiliate}target="_blank"{/if} class="souvenir-btn-booking">Booking</a>
    {/if}
    
    <div class="height10"></div>
    
    <!--banner footer-->
    <!--banner footer-->
     
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
{literal}
<script type="text/javascript"> $(document).ready( function(){$('.souvenir_product_list li').click( function(){ _src = $(this).find('img').attr('src');$('.souvenir_product_show').find('img').fadeOut( function(){ $(this).attr('src',_src ); } ).fadeIn();} ); } ); </script>
{/literal}
</body></html>