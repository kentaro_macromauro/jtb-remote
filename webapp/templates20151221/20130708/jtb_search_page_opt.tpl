{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
    <ul class="bread-camp">
      <li><a href="index.php">TOP</a><span>&gt;</span></li>
      <li>検索</li>
    </ul>
    <div class="clear"></div>
    <!-- box search -->
    <form action="search_opt.php" method="get" name="frmSearch">
      <div class="box-search-header">検索</div>
      <div class="box-search">
        <ul class="box-search1" >
          <li>
            <p>国名</p>
            {$inp_country} </li>
          <li>
            <p>都市名</p>
            <span id="select_city"> {$inp_city} </span> </li>
          <li>&nbsp;</li>
        </ul>
        <ul class="box-search2" >
          
           <li>
            <p>テーマ指定</p>
            {$select_category} </li>
          
          <li>
            <p>条件検索</p>
            <span id="select_option"> {$select_option} </span></li>
          
          <li>
            <p>キーワード</p>
            <span style="float:left;">{$inp_keyword}</span></li>
 
        </ul>
        <ul class="box-search3">
          <li>
          	<p>体験記</p>
            {$select_review}
          </li>
          <li>
            <p>時間帯</p>
            {$select_time}
          <li>
            <p class="p_widthlimit">日にち</p>
            {$select_yearmonth}
            {$select_day} </li>
        </ul>
        <div class="box-search4">
          <input name="inp_search" class="btn_search" type="submit" value="検索" />
        </div>
      </div>
      <!-- box search -->
      
      <div class="height10"></div>
      <!-- search result-->
      <div class="pagi-search-result">
      全{$max_record}件中{$page_start}～ {$page_end}件目を表示しています。
      <div class="btn-order"> {$order_select} </div>
    </form>
  </div>
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <div class="height3"></div>
  {$search_content}
  <div class="height3"></div>
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <!--banner footer-->
</div>
<!--container-left-->
<!--container-right-->
{include file="jtb_container_right.tpl"}
<!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>