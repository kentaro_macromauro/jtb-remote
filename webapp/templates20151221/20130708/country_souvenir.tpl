{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider"> <img src="images/country/001.jpg" alt="" />
    <img src="images/country/002.jpg" alt=""  class="hide" />
    <img src="images/country/003.jpg" alt=""  class="hide" />
    <img src="images/country/004.jpg" alt=""  class="hide" />
    <img src="images/country/005.jpg" alt=""  class="hide" />
    <img src="images/country/006.jpg" alt=""  class="hide" />
    <img src="images/country/007.jpg" alt=""  class="hide" />
    <img src="images/country/008.jpg" alt=""  class="hide" />
    <img src="images/country/009.jpg" alt=""  class="hide" />
    <img src="images/country/010.jpg" alt="" class="hide" /> </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="text-content">
    {if $config.country eq "TWN"}
    <h1 class="font_red">台湾{elseif $config.country eq "IDN"}
    <h1 class="font_red">インドネシア{elseif $config.country eq "THA"}
    <h1 class="font_red">タイ{elseif $config.country eq "VNM"}
    <h1 class="font_red">ベトナム{elseif $config.country eq "KHM"}
    <h1 class="font_red">カンボジア{elseif $config.country eq "MYS"}
    <h1 class="font_red">マレーシア{elseif $config.country eq "SGP"}
    <h1 class="font_red">シンガポール{elseif $config.country eq "HKG"}
    <h1 class="font_red">香港{elseif $config.country eq "AUS"}
    <h1 class="font_red">オーストラリア{elseif $config.country eq "NZL"}
    <h1 class="font_red">ニュージーランド{/if}のおみやげを販売</h1>
     
    {if $config.country eq "TWN"}
    <p>JTB台湾が厳選したおみやげをオンラインで販売。<br/>
【季節限定】台湾からフレッシュなフルーツをお届けします！JTB台湾の契約農家が丹精こめて育てたアップルマンゴー、玉荷包ライチ、黒葉ライチを日本まで航空＆クール便でお届け。南国の太陽をたっぷり浴びて育った甘い果実を、日本でお楽しみ下さい。お中元やお祝い、贈り物にも大変喜ばれます。<br/>
※フレッシュフルーツは収穫状況によりお申込期間、配送期間が変わります。詳しくは各商品ページのご案内をご覧下さい。</p>
    
    {elseif $config.country eq "IDN"}
    <p>JTBインドネシアが厳選したおみやげをオンラインで販売。JTBインドネシアがお奨めするおみやげラインナップ。楽しい旅の思い出と一緒にすてきなお土産はいかがですか。</p>
    {elseif $config.country eq "THA"}
    <p>JTBタイが厳選したおみやげをオンラインで販売。JTBタイがお奨めするおみやげラインナップ。楽しい旅の思い出と一緒にすてきなお土産はいかがですか。</p>
    {elseif $config.country eq "VNM"}
    <p>JTBベトナムが厳選したおみやげをオンラインで販売。JTBベトナムがお奨めするおみやげラインナップ。楽しい旅の思い出と一緒にすてきなお土産はいかがですか。</p>
    {elseif $config.country eq "KHM"}
    <p>JTBカンボジアが厳選したおみやげをオンラインで販売。JTBカンボジアがお奨めするおみやげラインナップ。楽しい旅の思い出と一緒にすてきなお土産はいかがですか。</p>
    {elseif $config.country eq "MYS"}
    <p>JTBマレーシアが厳選したおみやげをオンラインで販売。<br/>
JTBマレーシアがお奨めするおみやげラインナップ。楽しい旅の思い出と一緒に、マレーシアのすてきなお土産はいかがですか。</p>
    {elseif $config.country eq "SGP"}
    <p>JTBシンガポールが厳選したおみやげをオンラインで販売。JTBオリジナル、限定販売品のマーライオン＆ハローキティーのりチップスや、ご当地QPの決定版、マーライオンQPなどオリジナリティーあふれる商品は絶対おすすめ。<br/>
また、かさばるお酒類や植物検疫などの関係で持込ができないトロピカルフルーツもJTBの宅配サービスなら心配ご無用!!日本のご自宅や、その他指定の場所にお届けいたします。</p>
    {elseif $config.country eq "HKG"}
    
    {elseif $config.country eq "AUS"}
    <p>JTBオーストラリアが厳選したおみやげをオンラインで販売。オーストラリア旅行の思い出になるおみやげをお探しの方へ、マイバスデスクスタッフが選んだ旅のおみやげをご案内しています。ご旅行出発前にゆっくり選んで事前お申込みいただければ、到着後にピックアップできます。ご滞在中の時間がさらに満喫できますね。</p>
    {elseif $config.country eq "NZL"}
    <p>JTBニュージーランドが厳選したおみやげをオンラインで販売。日本でも話題になる美容プラセンタクリームや、健康食品としてお土産一番人気のニュージーランド産ハチミツが、こちらからご購入いただけます。</p>
    {/if} 
    </div>
   <div class="clear"></div>
   <div class="height10"></div>
	<!-- box search -->
    <form action="souvenir_search.php" method="get" name="frmSearch">
       <div class="box-search-souvenir-header">Search</div>
	   		<div class="box-search-souvenir">
      	    <div class="inp-inputsearch">
            	<div class="inp-inputsearch-inp1"><p>国名</p> {$inp_country}</div>
                <div class="inp-inputsearch-inp2"><p>受取場所</p> {$inp_location} </div>
                <div class="inp-inputsearch-inp3"><p>カテゴリー</p>{$inp_theme}</div>
                <div class="clear"></div>
                <div class="inp-inputsearch-inp4"><p>フリーワード</p><input type="text" name="keyword" value=""  class="w210" /></div>
            </div>                
       <div class="box-search4">
       		<input name="inp_search" class="btn_search" type="submit" value="検索" />
       </div>      
    </div>
    <!-- box search -->

    
    <div class="header-text-tittle">おすすめ{if $config.country eq "TWN"}
台湾{elseif $config.country eq "IDN"}
インドネシア{elseif $config.country eq "THA"}
タイ{elseif $config.country eq "VNM"}
ベトナム{elseif $config.country eq "KHM"}
カンボジア{elseif $config.country eq "MYS"}
マレーシア{elseif $config.country eq "SGP"}
シンガポール{elseif $config.country eq "HKG"}
香港{elseif $config.country eq "AUS"}
オーストラリア{elseif $config.country eq "NZL"}
ニュージーランド{/if}おみやげ</div>
    <div class="image-slider">
      <div class="slider-left"></div>
      <div class="slider-mid">
        <ul class="slider-content-index">
          {$productslider1}
        </ul>
      </div>
      <div class="slider-right"></div>
    </div>
    
    
     {if $banner_name1 != "" || $banner_name2 != "" || $banner_name3 != ""}    
    <!--banner-navigation-->
    
    <!--Banner List-->
     <div class="clear"></div>
    <div class="height10"></div>
    
    <div class="banner-navigation">
      <div class="banner-navigation-main"> {if $banner_name1 != ""} <a href="{$banner_link1}" title="menu1" ><img src="{$banner_img1}" alt="menu1" height="168" width="767"  /></a> {/if}
        {if $banner_name2 != ""} <a href="{$banner_link2}" title="menu2" class="hide"><img src="{$banner_img2}" alt="menu2" height="168"  width="767" /></a> {/if}
        {if $banner_name3 != ""} <a href="{$banner_link3}" title="menu3" class="hide"><img src="{$banner_img3}" alt="menu3" height="168" width="767" /></a> {/if} </div>
      <ul class="banner-navigation-btn">
        {if $banner_name1 != ""}
        <li class="menu1 p-right3 active" id="banner-nav1">{$banner_name1}</li>
        {/if}
        {if $banner_name2 != ""}
        <li class="menu2 p-right3"  id="banner-nav2">{$banner_name2}</li>
        {/if}
        {if $banner_name3 != ""}
        <li class="menu3"  id="banner-nav3">{$banner_name3}</li>
        {/if}
      </ul>
    </div>
    <!--Banner List-->
    <div class="clear"></div> 
    <div class="height5"></div>  
     
    <!--banner-navigation-->
    {/if}
    
   {if $souvenir_product_list1}
    
    <div class="header-text-tittle">おすすめ{if $config.country eq "TWN"}
台湾{elseif $config.country eq "IDN"}
インドネシア{elseif $config.country eq "THA"}
タイ{elseif $config.country eq "VNM"}
ベトナム{elseif $config.country eq "KHM"}
カンボジア{elseif $config.country eq "MYS"}
マレーシア{elseif $config.country eq "SGP"}
シンガポール{elseif $config.country eq "HKG"}
香港{elseif $config.country eq "AUS"}
オーストラリア{elseif $config.country eq "NZL"}
ニュージーランド{/if}おみやげ　現地受取商品</div>
     
    <div class="image-slider" >
     <div class="padding-slider-left"></div>
      <div class="slider-mid">	
      
        <ul class="slider-content-index-a" >
         {$souvenir_product_list1}
        </ul>
      </div>
    
    </div>
   
    
    <div class="clear"></div>
    {/if}
    
    {if $souvenir_product_list2}
    
     <div class="header-text-tittle">おすすめ{if $config.country eq "TWN"}
台湾{elseif $config.country eq "IDN"}
インドネシア{elseif $config.country eq "THA"}
タイ{elseif $config.country eq "VNM"}
ベトナム{elseif $config.country eq "KHM"}
カンボジア{elseif $config.country eq "MYS"}
マレーシア{elseif $config.country eq "SGP"}
シンガポール{elseif $config.country eq "HKG"}
香港{elseif $config.country eq "AUS"}
オーストラリア{elseif $config.country eq "NZL"}
ニュージーランド{/if}おみやげ　日本受取商品</div>
      <div class="image-slider">
      <div class="padding-slider-left"></div>
      <div class="slider-mid">
      <ul class="slider-content-index-a">
     {$souvenir_product_list2}
      </ul>
      </div>
      
      </div>
    
    
     <div class="clear"></div>
    
  {/if}  
    <!--banner footer-->
     <div class="clear"></div>
    <!--banner footer-->
    
    <!--end special content-->
    
    {if $config.openkeyword eq "true"}
    {if $config.country eq "TWN"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "IDN"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "THA"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "VNM"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "KHM"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "MYS"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "SGP"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "HKG"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "AUS"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {elseif $config.country eq "NZL"}
    <!--POPULAR TOUR KEYWORD-->
    <div class="popular_tour_keyword">
      <!--box1-->
      <p class="city">クアラルンプール</p>
      <p class="word">世界遺産マラッカ　クアラルンプール市内観光　バツー洞窟観光　蛍鑑賞ツアー　クアラガンダ象保護区ツアー　ジャングル体験ツアーラフレシアツアー　スパ　エステ　マッサージ　ゴルフ　マレーシアショッピング</p>
      <!--end box1-->
      <!--box2-->
      <p class="city">コナキタバル</p>
      <p class="word">コナキタバル市内観光　ロッカウイ動物園　キナバル公園　テングザル　マングローブツアー　昆虫ツアー　北ボルネオ鉄道</p>
      <!--end box2-->
      <!--box3-->
      <p class="city">ランカウイ</p>
      <p class="word">シュノーケリング　カヤック　島内観光　無人島ツアー　ナイトマーケット</p>
      <!--end box3-->
      <!--box4-->
      <p class="city">ペナン</p>
      <p class="word">バタフライファーム　ロングステイ　オランウータン島　ゴルフ　世界遺産ジョージタウン</p>
      <!--end box4-->
    </div>
    <!--END POPULAR TOUR KEYWORD-->
    {/if} 
    {/if}
    </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>