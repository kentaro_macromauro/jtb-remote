{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider">
  	<img src="images/country/001.jpg" alt="" />
    <img src="images/country/002.jpg" alt=""  class="hide" />
    <img src="images/country/003.jpg" alt=""  class="hide" />
    <img src="images/country/004.jpg" alt=""  class="hide" />
    <img src="images/country/005.jpg" alt=""  class="hide" />
    <img src="images/country/006.jpg" alt=""  class="hide" />
    <img src="images/country/007.jpg" alt=""  class="hide" />
    <img src="images/country/008.jpg" alt=""  class="hide" />
    <img src="images/country/009.jpg" alt=""  class="hide" />
    <img src="images/country/010.jpg" alt=""  class="hide" />
  </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
  	{$breadcamp}
    <div class="text-content">
      <p> {if $config.country eq "SGP"}
        マイバスシンガポールへようこそ！シンガポールは前代未聞の大変革中！今なお進化し続けていて、めまぐるしい変貌は絶対に見逃せません。小さいけれど緑豊かな南の島にはおもちゃ箱のように夢がいっぱい詰まっているんです。度肝を抜く奇抜さと驚きの大建造物、動物や自然と触れ合える都会のオアシス、異文化のユニークちゃんぽん、グルメにはたまらない美食探訪、可愛いアジアン雑貨から最旬のブランドまでの買い物天国、子どもも大人も大興奮のエンターテインメントの数々、一流ホテルでゴージャスな紳士・淑女気分…古きよき伝統と新しいトレンドの楽しみ方は十人十色。あなた流の豊かな時間を演出してみてください。
        {elseif $config.country eq "THA"}
        サワディーカッ！観るも食べるも癒されるも、全てにおいて充実したタイランド。世界遺産に古を学び、寺院で地元の人々の信仰心に触れ・・・ビーチではじけたあとは、美味しいタイフードに舌鼓。大都会バンコクでは買物三昧に食い倒れ。そんなAmazing Thailand があなたをお待ちしております。
        {elseif $config.country eq "IDN"}
        バリ島のご旅行は当島で開業２０年の実績のあるJTBバリ支店にお任せください。観光、各種アクティビティ、ディナー、スパ等等、バリ島の魅力を取り上げた多彩なツアーをご用意しております。楽しく忘れられない旅の思い出をつくりましょう。
        {elseif $config.country eq "MYS"}
        自然豊かなボルネオ島のコタキナバルやランカウイ島、世界遺産の街マラッカやペナンのジョージタウン、長期滞在先としても人気のクアラルンプールなどマレーシアの旅のことならホテルや、日本語ガイドがご案内する定期観光だけでなく、レストランやスパのご予約までなんでもお任せください！思い出に残る旅のお手伝いをいたします！
        {elseif $config.country eq "TPE"}
        歓迎光臨！マイバス台湾では、初めての方にも、もっと台湾を知りたい方にもお楽しみいただけるオプショナルツアーをご用意しています。固定古蹟の「総統府」、ノスタルジックな雰囲気に包まれる街「九份」、新幹線で行く「水の都高雄」を訪れるコースや、願い事を書いて飛ばす「天燈あげ」も大好評。旅先ならではのリラックスに「スパ・マッサージ」、言葉やチップの心配無用な「各種ミールクーポン」では家庭的なお食事から宮廷料理まで豊富なメニューを取り揃えました。旅の思い出に彩りを添える、マイバスオプショナルツアーを是非ご利用ください。
        {elseif $config.country eq "VNM"}
        弊社はホーチミン及びハノイに支店をもち日本からのお客様並びにベトナム在住の皆様に対し旅行業のあらゆるサービスをご提供させて頂いております。特にホーチミンにおいては市内中心部にデスク機能を２箇所（ドンコイデスク店/マジェスティックホテル隣、TAXデパート店）に設置しホーチミンご到着後のお客様に対し万全な対応を行える体制を整えております。また、それらのデスクでは、現地ご到着後の追加オプション申し込みやあらゆる旅のご相談に対しお客様のご満足を頂ける各種情報も整えております。弊社デスクへ是非お立ち寄り下さい。
        {elseif $config.country eq "AUS" }
        オーストラリアは、日本の約20倍の国土を持ち、19の世界遺産がある南半球の国。雄大な自然の中、コアラやカンガルーなどの野生動物との触れ合い、グレートバリアリーフでのダイビングやマリンスポーツ、ロマンチックなクルーズ、ワインと食の旅など、ひとり旅からカップル、家族・グループの方々に広くお楽しみいただけます。トロピカルなケアンズ・ゴールドコースト、温暖なシドニー・メルボルン、乾燥した内陸のエアーズロックなど、スケールの大きな体験があなたを待っています。
        {elseif $config.country eq "NZL" }
        ニュージーランドには、皆様にご利用いただけるマイバスセンター、ツアーデスクが、オークランド、クライストチャーチ（2011年12月現在　一時閉鎖中）、クイーンズタウンの3ヶ所にございます。現地オプショナルツアーや人気レストランのお食事券（ミールクーポン）など、多数の商品を取り扱っております。スタッフ全員が日本語を話しますので、お気軽にお立ち寄りください。
主要クレジットカード、Ji保険の現地対応窓口として無料サポートもいたします。“地球の箱庭”ニュージーランドへお越しの際は、真っ先にマイバスセンター、ツアーデスクへどうぞ！
        {elseif $config.country eq "HKG"}        
        JTBが自信を持ってご提供する「ランドパッケージ」のアジア・オセアニア専門販売サイトです。「ランドパッケージ」とは飛行機なしの、宿泊付き旅行パッケージ商品のことです。LCC（ローコストキャリア）やマイルでのご旅行を予定されているお客様に便利な商品です。
JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。
        {/if} </p>
    </div>
    <form action="search_lp.php">
    <div class="serach-menu-left">
      <div class="serach-menu-left-tittle">検索</div>
      
      <div class="search-menu-subject1">目的地</div>
      {$inp_country}
      <div class="height5"></div>
      <span id="select_city">
      {$inp_city}
      </span>
      <div class="height5"></div>
      <div class="search-menu-subject1">日にち </div>
      <div class="search-center">
      {$select_yearmonth}
      {$select_day}
      </div>
      <div class="p_ddpicker" style="padding-left:12px; margin-top:5px;">
      
        <span id="id_cal">カレンダーから選ぶ</span>
        
      </div>
      <div class="height5"></div>
      <p>カテゴリー</p>
      <select class="search-left-selectbox" name="inp_category">
        <option value="0">---Please Select---</option>
        {$select_category}
      </select>
      <div class="height5"></div>
      <p>条件検索</p>
      <select class="search-left-selectbox" name="inp_option">
      	<option value="0">---Please Select---</option>
        {$select_option}
      </select>
      
      
      <div class="height5"></div>
      <p>旅行日数</p>
      <select class="search-left-selectbox" name="inp_time">
        <option value="0">---Please Select---</option>
        {$select_time}
      </select>
      <div class="height5"></div>
      <div class="search-menu-subject1">フリーワード検索</div>
      <input type="text" class="search_left_inp" name="inp_keyword" value="" />
      <div class="height10"></div>
      <input type="submit" value="検索" class="search-left-button" />
      <div class="height5"></div>
      <div class="search-bg-footer"></div>
      </div>
    </form>
    <div class="container-wsearch-left">
      <div class="container-wsearch-tittle">おすすめツアー</div>
      <div class="content">
        <div class="slider-box-top"></div>
        <div class="slider-content">
          <ul class="slider-content-mid">
            {$productslider}
          </ul>
        </div>
        <div class="slider-box-bottom"></div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="height10"></div>
    <!--banner-navigation-->
    <div class="banner-navigation">
   	<div class="banner-navigation-main">
    	{if $banner_name1 != ""}
    	<a href="{$banner_link1}" title="menu1" ><img src="{$banner_img1}" alt="menu1" height="168" width="767"  /></a>
        {/if}
        {if $banner_name2 != ""}
        <a href="{$banner_link2}" title="menu2" class="hide"><img src="{$banner_img2}" alt="menu2" height="168"  width="767" /></a>
        {/if}
        {if $banner_name3 != ""}
        <a href="{$banner_link3}" title="menu3" class="hide"><img src="{$banner_img3}" alt="menu3" height="168" width="767" /></a>
        {/if}
    </div>
   	<ul class="banner-navigation-btn">
      {if $banner_name1 != ""}
   	  <li class="menu1 p-right3 active" id="banner-nav1">{$banner_name1}</li>
      {/if}
      {if $banner_name2 != ""}
      <li class="menu2 p-right3"  id="banner-nav2">{$banner_name2}</li>
      {/if}
      {if $banner_name3 != ""}
      <li class="menu3"  id="banner-nav3">{$banner_name3}</li>
      {/if}
    </ul>
    </div>
    <div class="clear"></div>
    <!--banner-navigation-->
    <div class="height10"></div>
    
    <div class="border-b4"></div>
<div class="height10"></div>
    
    
    <!--category-->
    <div class="category2">
    	<div class="category_box">
        	<div class="category_tittle">Sale!</div>
            <div class="category_image">
            <img src="{$catimg1}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg1}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category1}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_country={$config.country}&amp;inp_option=1" >「Sale!」検索結果一覧へ</a></div>
        </div>
     </div>   
     <div class="category2">    
        <div class="category_box">
        	<div class="category_tittle">観光充実</div>
            <div class="category_image">
            <img src="{$catimg4}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg4}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category4}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_country={$config.country}&amp;inp_option=4" >「観光充実」検索結果一覧へ</a></div>
        </div>
      </div> 
      <div class="category2"> 
        <div class="category_box">
        	<div class="category_tittle">新着</div>
            <div class="category_image">
            <img src="{$catimg3}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg3}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category3}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_country={$config.country}&amp;inp_option=2"  >「新着」検索結果一覧へ</a></div>
        </div>
        
    </div>
   	<div class="category2">     
        <div class="category_box">
        	<div class="category_tittle">こだわりの旅</div>
            <div class="category_image">
            <img src="{$catimg2}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg2}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
                {$category2}
            </ul>           	
        </div>
        
        <div class="category_link"><a href="search_lp.php?inp_country={$config.country}&amp;inp_option=3"  >遊「こだわりの旅」検索結果一覧へ</a></div>
    </div>
    <!--category-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>