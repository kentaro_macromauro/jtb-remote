{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        
        
        <ul class="booking-nav-process {if $config.allot != 'false'}limit4{/if}" >
            <li id="step1" class="active_last">希望日選択</li>
            <li id="step2" class="active">カート</li>
            
            <li id="step3" class="active_first">予約情報入力</li>
            <li id="step4">予約情報確認</li>
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step5">お支払い方法の選択</li>
            {/if}
            
            {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'}
            <li id="step6">決済情報確認</li>
            <li id="step7">決済情報確認</li>
            {/if}
            
            <li id="step8">完了</li>
        </ul>
        
        <div class="clear"></div>
        <span class="texttittle" >カート</span>
        <!-- op packet -->
        <table cellspacing="1" class="tbform" >
          <tr>
            <th class="center"> 日付 </th>
            <th class="center"> ツアー名 </th>
            <th class="center">参加人数 </th>
            <th class="center" colspan="3">ツアー料金 </th
          >
          </tr>
          {foreach from=$bookingArray item=booking}
          <tr>
            <td>{$booking.date_format}</td>
            <td>{$booking.product.name_jp}</td>
            <td>
            {if $booking.charge_type eq 'Per Service'}
            	{$booking.product.qty_adult}
            {else}
            
            
            {if $booking.product.price_adult > 0}
            
              大人：{$booking.product.qty_adult}<br/>
            {/if}  
              
            {if $booking.product.price_child > 0}
              子供：{$booking.product.qty_child}<br/>
            {/if}  
              
              
              幼児：{$booking.product.qty_infant} 
              
           {/if}
              </td>
            <td style="width:77px; "> 
            
            小計：
            {if $booking.amount_sgd eq 0}-
            {else}
            	{$booking.product.sign}{$booking.amount_sgd} {if $booking.other.show_rate eq 1 }<br/>（約{$booking.amount_yen}円）{/if}
            {/if}
            </td>
            <td style="width:57px; text-align:center;"><a href="../{$booking.product.country_path}/booking_del.php?product_id={$booking.product.id}">削除</a> <a href="{$booking.product.info}">修正</a></td>
            <td style="text-align:center; width:50px;"><form action="../{$booking.product.country_path}/booking_cart_action.php" method="post">
                
                <input type="hidden"  name="product_id"  value="{$booking.product.id}" />
                
                <input type="submit"  class="btn_submit" value="お申し込み" />
              </form>
            </td>
          </tr>
          {/foreach}
        </table>
        <!-- op packet -->
        <span style="padding:5px 150px 5px 165px; display:block;">
        
        <input type="button" class="btn_submit toindex" value="続けて他のツアーを検索する" style="width:180px;" />
        </span> </div>
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>