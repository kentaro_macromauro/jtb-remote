{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        
        <div class="souvenir_step2"></div>
        
        <div class="clear"></div>
        <span class="texttittle" >カート</span>
        
        {section name=foo start=0 loop=8 step=1}
            
          
            {assign var="no_cart" value=$smarty.section.foo.index}
            <!--souvenir cart-->
            
           
            
            {if $cart_amount.$no_cart > 0}
            	
            	<form name="on_cart{$no_cart}" method="post" >
                
                <!--product_can_sale--recive_on_spot--credit-->
                {if $no_cart eq 0} <!--product_can_sale--recive_on_spot--credit--> {/if}
                
                <!--product_can_sale--recive_on_spot--cash-->
                {if $no_cart eq 1} <!--product_can_sale--recive_on_spot--cash-->  {/if}
                
                <!--product_can_sale_recive_in_japan--credit-->
                {if $no_cart eq 2} <!--product_can_sale_recive_in_japan--credit-->  {/if}
                
                <!--product_can_sale_recive_in_japan--cash-->
                {if $no_cart eq 3} <!--product_can_sale_recive_in_japan--cash-->  {/if}
                
                
                <!--product_on_request--recive_on_spot--credit-->
                {if $no_cart eq 4} <!--product_on_request--recive_on_spot-->  {/if}

                <!--product_on_request--recive_in_japan--credit-->
                {if $no_cart eq 5} <!--product_on_request--recive_in_japan-->  {/if}
               
               
                
                
                <table cellspacing="1" class="tbformSouvenir" >
                <tr>
                	<th class="alignC">削除</th>
                    <th class="alignC">商品写真</th>
                    <th class="alignC">商品名</th>
                    <th class="alignC">単価</th>
                    <th class="alignC">数量</th>
                    <th class="alignC">小計</th>
                </tr>
                {foreach from=$items_on_cart.$no_cart item="_items"}
                    <tr>
                    <td class="alignC"><input type="submit" class="btn_link" name="cart_remove_item_{$_items.id}" value="削除" /></td>
                    <td class="alignC"><a href="souvenir_booking.php?id={$_items.id}&inp_qty={$_items.qty}"><img src="{$config.documentroot}product/images/souvenir/{$_items.id}-1.jpg" style="width:56px;" /></a></td>
                    <td class="alignL"><a href="souvenir_booking.php?id={$_items.id}&inp_qty={$_items.qty}" class="txtBlack">{$_items.detail.souvenir_name}</a></td>
                    <td class="alignR">{$_items.detail.sign} {$_items.price} </td>
                    <td class="alignC">{$_items.inp_qty} <div>
                    <input type="submit" class="btn_link_small" name="cart_update" value="更新" /></div> </td>
                    <td class="alignR">{$_items.detail.sign} {$_items.amount} </td>
                    
                    </tr>
                {/foreach}
                <tr><th colspan="5" class="alignR" >合計<!--</td><td>{$cart_qty.0}--></td><td class="alignR">{$_items.detail.sign} {$cart_amount.$no_cart}</th></tr>
                <tr><td colspan="6" class="alignR">
                <input type="hidden" name="cart_type" value="{$no_cart}" />
                
              
                <input type="submit" class="btn_submit btn_Checkout" name="cart_checkout" value="注文" />
            
                </td>
                </tr>
                </table>
                </form>
            {/if}   
            <!--souvenir cart-->
         {/section}
        </div>
    </div>
    
  
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    {literal}
    <script type="text/javascript">
		$(document).ready(
			function(){
				$('.search-left-selectbox').change(
					function(){
						$(this).parent().find('.btn_link_small').show();	
						$(this).parent().find('.btn_link_small').click();
						
					}								   
				);
			}				  
		);
	</script>
    {/literal}    
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>