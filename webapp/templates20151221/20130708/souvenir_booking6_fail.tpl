{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
     <div class="formbox">
           
          {if $credit eq "1"}
          <div class="souvenir_step4"></div>
          {else}
          <div class="souvenir_none_credit_step4"></div>
 		  {/if}         
          
          
          <div class="clear"></div>
			<div class="height10"></div>
			
        	<p>ご注文は完了しませんでした。<br/>大変恐れ入りますが今一度お試しくださいませ。</p>
          
          </div>
      
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>