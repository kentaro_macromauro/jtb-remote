<div class="bg_footer">
  <!--FOOTER CONTAINER-->
  <div class="footer_container">
    <!--QUICK NAV-->
    <div class="quick_nav">
      <!--block1-->
      <div class="quick_nav_block">
        <p><a href="../taiwan/">台湾</a></p>
        <ul>
          <li> <a href="taiwan/search_opt.php?inp_country=TWN&amp;inp_city=TPE"> 台北</a> </li>
          <li> <a href="taiwan/search_opt.php?inp_country=TWN&amp;inp_city=KAO"> 高雄</a> </li>
          <li> <a href="taiwan/search_opt.php?inp_country=TWN&amp;inp_city=HUA"> 花蓮</a> </li>
          <li> <a href="taiwan/search_opt.php?inp_country=TWN&amp;inp_city=OTH"> その他の地域</a> </li>
        </ul>
      </div>
      <!--end block1-->
      <!--block2-->
      <div class="quick_nav_block">
        <p><a href="../indonesia/" >インドネシア</a></p>
        <ul>
        	<li> <a href="indonesia/search_opt.php?inp_country=IDN&amp;inp_city=BAR"> バリ</a></li>
            <li> <a href="indonesia/search_opt.php?inp_country=IDN&amp;inp_city=JAK"> ジャカルタ</a></li>
            <li> <a href="indonesia/search_opt.php?inp_country=IDN&amp;inp_city=OTH"> その他の地域</a></li>            
        </ul>
      </div>
      <!--end block2-->
      <!--block3-->
      <div class="quick_nav_block">
        <p><a href="../thailand/">タイ</a></p>
        <ul>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=BKK"> バンコク</a> </li>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=CNX"> チェンマイ・チェンライ</a> </li>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=HKT"> プーケット</a> </li>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=KSM"> サムイ</a> </li>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=KRB"> グラビ</a> </li>
          <li> <a href="thailand/search_opt.php?inp_country=THA&amp;inp_city=OTH"> その他の地域</a> </li>
        </ul>
      </div>
      <!--end block3-->
      <!--block4-->
      <div class="quick_nav_block" style="width:100px;">
        <p><a href="../vietnam/">ベトナム</a></p>
        <ul>
          <li> <a href="vietnam/search_opt.php?inp_country=VNM&amp;inp_city=HCM"> ホーチミン</a> </li>
          <li> <a href="vietnam/search_opt.php?inp_country=VNM&amp;inp_city=HAN"> ハノイ</a> </li>
          <li> <a href="vietnam/search_opt.php?inp_country=VNM&amp;inp_city=OTH"> 中部ベトナム・その他</a> </li>
        </ul>
      </div>
      <!--end block4-->
      <!--block5-->
      <div class="quick_nav_block">
        <p><a href="../cambodia/">カンボジア</a></p>
        <ul>
          <li> <a href="cambodia/search_opt.php?inp_country=KHM&amp;inp_city=REP"> シェムリアップ</a> </li>
          <li> <a href="cambodia/search_opt.php?inp_country=KHM&amp;inp_city=OTH"> その他の地域</a> </li>
        </ul>
      </div>
      <!--end block5-->
      <!--block6-->
      <div class="quick_nav_block">
        <p><a href="../malaysia/">マレーシア</a></p>
        <ul>
          <li> <a href="malaysia/search_opt.php?inp_country=MYS&amp;inp_city=KUA"> クアラルンプール</a> </li>
          <li> <a href="malaysia/search_opt.php?inp_country=MYS&amp;inp_city=PEN"> ペナン</a> </li>
          <li> <a href="malaysia/search_opt.php?inp_country=MYS&amp;inp_city=LAN"> ランカウイ</a> </li>
          <li> <a href="malaysia/search_opt.php?inp_country=MYS&amp;inp_city=KOT"> コタキナバル</a> </li>
          <li> <a href="malaysia/search_opt.php?inp_country=MYS&amp;inp_city=MLC"> マラッカ</a> </li>
        </ul>
      </div>
      <!--end block6-->
      <!--block7-->
      <div class="quick_nav_block">
        <p><a href="../singapore/">シンガポール</a></p>
        <ul>
          <li> <a href="singapore/search_opt.php?inp_country=SGP&amp;inp_city=SIN"> シンガポール</a> </li>
          <li> <a href="singapore/search_opt.php?inp_country=SGP&amp;inp_city=OTH"> その他の地域</a> </li>
        </ul>
      </div>
      <!--end block7-->
      <!--block8-->
     
      <div class="quick_nav_block" style="width:70px;">
        <p><a href="../hongkong/" target="_blank">香港</a></p>
       <ul> <!--
           <li><a href="hongkong/search_opt.php?inp_country=HKG&inp_city=HKG"> 香港</a></li>
           <li><a href="hongkong/search_opt.php?inp_country=HKG&inp_city=MAC"> マカオ</a></li>
           <li><a href="hongkong/search_opt.php?inp_country=HKG&inp_city=SHZ"> 深圳</a></li>
           <li><a href="hongkong/search_opt.php?inp_country=HKG&inp_city=GGZ"> 広州</a></li>
        </ul>-->
        
      </div>
      <!--end block8-->
      <!--block9-->
      <div class="quick_nav_block">
        <p><a href="../australia/">オーストラリア</a></p>
        <ul>
          <li><a href="../australia/page.php?id=sydney.html">シドニー</a></li>
          <li><a href="../australia/page.php?id=gold_coast.html">ゴールドコースト</a></li>
          <li><a href="../australia/page.php?id=cairns.html">ケアンズ</a></li>
          <li><a href="../australia/page.php?id=melbourne.html">メルボルン</a></li>
          <li><a href="../australia/page.php?id=perth.html">パース</a></li>
          <li><a href="../australia/page.php?id=ayersrock.html">エアーズロック</a></li>
          <li><a href="../australia/page.php?id=adelaide.html">アデレード</a></li>
          <li><a href="../australia/page.php?id=tasmania.html">タスマニア</a></li>
          <li><a href="../australia/page.php?id=brisbane.html">ブリスベン</a></li>
          <li><a href="../australia/page.php?id=darwin.html">ダーウィン</a></li>
          <li><a href="../australia/page.php?id=hamilton_island">ハミルトン島</a></li>
        </ul>
      </div>
      <!--end block9-->
      <!--block10-->
      <div class="quick_nav_block" style="margin-right:0px; width:99px;">
        <p><a style="font-size:12px;" href="../newzealand/">ニュージーランド</a></p>
        <ul>
          <li><a href="../newzealand/page.php?id=auckland.html">オークランド</a></li>
          <li><a href="../newzealand/page.php?id=rotorua.html">ロトルア</a></li>
          <li><a href="../newzealand/page.php?id=christchurch.html">クライストチャーチ</a></li>
          <li><a href="../newzealand/page.php?id=mount_cook.html">マウントクック</a></li>
          <li><a href="../newzealand/page.php?id=queenstown.html">クイーンズタウン</a></li>
        </ul>
      </div>
      <!--end block10-->
      <div class="clear"></div>
    </div>
    <!--END QUICK NAV-->
    <div class="footer_link">
      <ul>
        <li><a href="page.php?id=branch.html">会社概要</a></li>
        <li><a href="page.php?id=term.html">旅行約款</a></li>
        <li><a href="page.php?id=privacy.html">個人情報保護方針</a></li>
      </ul>
    </div>
    <div class="copyrights">Copyright &copy; 2013 MyBus-Asia.com All rights reserved </div>
    <div class="clear"></div>
  </div>
  <!--END FOOTER CONTAINER-->
</div>
{literal} 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32237848-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}