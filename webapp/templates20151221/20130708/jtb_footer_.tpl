<div class="bg_footer">
  <!--FOOTER CONTAINER-->
  <div class="footer_container">
    <!--QUICK NAV-->
    <div class="quick_nav">
      
      <!--block1-->
      <div class="quick_nav_block">
        <p><a href="../taiwan/">台湾</a></p>
        <ul>
          <li><a href="../taiwan/page.php?id=taipei.html">台北</a></li>
          <li><a href="../taiwan/page.php?id=kaohsiung.html">高雄</a></li>
        </ul>
      </div>
      <!--end block1-->
      <!--block2-->
      <!--
      <div class="quick_nav_block">
        <p><a href="../indonesia/">インドネシア</a></p>
        <ul>
        	<li><a href="../indonesia/page.php?id=bali.html">バリ</a></li>
            <li><a href="../indonesia/page.php?id=jakarta.html">ジャカルタ</a></li>
        </ul>
      </div>
      -->
      <!--end block2-->
      <!--block3-->
      <div class="quick_nav_block">
        <p><a href="../thailand/">タイ</a></p>
        <ul>
          <li><a href="../thailand/page.php?id=bangkok.html">バンコク</a></li>
          <li><a href="../thailand/page.php?id=phuket.html">プーケット</a></li>
          <li><a href="../thailand/page.php?id=chiang_mai.html">チェンマイ</a></li>
          <li><a href="../thailand/page.php?id=samui.html">サムイ</a></li>
          <li><a href="../thailand/page.php?id=krabi.html">グラビ</a></li>
        </ul>
      </div>
      <!--end block3-->
      <!--block4-->
      <div class="quick_nav_block">
        <p><a href="../vietnam/">ベトナム</a></p>
        <ul>
          <li><a href="../vietnam/page.php?id=ho_chi_minh_city_saigon.html">ホーチミン</a></li>
      	  <li><a href="../vietnam/page.php?id=hanoi.html">ハノイ</a></li>
        </ul>
      </div>
      <!--end block4-->
      <!--block5-->
      <div class="quick_nav_block">
        <p><a href="../cambodia/">カンボジア</a></p>
        <ul>
           <li><a href="../cambodia/page.php?id=siem_reap.html">シェムリアップ</a></li>
        </ul>
      </div>
      <!--end block5-->
      <!--block6-->
      <div class="quick_nav_block">
        <p><a href="../malaysia/">マレーシア</a></p>
        <ul>
          <li><a href="../malaysia/page.php?id=kuala_lumpur.html">クアラルンプール</a></li>
          <li><a href="../malaysia/page.php?id=penang.html">ペナン</a></li>
          <li><a href="../malaysia/page.php?id=kota_kinabalu.html">コタキナバル</a></li>
          <li><a href="../malaysia/page.php?id=langkawi.html">ランカウイ</a></li>
          <li><a href="../malaysia/page.php?id=malacca.html">マラッカ</a></li>
          <li><a href="../malaysia/page.php?id=cameron_highlands.html" style="font-size:8px;">キャメロンハイランド</a></li>
          <li><a href="../malaysia/page.php?id=pangkor.html">パンコール</a></li>
          <li><a href="../malaysia/page.php?id=terengganu.html">トレンガヌ</a></li>
        </ul>
      </div>
      <!--end block6-->
      <!--block7-->
      <div class="quick_nav_block">
        <p><a href="../singapore/">シンガポール</a></p>
        <ul>
        	<li><a href="../singapore/page.php?id=singapore.html">シンガポール</a></li>
        </ul>
      </div>
      <!--end block7-->
      <!--block8-->
      <div class="quick_nav_block">
        <p><a href="../hongkong/">香港</a></p>
        <ul>
           <li><a href="../hongkong/page.php?id=hongkong.html">香港</a></li>
      	   <li><a href="../hongkong/page.php?id=macau.html">マカオ</a></li>
        </ul>
      </div>
      <!--end block8-->
      <!--block9-->
      <div class="quick_nav_block">
        <p><a href="../australia/">オーストラリア</a></p>
        <ul>
          <li><a href="../australia/page.php?id=sydney.html">シドニー</a></li>
          <li><a href="../australia/page.php?id=gold_coast.html">ゴールドコースト</a></li>
          <li><a href="../australia/page.php?id=cairns.html">ケアンズ</a></li>
          <li><a href="../australia/page.php?id=melbourne.html">メルボルン</a></li>
          <li><a href="../australia/page.php?id=perth.html">パース</a></li>
          <li><a href="../australia/page.php?id=ayersrock.html">エアーズロック</a></li>
          <li><a href="../australia/page.php?id=adelaide.html">アデレード</a></li>
          <li><a href="../australia/page.php?id=tasmania.html">タスマニア</a></li>
          <li><a href="../australia/page.php?id=brisbane.html">ブリスベン</a></li>
          <li><a href="../australia/page.php?id=darwin.html">ダーウィン</a></li>
          <li><a href="../australia/page.php?id=hamilton_island">ハミルトン島</a></li>
        </ul>
      </div>
      <!--end block9-->
      <!--block10-->
      <div class="quick_nav_block" style="margin-right:0px; width:99px;">
        <p><a style="font-size:12px;" href="../newzealand/">ニュージーランド</a></p>
        <ul>
          <li><a href="../newzealand/page.php?id=auckland.html">オークランド</a></li>
          <li><a href="../newzealand/page.php?id=rotorua.html">ロトルア</a></li>
          <li><a href="../newzealand/page.php?id=another_north_island.html">その他北島</a></li>
          <li><a href="../newzealand/page.php?id=christchurch.html">クライストチャーチ</a></li>
          <li><a href="../newzealand/page.php?id=mount_cook.html">マウントクック</a></li>
          <li><a href="../newzealand/page.php?id=queenstown.html">クイーンズタウン</a></li>
          <li><a href="../newzealand/page.php?id=another_south_island.html">その他南島</a></li>
        </ul>
      </div>
      <!--end block10-->
      <div class="clear"></div>
    </div>
    <!--END QUICK NAV-->
    <div class="footer_link">
      <ul>
        <li><a href="page.php?id=branch.html">会社概要</a></li>
        <li><a href="page.php?id=term.html">旅行約款</a></li>
        <li><a href="page.php?id=privacy.html">個人情報保護方針</a></li>
      </ul>
    </div>
    <div class="copyrights">Copyright &copy; 2012 My Bus. All rights reserved </div>
    <div class="clear"></div>
  </div>
  <!--END FOOTER CONTAINER-->
</div>