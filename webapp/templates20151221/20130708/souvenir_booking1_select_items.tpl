﻿{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      
        <div class="formbox">
           <form action="?" method="post">
          
         
          <div class="souvenir_step1"></div>
          
          
          <div class="clear"></div>

          <!--product select-->
         
          <div class="booking-view-product">
            <div class="booking-view-souvenir-img"> <img src="{$booking.souvenir.img}" width="100" /> </div>
            <div class="booking-view-souvenir-text"> <strong class="booking-view-souvenir-title">{$booking.souvenir.name}</strong>
              <p>{$booking.souvenir.description}</p>
              <input type="hidden" name="souvenir_id" value="{$booking.souvenir.id}" />
            </div>
            <div class="clear"></div>
          </div>
          
          <!--product select-->
          <div class="booking-view-product">
          
          {if $msg_error != ""}
          	  <p class="font_red">{$txt_error}</p>        
          {/if}
          
          	 <div  style="float:right; width:430px; font-size:14px;  color:#333;">
          
          	  <div style="float:left; width:140px; line-height:30px; text-align:right;">販売価格： <span style="font-weight:bold;">{$booking.souvenir.print_price}</span></div>
             
              <div style="float:left; width:85px; line-height:30px; text-align:right;">購買数： </div>
              <div style="float:left; width:100px; line-height:30px; padding-top:3px;">{$booking.souvenir.inp_selnumber}</div>
             
            
             <div style="float:left;">
             <input type="submit" name="add_cart" class="btn_submit addtoCart" value="カゴに入れる" />
                	<input type="hidden" name="id"             	   value="{$booking.souvenir.id}" />
                    <input type="hidden" name="sou_sign"           value="{$booking.souvenir.sign}"/>
                    <input type="hidden" name="sou_rate"           value="{$booking.souvenir.rate}" />
                    <input type="hidden" name="sou_price"          value="{$booking.souvenir.price}"  />
            
              </div>
              
              </div>
             <div class="clear"></div>
          </form>
          </div>
          </div>
      
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>