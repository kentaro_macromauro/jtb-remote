{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        
        {if $cart_type == 0 || $cart_type == 2}
        <div class="souvenir_step3"></div>
        {else}
        <div class="souvenir_none_credit_step3"></div>
        {/if}
        
        <div class="clear"></div>
        <span class="texttittle" >予約情報確認</span>
        <p>以下の情報でお間違いないでしょうか？</p>
        <div class="form3-content">

            <!-- form Register-->
            <div class="height10"></div>
            <div class="product_list">
            <table class="tbformSouvenir" cellspacing="1">
            <tr>
            	<th class="alignC">商品写真</th>
                <th class="alignC">商品名</th>
                <th class="alignC">単価</th>
                <th class="alignC">数量</th>
                <th class="alignC">小計</th>
            </tr>
            {foreach from=$cart.souvenir_product item="_items"}
            <tr>
            	<td  class="alignC">{$_items.image}</td>
            	<td  class="alignL"><span class="txtBold">{$_items.name}</span></td>
                <td  class="alignR">{$_items.price}</td>
                <td  class="alignR">{$_items.qty}</td>
                <td  class="alignR">{$_items.amount}</td>
            </tr>
            {/foreach}
            <tr>
            	<th colspan="4" class="alignR">合計</th>
                <td class="alignR">{$cart.price_sum}</td>
            </tr>
            </table>
            </div>
            
            <div class="height10"></div>
            <p>■購入者情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>購入者名（漢字）</th>
                <td>{$order.inp_surname} {$order.inp_firstname}</td>
              </tr>
              <tr>
                <th>購入者名（ローマ字）</th>
                <td>{$order.inp_surname_furi} {$order.inp_firstname_furi}</td>
              </tr>
              <tr>
                <th>生年月日 </th>
                <td>{$order.birthday_year}年{$order.birthday_month}月{$order.birthday_day}日</td>
              </tr>
              <tr>
                <th height="25">性別 </th>
                <td>{$order.inp_sex}</td>
              </tr>
              <tr>
                <th>メールアドレス（PC）</th>
                <td>{$order.inp_email}</td>
              </tr>
              <tr>
                <th>住所</th>
                <td> 
                  
                  {if $order.type_addr eq 'true'}
                  		{$order.inp_addrovs1} <br/>
                        {$order.inp_addrovs2}
     
                  {else} 
                  
                  <table cellspacing="0" class="regis_address_local_cfm" >
                    <tr >
                      <th>郵便番号</th>
                      <td>〒{$order.inp_addrjp_zip1}-{$order.inp_addrjp_zip2}</td>
                    </tr>
                    <tr>
                      <th>都道府県</th>
                      <td>{$order.inp_addrjp_pref}
                    
                      </td>
                    </tr>
                    <tr>
                      <th>市区町村</th>
                      <td>{$order.inp_addrjp_city}
                      	
                      </td>
                    </tr>
                    <tr>
                      <th>町域</th>
                      <td>{$order.inp_addrjp_area}
                      	
                      </td>
                    </tr>
                    <tr>
                      <th>建物名<br/>
                        部屋番号</td>
                      <td>{$order.inp_addrjp_building}
                      	
                      </td>
                    </tr>
                  </table>
                  {/if}
                  
                 </td>
              </tr>
              <tr>
                <th>連絡先（電話）</th>
                <td>{$order.inp_tel1}
                	
                </td>
              </tr>
              <tr>
                <th>連絡先（携帯）</th>
                <td>{$order.inp_mobile1}
                </td>
              </tr>
            </table>
           
   		   
           <div class="height10"></div>
           
          {if $order.location_rec eq '1'} 
           
          <!-- order outsite japan -->
          <p>■滞在先情報</p>
          <table cellspacing="1" class="tbform" >
          	<tr>
            	 <th>滞在先</th>
                 <td>
                 	<table cellspacing="0" class="regis_address_local_cfm">
                        <tr >
                          <th>滞在先名 </th>
                          <td>{$order.reg_hotel_name}</td>
                        </tr>
                        <tr >
                          <th>滞在先TEL </th>
                          <td>{$order.reg_hotel_tel}</td>
                        </tr>
                        <tr >
                          <th>滞在先住所</th>
                          <td>{$order.reg_hotel_addr}</td>
                        </tr>
                        
                    </table>
                 
                 </td>
            </tr>
            <tr>
            	<th>滞在期間 </th>
                <td>{$order.reg_arrfrm_date_jp} ～ {$order.reg_arrto_date_jp}</td>
            </tr>
            <tr>
            	<th>到着日フライト</th>
                <td>{$order.reg_arrgo_date_jp} {$order.reg_arrgo_time}</td>
            </tr>
            
            <tr>
            	<th>&nbsp;</th>
                <td>フライトナンバー {$order.air_no} </td>
            </tr>
            <tr>
            	<th>ご旅行形態</th>
             	<td>{$order.sel_info}</td>
            </tr>
          </table>
          <!-- order outsite japan -->
          {elseif $order.location_rec eq '2'}
          <!-- order insite japan-->
          
          <p>■お届け先情報</p>
            
            
            <!--address send product-->
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>お届け先名（漢字） </th>
                <td>{$order.inprec_surname} {$order.inprec_firstname}</td>
              </tr>
              <tr>
                <th>お届け先名（ローマ字）</th>
                <td> {$order.inprec_surname_furi} {$order.inprec_firstname_furi}</td>
              </tr>
              <tr>
                <th>お届け先住所</th>
                <td><!--Address JP-->
                  {if $order.type_addr2 eq 'true'}
                  		{$order.inprec_addrovs1}  <br/>
                        {$order.inprec_addrovs2}
                  {else} 
                      <table cellspacing="0" class="regis_address_local_cfm">
                        <tr >
                          <th>郵便番号</th>
                          <td> 〒{$order.inprec_addrjp_zip1}-{$order.inprec_addrjp_zip2} </td>
                        </tr>
                        <tr>
                          <th>都道府県 </th>
                          <td>{$order.inprec_addrjp_pref}</td>
                        </tr>
                        <tr>
                          <th>市区町村 </th>
                          <td>{$order.inprec_addrjp_city}</td>
                        </tr>
                        <tr>
                          <th> 町域 </th>
                          <td>{$order.inprec_addrjp_area}</td>
                        </tr>
                        <tr>
                          <th>建物名<br/>
                            部屋番号 </th>
                          <td>{$order.inprec_addrjp_building}</td>
                        </tr>
                      </table>
                  {/if}
                  </td>
              </tr>
              <tr>
              	<th>お届け先連絡先（電話）</th>
                <td>{$order.inprec_tel1}</td>
              </tr>
            </table>
          {/if}
          
          <!-- order insite japan-->       
                      
            <!--address send product-->
            <div class="height10"></div>
            <p>■その他の情報</p>
            <table cellspacing="1" class="tbform">
              <tr>
                <th >&nbsp;</th>
                <td>{$order.inp_remark}</td>
              </tr>
            </table>
           
          <!-- form Register-->
           {if $cart_type == 0 || $cart_type == 2}
         	 <form action="https://secure.axes-payments.com/cgi-bin/credit/link-point.cgi" name="gotonext" method="post" >
           		<input type="hidden" name="site_code" value="{$payment_code}"  />
                <input type="hidden" name="amount" value="{$amount}"  />
                <input type="hidden" name="currency" value="{$currency_product}" />
                <input type="hidden" name="telephone_no" value=""  />
                <input type="hidden" name="email" value="{$email}" />
                <input type="hidden" name="order_code" value="{$ordercode}"  />
                <input type="hidden" name="user_id" value="{$ordercode}"  />
                <input type="hidden" name="optional" value="{$optional}" />													
                <input type="hidden" name="success_url" value="http://www.mybus-asia.com/{$config.countryname}/souvenir_payment_action.php?page=payment&transection={$transection}" />
                <input type="hidden" name="failure_url" value="http://www.mybus-asia.com/{$config.countryname}/souvenir_fail.php?transection={$transection}&credit={$credit}" /> 
            </form>
          {else}
           <form action="souvenir_payment_action.php?page=payment{if $transection}&transection={$transection}{/if}" name="gotonext" method="post">
           </form>
          {/if}
          
          <span style="padding:5px 150px 5px 165px; display:block;">         
          <input type="button" class="btn_submit gotoback " value="戻る" style="width:70px;" />
          
          {if $pay_credit}
          <input type="button" class="btn_submit gotonext " value="次へ" style="width:70px;" />
          {else}
          <input type="button" class="btn_submit gotonext " style=" padding-left:10px; padding-right:10px;" value="ご注文完了ページへ"  />
          {/if}
          
          </span> </div>
      </div>
    </div>
    <form action="souvenir_cart_inp_address.php" name="gotoback" method="post">
        {$booking.his.reg_hidden}
    </form>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>