{if $config.country eq "TWN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'TPE' : $tagtittle  = '台北観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの台北観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '台北観光,台北オプショナルツアー,台北現地ツアー,台北ランドパッケージ,マイバス,JTB';             
        break;
        			
        case 'KAO' : $tagtittle  = '高雄観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの高雄観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '高雄観光,高雄オプショナルツアー,高雄現地ツアー,高雄ランドパッケージ,マイバス,JTB';
        break;
        case 'HUA' : $tagtittle  = '花蓮観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの花蓮観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '花蓮観光,花蓮オプショナルツアー,花蓮現地ツアー,花蓮ランドパッケージ,マイバス,JTB';
        break;
        default    : $tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,台湾ランドパッケージ,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,台湾ランドパッケージ,マイバス,JTB'; 
}
{/php}
{elseif $config.country eq "IDN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'BAR' : $tagtittle  = 'バリ観光・オプショナルツアーならJTBマイバスサイト'; 			 
         			 $tagdesc    = 'JTBのバリ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バリ観光,バリオプショナルツアー,バリ現地ツアー,バリランドパッケージ,マイバス,JTB';
        break;
        case 'JAK' : $tagtittle  = 'ジャカルタ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのジャカルタ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'ジャカルタ観光,ジャカルタオプショナルツアー,ジャカルタ現地ツアー,ジャカルタランドパッケージ,マイバス,JTB';
        break;
        default    : $tagtittle  = 'インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのインドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'インドネシア観光,インドネシアオプショナルツアー,インドネシア現地ツアー,インドネシアランドパッケージ,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = 'インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのインドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'インドネシア観光,インドネシアオプショナルツアー,インドネシア現地ツアー,インドネシアランドパッケージ,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "THA"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'BKK' : $tagtittle  = 'バンコク観光・オプショナルツアーならJTBマイバスサイト'; 		
        		     $tagdesc    = 'JTBのバンコク観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バンコク観光,バンコクオプショナルツアー,バンコク現地ツアー,バンコクランドパッケージ,マイバス,JTB';		 
        break;
        case 'PYX' : $tagtittle  = 'パタヤ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのパタヤ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'パタヤ観光,パタヤオプショナルツアー,パタヤ現地ツアー,パタヤランドパッケージ,マイバス,JTB';			     
        break;
        case 'CNX' : $tagtittle  = 'チェンライ・チェンマイ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのチェンライ・チェンマイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'チェンライ・チェンマイ観光,チェンライ・チェンマイオプショナルツアー,チェンライ・チェンマイ現地ツアー,チェンライ・チェンマイランドパッケージ,マイバス,JTB';
        break;
        case 'HKT' : $tagtittle  = 'プーケット観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのプーケット観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'プーケット観光,プーケットオプショナルツアー,プーケット現地ツアー,プーケットランドパッケージ,マイバス,JTB';		 
        break;
        case 'KSM' : $tagtittle  = 'サムイ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのサムイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'サムイ観光,サムイオプショナルツアー,サムイ現地ツアー,サムイランドパッケージ,マイバス,JTB';			 
        break;
        case 'KRB' : $tagtittle  = 'クラビ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクラビ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'クラビ観光,クラビオプショナルツアー,クラビ現地ツアー,クラビランドパッケージ,マイバス,JTB'; 				 
        break;
        default    : $tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,タイランドパッケージ,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,タイランドパッケージ,マイバス,JTB';    
}
{/php}
{elseif $config.country eq "VNM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'HCM' : $tagtittle  = 'ホーチミン観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのホーチミン観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';	
                     $tagkeyword = 'ホーチミン観光,ホーチミンオプショナルツアー,ホーチミン現地ツアー,ホーチミンランドパッケージ,マイバス,JTB';	     
        break;
        case 'HAN' : $tagtittle  = 'ハノイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのハノイ観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = 'ハノイ観光,ハノイオプショナルツアー,ハノイ現地ツアー,ハノイランドパッケージ,マイバス,JTB';				 
        break;
        case 'OTH' : $tagtittle  = '中部ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの中部ベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = '中部ベトナム観光,中部ベトナムオプショナルツアー,中部ベトナム現地ツアー,中部ベトナムランドパッケージ,マイバス,JTB';			 
        break;
        default    : $tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 				 
        			 $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,ベトナムランドパッケージ,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,ベトナムランドパッケージ,マイバス,JTB';
}
{/php}
{elseif $config.country eq "KHM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'REP' : $tagtittle  = 'シェムリアップ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシェムリアップ観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'シェムリアップ観光,シェムリアップオプショナルツアー,シェムリアップ現地ツアー,シェムリアップランドパッケージ,マイバス,JTB';
                     	    
        break;
        default    : $tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
   					 $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,カンボジアランドパッケージ,マイバス,JTB';			
        break;
    }
}
else
{
	$tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,カンボジアランドパッケージ,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "MYS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'KUA' : $tagtittle  = 'クアラルンプール観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのクアラルンプール観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'クアラルンプール観光,クアラルンプールオプショナルツアー,クアラルンプール現地ツアー,クアラルンプールランドパッケージ,マイバス,JTB';
        break;
        case 'PEN' : $tagtittle  = 'ペナン観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのペナン観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ペナン観光,ペナンオプショナルツアー,ペナン現地ツアー,ペナンランドパッケージ,マイバス,JTB'; 	    	
        break;
        case 'LAN' : $tagtittle  = 'ランカウイ観光・オプショナルツアーならJTBマイバスサイト';  	
        			 $tagdesc    = 'JTBのランカウイ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ランカウイ観光,ランカウイオプショナルツアー,ランカウイ現地ツアー,ランカウイランドパッケージ,マイバス,JTB';	    
        break;
        case 'KOT' : $tagtittle  = 'コタキナバル観光・オプショナルツアーならJTBマイバスサイト';  	
        			 $tagdesc    = 'JTBのコタキナバル観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'コタキナバル観光,コタキナバルオプショナルツアー,コタキナバル現地ツアー,コタキナバルランドパッケージ,マイバス,JTB';	    
        break;
        case 'MLC' : $tagtittle  = 'マラッカ観光・オプショナルツアーならJTBマイバスサイト';  	
        			 $tagdesc    = 'JTBのマラッカ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マラッカ観光,マラッカオプショナルツアー,マラッカ現地ツアー,マラッカランドパッケージ,マイバス,JTB';	    	
        break;
        default    : $tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マレーシアランドパッケージ,マイバス,JTB';	
        break;
    }
}
else
{
	$tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マレーシアランドパッケージ,マイバス,JTB';
     
}
{/php}
{elseif $config.country eq "SGP"}
{php}
	$tagtittle  = 'シンガポール観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのシンガポール観光・現地オプショナルツアーブランドである「マイバス」シンガポールの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'シンガポール観光,シンガポールオプショナルツアー,シンガポール現地ツアー,シンガポールランドパッケージ,マイバス,JTB';
    
{/php}
{elseif $config.country eq "HKG"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'MAC' : $tagtittle  = 'マカオ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマカオ観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = 'マカオ観光,マカオオプショナルツアー,現地ツアー,ランドパッケージ,マイバス,JTB';
        break;
        case 'SHZ' : $tagtittle  = '深圳観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの深圳観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '深圳観光,深圳オプショナルツアー,深圳現地ツアー,深圳ランドパッケージ,マイバス,JTB';     
        break;
        case 'GGZ' : $tagtittle  = '広州観光・オプショナルツアーならJTBマイバスサイト';      
        			 $tagdesc    = 'JTBの広州観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '広州観光,広州オプショナルツアー,広州現地ツアー,広州ランドパッケージ,マイバス,JTB';
        break;
        default    : $tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト'; 		
        			 $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,香港ランドパッケージ,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,香港ランドパッケージ,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "AUS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'SYN' : $tagtittle  = 'シドニー観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシドニー観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'シドニー観光,シドニーオプショナルツアー,シドニー現地ツアー,シドニーランドパッケージ,マイバス,JTB';			
        break;
        case 'GOC' : $tagtittle  = 'ゴールドコースト観光・オプショナルツアーならJTBマイバスサイト'; 
       				 $tagdesc    = 'JTBのゴールドコースト観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ゴールドコースト観光,ゴールドコーストオプショナルツアー,ゴールドコースト現地ツアー,ゴールドコーストランドパッケージ,マイバス,JTB';		
        break;
        case 'CAN' : $tagtittle  = 'ケアンズ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのケアンズ観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ケアンズ観光,ケアンズオプショナルツアー,ケアンズ現地ツアー,ケアンズランドパッケージ,マイバス,JTB';			
        break;
        case 'MEL' : $tagtittle  = 'メルボルン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのメルボルン観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'メルボルン観光,メルボルンオプショナルツアー,メルボルン現地ツアー,メルボルンランドパッケージ,マイバス,JTB';	 		
        break;
        case 'PER' : $tagtittle  = 'パース観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのパース観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'パース観光,パースオプショナルツアー,パース現地ツアー,パースランドパッケージ,マイバス,JTB';			
        break;
        case 'AYR' : $tagtittle  = 'エアーズロック観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのエアーズロック観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'エアーズロック観光,エアーズロックオプショナルツアー,エアーズロック現地ツアー,エアーズロックランドパッケージ,マイバス,JTB';		
        break;
        default    : $tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,オーストラリアランドパッケージ,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,オーストラリアランドパッケージ,マイバス,JTB';
}
{/php}
{elseif $config.country eq "NZL"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'AUK' : $tagtittle  = 'オークランド観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのオークランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'オークランド観光,オークランドオプショナルツアー,オークランド現地ツアー,オークランドランドパッケージ,マイバス,JTB';	
        break;
        case 'CHR' : $tagtittle  = 'クライストチャーチ観光・オプショナルツアーならJTBマイバスサイト';  
        			 $tagdesc    = 'JTBのクライストチャーチ観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クライストチャーチ観光,クライストチャーチオプショナルツアー,クライストチャーチ現地ツアー,クライストチャーチランドパッケージ,マイバス,JTB';
        break;
        case 'QUE' : $tagtittle  = 'クイーンズタウン観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのクイーンズタウン観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クイーンズタウン観光,クイーンズタウンオプショナルツアー,クイーンズタウン現地ツアー,クイーンズタウンランドパッケージ,マイバス,JTB';	  
        break;
        case 'RTU' : $tagtittle  = 'ロトルア観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのロトルア観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'ロトルア観光,ロトルアオプショナルツアー,ロトルア現地ツアー,ロトルアランドパッケージ,マイバス,JTB';			  
        break;
        case 'MTC' : $tagtittle  = 'マウントクック観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマウントクック観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'マウントクック観光,マウントクックオプショナルツアー,マウントクック現地ツアー,マウントクックランドパッケージ,マイバス,JTB';
        break;
        default    : $tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,ニュージーランランドパッケージ,マイバス,JT '; 
        break;
    }
}
else
{
	$tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,ニュージーランランドパッケージ,マイバス,JTB';
}
{/php}
{/if}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{if $patetittle eq "product"}{$product.product_name_jp}{else}{php}echo $tagtittle;{/php}{/if}</title>
<meta name="Description" content="{php} echo $tagdesc;{/php}" />
<meta name="Keywords" content="{php} echo $tagkeyword;{/php}" />
<link type="text/css" href="{$config.documentroot}common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>


{if $product_review eq 1}
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.lightbox-0.5.min.js"></script>
<link type="text/css" href="{$config.documentroot}common/jquery.lightbox-0.5.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/start_lightbox.js"></script>
{/if}

</head>
<body>
<!--body-->
<!--header-->
<div class="header"> 
	<a class="logo_text_link" href="../index.php">海外観光・オプショナルツアー・ランドパッケージならJTB</a>
	<a class="mybus-logo" href="../index.php">
    	{if $smarty.now|date_format:'%Y%m%d' >= '20130107'}
   			<img src="{$config.documentroot}images/mybuslogo.jpg" alt="Mybus Logo"  /> 
        {else}
            <img src="{$config.documentroot}images/mybus-logo.jpg" alt="Mybus Logo"  /> 
 			<img src="{$config.documentroot}images/logo.jpg" alt="JTB Logo"  /> 
        {/if}
	</a>
  <div class="country_name" {if $smarty.now|date_format:'%Y%m%d' >= '20130107' }style="left:227px; top:55px;" {/if}>
    {if $config.country eq "TWN"}
    台湾
    {elseif $config.country eq "IDN"}
    インドネシア
    {elseif $config.country eq "THA"}
    タイ
    {elseif $config.country eq "VNM"}
    ベトナム
    {elseif $config.country eq "KHM"}
    カンボジア
    {elseif $config.country eq "MYS"}
    マレーシア
    {elseif $config.country eq "SGP"}
    シンガポール
    {elseif $config.country eq "HKG"}
    香港
    {elseif $config.country eq "AUS"}
    オーストラリア
    {elseif $config.country eq "NZL"}
    ニュージーランド
    {/if}
  </div>
  <!--social share-->   
    <ul class="social_network">
       <li><span class='st_facebook_hcount' displayText='Facebook'></span></li>
       <li><span class='st_twitter_hcount' displayText='Tweet'></span></li>
       <li><a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="a0028392a9c4e9ac68bccb2f09f14f59875140c6" data-button="button-1">Check</a><script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
       </li>
   </ul>
   <!--end social share-->
  <!--link-->
  <div class="header_link">
  	{if $config.basket eq true}
  
  	<a class="ico_cart" href="booking_cart.php">買い物カゴを見る</a>
    {/if}
    <div class="header_link_right"></div>
    <div class="header_link_center">
      <ul>
        <li><a href="../page.php?id=branch.html">会社概要</a></li>
        <li><a href="../page.php?id=sitemap.html">サイトマップ</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="header_link_left"></div>
    <div class="clear"></div>
  </div>
  <!--end link-->
  <!--link2-->
  <div class="header_link2">
    <ul>
      <li><a href="../index.php">JTBアジアパシフィックTOPへ</a>　</li>
      <!--<li><a href="../lp.php">ランドパッケージTOPへ</a></li>-->
      <li><a href="../opt.php">オプショナルツアーTOPへ</a></li>
    </ul>
  </div>
  <!--end link2-->
</div>
<!--header-->
