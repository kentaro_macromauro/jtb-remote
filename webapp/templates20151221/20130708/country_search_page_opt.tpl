{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
   {$breadcamp}
   
    <div class="clear"></div>
    <div class="text-content">
{if $config.country eq "TWN"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'TPE' : echo '<h1 class="font_red">台北観光・オプショナルツアー「マイバス」</h1>'; break; 
    case 'KAO' : echo '<h1 class="font_red">高雄観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'HUA' : echo '<h1 class="font_red">花蓮観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}
{elseif $config.country eq "IDN"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'BAR' : echo '<h1 class="font_red">バリ観光・オプショナルツアー「マイバス」</h1>'; break; 
    case 'JAK' : echo '<h1 class="font_red">ジャカルタ観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}
 
{elseif $config.country eq "THA"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'BKK' : echo '<h1 class="font_red">バンコク観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'PYX' : echo '<h1 class="font_red">パタヤ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'CNX' : echo '<h1 class="font_red">チェンライ・チェンマイ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'HKT' : echo '<h1 class="font_red">プーケット観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'KSM' : echo '<h1 class="font_red">サムイ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'KRB' : echo '<h1 class="font_red">クラビ観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}
{elseif $config.country eq "VNM"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'HCM' : echo '<h1 class="font_red">ホーチミン観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'HAN' : echo '<h1 class="font_red">ハノイ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'OTH' : echo '<h1 class="font_red">中部ベトナム観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}
{elseif $config.country eq "KHM"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'REP' : echo '<h1 class="font_red">シェムリアップ観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}   
{elseif $config.country eq "MYS"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'KUA' : echo '<h1 class="font_red">クアラルンプール観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'PEN' : echo '<h1 class="font_red">ペナン観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'LAN' : echo '<h1 class="font_red">ランカウイ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'KOT' : echo '<h1 class="font_red">コタキナバル観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'MLC' : echo '<h1 class="font_red">マラッカ観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}    
{elseif $config.country eq "HKG"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'MAC' : echo '<h1 class="font_red">マカオ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'SHZ' : echo '<h1 class="font_red">深圳観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'GGZ' : echo '<h1 class="font_red">広州観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}    
{elseif $config.country eq "AUS"}
{php}
switch ($_REQUEST[inp_city])
{	
	case 'SYN' : echo '<h1 class="font_red">シドニー観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'GOC' : echo '<h1 class="font_red">ゴールドコースト観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'CAN' : echo '<h1 class="font_red">ケアンズ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'MEL' : echo '<h1 class="font_red">メルボルン観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'PER' : echo '<h1 class="font_red">パース観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'AYR' : echo '<h1 class="font_red">エアーズロック観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}    
{elseif $config.country eq "NZL"}
{php}
switch ($_REQUEST[inp_city])
{
	case 'AUK' : echo ''; break;
    case 'CHR' : echo '<h1 class="font_red">クライストチャーチ観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'QUE' : echo '<h1 class="font_red">クイーンズタウン観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'RTU' : echo '<h1 class="font_red">ロトルア観光・オプショナルツアー「マイバス」</h1>'; break;
    case 'MTC' : echo '<h1 class="font_red">マウントクック観光・オプショナルツアー「マイバス」</h1>'; break;
}
{/php}
{/if}
    </div>
    <!-- box search -->
    <form action="search_opt.php" method="get" name="frmSearch">
      <div class="box-search-header">検索</div>
      <div class="box-search">
        <ul class="box-search1" >
          <li>
            <p>国名</p>
            {$inp_country} </li>
          <li>
            <p>都市名</p>
            <span id="select_city"> {$inp_city} </span> </li>
          <li>&nbsp;</li>
        </ul>
        <ul class="box-search2" >
          
          
          <li>
            <p>テーマ指定</p>
            {$select_category} </li>
          <li>
            <p>条件検索</p>
            <span id="select_option"> {$select_option} </span></li>  
            
            
          <li>
            <p>キーワード</p>
            <span style="float:left;">{$inp_keyword}</span></li>    
        </ul>
        <ul class="box-search3">
          <li>
          	<p>体験記</p>
            {$select_review}
          </li>
          <li>
            <p>時間帯</p>
            {$select_time}
          <li>
            <p class="p_widthlimit">日にち</p>
            {$select_yearmonth}
            {$select_day} </li>
        </ul>
        <div class="box-search4">
          <input name="inp_search" class="btn_search" type="submit" value="検索" />
        </div>
      </div>
      <!-- box search -->
      
      <div class="height10"></div>
      <!-- search result-->
      <div class="pagi-search-result">
      全{$max_record}件中{$page_start}～ {$page_end}件目を表示しています。
      <div class="btn-order"> {$order_select} </div>
    </form>
  </div>
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <div class="height3"></div>
  {$search_content}
  <div class="height3"></div>
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <!--banner footer-->
</div>
<!--container-left-->
<!--container-right-->
{include file="country_container_right.tpl"}
<!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>