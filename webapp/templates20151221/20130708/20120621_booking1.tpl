﻿{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      
        <div class="formbox">
           <form action="?" method="post">
           
           
           <ul class="booking-nav-process {if $config.allot != 'false'}limit4{/if}" >
            <li id="step1" class="active">希望日選択</li>
            <li id="step2" class="active_first">カート</li>
            
            <li id="step3">予約情報入力</li>
            <li id="step4">予約情報確認</li>
            
            {if $status_book == '3'}
            <li id="step5">お支払い方法の選択</li>
            {/if}
            
            {if $status_book == '1' || $status_book == '3'}
            <li id="step6">決済情報確認</li>
            <li id="step7">決済情報確認</li>
            {/if}
            <li id="step8">完了</li>
           </ul>
          
          
          <div class="clear"></div>
          <span class="texttittle">希望日選択</span>
          <!--product select-->
         
          <div class="booking-view-product">
            <div class="booking-view-product-img"> <img src="{$booking.product.img}" width="120" /> </div>
            <div class="booking-view-product-text"> <strong>{$booking.product.name_jp}</strong>
              <p>{$booking.product.description}</p>
              <input type="hidden" name="product_id" value="{$booking.product.id}" />
            </div>
            <div class="clear"></div>
          </div>
          <!--product select-->
          <div class="height10"></div>
          {if $error.product != ""}
          	  <p class="text-small-red">{$error.product}</p>        
          {/if}
          
          <p>申込可能日　（催行日  {$booking.product.day_cutoff}日　前まで）</p>
          <table cellspacing="1" class="tbform obj_select_datepicker">
          	<tr>
            <td>ご利用日 {$booking.input.select_yearmonth}  {$booking.input.select_day}
            
                
                
                <input type="hidden" name="action" value="{$booking.action}" />
                <input type="submit" class="btn_submit" name="confimation" value="確認" /> <span class="text-small-red conf-text" >&nbsp;</span>
                
                <p class="text-small-red">{$error.date}</p>
                
                </td>
            </tr>
          </table>   
          
          </form>
          {if $booking.page eq '1'}
         <form action="?" method="post"> 
         	
         
          <input type="hidden" name="currency_rate" value="{$booking.other.currency_rate}" />
          <div class="height10"></div>
          参加人数&nbsp;&nbsp;&nbsp;( 最少催行人員 : {$booking.product.pax_min}名様 )
          <table cellspacing="1" class="tbform">
            {if $booking.product.price_adult == '0'}
              <tr style="display:none;">
              	<th></th>
                <td>
                	<select type="hidden" name="inp_adult">
                    	<option value="0">0</option>
                    </select>
                </td>
              </tr>
            {else}
            <tr>
              <th>大人：</th>
              <td>{$booking.input.adult}{if $booking.product.price_adult == 1 }
              								  ：<span class="country">{$currency.sign} -</span>
                                            {if $booking.other.show_rate eq 1}（約 <span class="yen">-</span> 円）{/if}
                                        {else}：<span class="country">{$currency.sign} {$print.price_adult}</span>
                                        	{if $booking.other.show_rate eq 1}（約 <span class="yen">{$print.price_adult_yen}</span> 円）{/if}
                                        {/if}
              <input type="hidden" name="curcode" value="{$currency.sign}" />
              <input type="hidden" name="inp_price_adult" value="{$booking.product.price_adult}" /></td>
            </tr>
            {/if}
            {if $booking.product.price_child == '0' }
            	<tr style="display:none;"><th>
                	</th>
                	<td>
                    	<select type="hidden" name="inp_child">
                        	<option value="0">0</option>
                        </select>
                    </td>    
                </tr>	
            {else}
            
            
            <tr>
              <th>子供{if $age.child_menu eq 1}（{$age.child_from}～{$age.child_to} 歳）{/if}：</th>
              <td>{$booking.input.child}{if $booking.product.price_child == 1}
              								  ：<span class="country">{$currency.sign} -</span>
                                            {if $booking.other.show_rate eq 1}（約 <span class="yen">-</span> 円）{/if}
              							{else}：<span class="country">{$currency.sign} {$print.price_child}</span>
              								{if $booking.other.show_rate eq 1}（約 <span class="yen">{$print.price_child_yen}</span> 円）{/if}
              							{/if}
              <input type="hidden" name="inp_price_child" value="{$booking.product.price_child}" /></td>
            </tr>
            
            {/if}
            
            
            
            <tr>
              <th>幼児{if $age.infant_menu eq 1}（0～{$age.infant} 歳）{/if}：</th>
              <td>{$booking.input.infant}{if $booking.product.price_infant == 1 || $booking.product.price_infant == 1 }{else}：<span class="country">{$currency.sign} {$print.price_infant}</span>
              {if $booking.other.show_rate eq 1}（約 <span class="yen">{$print.price_infant_yen}</span> 円）{/if}
              {/if}
              <input type="hidden" name="inp_price_infant" value="{$booking.product.price_infant}" /></td>
            </tr>
            <tr {if $booking.product.price_infant == 1 }style="display:none;"{/if}  >
              <th>&nbsp;</th>
              <td id="price_amount">{if $booking.product.price_adult == 1 || $booking.product.price_adult == 1 }
              							小計：{$currency.sign} <span class="sgd">{$print.price_amount_sgd}</span>
              						{else}小計：{$currency.sign} <span class="sgd">{$print.price_amount_sgd}</span>
                                    	{if $booking.other.show_rate eq 1} （約 <span class="yen">{$print.price_amount_yen}</span> 円）{/if}
                                    {/if}<p class="text-small-red">{$error.number}</p>
              	<input type="hidden" name="mode" value="basket" />
              </td>
            </tr>
          </table>
          
          
          
          
           
                   
          	<span style="padding:5px 150px 5px 165px; display:block;">
          	  <input type="hidden" name="action" value="cart" />    
              <input type="hidden" name="allot"  value="{$booking.product.allot}" />
      	      <input type="hidden" name="inp_price_adult"  value="{$booking.product.price_adult}" />
              <input type="hidden" name="inp_price_child"  value="{$booking.product.price_child}" />
              <input type="hidden" name="inp_price_infant" value="{$booking.product.price_infant}" />
              
              <input type="hidden" name="inp_yearmonth" value="{$booking.product.product_yearmonth}"  />
              <input type="hidden" name="inp_day"       value="{$booking.product.product_day}" />
              <input type="hidden" name="product_id"    value="{$booking.product.id}" />
              <input type="hidden" name="cfm_code"      value="{$booking.product.cfm_code}" />
              
              <input type="submit" class="btn_submit" value="次へ" style="width:70px;" />
              
            </span> 
           
           
          </form>
          {/if}
          
          </div>
      
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>