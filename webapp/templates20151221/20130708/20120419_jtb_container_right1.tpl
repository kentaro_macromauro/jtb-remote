<div class="container-right">
    <div class="container-right-tittle">
      <p class=" optional_tour_ranking_head">オプショナルツアー </p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="{$opt_top5_link_1}"><img src="{$opt_top5_pic_1}" alt="" class="right-list-product-img" width="112" height="46"  /></a>      
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_1}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_1}">{$opt_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_1}</p>
          {$opt_top5_price_1}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="{$opt_top5_link_2}"><img src="{$opt_top5_pic_2}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_2}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_2}">{$opt_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_2}</p>
          {$opt_top5_price_2}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="{$opt_top5_link_3}"><img src="{$opt_top5_pic_3}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_3}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_3}">{$opt_top5_name_3}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_3}</p>
          {$opt_top5_price_3}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop4">
        <a href="{$opt_top5_link_4}"><img src="{$opt_top5_pic_4}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_4}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_4}">{$opt_top5_name_4}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_4}</p>
          {$opt_top5_price_4}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li > 
      	<div class="right-list-product-subject icontop5">
        <a href="{$opt_top5_link_5}"><img src="{$opt_top5_pic_5}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$opt_top5_country_5}</div>
        <div class="right-list-product-tittle"><a href="{$opt_top5_link_5}">{$opt_top5_name_5}</a></div>
        <div class="right-list-product-txt">
          <p >{$opt_top5_detail_5}</p>
          {$opt_top5_price_5}
        </div>
        <div class="clear"></div>
        
      </li>
    </ul>
    <div class="container-right-tittle">
      <p class=" land_package_ranking_head">ランドパッケージ</p>
    </div>
    <ul class="container-right-list">
      <li class="ptop10"> 
      	<div class="right-list-product-subject icontop1">
        <a href="{$lp_top5_link_1}"><img src="{$lp_top5_pic_1}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$lp_top5_country_1}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_1}">{$lp_top5_name_1}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_1}</p>
          {$lp_top5_price_1}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop2">
        <a href="{$lp_top5_link_2}"><img src="{$lp_top5_pic_2}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$lp_top5_country_2}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_2}">{$lp_top5_name_2}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_2}</p>
          {$lp_top5_price_2}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop3">
        <a href="{$lp_top5_link_3}"><img src="{$lp_top5_pic_3}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$lp_top5_country_3}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_3}">{$lp_top5_name_3}</a></div>        
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_3}</p>
          {$lp_top5_price_3}
        </div>
        <div class="clear"></div>
         <div class="right-list-rank-border"></div>
      </li>
      <li> 
      	<div class="right-list-product-subject icontop4">
        <a href="{$lp_top5_link_4}"><img src="{$lp_top5_pic_4}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$lp_top5_country_4}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_4}">{$lp_top5_name_4}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_4}</p>
          {$lp_top5_price_4}
        </div>
        <div class="clear"></div>
        <div class="right-list-rank-border"></div>
      </li>
      <li > 
      	<div class="right-list-product-subject icontop5">
        <a href="{$lp_top5_link_5}"><img src="{$lp_top5_pic_5}" alt="" class="right-list-product-img" width="112" height="46" /></a>
        </div>
        <div class="right-list-product-txt">{$lp_top5_country_5}</div>
        <div class="right-list-product-tittle"><a href="{$lp_top5_link_5}">{$lp_top5_name_5}</a></div>
        <div class="right-list-product-txt">
          <p >{$lp_top5_detail_5}</p>
          {$lp_top5_price_5}<br/>
        </div>
        <div class="clear"></div>
      </li>
    </ul>
    
  </div>
  <div class="clear"></div>