{include file="country_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <div class="clear  "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <h1 class="header-text-tittle">{$product.souvenir_name}</h1>
    <div class="souvenir_product_image"> 
		
        <div class="souvenir_product_main">
        	<div class="souvenir_product_show">
            	{$souvenir_product_img1}
            </div>
            
            <ul class="souvenir_product_list">
            	{$souvenir_product_image_list}
            </ul>        
        </div>      
        
     	<div class="clear"></div>
     	<div class="souvenir_theme">{$souvenir_product_theme}</div>
     </div>
         
    
    <div class="product-information">
      <div class="souvenir-info"> 
      	<div class="height10"></div>
        
        {if $product.text_top != ""}
        <p>{$product.text_top}</p>
        {/if}
        
        
         {if $product.souvenir_price != ""}
        <p class="souvenir_price"><strong>価格：{$product.souvenir_show_price}</strong></p>
      	{/if}
        
        {if $product.affiliate}
         <p style="color:#C41010;">※この商品はJTB商事が運営する「世界のおみやげ屋さん」からご購入いただきます。購入ボタンをクリックすると「世界の<br/>
おみやげ屋さん」に接続されます。</p> 
		{/if}
        
        
        {if $product.text_table != ""}
        
          <table border="0" cellpadding="0" cellspacing="0" class="product-souvenir-tb">
          	
           { $product.text_table}
            
          </table>
        
        {/if}
        
        {if $product.text_bottom != ""}<p>{$product.text_bottom}</p>{/if}
       
        
      <div class="clear"></div>
      {if $product.template_id == 3}
      <div class="product_text">{$product.text_top}</div>
      
      <div class="height10"></div>
      <div class="clear"></div>
      
      
      {/if}
      
      {if $product.remark != ""}
       <p>{$product.remark}</p>
      {/if}
      
     
    
    </div>
      <div class="clear"></div>
    </div>
    
   
    
    <div class="height10"></div>
   
    <div class="height10"></div>
    <a href="{$product.submiturl}" {if $product.affiliate}target="_blank"{/if} class="souvenir-btn-booking">Booking</a>
    <div class="height10"></div>
    
    <!--banner footer-->
    <!--banner footer-->
     
  </div>
  <!--container-left-->
  <!--container-right-->
  
  {include file="country_container_right_souvenir.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
{literal}
<script type="text/javascript"> $(document).ready( function(){$('.souvenir_product_list li').click( function(){ _src = $(this).find('img').attr('src');$('.souvenir_product_show').find('img').fadeOut( function(){ $(this).attr('src',_src ); } ).fadeIn();} ); } ); </script>
{/literal}
</body></html>