<div class="bg_footer">
  <!--FOOTER CONTAINER-->
  <div class="footer_container">
    <div class="footer_link">
      <ul>
        <li><a href="#">会社概要</a></li>
        <li><a href="#">お問合せ</a></li>
        <li><a href="#">旅行約款</a></li>
        <li><a href="#">個人情報保護方針</a></li>
      </ul>
    </div>
    <div class="copyrights">Copyright &copy; 2012 My Bus. All rights reserved </div>
    <div class="clear"></div>
  </div>
  <!--END FOOTER CONTAINER-->
</div>