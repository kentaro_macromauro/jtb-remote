{literal}
<script>
    var product_data = {/literal}{$order_info}{literal};
    dataLayer = product_data;
</script>
{/literal}
{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
     <div class="formbox">
           
          {if $credit eq "1"}
          <div class="souvenir_step4"></div>
          {else}
          <div class="souvenir_none_credit_step4"></div>
 		  {/if}         
          
          
          <div class="clear"></div>
			<div class="height10"></div>
            
            
            <span class="texttittle"> 完了 </span>
			
                           
            {if $credit eq "1"}
                              
                  
            <br/> <p>ご購入ありがとうございました。<br/>
商品お引渡しまで今しばらくお待ちくださいませ。<br/><br/><br/>


			<a href="./">ページTOPに戻る</a>
            </p>                  
            
            {else}
            <br/> <p>ご注文ありがとうございました。<br/><br/>
            【ご注意】<br/>
          	 この商品のご購入はまだ確定しておりません。<br/>
            今しばらくお待ちくださいませ。
            <br/><br/><br/>


			<a href="./">ページTOPに戻る</a>
            </p> 
            {/if}
            
            
        	
          
          </div>
      
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
</div>
<div class="clear"></div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>