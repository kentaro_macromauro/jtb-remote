{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <div class="header-slider">
  	<img src="public/images/index.php?root=country&amp;width=967&amp;name=001.jpg" alt="" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=002.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=003.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=004.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=005.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=006.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=007.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=008.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=009.jpg" alt=""  class="hide" />
    <img src="public/images/index.php?root=country&amp;width=967&amp;name=010.jpg" alt="" class="hide" />
  </div>
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
  	{$breadcamp}
    <div class="text-content">
      <p> JTBが自信を持ってご提供する「ランドパッケージ」のアジア・オセアニア専門販売サイトです。<br/>
「ランドパッケージ」とは飛行機なしの、宿泊付き旅行パッケージ商品のことです。<br/>
LCC（ローコストキャリア）やマイルでのご旅行を予定されているお客様に便利な商品です。<br/>
JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。 </p>
    </div>
    <form action="search_lp.php">
    <div class="serach-menu-left">
      <div class="serach-menu-left-tittle">検索</div>
      
      <div class="search-menu-subject1">目的地</div>
      {$inp_country}
      <div class="height5"></div>
      <span id="select_city">
     <select class="search-left-selectbox " name="inp_city">
     	<option value="0">-------Please Select-------</option>
      </select> 
      </span>
      <div class="height5"></div>
      <div class="search-menu-subject1">日にち </div>
      <div class="search-center">
      {$select_yearmonth}
      {$select_day}
      </div>
      <div class="p_ddpicker" style="padding-left:12px; margin-top:5px;">
      
        <span id="id_cal">カレンダーから選ぶ</span>
        
      </div>
      <div class="height5"></div>
      <p>カテゴリー</p>
      <select class="search-left-selectbox" name="inp_category">
        <option value="0">---Please Select---</option>
        {$select_category}
      </select>
      <div class="height5"></div>
      <p>条件検索</p>
      <select class="search-left-selectbox" name="inp_option">
      	<option value="0">---Please Select---</option>
        {$select_option}
      </select>
      
      
      <div class="height5"></div>
      <p>旅行日数</p>
      <select class="search-left-selectbox" name="inp_time">
        <option value="0">---Please Select---</option>
        {$select_time}
      </select>
      <div class="height5"></div>
      <div class="search-menu-subject1">フリーワード検索</div>
      <input type="text" class="search_left_inp" name="inp_keyword" value="" />
      <div class="height10"></div>
      <input type="submit" value="検索" class="search-left-button" />
      <div class="height5"></div>
      <div class="search-bg-footer"></div>
    	</div>
    </form>
    <div class="container-wsearch-left">
      <div class="container-wsearch-tittle">おすすめツアー</div>
      <div class="content">
        <div class="slider-box-top"></div>
        <div class="slider-content">
          <ul class="slider-content-mid">
            {$productslider}
          </ul>
        </div>
        <div class="slider-box-bottom"></div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="height10"></div>
    <!--banner-navigation-->
    <div class="banner-navigation">
   	<div class="banner-navigation-main">
    	{if $banner_name1 != ""}
    	<a href="{$banner_link1}" title="menu1" ><img src="{$banner_img1}" alt="menu1" height="168" width="767"  /></a>
        {/if}
        {if $banner_name2 != ""}
        <a href="{$banner_link2}" title="menu2" class="hide"><img src="{$banner_img2}" alt="menu2" height="168"  width="767" /></a>
        {/if}
        {if $banner_name3 != ""}
        <a href="{$banner_link3}" title="menu3" class="hide"><img src="{$banner_img3}" alt="menu3" height="168" width="767" /></a>
        {/if}
    </div>
   	<ul class="banner-navigation-btn">
      {if $banner_name1 != ""}
   	  <li class="menu1 p-right3 active" id="banner-nav1">{$banner_name1}</li>
      {/if}
      {if $banner_name2 != ""}
      <li class="menu2 p-right3"  id="banner-nav2">{$banner_name2}</li>
      {/if}
      {if $banner_name3 != ""}
      <li class="menu3"  id="banner-nav3">{$banner_name3}</li>
      {/if}
    </ul>
    </div>
    <div class="clear"></div>
    <!--banner-navigation-->
    <div class="height10"></div>
    <div class="border-b4"></div>
<div class="height10"></div> 
    <!--category-->
    <div class="category2">
    	<div class="category_box">
        	<div class="category_tittle">Sale!</div>
            <div class="category_image">
            <img src="{$catimg1}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg1}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category1}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_option=1" >「Sale!」検索結果一覧へ</a></div>
        </div>
     </div>   
     <div class="category2">    
        <div class="category_box">
        	<div class="category_tittle">観光充実</div>
            <div class="category_image">
            <img src="{$catimg4}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg4}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category4}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_option=4" >「観光充実」検索結果一覧へ</a></div>
        </div>
      </div> 
      <div class="category2"> 
        <div class="category_box">
        	<div class="category_tittle">新着</div>
            <div class="category_image">
            <img src="{$catimg3}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg3}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	{$category3}
            </ul>
            <div class="category_link"><a href="search_lp.php?inp_option=2"  >「新着」検索結果一覧へ</a></div>
        </div>  
    </div>
   	<div class="category2">     
        <div class="category_box">
        	<div class="category_tittle">こだわりの旅</div>
            <div class="category_image">
            <img src="{$catimg2}" alt="" width="230" height="94" class="org_img" />
            <img src="{$catimg2}" alt="" width="230" height="94" class="hide now" /></div>
            <ul class="category_text">
            	<!--<li><a href="#"> test link1 </a></li>
                <li><a href="#"> test link2 </a></li>
                <li><a href="#"> test link3 </a></li>-->
                {$category2}
            </ul>
             <!--<div class="category_product_detail">
               <p class="tittle"><a href="#">キストテキストキストトトテキストテキス</a></p>
               <p>
テキストテキストテキストキテキストテキストテキストキストテテキストテキストテキストキストテストテキストテキストテキストテキスキストテキストテキストテキ...</p>
				<p class="price">100 円 ～ 200 円</p>
            </div>-->         	
        </div>
        <div class="category_link"><a href="search_lp.php?inp_option=3"  >遊「こだわりの旅」検索結果一覧へ</a></div>
    </div>
    <!--category-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="jtb_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>