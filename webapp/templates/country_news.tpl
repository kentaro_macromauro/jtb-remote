{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <!--news-->
    
    
    <div class="news">
      <div class="news_header"></div>
      <div class="news_middle">
        {foreach from=$news item=items}
            <div class="news_box">
                <div class="news_icon">
                    {if $items.icon eq "1"}
                    <img src="{$config.documentroot}images/icon_new.gif" alt="new" />
                    {/if}
                </div>
                <div class="news_date">{$items.date}</div> 
                <div class="news_title">
                    <a href="news_detail.php?id={$items.id}">{$items.subject}</a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="news_separate"></div>
        {/foreach}
      </div>
      <div class="news_bottom"></div>
    </div>
    <!--end news-->
    
    <!--pagination start-->
    
    {$pagination}
	<div class="clear"></div>    
    <!--pagination end-->
    
  </div>
  <!--container-left-->
    <div class="clear"></div>

</div>
<!--container-->
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>