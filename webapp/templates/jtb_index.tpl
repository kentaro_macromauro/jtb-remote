{include file="jtb_header.tpl" }
<!--container-->
<div class="container home-content pull-container" style="padding-top: 0px;">
<div class="mybus-breadcrumb" style="padding: 0 20px 0 10px; font-family: 'Arial', sans-serif;">
    {$breadcamp}
    <div style="height: 5px;"></div>
</div>

<div class="row">
    {include file="jtb_search_box.tpl" }

    <div class="col-md-8 col-sm-7">
        <div id="wrapper">
            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                    {$top_banner_img}
                </div>
            </div>
        </div>

    </div>
</div>
<div style="height: 5px;"></div>
<div style="padding: 0 5px 0 5px;">

    <div class="grey-tittle-hide">
        <div style="height: 15px;"></div>
        <div class="row">
            <div class="col-md-12" style="">
                <div class="grey-tittle-before">
                    <p class="grey-tittle-after" style="font-weight: 700;">国一覧</p>
                </div>
            </div>
        </div>

        <div style="height: 16px;"></div>
    </div>


    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1" >
            <a href="singapore/"><img src="{$config.documentroot}images/top/singapore_top.jpg" class="img-responsive" alt="okinawa"></a>
            <div class="flying-text-md"><p style="">シンガポール</p></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2" >
            <a href="taiwan/"><img src="{$config.documentroot}images/top/taiwan_top.jpg" class="img-responsive" alt="fukoka"></a>
            <div class="flying-text-md"><p style="">台湾</p></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1 push-thumbnail" >
            <a href="thailand/"><img src="{$config.documentroot}images/top/thailand_top.jpg" class="img-responsive" alt="yokohama"></a>
            <div class="flying-text-md"><p style="">タイ</p></div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2 push-thumbnail" >
            <a href="indonesia/"><img src="{$config.documentroot}images/top/indonesia_top.jpg" class="img-responsive" alt="kyoto"></a>
            <div class="flying-text-md"><p style="">バリ島／インドネシア</p></div>
        </div>
    </div>

    <!-- THUMBNAIL KE DUA -->
    <div style="height: 16px;"></div>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1" >
            <a href="australia/"><img src="{$config.documentroot}images/top/australia_top.jpg" class="img-responsive" alt="okinawa"></a>
            <div class="flying-text-md"><p style="">オーストラリア</p></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2" >
            <a href="newzealand/"><img src="{$config.documentroot}images/top/newzealand_top.jpg" class="img-responsive" alt="fukoka"></a>
            <div class="flying-text-md"><p style="">ニュージーランド</p></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1 push-thumbnail" >
            <a href="malaysia/"><img src="{$config.documentroot}images/top/malaysia_top.jpg" class="img-responsive" alt="yokohama"></a>
            <div class="flying-text-md"><p style="">マレーシア</p></div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2 push-thumbnail" >
            <a href="vietnam/"><img src="{$config.documentroot}images/top/vietnam_top.jpg" class="img-responsive" alt="kyoto"></a>
            <div class="flying-text-md"><p style="">ベトナム</p></div>
        </div>
    </div>


    <!-- THUMBNAIL KE TIGA -->
    <div style="height: 16px;"></div>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1" >
            <a href="cambodia/"><img src="{$config.documentroot}images/top/cambodia_top.jpg" class="img-responsive" alt="okinawa"></a>
            <div class="flying-text-md"><p style="">カンボジア</p></div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2" >
            <a href="philippines/"><img src="{$config.documentroot}images/top/philippines_top.jpg" class="img-responsive" alt="kyoto"></a>
            <div class="flying-text-md"><p style="">フィリピン</p></div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img1 push-thumbnail" >
            <a href="myanmar/"><img src="{$config.documentroot}images/top/myanmar_top.jpg" class="img-responsive" alt="yokohama"></a>
            <div class="flying-text-md"><p style="">ミャンマー</p></div>
        </div>

        <!--
        <div class="col-md-3 col-sm-3 col-xs-6 bttm-last-img2" >
            <a href="#"><img src="{$config.documentroot}images/top/10.jpg" class="img-responsive" alt="fukoka"></a>
            <div class="flying-text-md"><p style="">インド</p></div>
        </div>

-->
    </div>

</div>


<div class="gotop">
    <div style="height: 35px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png" alt="gotop">&nbsp;<a href="#top" style="text-decoration: underline; color: #847973;">ページTOP</a></p>
</div>

</div><!-- end of main content -->

<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->

<!--footer script-->
{include file="jtb_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>