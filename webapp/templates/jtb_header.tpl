<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<title>
{php}
switch ($_SERVER[REQUEST_URI])
{
    case '/' : echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
    case '/souvenir.php' :  echo '海外おみやげならJTBマイバスサイト';   break;
    default: echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
}
{/php}
</title>
<meta name="Description" content="{php}
switch ($_SERVER[REQUEST_URI])
{
	case '/' : echo 'JTBの海外観光・現地オプショナルツアーブランドである「マイバス」のアジア・オセアニア専門販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
    case '/souvenir.php': echo 'アジア・パシフィックの海外おみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
    default : echo 'JTBの海外観光・現地オプショナルツアーブランドである「マイバス」のアジア・オセアニア専門販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
}
{/php}" />
<meta name="Keywords" content="{php}
switch ($_SERVER[REQUEST_URI])
{
	case '/' : echo '海外観光,オプショナルツアー,現地ツアー,マイバス,JTB'; break;
    case '/souvenir.php' : echo '海外おみやげ,アジアおみやげ,マイバス,JTB' ; break;
    default : echo '海外観光,オプショナルツアー,現地ツアー,マイバス,JTB'; break;
}
{/php}" />
    <link rel="shortcut icon" href="/ico/favicon.ico" />
    <link rel="apple-touch-icon" href="http://www.mybus-asia.com/apple-touch-icon.png" />

    <link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict(true);
    </script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>

<link rel="stylesheet" href="{$config.documentroot}common/css/bootstrap2.css" />
<link rel="stylesheet" href="{$config.documentroot}common/css/style-secondary.css" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Arimo|Signika:400,600' rel='stylesheet' type='text/css'>


<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/nivo-slider.css" type="text/css" media="screen" />

<link href='//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.min.css' rel='stylesheet'/>

<!--
<link type="text/css" href="{$config.documentroot}common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>
-->
    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
        <!-- End Google Tag Manager -->
    {/literal}
</head>
<body>
<header  class="ha-header">
<div class="container head-desktop">
    <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2 logo-top">
            <div class="navbar-brand">
                <a  href="./" class="desktops">
                    <img src="{$config.documentroot}images/logo/mybuslogo.jpg" class="img-responsive" alt="Responsive image"/>
                </a>
            </div>
        </div>
        <div class="col-md-5 col-sm-3 col-xs-2 padding-cn">
        </div>

        <div class="col-md-5 col-sm-7 col-xs-12">
            <div class="row social-top">
                <div class="col-md-9 col-sm-9 col-xs-12 button-sosmed">
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <div align="right" style="margin-right: -15px;"><img src="{$config.documentroot}images/logo/jtb-perfect-moment.jpg" class="img-responsive" alt="Responsive image" style="margin-top: 2px;"/></div>
                        </div>
                        <!--
                        <div class="col-md-3 col-sm-3">
                            <img src="images/logo/logo-notext.png" class="img-responsive" alt="Responsive image" style="margin-top: 5px;"/>
                        </div>
                        <div class="col-md-6 col-sm-6">
                        <p class="welcome-text" style="color: #777777; font-weight: 700;  text-align: left; line-height: 1em; margin-top: 29px; font-style: italic;">Perfect moments, always</p>
                        </div>
                        -->
                        <div class="col-md-3 col-sm-3">
                            <p style="padding-top: 23px; padding-right: 10px;"><a href="page.php?id=branch.html" style="font-size: 10px; text-align: right; color: #3212f3;">会社情報</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 button-sosmed">
                    <div class="round-social-color">
                        <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank" target="_blank"><span>Facebook</span></a></span>
                        <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Twitter</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="header-second">
<div class="container grey-bg">
<div class="row">
<div id="ribon-red-desktop-sps2"></div>

<div class="row">
    <div class="col-xs-12 menus">
        <div class="row head-mobile">
            <div id="ribon-red-desktop-sps"></div>

            <div class="head-mobile-inner">
                <div class="col-xs-12">
                    <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
                </div>

                <div class="row">
                    <div class="col-xs-3" style="padding: 0px; margin-left: 0px;">
                        <a  href="{$config.documentroot}{$config.environment}"><img src="{$config.documentroot}images/logo/mybuslogo.jpg" class="img-responsive" alt="Responsive image"/></a>
                    </div>

                    <div class="col-xs-2" style="padding-top: 0px; padding-right: 0px;">
                    </div>

                    <div class="col-xs-5" style="padding-left: 15px; padding-right: 0px; padding-top: 7px; padding-bottom: 4px;">
                        <div class="row">

                            <div class="col-xs-12">
                                <div align="right"><img src="{$config.documentroot}images/logo/jtb-perfect-moment.jpg" class="img-responsive" alt="Responsive image" style="margin-top: 5px;"/></div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xs-2" style="padding-top: 0px; padding-left: 5px;">

                        <button class="btn search-mobile2 collapsed" data-toggle="collapse" aria-expanded="true" data-target="#mobilesearch" aria-expanded="false" aria-controls="navbar">
                            <div><span class="fa-white fa-search"></span></div>
                        </button>
                    </div>
                </div>
            </div>

            <div id="ribon-red-desktop-sps"></div>
        </div>
        <!-- Mobile Search -->
        <div class="row mobilesearch">

            <div id="mobilesearch" class="mybus-search-mobile navbar-collapse collapse" style="margin-top: 0px; text-align: left;">
                <div class="row">
                    <div class="col-md-12" style="">
                        <div class="search-header-before">
                            <p class="search-header" style="font-weight: 700;">オプショナルツアー検索</p>
                        </div>
                    </div>
                </div>
                <div class="mybus-search-inner">
                    <form action="search_opt.php" name="frmSearch2" onSubmit="return search_check2()">
                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">国名</div>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_country" style="padding-left: 0px;">
                            {$inp_country}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">都市名</div>
                        </div>
                        <div id="" class="select_city col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">
                            {$inp_city}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">テーマ</div>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_category" style="padding-left: 0px;">
                            <select class="form-control" name="inp_category">
                                <option value="0">-Please Select-</option>
                                {$select_category}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">条件検索</div>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_option" style="padding-left: 0px;">
                            <select name="inp_option" class="form-control">
                                <option value="0">-Please Select-</option>
                                {$select_option}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">時間帯</div>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_time" style="padding-left: 0px;">
                            <select name="inp_time" class="form-control">
                                <option value="0" selected>-Please Select-</option>
                                {$select_time}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                            <div class="caption-form">フリーワード</div>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">
                            <input class="form-control" type="text" name="inp_keyword" />
                        </div>
                    </div>
                    <div style="height: 15px;"></div>
                    <input type="submit" value="検索する" class="btn btn-blue-search" style="width: 100%;"/>
                    <div style="height: 5px;"></div>
                </div>
                </form>
            </div>
        </div>
        <!-- End of Mobile Search -->

    </div>
</div>

<div class="col-sm-12 col-xs-12 menus" style="">
    <!--<div class="navbar-header">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right" style="font-size:12px">
                <li><a href="index.html" style="border-left: 1px solid #c9c9ca; background: #d8d8da;"><img src="images/menu/red-btn3-noround.png"> &nbsp;TOP</a></li>
                <li class="active"><a href="#" style=""><img src="images/menu/red-btn3-white.png"> &nbsp;オプショナルツアー</a></li>
                <li><a href="#"><img src="images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                <li><a href="#"><img src="images/menu/red-btn3-noround.png"> &nbsp;レストラン</a></li>
                <li><a href="#"><img src="images/menu/red-btn3-noround.png"> &nbsp;みやげ</a></li>
                <li><a class="menu_items" href="local-information.html"><img src="images/menu/red-btn3-noround.png" > &nbsp;現地情報</a></li>
                </ul>
            </div>
       </div>-->
</div>
</div><!-- rownya -->

</div>
</div>
</header>

