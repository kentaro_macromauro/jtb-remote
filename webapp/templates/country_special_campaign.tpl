{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  <div class="header-slider">
  	<img src="images/country/001.jpg" alt="" />
    <img src="images/country/002.jpg" alt=""  class="hide" />
    <img src="images/country/003.jpg" alt=""  class="hide" />
    <img src="images/country/004.jpg" alt=""  class="hide" />
    <img src="images/country/005.jpg" alt=""  class="hide" />
    <img src="images/country/006.jpg" alt=""  class="hide" />
    <img src="images/country/007.jpg" alt=""  class="hide" />
    <img src="images/country/008.jpg" alt=""  class="hide" />
    <img src="images/country/009.jpg" alt=""  class="hide" />
    <img src="images/country/010.jpg" alt="" class="hide" />
    {if $config.country eq "IDN"} <img src="images/country/011.jpg" alt=""  class="hide" />{/if}
  </div>
  
  <!--container-left-->
  <div class="container-left">
    {$breadcamp}
    
    <!--content detail-->
     <div class="header-text-tittle">キャンペーン・特集ページ</div>
      <div class="height10"></div>
      <div class="height5"></div>
     <!--
      <ul class="campaign_tab">
  		<li id="campaign_tab1" class="active">&nbsp;</li>
    	<li id="campaign_tab2" >&nbsp;</li>
      </ul>
      -->
      <div class="clear"></div>
      <div class="height5"></div>
      <div class="text-content">
      	JTBがお届けする、MyBusブランドのオプショナルツアー キャンペーン・特集
      </div>
      <div  class="height20"></div>
      
      <ul class="campaign_list1">   
      {$banner_opt}
      </ul>
      
      <ul class="campaign_list2 hide"> 
      {$banner_pkg}
      </ul>
      
    <!--content detail-->
  </div>
  <!--container-left-->
  <!--container-right-->
    <div class="clear"></div>
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--body-->
</body>
</html>