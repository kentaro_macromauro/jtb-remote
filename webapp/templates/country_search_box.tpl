<div class="col-md-4 col-sm-5" style="padding: 0 15px 0 5px;">
    <div class="mybus-search">
        <div class="row">
            <div class="col-md-12" style="">
                <div class="search-header-before">
                    <p class="search-header" style="font-weight: 700;">オプショナルツアー検索</p>
                </div>
            </div>
        </div>
        <div class="mybus-search-inner">
            <form action="search_opt.php" name="frmSearch1" onSubmit="return search_check1()">
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">国名</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_country" style="padding-left: 0px;">
                    {$inp_country}
                </div>
            </div>
            <div style="height: 7px;"></div>
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">都市名</div>
                </div>
                <div class="select_city col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">
                    {$inp_city}
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">テーマ</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_category" style="padding-left: 0px;">
                    <select class="form-control" name="inp_category">
                        <option value="0">-Please Select-</option>
                        {$select_category}
                    </select>
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">条件検索</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_option" style="padding-left: 0px;">
                    <select name="inp_option" class="form-control">
                        <option value="0">-Please Select-</option>
                        {$select_option}
                    </select>
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">時間帯</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_time" style="padding-left: 0px;">
                    <select name="inp_time" class="form-control">
                        <option value="" selected>-Please Select-</option>
                        {$select_time}
                    </select>

                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">フリーワード</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">

                    <input class="form-control" type="text" name="inp_keyword" />

                </div>
            </div>
            <div style="height: 15px;"></div>
            <input type="submit" value="検索する" class="btn btn-blue-search opt-search" style="width: 100%;"/>
            </form>
            <div style="height: 5px;"></div>
        </div>
    </div>
</div>