<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>カード決済申し込み</title>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <!--css layout-->
    <link type="text/css" href="{$config.documentroot}common/css/payment_form.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/css/payment_form_custom.css" rel="stylesheet" media="all" />
    <!--/css layout-->
    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
        <!-- End Google Tag Manager -->
    {/literal}

</head>
<body>
<a name="pagetop" id="pagetop" class="cmnanc"></a>
<div id="wrapper">
<!--
header
-->
<div id="header">
    <h1><img src="{$config.documentroot}/images/pay_form_header.gif" alt=""></h1>
</div>
<div id="contents">
    <div class="headline">
        <h2>決済失敗</h2>
        <p><br>
            お申込は完了しませんでした。大変恐れ入りますが今一度お試しくださいませ。<br>
            <a href="../../index.php">MYBUS TOPに戻る</a>
        </p>
    </div>
    <br>
    <div class="info">
        <div>
            <dl>
                <dt>お問い合わせ先 {$branch.company}</dt>
                <dd></dd>
                <dd>【オプショナルツアーのお問合せ】</dd>
                <dd><font size="2">{$branch.optional.time}</font></dd>
                <dd>Tel：<em>{$branch.optional.tel}</em></dd><dd>E-mail：<a href="mailto:{$branch.optional.email}" style="text-decoration: none">{$branch.optional.email}</a></dd><BR>
                {if $country_iso3 eq 'SGP' || $country_iso3 eq 'IDN' || $country_iso3 eq 'THA' || $country_iso3 eq 'TWN'}
                    <dd>【海外おみやげのお問合せ】</dd>
                    <dd>{$branch.souvenir.company}</dd>
                    <dd><font size="2">{$branch.souvenir.time}</font></dd>
                    <dd>Tel：<em>{$branch.souvenir.tel}</em></dd>
                    <dd>E-mail：<a href="mailto:{$branch.souvenir.email}" style="text-decoration: none">{$branch.souvenir.email}</a></dd></dd>
                {/if}
            </dl>
        </div>
    </div>
    <br>
</div>
<div id="footer_contents">
</div>
<!--
footer
-->
<div id="footer">
</div>
<!--
/footer
-->
</div>
</body>
</html>
