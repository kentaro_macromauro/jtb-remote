{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
    <ul class="bread-camp">
      <li><a href="index.php">TOP</a><span>&gt;</span></li>
      <li><a href="souvenir.php">海外おみやげ</a><span>&gt;</span></li>
      <li>検索</li>
    </ul>
    <div class="clear"></div>

    <!-- box search -->
        <form action="souvenir_search.php" method="get" name="frmSearch">
        <div class="box-search-souvenir-header">Search</div>
			<div class="box-search-souvenir">
            <div class="inp-inputsearch">
            	<div class="inp-inputsearch-inp1"><p>国名</p> {$inp_country}</div>
                <div class="inp-inputsearch-inp2"><p>受取場所</p> {$inp_location} </div>
                <div class="inp-inputsearch-inp3"><p>カテゴリー</p>{$inp_theme}</div>
                <div class="clear"></div>
                <div class="inp-inputsearch-inp4"><p>フリーワード</p>{$inp_search_text}</div>
            </div>        
           <div class="box-search4">
             <input name="inp_search" class="btn_search" type="submit" value="検索" />
           </div>
      	</div>
         <!-- box search -->
        <div class="height10"></div>
        <!-- search result-->
        <div class="pagi-search-result">
        	全{$max_record}件中{$page_start}～ {$page_end}件目を表示しています。
        <div class="btn-order">
        	{$order_select}
        </div>
        </form>
        </div>
   <!-- box search -->
    
     
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <div class="height3"></div>
  {$search_content}
  <div class="height3"></div>
  <div class="clear"></div>
  <!--pagination bar-->
  {$pagination}
  <div class="clear"></div>
  <!--pagination bar-->
  <!--banner footer-->
</div>
<!--container-left-->
<!--container-right-->
{include file="jtb_souvenir_container_right.tpl"}
<!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body></html>