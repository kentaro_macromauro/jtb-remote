<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>カード決済申し込み</title>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <!--css layout-->
    <link type="text/css" href="{$config.documentroot}common/css/payment_form.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/css/payment_form_custom.css" rel="stylesheet" media="all" />
    <!--/css layout-->
    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
        <!-- End Google Tag Manager -->
    {/literal}

</head>
<body>
<a name="pagetop" id="pagetop" class="cmnanc"></a>
<div id="wrapper">
<!--
header
-->
<div id="header">
    <h1><img src="{$config.documentroot}/images/pay_form_header.gif" alt=""></h1>
</div>
<!--
contents
-->
<div id="contents">
    <div class="headline">
        <h2>【お申し込み注意事項】</h2>
        <ul>
            <li><span>&#149;</span><em>決済が完了するとご登録いただいたメールアドレスへ「決済確認メール」が自動的に送信されます。</em></li>
            <li><span>&#149;</span><em>ご利用いただけるカードは<strong> VISA, MasterCard,Amex</strong>のロゴが付いているカードです。</em></li>
            <li><span>&#149;</span><em><font style="color: red;">カード会社のご利用明細書には「JTB」と記載されます。</font></em></li>
        </ul>
    </div>
    <div class="form">
        <form action="http://localhost:8888/payment/action.php" method="post" autocomplete="off">
            <input type="hidden" name="type" value="payment">
            <input type="hidden" name="country_iso3" value="{$form_data.country_iso3}"  />
            <input type="hidden" name="site_name" value="{$form_data.site_name}">
            <input type="hidden" name="site_code" value="{$form_data.site_code}">
            <input type="hidden" name="product_kind" value="{$form_data.product_kind}">
            <input type="hidden" name="order_id" value="{$form_data.order_id}">
            <input type="hidden" name="currency" value="{$form_data.currency}">
            <input type="hidden" name="amount" value="{$form_data.amount}">
            <input type="hidden" name="card_holder" value="{$form_data.card_holder}">
            <input type="hidden" name="exp_m" value="{$form_data.exp_m}">
            <input type="hidden" name="exp_y" value="{$form_data.exp_y}">
            <input type="hidden" name="stripeToken" value="{$form_data.stripeToken}">
            <input type="hidden" name="tel_country_code" value="{$form_data.tel_country_code}">
            <input type="hidden" name="telephone_no" value="{$form_data.telephone_no}">
            <input type="hidden" name="email" value="{$form_data.email}">

            <h2>カード決済申し込みフォーム</h2>
            <table>
                <tr>
                    <th>オーダー商品</th>
                    <td>{if $form_data.product_kind eq 'OPTIONAL_TOUR'}
                            オプショナルツアー
                        {elseif $form_data.product_kind eq 'SOUVENIR'}
                            おみやげ
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th>予約ID</th>
                    <td>{$form_data.order_id}
                    </td>
                </tr>
                <tr>
                    <th>利用料金</th>
                    <td>{$form_data.currency} {$form_data.amount}
                    </td>
                </tr>
                <tr id="cel02">
                    <th>カード名義人名</th>
                    <td>{$form_data.card_holder}
                    </td>
                </tr>
                <tr id="cel03">
                    <th>カード番号</th>
                    <td>**** **** **** ****</td>
                </tr>
                <tr id="cel04">
                    <th>有効期限<br />(月/年)</th>
                    <td>{$form_data.exp_m}/{$form_data.exp_y}</td>
                </tr>
                <tr id="cel05">
                    <th>セキュリティコード</th>
                    <td>***</td>
                </tr>

                <tr id="cel06">
                    <th>電話番号</th>
                    <td>{$form_data.tel_country_code}{$form_data.telephone_no}</td>
                </tr>
                <tr id="cel07">
                    <th>メールアドレス</th>
                    <td>{$form_data.email}
                    <span>決済完了後に「決済確認メール」が送信されます。</span></td>
                </tr>
            </table>
            <p id="p01"><button id="submit01" type="submit">申込確認</button></p>
        </form>
        <p id="p02">
            <SCRIPT LANGUAGE="JavaScript"  TYPE="text/javascript"
                    SRC="//smarticon.geotrust.com/si.js"></SCRIPT>
        </p>
    </div>
    <br>
    <div class="info">
        <div>
            <dl>
                <dt>お問い合わせ先 {$branch.company}</dt>
                <dd></dd>
                <dd>【オプショナルツアーのお問合せ】</dd>
                <dd><font size="2">{$branch.optional.time}</font></dd>
                <dd>Tel：<em>{$branch.optional.tel}</em></dd><dd>E-mail：<a href="mailto:{$branch.optional.email}" style="text-decoration: none">{$branch.optional.email}</a></dd><BR>
                {if $country_iso3 eq 'SGP' || $country_iso3 eq 'IDN' || $country_iso3 eq 'THA' || $country_iso3 eq 'TWN'}
                    <dd>【海外おみやげのお問合せ】</dd>
                    <dd>{$branch.souvenir.company}</dd>
                    <dd><font size="2">{$branch.souvenir.time}</font></dd>
                    <dd>Tel：<em>{$branch.souvenir.tel}</em></dd>
                    <dd>E-mail：<a href="mailto:{$branch.souvenir.email}" style="text-decoration: none">{$branch.souvenir.email}</a></dd></dd>
                {/if}
            </dl>        </div>
    </div>
    <br>
</div>
<div id="footer_contents">
</div>
<!--
footer
-->
<div id="footer">
</div>
<!--
/footer
-->
</div>
</body>
</html>
