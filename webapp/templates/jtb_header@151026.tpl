<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{php}
switch ($_SERVER[REQUEST_URI])
{
	case '/' : echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
    case '/souvenir.php' :  echo '海外おみやげならJTBマイバスサイト';   break;
    default: echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
}
{/php}
</title>
<meta name="Description" content="{php}
switch ($_SERVER[REQUEST_URI])
{
	case '/' : echo 'JTBの海外観光・現地オプショナルツアーブランドである「マイバス」のアジア・オセアニア専門販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
    case '/souvenir.php': echo 'アジア・パシフィックの海外おみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
    default : echo 'JTBの海外観光・現地オプショナルツアーブランドである「マイバス」のアジア・オセアニア専門販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; break;
}
{/php}" />
<meta name="Keywords" content="{php}
switch ($_SERVER[REQUEST_URI])
{
	case '/' : echo '海外観光,オプショナルツアー,現地ツアー,マイバス,JTB'; break;
    case '/souvenir.php' : echo '海外おみやげ,アジアおみやげ,マイバス,JTB' ; break;
    default : echo '海外観光,オプショナルツアー,現地ツアー,マイバス,JTB'; break;
}
{/php}" />

<link type="text/css" href="{$config.documentroot}common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>

</head>
<body>
<!--body-->
<!--header-->
<div class="header">
   <a class="logo_text_link" href="index.php">海外観光・オプショナルツアーならJTB</a>
    <a class="mybus-logo" href="index.php">
    	<img src="{$config.documentroot}images/mybuslogo.jpg" alt="Mybus Logo"  /> 
 	</a>
    
    
    
    <div style="position:absolute; right:70px; top:5px; width:500px;" >
  	      
      
      {if $config.basket eq true}
      <a class="ico_cart" href="http://{$smarty.server.HTTP_HOST}/booking_cart.php">買い物カゴを見る</a>
      {/if}
    
  </div>
    
    
    <!--social share-->   
    <ul class="social_network">
        <li><span class='st_facebook_hcount' displayText='Facebook'></span></li>
        <li><span class='st_twitter_hcount' displayText='Tweet'></span></li>
        <li><a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="a0028392a9c4e9ac68bccb2f09f14f59875140c6" data-button="button-1">Check</a><script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script></li>
   </ul>
   <!--end social share-->
    <!--link-->
    
     <div style="position:absolute; right:205px; top:34px; ">
       <img alt="" src="images/jtblogo.jpg" />
      </div>
    
    <div class="header_link">
      <div class="header_link_right"></div>
      <div class="header_link_center">
        <ul>
          <li><a href="page.php?id=branch.html">会社概要</a></li>
          <li><a href="page.php?id=sitemap.html">サイトマップ</a></li>
        </ul>
        <div class="clear"></div>
      </div>
      <div class="header_link_left"></div>
      <div class="clear"></div>
    </div>
    <!--end link-->
    <!--link2-->
    <div class="header_link2">
      <ul>
        <li><a href="index.php">JTBアジアパシフィックTOPへ</a></li>
        <li><a href="souvenir.php">海外おみやげTOPへ</a></li>
        <li><a href="opt.php">オプショナルツアーTOPへ</a></li>
      </ul>
    </div>
    <!--end link2-->
</div>
