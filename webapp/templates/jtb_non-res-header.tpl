<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="{$config.documentroot}images/icon.png">
    <title>
        {php}
        switch ($_SERVER[REQUEST_URI]){
        case '/' : echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
        case '/souvenir.php' :  echo '海外おみやげならJTBマイバスサイト';   break;
        default: echo '海外観光・オプショナルツアーならJTBアジア・パシフィック　マイバスサイト'; break;
        }{/php}
    </title>
    <meta name="Description" content="{if $patetittle eq "product_souvenir"}{$product.short_desc|strip}{else}{php} echo $tagdesc;{/php}{/if}" />
    <meta name="Keywords" content="{if $patetittle eq "product_souvenir"}{if $config.country eq "TWN"}台湾おみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "IDN"}バリ島/インドネシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "THA"}タイおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "VNM"}ベトナムおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "MYS"}マレーシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "SGP"}シンガポールおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "AUS"}オーストラリアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "NZL"}ニュージーランドおみやげ,アジアおみやげ,マイバス,JTB{/if}{else}{php} echo $tagkeyword;{/php}{/if}" />
    <link rel="shortcut icon" href="/ico/favicon.ico" />
    <link rel="apple-touch-icon" href="http://www.mybus-asia.com/apple-touch-icon.png" />

    <link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict(true);
    </script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>

    <link rel="stylesheet" href="{$config.documentroot}common/css/non-res-style.css" />
    <link rel="stylesheet" href="{$config.documentroot}common/css/datepicker.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Arimo|Signika:400,600' rel='stylesheet' type='text/css'>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/nivo-slider.css" type="text/css" media="screen" />

    <link href='//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.min.css' rel='stylesheet'/>


<!--
<link type="text/css" href="{$config.documentroot}common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
-->
<!--
{if $souvenir_form}
<script type="text/javascript" src="{$config.documentroot}common/js/souvenir_form.js"></script>
{else}
<script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>
{/if}

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>


{if $product_review eq 1}
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.lightbox-0.5.min.js"></script>
<link type="text/css" href="{$config.documentroot}common/jquery.lightbox-0.5.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/start_lightbox.js"></script>
{/if}

{if $config.country eq "TWN" }    
{literal}
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32237848-1', 'auto');

</script> 
{/literal}
{/if}
-->
    {literal}
        <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
    <!-- End Google Tag Manager -->
    {/literal}


</head>
<body>


<!--body-->
<!--header-->
<header>
    <div class="container">
        <table class="rows">
            <tbody>
                <tr>
                    <td>
                        <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="rows" style="margin:5px 0 5px 0;">
            <tbody>
                <tr>
                    <td><div class="col-md-2">
                            <div class="navbar-brand">
                                <a  href="./" class="desktops">
                                    <img src="{$config.documentroot}images/logo/mybuslogo.jpg" width="143px" alt="Responsive image"/>
                                </a>
                            </div>
                        </div></td>
                    <td><div class="col-md-5 padding-cn"></div></td>
                    <td><div class="col-md-5">
                        <table class="rows social-top">
                            <tr>
                                <td><div class="col-md-4ss">
                                        <table class="rows">
                                            <td><div class="col-md-2s">
                                                    <div align="right" style="margin-right: -15px;"><img src="{$config.documentroot}images/logo/jtb-perfect-moment.jpg" class="img-responsive" alt="Responsive image" style="margin-top: 6px;"/></div>
                                                </div></td>

                                            <td><div class="col-md-2s" style="width: 80px;">
                                                    <p style="padding-top: 23px; padding-right: 10px; margin-bottom: 0;"><a href="page.php?id=branch.html" style="font-size: 10px; text-align: right; color: #3212f3;">会社概要</a></p>
                                                </div></td>
                                        </table>
                                    </div></td>

                                <td><div class="col-md-2s">
                                        <div class="round-social-color">
                                            <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                                            <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="header-second">
        <div class="container grey-bg">
            <div id="ribon-red-desktop-sps2"></div>
        </div>
    </div>
</header>