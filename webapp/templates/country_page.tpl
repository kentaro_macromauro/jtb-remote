{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">

  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
    {$breadcamp}
    {$data_tempage}
  </div>
  <!--container-left-->
</div>
<!--container-->
{if $config.country eq 'TWN'}
{literal}
<script type="text/javascript">
var trackOutboundLink = function(url) {
   ga('send', 'event', 'URL', 'click', url, {'hitCallback':
     function () {
     document.location = url;
     }
   });
}
</script>
{/literal}
{/if}
<div class="clear"></div>
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->
<!--body-->
</body>
</html>