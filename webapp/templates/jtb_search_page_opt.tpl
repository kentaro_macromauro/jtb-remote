{include file="jtb_header.tpl" }
<!--container-->
<div class="container home-content pull-container">
<div class="mybus-breadcrumb" style="padding: 0 20px 0 10px; font-family: 'Arial', sans-serif;">
    {$breadcamp}
    <div style="height: 0px;"></div>
</div>
<div class="row"></div>
<div style="padding: 0 5px 0 5px;">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="mybus-search">
    <div class="row">
        <div class="col-md-12" style="">
            <div class="search-header-before">
                <p class="search-header" style="font-weight: 700;">オプショナルツアー検索</p>
            </div>
        </div>
    </div>
    <div class="mybus-search-inner">
        <form action="search_opt.php" method="get" name="frmSearch" onSubmit="return search_check()">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">国名</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_country" style="padding-left: 0px;">
                            {$inp_country}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">都市名</div>
                        </div>
                        <div class="select_city col-md-9 col-sm-8 col-xs-7 long-selects" style="padding-left: 0px;">
                            {$inp_city}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">テーマ</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_category" style="padding-left: 0px;">
                            <select name="inp_category" class="form-control">
                                <option value="0"></option>
                                {$select_category}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">条件検索</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_option" style="padding-left: 0px;">
                            <select name="inp_option" class="form-control">
                                <option value="0"></option>
                                {$select_option}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">時間帯</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_time" style="padding-left: 0px;">
                            <select name="inp_time" class="form-control">
                                <option value="" selected></option>
                                {$select_time}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">フリーワード</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects" style="padding-left: 0px;">
                            {$inp_keyword}
                        </div>
                    </div>
                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-9 col-sm-8">
                            <input type="submit" value="検索する" class="btn btn-blue-search" style="width: 100%;"/>
                        </div>
                    </div>
                    <div style="height: 0px;"></div>
                </div>
            </div>
    </div>
</div>
<div style="text-align: right;">
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-9"><div class="pagination-words"><p>{$max_record}件中{$page_start}〜{$page_end}件目を表示中</p></div></div>
        <div class="col-md-3 col-sm-3 col-xs-3" style="padding-top: 20px;">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    {$order_select}
                </div>
            </div>
        </div>
    </div>
</div>
<div style="text-align: right;">
<div class="row">
    <div class="col-md-4 col-sm-1 col-xs-1"></div>

    <div class="col-md-8 col-sm-11 col-xs-11">
    {$pagination}
</div>
</form>
</div>
</div>


{$search_content}
<!-- data -->

<!--

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>


            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ts">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-5">
            <div class="ts-thumbnail">
                <a href="#">
                    <div class="row off-hundread">
                        <div class="col-md-12 col-sm-12 col-xs-9">
                            <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                            <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                        </div>
                        <div class="col-xs-3"></div>
                    </div>

                    <img src="{$config.documentroot}images/tour-search/thumbnail.jpg" class="img-responsive" alt="tour search"></a>
            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-7">
            <div class="ts-description">
                <div class="ts-list-cn-name">シンガポール</div>
                <div class="ts-list-title"><a href="#">楽しさ盛りだくさん！センとーサ島観光</a></div>
                <div class="ts-list-desc">セントーサ島最新スポット、世界最大級の水族園”シーアクアリウム™をはじめ、‘’マーライオンタワー””スカイタワー”も訪れます。セントーサ島をぐるっとまわる欲張りツアーです。【ツアー代金】大人S$125／子供（2歳以上12歳未満）　S$95 ＊11月1日依降ツアー代金が改訂いたします。</div>

                <div class="ts-des-btn">
                    <a class="btn btn-grey-ts">定番観光</a>
                    <a class="btn btn-grey-ts">歴史・世界遺産</a>
                    <a class="btn btn-grey-ts">観光</a>
                </div>

                <div class="ts-price-mobile">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                            <h5>SGD 96~</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="ts-prices">
                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                    <h5><del>SGD 120&nbsp;&nbsp;&nbsp;</del></h5>
                    <h5>SGD 96~</h5>
                </div>


                <div class="tour-price logo" style="">
                    <div style="height: 10px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
                <div class="tour-price tab">
                    <div style="height: 16px;"></div>
                    <a href="#"><img src="{$config.documentroot}images/tour-search/clock-md.png" class="img-responsives" alt="tour search"></a>
                    <span>午前</span>
                    <div style="height: 12px;"></div>
                    <a href="#" class="btn btn-blue-ts">詳細はこちら</a>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<!-- End of Search Result -->





<!-- Rangking Veltra -->
<div style="height: 30px"></div>
<div class="row">
    {if $config.country != "PHL"}
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="ranking-vertical-left">
            <div class="ranking-vertical">
                <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>売れ筋ランキング</h5>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-1.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-2.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-3.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
            </div>
        </div>
    </div>
    {/if}
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="ranking-vertical-right">
            <div class="ranking-vertical">
                <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>おすすめオプショナルツアー</h5>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-1.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-2.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
                <p><a href="#"><span><img src="{$config.documentroot}images/sidebar/ver-ranking-3.png" alt="mybus"/></span>ナイトサファリ　（往復送迎/食事つき）...</a></p>
            </div>
        </div>
    </div>
</div>
<!-- Rangking Veltra -->



</div>






</div>


<div class="gotop">
    <div style="height: 55px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png" alt="gotop">&nbsp;<a href="#top" style="text-decoration: underline; color: #847973;">ページTOP</a></p>
</div>

</div><!-- end of main content -->
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>


<!-- JS SLIDER -->
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.nivo.slider.js"></script>
{literal}
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
{/literal}


<!-- DATE TIME -->
<script src="{$config.documentroot}common/js/datepicker/bootstrap-datetimepicker.js"></script>
{literal}
    <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {

                $('#example1').datepicker({
                    format: "dd/mm/yyyy",
					autoclose: true
                });

				$('#example2').datepicker({
                    format: "dd/mm/yyyy",
					autoclose: true
                });

            });
        </script>
{/literal}
<!-- END OF DATE TIME -->

<!-- MAP -->
{literal}
    <style>
        #map_canvas {
            width: 100%;
            height: 180px;
        }
    </style>
{/literal}
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

{literal}
    <script>
			function initialize() {
			var map_canvas = document.getElementById('map_canvas');
			var map_options = {
				center: new google.maps.LatLng(-8.736365,115.168387),
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var map = new google.maps.Map(map_canvas, map_options)
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
{/literal}
<!-- END OF MAP -->
<!--body-->
</body></html>