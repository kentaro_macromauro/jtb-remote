<div class="footer">
    <div class="container footer-padding">
        <div id="ribon-red-desktop-sps"></div>
        <div style="height: 15px;"></div>
        <div class="footer-text-desktop">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">

                        <!-- Satu -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../taiwan/">台湾</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../taiwan/search_opt.php?inp_country=TWN&inp_city=TPE">台北</a></p>
                                    <p><a href="../taiwan/search_opt.php?inp_country=TWN&inp_city=KAO">高雄</a></p>
                                    <p><a href="../taiwan/search_opt.php?inp_country=TWN&inp_city=HUA">花蓮</a></p>
                                    <p><a href="../taiwan/search_opt.php?inp_country=TWN&inp_city=OTH">その他の地域</a></p>
                                </div>

                                <div style="height: 30px;"></div>
                                <div class="footer-title"><p><a href="../philippines/">フィリピン</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../philippines/search_opt.php?inp_country=PHL&inp_city=CEB">セブ</a></p>
                                    <p><a href="../philippines/search_opt.php?inp_country=PHL&inp_city=GWU">ボホール</a></p>
                                    <p><a href="../philippines/search_opt.php?inp_country=PHL&inp_city=MNL">マニラ</a></p>
                                </div>

                            </center>
                        </div>
                        <!-- Dua -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%%">
                            <center>
                                <div class="footer-title"><p><a href="../indonesia/">インドネシア</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../indonesia/search_opt.php?inp_country=IDN&inp_city=DPS">バリ</a></p>
                                    <p><a href="../indonesia/search_opt.php?inp_country=IDN&inp_city=CGK">ジャカルタ</a></p>
                                    <p><a href="../indonesia/search_opt.php?inp_country=IDN&inp_city=OTH">その他の地域</a></p>
                                </div>
                                <div style="height: 48px;"></div>
                                <div class="footer-title"><p><a href="myanmar/">ミャンマー</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../myanmar/search_opt.php?inp_country=MMR&inp_city=RGN">ヤンゴン</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Tiga -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../thailand/">タイ</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=BKK">バンコク</a></p>
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=CNX">チェンマイ・チェンライ</a></p>
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=HKT">プーケット</a></p>
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=USM">サムイ</a></p>
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=KVR">クラビ</a></p>
                                    <p><a href="../thailand/search_opt.php?inp_country=THA&inp_city=OTH">その他の地域</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Empat -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../vietnam/">ベトナム</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../vietnam/search_opt.php?inp_country=VNM&inp_city=SGN">ホーチミン</a></p>
                                    <p><a href="../vietnam/search_opt.php?inp_country=VNM&inp_city=HAN">ハノイ</a></p>
                                    <p><a href="../vietnam/search_opt.php?inp_country=VNM&inp_city=OTH">中部ベトナム・その他</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Lima -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../cambodia/">カンボジア</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../cambodia/search_opt.php?inp_country=KHM&inp_city=REP">シェムリアップ</a></p>
                                    <p><a href="../cambodia/search_opt.php?inp_country=KHM&inp_city=OTH">その他の地域</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Enam -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../malaysia/">マレーシア</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../malaysia/search_opt.php?inp_country=MYS&inp_city=KUL"> クアラルンプール</a></p>
                                    <p><a href="../malaysia/search_opt.php?inp_country=MYS&inp_city=PEN"> ペナン</a></p>
                                    <p><a href="../malaysia/search_opt.php?inp_country=MYS&inp_city=LGK"> ランカウイ</a></p>
                                    <p><a href="../malaysia/search_opt.php?inp_country=MYS&inp_city=BKI"> コタキナバル</a></p>
                                    <p><a href="../malaysia/search_opt.php?inp_country=MYS&inp_city=MLC"> マラッカ</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Tujuh -->
                        <div class="col-md-1 col-sm-1" style="width: 11.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../singapore/">シンガポール</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../singapore/search_opt.php?inp_country=SGP&inp_city=SIN"> シンガポール</a></p>
                                    <p><a href="../singapore/search_opt.php?inp_country=SGP&inp_city=OTH"> その他の地域</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Delapan
                            <div class="col-md-1 col-sm-1" style="width: 6.33333%">
                                <center>
                                    <div class="footer-title"><p><a href="http://www.mybus-asia.com/hotel.php?id=http://www.mybus.com.cn/hkg/">香港</a></p></div>
                                </center>
                            </div>
                        -->
                        <!-- Sembilan -->
                        <div class="col-md-1 col-sm-2" style="width: 12.03733%">
                            <center>
                                <div class="footer-title"><p><a href="../australia/">オーストラリア</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../australia/page.php?id=sydney.html">シドニー</a></p>
                                    <p><a href="../australia/page.php?id=gold_coast.html">ゴールドコースト</a></p>
                                    <p><a href="../australia/page.php?id=cairns.html">ケアンズ</a></p>
                                    <p><a href="../australia/page.php?id=melbourne.html">メルボルン</a></p>
                                    <p><a href="../australia/page.php?id=perth.html">パース</a></p>
                                    <p><a href="../australia/page.php?id=ayersrock.html">エアーズロック</a></p>
                                    <p><a href="../australia/page.php?id=adelaide.html">アデレード</a></p>
                                    <p><a href="../australia/page.php?id=tasmania.html">タスマニア</a></p>
                                    <p><a href="../australia/page.php?id=brisbane.html">ブリスベン</a></p>
                                    <p><a href="../australia/page.php?id=darwin.html">ダーウィン</a></p>
                                    <p><a href="../australia/page.php?id=hamilton_island">ハミルトン島</a></p>
                                </div>
                            </center>
                        </div>
                        <!-- Sepuluh -->
                        <div class="col-md-1 col-sm-2" style="width: 11.33333%">
                            <center>
                                <div class="footer-title"><p><a href="../newzealand/">ニュージーランド</a></p></div>
                                <div class="footer-child">
                                    <p><a href="../newzealand/search_opt.php?inp_country=NZL&amp;inp_city=AKL">オークランド</a></p>
                                    <p><a href="../newzealand/search_opt.php?inp_country=NZL&amp;inp_city=ROT">ロトルア</a></p>
                                    <p><a href="../newzealand/search_opt.php?inp_country=NZL&amp;inp_city=CHC">クライストチャーチ</a></p>
                                    <p><a href="../newzealand/search_opt.php?inp_country=NZL&amp;inp_city=MON">マウントクック</a></p>
                                    <p><a href="../newzealand/search_opt.php?inp_country=NZL&amp;inp_city=ZQN">クイーンズタウン</a></p>
                                </div>
                            </center>
                        </div>

                    </div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 pull-foot thumb-black-link">
                    <div style="height: 20px;"></div>
                    <center>
                        <div class="round-social-color">
                            <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                            <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
                        </div>
                        <div style="height: 10px;"></div>
                        <div><span><a href="{$config.documentroot}{$config.environment}page.php?id=branch.html" style="font-size: 12px; font-weight: 700; color: #000;">会社概要</a>&nbsp;&nbsp;&nbsp;</span>
                            <span><a href="{$config.documentroot}{$config.environment}page.php?id=term.html" style="font-size: 12px; font-weight: 700; color: #000;">旅行約款</a></span>
                            <span>&nbsp;&nbsp;&nbsp;<a href="{$config.environment}{$config.environment}page.php?id=privacy.html" style="font-size: 12px; font-weight: 700; color: #000;">個人情報保護方針</a></span></div>
                    </center>
                </div>

            </div>
        </div>


        <div class="footer-text-mobile">
            <div class="row">
                <div class="col-xs-12">
                    <center>
                        <div class="round-social-color">
                            <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                            <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
                        </div>
                        <div style="height: 10px;"></div>
                        <div><span><a href="{$config.documentroot}{$config.environment}page.php?id=branch.html" style="font-size: 12px; color: #000; font-weight: bold;">会社概要</a>&nbsp;&nbsp;&nbsp;</span>
                            <span><a href="{$config.documentroot}{$config.environment}page.php?id=term.html" style="font-size: 12px; color: #000; font-weight: bold;">旅行約款</a></span>
                            <span>&nbsp;&nbsp;&nbsp;<a href="{$config.documentroot}{$config.environment}page.php?id=privacy.html" style="font-size: 12px; color: #000; font-weight: bold;">個人情報保護方針</a></span></div>
                    </center>
                </div>
            </div>
        </div>

    </div>

</div>


<div class="container footer-padding">
    <div class="footer-bottom">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <p style="margin-top: 5px;">Copyright &copy; 2013-2015 My Bus. All rights reserved</p>

            </div>
        </div>
    </div>
</div>