﻿{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
  <div class="clear "></div>
  <!--navigation bar-->
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
        <div class="formbox">
            <!--
        	<form action="https://secure.axes-payments.com/cgi-bin/credit/link-point.cgi" name="card" method="post" >
           		<input type="hidden" name="site_code" value="{$payment_code}"  />
                <input type="hidden" name="amount" value="{$amount}"  />
                <input type="hidden" name="currency" value="{$currency_product}" />
                <input type="hidden" name="telephone_no" value=""  />
                <input type="hidden" name="email" value="{$email}" />
                <input type="hidden" name="order_code" value="{$ordercode}"  />
                <input type="hidden" name="user_id" value="{$ordercode}"  />
                <input type="hidden" name="optional" value="{$optional}" />													
                <input type="hidden" name="success_url" value="http://www.mybustest.com/{$config.countryname}/booking_payment_action.php?paid_type=1&transection={$book_id}" />
                <input type="hidden" name="failure_url" value="http://www.mybustest.com/{$config.countryname}/booking_fail_action.php?paid_type=1&transection={$book_id}" />  
            </form>

            <form action="https://mybus-asia.com/{$config.countryname}/booking_payment_form.php" name="card" method="post" >
-->
            <form action="http://localhost:8888/{$config.countryname}/booking_payment_form.php?paid_type=1&transection={$book_id}" name="card" method="post" >
                <input type="hidden" name="site_code" value="{$payment_code}"  />
                <input type="hidden" name="amount" value="{$amount}"  />
                <input type="hidden" name="currency" value="{$currency_product}" />
                <input type="hidden" name="telephone_no" value=""  />
                <input type="hidden" name="email" value="{$email}" />
                <input type="hidden" name="order_code" value="{$ordercode}"  />
                <input type="hidden" name="user_id" value="{$ordercode}"  />
                <input type="hidden" name="optional" value="{$optional}" />
            </form>


           <form action="booking_payment_action.php?paid_type=2&transection={$book_id}" name="none_card" method="post">
           		
                
           </form>
         
           <input type="hidden" name="transection_id" value="{$book_id}" />
           <ul class="booking-nav-process">         
            
            
            <li id="step1">希望日選択</li>
            <li id="step2">カート</li>
            
            <li id="step3">予約情報入力</li>
            <li id="step4" class="active_last">予約情報確認</li>
            
            {if $status_book == '1' || $status_book == '2' || $status_book == '3'}
            <li id="step5" class="active">お支払い方法の選択</li>
            {/if}
            
            {if $status_book == '1' || $status_book == '2' || $status_book == '3'}
            <li id="step6" class="active_first">決済情報確認</li>
            <li id="step7" >決済情報確認</li>
            {/if}
            
            <li id="step8">完了</li>
            
            
            
            
            
            
            
            
            
            
          </ul>
          <div class="clear"></div>
          <span class="texttittle">お支払い方法の選択</span>
          
          <div class="height10"></div>
          
          {if $msg == "error"}
          <p class="text-small-red">※ お支払い方法が入力されていません。</p>
          {/if}
          <table cellspacing="1" class="tbform">
          	
           {if $product_status eq 1}
            <tr>
            <td style="vertical-align:middle; width:25px; table-layout:fixed;">
            	<input name="paid_type" type="radio" value="1" />
            <td style="vertical-align:middle;">クレジットカード</td>
            <td>
                <div style="padding-top:5px;">
                    <img src="../images/card_all.gif" alt="VISA,Master"  /></div>
                <div style="padding-left:5px;">
                    ※決済は、Stripeに委託しております。<br/>お客様のカード支払明細には「 JTB 」名義で表示されます。</div></td>
            </tr>
           {/if}
           
           {if $product_status eq 2}
            <tr>
            <td><input name="paid_type" type="radio" value="2" /></td>
            <td>現地にて現金でお支払い</td>
            <td>当日現地係員へ直接お支払いください</td>
            </tr>
            {/if}
            
            {if $product_status eq 3}
            <tr>
            <td style="vertical-align:middle; width:25px; table-layout:fixed;">
            	<input name="paid_type" type="radio" value="1" />
            <td style="vertical-align:middle;">クレジットカード</td>
            <td>
                <div style="padding-top:5px;">
                <img src="../images/card_all.gif" alt="VISA,Master"  /></div>
                <div style="padding-top:5px;">
                    ※決済は、Stripeに委託しております。<br/>お客様のカード支払明細には「 JTB 」名義で表示されます。</div></td>
            </tr>
            <tr>
            <td><input name="paid_type" type="radio" value="2" /></td>
            <td>現地にて現金でお支払い</td>
            <td>当日現地係員へ直接お支払いください</td>
            </tr>
            
       
            {/if}
            
            
          </table>

          </form>
                  
          <span style="padding:5px 150px 5px 165px; display:block;">
          <input type="button" class="btn_submit gotoback" value="戻る" style="width:70px;" />
          <input type="button" class="btn_submit gotopay" value="次へ" style="width:70px;" />
          </span>  
          
          <form action="booking_infoinput_confirm.php?transection={$book_id}" name="gotoback" method="post"></form>
          
          </div>

           {literal}
            <div style="height:100px;"></div>
            <div style="float:right; margin-right:5px;">
            
            <span id="ss_gmo_img_wrapper_100-50_image_ja">
            <a href="https://jp.globalsign.com/" target="_blank">
            <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
            </a>
            </span>
            <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
          </div>
      	  {/literal}
          
    </div>
  </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
</div>
<!--container-->
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->
<!--body-->
</body></html>