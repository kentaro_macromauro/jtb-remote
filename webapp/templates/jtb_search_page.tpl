{include file="country_header.tpl" }

<body>
<!--body-->

{include file="country_header_menu.tpl" }

<!--container-->
<div class="container">
  <!--navigation bar-->
  {include file="country_navigation_bar.tpl" }
  <!--navigation bar-->
  <!--banner-top-->
  <!--banner-top-->
  <!--container-left-->
  <div class="container-left">
    	<ul class="bread-camp">
        	<li><a href="../index.php">TOP</a><span>&gt;</span></li>
			<li><a href="index.php">タイ</a><span>&gt;</span></li>
			<li>オプショナルツアー</li>
        </ul>
      <div class="clear"></div>

        <!-- box search -->
        <form action="search.php" method="get" name="frmSearch">
        	<div class="box-search-header">Search</div>
			<div class="box-search">


           <ul class="box-search1" >
           		<li><p>国名</p> {$inp_country} </li>
                <li><p>都市名</p>
                	 <span id="select_city">
                     {$inp_city}
                    </span>
                </li>

           </ul>

           <ul class="box-search2">
           		<li>  <p>キーワード</p> {$inp_keyword}   </li>
                <li>  <p>テーマ指定</p>
                {$select_category}

                </li>

           </ul>

           <ul class="box-search3">
           		<li>  <p>時間帯</p>
                	{$select_time}
                <li style="display:none;">  <p class="p_widthlimit">日にち</p>
                	{$select_yearmonth}
                    {$select_day}

                </li>
           </ul>


           <div class="box-search4">
             <input name="inp_search" class="btn_search" type="submit" value="検索" />
           </div>

      		</div>


        <!-- box search -->
        <div class="height10"></div>
        <!-- search result-->
        <div class="pagi-search-result">
        	全{$max_record}件中{$page_start}～ {$page_end}件目を表示しています。

        <div class="btn-order">
        	{$order_select}

        </div>
        </form>

        </div>
        <div class="clear"></div>
        <!--pagination bar-->

        {$pagination}


        <div class="clear"></div>
        <!--pagination bar-->

       	 <div class="height3"></div>
          {$search_content}
         <div class="height3"></div>

     	<div class="clear"></div>
        <!--pagination bar-->
        {$pagination}

        <div class="clear"></div>
        <!--pagination bar-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="country_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="country_country_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>