{include file="country_header.tpl" }

<body>
<!--body-->

{include file="country_header_menu.tpl" }

<div class="container home-content pull-container">
<div class="mybus-breadcrumb" style="padding: 0 20px 0 10px; font-family: 'Arial', sans-serif;">
    {$breadcamp}
    <div style="height: 0px;"></div>
</div>

<div class="row">

</div>
<div style="height: 0px;"></div>

<div class="row">
<div class="col-md-12 col-sm-12">
<div class="localinfo-banner-desktop">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 three-thumbnail-first">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=info.html"><img
                        src="{$config.documentroot}images/local-information/country_info.jpg"
                        class="img-responsive" alt="okinawa"></a>

            <div class="grey-thumb-desc"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=info.html"><b>国情報</b></a>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 three-thumbnail-center">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=city.html""><img
                    src="{$config.documentroot}images/local-information/city_info.jpg" class="img-responsive"
                    alt="fukoka"></a>
            <div class="grey-thumb-desc"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=city.html""><b>都市情報</b></a>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 three-thumbnail-last">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=tourist_desk.html""><img
                    src="{$config.documentroot}images/local-information/traveldesk.jpg" class="img-responsive"
                    alt="yokohama"></a>
            <div class="grey-thumb-desc"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=tourist_desk.html""><b>現地トラベルデスク</b></a>
            </div>
        </div>
    </div>
</div>

<div class="localinfo-banner-mobile">
    <div class="row">
        <div class="col-xs-12">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=info.html"><img
                        src="{$config.documentroot}images/local-information/country_info_sp.jpg"
                        class="img-responsive" alt="okinawa"></a>

            <div class="flying-text-other"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=info.html"><b>国情報</b></a>
            </div>
        </div>

        <div class="col-xs-12" style="margin-top: 20px;">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=city.html""><img
                    src="{$config.documentroot}images/local-information/city_info_sp.jpg" class="img-responsive"
                    alt="fukoka"></a>
            <div class="flying-text-other"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=city.html""><b>都市情報</b></a>
            </div>
        </div>

        <div class="col-xs-12" style="margin-top: 20px;">
            <a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=tourist_desk.html""><img
                    src="{$config.documentroot}images/local-information/traveldesk_sp.jpg"
                    class="img-responsive" alt="yokohama"></a>
            <div class="flying-text-other"><a
                        href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=tourist_desk.html""><b>現地トラベルデスク</b></a>
            </div>
        </div>
    </div>
</div>

<div class="grey-tittle-hide">
    <div style="height: 20px;"></div>
    <div class="row">
        <div class="col-md-12" style="">
            <div class="grey-tittle-before">
                <p class="grey-tittle-after" style="font-weight: 700;">最新情報</p>
            </div>
        </div>
    </div>
    <div style="height: 0px;"></div>
</div>

<div class="latest-informations">
    {foreach from=$news item=items}
        <div class="news_icon">
        {if $items.icon eq "1"}
            <img src="{$config.documentroot}images/icon_new.gif" alt="new" />
        {/if}
    </div>
        <p>{$items.date} <a href="news_detail.php?id={$items.id}">{$items.subject}</a></p>
    {/foreach}

    <div style="height: 10px;"></div>
    <div align="right">
        <a href="{$config.documentroot}{$config.environment}{$config.countryname}/news.php"
           style="font-size: 12px; color: #3212f3;">その他はこちら</a>
    </div>
</div>
{if $config.country eq "IDN" or "MYS"}
<div class="grey-tittle">
    <div style="height: 20px;"></div>
    <div class="row">
        <div class="col-md-12" style="">
            <div class="grey-tittle-before">
                <p class="grey-tittle-after" style="font-weight: 700;">特集記事</p>
            </div>
        </div>
    </div>
</div>

<div class="local-info-content-desktop">
    {$special_page}
</div>
<div class="local-info-content-mobile">
    {$special_page}
</div>
<div style="height: 20px;"></div>
{/if}

<div class="grey-tittle">
    <div style="height: 20px;"></div>
    <div class="row">
        <div class="col-md-12" style="">
            <div class="grey-tittle-before">
                <p class="grey-tittle-after" style="font-weight: 700;">現地情報</p>
            </div>
        </div>
    </div>

    <div class="local-info-content-desktop">
        <div style="height: 16px;"></div>
        {$sp_content}
    </div>
    <div class="local-info-content-mobile">
        {$sp_content}
    </div>


    <!-- Rangking Veltra -->
    <div style="height: 30px"></div>
    <div class="row">
        {if $config.country != "PHL" && $config.country != "MMR" }
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="ranking-vertical-left">
                <div class="ranking-vertical">
                    <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>売れ筋ランキング
                    </h5>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_POPULAR_#1" href="{$sales_ranking[0].product_link}" {$sales_ranking[0].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-1.png"
                                        alt="mybus"/></span>{$sales_ranking[0].product_name}</a></p>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_POPULAR_#2" href="{$sales_ranking[1].product_link}" {$sales_ranking[1].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-2.png"
                                        alt="mybus"/></span>{$sales_ranking[1].product_name}</a></p>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_POPULAR_#3" href="{$sales_ranking[2].product_link}" {$sales_ranking[2].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-3.png"
                                        alt="mybus"/></span>{$sales_ranking[2].product_name}</a></p>
                </div>
            </div>
        </div>
        {/if}

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="ranking-vertical-right">
                <div class="ranking-vertical">
                    <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>おすすめオプショナルツアー
                    </h5>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_RECOMMEND_#1" href="{$recommend_tour[0].product_link}" {$recommend_tour[0].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-1.png"
                                        alt="mybus"/></span>{$recommend_tour[0].product_name}</a></p>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_RECOMMEND_#2" href="{$recommend_tour[1].product_link}" {$recommend_tour[1].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-2.png"
                                        alt="mybus"/></span>{$recommend_tour[1].product_name}</a></p>

                    <p><a class="ga_click" id="{$config.countryname}_COUNTRYINFO_RECOMMEND_#3" href="{$recommend_tour[2].product_link}" {$recommend_tour[2].link_type}><span><img
                                        src="{$config.documentroot}images/sidebar/ver-ranking-3.png"
                                        alt="mybus"/></span>{$recommend_tour[2].product_name}</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Rangking Veltra -->

    <!-- Banner -->
    {$footer_banner_img}
    <!-- end Banner -->

</div>


<div class="gotop">
    <div style="height: 55px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png"
                                                      alt="gotop">&nbsp;<a href="#top"
                                                                           style="text-decoration: underline; color: #847973;">ページTOP</a>
    </p>
</div>

</div>
</div>
<!-- end of main content -->
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>