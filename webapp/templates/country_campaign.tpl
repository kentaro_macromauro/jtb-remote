{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->

  <div class="header-slider-odd">
  	<img src="images/banner/{$campaign_id}-1.jpg" alt="" />
  </div>
  
  <!--container-left-->
  <div class="container-center">
    {$breadcamp}
    
    <!--content detail-->
     <div class="header-text-tittle">{$campaign_tittle}</div>
      <div class="text-content">
      	{$campaign_content}
      </div>
      <div  class="height10"></div>     
      {$campaign_list} 
    <!--content detail-->
  </div>
  <!--container-left-->
    <div class="clear"></div>
</div>
<!--container-->
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--body-->
</body>
</html>