{if $config.country eq "TWN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'TPE' : $tagtittle  = '台北観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの台北観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '台北観光,台北オプショナルツアー,台北現地ツアー,マイバス,JTB';             
        break;
        			
        case 'KAO' : $tagtittle  = '高雄観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの高雄観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '高雄観光,高雄オプショナルツアー,高雄現地ツアー,マイバス,JTB';
        break;
        case 'HUA' : $tagtittle  = '花蓮観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの花蓮観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '花蓮観光,花蓮オプショナルツアー,花蓮現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,マイバス,JTB'; 
}
{/php}
{elseif $config.country eq "IDN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'DPS' : $tagtittle  = 'バリ観光・オプショナルツアーならJTBマイバスサイト';
         			 $tagdesc    = 'JTBのバリ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バリ観光,バリオプショナルツアー,バリ現地ツアー,マイバス,JTB';
        break;
        case 'CGK' : $tagtittle  = 'ジャカルタ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのジャカルタ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'ジャカルタ観光,ジャカルタオプショナルツアー,ジャカルタ現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = 'インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのインドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'インドネシア観光,インドネシアオプショナルツアー,インドネシア現地ツアー,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = 'バリ島/インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのバリ島/インドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'バリ島/インドネシア観光,バリ島/インドネシアオプショナルツアー,バリ島/インドネシア現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "THA"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'BKK' : $tagtittle  = 'バンコク観光・オプショナルツアーならJTBマイバスサイト'; 		
        		     $tagdesc    = 'JTBのバンコク観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バンコク観光,バンコクオプショナルツアー,バンコク現地ツアー,マイバス,JTB';		 
        break;
        case 'PYX' : $tagtittle  = 'パタヤ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのパタヤ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'パタヤ観光,パタヤオプショナルツアー,パタヤ現地ツアー,マイバス,JTB';			     
        break;
        case 'CNX' : $tagtittle  = 'チェンライ・チェンマイ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのチェンライ・チェンマイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'チェンライ・チェンマイ観光,チェンライ・チェンマイオプショナルツアー,チェンライ・チェンマイ現地ツアー,マイバス,JTB';
        break;
        case 'HKT' : $tagtittle  = 'プーケット観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのプーケット観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'プーケット観光,プーケットオプショナルツアー,プーケット現地ツアー,マイバス,JTB';		 
        break;
        case 'USM' : $tagtittle  = 'サムイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのサムイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'サムイ観光,サムイオプショナルツアー,サムイ現地ツアー,マイバス,JTB';			 
        break;
        case 'KVR' : $tagtittle  = 'クラビ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクラビ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'クラビ観光,クラビオプショナルツアー,クラビ現地ツアー,マイバス,JTB'; 				 
        break;
        default    : $tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,マイバス,JTB';    
}
{/php}
{elseif $config.country eq "VNM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'SGN' : $tagtittle  = 'ホーチミン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのホーチミン観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';	
                     $tagkeyword = 'ホーチミン観光,ホーチミンオプショナルツアー,ホーチミン現地ツアー,マイバス,JTB';	     
        break;
        case 'HAN' : $tagtittle  = 'ハノイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのハノイ観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = 'ハノイ観光,ハノイオプショナルツアー,ハノイ現地ツアー,マイバス,JTB';				 
        break;
        case 'OTH' : $tagtittle  = '中部ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの中部ベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = '中部ベトナム観光,中部ベトナムオプショナルツアー,中部ベトナム現地ツアー,マイバス,JTB';			 
        break;
        default    : $tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 				 
        			 $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "KHM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'REP' : $tagtittle  = 'シェムリアップ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシェムリアップ観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'シェムリアップ観光,シェムリアップオプショナルツアー,シェムリアップ現地ツアー,マイバス,JTB';
                     	    
        break;
        default    : $tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
   					 $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,マイバス,JTB';			
        break;
    }
}
else
{
	$tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "MYS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'KUL' : $tagtittle  = 'クアラルンプール観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクアラルンプール観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'クアラルンプール観光,クアラルンプールオプショナルツアー,クアラルンプール現地ツアー,マイバス,JTB';
        break;
        case 'PEN' : $tagtittle  = 'ペナン観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのペナン観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ペナン観光,ペナンオプショナルツアー,ペナン現地ツアー,マイバス,JTB'; 	    	
        break;
        case 'LGK' : $tagtittle  = 'ランカウイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのランカウイ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ランカウイ観光,ランカウイオプショナルツアー,ランカウイ現地ツアー,マイバス,JTB';	    
        break;
        case 'BKI' : $tagtittle  = 'コタキナバル観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのコタキナバル観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'コタキナバル観光,コタキナバルオプショナルツアー,コタキナバル現地ツアー,マイバス,JTB';	    
        break;
        case 'MLC' : $tagtittle  = 'マラッカ観光・オプショナルツアーならJTBマイバスサイト';  	
        			 $tagdesc    = 'JTBのマラッカ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マラッカ観光,マラッカオプショナルツアー,マラッカ現地ツアー,マイバス,JTB';	    	
        break;
        default    : $tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マイバス,JTB';	
        break;
    }
}
else
{
	$tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マイバス,JTB';
     
}
{/php}
{elseif $config.country eq "SGP"}
{php}
	$tagtittle  = 'シンガポール観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのシンガポール観光・現地オプショナルツアーブランドである「マイバス」シンガポールの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'シンガポール観光,シンガポールオプショナルツアー,シンガポール現地ツアー,マイバス,JTB';
    
{/php}
{elseif $config.country eq "HKG"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'MAC' : $tagtittle  = 'マカオ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマカオ観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = 'マカオ観光,マカオオプショナルツアー,現地ツアー,マイバス,JTB';
        break;
        case 'SHZ' : $tagtittle  = '深圳観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの深圳観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '深圳観光,深圳オプショナルツアー,深圳現地ツアー,マイバス,JTB';     
        break;
        case 'GGZ' : $tagtittle  = '広州観光・オプショナルツアーならJTBマイバスサイト';      
        			 $tagdesc    = 'JTBの広州観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '広州観光,広州オプショナルツアー,広州現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト'; 		
        			 $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "AUS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'SYN' : $tagtittle  = 'シドニー観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシドニー観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'シドニー観光,シドニーオプショナルツアー,シドニー現地ツアー,マイバス,JTB';			
        break;
        case 'GOC' : $tagtittle  = 'ゴールドコースト観光・オプショナルツアーならJTBマイバスサイト'; 
       				 $tagdesc    = 'JTBのゴールドコースト観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ゴールドコースト観光,ゴールドコーストオプショナルツアー,ゴールドコースト現地ツアー,マイバス,JTB';		
        break;
        case 'CAN' : $tagtittle  = 'ケアンズ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのケアンズ観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ケアンズ観光,ケアンズオプショナルツアー,ケアンズ現地ツアー,マイバス,JTB';			
        break;
        case 'MEL' : $tagtittle  = 'メルボルン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのメルボルン観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'メルボルン観光,メルボルンオプショナルツアー,メルボルン現地ツアー,マイバス,JTB';	 		
        break;
        case 'PER' : $tagtittle  = 'パース観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのパース観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'パース観光,パースオプショナルツアー,パース現地ツアー,マイバス,JTB';			
        break;
        case 'AYR' : $tagtittle  = 'エアーズロック観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのエアーズロック観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'エアーズロック観光,エアーズロックオプショナルツアー,エアーズロック現地ツアー,マイバス,JTB';		
        break;
        default    : $tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "NZL"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'AKL' : $tagtittle  = 'オークランド観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのオークランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'オークランド観光,オークランドオプショナルツアー,オークランド現地ツアー,マイバス,JTB';	
        break;
        case 'CHC' : $tagtittle  = 'クライストチャーチ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクライストチャーチ観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クライストチャーチ観光,クライストチャーチオプショナルツアー,クライストチャーチ現地ツアー,マイバス,JTB';
        break;
        case 'ZQN' : $tagtittle  = 'クイーンズタウン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクイーンズタウン観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クイーンズタウン観光,クイーンズタウンオプショナルツアー,クイーンズタウン現地ツアー,マイバス,JTB';	  
        break;
        case 'ROT' : $tagtittle  = 'ロトルア観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのロトルア観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'ロトルア観光,ロトルアオプショナルツアー,ロトルア現地ツアー,マイバス,JTB';			  
        break;
        case 'MON' : $tagtittle  = 'マウントクック観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのマウントクック観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'マウントクック観光,マウントクックオプショナルツアー,マウントクック現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,マイバス,JT '; 
        break;
    }
}
else
{
	$tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "PHL"}
{php}
    if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
    {
    switch ($_REQUEST[inp_city])
    {
    case 'CEB' : $tagtittle  = 'セブ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのセブ観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'セブ観光,セブプショナルツアー,現地ツアー,マイバス,JTB';
    break;
    case 'GWU' : $tagtittle  = 'ボホール観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのボホール観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ボホール観光,ボホールオプショナルツアー,ボホール現地ツアー,マイバス,JTB';
    break;
    case 'MNL' : $tagtittle  = 'マニラ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのマニラ観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'マニラ観光,マニラオプショナルツアー,マニラ現地ツアー,マイバス,JTB';
    break;
    default    : $tagtittle  = 'フィリピン観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのフィリピン観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'フィリピン観光,フィリピンオプショナルツアー,フィリピン現地ツアー,マイバス,JTB';
    break;
    }
    }
    else
    {
    $tagtittle  = 'フィリピン観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのフィリピン観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'フィリピン観光,フィリピンオプショナルツアー,フィリピン現地ツアー,マイバス,JTB';
    }
{/php}
{elseif $config.country eq "MMR"}
    {php}
        if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
        {
        switch ($_REQUEST[inp_city])
        {
        case 'RGN' : $tagtittle  = 'ヤンゴン観光・オプショナルツアーならJTBマイバスサイト';
        $tagdesc    = 'JTBのヤンゴン観光・現地オプショナルツアーブランドである「マイバス」ミャンマーの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ヤンゴン観光,ヤンゴンプショナルツアー,現地ツアー,マイバス,JTB';
        break;
        }
        }
        else
        {
        $tagtittle  = 'ミャンマー観光・オプショナルツアーならJTBマイバスサイト';
        $tagdesc    = 'JTBのミャンマー観光・現地オプショナルツアーブランドである「マイバス」ミャンマーの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ミャンマー観光,ミャンマーオプショナルツアー,ミャンマー現地ツアー,マイバス,JTB';
        }
    {/php}
{/if}

{php}
switch ($_SERVER[REQUEST_URI]) {
  case  '/taiwan/souvenir.php'			:	
  		$tagtittle 	   = '台湾おみやげならJTBマイバスサイト';
        $tagdesc	   = '台湾のおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = '台湾おみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/indonesia/souvenir.php'		: 	
    	$tagtittle 	   = 'バリ島/インドネシアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'バリ島/インドネシアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'バリ島/インドネシアおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/thailand/souvenir.php'		: 	
    	$tagtittle 	   = 'タイおみやげならJTBマイバスサイト';
        $tagdesc	   = 'タイのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'タイおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/vietnam/souvenir.php'		:	
    	$tagtittle 	   = 'ベトナムおみやげならJTBマイバスサイト';
        $tagdesc	   = 'ベトナムのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ベトナムおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/malaysia/souvenir.php'		:
    	$tagtittle 	   = 'マレーシアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'マレーシアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'マレーシアおみやげ,アジアおみやげ,マイバス,JTB'; 	
  break;
  case  '/singapore/souvenir.php'		: 	
    	$tagtittle 	   = 'シンガポールおみやげならJTBマイバスサイト';
        $tagdesc	   = 'シンガポールのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'シンガポールおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/australia/souvenir.php'		: 	
    	$tagtittle 	   = 'オーストラリアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'オーストラリアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'オーストラリアおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/newzealand/souvenir.php'	: 	
    	$tagtittle 	   = 'ニュージーランドおみやげならJTBマイバスサイト';
        $tagdesc	   = 'ニュージーランドのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ニュージーランドおみやげ,アジアおみやげ,マイバス,JTB';
  break;
}

{/php}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{if $patetittle eq "product"}{$product.product_name_jp}{elseif $patetittle eq  "product_souvenir"}{$product.souvenir_name} - {if $config.country eq "TWN"}台湾{elseif $config.country eq "IDN"}バリ島/インドネシア{elseif $config.country eq "THA"}タイ{elseif $config.country eq "VNM"}ベトナム{elseif $config.country eq "KHM"}カンボジア{elseif $config.country eq "MYS"}マレーシア{elseif $config.country eq "SGP"}シンガポール{elseif $config.country eq "HKG"}香港{elseif $config.country eq "AUS"}オーストラリア{elseif $config.country eq "NZL"}ニュージーランド{/if}おみやげならJTBマイバスサイト{else}{php}echo $tagtittle;{/php}{/if}</title>
    <meta name="Description" content="{if $patetittle eq "product_souvenir"}{$product.short_desc|strip}{else}{php} echo $tagdesc;{/php}{/if}" />
    <meta name="Keywords" content="{if $patetittle eq "product_souvenir"}{if $config.country eq "TWN"}台湾おみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "IDN"}バリ島/インドネシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "THA"}タイおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "VNM"}ベトナムおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "MYS"}マレーシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "SGP"}シンガポールおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "AUS"}オーストラリアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "NZL"}ニュージーランドおみやげ,アジアおみやげ,マイバス,JTB{/if}{else}{php} echo $tagkeyword;{/php}{/if}" />
    <link rel="shortcut icon" href="/ico/favicon.ico" />
    <link rel="apple-touch-icon" href="http://www.mybus-asia.com/apple-touch-icon.png" />

    <link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript">
        // $ 関数および jQuery関数の上書きを元に戻します。
        var $j = jQuery.noConflict(true);
    </script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>

    <link rel="stylesheet" href="{$config.documentroot}common/css/non-res-style.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Arimo|Signika:400,600' rel='stylesheet' type='text/css'>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/nivo-slider.css" type="text/css" media="screen" />

    <link href='//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.min.css' rel='stylesheet'/>


<!--
<link type="text/css" href="{$config.documentroot}common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
<link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
-->
<!--
{if $souvenir_form}
<script type="text/javascript" src="{$config.documentroot}common/js/souvenir_form.js"></script>
{else}
<script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>
{/if}

<script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>


{if $product_review eq 1}
<script type="text/javascript" src="{$config.documentroot}common/js/jquery.lightbox-0.5.min.js"></script>
<link type="text/css" href="{$config.documentroot}common/jquery.lightbox-0.5.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="{$config.documentroot}common/js/start_lightbox.js"></script>
{/if}


{if $config.country eq "TWN" }
{literal}
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32237848-1', 'auto');

</script>
{/literal}
{/if}
-->
    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
        <!-- End Google Tag Manager -->
    {/literal}
</head>
<body>


<!--body-->
<!--header-->
<header>
    <div class="container">

        <table class="rows">
            <tr>
                <td>
                    <div class="col-md-4">
                        <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
                    </div>
                </td>
                <td>
                    <div class="col-md-6">
                        {if $basket_count}
                            <div class="red-text-top" align="right"><p><a href="http://{$smarty.server.HTTP_HOST}/{$config.environment}{$config.countryname}/souvenir_cart.php"><img src="/images/souvenir/shopping-cart.png" alt="my bus" style="margin-top: -7px;" />{$basket_count}個の商品が入っています。</a></p></div>
                        {/if}
                    </div>
                </td>
                <td>
                    <div class="col-md-2">
                        <div class="red-text-top" align="right">
                            <a href="page.php?id=tourist_desk.html" class="btn btn-grey-new" style="">現地デスクはこちら</a>
                        </div>
                    </div>
                </td>
            </tr>
        </table>

        <table class="rows">
            <tr>
                <td><div class="col-md-2">
                        <div class="navbar-brand">
                            <a  href="{$config.documentroot}{$config.environment}" class="desktops">
                                <img src="{$config.documentroot}images/logo/mybuslogo2.jpg" class="img-responsive" alt="Responsive image"/>
                            </a>
                        </div>
                    </div></td>

                <td><div class="col-md-5 padding-cn">
                        <p><b> {if $config.country eq "TWN"}
                                    台湾
                                {elseif $config.country eq "IDN"}
                                    バリ島/インドネシア
                                {elseif $config.country eq "THA"}
                                    タイ
                                {elseif $config.country eq "VNM"}
                                    ベトナム
                                {elseif $config.country eq "KHM"}
                                    カンボジア
                                {elseif $config.country eq "MYS"}
                                    マレーシア
                                {elseif $config.country eq "SGP"}
                                    シンガポール
                                {elseif $config.country eq "HKG"}
                                    香港
                                {elseif $config.country eq "AUS"}
                                    オーストラリア
                                {elseif $config.country eq "NZL"}
                                    ニュージーランド
                                {/if}</b></p>
                    </div></td>

                <td><div class="col-md-5">
                        <table class="rows social-top">
                            <tr>
                                <td><div class="col-md-4ss">
                                        <table class="rows">
                                            <td><div class="col-md-2s">
                                                    <div align="right" style="margin-right: -15px;"><img src="{$config.documentroot}images/logo/jtb-perfect-moment.jpg" class="img-responsive" alt="Responsive image" style="margin-top: 6px;"/></div>
                                                </div></td>

                                            <td><div class="col-md-2s" style="width: 80px;">
                                                    <p style="padding-top: 23px; padding-right: 10px;"><a href="page.php?id=qa.html" style="font-size: 10px; text-align: right; color: #3212f3;">FAQ</a></p>
                                                </div></td>
                                        </table>
                                    </div></td>

                                <td><div class="col-md-2s">
                                        <div class="round-social-color">
                                            <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                                            <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div></td>
            </tr>
        </table>
    </div>


    <div class="header-second">
        <div class="container grey-bg">
            <div id="ribon-red-desktop-sps2"></div>
            <div class="row menu">

                <div class="col-sm-12" style="margin-top: -1px;">
                    <div class="navbar-header">
                        <div id="navbar" class="navbar-collapse">
                            <ul class="nav navbar-nav navbar-right" style="font-size:12px">
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/" {$menu_selected_style.top}"><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;TOP</a></li>
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/opt.php" {$menu_selected_style.opt}><img src="{$config.documentroot}images/menu/red-btn3-white.png"> &nbsp;オプショナルツアー</a></li>
                                {if $config.country eq "TWN"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/TWN/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "IDN"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/INA/index.html" {$menu_selected_style.hotel} class="idn_menu"><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "THA"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/THA/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "VNM"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/VIE/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "MYS"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/MAS/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "AUS"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/AUS/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "SGP"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/SIN/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
                                {if $config.country eq "NZL"}
                                    <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/NZL/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                                {/if}
<!--
                                <li><a href="#"><img src="{$config.documentroot}images/menu/red-btn3-noround.png" {$menu_selected_style.resutaurant}> &nbsp;レストラン</a></li>
-->
                                {if $config.country !== "KHM" && $config.country !== "PHL" && $config.country !== "MMR"}
                                    <li class="dropdown"><a class="{if $config.country == "IDN"}idn_menu{/if}" href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir.php" {$menu_selected_style.souvenir}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;おみやげ{if $config.country == "IDN"}<span style=font-size:10px>(現地受取)</span>{/if}</a>
                                        <ul class="dropdown-menu">
                                            {if $config.country == "THA" ||
                                            $config.country == "IDN" ||
                                            $config.country == "VNM" ||
                                            $config.country == "MYS" ||
                                            $config.country == "NZL"}
                                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=1"><img src="/images/menu/red-btn3-noround.png"> &nbsp;現地受取</a></li>
                                            {else}
                                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=1"><img src="/images/menu/red-btn3-noround.png"> &nbsp;現地受取</a></li>
                                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=2"><img src="/images/menu/red-btn3-noround.png"> &nbsp;日本受取</a></li>
                                            {/if}
                                            {if $config.country eq 'TWN' ||
                                            $config.country eq 'SGP' ||
                                            $config.country eq 'NZL' ||
                                            $config.country eq 'MYS' ||
                                            $config.country eq 'IDN' ||
                                            $config.country eq 'AUS' ||
                                            $config.country eq 'VNM' ||
                                            $config.country eq 'THA'}
                                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=user_guide.html"><img src="/images/menu/red-btn3-noround.png"> &nbsp;利用ガイド</a></li>
                                            {/if}
                                        </ul>
                                    </li>
                                {/if}
                                <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/info_top.php" {$menu_selected_style.local}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;現地情報</a></li>
                                {if $config.country eq "AUS" || $config.country eq "NZL" || $config.country eq "MYS" || $config.country eq "KHM" }
                                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                                {elseif $config.country eq "SGP" || $config.country eq "TWN" || $config.country eq "THA" || $config.country eq "VNM" }
                                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog2.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                                {elseif $config.country eq "IDN"}
                                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog3.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                                {/if}
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- rownya -->

        </div>
    </div>
</header>


<!--
<div class="header">
	<a class="logo_text_link" href="http://{$smarty.server.HTTP_HOST}">海外観光・オプショナルツアーならJTB</a>
	<a class="mybus-logo" href="http://{$smarty.server.HTTP_HOST}">
    	<img src="{$config.documentroot}images/mybuslogo.jpg" alt="Mybus Logo"  /> 
 	</a>
  <div class="country_name" style="left:165px; top:70px;">
    {if $config.country eq "TWN"}
    台湾
    {elseif $config.country eq "IDN"}
    バリ島/インドネシア
    {elseif $config.country eq "THA"}
    タイ
    {elseif $config.country eq "VNM"}
    ベトナム
    {elseif $config.country eq "KHM"}
    カンボジア
    {elseif $config.country eq "MYS"}
    マレーシア
    {elseif $config.country eq "SGP"}
    シンガポール
    {elseif $config.country eq "HKG"}
    香港
    {elseif $config.country eq "AUS"}
    オーストラリア
    {elseif $config.country eq "NZL"}
    ニュージーランド
    {/if}
  </div>
  
  
  <div style="position:absolute; right:70px; top:5px; width:500px;" >
  	{if $basket_count}
     <a class="ico_basket" href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/souvenir_cart.php" {if $config.basket eq true} style="width:285px;" {/if}>{$basket_count}個の商品が入っています</a>
    {/if}
  
  	{if $config.basket eq true}
  	<a class="ico_cart" href="http://{$smarty.server.HTTP_HOST}/{$config.countryname}/booking_cart.php">買い物カゴを見る</a>
    {/if}
    
  </div>
  
  
  
  
  {if $smarty.server.HTTPS eq "on"}
  {else}
    <ul class="social_network">
       <li><span class='st_facebook_hcount' displayText='Facebook'></span></li>
       <li><span class='st_twitter_hcount' displayText='Tweet'></span></li>
       <li><a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="a0028392a9c4e9ac68bccb2f09f14f59875140c6" data-button="button-1">Check</a><script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script>
       </li>
   </ul>
   {/if}

   
    <div style="position:absolute; right:205px; top:34px; ">
       <img alt="" src="../images/jtblogo.jpg" />
    </div>
   
   
  <div class="header_link" style="width:780px;">
  	
    <div class="header_link_right"></div>
    <div class="header_link_center">
      <ul>
        <li><a href="http://{$smarty.server.HTTP_HOST}/page.php?id=branch.html">会社概要</a></li>
        <li><a href="http://{$smarty.server.HTTP_HOST}/page.php?id=sitemap.html">サイトマップ</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <div class="header_link_left"></div>
    <div class="clear"></div>
  </div>
  <div class="header_link2">
    <ul>
      <li><a href="http://{$smarty.server.HTTP_HOST}/index.php">JTBアジアパシフィックTOPへ</a>　</li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/souvenir.php">海外おみやげTOPへ</a></li>
      <li><a href="http://{$smarty.server.HTTP_HOST}/opt.php">オプショナルツアーTOPへ</a></li>
    </ul>
  </div>
</div>
<!--header-->
