{literal}
<script>
    var product_data = {/literal}{$order_info}{literal};
    dataLayer = product_data;
</script>
{/literal}
{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <div class="clear"></div>
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
        <ul class="booking-nav-process {if $config.allot != 'false'}limit4{/if}">

          <li id="step1">希望日選択</li>
          <li id="step2">カート</li>
           
          <li id="step3">予約情報入力</li>
          <li id="step4" {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'} class="active_last" {/if} >予約情報確認</li>
            
          {if $status_book == '1' || $status_book == '2' }
          <li id="step5"  {if $status_book == '1' ||  $status_book == '2' || $status_book == '3'} class="active_last" {/if}  >お支払い方法の選択</li>
          {/if}
          
          
            
          {if $status_book == '1' }
          <li id="step6">決済情報確認</li>
          <li id="step7" class="active_last">決済情報確認</li>
          {/if}
            
          <li id="step8" class="active">完了</li>  
          
        </ul>
        <div class="clear"></div>
        <span class="texttittle">完了</span>
        <div class="height10"></div>
        {if $option == "opt"}
        <p>お申し込みありがとうございました。<br/>
          集合場所と時間に関しては追って弊社スタッフよりご連絡をさせていただきます。</p>
        {else}
        <p> お申し込みありがとうございました。<br/>
          集合場所と時間に関しては追って弊社スタッフよりご連絡をさせていただきます。 </p>
        {/if}<br/>
        <p> <a href="booking_cart.php">買い物カゴを見る</a></p>
        <p> <a href="index.php">ページTOPに戻る</a></p> </div>
    </div>
    </div>
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
</div>
<!--container-->
<!--footer-->
{include file="country_non_res-footer.tpl" }
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->
<!--body-->
</body></html>