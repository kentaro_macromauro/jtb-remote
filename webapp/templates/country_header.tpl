{if $config.country eq "TWN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'TPE' : $tagtittle  = '台北観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの台北観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '台北観光,台北オプショナルツアー,台北現地ツアー,マイバス,JTB';             
        break;
        			
        case 'KAO' : $tagtittle  = '高雄観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの高雄観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '高雄観光,高雄オプショナルツアー,高雄現地ツアー,マイバス,JTB';
        break;
        case 'HUA' : $tagtittle  = '花蓮観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの花蓮観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = '花蓮観光,花蓮オプショナルツアー,花蓮現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = '台湾観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの台湾観光・現地オプショナルツアーブランドである「マイバス」台湾の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '台湾観光,台湾オプショナルツアー,台湾現地ツアー,マイバス,JTB'; 
}
{/php}
{elseif $config.country eq "IDN"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'DPS' : $tagtittle  = 'バリ観光・オプショナルツアーならJTBマイバスサイト';
         			 $tagdesc    = 'JTBのバリ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バリ観光,バリオプショナルツアー,バリ現地ツアー,マイバス,JTB';
        break;
        case 'CGK' : $tagtittle  = 'ジャカルタ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのジャカルタ観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'ジャカルタ観光,ジャカルタオプショナルツアー,ジャカルタ現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = 'インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのインドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'インドネシア観光,インドネシアオプショナルツアー,インドネシア現地ツアー,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = 'バリ島/インドネシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのバリ島/インドネシア観光・現地オプショナルツアーブランドである「マイバス」インドネシア（バリ・ジャカルタ）の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'バリ島/インドネシア観光,バリ島/インドネシアオプショナルツアー,バリ島/インドネシア現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "THA"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'BKK' : $tagtittle  = 'バンコク観光・オプショナルツアーならJTBマイバスサイト'; 		
        		     $tagdesc    = 'JTBのバンコク観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'バンコク観光,バンコクオプショナルツアー,バンコク現地ツアー,マイバス,JTB';		 
        break;
        case 'PYX' : $tagtittle  = 'パタヤ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのパタヤ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'パタヤ観光,パタヤオプショナルツアー,パタヤ現地ツアー,マイバス,JTB';			     
        break;
        case 'CNX' : $tagtittle  = 'チェンライ・チェンマイ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのチェンライ・チェンマイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'チェンライ・チェンマイ観光,チェンライ・チェンマイオプショナルツアー,チェンライ・チェンマイ現地ツアー,マイバス,JTB';
        break;
        case 'HKT' : $tagtittle  = 'プーケット観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのプーケット観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'プーケット観光,プーケットオプショナルツアー,プーケット現地ツアー,マイバス,JTB';		 
        break;
        case 'USM' : $tagtittle  = 'サムイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのサムイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'サムイ観光,サムイオプショナルツアー,サムイ現地ツアー,マイバス,JTB';			 
        break;
        case 'KVR' : $tagtittle  = 'クラビ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクラビ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'クラビ観光,クラビオプショナルツアー,クラビ現地ツアー,マイバス,JTB'; 				 
        break;
        default    : $tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    				 $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,マイバス,JTB'; 
        break;
    }
}
else
{
	$tagtittle  = 'タイ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのタイ観光・現地オプショナルツアーブランドである「マイバス」タイの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'タイ観光,タイオプショナルツアー,タイ現地ツアー,マイバス,JTB';    
}
{/php}
{elseif $config.country eq "VNM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'SGN' : $tagtittle  = 'ホーチミン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのホーチミン観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';	
                     $tagkeyword = 'ホーチミン観光,ホーチミンオプショナルツアー,ホーチミン現地ツアー,マイバス,JTB';	     
        break;
        case 'HAN' : $tagtittle  = 'ハノイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのハノイ観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = 'ハノイ観光,ハノイオプショナルツアー,ハノイ現地ツアー,マイバス,JTB';				 
        break;
        case 'OTH' : $tagtittle  = '中部ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの中部ベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。'; 	
                     $tagkeyword = '中部ベトナム観光,中部ベトナムオプショナルツアー,中部ベトナム現地ツアー,マイバス,JTB';			 
        break;
        default    : $tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト'; 				 
        			 $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'ベトナム観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのベトナム観光・現地オプショナルツアーブランドである「マイバス」ベトナムの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ベトナム観光,ベトナムオプショナルツアー,ベトナム現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "KHM"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'REP' : $tagtittle  = 'シェムリアップ観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシェムリアップ観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
                     $tagkeyword = 'シェムリアップ観光,シェムリアップオプショナルツアー,シェムリアップ現地ツアー,マイバス,JTB';
                     	    
        break;
        default    : $tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
   					 $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,マイバス,JTB';			
        break;
    }
}
else
{
	$tagtittle  = 'カンボジア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのカンボジア観光・現地オプショナルツアーブランドである「マイバス」カンボジアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'カンボジア観光,カンボジアオプショナルツアー,カンボジア現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "MYS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'KUL' : $tagtittle  = 'クアラルンプール観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクアラルンプール観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'クアラルンプール観光,クアラルンプールオプショナルツアー,クアラルンプール現地ツアー,マイバス,JTB';
        break;
        case 'PEN' : $tagtittle  = 'ペナン観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのペナン観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ペナン観光,ペナンオプショナルツアー,ペナン現地ツアー,マイバス,JTB'; 	    	
        break;
        case 'LGK' : $tagtittle  = 'ランカウイ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのランカウイ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ランカウイ観光,ランカウイオプショナルツアー,ランカウイ現地ツアー,マイバス,JTB';	    
        break;
        case 'BKI' : $tagtittle  = 'コタキナバル観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのコタキナバル観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'コタキナバル観光,コタキナバルオプショナルツアー,コタキナバル現地ツアー,マイバス,JTB';	    
        break;
        case 'MLC' : $tagtittle  = 'マラッカ観光・オプショナルツアーならJTBマイバスサイト';  	
        			 $tagdesc    = 'JTBのマラッカ観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マラッカ観光,マラッカオプショナルツアー,マラッカ現地ツアー,マイバス,JTB';	    	
        break;
        default    : $tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マイバス,JTB';	
        break;
    }
}
else
{
	$tagtittle  = 'マレーシア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのマレーシア観光・現地オプショナルツアーブランドである「マイバス」マレーシアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'マレーシア観光,マレーシアオプショナルツアー,マレーシア現地ツアー,マイバス,JTB';
     
}
{/php}
{elseif $config.country eq "SGP"}
{php}
	$tagtittle  = 'シンガポール観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのシンガポール観光・現地オプショナルツアーブランドである「マイバス」シンガポールの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'シンガポール観光,シンガポールオプショナルツアー,シンガポール現地ツアー,マイバス,JTB';
    
{/php}
{elseif $config.country eq "HKG"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'MAC' : $tagtittle  = 'マカオ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのマカオ観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = 'マカオ観光,マカオオプショナルツアー,現地ツアー,マイバス,JTB';
        break;
        case 'SHZ' : $tagtittle  = '深圳観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBの深圳観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '深圳観光,深圳オプショナルツアー,深圳現地ツアー,マイバス,JTB';     
        break;
        case 'GGZ' : $tagtittle  = '広州観光・オプショナルツアーならJTBマイバスサイト';      
        			 $tagdesc    = 'JTBの広州観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
         			 $tagkeyword = '広州観光,広州オプショナルツアー,広州現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト'; 		
        			 $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,マイバス,JTB';
        break;
    }
}
else
{
	$tagtittle  = '香港観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBの香港観光・現地オプショナルツアーブランドである「マイバス」香港の販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = '香港観光,香港オプショナルツアー,香港現地ツアー,マイバス,JTB';
    
}
{/php}
{elseif $config.country eq "AUS"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'SYN' : $tagtittle  = 'シドニー観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのシドニー観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'シドニー観光,シドニーオプショナルツアー,シドニー現地ツアー,マイバス,JTB';			
        break;
        case 'GOC' : $tagtittle  = 'ゴールドコースト観光・オプショナルツアーならJTBマイバスサイト'; 
       				 $tagdesc    = 'JTBのゴールドコースト観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ゴールドコースト観光,ゴールドコーストオプショナルツアー,ゴールドコースト現地ツアー,マイバス,JTB';		
        break;
        case 'CAN' : $tagtittle  = 'ケアンズ観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのケアンズ観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'ケアンズ観光,ケアンズオプショナルツアー,ケアンズ現地ツアー,マイバス,JTB';			
        break;
        case 'MEL' : $tagtittle  = 'メルボルン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのメルボルン観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'メルボルン観光,メルボルンオプショナルツアー,メルボルン現地ツアー,マイバス,JTB';	 		
        break;
        case 'PER' : $tagtittle  = 'パース観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのパース観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'パース観光,パースオプショナルツアー,パース現地ツアー,マイバス,JTB';			
        break;
        case 'AYR' : $tagtittle  = 'エアーズロック観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのエアーズロック観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'エアーズロック観光,エアーズロックオプショナルツアー,エアーズロック現地ツアー,マイバス,JTB';		
        break;
        default    : $tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト'; 
        			 $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
   					 $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,マイバス,JTB';		
        break;
    }
}
else
{
	$tagtittle  = 'オーストラリア観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのオーストラリア観光・現地オプショナルツアーブランドである「マイバス」オーストラリアの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'オーストラリア観光,オーストラリアオプショナルツアー,オーストラリア現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "NZL"}
{php}
if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
{
	switch ($_REQUEST[inp_city])
    {
    	case 'AKL' : $tagtittle  = 'オークランド観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのオークランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'オークランド観光,オークランドオプショナルツアー,オークランド現地ツアー,マイバス,JTB';	
        break;
        case 'CHC' : $tagtittle  = 'クライストチャーチ観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクライストチャーチ観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クライストチャーチ観光,クライストチャーチオプショナルツアー,クライストチャーチ現地ツアー,マイバス,JTB';
        break;
        case 'ZQN' : $tagtittle  = 'クイーンズタウン観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのクイーンズタウン観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'クイーンズタウン観光,クイーンズタウンオプショナルツアー,クイーンズタウン現地ツアー,マイバス,JTB';	  
        break;
        case 'ROT' : $tagtittle  = 'ロトルア観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのロトルア観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'ロトルア観光,ロトルアオプショナルツアー,ロトルア現地ツアー,マイバス,JTB';			  
        break;
        case 'MON' : $tagtittle  = 'マウントクック観光・オプショナルツアーならJTBマイバスサイト';
        			 $tagdesc    = 'JTBのマウントクック観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';  
                     $tagkeyword = 'マウントクック観光,マウントクックオプショナルツアー,マウントクック現地ツアー,マイバス,JTB';
        break;
        default    : $tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト'; 	
        			 $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    				 $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,マイバス,JT '; 
        break;
    }
}
else
{
	$tagtittle  = 'ニュージーランド観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのニュージーランド観光・現地オプショナルツアーブランドである「マイバス」ニュージーランドの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ニュージーラン観光,ニュージーランオプショナルツアー,ニュージーラン現地ツアー,マイバス,JTB';
}
{/php}
{elseif $config.country eq "PHL"}
{php}
    if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
    {
    switch ($_REQUEST[inp_city])
    {
    case 'CEB' : $tagtittle  = 'セブ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのセブ観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'セブ観光,セブプショナルツアー,現地ツアー,マイバス,JTB';
    break;
    case 'GWU' : $tagtittle  = 'ボホール観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのボホール観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'ボホール観光,ボホールオプショナルツアー,ボホール現地ツアー,マイバス,JTB';
    break;
    case 'MNL' : $tagtittle  = 'マニラ観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのマニラ観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'マニラ観光,マニラオプショナルツアー,マニラ現地ツアー,マイバス,JTB';
    break;
    default    : $tagtittle  = 'フィリピン観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのフィリピン観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'フィリピン観光,フィリピンオプショナルツアー,フィリピン現地ツアー,マイバス,JTB';
    break;
    }
    }
    else
    {
    $tagtittle  = 'フィリピン観光・オプショナルツアーならJTBマイバスサイト';
    $tagdesc    = 'JTBのフィリピン観光・現地オプショナルツアーブランドである「マイバス」フィリピンの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
    $tagkeyword = 'フィリピン観光,フィリピンオプショナルツアー,フィリピン現地ツアー,マイバス,JTB';
    }
{/php}
{elseif $config.country eq "MMR"}
    {php}
        if (strpos($_SERVER[SCRIPT_NAME],'search_opt'))
        {
        switch ($_REQUEST[inp_city])
        {
        case 'RGN' : $tagtittle  = 'ヤンゴン観光・オプショナルツアーならJTBマイバスサイト';
        $tagdesc    = 'JTBのヤンゴン観光・現地オプショナルツアーブランドである「マイバス」ミャンマーの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ヤンゴン観光,ヤンゴンプショナルツアー,現地ツアー,マイバス,JTB';
        default    : $tagtittle  = 'ミャンマー観光・オプショナルツアーならJTBマイバスサイト';
        $tagdesc    = 'JTBのミャンマー観光・現地オプショナルツアーブランドである「マイバス」ミャンマーの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ミャンマー観光,ミャンマーオプショナルツアー,ミャンマー現地ツアー,マイバス,JTB';
        break;
        }
        }
        else
        {
        $tagtittle  = 'ミャンマー観光・オプショナルツアーならJTBマイバスサイト';
        $tagdesc    = 'JTBのミャンマー観光・現地オプショナルツアーブランドである「マイバス」ミャンマーの販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ミャンマー観光,ミャンマーオプショナルツアー,ミャンマー現地ツアー,マイバス,JTB';
        }
    {/php}
{/if}

{php}
switch ($_SERVER[REQUEST_URI]) {
  case  '/taiwan/souvenir.php'			:	
  		$tagtittle 	   = '台湾おみやげならJTBマイバスサイト';
        $tagdesc	   = '台湾のおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = '台湾おみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/indonesia/souvenir.php'		: 	
    	$tagtittle 	   = 'バリ島/インドネシアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'バリ島/インドネシアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'バリ島/インドネシアおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/thailand/souvenir.php'		: 	
    	$tagtittle 	   = 'タイおみやげならJTBマイバスサイト';
        $tagdesc	   = 'タイのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'タイおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/vietnam/souvenir.php'		:	
    	$tagtittle 	   = 'ベトナムおみやげならJTBマイバスサイト';
        $tagdesc	   = 'ベトナムのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ベトナムおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/malaysia/souvenir.php'		:
    	$tagtittle 	   = 'マレーシアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'マレーシアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'マレーシアおみやげ,アジアおみやげ,マイバス,JTB'; 	
  break;
  case  '/singapore/souvenir.php'		: 	
    	$tagtittle 	   = 'シンガポールおみやげならJTBマイバスサイト';
        $tagdesc	   = 'シンガポールのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'シンガポールおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/australia/souvenir.php'		: 	
    	$tagtittle 	   = 'オーストラリアおみやげならJTBマイバスサイト';
        $tagdesc	   = 'オーストラリアのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'オーストラリアおみやげ,アジアおみやげ,マイバス,JTB';
  break;
  case  '/newzealand/souvenir.php'	: 	
    	$tagtittle 	   = 'ニュージーランドおみやげならJTBマイバスサイト';
        $tagdesc	   = 'ニュージーランドのおみやげ販売サイトです。JTBだからこそできる、多種多様で高品質な商品をベストプライスでご提供します。';
        $tagkeyword = 'ニュージーランドおみやげ,アジアおみやげ,マイバス,JTB';
  break;
}

{/php}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{if $patetittle eq "product"}{$product.product_name_jp}{elseif $patetittle eq  "product_souvenir"}{$product.souvenir_name} - {if $config.country eq "TWN"}台湾{elseif $config.country eq "IDN"}バリ島/インドネシア{elseif $config.country eq "THA"}タイ{elseif $config.country eq "VNM"}ベトナム{elseif $config.country eq "KHM"}カンボジア{elseif $config.country eq "MYS"}マレーシア{elseif $config.country eq "SGP"}シンガポール{elseif $config.country eq "HKG"}香港{elseif $config.country eq "AUS"}オーストラリア{elseif $config.country eq "NZL"}ニュージーランド{/if}おみやげならJTBマイバスサイト{else}{php}echo $tagtittle;{/php}{/if}</title>
    <meta name="Description" content="{if $patetittle eq "product_souvenir"}{$product.short_desc|strip}{else}{php} echo $tagdesc;{/php}{/if}" />
    <meta name="Keywords" content="{if $patetittle eq "product_souvenir"}{if $config.country eq "TWN"}台湾おみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "IDN"}バリ島/インドネシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "THA"}タイおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "VNM"}ベトナムおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "MYS"}マレーシアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "SGP"}シンガポールおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "AUS"}オーストラリアおみやげ,アジアおみやげ,マイバス,JTB{elseif $config.country eq "NZL"}ニュージーランドおみやげ,アジアおみやげ,マイバス,JTB{/if}{else}{php} echo $tagkeyword;{/php}{/if}" />
    <link rel="shortcut icon" href="/ico/favicon.ico" />
    <link rel="apple-touch-icon" href="http://www.mybus-asia.com/apple-touch-icon.png" />

    <link type="text/css" href="{$config.documentroot}common/initial.css" rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/form.css"  rel="stylesheet" media="all" />
    <link type="text/css" href="{$config.documentroot}common/country_info.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict(true);
    </script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.timers.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.topzindex.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/banner_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/main.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/home_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/index_slider.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/top_slider_product.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/form.js"></script>

    <script type="text/javascript" src="{$config.documentroot}common/js/jquery.hoverIntent.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/navigation.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/ppc_datepicker.js"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript" src="{$config.documentroot}common/js/social.js"></script>

    <link rel="stylesheet" href="{$config.documentroot}common/css/bootstrap2.css" />
    <link rel="stylesheet" href="{$config.documentroot}common/css/style-secondary.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Arimo|Signika:400,600' rel='stylesheet' type='text/css'>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="{$config.documentroot}common/css/nivo-slider/nivo-slider.css" type="text/css" media="screen" />

    <link href='//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.min.css' rel='stylesheet'/>

    {literal}
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WSMV6Z"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSMV6Z');</script>
        <!-- End Google Tag Manager -->
     {/literal}
</head>