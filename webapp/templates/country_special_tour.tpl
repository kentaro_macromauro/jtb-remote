{include file="country_special_tour_header.tpl" }

<body>
<!--body-->

{include file="country_header_menu.tpl" }

<div class="container home-content pull-container">
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="mybus-breadcrumb" style="padding: 0 20px 0 5px; font-family: 'Arial', sans-serif;">
            <p><a href="index.html" style="color: #000; font-size: 12px;">TOP</a>&nbsp;&nbsp;>&nbsp;&nbsp; <a href="http://www.mybus-asia.com/indonesia/" style="color: #000; font-size: 12px;">マレーシア</a></p>
            <div style="height: 0px;"></div>
        </div>
    </div>

</div>

<div><!--body content-->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <img src="{$config.documentroot}{$config.countryname}/images/special_content/header-img_v2.jpg" class="img-responsive" />

    </div>



</div>
<br />
<div class="menutitle">
    <p>コースプラン紹介</p>
</div>
<p class="front-msg">日本から約７時間、常夏のコタキナバルは、世界遺産から青い海でのマリンスポーツまで沢山の魅力でいっぱい！旅行をスムーズに無駄なくするためのオプショナルツアー、今回はとってもお得な組み合わせパックをご用意しました。</p>

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6" >
        <a href="special_content_detail.php?id=160427-01.html"><img src="{$config.documentroot}{$config.countryname}/images/special_content/cat-A.jpg" class="img-responsive" /></a>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <a href="special_content_detail.php?id=160427-02.html"><img src="{$config.documentroot}{$config.countryname}/images/special_content/cat-B.jpg" class="img-responsive" /></a>
    </div>
</div>
<br />

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6" >
        <a href="special_content_detail.php?id=160427-03.html"><img src="{$config.documentroot}{$config.countryname}/images/special_content/cat-C.jpg" class="img-responsive" /></a>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6" >
        <a href="special_content_detail.php?id=160427-04.html"><img src="{$config.documentroot}{$config.countryname}/images/special_content/cat-D.jpg" class="img-responsive" /></a>
    </div>
</div>

<br />
<div class="menutitle">
    <p>お得なプラン</p>
</div>


<div class="row plan-row">

    <div class="col-md-1 col-sm-1 col-xs-1">

    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <strong><i class="fa fa-angle-double-left"></i> 2 コースプラン <i class="fa fa-angle-double-right"></i></strong>
    </div>
</div>


<div class="row plan-row 1 ">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            1
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>A（キナバル公園）+　B（マングローブクルーズ） ＝大人RM600、子供RM330</p>
        <strong><font style="color:#FF0000;">大人RM100、子供RM30</font> もお得</strong>
    </div>
</div>


<div class="row plan-row 2">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            2
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>B（マングローブクルーズ）+　C（サピ島） ＝大人RM530、子供RM270</p>
        <strong><font style="color:#FF0000;">大人RM80、子供RM30</font> もお得</strong>
    </div>
</div>


<div class="row plan-row 3">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            3
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>B（マングローブクルーズ）+　D（ロッカウィー） ＝大人RM530、子供RM300</p>
        <strong><font style="color:#FF0000;">大人RM70、子供RM20</font> もお得</strong>
    </div>
</div>


<div class="row plan-row 4">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            4
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>A（キナバル公園）+　D（ロッカウィー） ＝大人RM520、子供RM300</p>
        <strong><font style="color:#FF0000;">大人RM60、子供RM20</font> もお得</strong>
    </div>
</div>


<div class="row plan-row 5">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            5
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>C（サピ島）+　D（ロッカウィー） ＝大人RM430、子供RM240</p>
        <strong><font style="color:#FF0000;">大人RM50、子供RM20</font> もお得</strong>
    </div>
</div>

<div class="row plan-row 6">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            6
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>A（キナバル公園）+　C（サピ島） ＝大人RM550、子供RM280</p>
        <strong><font style="color:#FF0000;">大人RM40、子供RM20</font> もお得</strong>
    </div>
</div>


<div class="row plan-row">

    <div class="col-md-1 col-sm-1 col-xs-1">

    </div>

    <div class="col-md-11 col-sm-11 col-xs-11" >
        <strong ><i class="fa fa-angle-double-left"></i> 3 コースプラン <i class="fa fa-angle-double-right"></i></strong>
    </div>
</div>

<div class="row plan-row 7">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            7
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>A(キナバル) + B（マングローブ）+ C（サピ） ＝大人RM820、子供RM410</p>
        <strong><font style="color:#FF0000;">大人RM130、子供RM70</font> もお得</strong>
    </div>
</div>

<div class="row plan-row 8">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            8
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>A（キナバル）+ B（マングローブ）+ D（ロッカウィー） ＝大人RM820、子供RM410</p>
        <strong><font style="color:#FF0000;">大人RM120、子供RM90</font> もお得</strong>
    </div>
</div>

<div class="row plan-row 9">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            9
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p>B（マングローブ）+ C (サピ) + D（ロッカウィー） ＝大人RM710、子供RM370</p>
        <strong><font style="color:#FF0000;">大人RM140、子供RM70</font> もお得</strong>
    </div>
</div>

<div class="row plan-row 10">

    <div class="col-md-1 col-sm-1 col-xs-1">
        <div class="plan-number">
            10
        </div>
    </div>

    <div class="col-md-11 col-sm-11 col-xs-11">
        <p >A（キナバル）+ C（サピ）+ D（ロッカウィー） ＝大人RM730、子供RM380</p>
        <strong><font style="color:#FF0000;">大人RM100、子供RM60</font> もお得</strong>
    </div>
</div>
<br />
<div class="menutitle">
    <p>予約フォーム</p>
</div>
<br />

<div class="plan-form">
    <form enctype="multipart/form-data" method="post" id="planform">
        <div class="row plan-row">
            <div class="col-md-4 col-sm-11 col-xs-11" style="padding-left:20px;padding-top:10px;">
                氏名　（漢字） <font style="color:#FF0000;">*必須</font>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                姓 <input name="family-name1" class="family-name1" type="text" placeholder="例）山田" style="width:70%;" />
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                名  <input name="name1" class="name1" type="text" placeholder="例）太郎" style="width:70%;" />
            </div>
        </div>

        <div class="row plan-row">
            <div class="col-md-4 col-sm-11 col-xs-11" style="padding-left:20px; padding-top:10px;">
                氏名　（フリガナ） <font style="color:#FF0000;">*必須</font>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                姓 <input name="family-name2" class="family-name2" type="text" placeholder="例）ヤマダ" style="width:70%;" />
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                名 <input name="name2" class="name2" type="text" placeholder="例）タロウ" style="width:70%;" />
            </div>
        </div>



        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px; padding-top:10px;">
                メールアドレス <font style="color:#FF0000;">*必須</font><font class="ket-text1" style="margin-left:20%; display:none;">*半角英数</font>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12" >
                &nbsp;&nbsp;&nbsp;<input name="email" class="email" type="email" placeholder="" style="width:70%;" />  <font class="ket-text2" style="color:#FF0000;">*半角英数</font>
            </div>

        </div>


        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px; padding-top:10px;">
                メールアドレス　（確認） <font style="color:#FF0000;">*必須</font><font class="ket-text1" style="display:none; margin-left:20%;">*確認のため再入力</font>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12" >
                &nbsp;&nbsp;&nbsp;<input name="emailkon" class="emailkon" type="email" placeholder="" style="width:52%;" /> <font class="ket-text2" style="color:#FF0000;">*確認のため再入力</font>

            </div>
        </div>


        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px; padding-top:10px;">
                日本出発日 <font style="color:#FF0000;">*必須</font>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-3" >
                &nbsp;&nbsp;&nbsp;<select name="year0" class="year0" style="width:70%;">
                    <option value="0"></option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option></select> 年
            </div>
            <div class="col-md-2 col-sm-3 col-xs-3" >
                &nbsp;&nbsp;&nbsp;<select name="month0" class="month0" style="width:70%;">
                    <option value="0"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option></select> 月
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6" >
                &nbsp;&nbsp;&nbsp;<select name="day0" class="day0" style="width:30%;">
                    <option value="0"></option>
                    {php} for ($i=1;$i<=31;$i++) { {/php}
                    <option value="{php} echo $i; {/php}">{php} echo $i; {/php}</option>

                    {php} } {/php}</select> 日  <!--<span onClick="getdate(this)" style="color:red; cursor:pointer;">カレンダーから選択</span>-->

                <input type="text" class="datepicker" data-date="0" style=" position:absolute; background:none; border:none; outline:none;box-shadow:none; color:#FFFFFF; margin-top:-60px;
				   margin-left:-170px;" disabled="disabled"></input>
            </div>
        </div>

        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px;padding-top:10px; ">
                宿泊先名（ホテル名）
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12" >
                &nbsp;&nbsp;&nbsp;<input name="hotel-name" class="hotel-name" type="text" placeholder="" style="width:95%;" />
            </div>

        </div>
        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px;padding-top:10px;">
                お客様の電話番号
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12" >
                &nbsp;&nbsp;&nbsp;<input name="telphone" class="telphone" type="number" placeholder="" style="width:50%;" />
            </div>

        </div>


        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px; padding-top:10px;">
                参加人数 <font style="color:#FF0000;">*必須</font>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 adult"  >
                大人 <select name="adult" id="adult" style="width:45%;">
                    <option value="0">0人</option>
                    <option value="1">1 人</option>
                    <option value="2">2 人</option>
                    <option value="3">3 人</option>
                    <option value="4">4 人</option>
                    <option value="5">5 人</option>
                    <option value="6">6 人</option>
                    <option value="7">7 人</option>
                    <option value="8">8 人</option>
                    <option value="9">9 人</option>
                    <option value="10">10 人</option></select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4" >
                子供 <select name="kid" id="kid" style="width:45%;">
                    <option value="0">0人</option>
                    <option value="1">1 人</option>
                    <option value="2">2 人</option>
                    <option value="3">3 人</option>
                    <option value="4">4 人</option>
                    <option value="5">5 人</option>
                    <option value="6">6 人</option>
                    <option value="7">7 人</option>
                    <option value="8">8 人</option>
                    <option value="9">9 人</option>
                    <option value="10">10 人</option></select>
            </div>

        </div>


        <div class="row plan-row">
            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:20px; padding-top:10px;">
                コースNo. <font style="color:#FF0000;">*必須</font> <font class="ket-text1" style="color:#FF0000; display:none;">コースプランの①～⑩からご選択下さい。</font>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12" >
                &nbsp;&nbsp;&nbsp;<select name="plantype" id="plantype" style="width:30%;">
                    <option value="0"></option>
                    <option value="packet1">1.  A（キナバル公園）+　B（マングローブクルーズ）</option>
                    <option value="packet2">2.  B（マングローブクルーズ）+　C（サピ島）</option>
                    <option value="packet3">3.  B（マングローブクルーズ）+　D（ロッカウィー）</option>
                    <option value="packet4">4.  A（キナバル公園）+　D（ロッカウィー）</option>
                    <option value="packet5">5.  C（サピ島）+　D（ロッカウィー）</option>
                    <option value="packet6">6.  A（キナバル公園）+　C（サピ島）</option>
                    <option value="packet7">7.  A（キナバル) + B（マングローブ）+ C（サピ）</option>
                    <option value="packet8">8.  A（キナバル）+ B（マングローブ）+ D（ロッカウィー）</option>
                    <option value="packet9">9.  B（マングローブ）+ C (サピ) + D（ロッカウィー）</option>
                    <option value="packet10">10. A（キナバル）+ C（サピ）+ D（ロッカウィー）</option></select> <font class="ket-text2" style="color:#FF0000;">コースプランの①～⑩からご選択下さい。</font>
            </div>

        </div>


        <div class="row plan-row" style="border:none;">
            <center><button type="button" onclick="planoffer()" data-toggle="modal" data-target="#myModal" class="btn btn-success" style="width:70%; height:40px; font-size:18px; border: 2px solid #ccc; border-radius:8px;" id="plan-offer">確認画面に進む</button></center>
            <br />
            <!-- Modal -->


            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity:1;"><span aria-hidden="true"><img src="{$config.documentroot}{$config.countryname}/images/special_content/close.png" width="30"  /></span></button>
                            <h4 class="modal-title" style="text-align:center;" id="myModalLabel">各コースの希望日をご入力ください。</h4>
                        </div>
                        <div class="modal-body">
                            <div id="theplan">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <center><button data-action="true" type="button" onclick="sendoffer(this)" class="btn btn-success sendoffer" style="width:70%; height:40px; font-size:18px; border: 2px solid #ccc; border-radius:8px;">確認画面に進む</button></center></form>
</div>
</div>
</div>

</div>
</div>





</div>

<br /><br />

<!-- Modal -->
<div class="modal fade" id="plandetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel" style="text-align:center;">確認画面</h4>
            </div>
            <div class="modal-body" style="padding-top:20px; padding-bottom:20px;">
                <div id="details">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <p><strong>氏名　（漢字） : <span class="username1"></span></strong></p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 approve-title" style="padding-left:20px;">
                            <p><strong>氏名　（フリガナ） : <span class="username2"></span></strong></p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <p><strong>メールアドレス : <span class="useremail"></span></strong></p>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 approve-title" style="padding-left:20px;">
                            <p><strong>日本出発日 : <span class="userdate"></span></strong></p>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <p><strong>宿泊先名（ホテル名） : <span class="hotelname"></span></strong></p>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 approve-title" style="padding-left:20px;">
                            <p><strong>お客様の電話番号 : <span class="usertlp"></span></strong></p>
                        </div>

                    </div>
                    <table class="table table-striped">
                        <tr><td>コース</td><td>:</td><td><p class="packetname"></p></td></tr>
                        <tr><td><p class="item1"></p></td><td>:</td><td><p class="date1"></p></td></tr>
                        <tr><td><p class="item2"></p></td><td>:</td><td><p class="date2"></p></td></tr>
                        <tr class="itemrow3"><td><p class="item3"></p></td><td>:</td><td><p class="date3"></p></td></tr>
                        <tr><td>価格</td><td>:</td><td><p class="itemcost"></p></td></tr>
                        <tr><td>大人</td><td>:</td><td><p class="adult-cost"></p></td></tr>
                        <tr><td>子供</td><td>:</td><td><p class="kid-cost"></p></td></tr>
                        <tr><td>総費用</td><td>:</td><td><p class="totalcost" style="font-weight:bold;"></p></td></tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer details-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                <button onclick="approveoffer()" type="button" class="btn btn-success" style="height:40px; font-size:18px; border: 2px solid #ccc; border-radius:8px;">送信する</button>
            </div>
        </div>
    </div>
</div>


</div> <!--end body content-->



<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

<script type="text/javascript" src="{$config.documentroot}common/js/jquery-1.9.0.min.js"></script>
<script src="{$config.documentroot}common/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="{$config.documentroot}common/js/plan-offer.js"></script>


<div class="gotop">
    <div style="height: 55px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png" alt="gotop">&nbsp;<a href="#top" style="text-decoration: underline; color: #847973;">ページTOP</a></p>
</div>

</div><!-- end of main content -->

{include file="country_footer.tpl"}
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>