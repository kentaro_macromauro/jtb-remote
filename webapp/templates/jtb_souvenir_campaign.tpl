{include file="jtb_header.tpl" }
<!--container-->
<div class="container">
  <!--navigation bar-->
   {include file="jtb_navigation_bar.tpl" }
  <!--navigation bar-->
  <div class="header-slider-odd">
  	<img src="public/images/souvenir_banner/{$campaign_id}-1.jpg" alt="" />
  </div>
  <!--container-left-->
  <div class="container-left">
    {$breadcamp}
    <!--content detail-->
     <div class="header-text-tittle">{$campaign_tittle}</div>
      <div class="text-content">
      	{$campaign_content}
      </div>
      <div  class="height10"></div>     
      {$campaign_list} 
    <!--content detail-->
  </div>
  <!--container-left-->
  <!--container-right-->
  {include file="jtb_souvenir_container_right.tpl"}
  <!--container-right-->
</div>
<!--container-->
<!--footer-->
{include file="jtb_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>