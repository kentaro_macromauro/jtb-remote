{include file="country_header.tpl" }

<body>
<!--body-->

{include file="country_header_menu.tpl" }

<!--container-->
<div class="container home-content pull-container">

<div style="padding: 0 5px 0 5px;">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="mybus-breadcrumb" style="padding: 0 20px 0 5px; font-family: 'Arial', sans-serif;">
                {$breadcamp}
                <div style="height: 0px;"></div>
            </div>
        </div>

        <!--
        <div class="col-md-3 col-sm-3">
            <div class="search-additional">
            <div class="inner-addon right-addon">
                <i class="fa-black fa-search" aria-hidden="true"></i>
                <input type="text" class="form-control" />
            </div>
            </div>
        </div>
        -->
    </div>
</div>
<div style="height: 10px;"></div>
<div class="row">
<div class="col-md-12 col-sm-12">
{include file="country_search_box.tpl" }
<div class="row">
    <div class="col-md-8 col-sm-7">
        <div id="wrapper">
            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                    {$top_banner_img}
                </div>
            </div>
        </div>

    </div>
</div>
<br><br>
<!-- collaps Selling Ranking -->
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    {if $config.country != "PHL" && $config.country != "MMR"}
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                    <div class="grey-tittle-before">
                        <h3 style="margin-bottom: 0; margin-top: 0;" class="grey-tittle-after">
                            売れ筋ランキング
                        </h3>
                    </div>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <!-- Selling Ranking -->
                <div style="height: 10px;"></div>
                <div class="row">
                    {section name=cnt start=0 loop=$sales_ranking}
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail1">
                            <div class="img-responsive img-box">
                                <a class="ga_click" id="{$config.countryname}_OPT_POPULAR_#{$smarty.section.cnt.iteration}"  href="{$sales_ranking[cnt].product_link}"><img src="{$sales_ranking[cnt].product_picture}" alt="{$sales_ranking[cnt].product_name}"></a>
                            </div>
                            <div class="selling-rangking">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img src="{$config.documentroot}images/sidebar/crawn.png" class="img-responsive" alt="{$sales_ranking[cnt].product_name}">
                                    <span>{$smarty.section.cnt.index+1}</span>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8"></div>
                            </div>
                            <div class="grey-thumb-ct-redlink"><a href="{$sales_ranking[cnt].product_link}"><b>{$sales_ranking[cnt].product_name}</b></a></div>
                        </div>
                    {/section}
                </div>
                <!-- End of Sell Ranking -->
            </div>
        </div>
    </div>
    {/if}
</div>
<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading4">
            <h4>
                <a data-toggle="collapse" data-parent="#accordion4" href="#collapse4" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                    <div class="grey-tittle-before">
                        <h3 style="margin-bottom: 0; margin-top: 0;" class="grey-tittle-after">
                            おすすめオプショナルツアー
                        </h3>
                    </div>
                </a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4">
            <div class="panel-body">
                <!-- Blue -->
                <div style="height: 10px;"></div>
                <div class="row">
                    {section name=cnt start=0 loop=$recommend_tour}
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail1">
                            <div class="img-responsive img-box">
                                <a class="ga_click" id="{$config.countryname}_OPT_RECOMMEND_#{$smarty.section.cnt.iteration}"  href="{$recommend_tour[cnt].product_link}"><img src="{$recommend_tour[cnt].product_picture}" alt="{$recommend_tour[cnt].product_name}"></a>
                            </div>
                            <div class="grey-thumb-ct-redlink"><a href="{$recommend_tour[cnt].product_link}"><b>{$recommend_tour[cnt].product_name}</b></a></div>
                        </div>
                    {/section}
                </div>
                <!-- End of Blue -->
            </div>
        </div>
    </div>
</div>

<!-- End of blue recomended -->

<!-- collaps red -->
<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4>
                <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                    <div class="grey-tittle-before">
                        <h3 style="margin-bottom: 0; margin-top: 0;" class="grey-tittle-after">
                            スポットから選ぶ
                        </h3>
                    </div>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <!-- Red -->
                <div style="height: 10px;"></div>
                <div class="row">
                    {section name=cnt start=0 loop=$spot_data}
                    <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail1">
                        <a href="{$spot_data[cnt].spot_link}"><img src="{$spot_data[cnt].spot_picture}" class="img-responsive" alt="{$spot_data[cnt].spot_name}"></a>
                        <div class="grey-thumb-ct-blueredlink"><a href="{$spot_data[cnt].spot_link}"><span>{$config.countryname_jp}</span><br><b>{$spot_data[cnt].spot_name}</b></a></div>
                    </div>
                    {/section}
                </div>
                <!-- End of Red -->
            </div>
        </div>
    </div>
</div>
<!-- end of collaps red -->

<!-- collaps blue -->
<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4>
                <a data-toggle="collapse" data-parent="#accordion3" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                    <div class="grey-tittle-before">
                        <h3 style="margin-bottom: 0; margin-top: 0;" class="grey-tittle-after">
                            シーン別で選ぶ
                        </h3>
                    </div>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <!-- Blue -->
                <div style="height: 10px;"></div>
                {if $config.country eq "NZL"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=002"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=002"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_option=4"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/sale.jpg" class="img-responsive" alt="SALE!"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_option=4"><b>SALE!</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=21"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/active.jpg" class="img-responsive" alt="海であそぶ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=21"><b>海であそぶ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=008"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/gourmet.jpg" class="img-responsive" alt="ぐるめツアー"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=008"><b>ぐるめツアー</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg"" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=014"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/car.jpg" class="img-responsive" alt="送迎・ホテル送迎"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=014"><b>送迎・ホテル送迎</b></a></div>
                        </div>
                    </div>
                {elseif  $config.country eq "VNM"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=002"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=002"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=007"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/spa.jpg" class="img-responsive" alt="エステ・スパ・マッサージ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=007"><b>エステ・スパ・マッサージ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=21"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/active.jpg" class="img-responsive" alt="海であそぶ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=21"><b>海であそぶ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=008"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/gourmet.jpg" class="img-responsive" alt="ぐるめツアー"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=008"><b>ぐるめツアー</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=014"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/car.jpg" class="img-responsive" alt="送迎・ホテル送迎"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=014"><b>送迎・ホテル送迎</b></a></div>
                        </div>
                    </div>
                {elseif  $config.country eq "KHM"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=002"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=002"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=008"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/gourmet.jpg" class="img-responsive" alt="ぐるめツアー"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=008"><b>ぐるめツアー</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=014"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/car.jpg" class="img-responsive" alt="送迎・ホテル送迎"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=014"><b>送迎・ホテル送迎</b></a></div>
                        </div>
                    </div>
                {elseif  $config.country eq "TWN"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=2"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=2"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_option=4"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/sale.jpg" class="img-responsive" alt="SALE!"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_option=4"><b>SALE!</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=7"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/spa.jpg" class="img-responsive" alt="エステ・スパ・マッサージ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=7"><b>エステ・スパ・マッサージ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=8"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/gourmet.jpg" class="img-responsive" alt="ぐるめツアー"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=8"><b>ぐるめツアー</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=014"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/car.jpg" class="img-responsive" alt="送迎・ホテル送迎"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=014"><b>送迎・ホテル送迎</b></a></div>
                        </div>
                    </div>
                {elseif  $config.country eq "PHL"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=2"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=2"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=7"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/spa.jpg" class="img-responsive" alt="エステ・スパ・マッサージ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=7"><b>エステ・スパ・マッサージ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=21"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/active.jpg" class="img-responsive" alt="海であそぶ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>海であそぶ</b></a></div>
                        </div>
                    </div>
                {elseif  $config.country eq "MMR"}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=2"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=2"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                    </div>
                {else}
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=2"><img src="{$config.documentroot}{$config.countryname}/images/banner_scene/tourism.jpg" class="img-responsive" alt="観光"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=2"><b>観光</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_option=4"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/sale.jpg" class="img-responsive" alt="SALE!"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_option=4"><b>SALE!</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=7"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/spa.jpg" class="img-responsive" alt="エステ・スパ・マッサージ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=7"><b>エステ・スパ・マッサージ</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=21"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/active.jpg" class="img-responsive" alt="海であそぶ"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=21"><b>海であそぶ</b></a></div>
                        </div>
                    </div>
                    <div style="height: 20px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=8"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/gourmet.jpg" class="img-responsive" alt="ぐるめツアー"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=8"><b>ぐるめツアー</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=012"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/history.jpg" class="img-responsive" alt="世界遺産"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=012"><b>世界遺産</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene1">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=013"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/culture.jpg" class="img-responsive" alt="文化芸術"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=013"><b>文化芸術</b></a></div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail-scene2">
                            <a href="search_opt.php?inp_country={$config.country}&inp_category=014"><img src="{$config.documentroot}/{$config.countryname}/images/banner_scene/car.jpg" class="img-responsive" alt="送迎・ホテル送迎"></a>
                            <div class="grey-thumb-ct-bluelink"><a href="search_opt.php?inp_country={$config.country}&inp_category=014"><b>送迎・ホテル送迎</b></a></div>
                        </div>
                    </div>
                {/if}
                <!-- End of Blue -->
            </div>
        </div>
    </div>
</div>
<!-- end of collaps blue -->

<!-- Banner -->
<div style="height: 30px"></div>
<div style="padding: 0 5px 0 5px;">
    {$footer_banner_img}
</div>
<!-- end Banner -->

</div>




</div>


<div class="gotop">
    <div style="height: 55px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png" alt="gotop">&nbsp;<a href="#top" style="text-decoration: underline; color: #847973;">ページTOP</a></p>
</div>

</div><!-- end of main content -->
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>