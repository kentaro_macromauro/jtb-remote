{include file="country_header.tpl" }

<body>
<!--body-->

{include file="country_header_menu.tpl" }

<!--container-->
<div class="container home-content pull-container">
<div class="mybus-breadcrumb" style="padding: 0 20px 0 10px; font-family: 'Arial', sans-serif;">
    {$breadcamp}
    <div style="height: 0px;"></div>
</div>
<div class="row"></div>
<div style="padding: 0 5px 0 5px;">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="mybus-search">
    <div class="row">
        <div class="col-md-12" style="">
            <div class="search-header-before">
                <p class="search-header" style="font-weight: 700;">オプショナルツアー検索</p>
            </div>
        </div>
    </div>
    <div class="mybus-search-inner">
        <form action="search_opt.php" method="get" name="frmSearch">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">国名</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_country" style="padding-left: 0px;">
                            {$inp_country}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">都市名</div>
                        </div>
                        <div class="select_city col-md-9 col-sm-8 col-xs-7 long-selects" style="padding-left: 0px;">
                            {$inp_city}
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">テーマ</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_category" style="padding-left: 0px;">
                            <select name="inp_category" class="form-control">
                                <option value="0"></option>
                                {$select_category}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">条件検索</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_option" style="padding-left: 0px;">
                            <select name="inp_option" class="form-control">
                                <option value="0"></option>
                                {$select_option}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">時間帯</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects select_time" style="padding-left: 0px;">
                            <select name="inp_time" class="form-control">
                                <option value="" selected></option>
                                {$select_time}
                            </select>
                        </div>
                    </div>
                    <div style="height: 7px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5 " style="padding: 0 5px 0 0px;">
                            <div class="caption-form2">フリーワード</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-xs-7 long-selects" style="padding-left: 0px;">
                            {$inp_keyword}
                        </div>
                    </div>
                    <div style="height: 15px;"></div>
                    <div class="row">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-9 col-sm-8">
                            <input type="submit" value="検索する" class="btn btn-blue-search" style="width: 100%;"/>
                        </div>
                    </div>
                    <div style="height: 0px;"></div>
                </div>
            </div>
    </div>
</div>
<div style="text-align: right;">
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-8"><div class="pagination-words"><p>{$max_record}件中{$page_start}〜{$page_end}件目を表示中</p></div></div>
        <div class="col-md-3 col-sm-3 col-xs-4" style="padding-top: 20px;">
            <div class="row">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    {$order_select}
                </div>
            </div>
        </div>
    </div>
</div>
<div style="text-align: right;">
    <div class="row">
        <div class="col-md-4 col-sm-1 col-xs-1"></div>

        <div class="col-md-8 col-sm-11 col-xs-11">
            {$pagination}
        </div>
</div>
</form>
</div>
</div>
{$search_content}

    <div style="text-align: right;">
        <div class="row">
            <div class="col-md-4 col-sm-1 col-xs-1"></div>

            <div class="col-md-8 col-sm-11 col-xs-11">
                {$pagination}
            </div>
        </div>
    </div>

<!-- Rangking Veltra -->
<div style="height: 30px"></div>
<div class="row">
    {if $config.country != "PHL" && $config.country != "MMR"}
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="ranking-vertical-left">
            <div class="ranking-vertical">
                <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>売れ筋ランキング</h5>
                <p><a href="{$sales_ranking[0].product_link}" {$sales_ranking[0].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-1.png" alt="mybus"/></span>{$sales_ranking[0].product_name}</a></p>
                <p><a href="{$sales_ranking[1].product_link}" {$sales_ranking[1].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-2.png" alt="mybus"/></span>{$sales_ranking[1].product_name}</a></p>
                <p><a href="{$sales_ranking[2].product_link}" {$sales_ranking[2].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-3.png" alt="mybus"/></span>{$sales_ranking[2].product_name}</a></p>
            </div>
        </div>
    </div>
    {/if}
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="ranking-vertical-right">
            <div class="ranking-vertical">
                <h5><span><img src="{$config.documentroot}images/sidebar/ver-ranking.png" alt="mybus"/></span>おすすめオプショナルツアー</h5>
                <p><a href="{$recommend_tour[0].product_link}" {$recommend_tour[0].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-1.png" alt="mybus"/></span>{$recommend_tour[0].product_name}</a></p>
                <p><a href="{$recommend_tour[1].product_link}" {$recommend_tour[1].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-2.png" alt="mybus"/></span>{$recommend_tour[1].product_name}</a></p>
                <p><a href="{$recommend_tour[2].product_link}" {$recommend_tour[2].link_type}><span><img src="{$config.documentroot}images/sidebar/ver-ranking-3.png" alt="mybus"/></span>{$recommend_tour[2].product_name}</a></p>
            </div>
        </div>
    </div>
</div>
<!-- Rangking Veltra -->

</div>






</div>


<div class="gotop">
    <div style="height: 55px;"></div>
    <p align="right" style="padding-right: 3px;"><img src="{$config.documentroot}images/top-btn-black.png" alt="gotop">&nbsp;<a href="#top" style="text-decoration: underline; color: #847973;">ページTOP</a></p>
</div>

</div><!-- end of main content -->
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--footer script-->
{include file="country_footer_script.tpl"}
<!--footer script-->

<!--body-->
</body></html>