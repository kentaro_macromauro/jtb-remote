﻿{include file="country_non-res-header.tpl" }
<!--container-->
<div class="container">
  <!--container-left-->
  <div class="container-left"> {$breadcamp}
    <div class="clear"></div>
    <!--reservation-->
    <div class="text-content">
      <div class="formbox">
      
      	{if ($cart_type == 0) || ($cart_type == 2)}
        <div class="souvenir_step3"></div>
        {else}
        <div class="souvenir_none_credit_step3"></div>
        {/if}
        
        
        
        <div class="clear"></div>
        <span class="texttittle" > 購入者情報入力 </span>
        <div class="text-policy">
          <p>個人情報保護方針</p>
          <p>入力されました個人情報は、ご本人の承諾なしに第三者（業務委託先を除く）に開示するようなことはいたしません。 JTBの商品・情報提供のため利用させていただきます。詳しくは個人情報保護方針をご覧下さい。</p>
          <div class="height10"></div>
          <p>
            <input type="checkbox" value="true" name="frm_step3_checkview" {if $customer.form_input }checked="checked"{/if}   />
            上記の内容に同意する</p>
        </div>
        <div class="form3-content">
          <!--<form action="booking_infoinput_confirm.php" name="gotonext" method="post">-->
          <form action="souvenir_cart_inp_address_action.php" name="gotonext" method="post">
            <!-- form Register-->
          
            <div class="height10"></div>
            <p>■購入者情報</p>
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>購入者名（漢字） <span class="text-small-red">※必須</span></th>
                <td>{$error.name} <span class="text-firstname" style="width:55px;">姓</span>
                  <input type="text" class="inp-name"  value="{$customer.inp_surname}" name="inp_surname" />
                  <span class="text-lastname" style="width:65px;">名</span>
                  <input type="text" class="inp-name"  value="{$customer.inp_firstname}" name="inp_firstname" />
                  <span class="text-small">例） 山田 太郎</span></td>
              </tr>
              <tr>
                <th>購入者名（ローマ字） <span class="text-small-red">※必須</span></th>
                <td> {$error.name_furi} <span class="text-firstname" style="width:55px;">Surname</span>
                  <input type="text" class="inp-name" value="{$customer.inp_surname_furi}" name="inp_surname_furi" />
                  <span class="text-lastname" style="width:65px;">Firstname</span>
                  <input type="text" class="inp-name" value="{$customer.inp_firstname_furi}" name="inp_firstname_furi" />
                  <span class="text-small">例） Yamada Taro</span></td>
              </tr>
              <tr>
                <th>生年月日 <span class="text-small-red">※必須</span></th>
                <td> {$error.birth_date}
                  <select name="birthday_year">
                    <option selected="selected" value="" label="----">----</option>
                    {foreach from=$form.birthyear item=year}
                    	{if $customer.birthday_year eq $year   }
                    		<option selected="selected" value="{$year}">{$year}</option>
                        {else}                    
                    		<option value="{$year}">{$year}</option>
                    	{/if}
                    {/foreach}
                  </select>
                  年
                  <select style="" name="birthday_month">
                    <option selected="selected" value="" >--</option>
                    <option value="1"  {if $customer.birthday_month eq "1"} selected="selected"{/if} >1</option>
                    <option value="2"  {if $customer.birthday_month eq "2"} selected="selected"{/if} >2</option>
                    <option value="3"  {if $customer.birthday_month eq "3"} selected="selected"{/if} >3</option>
                    <option value="4"  {if $customer.birthday_month eq "4"} selected="selected"{/if} >4</option>
                    <option value="5"  {if $customer.birthday_month eq "5"} selected="selected"{/if} >5</option>
                    <option value="6"  {if $customer.birthday_month eq "6"} selected="selected"{/if} >6</option>
                    <option value="7"  {if $customer.birthday_month eq "7"} selected="selected"{/if} >7</option>
                    <option value="8"  {if $customer.birthday_month eq "8"} selected="selected"{/if} >8</option>
                    <option value="9"  {if $customer.birthday_month eq "9"} selected="selected"{/if} >9</option>
                    <option value="10" {if $customer.birthday_month eq "10"}selected="selected"{/if} >10</option>
                    <option value="11" {if $customer.birthday_month eq "11"}selected="selected"{/if} >11</option>
                    <option value="12" {if $customer.birthday_month eq "12"}selected="selected"{/if} >12</option>
                  </select>
                  月
                  <select style="" name="birthday_day">
                    <option selected="selected" value="" >--</option>
                    <option value="1"  {if $customer.birthday_day eq "1"} selected="selected"{/if} >1</option>
                    <option value="2"  {if $customer.birthday_day eq "2"} selected="selected"{/if} >2</option>
                    <option value="3"  {if $customer.birthday_day eq "3"} selected="selected"{/if} >3</option>
                    <option value="4"  {if $customer.birthday_day eq "4"} selected="selected"{/if}>4</option>
                    <option value="5"  {if $customer.birthday_day eq "5"} selected="selected"{/if}>5</option>
                    <option value="6"  {if $customer.birthday_day eq "6"} selected="selected"{/if}>6</option>
                    <option value="7"  {if $customer.birthday_day eq "7"} selected="selected"{/if}>7</option>
                    <option value="8"  {if $customer.birthday_day eq "8"} selected="selected"{/if}>8</option>
                    <option value="9"  {if $customer.birthday_day eq "9"} selected="selected"{/if}>9</option>
                    <option value="10" {if $customer.birthday_day eq "10"} selected="selected"{/if}>10</option>
                    <option value="11" {if $customer.birthday_day eq "11"} selected="selected"{/if}>11</option>
                    <option value="12" {if $customer.birthday_day eq "12"} selected="selected"{/if}>12</option>
                    <option value="13" {if $customer.birthday_day eq "13"} selected="selected"{/if}>13</option>
                    <option value="14" {if $customer.birthday_day eq "14"} selected="selected"{/if}>14</option>
                    <option value="15" {if $customer.birthday_day eq "15"} selected="selected"{/if}>15</option>
                    <option value="16" {if $customer.birthday_day eq "16"} selected="selected"{/if}>16</option>
                    <option value="17" {if $customer.birthday_day eq "17"} selected="selected"{/if}>17</option>
                    <option value="18" {if $customer.birthday_day eq "18"} selected="selected"{/if}>18</option>
                    <option value="19" {if $customer.birthday_day eq "19"} selected="selected"{/if}>19</option>
                    <option value="20" {if $customer.birthday_day eq "20"} selected="selected"{/if}>20</option>
                    <option value="21" {if $customer.birthday_day eq "21"} selected="selected"{/if}>21</option>
                    <option value="22" {if $customer.birthday_day eq "22"} selected="selected"{/if}>22</option>
                    <option value="23" {if $customer.birthday_day eq "23"} selected="selected"{/if}>23</option>
                    <option value="24" {if $customer.birthday_day eq "24"} selected="selected"{/if}>24</option>
                    <option value="25" {if $customer.birthday_day eq "25"} selected="selected"{/if}>25</option>
                    <option value="26" {if $customer.birthday_day eq "26"} selected="selected"{/if}>26</option>
                    <option value="27" {if $customer.birthday_day eq "27"} selected="selected"{/if}>27</option>
                    <option value="28" {if $customer.birthday_day eq "28"} selected="selected"{/if}>28</option>
                    <option value="29" {if $customer.birthday_day eq "29"} selected="selected"{/if}>29</option>
                    <option value="30" {if $customer.birthday_day eq "30"} selected="selected"{/if}>30</option>
                    <option value="31" {if $customer.birthday_day eq "31"} selected="selected"{/if}>31</option>
                  </select>
                  日 </td>
              </tr>
              <tr>
                <th>性別 <span class="text-small-red">※必須</span></th>
                <td> {$error.sex}
                  <select name="inp_sex">
                    <option value="">---</option>
                    <option value="男性" {if $customer.inp_sex eq "男性"}selected="selected"{/if} >男性</option>
                    <option value="女性" {if $customer.inp_sex eq "女性"}selected="selected"{/if} >女性</option>
                  </select></td>
              </tr>
              <tr>
                <th>メールアドレス（PC） <span class="text-small-red">※必須</span></th>
                <td> {$error.mail}
                  <input type="text" class="width250"  name="inp_email" value="{$customer.inp_email}" />
                  <span class="text-small">例） yamada@●●●.co.jp</span></td>
              </tr>
              <tr>
                <th>確認用メールアドレス（PC） <br/>
                  <span class="text-small-red">※必須</span></th>
                <td><input type="text" class="width250" name="inp_email_conf" value="{if $error.mail == ""}{$customer.inp_email}{/if}" />
                  <span class="text-small">例） yamada@●●●.co.jp</span></td>
              </tr>
              <tr >
                <th>&nbsp;</th>
                <td> 
                  {if $customer.type_addr eq "true"}
                  <input type="checkbox" name="type_addr" value="true" checked="checked" />
                  {else}
                  <input type="checkbox" name="type_addr" value="true"  />
                  {/if}
                  
                  海外在住の方 </td>
              </tr>
              <tr>
                <th>住所</th>
                <td><!--Address JP-->
                  <table cellspacing="0" class="regis_address_local">
                    <tr >
                      <td>郵便番号 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_zip} 
                        〒
                        <input type="text" class="width40"  maxlength="3" value="{$customer.inp_addrjp_zip1}" name="inp_addrjp_zip1"/>
                        -
                        <input type="text"  class="width40"  maxlength="4" value="{$customer.inp_addrjp_zip2}" name="inp_addrjp_zip2"/>
                        {*<a target="_blank" href="http://search.post.japanpost.jp/zipcode/"><span class="mini">郵便番号検索</span></a></td>*}
                    </tr>
                    <tr>
                      <td>都道府県 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_pref}
                        <select name="inp_addrjp_pref" >
                          <option value="">都道府県を選択</option>   
                        {foreach from=$form.city item=arr_city}
                          <option value="{$arr_city.id}"  {if $customer.inp_addrjp_pref == $arr_city.id}selected="selected"{/if}  >{$arr_city.name}</option>
                    	{/foreach}
                        </select></td>
                    </tr>
                    <tr>
                      <td>市区町村 </td>
                      <td><input type="text" name="inp_addrjp_city" class="width350" value="{$customer.inp_addrjp_city}"  />
                        <span class="text-small">例） 品川区</span></td>
                    </tr>
                    <tr>
                      <td> 町域 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_area}
                        <input type="text" name="inp_addrjp_area" class="width350" value="{$customer.inp_addrjp_area}" />
                        <span class="text-small">例） 東品川2-3-11</span></td>
                    </tr>
                    <tr>
                      <td>建物名<br/>
                        部屋番号 </td>
                      <td><input type="text" name="inp_addrjp_building" class="width350" value="{$customer.inp_addrjp_building}" />
                        <span class="text-small">例） ＪＴＢビル</span></td>
                    </tr>
                  </table>
                  <!--Address JP-->
                  <!--Address EN-->
                  <table  class="regis_address_oversea">
                    <tr>
                      <td>住所 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type2}
                        <input type="text" name="inp_addrovs1" class="width300"  style="width:300px;"  value="{$customer.inp_addrovs1}" />
                        <span class="text-small">例)　79 Anson Road #12-01 Singapore079906</span></td>
                    </tr>
                    <tr>
                      <td>国 <span class="text-small-red">※必須</span></td>
                      <td><input type="text" name="inp_addrovs2" class="width300" style="width:300px;" value="{$customer.inp_addrovs2}" />
                        <span class="text-small">例)　Singapore</span></td>
                    </tr>
                  </table>
                  <!--Address EN--></td>
              </tr>
              <tr>
                <th>連絡先（電話）</th>
                <td><input type="text" name="inp_tel1_1" class="width40" value="{$customer.inp_tel1_1}" />
                  -
                  <input type="text" name="inp_tel1_2"  class="width40" value="{$customer.inp_tel1_2}" />
                  -
                  <input type="text" name="inp_tel1_3"  class="width40" value="{$customer.inp_tel1_3}" />
                  <span class="text-small">例） 03-1111-1111</span></td>
              </tr>
              <tr>
                <th>連絡先（携帯）<span class="text-small-red">※必須</span></th>
                <td> {$error.mobile}
                  <input type="text" name="inp_mobile1_1" class="width40" value="{$customer.inp_mobile1_1}" />
                  -
                  <input type="text" name="inp_mobile1_2" class="width40" value="{$customer.inp_mobile1_2}" />
                  -
                  <input type="text" name="inp_mobile1_3" class="width40" value="{$customer.inp_mobile1_3}"  />
                  <span class="text-small">例） 090-1111-1111</span></td>
              </tr>
              
            </table>
            <div class="height10"></div>
            
            
            {if $location_rec eq "2"}
            
            <p>■お届け先情報</p>
            
            <p><input type="checkbox" value="true" name="recive_address" /> 別のお届け先を指定　※上記に入力されたご住所へお届けの場合はそのまま次へお進みください</p>
            <!--address send product-->
            
            <div class="addreceive_production">
            <table cellspacing="1" class="tbform" >
              <tr>
                <th>お届け先名（漢字） <span class="text-small-red">※必須</span></th>
                <td>{$error.rec_name} <span class="text-firstname" style="width:55px;">姓</span>
                  <input type="text" class="inp-name"  value="" name="inprec_surname" />
                  <span class="text-lastname" style="width:65px;">名</span>
                  <input type="text" class="inp-name"  value="" name="inprec_firstname" />
                  <span class="text-small">例） 山田 太郎</span></td>
              </tr>
              <tr>
                <th>お届け先名（ローマ字） <span class="text-small-red">※必須</span></th>
                <td> {$error.rec_name_furi} <span class="text-firstname" style="width:55px;">Surname</span>
                  <input type="text" class="inp-name" value="" name="inprec_surname_furi" />
                  <span class="text-lastname" style="width:65px;">Firstname</span>
                  <input type="text" class="inp-name" value="" name="inprec_firstname_furi" />
                  <span class="text-small">例） Yamada Taro</span></td>
              </tr>
              
              <tr>
                <th>お届け先住所</th>
                <td><!--Address JP-->
                  
                  <table cellspacing="0" >
                    <tr >
                      <td>郵便番号 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_zip_rec} 
                        〒
                        <input type="text" class="width40"  maxlength="3" value="" name="inprec_addrjp_zip1"/>
                        -
                        <input type="text"  class="width40"  maxlength="4" value="" name="inprec_addrjp_zip2"/>
                        {*<a target="_blank" href="http://search.post.japanpost.jp/zipcode/"><span class="mini">郵便番号検索</span></a></td>*}
                    </tr>
                    <tr>
                      <td>都道府県 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_pref_rec}
                        <select name="inprec_addrjp_pref" >
                          <option value="">都道府県を選択</option>
                        	{foreach from=$form.city item=arr_city}
                         		<option value="{$arr_city.id}" >{$arr_city.name}</option>
                    		{/foreach} 
                        </select></td>
                    </tr>
                    <tr>
                      <td>市区町村 </td>
                      <td><input type="text" name="inprec_addrjp_city" class="width350" value=""  />
                        <span class="text-small">例） 品川区</span></td>
                    </tr>
                    <tr>
                      <td> 町域 <span class="text-small-red">※必須</span></td>
                      <td> {$error.addr_type1_area_rec}
                        <input type="text" name="inprec_addrjp_area" class="width350" value="" />
                        <span class="text-small">例） 東品川2-3-11</span></td>
                    </tr>
                    <tr>
                      <td>建物名<br/>
                        部屋番号 </td>
                      <td><input type="text" name="inprec_addrjp_building" class="width350" value="" />
                        <span class="text-small">例） ＪＴＢビル</span></td>
                    </tr>
                  </table>
                  
                  <!--Address JP-->
                  </td>
              </tr>
               <tr>
                <th>お届け先連絡先（電話）</th>
                <td><input type="text" name="inprec_tel1_1" class="width40" value="" />
                  -
                  <input type="text" name="inprec_tel1_2"  class="width40" value="" />
                  -
                  <input type="text" name="inprec_tel1_3"  class="width40" value="" />
                  <span class="text-small">例） 03-1111-1111</span></td>
              </tr>
            </table>
              {else if  $location_rec eq "1"}
              
              <p>■滞在先情報</p>
            <!--address send product-->
            
            <div class="addreceive_production">
            <table cellspacing="1" class="tbform" >
              	<table cellspacing="1" class="tbform" >
              <tr>
                <th>滞在先</th>
                <td>
                  <div class="hotel_manual">
                    <table cellspacing="0">
                      <tr>
                        <td>滞在先名 </td>
                        <td>{$error.hotel_type2_name}<input type="text" class="width350"  name="reg_hotel_name" value="{$booking.his.reg.hotel_name}" /></td>
                      </tr>
                      <tr>
                        <td>滞在先TEL</td>
                        <td><input type="text" name="reg_hotel_tel" class="width120" value="{$booking.his.reg.hotel_tel}" />
                          <span class="text-small">例） 090-1111-1111</span></td>
                      </tr>
                      <tr>
                        <td>滞在先住所</td>
                        <td> <input type="text"class="width350"  name="reg_hotel_addr" value="{$booking.his.reg.hotel_addr}"  /></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              <tr style="display:none;">
                <th>&nbsp;</th>
                <td><input type="checkbox" name="reg_hotel_select" value="1" {if $booking.his.reg.hotel_select == "1" }checked="checked"{/if}  />
                  リストにない場合 
              </tr>
              <tr>
                <th>滞在期間 </th>
                <td><select name="reg_arrfrm_year">
                    <option selected="selected" value="" label="----">----</option>
                    
                    
                    {foreach from=$book_year item=year}
                    	
                    
                    <option value="{$year}"  >{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select name="reg_arrfrm_month">
                    <option selected="selected" value=""  label="--">--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                  </select>
                  月
                  <select style="" name="reg_arrfrm_day">
                    <option selected="selected" value="" >--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                    <option value="13" >13</option>
                    <option value="14" >14</option>
                    <option value="15" >15</option>
                    <option value="16" >16</option>
                    <option value="17" >17</option>
                    <option value="18" >18</option>
                    <option value="19" >19</option>
                    <option value="20" >20</option>
                    <option value="21" >21</option>
                    <option value="22" >22</option>
                    <option value="23" >23</option>
                    <option value="24" >24</option>
                    <option value="25" >25</option>
                    <option value="26" >26</option>
                    <option value="27" >27</option>
                    <option value="28" >28</option>
                    <option value="29" >29</option>
                    <option value="30" >30</option>
                    <option value="31" >31</option>
                  </select>
                  日  
                  ～
                  <select name="reg_arrto_year">
                    <option value="" selected="selected" label="----">----</option>
                    
                    
                    {foreach from=$book_year item=year}
                    	
                    
                    <option value="{$year}" >{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="reg_arrto_month">
                    <option value=""  >--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                  </select>
                  月
                  <select style="" name="reg_arrto_day">
                    <option value=""  >--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                    <option value="13" >13</option>
                    <option value="14" >14</option>
                    <option value="15" >15</option>
                    <option value="16" >16</option>
                    <option value="17" >17</option>
                    <option value="18" >18</option>
                    <option value="19" >19</option>
                    <option value="20" >20</option>
                    <option value="21" >21</option>
                    <option value="22" >22</option>
                    <option value="23" >23</option>
                    <option value="24" >24</option>
                    <option value="25" >25</option>
                    <option value="26" >26</option>
                    <option value="27" >27</option>
                    <option value="28" >28</option>
                    <option value="29" >29</option>
                    <option value="30" >30</option>
                    <option value="31" >31</option>
                  </select>
                  日 
                  
                  {if $error.hotel_stay1 != ""}
                  	<br/>{$error.hotel_stay1}
                  {/if}
              </tr>
              <tr>
                <th>到着日フライト </th>
                <td><select name="reg_arrgo_year">
                    <option value="" selected="selected">----</option>
                    
                    
                    {foreach from=$book_year item=year}
                                        
                    	
                    
                    <option value="{$year}" {if $booking.his.reg.arrgo_year == $year}selected="selected"{/if} >{$year}</option>
                    
                    
                    {/foreach}
                
                  
                  </select>
                  年
                  <select style="" name="reg_arrgo_month">
                    <option selected="selected" value=""  label="--">--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                  </select>
                  月
                  <select style="" name="reg_arrgo_day">
                    <option selected="selected" value="" >--</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10" >10</option>
                    <option value="11" >11</option>
                    <option value="12" >12</option>
                    <option value="13" >13</option>
                    <option value="14" >14</option>
                    <option value="15" >15</option>
                    <option value="16" >16</option>
                    <option value="17" >17</option>
                    <option value="18" >18</option>
                    <option value="19" >19</option>
                    <option value="20" >20</option>
                    <option value="21" >21</option>
                    <option value="22" >22</option>
                    <option value="23" >23</option>
                    <option value="24" >24</option>
                    <option value="25" >25</option>
                    <option value="26" >26</option>
                    <option value="27" >27</option>
                    <option value="28" >28</option>
                    <option value="29" >29</option>
                    <option value="30" >30</option>
                    <option value="31" >31</option>
                  </select>
                  日
                  <input name="reg_arrgo_hh" type="text" class="width40" style="margin-left:15px;" value="" maxlength="2"  />
                  :
                  <input name="reg_arrgo_mm" type="text" class="width40" value="" maxlength="2"  />

                  <br/>
                  <span class="text-small-red">※日をまたぐフライトの場合、出発日と到着日の日付は異なりますのでご注意ください。</span></td>
              </tr>
              <tr>
                <th>&nbsp;</th>
                <td>フライトナンバー
                  <input type="text" name="air_no" class="width80" value="{$booking.his.reg.air_no}" />
                  <span class="text-small">例) SQ20、NA774等</span>
              </tr>
              <tr>
                <th>ご旅行形態：</th>
                <td><select name="sel_info" >
                	<option value="" selected="" >---</option>
                    <option value="ルックJTB" >ルックJTB</option>
                    <option value="その他JTB" >その他JTB</option>
                    <option value="JTB以外"   >JTB以外</option>
                  </select></td>
              </tr>
            </table>
      
              
              {/if}
              
              
              
              
             
            
            </div>
            <!--address send product-->
            <div class="height10"></div>
            <p>■その他の情報</p>
            <table cellspacing="1" class="tbform">
              <tr>
                <th >&nbsp;</th>
                <td><textarea name="inp_remark" rows="5" class="width350">{$customer.inp_remark}</textarea></td>
              </tr>
            </table>
           
            <!-- form Register-->
            <span style="padding:5px 150px 5px 165px; display:block;">
            <a href="souvenir_cart.php" name="btn_goback"  class="btn_submit gotoback"   style="width:60px; text-align:center; line-height:17px; height:17px; color:#333; display:block; float:left; margin-right:5px; text-decoration:none;" >戻る</a>
            
          	
          	<input type="button" class="btn_submit gotonext" value="次へ" style="width:70px;" name="btn_gotonext" />
          	</span> 
          </form>
          </div>
          
          {literal}
          <div style="height:100px;"></div>
            <div style="float:right; margin-right:5px;">
            
            <span id="ss_gmo_img_wrapper_100-50_image_ja">
            <a href="https://jp.globalsign.com/" target="_blank">
            <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
            </a>
            </span>
            <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
          </div>
      	  {/literal}
      </div>
    </div>
    {if $config.allot != 'false'}
    <form action="souvenir_cart.php" name="gotoback" method="post">
    </form>
    {else}
    <form action="booking.php?product_id={$booking.product.id}" name="gotoback" method="post">
    </form>
    {/if}
    
    {literal}

    {/literal}
    
    <!--reservation-->
    <div class="clear"></div>
    <!--banner footer-->
    <!--banner footer-->
  </div>
  <!--container-left-->
  <div class="clear"></div>
</div>
<!--container-->
<!--footer-->
{include file="country_footer.tpl"}
<!--footer-->
<!--body-->
</body>
</html>