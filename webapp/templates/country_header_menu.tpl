<!--header-->
<header  class="ha-header">
<div class="container head-desktop">

    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-2" >
            {if $basket_count}
                <div class="red-text-top" align="right"><p><a href="http://{$smarty.server.HTTP_HOST}/{$config.environment}{$config.countryname}/souvenir_cart.php"><img src="/images/souvenir/shopping-cart.png" alt="my bus" style="margin-top: -7px;" />{$basket_count}個の商品が入っています。</a></p></div>
            {/if}
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="red-text-top" align="right">
                <a href="page.php?id=tourist_desk.html" class="btn btn-grey-new" style="">現地デスクはこちら</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-2 logo-top">
            <div class="navbar-brand">
                <a  href="{$config.documentroot}{$config.environment}" class="desktops">
                    <img src="{$config.documentroot}images/logo/mybuslogo.jpg" class="img-responsive" alt="Responsive image"/>
                </a>
            </div>
        </div>

        <div class="col-md-5 col-sm-2 col-xs-2 padding-cn">
            <p><b>
                    {if $config.country eq "TWN"}
                        台湾
                    {elseif $config.country eq "IDN"}
                        バリ島/インドネシア
                    {elseif $config.country eq "THA"}
                        タイ
                    {elseif $config.country eq "VNM"}
                        ベトナム
                    {elseif $config.country eq "KHM"}
                        カンボジア
                    {elseif $config.country eq "MYS"}
                        マレーシア
                    {elseif $config.country eq "SGP"}
                        シンガポール
                    {elseif $config.country eq "HKG"}
                        香港
                    {elseif $config.country eq "AUS"}
                        オーストラリア
                    {elseif $config.country eq "NZL"}
                        ニュージーランド
                    {elseif $config.country eq "PHL"}
                        フィリピン
                    {/if}
            </b></p>
        </div>

        <div class="col-md-5 col-sm-7 col-xs-12">
            <div class="row social-top">
                <div class="col-md-9 col-sm-9 col-xs-12 button-sosmed">
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <div align="right" style="margin-right: -15px;"><img src="{$config.documentroot}images/logo/jtb-perfect-moment.jpg" class="img-responsive" alt="Responsive image" style="margin-top: 2px;"/></div>
                        </div>

                        <div class="col-md-3 col-sm-3">
                            <p style="padding-top: 23px; padding-right: 10px;"><a href="page.php?id=qa.html" style="font-size: 10px; text-align: right; color: #3212f3;">FAQ</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 button-sosmed">
                    <div class="round-social-color">
                        <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                        <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="header-second">
<div class="container grey-bg">
<div class="row">
<div id="ribon-red-desktop-sps2"></div>

<div class="row">
<div class="col-xs-12 menus">
<div class="row head-mobile">
<div id="ribon-red-desktop-sps"></div>

<div class="head-mobile-inner">
    <div class="col-xs-12">
        <div class="red-text-top"><p>海外観光・オプショナルツアーならJTB</p></div>
    </div>

    <!--
    <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-xs-5" style="padding-left: 5px; padding-right: 2px;">
            <div class="red-text-top-other"><p>海外観光・オプショナルツアーならJTB</p></div>
        </div>
        <div class="col-xs-4"></div>
    </div>
    -->

    <div class="row">
        <div class="col-xs-3" style="padding: 0px; margin-left: 0px;">
            <a  href="{$config.documentroot}{$config.environment}"><img src="{$config.documentroot}images/logo/mybuslogo.jpg" class="img-responsive" alt="Responsive image"/></a>
        </div>
        <div class="col-xs-5 nama-negara-mobile">
            <div class="row">
                <div class="col-xs-12">
                    <p>
                        {if $config.country eq "TWN"}
                            台湾
                        {elseif $config.country eq "IDN"}
                            バリ島/インドネシア
                        {elseif $config.country eq "THA"}
                            タイ
                        {elseif $config.country eq "VNM"}
                            ベトナム
                        {elseif $config.country eq "KHM"}
                            カンボジア
                        {elseif $config.country eq "MYS"}
                            マレーシア
                        {elseif $config.country eq "SGP"}
                            シンガポール
                        {elseif $config.country eq "HKG"}
                            香港
                        {elseif $config.country eq "AUS"}
                            オーストラリア
                        {elseif $config.country eq "NZL"}
                            ニュージーランド
                        {elseif $config.country eq "PHL"}
                            フィリピン
                        {/if}
                    </p>
                </div>

            </div>
        </div>
        <div class="col-xs-2" style="padding-top: 0px; padding-right: 0px;">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                <div class="menu-toggle-mobile"><span class="fa-grey fa-list-ul"></span></div>
            </button>
        </div>
        <div class="col-xs-2" style="padding-top: 0px; padding-left: 5px;">
            <button class="btn search-mobile collapsed" data-toggle="collapse" aria-expanded="true" data-target="#mobilesearch" aria-expanded="false" aria-controls="navbar">
                <div><span class="fa-white fa-search"></span></div>
            </button>
        </div>
    </div>
</div>




<div id="ribon-red-desktop-sps"></div>



<!-- Mobile Search -->


<div class="row mobilesearch" style="background: #fff;">

    <div id="mobilesearch" class="mybus-search-mobile navbar-collapse collapse" style="margin-top: 0px; text-align: left; z-index: 99999;">
        <div class="row">
            <div class="col-md-12" style="">
                <div class="search-header-before">
                    <p class="search-header" style="font-weight: 700;">オプショナルツアー検索</p>
                </div>
            </div>
        </div>

        <div class="mybus-search-inner">
            <form action="search_opt.php" name="frmSearch2" onSubmit="return search_check2()">
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">国名</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_country" style="padding-left: 0px;">
                    {$inp_country}
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">都市名</div>
                </div>
                <div class="select_city col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">
                    {$inp_city}
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">テーマ</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_category" style="padding-left: 0px;">
                    <select class="form-control" name="inp_category">
                        <option value="0">-Please Select-</option>
                        {$select_category}
                    </select>
                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">条件検索</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_option" style="padding-left: 0px;">
                    <select name="inp_option" class="form-control">
                        <option value="0">-Please Select-</option>
                        {$select_option}
                    </select>

                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">時間帯</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects select_time" style="padding-left: 0px;">
                    <select name="inp_time" class="form-control">
                        <option value="" selected>-Please Select-</option>
                        {$select_time}
                    </select>

                </div>
            </div>
            <div style="height: 7px;"></div>

            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-5 " style="padding: 0 10px 0 0px;">
                    <div class="caption-form">フリーワード</div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-7 long-selects" style="padding-left: 0px;">

                    <input class="form-control" type="text" name="inp_keyword" />
                </div>
            </div>
            <div style="height: 15px;"></div>
                <input type="submit" value="検索する" class="btn btn-blue-search" style="width: 100%;"/>
            </form>
        </div>
    </div>
</div>
<!-- End of Mobile Search -->






<div class="row" style="padding-top: 5px; border-bottom: 1px solid #000; padding-bottom: 5px; z-index: 0;">
    <div class="col-xs-4">
        <div style="padding-left: 20px; padding-top: 10px; font-size: 16px;"><b></b></div>
    </div>
    <div class="col-xs-8" style="text-align: right; padding-right: 10px;">
        {if $basket_count}
            <span>
                <a href="http://{$smarty.server.HTTP_HOST}/{$config.environment}{$config.countryname}/souvenir_cart.php"><img src="/images/souvenir/shopping-cart.png" alt="my bus" width="28px" style="margin-top: -7px;" /></a>
            </span>
        {/if}
            <span class="round-social-grey2" style="text-align: right;">
                <span><a class="fa fa-facebook solo" href="http://www.facebook.com/sharer.php?u=http://www.mybus-asia.com" target="_blank"><span>Facebook</span></a></span>
                <span><a class="fa fa-twitter solo" href="https://twitter.com/share?url=http://www.mybus-asia.com&amp;text=My%20Bus%20Asia&amp;hashtags=mybusasia" target="_blank"><span>Facebook</span></a></span>
            </span>
        <span><a href="page.php?id=qa.html" class="btn btn-grey" style="background: transparent; border: 1px solid #ddd; border-radius: 0; margin-top: -5px">FAQ</a></span>
    </div>
</div>




</div>


</div>
</div>
<div class="col-sm-12 col-xs-12 menu" style="margin-top: -1px;">
    <div class="navbar-header">
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right" style="font-size:12px">
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/" {$menu_selected_style.top}"><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;TOP</a></li>
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/opt.php" {$menu_selected_style.opt}><img src="{$config.documentroot}images/menu/red-btn3-white.png"> &nbsp;オプショナルツアー</a></li>

                {if $config.country eq "TWN"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/TWN/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "IDN"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/INA/index.html" {$menu_selected_style.hotel} class="idn_menu"><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "THA"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/THA/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "VNM"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/VIE/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "MYS"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/MAS/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "AUS"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/AUS/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "SGP"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_fit/fr/v2/hotel/city/SIN/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
                {if $config.country eq "NZL"}
                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/hotel.php?id=http://www.jtb.co.jp/kaigai_htl/country/NZL/index.html" {$menu_selected_style.hotel}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;ホテル</a></li>
                {/if}
<!--
                <li><a href="#"><img src="{$config.documentroot}images/menu/red-btn3-noround.png" {$menu_selected_style.resutaurant}> &nbsp;レストラン</a></li>
-->
                {if $config.country !== "KHM" && $config.country !== "PHL" && $config.country !== "MMR"}
                    <li class="dropdown"><a class="{if $config.country == "IDN"}idn_menu{/if}" href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir.php" {$menu_selected_style.souvenir}><img src="{$config.documentroot}images/menu/red-btn3-noround.png"> &nbsp;おみやげ{if $config.country == "IDN"}<span style=font-size:10px>(現地受取)</span>{/if}</a>
                        <ul class="dropdown-menu">
                            {if $config.country == "THA" ||
                            $config.country == "VNM" ||
                            $config.country == "MYS" ||
                            $config.country == "IDN" ||
                            $config.country == "NZL"}
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=1"><img src="/images/menu/red-btn3-noround.png"> &nbsp;現地受取</a></li>
                            {else}
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=1"><img src="/images/menu/red-btn3-noround.png"> &nbsp;現地受取</a></li>
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/souvenir_search.php?inp_country={$config.country}&inp_location=2"><img src="/images/menu/red-btn3-noround.png"> &nbsp;日本受取</a></li>
                            {/if}
                            {if $config.country eq 'TWN' ||
                            $config.country eq 'SGP' ||
                            $config.country eq 'NZL' ||
                            $config.country eq 'MYS' ||
                            $config.country eq 'AUS' ||
                            $config.country == "IDN" ||
                            $config.country eq 'VNM' ||
                            $config.country eq 'THA'}
                                <li><a href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=user_guide.html"><img src="/images/menu/red-btn3-noround.png"> &nbsp;利用ガイド</a></li>
                            {/if}
                        </ul>
                    </li>
                {/if}
                <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/info_top.php" {$menu_selected_style.local}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;現地情報</a></li>
                {if $config.country eq "AUS" || $config.country eq "NZL" || $config.country eq "MYS" || $config.country eq "KHM" }
                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                {elseif $config.country eq "SGP" || $config.country eq "TWN" || $config.country eq "THA" || $config.country eq "VNM" }
                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog2.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                {elseif $config.country eq "IDN"}
                    <li><a class="menu_items" href="{$config.documentroot}{$config.environment}{$config.countryname}/page.php?id=online_catalog3.html" {$menu_selected_style.online_catalog}><img src="{$config.documentroot}images/menu/red-btn3-noround.png" > &nbsp;オンラインカタログ</a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>
</div><!-- rownya -->

</div>
</div>
</header>