<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH;
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'include/webservice.php');

/* setting information */

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$site_country = $_GET[inp_country];
$page_type    = $_GET[inp_type];
$breadcamp = output_breadcamp(array('TOP','オプショナルツアー検索'),array('./',''));

/* AUS Search */
if($site_country == 'AUS'){
    $aus_base_url = 'http://www.jtb.com.au/tour/search.php?';

    $city_query = '';
    if($_GET['inp_city'] != '0'){
        switch($_GET['inp_city']){
            case 'SYN':
                $city_query = '&city=3';
                break;
            case 'GOC':
                $city_query = '&city=2';
                break;
            case 'CAN':
                $city_query = '&city=1';
                break;
            case 'MEL':
                $city_query = '&city=4';
                break;
            case 'PER':
                $city_query = '&city=5';
                break;
            case 'AYR':
                $city_query = '&city=6';
                break;
            case 'AYR':
                $city_query = '&city=7&city=8';
            default:
                $city_query = '&city=3';
                break;
        }
    }
    $keyword_query = '';
    if($_GET['inp_keyword'] != '0'){
        $keyword_query = '&kw='.$_GET['inp_keyword'];
    }
    $time_query = '';
    $category_query = '';
    $option_query = '';
    if($_GET['inp_time'] != 0){
        $time_query = '&c_'.$_GET['inp_time'].'=1';
    }
    if($_GET['inp_category'] != 0){
        $category_query = '&c_'.$_GET['inp_category'].'=1';
    }
    if($_GET['inp_option'] != 0){
        $option_query = '&c_'.$_GET['inp_option'].'=1';
    }
    $auth_search_url = $aus_base_url.$city_query.$time_query.$category_query.$option_query.$keyword_query;

    header('Location: '.$auth_search_url);
}
$country_path = $db->showpath_bycountry($site_country);
$country_redirect_url = $country_path.'/search_opt.php?'.$_SERVER['QUERY_STRING'];
header('Location: '.$country_redirect_url);


















// For select part on header
$record = $db->get_country();
$sql = 'SELECT city_id,city_name_jp,city_iso3 FROM '._DB_PREFIX_TABLE.'city WHERE country_iso3 = UPPER("'.$_GET[inp_country].'") ORDER BY city_idx,city_id ';

$result = $db->db_query($sql); $i =0 ;
while ( $record1 = mysql_fetch_array($result))
{
    $arr_city[0][$i] = $record1[city_iso3];
    $arr_city[1][$i] = $record1[city_name_jp];
    $i++;
}

$inp_country = input_selectbox('inp_country',$record[1],$record[0],$_GET[inp_country],'-Please Select-','form-control') ;
$select_city = input_selectbox('inp_city',$arr_city[1],$arr_city[0],$_GET[inp_city],'-Please Select-','form-control') ;

$inp_search_keyword = input_textbox('inp_keyword',$_GET[inp_keyword],'form-control');

$record = $db->view_theme_all();
$select_category = input_option($record['data'], $record['value'], $_GET[inp_category]);

if (is_array($_GET[inp_category]))
{
    for ($i = 0; $i < count($_GET[inp_category]) ; $i++)
    {
        $select_category .= input_hiddenbox('inp_category[]',$_GET[inp_category][$i]) ;
    }
}

$arr_option = $db->view_option_all();
$select_option = input_option($arr_option['data'], $arr_option['value'], $_GET[inp_option]);


$record = $db->viw_time_all();
$select_time = input_option($record['data'], $record['value'], $_GET[inp_time]);
/*search box*/

/* pagination */

$inp_select_country  = $_GET[inp_country];
$inp_select_city	 = $_GET[inp_city];
$inp_select_category = $_GET[inp_category];
$inp_keyword		 = $_GET[inp_keyword];

if ( empty($inp_select_category) || (trim($inp_select_category) == "0") )
{
    $inp_select_category = "";
}

if ( empty($inp_select_city) || (trim($inp_select_city) == "0"))
{
    $inp_select_city = "";
}

if ( empty($inp_select_country ) || (trim($inp_select_country) == "0"))
{
    $inp_select_country = "" ;
}
/*
$sql = 'SELECT country_iso3,country_name_en,path FROM '._DB_PREFIX_TABLE.'country WHERE country_iso3 = "'.$_GET[inp_country].'" ';

$result = $db->db_query($sql);

while ($record = mysql_fetch_array($result))
{
	$country_name_en = $record[$country_name];
	$location        = $record[path];
}

*/

$max_record = $db->page_search_opt_count($inp_select_country,$inp_select_city,$inp_keyword, number_format_zero2($inp_select_category),$_GET['inp_option'], $_GET['inp_review'], $_GET['inp_time']);

$page_now = 0;
$page_num = 15;

$link1 = 'search_opt.php?inp_country='.$_GET[inp_country].'&inp_city='.$_GET[inp_city].'&inp_keyword='.$_GET[inp_keyword].'&inp_category='.$_GET[inp_category].'&inp_option='.$_GET[inp_option].'&inp_time='.$_GET[inp_time].'&inp_yearmonth='.$_GET[inp_yearmonth].'&inp_day='.$_GET[inp_day].'&review='.$_GET[inp_review].'&order='.$_GET[order];


if (is_array($_GET[inp_category]) )
{

    $link1 = 'search_opt.php?inp_country='.$_GET[inp_country].'&inp_city='.$_GET[inp_city].'&inp_keyword='.$_GET[inp_keyword].'&inp_option='.$_GET[inp_option].'&inp_time='.$_GET[inp_time].'&inp_yearmonth='.$_GET[inp_yearmonth].'&inp_day='.$_GET[inp_day].'&review='.$_GET[inp_review].'&order='.$_GET[order];

    for ($i = 0; $i < count( $_GET[inp_category] ) ; $i++)
    {
        $link1 .= '&inp_category[]='.$_GET[inp_category][$i].'&';
    }

}



$page_now   = pagenavi_start($max_record,$page_num,$_GET[page]);
$pagination = res_pagenavi( $page_now, $max_record ,$link1.'&page=',$page_num);

$page_first = pagenavi_first($page_now,$page_num);


$page_start  = $page_now;
if ($page_start > 1)
{
    $page_start = (($page_start-1)*$page_num) ;
}


if (($page_now*$page_num) > $max_record)
{
    $page_end = $max_record ;

}
else
{
    $page_end = ($page_now*$page_num);

}
/* pagination */


$data =	$db->page_search_opt($inp_select_country,
    $inp_select_city,
    $inp_keyword,
    number_format_zero2($inp_select_category),
    $_GET['inp_option'],
    $_GET['inp_review'],
    $_GET['inp_time'],
    $_GET[order],
    $page_first,
    $page_num);
/* matching category */
$sql  = 'SELECT theme_id,theme_name FROM ';
$sql .=  '(SELECT IF(theme_id<100,IF (theme_id<10,CONCAT(\'00\',theme_id),CONCAT(\'0\',theme_id)),theme_id) theme_id,theme_name FROM '._DB_PREFIX_TABLE.'theme ) AS a ORDER BY theme_id ';
$result = $db->db_query($sql);

$i= 0;

$arr_category = array();
while ($record = mysql_fetch_array($result))
{
    $arr_category['id'][$i]  = $record['theme_id'];
    $arr_category['name'][$i] = $record['theme_name'];
    $i++;
}
$buffer_data = '';
$data_path ='';

for ($i=0; $i < count( $data ) ; $i++)
{
    $list_category = explode(';',$data[$i]['product_theme']) ;
    $category_ofproduct = array();

    for ($k=0; $k < count($list_category);$k++)
    {
        for ($j=0;$j< count($arr_category['id']);$j++)
        {
            if ($list_category[$k] == $arr_category['id'][$j])
            {
                $category_ofproduct[$j] =  $arr_category['name'][$j];
            }
        }
    }
    $show_rate =  $db->show_rate($data[$i][country_iso3]);

    $location = $data[$i]['path'];
/*
    $buffer_data .= them_search($data,$i,$category_ofproduct,$path,$location, array('rate' => $data[$i][rate] ,
        'sign' => $data[$i][sign] ,
        'show' => $show_rate ) ,"OPT");
*/
    $buffer_data .= opt_search($data,$i,$category_ofproduct,$path,$location, array('rate' => $data[$i][rate] ,
        'sign' => $data[$i][sign] ,
        'show' => $show_rate ) ,"OPT");
}
/* matching category */


/* produuct path setting   */

$img  = $path.'product/images/product/';
$link = $path.'product.php?product_id=';

/* produuct path setting   */


switch ($_GET[order])
{
    case '1' : $inp_order = '<select name="order" class="form-control ts-fc" style="border: 1px solid #d3d3d3;">
								<option value="1" selected="selected">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3">人気順</option>
							</select>'	; break;

    case '2' : $inp_order = '<select name="order" class="form-control ts-fc" style="border: 1px solid #d3d3d3;">
								<option value="1">価格が高い順</option>
								<option value="2" selected="selected">価格が安い順</option>
								<option value="3">人気順</option>
							</select>'	; break;

    case '3' : $inp_order = '<select name="order" class="form-control ts-fc" style="border: 1px solid #d3d3d3;">
								<option value="1">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3" selected="selected">人気順</option>
							</select>'	; break;

    default : $inp_order = '<select name="order" class="form-control ts-fc" style="border: 1px solid #d3d3d3;">
								<option value="1">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3" selected="selected">人気順</option>
							</select>'	; break;
}


$smarty = new Smarty;
$smarty->template_dir = 'webapp/templates';
$smarty->compile_dir = 'webapp/templates_c';

/* center product*/
$site_country = 'ALL';

/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_city",$select_city);

$smarty->assign("select_category",$select_category);
$smarty->assign("select_time",$select_time);
/* search box */


/* banner */
$config[documentroot] = $path;

$smarty->assign("config",$config);
include("include/jtb_navigation.php");

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("select_day",$select_day);
$smarty->assign("select_option",$select_option);
$smarty->assign("select_review",$select_review);

$smarty->assign("pagination",$pagination);

$smarty->assign("max_record",$max_record);
$smarty->assign("page_start",$page_start);
$smarty->assign("page_end",$page_end);

$smarty->assign("order_select",$inp_order);
$smarty->assign("link1",$link_no_order);
$smarty->assign("inp_keyword",$inp_search_keyword);

//
$smarty->assign("search_content",$buffer_data);

//
$smarty->display('jtb_search_page_opt.tpl');
/* select tempage */

/* set tempage */

?>
