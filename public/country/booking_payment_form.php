<?
if ( $_SERVER["SERVER_PORT"] != 443 ){
//    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}
$path = '../';

require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
require_once($path."include/config.php");

if (!empty($_REQUEST[transection]))
{
    $sql = 'SELECT book_id,session_status FROM '._DB_PREFIX_TABLE.'booking WHERE md5(book_id) = "'.trim($_REQUEST[transection]).'" ';
    $result = $db->db_query($sql);
}
while ($record = mysql_fetch_array($result))
{
    $cbook_id = $record[book_id];
}
if ($cbook_id != 0)
{
    $transection = $_REQUEST[transection];
    $book_id = $cbook_id;
}else{
    header("Location: booking_payment.php?msg=error&transection=".$transection);
}

//For getting charge info
$charge_info['site_code'] = $_POST['site_code'];
$charge_info['amount'] = $_POST['amount'];
$charge_info['currency'] = $_POST['currency'];
$charge_info['email'] = $_POST['email'];
$charge_info['order_code'] = $_POST['order_code'];
$charge_info['user_id'] = $_POST['user_id'];
$charge_info['optional'] = $_POST['optional'];

//Get website information
$data = fetch_site_info($site_country);
$branch['company'] = $data['branch_name'];
$branch['optional']['time'] = $data['opt_time'];
$branch['optional']['tel'] = $data['opt_tel'];
$branch['optional']['email'] = $data['opt_email'];
$branch['souvenir']['company'] = $data['souv_company'];
$branch['souvenir']['time'] = $data['souv_time'];
$branch['souvenir']['tel'] = $data['souv_tel'];
$branch['souvenir']['email'] = $data['souv_email'];
$branch['site_code'] = $data['site_code'];

$type = $_GET['type'];
$paid_type = $_GET['paid_type'];
if($type == 'confirm'){
    $smarty = new Smarty;
    $config[documentroot] = '../';
    $smarty->assign("config",$config);
    $smarty->assign("country_iso3",$site_country);
    $smarty->assign("form_data",$_POST);
    $smarty->assign("branch",$branch);
    $smarty->assign("book_id",$book_id);
    $smarty->assign("product_kind","OPTIONAL_TOUR");
    $smarty->assign("transection",$transection);
    $smarty->assign("paid_type",$paid_type);
    $smarty->display('payment_form_confirm.tpl');
    exit;
}

$smarty = new Smarty;
$config[documentroot] = '../';
$smarty->assign("config",$config);
$smarty->assign("country_iso3",$site_country);
$smarty->assign("branch",$branch);
$smarty->assign("charge_info",$charge_info);
$smarty->assign("book_id",$book_id);
$smarty->assign("transection",$transection);
$smarty->assign("paid_type",$paid_type);
$smarty->assign("stripe_publish_key",_STRIPE_TEST_PUBLISH_KEY_);

$smarty->display('payment_form.tpl');
?>