<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$status_book = $_SESSION[pay_type];

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'オプショナルツアー','予約する'),array('../','./','./opt.php'));

/* For Menu Style */
include("../include/country_top_navi.php");

require_once($path."include/config.php");
$config['allot'] = $_SESSION['checkout']['allot'];

$product_id = $_POST[cart_product_id];
$result  = $db->view_product($product_id);

$g_order_id = $_SESSION['g_order_id'];
$sql = 'SELECT a.* ,b.* FROM '._DB_PREFIX_TABLE.'booking a
			LEFT JOIN '._DB_PREFIX_TABLE.'booking_detail b ON a.book_id = b.book_id
			WHERE a.book_id = "'.$g_order_id.'" ';
$result =$db->db_query($sql);
while ($record = mysql_fetch_array($result))
{
    $book_id = $record['book_id'];
    $product_id = $record['product_id'];
    $product_data  = $db->view_product($product_id);

    $total_price = $record['amount_adult'] + $record['amount_child'] + $record['amount_infant'];
    $converted_total_price = fetch_local_amount($product_data['code'], $total_price, 'JPY');

    $converted_price = fetch_local_amount($product_data['code'], $product_data['price_min'], 'JPY');

    $product_data_array[] = array(
        'sku' => $product_data['product_code'],
        'name' => $product_data['product_name_jp'],
        'category' => $product_data['product_type'],
        'price' => $converted_price,
        'quantity' => $record['qty_amount'],
    );
}


//$order_info
$order_info[]  = array(
    'transactionId' => $book_id,
    'transactionAffiliation' => '', //販売代理店
    'transactionTotal' => $converted_total_price, // price
    'transactionTax' => '', //tax
    'transactionShipping' => '', //shipping cost
    'transactionProducts' => $product_data_array
);
//print_r($order_info);
//exit;
unset($_SESSION['g_order_id']);

$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;



$smarty->assign("form",$form);
$smarty->assign("a_status_book",$_SESSION[status_book]);
$smarty->assign("status_book",$_SESSION[pay_type]);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("booking",$booking);
$smarty->assign("order_info",json_encode($order_info));
$smarty->assign("config",$config);
$smarty->display('booking6.tpl');
?>

