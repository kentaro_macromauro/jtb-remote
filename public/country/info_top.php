<?

require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path."include/config.php");
$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'現地情報'),array('../','./',''));

/* For Menu Style */
include("../include/country_top_navi.php");

$rate = $db->get_currency_rate($site_country);

/* For Footer banner */
$country_path = $db->showpath_bycountry($site_country);
include("../include/country_footer_banner.php");

/* news */
$root_spcontent = $path.'../article_img/index.php?root='.$countryname.'-content&amp;width=170&amp;height=70&amp;name=';
$country_news = $db->fetch_news(0,5,$site_country,"yes");
$news = array();
for ($i = 0; $i < count($country_news[id]) ;$i++)
{
    $news[ $country_news['id'][$i] ] 	= array( 'id' => $country_news['id'][$i],
        'date' =>  $country_news['date'][$i],
        'subject' => $country_news['subject'][$i],
        'icon' => $country_news['icon'][$i] );
}
/* news */

/* branch special content */
$special_contents['indonesia'][0]['file'] = '160326-01';
$special_contents['indonesia'][0]['title'] = '象に乗ってお散歩しよう！';
$special_contents['malaysia'][0]['file'] = '160427-01';
$special_contents['malaysia'][0]['title'] = '世界遺産キナバル公園とキャノピーウォーク';
$special_contents['malaysia'][1]['file'] = '160427-02';
$special_contents['malaysia'][1]['title'] = 'マングローブリバークルーズとホタルツアー';
$special_contents['malaysia'][2]['file'] = '160427-03';
$special_contents['malaysia'][2]['title'] = 'サピ島アイランドホッピング';
$special_contents['malaysia'][3]['file'] = '160427-04';
$special_contents['malaysia'][3]['title'] = ' ロッカウィーワイルドライフパークとローカルランチ';


$path_special_content = $path.'../../../'.$countryname.'/images/special_content/';
for ($i = 0; $i < count($special_contents[$countryname]); $i++)
{
    if($i == 0 || ($i % 4) == 0){
        $special_page .= '<div style="height: 16px;"></div>';
        $special_page .= '<div class="row">';
    }
    if($i == 0 || ($i % 2) == 0){
        $special_page .= '<div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail1">';
    }else{
        $special_page .= '<div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail2">';
    }
    $special_page .= '<div class="img-responsive img-box">';
    $special_page .= '<a href="special_content_detail.php?id='.$special_contents[$countryname][$i]['file'].'.html"><img src="'.$path_special_content.''.$special_contents[$countryname][$i]["file"].'.jpg'.'" alt="'.$special_contents[$countryname][$i]["title"].'"></a></div>';
    $special_page .= '<div class="grey-thumb-ct-greenlink"><a href="special_content_detail.php?id='.$special_contents[$countryname][$i]['file'].'.html"><b>'.$special_contents[$countryname][$i]["title"].'</b></a></div>';
    $special_page .= '</div>';
    if((($i % 4) == 3 && $i != 0) || $i == count($special_contents[$countryname])-1){
        $special_page .= '</div>';
    }
}

/* branch sp content */
$branch_content = $db->fetch_branch_content(0,10,$site_country,"yes");
$root_spcontent = $path.'../../../article_img/index.php?root='.$countryname.'-content&amp;width=400&amp;height=300&amp;name=';
$sp_content = array();
$sp_content = '';
for ($i = 0; $i < count($branch_content[id]); $i++)
{
    if($i == 0 || ($i % 4) == 0){
        $sp_content .= '<div style="height: 16px;"></div>';
        $sp_content .= '<div class="row">';
    }
    if($i == 0 || ($i % 2) == 0){
        $sp_content .= '<div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail1">';
    }else{
        $sp_content .= '<div class="col-md-3 col-sm-3 col-xs-6 four-thumbnail2">';
    }
    $sp_content .= '<div class="img-responsive img-box">';
    $sp_content .= '<a href="branch_content_detail.php?id='.$branch_content['id'][$i].'"><img src="'.$root_spcontent.''.$branch_content["id"][$i].'-1.jpg'.'" alt="'.$branch_content["subject"][$i].'"></a></div>';
    $sp_content .= '<div class="grey-thumb-ct-greenlink"><a href="branch_content_detail.php?id='.$branch_content['id'][$i].'"><b>'.$branch_content["subject"][$i].'</b></a></div>';
    $sp_content .= '</div>';
    if((($i % 4) == 3 && $i != 0) || $i == count($branch_content[id])-1){
        $sp_content .= '</div>';
    }
}

$record = $db->get_country();
$inp_country = input_selectbox('inp_country',$record[1],$record[0],$site_country,'-Please Select-','form-control') ;
$inp_city = input_selectbox('inp_city',$arr_city[1],$arr_city[0],$_GET[inp_city],'-Please Select-','form-control') ;

//sales_ranking
$sales_ranking = fetch_sales_ranking($site_country, $path,3);

//recommend_tour
$recommend_tour = fetch_recommend_tour($site_country, $path,3);

$smarty = new Smarty;

$config[documentroot] = $path;
$smarty->assign("data_tempage",$c);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
include("../include/check_souvenir_cart.php");

$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_city",$inp_city);

$smarty->assign("special_page",$special_page);

$smarty->assign("sp_content",$sp_content);

$smarty->assign("sales_ranking",$sales_ranking);
$smarty->assign("recommend_tour",$recommend_tour);

$smarty->assign("footer_banner_img",$footer_banner_img);

/* news */
$smarty->assign("news",$news);
$smarty->display('country_info.tpl');
?>