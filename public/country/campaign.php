<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$rate = $db->get_currency_rate($site_country);

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

require_once($path."include/config.php");

/* produuct path setting   *//*
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';*/

function print_right_price_start_to_campan($sgd,$yen)
{
	if ($yen[show] == 1)
	{
		return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'～( 目安：\\'.number_format( round($yen[rate]*$sgd) ).'～)</span>';
	}
	else
	{
		return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'～';
	}
}

if (empty($_GET[id]))
{
	header("Location: index.php");
	
}

$campaign = $db->view_promotion_country($_GET[id]);

$campaign_id = $campaign[promo_id];
$campaign_tittle = $campaign[promo_tittle];
$campaign_content = nl2br($campaign[promo_detail]);
										
$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'キャンペーン･特集',$campaign_tittle),array('../','./','special_campaign.php'));


if (empty($campaign_id))
{
	header("Location: index.php");
}	
	
										
	$arr_product_list =  explode(',',$campaign[product_list]); 
	
	$sql = 'SELECT product_id,path,
		    b.country_iso3,a.city_iso3,country_name_en,country_name_jp,city_name_en,city_name_jp,
			product_code,product_name_en,product_name_jp,short_desc,
			price_min,price_max,
			a.update_date,rate,
			rev_status,rev_date,d.sign   
			FROM 
			'._DB_PREFIX_TABLE.'product a 
			LEFT JOIN 
			'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3 AND a.country_iso3 = b.country_iso3 
			LEFT JOIN
			'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 
			LEFT JOIN mbus_currency d ON a.country_iso3 = d.country_iso3 
			WHERE ';
			
	
	for ($i =0; $i < count($arr_product_list); $i++)
	{
		if (!empty( $arr_product_list[$i] ) )
		{		
			$sql .= 'product_id = "'.$arr_product_list[$i].'" OR ';		
		}
	}
	
	$sql .= 'product_id = "0" ';
	
	$sql .= 'ORDER BY field(product_id,'.$campaign[product_list].'0)';
	

	
	$result = $db->db_query($sql); $i=0;
	
	$product_campaign = '<ul class="campaign-product-unit">'; 
	

	
	while ($rec = mysql_fetch_array($result))
	{
		
		$product_list[id][$i]      = $rec[product_id];
		$product_list[path][$i]    = $rec[path];
		$product_list[country][$i] = $rec[country_name_jp];
		$product_list[name][$i]    = $rec[product_name_jp];
		//$product_list[price][$i]   = show_price($rec['price_min'],$rate[0]);
		$product_list[desc][$i]    = $rec[short_desc];
		
		
		$imgcheck =  trim( DEV_PATH.'../product/images/product/'.$product_list[id][$i].'-1.jpg' ) ;
	
		if (!file_exists($imgcheck) )
		{
			$img = '<img alt="" src="../images/img_notfound.jpg" width="170" height="70" />';
		}
		else
		{
			
			$product_img = '../product/images/index.php?root=product&amp;width=170&amp;name='.trim($product_list[id][$i].'-1.jpg') ;
			$img = '<p class="trimming"><img alt="" src="'.DEV_PATH.$product_img.'" width="170" /></p>';
		}
			
		
		$product_list[id][$i]      = $rec[product_id];
		$product_list[path][$i]    = $rec[path];
		$product_list[country][$i] = $rec[country_name_jp];
		$product_list[name][$i]    = $rec[product_name_jp];
		
		$product_list[price][$i]   = print_right_price_start_to_campan( 
									$rec['price_min'], 
									array( 'rate' => $rec[rate] , 
										   'sign' => $rec[sign] , 
										   'show' => $db->show_rate($rec['country_iso3']) )  
									) ;
		
		//$product_list[price][$i]   = show_price($rec['product_name_jp'],$rate[0]);
		$product_list[desc][$i]    = $rec[short_desc];
		
		$product_campaign .= '<li>
								<div class="campaign-product-list">
									<div class="campaign-product-img"><a href="product.php?product_id='.$product_list[id][$i].'">'.$img.'</a></div>
									<div class="campaign-product-country">'.$product_list[country][$i].'</div>
									<div class="campaign-product-tittle"><a href="product.php?product_id='.$product_list[id][$i].'">'.str_len( $product_list[name][$i] ,20).'</a></div>	
									<p class="campaign-product-des">'.str_len($product_list[desc][$i],85).'</p>
									<p class="campaign-product-price"><span>'.$product_list[price][$i].'</span></p>
								</div>
							 </li>';  
	}
	
	
	$product_campaign .= "</ul>";			
					
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';


$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;
include("../include/check_souvenir_cart.php");

$smarty->assign("campaign_id",$campaign_id);
$smarty->assign("campaign_tittle",$campaign_tittle);
$smarty->assign("campaign_content",$campaign_content);
$smarty->assign("campaign_list",$product_campaign);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);
$smarty->display('country_campaign.tpl');
?>