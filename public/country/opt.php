<?

if ( $_SERVER["SERVER_PORT"] == 443 ){
    Header("Location: http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);
$page_type	  = 'OPT';
$banner_link = 'images/banner/';
require_once($path."include/config.php");

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'オプショナルツアー'),array('../','./',''));

/* For Menu Style */
include("../include/country_top_navi.php");

/* For slide banner */
$country_path = $db->showpath_bycountry($site_country);
$top_banner_path  = $path.$country_path.'/images/top_banner/';
if($site_country == 'ALL'){
    $top_banner_path  = $path.'images/top_banner/';
}
$record = $db->fetch_top_banner(0,20, $site_country,'yes');
$top_banner_img = '';
for ($i=0; $i< count( $record['top_banner_id']) ;$i++)
{
    if($record['jump'][$i] == 1){
        $target = '';
        if($record['new_window'][$i] == 1){
            $target = 'target="_blank"';
        }
        $custom_id = $countryname.'_#'.($i+1);
        $top_banner_img .= '<a href="'.$record['link'][$i].'" '.$target.' id="'.$custom_id.'" class="slide"><img src="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg" data-thumb="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg"></a>';
    }else{
        $top_banner_img .= '<img src="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg" data-thumb="'.$top_banner_path.$record['top_banner_id'][$i].'-1.jpg">';
    }
}

/* For Footer banner */
include("../include/country_footer_banner.php");

$record = $db->get_country();
$inp_country = input_selectbox('inp_country',$record[1],$record[0],$site_country,'-Please Select-','form-control') ;

$sql = 'SELECT city_id,city_name_jp,city_iso3 FROM '._DB_PREFIX_TABLE.'city WHERE country_iso3 = UPPER("'.$site_country.'") ORDER BY city_idx,city_id';

$result = $db->db_query($sql); $i =0 ;

while ( $record1 = mysql_fetch_array($result))
{
	$arr_city[0][$i] = $record1[city_iso3];
	$arr_city[1][$i] = $record1[city_name_jp];
	$i++;
}
$inp_city = input_selectbox('inp_city',$arr_city[1],$arr_city[0],$_GET[inp_city],'-Please Select-','form-control') ;

$record = $db->view_theme_all();
$select_category  = '';

for ($i=0; $i< count( $record['value']) ;$i++)
{
	$select_category .= '<option value="'.number_format_zero2($record['value'][$i]).'" >'.$record['data'][$i].'</option>';
}

$record = $db->view_option_all();
$select_option = '';
for ($i=0; $i< count( $record['value']) ;$i++)
{
	$select_option .= '<option value="'.$record['value'][$i].'" >'.$record['data'][$i].'</option>';
}

$record = $db->viw_time_all();
$select_time  = '';
for ($i=0; $i< count( $record['value']) ;$i++)
{
	$select_time .= '<option value="'.$record['value'][$i].'" >'.$record['data'][$i].'</option>';
}
$result = $db->view_option_all();

//sales_ranking
$sales_ranking = fetch_sales_ranking($site_country, $path,4);

//recommend_tour
$recommend_tour = fetch_recommend_tour($site_country, $path,4);

//spot
$spot_data = fetch_spot($site_country, $path);

$smarty = new Smarty;
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);

/* For Top slide */
$smarty->assign("top_banner_img",$top_banner_img);
$smarty->assign("footer_banner_img",$footer_banner_img);

/* For Top Contents */
$smarty->assign("sales_ranking",$sales_ranking);
$smarty->assign("recommend_tour",$recommend_tour);
$smarty->assign("spot_data",$spot_data);

/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("select_category",$select_category);
$smarty->assign("select_time",$select_time);
$smarty->assign("select_option",$select_option);
/* search box */

/* banner */

$config[documentroot] = '../../';

$smarty->assign("config",$config);
$smarty->assign("inp_city",$inp_city);

include("../include/check_souvenir_cart.php");

/* select tempage */
$smarty->display('country_opt.tpl');
/* select tempage */
?>
