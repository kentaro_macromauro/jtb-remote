<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path."include/config.php");
$breadcamp = '<p><a href="../" style="color: #000; font-size: 12px;">TOP</a>&nbsp;&nbsp;>&nbsp;&nbsp;<a href="index.php" style="color: #000; font-size: 12px;">'.$site_name.'</a>';

/* For Menu Style */
include("../include/country_top_navi.php");

$rate = $db->get_currency_rate($site_country);

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

if (!empty($_GET[id]))
{
	$id= $_GET[id];
	$filepath = ($path."www_config/html/".$countryname."/".$id);
	$c = file_get_contents($filepath);

	if (empty($c))
	{
		header('Location: index.php');		
	}
	$title = mb_ereg_replace('^.*<h2.*?>(.*)h2.*$','\1',$c);
    $title = str_replace('</','',$title);

    $category = mb_ereg_replace('^.*<h1.*?>(.*)h1.*$','\1',$c);
    $category = str_replace('</','',$category);

    $c = mb_ereg_replace('^.*<!--country info-->(.*)<!--end country info-->.*$','\1',$c);

    if ($_REQUEST[id] == 'online_catalog.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
    }
	if ($_REQUEST[id] == 'qa.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
        $title = $site_name.'Q&amp;A';
	}
    if ($_REQUEST[id] == 'tourist_desk.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
        $title = $site_name.'トラベルデスク';
    }
    if ($_REQUEST[id] == 'info.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
        $title = $site_name.'情報';
    }
    if ($_REQUEST[id] == 'city.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
        $title = $site_name.'都市一覧';
    }
    if($category == '都市情報'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>';
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="page.php?id=city.html" style="color: #000; font-size: 12px;">'.$site_name.'都市一覧</a>';
    }
    if ($_REQUEST[id] == 'user_guide.html'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="souvenir.php" style="color: #000; font-size: 12px;">'.$site_name.'おみやげ</a>';
        $title = 'ユーザーガイド';
    }

	if (!empty($title))
	{
		if (strlen($title) < 1024)
		{
			$breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;'.$title.'</p>';
		}
		else
		{
			$breadcamp .= '</p>';
		}
	}
	else
	{
		$breadcamp .= '</p>';
	}


}
else
{
	
	if (empty($c))
	{
		header('Location: index.php');		
	}
	$c = 'not found information.';	
}

$entry_year = Date('Y');  $entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}



$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;
include("../include/check_souvenir_cart.php");

$smarty->assign("data_tempage",$c);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
$smarty->display('country_page.tpl');
?>