<?

require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path."include/config.php");

$sp_content = $db->view_branch_content($_GET[id]);

if ( ($sp_content['public'] == '0')||(empty($sp_content[branchct_subject]))||(empty($sp_content['public'])) )
{
	header("Location: index.php");
}


$rate = $db->get_currency_rate($site_country);

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */


$subject =  jd_decode($sp_content[branchct_subject]);
$content =  '<div class="header-text-tittle-news">'.$subject.'</div><div class="text-content pleft5" >'.jd_decode($sp_content[branchct_countent])."</div>";

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'現地情報',str_len($subject,50)),array('../','./','./info_top.php',''));

/* For Menu Style */
include("../include/country_top_navi.php");

$entry_year = Date('Y');  $entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}



$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;
include("../include/check_souvenir_cart.php");

$smarty->assign("data_tempage", $content);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
$smarty->display('country_page.tpl');
?>
