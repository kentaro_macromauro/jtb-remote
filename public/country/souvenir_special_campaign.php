<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$rate = $db->get_currency_rate($site_country);

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

require_once($path."include/config.php");


function print_right_price_start_to_campan($sgd,$yen)
{
	if ($yen[affiliate]){
		return '<span class="txt-red-bold-price">\\'.number_format($sgd).'';
	}
	else{
	
		if ($yen[show] == 1)
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).' ( 目安：\\'.number_format( round($yen[rate]*$sgd) ).')</span>';
		}
		else
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'';
		}
	
	}
	 
}

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'おみやげ'),array('../','./',''));






$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/souvenir_product.php?product_id=';

/* */	
		
$promotion_opt ='';		
$simg  = './images/souvenir_banner/';
$slink = './souvenir_campaign.php?product_id=';

$sql = 'SELECT path,a.country_iso3,scampaign_id FROM mbus_souvenir_campaign a LEFT JOIN mbus_country b ON 
		a.country_iso3 = b.country_iso3 
		WHERE a.public = 1 AND a.country_iso3 = "'.$site_country.'" ORDER BY a.update_date DESC,a.country_iso3 ';

$result = $db->db_query($sql);
		
while ($record = mysql_fetch_array($result)){
	

	
	$promotion_opt .= '<li><a href="./souvenir_campaign.php?id='.$record[scampaign_id].'" ><img src="./images/souvenir_banner/'.$record[scampaign_id].'-1.jpg" width="767" alt="" /></a></li>';

}
	
/**/	


$smarty = new Smarty;
include("../include/souvenir_country_right_menu.php");

$config[documentroot] = $path;

$smarty->assign("banner_opt",$promotion_opt);

$smarty->assign("campaign_id",$campaign_id);
$smarty->assign("campaign_tittle",$campaign_tittle);
$smarty->assign("campaign_content",$campaign_content);
$smarty->assign("campaign_list",$product_campaign);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);
$smarty->display('country_souvenir_special_campaign.tpl');
?>