<?
$msg_error = array('日付の形式が正しくありません。',
				   '"1"以上をご入力ください。');

$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'class/c_service.php');


$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

require_once($path."include/config.php");
$config['allot'] = 'true';

$breadcamp = breadcamp(array('TOP','予約する'),array('index.php'));

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

/* reservation  */
	$age = array('child_from'=> 0,
				 'child_to'  => 0,
				 'infant'    => 0);

	
	$product_id = $_REQUEST[product_id];
	
	
	if (empty($_REQUEST[product_id]))
	{
		header("Location: index.php");	
	}
	
	$result  = $db->view_product($product_id);
	
	$status_book = $result['status_book'];
	
	if ($result['public'] != '1')
	{
		header( "HTTP/1.1 301 Moved Permanently" ); 
		header('Location: index.php');	
	}
	
	
	
	if ($result[country_iso3] != $site_country)
	{
		$url = $_SERVER[REQUEST_URI];
		
			$refpath  = $db->showpath_bycountry($result[country_iso3]);
			
	
			$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
			
			header('Location: '.$url);
	}

	
	
	$arr_age = $db->find_age($result[country_iso3]);
	
	
	$age = array('child_from'=> ($arr_age[1]+1),
				 'child_to'  => ($arr_age[0]) ,
				 'infant'    => ($arr_age[1]));
	
	
		
	/* select year month date */
	$entry_year  = Date('Y');
	$entry_month = Date('n');
	
	$arr_yearmonth_data = array();
	$arr_yearmonth_value = array();
	$arr_day_data   = array();
	$arr_day_value  = array();
	
	function zero_fill($data)
	{
		if (($data) < 10)
		{
			$data = '0'.$data;
		}
		
		return $data;	
	}
	
	/* inputyearmonth */
	for ($i=0; $i<12;$i++)
	{
		
		if ( ($entry_month + $i) > 12)
		{
			$mm = ($entry_month + $i) - 12;
			
			$arr_yearmonth_value[$i] = ($entry_year+1).'-'.($mm);
			$arr_yearmonth_data[$i]  = ($entry_year+1).'年 '.zero_fill($mm).'月';
		}
		else
		{
			$arr_yearmonth_value[$i] = $entry_year.'-'.($entry_month + $i);
			$arr_yearmonth_data[$i]  = $entry_year.'年 '.zero_fill($entry_month + $i).'月';
		}
	}
	/* inputyearmont */
	
	/* inputday */
	$select_day		  = input_selectbox('inp_day',array(),array(),$_GET[inp_day],'日','search-left-selectbox-dd');
	/* inputday */
	
	
	
	if ( !empty( $_POST[inp_yearmonth] ) )
	{
		$arr_lipday = explode('-', $_POST[inp_yearmonth]);
		
		$month = $arr_lipday[1];
		$year  = $arr_lipday[0]; 
		
		
		$max_day = 30;
		//echo cal_days_in_month(CAL_GREGORIAN,3, 2012);
		
		
		for ($i=0; $i < $max_day; $i++)
		{
			$arr_day_data[$i] = zero_fill($i+1).'日';
			$arr_day_value[$i] = ($i+1);
		}
		
		$select_day   = input_selectbox('inp_day',$arr_day_data,$arr_day_value,$_POST[inp_day],'日','search-left-selectbox-dd');
	}
	
	
	
	$select_yearmonth = input_selectbox('inp_yearmonth',$arr_yearmonth_data,$arr_yearmonth_value,$_POST[inp_yearmonth],'年月','search-left-selectbox-mmyy');
	
	/* select year month date */

	for ($i=0;$i<10;$i++)
	{	
		$array100[$i] = $i+1;
	}
	/* setup */
	
	$rate = $db->get_currency_rate($site_country);

	$currency_rate = $rate[0][rate];
	
	$currency = $rate;
	
	/* setup */
	
	
/* reservation */


/* check day cut off*/

$day_cutoff = $result[day_cutoff];

if ($day_cutoff == 0)
{
	$day_cutoff = 1;	
}

if ($day_cutoff == 999)
{
	$day_cutoff = 1;
}


$min_pax  = $result[pax_min];

if ($min_pax == 0)
{
	$min_pax = 1;	
}

if ($min_pax == 999)
{
	$min_pax = 1;	
}
/* check day cut off */

$booking  = array( 'product'=>array( 'id' =>  $product_id,
									 'name_jp' => $result[product_name_jp],
									 'img' => "../product/images/index.php?root=product&width=140&name=".$product_id."-1.jpg",
									 'description'  => $result[short_desc],
									 'product_date' => '',
									 'price_adult'  => 0,
									 'price_child'  => 0,
									 'price_infant' => 0,
									 'pax_min' => $min_pax,
									 'day_cutoff' => $day_cutoff,
									 ),
					'input' => array('select_yearmonth' => $select_yearmonth,
					 				 'select_day' => $select_day,
									 'adult' =>  input_selectbox('inp_adult',$array100,$array100,'','---') ,
									 'child'  => input_selectbox('inp_child',$array100,$array100,'','---') ,
									 'infant' => input_selectbox('inp_infant',$array100,$array100,'','---') ,
									 ),
					'other' => array('currency_rate' =>  $currency_rate ),
				    'page' => '0' );


$booking['action'] = "check";

if (empty($_SESSION[temp]))
{
	$_SESSION[temp] = array();
}

if ($_POST[action] == "check")
{
	$product_code   = $db->get_product_code($product_id);
	$customer_id    = "AGT01";
	
	
	
	$_SESSION[index_id] = session_id();   
	
	$session_id         = $_SESSION[index_id];
	session_regenerate_id();
	$transection_id     = session_id();
	$product_type       = "OPT";
	
	$date_inp  = $_POST[inp_yearmonth].'-'.$_POST[inp_day];
	$arr_date = explode('-',$date_inp);
	
	if ( ($arr_date[2] == "") || ($arr_date[1] =="") || ($arr_date[0] == "") )
	{
		$error['date'] = "日付の形式が正しくありません。";	
	}
	
	
	$sql = 'SELECT 
			if ( date("'.$date_inp.'") >  
			date(INTERVAL if (day_cutoff=999,0,day_cutoff) DAY + DATE_FORMAT(now(),"%Y-%m-%d")) , "Y" , "N" )
			cutoff  
			FROM '._DB_PREFIX_TABLE.'product 
			WHERE product_id = "'.$product_id.'" ';
	
	
	$result = $db->db_query($sql);
	
	while ($record = mysql_fetch_array($result))
	{
		$day_cut_off =  $record[cutoff];	
	}
	
	if ($day_cut_off == "Y")
	{

		$inp_date = zero_fill($arr_date[2]).'/'.zero_fill($arr_date[1]).'/'.$arr_date[0];
		
		
		$params = array ("cmd"    => "getproductpricedetails" ,
						 "params" => array("productid"      =>  $product_code,
											"producttype"   =>  $product_type,
											"date"          => 	$inp_date ,
											"customerid"    =>  $customer_id,
											"sessionid"     => 	$session_id,
											"transactionid" =>  $transection_id));
		
		
		$api   = new c_websv($params);
		$data  =  $api->getall();			//Step1:Check Price
		

		if ( $data['d']['statuscode'] == "E_SUCC") //1:SUCC
		{
			
			for ($i=0;$i< count( $data[d][output][PriceDetail] ) ; $i++ )
			{
				if ($data[d][output][PriceDetail][$i][ChargeTypeDesc] == "Adult")
				{
					$adult_price = $data[d][output][PriceDetail][$i][Price] ;
				}
					
				if ($data[d][output][PriceDetail][$i][ChargeTypeDesc] == "Child") 
				{
					$child_price = $data[d][output][PriceDetail][$i][Price] ;
				}
			}
			
			
			if ( ( ($adult_price == 0) && ($child_price == 0) ) || ( empty($adult_price) && empty($child_price)  ) )//1.1: Price = 0
			{
					$error['product'] = 'Not availability.';
			}
			else //1.2:Price > 0
			{
				session_regenerate_id();
				$transection_id     = session_id();
				
				$booking[product][price_adult]  = $adult_price;
				$booking[product][price_child]  = $child_price;
				
				
				
				if ( ($adult_price == 1)  || ($child_price == 1) ) //1.2.1:Price = 1:Case On Request
				{
					
					$params = array ("cmd"    => "getPRODUCTAVAILABILITY" ,
									 "params" => array("productid"      =>  $product_code,
														"producttype"   =>  $product_type,
														"date"          => 	$inp_date ,
														"customerid"    =>  $customer_id,
														"sessionid"     => 	$session_id,
														"transactionid" =>  $transection_id));
					$api   = new c_websv($params);
					$data  =  $api->getall();
					
					
					if ($data['d']['statuscode'] == "E_SUCC") //1.2.1.1:Case On Request:Check Availability
					{
						
						if ( $data['d']['output']['Availiable'] == "true") //1.2.1.1.1:Case On Request:Check Availability:True
						{
							$allot = 'true';

							
						
						
							session_regenerate_id();
							$transection_id     = session_id();
								
							$params = array("cmd"    => "HOLDALLOTMENT",
											"params" => array(
											"productid"   => $product_code,
											"producttype" => $product_type,
											"date"        => $inp_date,
											"customerid"  => $customer_id,
											"sessionid"   => $session_id,
											"transactionid" => $transection_id));
								
							$api   = new c_websv($params);
								
							$data  =  $api->getall();
							
							
							if ($data['d']['statuscode'] == "E_SUCC")
							{						
								
								$booking[product][allot]	    = $allot;
								
								$booking[product][cfm_code]     = "";
								
								$booking[product][product_yearmonth] = $_POST[inp_yearmonth];
								$booking[product][product_day]       = $_POST[inp_day];
									
								$booking[product][price_adult]  = $adult_price;
								$booking[product][price_child]  = $child_price;
								$booking[product][price_infant] = $infant_price;
									
								
								$booking['page']  = '1';
								
								$_SESSION[temp][$product_id]  = array( 'price_adult'  =>  $adult_price ,
																	   'price_child'  =>  $child_price,
																	   'price_infant' =>  $infant_price,
																	   'cfm_code'     =>  $booking[product][cfm_code],
																	   'allot'		  =>  $allot);
								$_SESSION['checkout']['allot'] = $allot;
		
									
								$status_book = 4;
								
									
							}
							else
							{
								
								$booking[product][allot]	    = $allot;
								
								$booking[product][cfm_code]     = "";
								
								$booking[product][product_yearmonth] = $_POST[inp_yearmonth];
								$booking[product][product_day]       = $_POST[inp_day];
									
								$booking[product][price_adult]  = $adult_price;
								$booking[product][price_child]  = $child_price;
								$booking[product][price_infant] = $infant_price;
									
								
								$booking['page']  = '1';
								
								
								
								$_SESSION[temp][$product_id]  = array( 'price_adult'  =>  $adult_price ,
																	   'price_child'  =>  $child_price,
																	   'price_infant' =>  $infant_price,
																	   'cfm_code'     =>  $booking[product][cfm_code],
																	   'allot'		  =>  $allot);
								$_SESSION['checkout']['allot'] = $allot;
		
									
								$status_book = 4;
																
								//$error['product'] = 'API Return Fail : HOLDALLOTMENT.';
							}
						
						}
						else //1.2.1.1.2:Case On Request:Check Availability:False
						{
							
							$allot = 'false';
							
						}
						
						//echo 'true'; exit();
					}
					else
					{
						$error['product'] = 'API Return Fail : GETPRODUCTAVAILABILITY. ';
					}
					
				}
				else //1.2.2:Price > 1:Case Normal
				{		
					$params = array ("cmd"    => "GETPRODUCTAVAILABILITY" ,
									 "params" => array("productid"      =>  $product_code,
														"producttype"   =>  $product_type,
														"date"          => 	$inp_date ,
														"customerid"    =>  $customer_id,
														"sessionid"     => 	$session_id,
														"transactionid" =>  $transection_id));
					$api   = new c_websv($params);
					$data  =  $api->getall();
					
					/* check availiable */
					if ( $data['d']['statuscode'] == "E_SUCC")
					{
						if ( $data['d']['output']['Availiable'] == "true")
						{
							$allot		= 'true';
							$config['allot'] = 'true';
						
							
							/* HOLDALLOTMENT */
								session_regenerate_id();
								$transection_id     = session_id();
								
								$params = array(
											  "cmd"    => "HOLDALLOTMENT",
											  "params" => array(
											  "productid"   => $product_code,
											  "producttype" => $product_type,
											  "date"        => $inp_date,
											  "customerid"  => $customer_id,
											  "sessionid"   => $session_id,
											  "transactionid" => $transection_id));
								
								$api   = new c_websv($params);
								
								$data  =  $api->getall();
								
								if ($data['d']['statuscode'] == "E_SUCC")
								{
								
								
								
									$adult_price = 0;
									$child_price = 0;
									$infant_price = 0;
									
									
									if (!empty($data))
									{
										for ($i=0;$i< count( $data[d][output][Charges] ) ; $i++ )
										{
											if ($data[d][output][Charges][$i][ChargeTypeDescription] == "Adult")
											{
												$adult_price = $data[d][output][Charges][$i][Price] ;
											}
											
											if ($data[d][output][Charges][$i][ChargeTypeDescription] == "Child") 
											{
												$child_price = $data[d][output][Charges][$i][Price] ;
											}
											
											if ($data[d][output][Charges][$i][ChargeTypeDescription] == "Infant") 
											{
												$infant_price = $data[d][output][Charges][$i][Price] ;
											}
										}
									}
									$booking[product][allot]	    = $allot;
									
									$booking[product][cfm_code]     = $data[d][output][AllotmentConfirmationCode];
									
									$booking[product][product_yearmonth] = $_POST[inp_yearmonth];
									$booking[product][product_day]       = $_POST[inp_day];
									
									$booking[product][price_adult]  = $adult_price;
									$booking[product][price_child]  = $child_price;
									$booking[product][price_infant] = $infant_price;
									
									
									$booking['page']  = '1';
									
									$_SESSION[temp][$product_id]  = array( 'price_adult'  =>  $adult_price ,
																		   'price_child'  =>  $child_price,
																		   'price_infant' =>  $infant_price,
																		   'cfm_code'     =>  $booking[product][cfm_code],
																		   'allot'		=>  $allot);
									$_SESSION['checkout']['allot'] = $allot;
								
								}
								else
								{
									$error['product'] = 'API Return Fail : HOLDALLOTMENT.';
								}
								
							/* HOLDALLOTMENT */
												
							
							
						}
						else
						{
							$allot		= 'false';
							$config['allot'] = 'false';
							
							
							
							
							/* HOLDALLOTMENT */
								session_regenerate_id();
								$transection_id     = session_id();
								
								
								
								if ($data['d']['statuscode'] == "E_SUCC")
								{						
									
									$booking[product][allot]	    = $allot;
									
									$booking[product][cfm_code]     = "";
									
									$booking[product][product_yearmonth] = $_POST[inp_yearmonth];
									$booking[product][product_day]       = $_POST[inp_day];
									
									$booking[product][price_adult]  = $adult_price;
									$booking[product][price_child]  = $child_price;
									$booking[product][price_infant] = $infant_price;
									
									
									
									$booking['page']  = '1';
									
									$_SESSION[temp][$product_id]  = array( 'price_adult'  =>  $adult_price ,
																		   'price_child'  =>  $child_price,
																		   'price_infant' =>  $infant_price,
																		   'cfm_code'     =>  $booking[product][cfm_code],
																		   'allot'		=>  $allot);
									$_SESSION['checkout']['allot'] = $allot;
		
									
									$status_book = 4;
								
									
								}
								else
								{
									$error['product'] = 'API Return Fail : HOLDALLOTMENT.';
								}
								
							/* HOLDALLOTMENT */
		
						}	
					}
					else
					{
						$error['product'] = 'API Return Fail : GETPRODUCTAVAILABILITY.';
					}
				}
				
			}
			
			
		}
		else //2:Fail
		{
			$error['product'] = 'API Return Fail : GETPRODUCTPRICEDETAILS. ';
		}
		
		$print = array('price_adult' => 0,
						   'price_child' => 0,
						   'price_infant' => 0,
						   'price_amount_sgd' => 0);
	
		
	}
	else
	{
		$error['date'] = "ご指定日の催行はございません。";	
	}
	/* check availiable */
	
}

if ($_POST[action] == "cart")
{

	$booking[product][price_adult]  = $_POST[inp_price_adult];
	$booking[product][price_child]  = $_POST[inp_price_child];
	$booking[product][price_infant] = $_POST[inp_price_infant];
	$booking[product][id]			= $_POST[product_id];
	$product_id 					= $_POST[product_id];
	
	$pack  = ($_POST[inp_adult] + $_POST[inp_child]);  
	
	
	$count = '0';
	$sql = 'SELECT count(*) dd_count 
			FROM mbus_product a 
			LEFT JOIN mbus_product_detail b
			ON a.product_id = b.product_id
			LEFT JOIN mbus_city c 
			ON a.city_iso3 = c.city_iso3
			LEFT JOIN mbus_country d
			ON a.country_iso3 = d.country_iso3 
			WHERE ("'.$pack.'") between pax_min and pax_max AND a.product_id = "'.$product_id.'" ';
	$result = $db->db_query($sql);
	while ($record = mysql_fetch_array($result))
	{
		$count = $record[dd_count];	
	}
	
	if ($count > 0)
	{
		if ( empty( $_SESSION[temp][$product_id][price_adult] ) )
		{
			$_SESSION[temp][$product_id][price_adult] = 0;
		}
		
		if ( empty( $_SESSION[temp][$product_id][price_child] ) )
		{
			$_SESSION[temp][$product_id][price_child] = 0;
		}
		
		if ( empty( $_SESSION[temp][$product_id][price_infant] ) )
		{
			$_SESSION[temp][$product_id][price_infant] = 0;
		}
		
		
		if ($_SESSION[temp][$product_id][allot]  == 'true')
		{
			$allot = 'true';
		}
		else
		{
			$allot = 'false';
		}
		
		
		$basket = array('product_id'   => $_POST[product_id],
						'type'		   => 'OPT',
						'date' 		   => $_POST[inp_yearmonth].'-'.$_POST[inp_day],
						'price_adult'  => $_SESSION[temp][$product_id][price_adult],
						'price_child'  => $_SESSION[temp][$product_id][price_child],
						'price_infant' => $_SESSION[temp][$product_id][price_infant],
						'qty_adult'    => $_POST[inp_adult],
						'qty_child'    => $_POST[inp_child],
						'qty_infant'   => $_POST[inp_infant],
						'session_id'   => $_SESSION[index_id],
						'cfm_code'     => $_SESSION[temp][$product_id][cfm_code],
						'allot'		   => $allot,
						
						);
		
		
		$_SESSION[add_cart] = $basket; 
		
		
		
		
		header("Location: booking_cart.php");
		/*test */
		//$_SESSION[temp][$product_id][allot] = 'false';
		/*test */
		
		/*
		if ( $_SESSION[temp][$product_id][allot]  == 'true' )
		{
		
			header("Location: booking_cart.php");
		}
		else
		{
			
			$session_id   = $_SESSION[add_cart][session_id];
			
			
			$_SESSION['checkout'] = array( 'session_id'   =>  $session_id,							  
										   'product_id'   =>  $product_id,
										   
										   'cfm_code'     =>  $_SESSION[temp][$product_id][cfm_code],
										   
										   'booking_date' =>  $_POST[inp_yearmonth].'-'.$_POST[inp_day],
										   
										   'price_adult'  =>  $_SESSION[temp][$product_id][price_adult],
										   'price_child'  =>  $_SESSION[temp][$product_id][price_child],
										   'price_infant' =>  $_SESSION[temp][$product_id][price_infant],
										   
										   'qty_adult'    =>  $_POST[inp_adult],
										   'qty_child'    =>  $_POST[inp_child],
										   'qty_infant'   =>  $_POST[inp_infant],
										   'allot'		  =>  'false',
										   ); 
			
			header("Location: booking_infoinput.php");
		}*/
	}
	else
	{
		$error['number'] = "ご参加人数をご確認ください";
		
		
	}
	
	
	
	if ($_SESSION[temp][$product_id][allot] == 'false')
	{
		$status_book = 4;	
	}
	
	
	$booking[product][id]        = $_POST[product_id];
	$booking[product][product_yearmonth] = $_POST[inp_yearmonth];
	$booking[product][product_day]       = $_POST[inp_day];
	$booking[product][cfm_code]   = $_POST[cfm_code];
	
	$booking['page']  = '1';
	
	
	$booking[input][adult]  = input_selectbox('inp_adult',$array100,$array100,$_POST[inp_adult],'---') ;
	$booking[input][child]  = input_selectbox('inp_child',$array100,$array100,$_POST[inp_child],'---') ;
	$booking[input][infant] = input_selectbox('inp_infant',$array100,$array100,$_POST[inp_infant],'---') ;


	$print = array('price_adult' => trim( number_format( ($_POST[inp_price_adult] * $_POST[inp_adult]))) ,
			       'price_child' => trim( number_format(  ($_POST[inp_price_child] * $_POST[inp_child])    )),
			       'price_infant' => trim( number_format( ($_POST[inp_price_infant] * $_POST[inp_infant]) )),
				   
				   
				   'price_adult_yen' => trim( number_format( ($_POST[inp_price_adult] * $_POST[inp_adult]) * $currency_rate     )),
			       'price_child_yen' => trim( number_format (  ($_POST[inp_price_child] * $_POST[inp_child]) * $currency_rate   )),
			       'price_infant_yen' => trim( number_format ( ($_POST[inp_price_infant] * $_POST[inp_infant]) * $currency_rate )),
			       'price_amount_sgd' => trim( number_format( ($_POST[inp_price_adult] * $_POST[inp_adult]) +
													($_POST[inp_price_child] * $_POST[inp_child]) +
													($_POST[inp_price_infant] * $_POST[inp_infant])
													) ),
			       'price_amount_yen' =>trim(  number_format( (($_POST[inp_price_adult] * $_POST[inp_adult])* $currency_rate) +
													(($_POST[inp_price_child] * $_POST[inp_child])* $currency_rate) +
													(($_POST[inp_price_infant] * $_POST[inp_infant])* $currency_rate) 
												   )),
			   );	
}





/*echo $status_book;*/



/* setting */
include("../include/country_right_menu.php");

$config[documentroot] = $path;
$smarty->assign("age",$age);
/* setting */

$smarty->assign('status_book',$status_book);
$smarty->assign('currency',$currency[0]);
$smarty->assign("case",$case);
$smarty->assign("print",$print);
$smarty->assign("error",$error);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("booking",$booking);
$smarty->assign("config",$config);
$smarty->assign("productdetail",$product[description]);
$smarty->display('booking1.tpl');
?>