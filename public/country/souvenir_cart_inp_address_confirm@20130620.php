<? 

$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = breadcamp(array('TOP','注文する'),array('index.php'));

require_once($path."include/config.php");
$config['allot'] = $_SESSION['checkout']['allot'];

/* produuct path setting   */
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

include("../include/check_souvenir.php");


	/* show product list */
	
$souvenir_list = $_SESSION[cart_souvenir_checkout][souvenir_list];
	
	
if (!$souvenir_list){
		
	header('Location: /'.$countryname.'/souvenir.php');
}
	
	$souvenir_list_id = array();
	$souvenir_list_qty = array();
	
	function zero_fill($data){	
		return $data<10?'0'.$data:$data ;	
	}	
	
	
	
	if (is_array($souvenir_list)){
		while ($obj = current($souvenir_list)){
				
				$souvenir_id_array[]  = key($souvenir_list);
				
				$cart_items[]  =  array( 'id' 	 => key($souvenir_list),  
										 'value' => $obj);
				
			
			next($souvenir_list);
		}
	}
	
	/* check souvenir product price */
	
	$souvenir_id_string = implode(',',$souvenir_id_array);
	
	$arr_input_order_detail = array();
	
	$sql = 'SELECT a.souvenir_id, 
					a.country_iso3, country_name_en,country_name_jp,
					rate,sign,code,souvenir_theme,
					souvenir_code,souvenir_name,
					store_type,
					souvenir_price,souvenir_allotment,
					short_desc, public, 
					status_book,affiliate_code,
					affiliate,affiliate_url,remark,
					b.text_top,b.text_table,b.text_bottom ,
					IF ( souvenir_allotment > 0 , \'1\',\'0\' ) AS sale_status  
					FROM mbus_souvenir a 
					LEFT JOIN mbus_souvenir_detail b ON a.souvenir_id = b.souvenir_id 
					LEFT JOIN mbus_country c ON a.country_iso3 = c.country_iso3 
					LEFT JOIN mbus_currency d ON a.country_iso3 = d.country_iso3 
					WHERE a.souvenir_id IN ('.$souvenir_id_string.') AND public = 1 AND affiliate = 0 AND 
					a.country_iso3 = \''.$site_country.'\' 
					ORDER BY souvenir_name';
		
		$result = $db->db_query($sql); $i = 0;
		
		$amount_summery = 0;
		$qty_summery    = 0;
		$country_iso3   = "";	
	
		while ($record = mysql_fetch_array($result)){
			
			$country_iso3       = $record[country_iso3];
			
			$souvenir_allotment = $record[souvenir_allotment];
			$souvenir_id		= $record[souvenir_id];
			$souvenir_name		= $record[souvenir_name];
			$souvenir_code		= $record[souvenir_code];
			$souvenir_allotment = $record[souvenir_allotment];
			$store_type         = $record[store_type];  // recive : 1 == recive on spot 2 = recive in japan
			$status_book		= $record[status_book]; // paid type : 1 = credit 2 = cash 
			$price				= $record[souvenir_price];
			$sign				= $record[sign];
			
			
			$i++;
			
			foreach ($cart_items as $items){ // forech1 
				if ($items[id] == $souvenir_id){ // id matching 
	
					$qty = $items[value];
					$price = $price;
				
					$amount = number_format($qty * $price , (intval($price)==$price)?0:2);
					$amount_noneformat = ($qty * $price);
					
					$amount_summery += ($qty * $price);
					$qty_summery    +=  $qty;
					
					
					$arr_input_order_detail[] = array( $i, $souvenir_id, $souvenir_code, $price, $qty, $amount_noneformat);	
					
				$cart[souvenir_product][] = array('image'  => '<img src="../product/images/souvenir/'.$souvenir_id.'-1.jpg" style="width:56px;" alt="" />',
												  'name'   => $souvenir_name,
												  'price'  => $sign.' '.number_format($price , (intval($price)==$price)?0:2 ),
												  'qty'    => $qty,
												  'amount' => $sign.' '.$amount);
		
				}
			}
		}
		
		$cart[price_sum] = $sign.' '.number_format($amount_summery , (intval($amount_summery)==$amount_summery)?0:2 );
		
	/* check souvenir product price */
	
	/* show product list */
	
	
					if ($_SESSION[cart_souvenir_customer][recive_address]){
						$inprec_data			= '1';		
						$inprec_surname 	  	= $_SESSION[cart_souvenir_customer][inprec_surname];
						$inprec_firstname		= $_SESSION[cart_souvenir_customer][inprec_firstname];
						$inprec_surname_furi	= $_SESSION[cart_souvenir_customer][inprec_surname_furi];
						$inprec_firstname_furi	= $_SESSION[cart_souvenir_customer][inprec_firstname_furi];
						
						$type_addr2				= $_SESSION[cart_souvenir_customer][type_addr2]?true:false;
						$inprec_addrovs1		= $_SESSION[cart_souvenir_customer][inprec_addrovs1];
						$inprec_addrovs2		= $_SESSION[cart_souvenir_customer][inprec_addrovs2];
						$inprec_addrjp_zip1		= $_SESSION[cart_souvenir_customer][inprec_addrjp_zip1];
						$inprec_addrjp_zip2		= $_SESSION[cart_souvenir_customer][inprec_addrjp_zip2];
						$inprec_addrjp_pref		= $db->view_pref(trim($_SESSION[cart_souvenir_customer][inprec_addrjp_pref]));
						$inprec_addrjp_city		= $_SESSION[cart_souvenir_customer][inprec_addrjp_city];
						$inprec_addrjp_area		= $_SESSION[cart_souvenir_customer][inprec_addrjp_area];
						$inprec_addrjp_building	= $_SESSION[cart_souvenir_customer][inprec_addrjp_building];
						$inprec_tel1			= $_SESSION[cart_souvenir_customer][inprec_tel1_1].'-'.$_SESSION[cart_souvenir_customer][inprec_tel1_2].'-'.$_SESSION[cart_souvenir_customer][inprec_tel1_3];
					} 
					else{
						$inprec_data			= '0';		
						$inprec_surname 	  	= $_SESSION[cart_souvenir_customer][inp_surname];
						$inprec_firstname		= $_SESSION[cart_souvenir_customer][inp_firstname];
						$inprec_surname_furi	= $_SESSION[cart_souvenir_customer][inp_surname_furi];
						$inprec_firstname_furi	= $_SESSION[cart_souvenir_customer][inp_firstname_furi];
						
						$type_addr2				= $_SESSION[cart_souvenir_customer][type_addr]?true:false;
						$inprec_addrovs1		= $_SESSION[cart_souvenir_customer][inp_addrovs1];
						$inprec_addrovs2		= $_SESSION[cart_souvenir_customer][inp_addrovs2];
						$inprec_addrjp_zip1		= $_SESSION[cart_souvenir_customer][inp_addrjp_zip1];
						$inprec_addrjp_zip2		= $_SESSION[cart_souvenir_customer][inp_addrjp_zip2];
						$inprec_addrjp_pref		= $db->view_pref(trim($_SESSION[cart_souvenir_customer][inp_addrjp_pref]));
						$inprec_addrjp_city		= $_SESSION[cart_souvenir_customer][inp_addrjp_city];
						$inprec_addrjp_area		= $_SESSION[cart_souvenir_customer][inp_addrjp_area];
						$inprec_addrjp_building	= $_SESSION[cart_souvenir_customer][inp_addrjp_building];
						$inprec_tel1			= $_SESSION[cart_souvenir_customer][inp_mobile1_1].'-'.$_SESSION[cart_souvenir_customer][inp_mobile1_2].'-'.$_SESSION[cart_souvenir_customer][inp_mobile1_3];
					}
	
	
	/* customser information */
	$order = array( 'inp_surname' 		  	 => $_SESSION[cart_souvenir_customer][inp_surname],
					'inp_firstname' 	  	 => $_SESSION[cart_souvenir_customer][inp_firstname],
					'inp_surname_furi' 	  	 => $_SESSION[cart_souvenir_customer][inp_surname_furi],
					'inp_firstname_furi'  	 => $_SESSION[cart_souvenir_customer][inp_firstname_furi],
					
					'birthday_year' 	  	 => $_SESSION[cart_souvenir_customer][birthday_year],
					'birthday_month' 	  	 => $_SESSION[cart_souvenir_customer][birthday_month],
					'birthday_day' 		  	 => $_SESSION[cart_souvenir_customer][birthday_day],
					
					'inp_sex' 			  	 => $_SESSION[cart_souvenir_customer][inp_sex],
					'inp_email' 		  	 => $_SESSION[cart_souvenir_customer][inp_email],
					
					'type_addr'   		     => $_SESSION[cart_souvenir_customer][type_addr]?true:false,
					
					'inp_addrovs1' 		     => $_SESSION[cart_souvenir_customer][inp_addrovs1],
					'inp_addrovs2' 		     => $_SESSION[cart_souvenir_customer][inp_addrovs2],
					
					'inp_addrjp_zip1'	     => $_SESSION[cart_souvenir_customer][inp_addrjp_zip1],
					'inp_addrjp_zip2'	     => $_SESSION[cart_souvenir_customer][inp_addrjp_zip2],
					'inp_addrjp_pref'	     => $db->view_pref(trim($_SESSION[cart_souvenir_customer][inp_addrjp_pref])),
					'inp_addrjp_city'	     => $_SESSION[cart_souvenir_customer][inp_addrjp_city],
					'inp_addrjp_area'	     => $_SESSION[cart_souvenir_customer][inp_addrjp_area],
					'inp_addrjp_building'    => $_SESSION[cart_souvenir_customer][inp_addrjp_building],
					
					'inp_tel1'			     => $_SESSION[cart_souvenir_customer][inp_tel1_1].'-'.
												$_SESSION[cart_souvenir_customer][inp_tel1_2].'-'.$_SESSION[cart_souvenir_customer][inp_tel1_3],
					
					'inp_mobile1'		     => $_SESSION[cart_souvenir_customer][inp_mobile1_1].'-'.
												$_SESSION[cart_souvenir_customer][inp_mobile1_2].'-'.$_SESSION[cart_souvenir_customer][inp_mobile1_3],
					
					
				     'location_rec'			 => $db->check_store_location($site_country), // select recive location
					
					/* outsite japan location*/
					
			
					'reg_hotel_name'		 => $_SESSION[cart_souvenir_customer]['reg_hotel_name'],
					'reg_hotel_tel'		     => $_SESSION[cart_souvenir_customer]['reg_hotel_tel'],
					'reg_hotel_addr'		 => $_SESSION[cart_souvenir_customer]['reg_hotel_addr'],
					
					'reg_arrfrm_year'		 => $_SESSION[cart_souvenir_customer]['reg_arrfrm_year'],
					'reg_arrfrm_month'		 => $_SESSION[cart_souvenir_customer]['reg_arrfrm_month'],
					'reg_arrfrm_day'		 => $_SESSION[cart_souvenir_customer]['reg_arrfrm_day'],
					'reg_arrfrm_date'		 => $_SESSION[cart_souvenir_customer]['reg_arrfrm_date'],
					'reg_arrfrm_date_jp'	 => jp_strdate($_SESSION[cart_souvenir_customer]['reg_arrfrm_date']),
					
					'reg_arrto_year'		 => $_SESSION[cart_souvenir_customer]['reg_arrto_year'],
					'reg_arrto_month'		 => $_SESSION[cart_souvenir_customer]['reg_arrto_month'],
					'reg_arrto_day'		     => $_SESSION[cart_souvenir_customer]['reg_arrto_day'],
					'reg_arrto_date'		 => $_SESSION[cart_souvenir_customer]['reg_arrto_date'],
					'reg_arrto_date_jp'	 => jp_strdate($_SESSION[cart_souvenir_customer]['reg_arrto_date']),
					
					'reg_arrgo_year'		 => $_SESSION[cart_souvenir_customer]['reg_arrgo_year'],
					'reg_arrgo_month'		 => $_SESSION[cart_souvenir_customer]['reg_arrgo_month'],
					'reg_arrgo_day'		     => $_SESSION[cart_souvenir_customer]['reg_arrgo_day'],
					'reg_arrgo_date'		 => $_SESSION[cart_souvenir_customer]['reg_arrgo_date'],
					'reg_arrgo_date_jp'	 => jp_strdate($_SESSION[cart_souvenir_customer]['reg_arrgo_date']),
					
					'reg_arrgo_hh'		     => $_SESSION[cart_souvenir_customer]['reg_arrgo_hh'],
					'reg_arrgo_mm'		     => $_SESSION[cart_souvenir_customer]['reg_arrgo_mm'],
					'reg_arrgo_time'		 => $_SESSION[cart_souvenir_customer]['reg_arrgo_time'],
					
					'air_no'		     	=> $_SESSION[cart_souvenir_customer]['air_no'],
					'sel_info'		     	=> $_SESSION[cart_souvenir_customer]['sel_info'],
					/* outsite japan location*/
					
					/* insite japan location */
					
					'inprec_surname'	     => $inprec_surname,
					'inprec_firstname'	     => $inprec_firstname,
					'inprec_surname_furi'    => $inprec_surname_furi,
					'inprec_firstname_furi'  => $inprec_firstname_furi,
					
					'type_addr2'			 => $type_addr2,
					'order_rec'				 => $inprec_data,
					
					'inprec_addrovs1'		 => $inprec_addrovs1,
					'inprec_addrovs2'		 => $inprec_addrovs2,
					'inprec_addrjp_zip1'	 => $inprec_addrjp_zip1,
					'inprec_addrjp_zip2'	 => $inprec_addrjp_zip2,
					
					'inprec_addrjp_pref'	 => $inprec_addrjp_pref,
					'inprec_addrjp_city'	 => $inprec_addrjp_city,
					'inprec_addrjp_area'	 => $inprec_addrjp_area,
					'inprec_addrjp_building' => $inprec_addrjp_building,
					
					'inprec_tel1'			 =>	$inprec_tel1,
							
					/* insite japan location */		
							
					'inp_remark'			 =>	nl2br($_SESSION[cart_souvenir_customer][inp_remark]));
	
	
	/* customser information */
	
	
		if ( !empty($_SESSION[cart_souvenir_customer][birthday_year]) && 
			 !empty($_SESSION[cart_souvenir_customer][birthday_month]) &&
			 !empty($_SESSION[cart_souvenir_customer][birthday_day]))
		{
			
			
			
			$customer_birthday = zero_fill($_SESSION[cart_souvenir_customer][birthday_year]).'-'.
								 zero_fill($_SESSION[cart_souvenir_customer][birthday_month]).'-'.
								 zero_fill($_SESSION[cart_souvenir_customer][birthday_day]);
			
		} 
	
	
	
	
	$smarty = new Smarty;
	/* setting */
	include("../include/souvenir_country_right_menu.php");
	$config[documentroot] = '../';
	$smarty->assign("config",$config);
	/* setting */
	
	
	if ($_SESSION[checkout][allot] == 'false')
	{
		$status_book = 4;	
	}
	
	$config[documentroot] = $path;
	
	
	 $order_id = $_SESSION[order_id];
	
	
	if ($order_id){
		
		unset($_SESSION[order_id]);
		
		
		$payment = $db->paymentcode($country_iso3);
		
		
		
		
		$smarty->assign('payment_code', /*'A019000014'*/$payment[sitecode]);
		$smarty->assign('amount',$amount_summery);
		$smarty->assign('currency_product',$payment[code]);
		$smarty->assign('email'   ,$_SESSION[cart_souvenir_customer][inp_email]);
		$smarty->assign('ordercode',$order_id);
		$smarty->assign('optional',$_SESSION[cart_souvenir_customer][inp_firstname_furi].'-'.$order_id);
		
	
		
	
		
		$smarty->assign('transection',md5($order_id));
		
	
	}
	else{
		
		
		
		header( "HTTP/1.1 301 Moved Permanently" ); 
		header('Location: souvenir_cart_inp_address.php');		
	}
	
	
	
	
	
	
	
	$smarty->assign('order',$order);

	$smarty->assign('cart',$cart);
	$smarty->assign("cart_type",$_SESSION[cart_souvenir_checkout][cart_type]);
	
	
	
	if ( $_SESSION[cart_souvenir_checkout][cart_type] == '0' ||  $_SESSION[cart_souvenir_checkout][cart_type] == '2'){
		$smarty->assign('credit','1');
	}
	else{
		$smarty->assign('credit','0');	
	}
	
	
	$smarty->assign("breadcamp",$breadcamp);
	$smarty->assign("booking",$booking);
	$smarty->assign("config",$config);
	$smarty->display('souvenir_booking4_cart_inp_address_conf.tpl');
?>

