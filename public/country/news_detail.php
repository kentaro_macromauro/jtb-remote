<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path."include/config.php");

$rate = $db->get_currency_rate($site_country);

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */


$news = $db->view_news($_GET[id]);

if ( ($news['public'] == '0')||(empty($news[news_subject]))||(empty($news['public'])) )
{
	header("Location: news.php");
}

$site_name_base = $site_country=='IDN'?'バリ島':$site_name;


$subject =  jd_decode($news[news_subject]);
$content =  '<div class="header-text-tittle-news">'.$subject.'</div><div class="text-content pleft5">'.jd_decode($news[news_countent])."</div>";

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'現地情報',$site_name.'最新情報',str_len($subject,50)),array('../','./','./info_top.php','./news.php',''));

/* For Menu Style */
include("../include/country_top_navi.php");

require_once($path."include/config.php");

$smarty = new Smarty;
include("../include/country_right_menu.php");
include("../include/check_souvenir_cart.php");

$config[documentroot] = $path;
$smarty->assign("data_tempage", $content);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
$smarty->display('country_page.tpl');
?>
