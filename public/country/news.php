<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$news = $db->view_news($_GET[id]);
$subject =  jd_decode($news[news_subject]);
$content =  jd_decode($news[news_countent]);

$rate = $db->get_currency_rate($site_country);

$site_name_base = $site_country=='IDN'?'バリ島':$site_name;

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'現地情報',$site_name.'最新情報'),array('../','./','./info_top.php',''));

/* For Menu Style */
include("../include/country_top_navi.php");

require_once($path."include/config.php");

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */


$page_num = 25;
$max_record = $db->count_news( $site_country ,"yes");


$page_now   = pagenavi_start($max_record,$page_num,$_GET[page]);
$pagination = pagenavi( $page_now, $max_record ,'?page=',$page_num); 
$page_first = pagenavi_first($page_now,$page_num);

$country_news = $db->fetch_news($page_first,$page_num,$site_country,"yes");
$news = array();


for ($i = 0; $i < count($country_news[id]) ;$i++)
{
	$news[ $country_news['id'][$i] ] 	= array( 'id' => $country_news['id'][$i],
												 'date' =>  $country_news['date'][$i],
												 'subject' => $country_news['subject'][$i],
												 'icon' => $country_news['icon'][$i]);
}

$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;
include("../include/check_souvenir_cart.php");

$smarty->assign("news", $news);
$smarty->assign("pagination",$pagination);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
$smarty->display('country_news.tpl');
?>
