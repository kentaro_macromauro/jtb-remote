<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path."include/config.php");
$breadcamp = '<p><a href="../" style="color: #000; font-size: 12px;">TOP</a>&nbsp;&nbsp;>&nbsp;&nbsp;<a href="index.php" style="color: #000; font-size: 12px;">'.$site_name.'</a>';

/* For Menu Style */
include("../include/country_top_navi.php");

if (!empty($_GET['id']))
{
    $id = $_GET['id'];
    $filepath = ($path."www_config/html/".$countryname."/special_contents/".$id);
    $c = file_get_contents($filepath);

    if (empty($c))
    {
        header('Location: index.php');
    }

    //Get title
    preg_match("/<title>(.*)<\/title>/is", $c, $retArr);

    $title = $retArr[1];

    $c = mb_ereg_replace('^.*<!--body content-->(.*)<!--end of body content-->.*$','\1',$c);

    $breadcamp = '<p><a href="../" style="color: #000; font-size: 12px;">TOP</a>
                    &nbsp;&nbsp;>&nbsp;&nbsp;<a href="index.php" style="color: #000; font-size: 12px;">'.$site_name.'</a>
                    &nbsp;&nbsp;>&nbsp;&nbsp;<a href="info_top.php" style="color: #000; font-size: 12px;">'.$site_name.'現地情報</a>
                    &nbsp;&nbsp;>&nbsp;&nbsp;<span style="color: #000; font-size: 12px;">特集記事</span>&nbsp;&nbsp;>&nbsp;&nbsp;'.$title;
}
else
{

    if (empty($c))
    {
        header('Location: index.php');
    }
    $c = 'not found information.';
}

$smarty = new Smarty;

$config[documentroot] = $path;
include("../include/check_souvenir_cart.php");

$smarty->assign("data_tempage",$c);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("config",$config);
$smarty->display('country_special_contents.tpl');
?>