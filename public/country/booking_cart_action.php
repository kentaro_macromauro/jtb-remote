<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';

require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

/* db connect */
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);
/* db connect */

if (count( $_SESSION[basket] ) == 0)
{
	header("Location: index.php");
}



for ($i=0 ; $i < count($_SESSION[basket]) ;$i++)
{
	if ( $_SESSION[basket][$i][product_id] ==  $_POST['product_id'])
	{
		$booking_date = $_SESSION[basket][$i]['date'];
		$qty_adult    = $_SESSION[basket][$i]['qty_adult'];
		$qty_child    = $_SESSION[basket][$i]['qty_child'];
		$qty_infant   = $_SESSION[basket][$i]['qty_infant'];
		$cfm_code	  = $_SESSION[basket][$i]['cfm_code'];
		$price_adult  = $_SESSION[basket][$i]['price_adult'];
		$price_child  = $_SESSION[basket][$i]['price_child'];
		$price_infant = $_SESSION[basket][$i]['price_infant'];
		$allot 		  = $_SESSION[basket][$i]['allot'];
		
		$charge_type  = $_SESSION[basket][$i]['charge_type'];
		$charge_desc  = $_SESSION[basket][$i]['charge_desc'];
	}
}


 
/* post information */
$product_id   = $_POST['product_id'];


$session_id   = $_SESSION[add_cart][session_id];
/* post information */




/* set cart checkout sessin */
$_SESSION['checkout'] = array( 'session_id'  =>   $session_id,							  
							   'product_id'   =>  $product_id,
							   'cfm_code'     =>  $cfm_code,
							   
							   'booking_date' =>  $booking_date,
							   
							   'price_adult'  => $price_adult,
							   'price_child'  => $price_child,
							   'price_infant' => $price_infant,
							   
 							   'qty_adult'    =>  $qty_adult,
							   'qty_child'    =>  $qty_child,
							   'qty_infant'   =>  $qty_infant,
							   'allot'		  =>  $allot ,
							   'charge_type'  =>  $charge_type,
							   'charge_desc'  => $charge_desc,
							   ); 
/* set cart checkout session */




/* product select history */
$product_id = $_SESSION[checkout][product_id];

if (!in_array( $product_id ,$cart ))
{
	array_push($cart,$product_id);
}
else
{
	$_SESSION["cart"] = $cart;
}
/* product select history */

/* send out */
header("Location: booking_infoinput.php"); 
/* send out */
?>