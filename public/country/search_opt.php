<?

//URL replacement of new city code
switch($_GET['inp_city']){
    case 'KUA':
        $_GET['inp_city'] = 'KUL';
        break;
    case 'LAN':
        $_GET['inp_city'] = 'LGK';
        break;
    case 'KOT':
        $_GET['inp_city'] = 'BKI';
        break;
    case 'BAR':
        $_GET['inp_city'] = 'DPS';
        break;
    case 'HCM':
        $_GET['inp_city'] = 'SGN';
        break;
    case 'AUK':
        $_GET['inp_city'] = 'AKL';
        break;
    case 'AYR':
        $_GET['inp_city'] = 'AKL';
        break;
    case 'CHR':
        $_GET['inp_city'] = 'CHC';
        break;
    case 'QUE':
        $_GET['inp_city'] = 'ZQN';
        break;
    case 'KSM':
        $_GET['inp_city'] = 'USM';
        break;
    case 'KRB':
        $_GET['inp_city'] = 'KVR';
        break;
    case 'RTU':
        $_GET['inp_city'] = 'ROT';
        break;
    case 'MTC':
        $_GET['inp_city'] = 'MON';
        break;
    default:
        break;
}

/* redirect 20140412 */
if($site_country == 'AUS'){
    $aus_base_url = 'http://www.jtb.com.au/tour/search.php?';
    $city_query = '';
    if($_GET['inp_city'] != '0'){
        switch($_GET['inp_city']){
            case 'SYN':
                $city_query = '&city=3';
                break;
            case 'GOC':
                $city_query = '&city=2';
                break;
            case 'CAN':
                $city_query = '&city=1';
                break;
            case 'MEL':
                $city_query = '&city=4';
                break;
            case 'PER':
                $city_query = '&city=5';
                break;
            case 'AYR':
                $city_query = '&city=6';
                break;
            case 'AYR':
                $city_query = '&city=7&city=8';
                break;
            default:
                $city_query = '&city=3';
                break;
        }
    }
    $keyword_query = '';
    if($_GET['inp_keyword'] != '0'){
        $keyword_query = '&kw='.$_GET['inp_keyword'];
    }
    $time_query = '';
    $category_query = '';
    $option_query = '';
    if($_GET['inp_time'] != 0){
        $time_query = '&c_'.$_GET['inp_time'].'=1';
    }
    if($_GET['inp_category'] != 0){
        $category_query = '&c_'.$_GET['inp_category'].'=1';
    }
    if($_GET['inp_option'] != 0){
        $option_query = '&c_'.$_GET['inp_option'].'=1';
    }
    $auth_search_url = $aus_base_url.$city_query.$time_query.$category_query.$option_query.$keyword_query;

    header('Location: '.$auth_search_url);
}

/* redirect 20140412 */
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'include/webservice.php');

/* setting information */
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);
require_once($path."include/config.php");

$page_type    = $_GET[inp_type];

/* setting information */
$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'オプショナルツアー','検索一覧'),array('../','./','./opt.php',''));

/* For Menu Style */
include("../include/country_top_navi.php");

/* For select part on header */
$record = $db->get_country();
$sql = 'SELECT city_id,city_name_jp,city_iso3 FROM '._DB_PREFIX_TABLE.'city WHERE country_iso3 = UPPER("'.$_GET[inp_country].'") ORDER BY city_idx,city_id ';

$result = $db->db_query($sql); $i =0 ;
while ( $record1 = mysql_fetch_array($result))
{
	$arr_city[0][$i] = $record1[city_iso3];
	$arr_city[1][$i] = $record1[city_name_jp];
	$i++;
}

$inp_country = input_selectbox('inp_country',$record[1],$record[0],$_GET[inp_country],'-Please Select-','form-control') ;
$select_city = input_selectbox('inp_city',$arr_city[1],$arr_city[0],$_GET[inp_city],'-Please Select-','form-control') ;

$inp_search_keyword = input_textbox('inp_keyword',$_GET[inp_keyword],'form-control');

$record = $db->view_theme_all();
$select_category = input_option($record['data'], $record['value'], $_GET[inp_category]);

if (is_array($_GET[inp_category]))
{
	for ($i = 0; $i < count($_GET[inp_category]) ; $i++)
	{
		$select_category .= input_hiddenbox('inp_category[]',$_GET[inp_category][$i]) ;	
	}
}
$arr_option = $db->view_option_all();
$select_option = input_option($arr_option['data'], $arr_option['value'], $_GET[inp_option]);


$record = $db->viw_time_all();
$select_time = input_option($record['data'], $record['value'], $_GET[inp_time]);
/* For select part on header */

$entry_year = Date('Y');
$entry_month = Date('n');

$arr_yearmonth_data = array();
$arr_yearmonth_value = array();
$arr_day_data   = array();
$arr_day_value  = array();

function zero_fill($data)
{
	if (($data) < 10)
	{
		$data = '0'.$data;
	}

	
	return $data;
	
}

for ($i=0; $i<12;$i++)
{
	
	if ( ($entry_month + $i) > 12)
	{
		$mm = ($entry_month + $i) - 12;
		
		$arr_yearmonth_value[$i] = ($entry_year+1).'-'.($mm);
		$arr_yearmonth_data[$i]  = ($entry_year+1).'年 '.zero_fill($mm).'月';
	}
	else
	{
		$arr_yearmonth_value[$i] = $entry_year.'-'.($entry_month + $i);
		$arr_yearmonth_data[$i]  = $entry_year.'年 '.zero_fill($entry_month + $i).'月';
	}
}

$select_day		  = input_selectbox('inp_day',array(),array(),$_GET[inp_day],'日','search-left-selectbox-dd');

if ( (!empty( $_GET[inp_yearmonth] ) ) || ( $_GET[inp_yearmonth] != '0' ) )
{

	$arr_lipday = explode('-', $_GET[inp_yearmonth]);
	
	$max_day = days_in_month($arr_lipday[1] , $arr_lipday[0]);
	
	for ($i=0; $i < $max_day; $i++)
	{
		$arr_day_data[$i] = zero_fill($i+1).'日';
		$arr_day_value[$i] = ($i+1);
	}
	
	$select_day   = input_selectbox('inp_day',$arr_day_data,$arr_day_value,$_GET[inp_day],'日','search-left-selectbox-dd');

}

$select_yearmonth = input_selectbox('inp_yearmonth',$arr_yearmonth_data,$arr_yearmonth_value,$_GET[inp_yearmonth],'年月','search-left-selectbox-mmyy');


$arr_review_data  = array('あり','なし');
$arr_review_value = array('1','2');

$select_review    = input_selectbox('inp_review',$arr_review_data,$arr_review_value,$_GET[inp_review],'----All----','search-left-selectbox2');
/*search box*/
	
/* pagination */
			
$inp_select_country  = $_GET[inp_country];
$inp_select_city	 = $_GET[inp_city];
$inp_select_category = sprintf("%03d",($_GET[inp_category]));
$inp_keyword		 = $_GET[inp_keyword];


if ($_GET[inp_country] != $site_country)
{
	$url = $_SERVER[REQUEST_URI];

	if (!empty($_GET[inp_country]))
	{
		$refpath  = $db->showpath_bycountry( $_GET[inp_country]);
		

		$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
		
		header('Location: '.$url);
	}
	else
	{
		$refpath  = $db->showpath_bycountry( $_GET[inp_country]);
		$url = str_replace( '/'.$countryname.'/'  ,'/', $url);
		
		header('Location: '.$url);
	}
	
	
	
}



if ( empty($inp_select_category) || (trim($inp_select_category) == "0") )
{
	$inp_select_category = "";
}

if ( empty($inp_select_city) || (trim($inp_select_city) == "0"))
{
	$inp_select_city = "";
}

if ( empty($inp_select_country ) || (trim($inp_select_country) == "0")) 
{
	$inp_select_country = "" ;
}


$max_record = $db->page_search_opt_count($inp_select_country,$inp_select_city,$inp_keyword, $inp_select_category,$_GET['inp_option'], $_GET['inp_review'], $_GET['inp_time']);

$page_now = 0;
$page_num = 15;  


$link1 = 'search_opt.php?inp_country='.$_GET[inp_country].'&inp_city='.$_GET[inp_city].'&inp_keyword='.$_GET[inp_keyword].'&inp_category='.$_GET[inp_category].'&inp_option='.$_GET[inp_option].'&inp_time='.$_GET[inp_time].'&inp_yearmonth='.$_GET[inp_yearmonth].'&inp_day='.$_GET[inp_day].'&review='.$_GET[inp_review].'&order='.$_GET[order];


if (is_array($_GET[inp_category]) )
{ 
	
$link1 = 'search_opt.php?inp_country='.$_GET[inp_country].'&inp_city='.$_GET[inp_city].'&inp_keyword='.$_GET[inp_keyword].'&inp_option='.$_GET[inp_option].'&inp_time='.$_GET[inp_time].'&inp_yearmonth='.$_GET[inp_yearmonth].'&inp_day='.$_GET[inp_day].'&review='.$_GET[inp_review].'&order='.$_GET[order];	

	for ($i = 0; $i < count( $_GET[inp_category] ) ; $i++)
	{
		$link1 .= '&inp_category[]='.$_GET[inp_category][$i].'&';
	}

}

$page_now   = pagenavi_start($max_record,$page_num,$_GET[page]);
$pagination = res_pagenavi( $page_now, $max_record ,$link1.'&page=',$page_num);

$page_first = pagenavi_first($page_now,$page_num);


$page_start  = $page_now;
if ($page_start > 1)
{
	$page_start = (($page_start-1)*$page_num) ;	
}


if (($page_now*$page_num) > $max_record)
{
	$page_end = $max_record ;
	
}
else
{
	$page_end = ($page_now*$page_num);
	
}

/* pagination */


$data =	$db->page_search_opt($inp_select_country,
							 $inp_select_city,
							 $inp_keyword,
                             $inp_select_category,
							 $_GET['inp_option'],
							 $_GET['inp_review'],
							 $_GET['inp_time'],
							 $_GET[order],
							 $page_first,
							 $page_num);


/* matching category */

$sql  = 'SELECT theme_id,theme_name FROM ';
$sql .=  '(SELECT IF(theme_id<100,IF (theme_id<10,CONCAT(\'00\',theme_id),CONCAT(\'0\',theme_id)),theme_id) theme_id,theme_name FROM '._DB_PREFIX_TABLE.'theme ) AS a ORDER BY theme_id ';
	
$result = $db->db_query($sql);

$i= 0;

$arr_category = array();
while ($record = mysql_fetch_array($result))
{
	$arr_category['id'][$i]  = $record['theme_id'];
	$arr_category['name'][$i] = $record['theme_name'];
	$i++;
}

$buffer_data = '';	
for ($i=0; $i < count( $data ) ; $i++)
{
	$list_category = explode(';',$data[$i]['product_theme']) ;
	$category_ofproduct = array();
	
	for ($k=0; $k < count($list_category);$k++)
	{
		for ($j=0;$j< count($arr_category['id']);$j++)
		{
			if ($list_category[$k] == $arr_category['id'][$j])
			{
				$category_ofproduct[$j] =  $arr_category['name'][$j];
			}
		}
	}
	$location = $data[$i]['path'];
	
	$rate[0][show] = $db->show_rate($data[$i][country_iso3]);
/*

	$buffer_data .= them_search($data,$i,$category_ofproduct,$path,$location,$rate[0] ,"OPT");
*/
    $buffer_data .= opt_search($data,$i,$category_ofproduct,$path,$location, array('rate' => $data[$i][rate] ,
        'sign' => $data[$i][sign] ,
        'show' => $show_rate ) ,"OPT");

}
/* matching category */


/* produuct path setting   */

$img  = $path.'product/images/product/';
$link = $path.'product.php?product_id=';

/* produuct path setting   */


switch ($_GET[order])
{
	case '1' : $inp_order = '<select name="order" class="form-control ts-fc">
								<option value="1" selected="selected">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3">人気順</option>
							</select>'	; break;
							
	case '2' : $inp_order = '<select name="order" class="form-control ts-fc">
								<option value="1">価格が高い順</option>
								<option value="2" selected="selected">価格が安い順</option>
								<option value="3">人気順</option>
							</select>'	; break;
							
	case '3' : $inp_order = '<select name="order" class="form-control ts-fc">
								<option value="1">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3" selected="selected">人気順</option>
							</select>'	; break;
							
	default : $inp_order = '<select name="order" class="form-control ts-fc">
								<option value="1">価格が高い順</option>
								<option value="2">価格が安い順</option>
								<option value="3" selected="selected">人気順</option>
							</select>'	; break;
}

//sales_ranking
$sales_ranking = fetch_sales_ranking($site_country, $path,3);

//recommend_tour
$recommend_tour = fetch_recommend_tour($site_country, $path,3);

$smarty = new Smarty;

/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_city",$select_city);

$smarty->assign("select_category",$select_category);
$smarty->assign("select_time",$select_time);
/* search box */


/* banner */
$config[documentroot] = DEV_PATH.'../';

$smarty->assign("config",$config);
include("../include/check_souvenir_cart.php");

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("select_yearmonth",$select_yearmonth);
$smarty->assign("select_day",$select_day);
$smarty->assign("select_option",$select_option);
$smarty->assign("select_review",$select_review);
$smarty->assign("menu_selected_style",$menu_selected_style);

$smarty->assign("pagination",$pagination);

$smarty->assign("sales_ranking",$sales_ranking);
$smarty->assign("recommend_tour",$recommend_tour);

$smarty->assign("max_record",$max_record);
$smarty->assign("page_start",$page_start);
$smarty->assign("page_end",$page_end);

$smarty->assign("order_select",$inp_order);
$smarty->assign("link1",$link_no_order);
$smarty->assign("inp_keyword",$inp_search_keyword);

/* banner */
$smarty->assign("search_content",$buffer_data);


/* select tempage */
$smarty->display('country_search_page_opt.tpl');
/* select tempage */

/* set tempage */

?>
