<?
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = breadcamp(array('TOP',$site_name.'TOP','予約する'),array('../index.php','index.php'));
						

require_once($path."include/config.php");
$config['allot'] = $_SESSION['checkout']['allot'];


/* produuct path setting   */
$img  = '../product/images/product/';
//$link = '../thailand/product.php?product_id=';
/* produuct path setting   */

if (empty( $_SESSION[checkout][product_id] )) 
{
	header("Location: index.php");	
}


/* reservation  */	
	$product_id = $_SESSION[checkout][product_id];
	
	$result  = $db->view_product($product_id);
	
	$status_book = $result['status_book'];
	
	
	
	
	if ($result['public'] != '1')
	{
		header( "HTTP/1.1 301 Moved Permanently" ); 
		header('Location: index.php');	
	}
	
	
	if (empty( $product_id ))
	{
		
		header("Location: index.php");	
	}
	else
	{
	
		if ($result[country_iso3] != $site_country)
		{
			$url = $_SERVER[REQUEST_URI];
			
				$refpath  = $db->showpath_bycountry($result[country_iso3]);
				
		
				$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
				
				
				
				header('Location: '.$url);
		}

	}
	
	
	

	$entry_year  = Date('Y');
	/* select hotel */
	$arr_hotel = $db->select_hotel($site_country); 
	/* select hotel */
	
	/* currency */
	
	$currency_rate = $result[rate];	// sgd dollar to yen  

	
	$amount_sgd =  (  $_SESSION[checkout]['price_adult']  * $_SESSION[checkout][qty_adult]) +  
			       (  $_SESSION[checkout]['price_child']  * $_SESSION[checkout][qty_child]) +
			   	   (  $_SESSION[checkout]['price_infant'] * $_SESSION[checkout][qty_infant]) ;
				   
	
	
	$amount_yen = ($amount_sgd * $currency_rate);
	/* currency */
	

	if (trim( $_SESSION[register][reg][firstname] ) != '')
	{
		$his_view = 'on';
	}
	
	if ( is_array( $_SESSION[error] ))
	{
		$his_view = 'on';
	}


	$booking  = array('date' => jp_strdate($_SESSION[checkout][booking_date]), 
					  'amount_sgd' => number_format($amount_sgd) ,
					  'amount_yen'  =>  number_format($amount_sgd) ,
					  
					  'product'=>array( 'id'   =>  $product_id,
										'name' => $result[product_name_jp],
										'img'  => "../product/images/product/".$product_id."-1",
										'description'  => $result[description],
									    'qty_adult'    => $_SESSION[checkout][qty_adult],
										'qty_child'    => $_SESSION[checkout][qty_child],
										'qty_infant'   => $_SESSION[checkout][qty_infant],
										),
					  'his' => array('view'  => $his_view,
									 'reg'   => $_SESSION[register][reg],
									 'guest' => $_SESSION[register][guest]
									 )  
					  );

	/* setup */
/* reservation */

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

$smarty = new Smarty;
include("../include/country_right_menu.php");
$config[documentroot] = $path;

/*------- Component Setting -------*/

// booking register age between 18 - 100 year.
for ($i = 0;$i<100;$i++)
{
	$arrinp_year[$i] = ($entry_year-18) -$i;	
}	
// booking register age between 18 - 100 year.


/* return qty get from booking_confirm*/
$qty_adult  = $_SESSION['checkout'][qty_adult];
$qty_child  = $_SESSION['checkout'][qty_child];
$qty_infant = $_SESSION['checkout'][qty_infant];

/* return qty get from booking_confirm*/

// guest age between 0 to 100 year.
for ($i = 0;$i<100;$i++)
{
	$arr_guest_birth_year[$i] = $entry_year-$i;
}	

for ($i = 0;$i< 100; $i++)
{
	$arr_age[$i] = $i;	
}
// guest age between 0 to 100 year.

// passport age 0 to 10 year 
for ($i =0;$i<10;$i++)
{
	$arr_guest_sn_year[$i]	  = $entry_year+$i;
}	
// passport age 0 to 10 year 

	
$arr_city = $db->view_japan_city();

// generate input guest record max follow from number of guest.
$qty_guest = ($qty_adult+$qty_child+$qty_infant);
	
for ($i=0;$i< $qty_guest; $i++)
{
		$arr_guest[$i] = $i;	
}
// generate input guest record max follow from number of guest.

/*------- Component Setting -------*/


/* create input form */
$form = array('birthyear' => $arrinp_year,
			  'city' => $arr_city,
			  'age' =>  $arr_age,
			  'bookyear' =>array(Date('Y'),Date('Y')+1),
			  'hotel' => $arr_hotel,			  
			  'arr_guest' => $arr_guest,
			  'qty_adult' => $qty_adult,
			  'qty_child' => $qty_child,
			  'qty_infant' => $qty_infant,
			  'guest_sn_year' => $arr_guest_sn_year,
			  'guest_birth_year' => $arr_guest_birth_year
			  );
/* create input form */

if (!empty($check_err))
{
 var_dump( $_POST[error]);	
	
}

$booking[his][reg][hotel_select] = 1;



$smarty->assign("error",$_SESSION[error]);




if ($_SESSION[checkout][allot] == 'false')
{
	$status_book = 4;	
}

if ( ($_SESSION[checkout]['price_adult'] == 1) || ( $_SESSION[checkout]['price_child'] == 1 ) )
{
	$status_book = 4;	
}



$smarty->assign("form",$form);

$smarty->assign('status_book',$status_book);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("booking",$booking);
$smarty->assign("config",$config);
$smarty->display('booking3.tpl');
unset($_SESSION["error"]);
?>
