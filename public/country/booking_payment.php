<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'オプショナルツアー','予約する'),array('../','./','./opt.php'));

/* For Menu Style */
include("../include/country_top_navi.php");

require_once($path."include/config.php");
/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../thailand/product.php?product_id=';
/* produuct path setting   */

include("../include/check_booking.php");

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;


$result = $db->view_product($_SESSION[checkout][product_id]);

$status_book = $result['status_book'];



if ($result['public'] != '1')
{
	header( "HTTP/1.1 301 Moved Permanently" ); 
	header('Location: index.php');	
}


if ($result[country_iso3] != $site_country)
{
	$url = $_SERVER[REQUEST_URI];
		
	$refpath  = $db->showpath_bycountry($result[country_iso3]);
			
	$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
			
	header('Location: '.$url);
}

$amount = (($_SESSION[checkout][price_adult] * $_SESSION[checkout][qty_adult]) + 
		   ($_SESSION[checkout][price_child]  * $_SESSION[checkout][qty_child]) +
		   ($_SESSION[checkout][price_infant] * $_SESSION[checkout][qty_infant]));


if ($_SESSION[checkout][charge_type] == 'Per Service'){
	$amount = $_SESSION[checkout][price_adult];	
}



$payment = $db->paymentcode($result[country_iso3]);

if ($_SESSION['checkout'][allot]  == 'false' )
{
	$transection = $_REQUEST[transection];

	header("Location: booking_payment_action.php?paid_type=3&transection=".$transection); 
}

if ($_SESSION[checkout][allot] == 'false')
{
	$status_book = 4;	
}

/*
if ( ($_SESSION[checkout]['price_adult'] == 1) || ( $_SESSION[checkout]['price_child'] == 1 ) )
{
	$status_book = 4;	
}
*/

$customer_en = $_SESSION[register][reg][furi_firstname].' '.$_SESSION[register][reg][furi_lastname];

$book_id = $_REQUEST[transection];

$product_code = $db->get_product_code( $_SESSION[checkout][product_id] );

//session_regenerate_id();
//$transection_id     = session_id();

$smarty->assign("book_id",$book_id);

$smarty->assign("optional",$customer_en.'-'.$product_code);
$smarty->assign("email",$_SESSION[register][reg][email]);
$smarty->assign("status_book",$status_book);
$smarty->assign("msg",$_GET[msg]);
$smarty->assign("currency_product",$payment[code]);
$smarty->assign("payment_code",$payment[sitecode]);
$smarty->assign("tel" , $payment[tel]);
$smarty->assign("product_status",$result['status_book']);
$smarty->assign("amount",$amount);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->assign("booking",$booking);
$smarty->assign("config",$config);

/* transection id */
$sql = 'SELECT book_id FROM mbus_booking WHERE md5(book_id) = "'.trim($book_id).'" LIMIT 0,1';
$result = $db->db_query($sql);

while ($record = mysql_fetch_array($result)){
	$transection_id = $record[book_id];	
}

if (empty($transection_id)){
	header('Location: index.php'); 
}
/* transection id */

$smarty->assign("ordercode",$transection_id);
$smarty->display('booking5.tpl');

?>