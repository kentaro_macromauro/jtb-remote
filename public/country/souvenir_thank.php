<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'class/c_service.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

require_once($path."include/config.php");
$config['allot'] = 'true';

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'おみやげ','注文する'),array('../','./','./souvenir.php',''));

/* setting */
$credit = $_REQUEST['credit']?'1':'0';

$g_order_id = $_SESSION['g_order_id'];

$sql = 'SELECT a.* ,b.* FROM '._DB_PREFIX_TABLE.'souvenir_order a
			LEFT JOIN '._DB_PREFIX_TABLE.'souvenir_order_detail b ON a.order_id = b.order_id
			WHERE a.order_id = "'.trim($g_order_id).'"';

$result =$db->db_query($sql);
while ($record = mysql_fetch_array($result))
{
    $product = $db->view_souvenir_product($record['souvenir_id']);

    $order_id = $record['order_id'];
    $total_price = $record['product_amount'];

    $converted_total_price = fetch_local_amount($rate[0]['code'], $total_price, 'JPY');

    $converted_price = fetch_local_amount($rate[0]['code'], $product['souvenir_price'], 'JPY');

    $product_data[] = array(
        'sku' => $product['souvenir_code'],
        'name' => $product['souvenir_name'],
        'category' => 'SOUVENIR',
        'price' => $converted_price,
        'quantity' => $record['souvenir_qty']
    );
}

//$order_info
$order_info[]  = array(
    'transactionId' => $order_id,
    'transactionAffiliation' => '', //販売代理店
    'transactionTotal' => $converted_total_price, // price
    'transactionTax' => '', //tax
    'transactionShipping' => '', //shipping cost
    'transactionProducts' => $product_data
);
unset($_SESSION['g_order_id']);

include("../include/souvenir_country_right_menu.php");
$config[documentroot] = DEV_PATH.'../';
$smarty->assign("config",$config);
/* setting */

$smarty->assign("credit",$credit);

$smarty->assign("order_info" ,json_encode($order_info));
$smarty->assign("currency",$currency[0]);
$smarty->assign("breadcamp"	,$breadcamp);
$smarty->assign("config"	,$config);

$smarty->display('souvenir_booking5_thank.tpl');
?>