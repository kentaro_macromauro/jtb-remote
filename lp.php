<?
$path = '';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate();

$page_type	  = 'PKG';
$banner_link = 'images/banner/';
require_once($path."include/config.php");

$breadcamp    = '<ul class="bread-camp"><li><a href="index.php">TOP</a><span>&gt;</span></li>
										<li>ランドパッケージ</li></ul>';



$entry_year = Date('Y');  $entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}





/*search box*/

$record = $db->get_country();
$inp_country = input_selectbox('inp_country',$record[1],$record[0],'','-------Please Select-------','search-left-selectbox top') ;



$record = $db->view_category_all();
$select_category  = '';


for ($i=0; $i< count( $record['value']) ;$i++)
{

	$select_category .= '<option value="'.$record['value'][$i].'" >'.$record['data'][$i].'</option>';
}



$record = $db->view_option2_all();
$select_option = '';

for ($i=0; $i< count( $record['value']) ;$i++)
{

	$select_option .= '<option value="'.$record['value'][$i].'" >'.$record['data'][$i].'</option>';
}


$record = $db->viw_day_all();
$select_time  = '';

for ($i=0; $i< count( $record['value']) ;$i++)
{

	$select_time .= '<option value="'.$record['value'][$i].'" >'.$record['data'][$i].'</option>';
}

/* select year month date */
$entry_year = Date('Y');
$entry_month = Date('n');

$arr_yearmonth_data = array();
$arr_yearmonth_value = array();
$arr_day_data   = array();
$arr_day_value  = array();

function zero_fill($data)
{
	if (($data) < 10)
	{
		$data = '0'.$data;
	}
	return $data;
}

for ($i=0; $i<12;$i++)
{
	if ( ($entry_month + $i) > 12)
	{
		$mm = ($entry_month + $i) - 12;
		
		$arr_yearmonth_value[$i] = ($entry_year+1).'-'.($mm);
		$arr_yearmonth_data[$i]  = ($entry_year+1).'年 '.zero_fill($mm).'月';
	}
	else
	{
		$arr_yearmonth_value[$i] = $entry_year.'-'.($entry_month + $i);
		$arr_yearmonth_data[$i]  = $entry_year.'年 '.zero_fill($entry_month + $i).'月';
	}
}

$select_day = input_selectbox('inp_day',array(),array(),$_GET[inp_day],'日','search-left-selectbox-dd');

if ( (!empty( $_GET[inp_yearmonth] ) ) || ( $_GET[inp_yearmonth] != '0' ))
{
	if ($_GET[inp_yearmonth] != "")
	{
		$arr_lipday = explode('-', $_GET[inp_yearmonth]);
	
		$max_day = cal_days_in_month(CAL_GREGORIAN, ($arr_lipday[1] +1-1), $arr_lipday[0]);
	}
	
	for ($i=0; $i < $max_day; $i++)
	{
		$arr_day_data[$i] = zero_fill($i+1).'日';
		$arr_day_value[$i] = ($i+1);
	}
	
	$select_day   = input_selectbox('inp_day',$arr_day_data,$arr_day_value,$_GET[inp_day],'日','search-left-selectbox-dd');
}

$select_yearmonth = input_selectbox('inp_yearmonth',$arr_yearmonth_data,$arr_yearmonth_value,$_GET[inp_yearmonth],'年月','search-left-selectbox-mmyy');
/* select year month date */


/*search box*/

/* produuct path setting   */
 
$img  = 'product/images/product/';
$link = 'product.php?product_id=';

/* produuct path setting   */

/* product center */

function post_data($result,$link)
{

	$content[0] = '';
	for ($i=0;$i<count($result['product_id']);$i++)
	{
			$content[0] .= '<li><a href="'.$link.$result['product_id'][$i].'" tittle="'.$result['product_id'][$i].'"><span class="text-red">人気</span><span class="text-green">No'.($i+1).
						'</span>'.$result['product_name_jp'][$i].'</a><span class="hide product_id">'.$result['product_id'][$i].'</span></li>';	
		
	}
	
	if ($result['product_id'][0] != '')
	{	
		$content[1] = $img.$result['product_id'][0].'-1.jpg';
	}
	
	return $content;
}

$category_img = array('images/category/001.jpg',
					  'images/category/002.jpg',
					  'images/category/003.jpg',
					  'images/category/004.jpg',
					  'images/category/005.jpg',
					  'images/category/006.jpg',
					  'images/category/007.jpg',
					  'images/category/008.jpg',
					  'images/category/009.jpg',);


for ($i=0;$i<9;$i++)
{

	$result = $db->front_show_product_lp("ALL",$prv_year,$prv_month,($i+1),0,3); 
	$result_data = post_data($result,$link);
	$category[$i] = $result_data[0];
	$cat_img[$i] = $result_data[1];
	
	if (empty($result_data[1]))
	{
		$cat_img[$i] = check_img($category_img[$i]);
		
	
	}

}

/* product center */

/* product slider */


$product_slider = $db->front_show_photosnap_slider($page_type,'ALL');

$slider = '';

for ($i=0;$i<count($product_slider['product_id']);$i++)
{	

	$imgcheck 	 = $img.$product_slider['product_id'][$i].'-1.jpg';

	
	if (!file_exists($imgcheck) )
	{
		$product_img = 'images/img_notfound.jpg' ;
		
	}
	else
	{
		$product_img = 'product/images/index.php?root=product&amp;width=170&amp;name='.trim( $product_slider['product_id'][$i].'-1.jpg') ;
	}



	$slider .= '<li>
				<div class="slider-group">
				<a href="'.$product_slider['path'][$i].'/'.$link.$product_slider['product_id'][$i].'">
				<img class="slider-media" alt="" src="'.$product_img.'" width="170" height="70" />
				</a>
				<div class="slider-media-text">
				<p>'.$product_slider[country_name_jp][$i].'</p>
				<p>
				<a href="'.$product_slider['path'][$i].'/'.$link.$product_slider['product_id'][$i].'"><strong>'.str_len($product_slider['product_name_jp'][$i],27).'</strong></a>
				<br/>
				'. str_len($product_slider['short_desc'][$i],70).'
				</p>
				<div class="price"><span class="txt-red-bold-price">'.
				show_price($product_slider['price_min'][$i], array('sign' => $product_slider[currency_sign][$i],'rate' => $product_slider[currency_rate][$i] )  ).'
				</span>
				</div>
				</div>
				<div class="clear"></div>
				</div>
				</li>';
}
/* product slider */

/* banner */

$arr_record = $db->show_promotion( 'ALL' ,$page_type );
$data_select = explode(',',$arr_record);
	
$banner0 = $db->view_promotion_country($data_select[0]);
$banner1 = $db->view_promotion_country($data_select[1]);
$banner2 = $db->view_promotion_country($data_select[2]);

$banner = array ( 'banner_id' => array( $data_select[0],  $data_select[1],  $data_select[2]) ,
			  	  'banner_name' => array( $banner0[promo_tittle], $banner1[promo_tittle] , $banner2[promo_tittle]),
			      'banner_country' => array( $banner0[path], $banner1[path] , $banner2[path] )
			    );
	
/* banner */

$site_country = 'ALL';
include("include/right_menu.php");

$smarty->template_dir = 'webapp/templates';
$smarty->compile_dir = 'webapp/templates_c';

$smarty->assign("breadcamp",$breadcamp);

/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("select_category",$select_category);
$smarty->assign("select_time",$select_time);
$smarty->assign("select_option",$select_option);
$smarty->assign("select_yearmonth",$select_yearmonth);
$smarty->assign("select_day",$select_day);
/* search box */

/* center product*/


$smarty->assign("category1",$category[0]); 
$smarty->assign("category2",$category[1]);
$smarty->assign("category3",$category[2]);
$smarty->assign("category4",$category[3]);
$smarty->assign("category5",$category[4]);
$smarty->assign("category6",$category[5]);
$smarty->assign("category7",$category[6]);
$smarty->assign("category8",$category[7]);
$smarty->assign("category9",$category[8]);

$smarty->assign("catimg1",check_img($cat_img[0]));
$smarty->assign("catimg2",check_img($cat_img[1]));
$smarty->assign("catimg3",check_img($cat_img[2]));
$smarty->assign("catimg4",check_img($cat_img[3]));
$smarty->assign("catimg5",check_img($cat_img[4]));
$smarty->assign("catimg6",check_img($cat_img[5]));
$smarty->assign("catimg7",check_img($cat_img[6]));
$smarty->assign("catimg8",check_img($cat_img[7]));
$smarty->assign("catimg9",check_img($cat_img[8]));

/* center product*/

/* banner */
$config[documentroot] = '';

$smarty->assign("config",$config);
include("include/jtb_navigation.php");


$smarty->assign("banner_link1",$link.$banner['product_id'][0]);
$smarty->assign("banner_img1", $banner_link.$banner['banner_id'][0].'-1.jpg');
$smarty->assign("banner_name1",$banner['banner_name'][0]);

$smarty->assign("banner_link1",$banner[banner_country][0].'/campaign.php?id='.$banner['banner_id'][0]);
$smarty->assign("banner_img1", $banner[banner_country][0].'/images/banner/'.$banner['banner_id'][0].'-1.jpg');
$smarty->assign("banner_name1",$banner[banner_name][0]);

$smarty->assign("banner_link2",$banner[banner_country][1].'/campaign.php?id='.$banner['banner_id'][1]);
$smarty->assign("banner_img2", $banner[banner_country][1].'/images/banner/'.$banner['banner_id'][1].'-1.jpg');
$smarty->assign("banner_name2",$banner[banner_name][1]);

$smarty->assign("banner_link3",$banner[banner_country][2].'/campaign.php?id='.$banner['banner_id'][2]);
$smarty->assign("banner_img3", $banner[banner_country][2].'/images/banner/'.$banner['banner_id'][2].'-1.jpg');
$smarty->assign("banner_name3",$banner[banner_name][2]);
/* banner */

/* slider */

$smarty->assign("productslider",$slider);
/* slider */


/* select tempage */
$smarty->display('jtb_lp.tpl');
/* select tempage */

/* set tempage */

?>
