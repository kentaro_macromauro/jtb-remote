<?

if ( $_SERVER["SERVER_PORT"] == 443 ){
    Header("Location: http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}

$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

if ( strpos($_SERVER[REQUEST_URI],'index.php') ){
  $url  = substr($_SERVER[REQUEST_URI] ,0, strpos($_SERVER[REQUEST_URI],'index.php') );
  header( "HTTP/1.1 301 Moved Permanently" );
  header('Location: '. $url);
}

require_once($path."include/config.php");

/* produuct path setting */ 
$img  = '../product/images/souvenir/';
$link = '../product/index.php?product_id=';
/* produuct path setting */

/*Search Box*/
$record = $db->get_country();
$inp_country = input_selectbox('inp_country',$record[1],$record[0],$site_country,'---Please Select---','search-left-selectbox w150 top') ;

$record = $db->get_souvenir_theme();

$inp_theme   = input_selectbox('inp_theme',$record[1],$record[0],'','---Please Select---','search-left-selectbox w150 top');


$record = $db->get_souvenir_location();

$inp_location = input_selectbox('inp_location',$record[1],$record[0],'','----','search-left-selectbox w90 top');


$breadcamp    	 = '<ul class="bread-camp"><li><a href="../">TOP</a><span>&gt;</span></li><li><a href="index.php">'.$site_name.'TOP</a><span>&gt;</span></li><li>'.$site_name.'おみやげTOP</li></ul>';
/*Search Box*/


/* product slider1 */


$product_slider1 = $db->front_show_souvenir_photosnap_slider($site_country);



$slider1 = '';


for ($i=0;$i<count($product_slider1['souvenir_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="souvenir_product.php?souvenir_id='.$product_slider1['souvenir_id'][$i].'">';

	$imgcheck =  trim( '../product/images/souvenir/'.$product_slider1['souvenir_id'][$i].'-1.jpg' ) ;
	
	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media2" alt="" src="../images/img_notfound.jpg" width="100"  />';
	}
	else
	{
		
		$product_img = '../product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $product_slider1['souvenir_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media2" alt="'.$product_img.'" src="'.$product_img.'" width="100"  />';
	}
	
	$product_text = $product_slider1['souvenir_name'][$i];
	
	$rate = $db->show_rate($product_slider1['country_iso3'][$i]);

				
	$tempgae .= '</a>
				<div class="slider-media-text">
				<p>'.$product_slider1[country_name_jp][$i].'</p>
				<p>
				<a href="souvenir_product.php?souvenir_id='.$product_slider1['souvenir_id'][$i].'" title="'.$product_text.'"><strong>'.str_len($product_text,20).'</strong></a>
				<br/>
				'.str_len($product_slider1['short_desc'][$i],30).'
				</p>
				';
				
		if ($product_slider1['souvenir_price'][$i] > 0)
		{
			
				
			$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">';
				
			$tempgae .= $product_slider1['affiliate'][$i]?show_jp_price($product_slider1['souvenir_price'][$i]):show_location_price( $product_slider1['souvenir_price'][$i] , 
				array('sign' => $product_slider1[currency_sign][$i],'rate' => $product_slider1[currency_rate][$i], 'show' => $rate )	);
				
			$tempgae .=	'</span>
				</div>' ;
		
		}
		$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider1 .= $tempgae;
}



/* product slider1 */


$smarty = new Smarty;


include("../include/souvenir_country_right_menu.php");
$config[documentroot] = '../';
$smarty->assign("config",$config);


/* banner list */
$arr_record = $db->show_promotion_souvenir( $site_country );





$data_select = explode(',',$arr_record);

$banner0 = $db->view_promotion_souvenir_country($data_select[0]);
$banner1 = $db->view_promotion_souvenir_country($data_select[1]);
$banner2 = $db->view_promotion_souvenir_country($data_select[2]);

$banner = array ( 'banner_id' => array( $data_select[0],  $data_select[1],  $data_select[2]) ,
			  	  'banner_name' => array( $banner0[scampaign_tittle], $banner1[scampaign_tittle] , $banner2[scampaign_tittle]),
			      'banner_country' => array( $banner0[path], $banner1[path] , $banner2[path] )
			    );


$smarty->assign("banner_link1",'souvenir_campaign.php?id='.$banner['banner_id'][0]);

if ( empty( $banner[banner_country][0] ))
{
	$banner[banner_country][0]  = 'public';
}

$smarty->assign("banner_img1", 'images/souvenir_banner/'.$banner['banner_id'][0].'-1.jpg');
$smarty->assign("banner_name1",$banner[banner_name][0]);

$smarty->assign("banner_link2",'souvenir_campaign.php?id='.$banner['banner_id'][1]);

if ( empty( $banner[banner_country][1] ))
{
	$banner[banner_country][1]  = 'public';
}

$smarty->assign("banner_img2", 'images/souvenir_banner/'.$banner['banner_id'][1].'-1.jpg');
$smarty->assign("banner_name2",$banner[banner_name][1]);

$smarty->assign("banner_link3",'souvenir_campaign.php?id='.$banner['banner_id'][2]);

if ( empty( $banner[banner_country][2] ))
{
	$banner[banner_country][2]  = 'public';
}


$smarty->assign("banner_img3", 'images/souvenir_banner/'.$banner['banner_id'][2].'-1.jpg');
$smarty->assign("banner_name3",$banner[banner_name][2]);
/* banner list */

$smarty->assign("breadcamp",$breadcamp);


/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_location",$inp_location);
$smarty->assign("inp_theme",$inp_theme);
/* search box */





/* receive on spot */
$sql = 'SELECT product_list FROM mbus_photosnap_souvenir_country1 WHERE country_iso3 = "'.$select_country.'" ';

$result = $db->db_query($sql);


// number product x 4
/* receive on spot */

/* souvenir_product_list1 */

/* souvenir_product_list1 query */
$souvenir_product_list1 = '';

$sql = 'SELECT product_list FROM mbus_photosnap_souvenir_country1 WHERE country_iso3 = "'.trim($site_country).'" ';

$result = $db->db_query($sql);

while ($record = mysql_fetch_array($result)){
	$product_photo_list1 = $record[product_list];
}

$arr_photo_list1 = explode(';',$product_photo_list1);
$arr_photo1 = '';

if (is_array($arr_photo_list1)){

	foreach ($arr_photo_list1 as $aa){
		if ($aa != '')
		$arr_photo1[] = $aa;	
	}
	
	$in_photo_list1 = implode(',',$arr_photo1);
	
	
	
	$souvenir_product_list1 =  $db->front_show_country_snap1($in_photo_list1);
	
	
}
/* souvenir_product_list1 query */

/* souvenir_product_list1 tempage */


for ($i=0;$i<count($souvenir_product_list1['souvenir_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="souvenir_product.php?souvenir_id='.$souvenir_product_list1['souvenir_id'][$i].'">';

	$imgcheck =  trim( '../product/images/souvenir/'.$souvenir_product_list1['souvenir_id'][$i].'-1.jpg' ) ;
	
	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media2" alt="" src="../images/img_notfound.jpg" width="100"  />';
	}
	else
	{
		
		$product_img = '../product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $souvenir_product_list1['souvenir_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media2" alt="'.$product_img.'" src="'.$product_img.'" width="100"  />';
	}
	
	$product_text = $souvenir_product_list1['souvenir_name'][$i];
	
	$rate = $db->show_rate($souvenir_product_list1['country_iso3'][$i]);

				
	$tempgae .= '</a>
				<div class="slider-media-text">
				
				<p>
				<a href="souvenir_product.php?souvenir_id='.$souvenir_product_list1['souvenir_id'][$i].'" title="'.$product_text.'"><strong>'.str_len($product_text,20).'</strong></a>
				<br/>
				'.str_len($souvenir_product_list1['short_desc'][$i],30).'
				</p>';
				
		if ($souvenir_product_list1['souvenir_price'][$i] > 0)
		{
			
				
			$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">';
				
			$tempgae .= $souvenir_product_list1['affiliate'][$i]?show_jp_price($souvenir_product_list1['souvenir_price'][$i]):show_location_price( $souvenir_product_list1['souvenir_price'][$i] , 
				array('sign' => $souvenir_product_list1[currency_sign][$i],'rate' => $souvenir_product_list1[currency_rate][$i], 'show' => $rate )	);
				
			$tempgae .=	'</span>
				</div>' ;
		
		}
		$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider_souvenir1 .= $tempgae;
}

/*  souvenir_product_list1 tempage */


/* souvenir_product_list1 */



/* souvenir_product_list2 */

/* souvenir_product_list2 query */
$souvenir_product_list2 = '';

$sql = 'SELECT product_list FROM mbus_photosnap_souvenir_country2 WHERE country_iso3 = "'.trim($site_country).'" ';

$result = $db->db_query($sql);

while ($record = mysql_fetch_array($result)){
	$product_photo_list2 = $record[product_list];
}

$arr_photo_list2 = explode(';',$product_photo_list2);
$arr_photo2 = '';

if (is_array($arr_photo_list2)){

	foreach ($arr_photo_list2 as $aa){
		if ($aa != '')
		$arr_photo2[] = $aa;	
	}
	
	$in_photo_list2 = implode(',',$arr_photo2);
	
	
	
	$souvenir_product_list2 =  $db->front_show_country_snap1($in_photo_list2);
	
	
}
/* souvenir_product_list2 query */

/* souvenir_product_list2 tempage */

$slider2 = '';



for ($i=0;$i<count($souvenir_product_list2['souvenir_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="souvenir_product.php?souvenir_id='.$souvenir_product_list2['souvenir_id'][$i].'">';

	$imgcheck =  trim( '../product/images/souvenir/'.$souvenir_product_list2['souvenir_id'][$i].'-1.jpg' ) ;
	
	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media2" alt="" src="../images/img_notfound.jpg" width="100"  />';
	}
	else
	{
		
		$product_img = '../product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $souvenir_product_list2['souvenir_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media2" alt="'.$product_img.'" src="'.$product_img.'" width="100"  />';
	}
	
	$product_text = $souvenir_product_list2['souvenir_name'][$i];
	
	$rate = $db->show_rate($souvenir_product_list2['country_iso3'][$i]);

				
	$tempgae .= '</a>
				<div class="slider-media-text">
				
				<p>
				<a href="souvenir_product.php?souvenir_id='.$souvenir_product_list2['souvenir_id'][$i].'" title="'.$product_text.'"><strong>'.str_len($product_text,20).'</strong></a>
				<br/>
				'.str_len($souvenir_product_list2['short_desc'][$i],30).'
				</p>';
				
		if ($souvenir_product_list2['souvenir_price'][$i] > 0)
		{
			
				
			$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">';
				
			$tempgae .= $souvenir_product_list2['affiliate'][$i]?show_jp_price($souvenir_product_list2['souvenir_price'][$i]):show_location_price( $souvenir_product_list2['souvenir_price'][$i] , 
				array('sign' => $souvenir_product_list2[currency_sign][$i],'rate' => $souvenir_product_list2[currency_rate][$i], 'show' => $rate )	);
				
			$tempgae .=	'</span>
				</div>' ;
		
		}
		$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider_souvenir2 .= $tempgae;
	
	
}

/*  souvenir_product_list1 tempage */


/* souvenir_product_list2 */




$smarty->assign("souvenir_product_list1",$slider_souvenir1);
$smarty->assign("souvenir_product_list2",$slider_souvenir2);




$smarty->assign("productslider1",$slider1);



$smarty->display('country_souvenir.tpl');
?>