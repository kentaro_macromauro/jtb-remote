<? 
if ( $_SERVER["SERVER_PORT"] != 443 ){
    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}

$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = breadcamp(array('TOP','予約する'),array('index.php'));

require_once($path."include/config.php");
$config['allot'] = $_SESSION['checkout']['allot'];

/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../thailand/product.php?product_id=';
/* produuct path setting   */

$product_id = $_SESSION[checkout][product_id];
$result  = $db->view_product($product_id);

$status_book = $result['status_book'];

$payment = $db->paymentcode($result[country_iso3]);


if ($result[country_iso3] != $site_country)
{
	$url = $_SERVER[REQUEST_URI];
	
	$refpath  = $db->showpath_bycountry($result[country_iso3]);
			
	
	$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
			
	header('Location: '.$url);
}



$product_name = $result[product_name];

include("../include/check_booking.php");

$qty_adult  = $_SESSION['checkout'][qty_adult];
$qty_child  = $_SESSION['checkout'][qty_child];
$qty_infant = $_SESSION['checkout'][qty_infant];

$qty_guest = ($qty_adult+$qty_child+$qty_infant);
	
for ($i=0;$i< $qty_guest; $i++)
{
		$arr_guest[$i] = $i;	
}

if ($result['public'] != '1')
	{
		header( "HTTP/1.1 301 Moved Permanently" ); 
		header('Location: index.php');	
	}


if (empty( $_SESSION[checkout][product_id] )) 
{
	header("Location: index.php");	
}
else
	{
	
		if ($result[country_iso3] != $site_country)
		{
			$url = $_SERVER[REQUEST_URI];
			
				$refpath  = $db->showpath_bycountry($result[country_iso3]);
				
		
				$url = str_replace( '/'.$countryname.'/'  ,'/'.$refpath.'/', $url); 
				
				
				
				header('Location: '.$url);
		}

	}
	





if ( empty( $_SESSION['register']['reg']['firstname'] ))
{
	header("Location: index.php?");		
}

$form = array('arr_guest' => $arr_guest );

$booking  = array('date' => jp_strdate($_SESSION[checkout][booking_date]), 
				  'amount_sgd' => number_format($amount_sgd) ,
				  'amount_yen'  =>  number_format($amount_sgd) ,
					  
				  'product'=>array( 'id' =>  $product_id,
									'name' => $result[product_name_jp],
									'img' => "../product/images/product/".$product_id."-1",
									'description'  => $result[description],
								    'qty_adult'  => $_SESSION[checkout][qty_adult],
									'qty_child' => 	$_SESSION[checkout][qty_child],
									'qty_infant' => $_SESSION[checkout][qty_infant],),
				  'his' => array('view' => $his_view,
									'reg' => $_SESSION[register][reg],
								    'guest' => $_SESSION[register][guest] )  );




$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}


$smarty = new Smarty;
include("../include/country_right_menu.php");


if ($_SESSION[checkout][allot] == 'false')
{
	$status_book = 4;	
}


/*
if ( ($_SESSION[checkout]['price_adult'] == 1) || ( $_SESSION[checkout]['price_child'] == 1 ) )
{
	$status_book = 4;	
}
*/



$config[documentroot] = $path;

//
if ($status_book == 1)
{
	session_regenerate_id();
	$transection_id     = session_id();
	$smarty->assign("ordercode",$transection_id);		
	
	$amount = (($_SESSION[checkout][price_adult]  * $_SESSION[checkout][qty_adult]) + 
		       ($_SESSION[checkout][price_child]  * $_SESSION[checkout][qty_child]) +
		       ($_SESSION[checkout][price_infant] * $_SESSION[checkout][qty_infant]));

	
	$smarty->assign("amount",$amount);
	$smarty->assign("currency_product",$payment[code]);
	$smarty->assign("payment_code",$payment[sitecode]);
	$smarty->assign("tel" , $payment[tel]);
}
//

$transection = $_REQUEST[transection];


$smarty->assign('transection',$transection);


$smarty->assign('status_book',$status_book);

$smarty->assign("form",$form);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("booking",$booking);
$smarty->assign("config",$config);
$smarty->display('booking4.tpl');
?>

