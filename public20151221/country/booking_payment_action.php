<? 
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path.'class/c_service.php');
require_once($path."class/phpmailer/class.phpmailer.php");

/* function */
function zero_fill($data)
{
	if (($data) < 10)
	{
		$data = '0'.$data;
	}
		
	return $data;	
}
function jtb_formatdate($inp_date)
{
			
	if (!empty($inp_date))
	{
		$arr_inpdate = explode('-',$inp_date);
		
		$arr_inpdate[0] = substr($arr_inpdate[0],0,4); 
		$arr_inpdate[1] = substr($arr_inpdate[1],0,2);
		$arr_inpdate[2] = substr($arr_inpdate[2],0,2);
		
		if ( !empty( $arr_inpdate[2]) ||  !empty( $arr_inpdate[1]) || !empty( $arr_inpdate[0]) )
		{
			$oup_date = $arr_inpdate[2].'/'.$arr_inpdate[1].'/'.$arr_inpdate[0];	
			
		
		}
		else
		{
			$oup_date = "";
		}
		
		
	}
	else
	{
		$oup_date = "";
		
	}
	
	
	
	return $oup_date;
	
}
function jtb_formattime($inp_time)
{
	$go_time = '';	
	
	if (!empty($inp_time))
	{
		$arr_arrgo_time = explode(':',$inp_time);
		
		if (!empty($arr_arrgo_time[0]) || !empty($arr_arrgo_time[1]) )
		{
			$go_time = $arr_arrgo_time[0].$arr_arrgo_time[1];
		}
		else
		{
			$go_time = '';
		}
	}
	else
	{
		$go_time = '';	
	}
	
	return $go_time ;
}
function init_togender($init)
{
	switch ($init)
	{
		case 1: $data = '男性'; break;
		case 2: $data = '女性'; break;
		default    : $data = ''; break;
	}
	
	return $data;
}
/* function end */

/* setting */
$product_type = 'OPT';
$transection  = $_REQUEST[transection];
$paid_type    = $_REQUEST[paid_type] ;



/* setting end */

if (empty( $paid_type ) )
{	
	header("Location: booking_payment.php?msg=error&transection=".$transection); 	
	
}
else
{
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	/* 1 insert to payment gateway : yes */
	/* 2 insert local db : yes */
	/* 3 send packent to jtb */
	//include("../include/check_booking.php");

	$sql = 'SELECT a.* ,b.* FROM '._DB_PREFIX_TABLE.'booking a  
			LEFT JOIN '._DB_PREFIX_TABLE.'booking_detail b ON a.book_id = b.book_id  
			WHERE md5(a.book_id) = "'.trim($transection).'" ';
			
	$result =$db->db_query($sql);
	
	while ($record = mysql_fetch_array($result))
	{
		$book_id 					= $record[book_id];
		$session_status 			= $record[session_status];
		$country_iso3 				= $record[country_iso3];
		$book_type 					= $record[book_type];
		$book_date 					= $record[book_date];
		$jtb_bookind_id 			= $record[jtb_bookind_id];
		$jtb_cfmcode 				= $record[jtb_cfmcode];
		$jtb_statuscode 			= $record[jtb_statuscode];
		$allot 						= $record[allot];
		$charge_type 				= $record[charge_type];
		$charge_desc 				= $record[charge_desc];
		$transection_id 			= $record[transection_id];
		$amount 					= $record[amount];
		$currency 					= $record[currency];
		//$paid_type 				= $record[paid_type];	
		$paid_status 				= $record[paid_status];	
		$qty_adults 				= $record[qty_adults];		
		$qty_child 					= $record[qty_child];	
		$qty_infant 				= $record[qty_infant];		
		$qty_amount 				= $record[qty_amount];	
		$reg_firstname 				= $record[reg_firstname];	
		$reg_lastname 				= $record[reg_lastname];		
		$reg_furi_firstname 		= $record[reg_furi_firstname];		
		$reg_furi_lastname 			= $record[reg_furi_lastname];		
		$reg_sex 					= $record[reg_sex];	
		$reg_birthday 				= $record[reg_birthday];		
		$reg_email 					= $record[reg_email];	
		$reg_tel1 					= $record[reg_tel1];	
		$reg_tel2 					= $record[reg_tel2];		
		$reg_tel3 					= $record[reg_tel3];	
		$reg_mobile1 				= $record[reg_mobile1];	
		$reg_mobile2 				= $record[reg_mobile2];		
		$reg_mobile3 				= $record[reg_mobile3];		
		$reg_addrtype 				= $record[reg_addrtype];	
		$reg_addrjp_zip1 			= $record[reg_addrjp_zip1];		
		$reg_addrjp_zip2 			= $record[reg_addrjp_zip2];	
		$reg_addrjp_pref 			= $record[reg_addrjp_pref];		
		$reg_addrjp_city 			= $record[reg_addrjp_city];	
		$reg_addrjp_area 			= $record[reg_addrjp_area];		
		$reg_addrjp_building 		= $record[reg_addrjp_building];			
		$reg_addrovs1 				= $record[reg_addrovs1];		
		$reg_addrovs2 				= $record[reg_addrovs2];	
		$reg_backup_tel1 			= $record[reg_backup_tel1];	
		$reg_backup_tel2 			= $record[reg_backup_tel2];	
		$reg_backup_tel3 			= $record[reg_backup_tel3];	
		$reg_backup_addrtype 		= $record[reg_backup_addrtype];		
		$reg_backup_zip1 			= $record[reg_backup_zip1];	
		$reg_backup_zip2 			= $record[reg_backup_zip2];	
		$reg_backup_pref 			= $record[reg_backup_pref];	
		$reg_backup_addrjp_city 	= $record[reg_backup_addrjp_city];		
		$reg_backup_addrjp_area 	= $record[reg_backup_addrjp_area];	
		$reg_backup_addrjp_building = $record[reg_backup_addrjp_building];		
		$reg_backup_addrovs1 		= $record[reg_backup_addrovs1];		
		$reg_backup_addrovs2 		= $record[reg_backup_addrovs2];		
		$reg_hoteltype 				= $record[reg_hoteltype];		
		$reg_hotel_id 				= $record[reg_hotel_id];		
		$reg_hotel_name 			= $record[reg_hotel_name];	
		$reg_hotel_tel 				= $record[reg_hotel_tel];	
		$reg_hotel_addr1 			= $record[reg_hotel_addr1];	
		$reg_hotel_addr2 			= $record[reg_hotel_addr2];	
		$reg_arrfrm 				= $record[reg_arrfrm];	
		$reg_arrto 					= $record[reg_arrto];	
		$reg_arrgo 					= $record[reg_arrgo];	
		$reg_arrgo_time 			= $record[reg_arrgo_time];	
		$reg_airno 					= $record[reg_airno];	
		$reg_info 					= $record[reg_info];	
		$remark 					= $record[remark];	
		$update_date 				= $record[update_date];	
		$update_by 					= $record[update_by];	
		$status_cms 				= $record[status_cms];	
		$update_date_cms 			= $record[update_date_cms];	
		$update_by_cms 				= $record[update_by_cms];	
		$book_id 					= $record[book_id];	
		$book_idx 					= $record[book_idx];	
		$product_id 				= $record[product_id];	
		$product_code 				= $record[product_code];	
		$hotel_id 					= $record[hotel_id];	
		$hotel_sub_id 				= $record[hotel_sub_id];	
		$price_adult 				= $record[price_adult];	
		$price_child 				= $record[price_child];	
		$price_infant 				= $record[price_infant];	
		$qty_adult 					= $record[qty_adult];	
		$qty_child 					= $record[qty_child];	
		$qty_infant 				= $record[qty_infant];	
		$qty_amount 				= $record[qty_amount];	
		$amount_adult 				= $record[amount_adult];	
		$amount_child 				= $record[amount_child];	
		$amount_infant 				= $record[amount_infant];	
		$amount						= $record[amount];
		$allot_cfm  			    = $record[allot_cfm];
	}
	
	$sql = 'SELECT * FROM '._DB_PREFIX_TABLE.'booking_guest WHERE book_id = "'. $book_id .'" ';
	
	$result= $db->db_query($sql); $i= 0; 
	
	while ($record = mysql_fetch_array($result))
	{ 	
		$guest_firstname[$i]  = $record[guest_firstname]; 	
		$guest_lastname[$i]   = $record[guest_lastname]; 	
		$guest_age[$i] 	  	  = $record[guest_age];	
		$guest_sex[$i] 	   	  = $record[guest_sex];
		$guest_birthday[$i]   = $record[guest_birthday];		
		$guest_no[$i] 	      = $record[guest_no];
		$guest_noexpire[$i]   = $record[guest_noexpire];
		$i++;
	}

	$inp_date    = jtb_formatdate($book_date);
	
	$arrive_date = jtb_formatdate($reg_arrfrm);
	$go_date     = jtb_formatdate($reg_arrgo);
	$go_time     = jtb_formattime($reg_arrgo_time);	

	/* product infomation */
	$result         = $db->view_product($product_id);
	$status_book    = $result[status_book];
	$product_name   = $result[product_name_jp];
	$currency_rate  = $result[rate];
	$currency_code  = $result[code];
	$currency_sign  = $result[sign];
	$short_desc     = $result[short_desc];
	$city_iso3      = $result[city_iso3];
	/* product infomation end */
	
	session_regenerate_id();
	$session     = session_id();
		
	if (count($guest_firstname) > 0)
	{
		$guest_list = array();
	}
	
	for ($i=0;$i<count($guest_firstname);$i++)
	{	
		$guest_list[$i] = array( "paxtitle"        => "",
							     "paxname"         => $guest_firstname[$i].' '.$guest_firstname[$i],
							     "paxpassport"     => $guest_no[$i],
							     "passportcoutnry" => "",
							     "passportexpiry"  => jtb_formatdate($guest_noexpire[$i]),
							     "dob"             => jtb_formatdate($guest_birthday[$i]),
							     "paxtype" 		   => "",
							     "paxroomline"     => "");
	}
	
	$pax_dateof_birth = jtb_formatdate($reg_birthday);
	
	if ($charge_type == "Per Pax")
	{
		$service_count ="";
		$service_type  ="";
	}
	else
	{
		$service_count = $qty_amount;
		$service_type  = $charge_type;
	}


	$params = array( 'cmd' => "ADDPRODUCTBOOKING",
				  'params' =>  array("productid"     => $product_code,
								"producttype"   	 => "OPT",
								"datefrom"      	 => $inp_date,
								"dateto"        	 => $inp_date,
								"sessionid"     	 => $transection_id,
								"arrivalcity"   	 => $city_iso3 ,
								"arrivaldate"   	 => $arrive_date,
								"arrivaltime"   	 => "",
								"arvdepremarks" 	 => "None",
								"dropoff"       	 => "None",
								"departuredate" 	 => $go_date,
								"departuretime" 	 => $go_time,
								"pickup"        	 => "None",
								"customerid"    	 => "AGT01",
								"transactionid" 	 => $session,		
								"CurrencyCode"  	 => $currency_code,
								"AllotmentConfCode"  => $allot_cfm ,		
								"adults" 			 => $qty_adults,
								"child"  			 => $qty_child,
								"infant" 			 => $qty_infant,
								"servicetype"		 => $service_type,
    							"servicecount"		 => $service_count,    	
								"bookingremark" 	 => "Nothing",
								"paxlist"            => array( array(  "paxtitle" => "",
																	   "paxname" => $reg_firstname.' '.$reg_lastname ,
																	   "paxpassport" => "",
																	   "passportcoutnry" => "",
																	   "passportexpiry" => "",
																	   "dob" => $pax_dateof_birth,
																	   "paxtype" => "",
																	   "paxroomline" => "")),
								"hotellist"			 => array(),
								"addlservicelist" 	 => array()) );
	
	

				
	if (count($_SESSION[register][guest]) > 0)
	{
		foreach ($guest_list as $gust)
		{
			
			array_push($params[params][paxlist],$gust);
		}
	}		
	
	
	$jtb_statuscode = 'E_PENDING';
	$book_refnum    = '';
	$book_cofmcode  = '';
	
	
	/*-------total infant-------*/
	$db->db_start();		
	/* update confirm order booking */	
	$sql  = 'UPDATE '._DB_PREFIX_TABLE.'booking ';
	$sql .= 'SET session_status = "1", ';
	$sql .= 'jtb_bookind_id = "'.$book_refnum .'" , ';
	$sql .= 'jtb_cfmcode    = "'.$book_cofmcode.'" , ';
	$sql .= 'jtb_statuscode = "'.$jtb_statuscode.'" , ';
	$sql .= 'paid_type		= "'.$paid_type.'" ';
	$sql .= 'WHERE book_id  = "'.$book_id.'" ';

	
	$db->db_query($sql);	
	/* update confirm order booking end */
	
	/* update mothly booking */
	
	$sql   = 'SELECT amount FROM '._DB_PREFIX_TABLE.'monthly_product ';
	$sql  .= 'WHERE product_id = "'.$product_id.'" AND mpro_month = DATE_FORMAT(NOW(),"%c") AND ';
	$sql  .= 'mpro_year = DATE_FORMAT(NOW(),"%Y") '; 
	
	
	$result = $db->db_query($sql); $dd_amount = 0;
	
	while ($record  = mysql_fetch_array($result))
	{
		$dd_amount = $record[amount];	
	}
	
	if (empty($dd_amount))
	{
		$dd_amount = 0;	
	}
	
	$dd_amount= $dd_amount+1;
	
	$sql = 'DELETE FROM '._DB_PREFIX_TABLE.'monthly_product WHERE product_id = "'.$product_id.'" AND mpro_month = DATE_FORMAT(NOW(),"%c") AND ';
	$sql  .= 'mpro_year = DATE_FORMAT(NOW(),"%Y") '; 	
	
	$db->db_query($sql);
	
	$sql  = 'INSERT INTO '._DB_PREFIX_TABLE.'monthly_product(product_id,mpro_year,mpro_month,amount,update_date) ';
	$sql .= 'VALUES ("'.$product_id.'",DATE_FORMAT(NOW(),"%Y"),DATE_FORMAT(NOW(),"%c"),'.$dd_amount.',NOW() ) ';
	
	$db->db_query($sql);
	
	$db->db_commit();
	
	/* update mothly booking end */		
		
	if (($paid_type == 1)||($paid_type == 2))
	{
		
		$api   = new c_websv($params);
		$data  = $api->getall();
	
		
		$jtb_statuscode = $data[d][statuscode];
		$book_refnum    = $data[d][output][BookingRefNum];
		$book_cofmcode  = $data[d][output][BookingConfrimationCode];
	
	}
	else if ($paid_type == 3)
	{
		$jtb_statuscode = "NONE";	
		
		$book_refnum    = "";
		$book_cofmcode  = "";
	}
	
	
	/* update return api booking */
	
	$sql  = 'UPDATE '._DB_PREFIX_TABLE.'booking ';
	$sql .= 'SET jtb_bookind_id = "'.$book_refnum.'",jtb_cfmcode = "'.$book_cofmcode.'", jtb_statuscode = "'.$jtb_statuscode.'" '; 
	$sql .= 'WHERE book_id = "'.$book_id.'" ';
	
	$db->db_query($sql);
	
	/* update return api booking end */
	
	
	$site_name = $db->view_country_jp($country_iso3);
	
	if (($paid_type == 1)||($paid_type == 2))
	{
	$mail_tempage = 'この度はJTBツアーにお申込いただきまして誠にありがとうございます。
		
お申込いただきました内容は下記の通りでございます。
ご確認の程お願い申し上げます。
		
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
この度はJTB'.$site_name.'支店にお申込いただき誠にありがとうございます。
以下の通り、お申込を頂戴しております。ご確認ください。
		
お名前：'.$reg_firstname.' '. $reg_lastname.'
ツアー名：'.$product_name.'
ツアーコメント：'.$short_desc.'
ツアー日：'.date_jp($book_date).'
ご参加者名：';
		
		for ($i =0 ; $i < $qty_amount ; $i++)
		{
			$mail_tempage .= $guest_firstname[$i].' '.$guest_lastname[$i];
			$mail_tempage .= chr(13);	
			
		}
		
		$mail_tempage .= 'ツアー代金：'.$currency_sign.''.$amount;
	  }
			
	  if ($paid_type == 3)
	  {
				
		$mail_tempage = 'この度はJTBツアーにお問合せいただきまして誠にありがとうございます。
		
お問い合わせいただきました内容は下記の通りでございます。
ご確認の程お願い申し上げます。
		
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
この度はJTB'.$site_name.'支店にお問合せいただき、誠にありがとうございます。
以下の通り、お問い合わせを頂戴しております。ご確認ください。

お名前：'.$reg_firstname.' '.$reg_lastname.'
ツアー名：'.$product_name.'
ツアーコメント：'.$short_desc.'
ツアー日：'.date_jp($book_date).'
ご参加者名：';
		
		for ($i =0 ; $i < $qty_amount ; $i++)
		{
			$mail_tempage .= $guest_firstname[$i].' '.$guest_lastname[$i];
			$mail_tempage .= chr(13);	
			
		}
		
		$mail_tempage .= 'ツアー代金：'.$currency_sign.''.$amount;
	  }
		
		$mail_tempage .= chr(13);
		/* register inquiry */
		
		$mail_tempage .= chr(13);
		
		$mail_tempage .= '予約情報確認'.chr(13);
		
		
		$mail_tempage .= 'ツアー名：'.$product_name.chr(13);  
		$mail_tempage .= '日付：'.date_jp( $book_date ).chr(13);
		$mail_tempage .= chr(13);
		
		$mail_tempage .= '■申込情報'.chr(13);
		
		$mail_tempage .= '申込者名（漢字）:'.$reg_firstname.' '. $reg_lastname.chr(13); 
		
		$mail_tempage .= '申込者名（ローマ字）:'.$reg_furi_firstname.' '.$reg_furi_lastname.chr(13);
		
		$mail_tempage .= '生年月日:'.date_jp($reg_birthday).chr(13);
		
		$mail_tempage .= '性別:'.init_togender($reg_sex).chr(13);
		
		$mail_tempage .= 'メールアドレス（PC）:'.$reg_email.chr(13);
		
		$mail_tempage .= '住所'.chr(13);
		
		if ($reg_addrtype == 1)
		{
		$mail_tempage .= '郵便番号:〒'. $reg_addrjp_zip1.'-'.$reg_addrjp_zip2.chr(13);
		
		$mail_tempage .= '都道府県:'.$db->view_pref($reg_addrjp_pref).chr(13);
		
		$mail_tempage .= '市区町村:'.$reg_addrjp_city.chr(13);
		
		$mail_tempage .= '町域:'.$reg_addrjp_area.chr(13);
		
		$mail_tempage .= '建物名部屋番号:'.$reg_addrjp_building.chr(13);	
		}
		else 
		{
			
		$mail_tempage .= '住所1:'.$reg_addrovs1.chr(13);
		
		$mail_tempage .= '住所2:'.$reg_addrovs2.chr(13);
		
		}
		
		
		$mail_tempage .= '連絡先（電話）:'.$reg_tel1.'-'.$reg_tel2.'-'.$reg_tel3.chr(13);
		
		$mail_tempage .= '連絡先（携帯）:'.$reg_mobile1.'-'.$reg_mobile2.'-'.$reg_mobile3.chr(13);	
		
		$mail_tempage .= '住所（緊急連絡先）:'.$reg_backup_addrovs1.chr(13);	
		
		$mail_tempage .= '電話番号（緊急連絡先）:'.$reg_backup_tel1.'-'.$reg_backup_tel2.'-'.$reg_backup_tel3.chr(13);
		
		$mail_tempage .= chr(13);
		$mail_tempage .= '■参加情報'.chr(13);
		
		$mail_tempage .= '宿泊ホテル'.chr(13);
		
		$mail_tempage .= '滞在先名:'.$reg_hotel_name.chr(13);
		
		$mail_tempage .= '滞在先TEL:'.$reg_hotel_tel.chr(13);
		
		$mail_tempage .= '滞在先住所:'.$reg_hotel_addr1.chr(13);
		
		$mail_tempage .= '宿泊期間:'.jp_strdate($reg_arrfrm).'~'.jp_strdate($reg_arrto).chr(13);
		
		$mail_tempage .= '到着日フライト:'.jp_strdate($reg_arrgo).' '.$reg_arrgo_time.chr(13);
		$mail_tempage .= 'フライトナンバー:'.$reg_airno.chr(13);
		$mail_tempage .= 'ご旅行のご形態: '.$reg_info.chr(13);
		
		for ($i =0 ; $i < $qty_amount ; $i++)
		{
			
		$mail_tempage .= chr(13);
		$mail_tempage .= '■参加者情報'.chr(13);
		$mail_tempage .= '参加者名（ローマ字）:'.$guest_firstname[$i].' '.$guest_lastname[$i].chr(13);
		
		$mail_tempage .= '参加者性別:'.init_togender($guest_sex[$i]).chr(13);
		
		$mail_tempage .= '参加者年齢:'.$guest_age[$i].'歳'.chr(13);
		
		$mail_tempage .= '参加者生年月日:'.jp_strdate($guest_birthday[$i]).chr(13);
		
		$mail_tempage .= '参加者パスポート番号:'.$guest_no[$i].chr(13);
		
		$mail_tempage .= '参加者パスポートExpiry date:'.jp_strdate($guest_noexpire[$i]).chr(13); 
		
		}
		$mail_tempage .= chr(13);
		$mail_tempage .= '■その他の情報 '.chr(13);
		$mail_tempage .= $remark.chr(13);
		
		/* register inquiry */
			
		
		if (($paid_type == 1)||($paid_type == 2))
		{
		
$mail_tempage .= '
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
●申込いただいた後、3日以内に確認メールが届きます。
確認メールが届くまでは手配完了ではありませんのでご注意ください。
尚、後日お送りするメール内容は、日時・集合時間・料金・クーポン受け渡し（一部の商品）
・当日の注意（パスポート持参、その他）等の確認が目的となります。
その他旅行条件に関する重要な事項が記載されておりますので必ずご一読ください。
		
●3日以内に当社からの予約確認のEメールが届かない場合には、何らかの障害が発生した	
可能性がありますので、お手数ですが、タイトルに「再送」とご表示いただきの上で
ご再送いただけますようお願いいたします。
メールを受信次第、可能な限り速やかにご連絡をいたします。';
	
		}
		
		if (($paid_type == 3))
		{
$mail_tempage .= '
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
●お問合せいただいた後、3日以内に商品のご予約メールが届きます。
現在、ご予約・手配は完了しておりませんのでご注意ください。
尚、後日お送りするメール内容は、ご予約の可否、またご予約が可能だった場合のお支払
い方法・日時・集合時間・料金・クーポン受け渡し（一部の商品）・当日の注意（パスポート
持参、その他）等のご連絡が目的となります。
その他旅行条件に関する重要な事項が記載されておりますので、必ずご一読ください。

●3日以内に当社からの予約確認のEメールが届かない場合には、何らかの障害が発生した
可能性がありますので、お手数ですが、タイトルに「再送」とご表示の上でご再送いただけ
ますようお願いいたします。
メールを受信次第、可能な限り速やかにご連絡をいたします。';	
		}
		
		
		
		if ( ($country_iso3 == 'IDN' ) && ($city_iso3 == 'BAR' )) {
					$mail_tempage .= chr(13).'※ご予約回答、および、クレジットカード決済代行業者は「AXES Solutions」となります。';					   
		}
		
		$mail_tempage .= chr(13).chr(13);
		
		
		switch( $country_iso3)
		{
			case 'TWN' : $mail_tempage .= '============================================
JTB台湾（世帝喜旅行社股份有限公司）
マイバスデスク
台湾台北市中山北路2段56號JTBラウンジ
09：00～18：00（予約受付は17：30まで）年中無休
Tel: +(886)-02-2521-7315 
Fax:+(886)-02-8192-7111
<a target="_blank" href="http://www.jtbtaiwan.com">http://www.jtbtaiwan.com</a>
台湾のマイバスデスクはアンバサダーホテル（國賓飯店）向いのJTBラウンジ内です。
=============================================
●”マイバス”のオプショナルツアー（27コース）やお勧めレストラン情報（14箇所）、
割引特典（12店舗）の掲載されたパンフレットは下記で入手できます。
・台湾内各主要都市の主なホテル
● マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
'; break;	
			
	case 'THA' : $mail_tempage .= '=============================================
JTB (Thailand) Ltd （ジェイティービー　バンコク支店）
Mybus Operation 
Silom Complex Branch 5th FL.,Room 506-507     
191 Silom Rd.,Silom Khet Bangrak, Bangkok 10500 Thailand　
Tel: タイ国内からおかけの場合 (0)22300490
タイ国外からおかけの場合 +6622300490
Fax: (0)2344-4678
<a target="_blank" href="http://www.mybus-asia.com/thailand/" >http://www.mybus-asia.com/thailand/</a>
=============================================

=============================================
JTB (THAILAND) LIMITED, Phuket Branch, Mybus Desk
JTBプーケット営業所 マイバスデスク

Tel. タイ国内からおかけの場合 (0)76261740
タイ国外からおかけの場合 +6676261740
Fax. +66 (076) 261-748
お電話での予約受付時間 9：00～18：00（年中無休）
		
e-mail: toiawase_phuket.th@jtbap.com
<a href="http://www.jtbphuket.com/" target="_blank">http://www.jtbphuket.com/</a>
<a href="http://www.mybus-asia.com/thailand/" target="_blank">http://www.mybus-asia.com/thailand/</a>
=============================================
●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
'; break;	
			
	case 'VNM' : $mail_tempage .= '
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。	
'; break;	
	
	case 'KHM' : $mail_tempage .= '
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。
'; break;	
	
	
	case 'CAM' : $mail_tempage .= '
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。
'; break;	
		
	case 'MYS' : $mail_tempage .= '====================================
JTB (M) SDN BHD （ジェイティービー　マレーシア支店）
	
ＫＬオフィス　(メリアホテル横、AMODAビル16階）
16.05-16.07 Level 16, Amoda Bldg.
22 Jalan Imbi 55100 Kuala Lumpur
Malaysia 
Tel: +(603) 2142-8727
Fax:+(603) 2142-5344
		
ランカウイ：＋（604）966-4708
ペナン　　：＋（604）263-5788
コタキナバル：＋（6088）259-668
http://www.mybus-asia.com/malaysia/
======================================'; break;

	case 'IDN' : $mail_tempage .= '============================================
PT. JTB INDONESIA （JTBバリ支店）

Jl. Bypass Ngurah Rai No.88
Kelan Abian,Tuban, Kuta, 
Bali 80362 - Indonesia
マイバスほっとダイヤル：0361-708555（バリ島内 / 09：00～19：00）
緊急連絡先：081-2382-9139, 081-23802511（上記時間外）
WEBサイト：http://www.mybus-asia.com/indonesia/
=============================================
●バリ島でのお客様用カウンターデスク
・DFSギャラリア・バリ2階 （営業時間 10：00～18：00）
・グランド・ハイアット・バリ内 （営業時間 08：00～20：00）
●マイバスはアジアと豪州9ヵ国 （香港（マカオ）、台湾、タイ（プーケット）、マレーシア、シンガポール、バリ、ベトナム、オーストラリア、ニュージーランド）で年中無休で運行中。
';
break;	
			
	case 'SGP' : $mail_tempage .= '===============================
JTB Pte Ltd Singapore
Mybus Desk/マイバスデスク
（DFS免税品店内2階）
Tel: (65)-6735－2847 
Fax:(65)-6733－8148
受付時間：10：00－17：30（年中無休）
<a target=_blank" href="http://www.mybus-asia.com/singapore/">http://www.mybus-asia.com/singapore/</a>
e-mail:opjtb.sg@jtbap.com
================================'; 
	break;	
			
	case 'HKG' : $mail_tempage .=  '============================================ 
MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光） 
Add     : Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., 
	  　　Tsim Sha Tsui, Kowloon, Hong Kong. 
Tel      : (852)2375-7576 (*日本語対応　年中無休09:00-18:00)
Fax     : (852)2311-5803 
Mail    : inbound@hk.jtb.cn 
Web    : <a target=_blank" href="http://www.mybus-asia.com/hongkong/">http://www.mybus-asia.com/hongkong/</a>
============================================= 
●”マイバス香港”は、 
　ビギナーからリピーター向けオプショナルツアー（44コース）、 
　便利で安心のスパ・マッサージ（8店舗）、 
　あの憧れのレストランをはじめ厳選のミールクーポン（43コース）、 
　割引特典レストラン（3店舗）をご用意しています。 
●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、 
　バリ、プーケット、ベトナム）で年中無休で運行中。 '; break;
	
	case 'AUS' : $mail_tempage .=  '===========================================
JTB Australia Pty Ltd
		
ケアンズ マイバスデスク
電話　+61-(07)-4052-5872 
毎日 9：00～19：00
61　Abbott　Street, Cairns
	
ゴールドコーストマイバスデスク
電話　+61-(07)-5592-9491 
毎日 8：30～18：30 
Ground Floor, 3191 Surfers Paradise Boulevard
		
シドニー
電話　+61-(02)-9510-0313 
毎日 09:00 ～ 17:00
		
メルボルン
電話　+61(03)-8623-0091 
土日祝日を除く毎日 10:00 ～12:00/14:00 ～16:00 
Level 6, 10 Artemis Lane, Melbourne
(QVショッピングコンプレックス内) 
		
パース
電話　+61(08)-9218-9866 
毎日 09:00 ～ 12:00/13:00 ～ 17:30
Ground Floor, 273 Hay Street, East Perth
isdweb.au@jtbap.com
============================================'; break;	
			
	case 'NZL' : $mail_tempage .=  '===================================================================
JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）
Auckland Mybus Centre
Level 5, 191 Queen St, Auckland
Tel: +(64)-9379-6415 
Fax:+(64)-9309-7699
<a target=_blank" href="http://www.jtb.co.nz/">http://www.jtb.co.nz/</a>
===================================================================
●マイバスセンター　オークランド支店は平日　9：00〜17：30の営業です。
（土日祝日はお休みとなります。）
'; break;			
		}


	$html = nl2br($mail_tempage);


	$mail = new PHPMailer();
	$mail->AddAddress($reg_email);
	

	
	switch( $country_iso3)
	{
		case 'TWN' :  $address_mail = 'option_yoyaku.tw@jtbap.com'; break;	
		
		case 'IDN' :  $address_mail = 'reservation_jtbbali.id@jtbap.com'; break;
		
		case 'THA' : if ($city_iso3 == 'HKT')
					 { 
						$address_mail = 'toiawase_phuket.th@jtbap.com';  
					 }
					 else
					 {
						 $address_mail = 'reservation_mybus.th@jtbap.com';      	 
					 } 
		break;	
		
		case 'VNM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'KHM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;
		
		case 'CAM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'MYS' :  $address_mail = 'mybus.my@jtbap.com'; break;
		 
		case 'SGP' :  $address_mail = 'opjtb.sg@jtbap.com'; break;	
		
		case 'HKG' :  $address_mail = 'inbound@hk.jtb.cn';  break;
		
		case 'AUS' :  $address_mail = 'isdweb.au@jtbap.com'; break;	
		
		case 'NZL' :  $address_mail = 'webnz.nz@jtbap.com'; break;			
	}


	
	
	$mail->AddAddress( $address_mail);
	
	$mail->AddBcc( 'pornpen@auncon.co.jp');
	$mail->AddBcc('jedsadha@auncon.co.jp');
	
	
	$mail->CharSet = 'UTF-8'; 
	
	if (($paid_type == 1)||($paid_type == 2))
	{	
		$mail->Subject =  "【受付番号：".$book_id."】JTB MyBus 予約内容確認 オプショナルツアー";
		
	}
	
	if ($paid_type == 3)
	{
		$mail->Subject =  "【受付番号：".$book_id."】JTB MyBus お問い合わせ内容確認 オプショナルツアー【確認中】";
	}
	
	$mail->MsgHTML('<html><body><div style="width:600px; display:block;">'.$html.'</div></body></html>');
	$mail->SetFrom($address_mail);
	
	
	if (is_array($_SESSION[basket]))
	{
		$basket = array(); $j=0;
	
		for ($i= 0; $i <count($_SESSION[basket]); $i++)
		{
			if ( $_SESSION[basket][$i][product_id] == $_SESSION[checkout][product_id] )
			{
				
			}
			else
			{
				$basket[$j] = $_SESSION[basket][$i];	
				$j++;
			}
		}
		
		if ($_SESSION[add_cart][product_id] == $_SESSION[checkout][product_id])
		{
			unset($_SESSION["add_cart"]);
		}
		
		unset($_SESSION["basket"]);
		$_SESSION[basket] = $basket;
	}
	
	
	if (!$mail->Send()){
			$result = $mail->ErrorInfo;
			
			
			unset($_SESSION[checkout]);
	}else{
					
			$result = 'Mail Send Complete';
			unset($_SESSION[checkout]);
						
	}
	
	unset( $_SESSION[pay_type] );
	unset($_SESSION[status_book]);
	
	$_SESSION[pay_type]    = $_REQUEST[paid_type];
	$_SESSION[status_book] = $status_book;




	header('Location: booking_thank.php'); 

}

?>

