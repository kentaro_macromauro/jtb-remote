<?
if ( $_SERVER["SERVER_PORT"] != 443 ){
    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}


$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = breadcamp(array('TOP',$site_name.'TOP','予約する'),array('../index.php','index.php'));

require_once($path."include/config.php");
/* produuct path setting   */
$img  = '../product/images/product/';
$link = '../thailand/product.php?product_id=';
/* produuct path setting   */


if (empty( $_SESSION[basket] ) )
{
	
	$_SESSION[basket] = array();
	array_push($_SESSION[basket],$_SESSION[add_cart]);	
	
	
}

	$found = 0;
	
	$basket_temp = array();  //var_dump(  $_SESSION[basket] );
	
	
	
	for ($i= 0; $i <count( $_SESSION[basket] ) ; $i++)
	{
		if ($_SESSION[basket][$i][product_id] == $_SESSION[add_cart][product_id])
		{
			$found++;
		}
		else
		{
			array_push($basket_temp,$_SESSION[basket][$i]);	
		}
	}

	

	if ( ($found > 0) || (count( $_SESSION[basket]) > 0)  )
	{
		array_push($basket_temp,$_SESSION[add_cart]);			
	}
	

	$_SESSION[basket] = $basket_temp;




$rate = $db->get_currency_rate($site_country);


$currency_rate = $rate[0][rate];
/* reservation op tour  */


// formats money to a whole number or with 2 decimals; includes a dollar sign in front
function formatMoney($number, $cents = 1) { // cents: 0=never, 1=if needed, 2=always
  if (is_numeric($number)) { // a number
    if (!$number) { // zero
      $money = ($cents == 2 ? '0.00' : '0'); // output zero
    } else { // value
      if (floor($number) == $number) { // whole number
        $money = number_format($number, ($cents == 2 ? 2 : 0)); // format
      } else { // cents
        $money = number_format(round($number, 2), ($cents == 0 ? 0 : 2)); // format
      } // integer or decimal
    } // value
    return $money;
  } // numeric
} // formatMoney
		
	for ($i = 0 ; $i < count($_SESSION[basket]) ;$i++)
	{
		
		if (($_SESSION[basket][$i][allot] == 'true')  || ($_SESSION[basket][$i][allot] == 'false'))  
		{
			    $result  = $db->view_product($_SESSION[basket][$i][product_id]);
				
				$status_book = $result[status_book];
				
				if ($_SESSION[basket][$i][allot] == 'false')
				{
					$status_book = 4;	
				}
				
				if ( ($_SESSION[basket][$i][price_adult] == 1) || ($_SESSION[basket][$i][price_child] == 1) )
				{
					$status_book = 4;	
				}
				
				$country_path = $result['country_path'];
				
				//echo $country_path;
			/*if ($result['product_type'] == 'OPT')
			{*/
				$c_adult  = $_SESSION[basket][$i][price_adult];
				$c_child  = $_SESSION[basket][$i][price_child];
				$c_infant = $_SESSION[basket][$i][price_infant];
				
				if ($c_adult == 1){ $c_adult = 0;}
				if ($c_child == 1){ $c_child = 0;}
				if ($c_infant == 1){ $c_infant = 0;}
				

				
				
				if ($_SESSION[basket][$i]['charge_type'] == 'Per Service'){
					$amount = $c_adult;
				}else{
					
					$amount = (($c_adult  * $_SESSION[basket][$i]['qty_adult']) +
						   ($c_child  * $_SESSION[basket][$i]['qty_child']) +
						   ($c_infant * $_SESSION[basket][$i]['qty_infant']));
				}
				
				
				$currency_rate = $result[rate];
				
				$info = '../'.$country_path.'/booking.php?product_id='.$_SESSION[basket][$i][product_id];
				
				$arrdatefomat_jp = explode('-',$_SESSION[basket][$i]['date']);
				$dateformat_jp	 = $arrdatefomat_jp[0].'年'.$arrdatefomat_jp[1].'月'.$arrdatefomat_jp[2].'日';
				
			
				$show_rate = $db->show_rate($result['country_iso3']);
				
			
				$booking[$i]  = array(
							  'session_id' 	=> $_SESSION[basket][$i][session_id],	  
							  'date' 		=> $_SESSION[basket][$i]['date'], 
							  'date_format' => $dateformat_jp, 
							  'cfm_code' 	=> $_SESSION[basket][$i]['cfm_code'],
							  'charge_type' => $_SESSION[basket][$i]['charge_type'],
							  'charge_desc' => $_SESSION[basket][$i]['charge_desc'],
							  'amount_sgd'  => formatMoney($amount) ,
							  'amount_yen'  => formatMoney(($amount * $currency_rate)) ,
							  'product'		=> array(  'id'           =>  $_SESSION[basket][$i][product_id], 
												 'info'         =>  $info,
												 'country_path' => $country_path,
												 'name_jp'      => $result[product_name_jp],
												 'img'          => "../product/images/product/".$_SESSION[basket][$i][id]."-1",
												 'description'  => $result[description],
												 'price_adult'  => $_SESSION[basket][$i][price_adult],
												 'price_child'  => $_SESSION[basket][$i][price_child],
												 'price_infant' => $_SESSION[basket][$i][price_infant],
												 'qty_adult'    => $_SESSION[basket][$i]['qty_adult'],
												 'qty_child'    => $_SESSION[basket][$i]['qty_child'],
												 'qty_infant'   => $_SESSION[basket][$i]['qty_infant'],
												 'sign'		    => $result['sign'],
												),
							  'other' => array('show_rate' => $show_rate),
							   );
				
				
				
			/*}*/
		}
	}
	

	
	/* setup */
/* reservation op tour */




$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

$smarty = new Smarty;
include("../include/country_right_menu.php");

$config[documentroot] = $path;
$smarty->assign("status_book",$status_book);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("bookingArray",$booking);
$smarty->assign("config",$config);
$smarty->display('booking2.tpl');

?>
