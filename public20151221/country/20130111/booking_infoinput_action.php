<? 
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);




if ($_POST[reg_hotel_select] == true)
{
	$type_hotel = '2';	
	
	$hotel_name = $_POST[reg_hotel_name];
	$hotel_tel  = $_POST[reg_hotel_tel1].'-'.$_POST[reg_hotel_tel2].'-'.$_POST[reg_hotel_tel3];
	$hotel_addr = $_POST[reg_hotel_addr];
	
}
else
{
	$type_hotel = '1';	
	$result = $db->view_hotel($_POST[reg_hotel_id]);	
	$hotel_name = $result[hotel_name_jp];
}


$qty_adult  = $_SESSION['checkout'][qty_adult];
$qty_child  = $_SESSION['checkout'][qty_child];
$qty_infant = $_SESSION['checkout'][qty_infant];



$qty_guest = ($qty_adult+$qty_child+$qty_infant);

$guest_hidden_html = '';


if ($qty_guest> 0)
{
	
	$guest_data = array();
	for ($i =0;$i <  $qty_guest ; $i++)
	{
		
		$guest_data[$i] = array( 'firstname' => $_POST[inp_guest_firstname][$i],
						 		 'lastname'  => $_POST[inp_guest_lastname][$i],
								 'sex'		  => $_POST[inp_guest_sex][$i],
								 'age'		  => $_POST[inp_guest_age][$i],
								 'birth_year' => $_POST[inp_guest_birth_year][$i],
								 'birth_month' => $_POST[inp_guest_birth_month][$i],
								 'birth_day'  => $_POST[inp_guest_birth_day][$i],
								 'sn'		   => $_POST[inp_guest_sn][$i],
								 'expire_year' => $_POST[inp_guest_expire_year][$i],
								 'expire_month' => $_POST[inp_guest_expire_month][$i],
								 'expire_day' => $_POST[inp_guest_expire_day][$i], );
	}
}

$addrjp_pref_name = "";
if (!empty($_POST[reg_addrjp_pref]))
{
	$addrjp_pref_name = $db->view_pref(trim($_POST[reg_addrjp_pref]));
}



$booking = array( 'reg' => array('firstname' => $_POST[reg_firstname],
								 'lastname' => $_POST[reg_lastname],
								 'furi_firstname' => $_POST[reg_firstname_furi],
								 'furi_lastname' => $_POST[reg_lastname_furi],
								 
								 'birth_year' => $_POST[year],
								 'birth_month'=> $_POST[month],
								 'birth_day'  => $_POST[day],
								 
								 'sex' => $_POST[reg_sex],
								 'email'  => $_POST[reg_mail],
								 
								 'type_addr' => $_POST[type_addr],
								 
								 'qty_adult'  => $_POST[qty_adult],
								 'qty_child'  => $_POST[qty_child],
								 'qty_infant' => $_POST[qty_infant],
								 
								 'addrjp_zip1' => $_POST[reg_addrjp_zip1],
								 'addrjp_zip2' => $_POST[reg_addrjp_zip2],
								 'addrjp_pref' => $_POST[reg_addrjp_pref] ,
								 'addrjp_pref_name' => $addrjp_pref_name,
								 'addrjp_city' => $_POST[reg_addrjp_city],
								 'addrjp_area' => $_POST[reg_addrjp_area],
								 'addrjp_building' => $_POST[reg_addrjp_building],
								 
								 'addrovs1'  => $_POST[reg_addrovs1],
								 'addrovs2'	=> $_POST[reg_addrovs2],
								 
								 'tel' => $_POST[reg_tel01].'-'.$_POST[reg_tel02].'-'.$_POST[reg_tel03] ,
								 'tel1' => $_POST[reg_tel01] ,
								 'tel2' => $_POST[reg_tel02] ,
								 'tel3' => $_POST[reg_tel03] ,
								 
								 'mobile'  =>   $_POST[reg_mobile01].'-'.$_POST[reg_mobile02].'-'.$_POST[reg_mobile03] ,
								 'mobile1' => 	$_POST[reg_mobile01] ,
								 'mobile2' => 	$_POST[reg_mobile02] ,
								 'mobile3' => 	$_POST[reg_mobile03] ,

								 'backup_addr1' => $_POST[reg_backup_addr1],
								 'backup_tel'  	  => $_POST[reg_backup_tel1].'-'.$_POST[reg_backup_tel2].'-'.$_POST[reg_backup_tel3] ,
								 'backup_tel1' 	  => $_POST[reg_backup_tel1] ,
								 'backup_tel2'    => $_POST[reg_backup_tel2] ,
								 'backup_tel3'    => $_POST[reg_backup_tel3] ,
								         	 
								 'type_hotel' => $type_hotel,
								 'hotel_id'	  => $reg_hotel_id,
								 'hotel_name' =>  $hotel_name,
								 'hotel_tel' => $_POST[reg_hotel_tel],
								 
								 'hotel_addr' => $_POST[reg_hotel_addr],
								 
								 'hotel_select' => $_POST[reg_hotel_select],
								 
								 'arrfrm_year' 	=> $_POST[reg_arrfrm_year] ,
								 'arrfrm_month' => $_POST[reg_arrfrm_month] ,
								 'arrfrm_day' 	=> $_POST[reg_arrfrm_day] ,
								 
								 'arrto_year'   => $_POST[reg_arrto_year] ,
								 'arrto_month' 	=> $_POST[reg_arrto_month] ,
								 'arrto_day' 	=> $_POST[reg_arrto_day],
								 
								 'arrgo_year'   => $_POST[reg_arrgo_year] ,
								 'arrgo_month' 	=> $_POST[reg_arrgo_month] ,
								 'arrgo_day' 	=> $_POST[reg_arrgo_day] , 
								 
								 'arrgo_hh' 	=> $_POST[reg_arrgo_hh] ,
								 'arrgo_mm' 	=> $_POST[reg_arrgo_mm] ,
								 
								 'air_no' 		=> $_POST[air_no],
								 
								 'sel_info' 	=> $_POST[sel_info],
								 
								 
								 'remark_html' => nl2br( $_POST[remark] ),
								 'remark' => $_POST[remark],
								 )	,
				  	'guest' => $guest_data  );



$_SESSION['register'] = $booking; 
 
/* check error */
$chk_err = 0;

$error = array( 'name' => '',
				'name_furi' => '',
				'birth_date' => '',
				'sex' => '',
				'mail' => '',
				'type_addr' => '',
				
				'addr_type1_zip' => '',
				'addr_type1_pref' => '',
				'addr_type1_area' => '',
				
				'addr_type2' => '',
				'mobile' => '',
				'addr_backup' => '',
				'mobile_backup' => '',
				
				'type_hotel' => '',
				'hotel_type1_id' => '',
				
				'hotel_type2_name' => '',
				'hotel_type2_tel'  => '',
				'hotel_type2_addr' => '',
				
				'guest' => array(),				
		);

if (empty( $_POST[reg_firstname] ) || empty( $_POST[reg_lastname]))
{

	$error[name] = '<p class="text-small-red">お名前を入力してください。</p>';
	$chk_err++;
}

if (empty( $_POST[reg_firstname_furi]) || empty( $_POST[reg_lastname_furi]))
{
	$error[name_furi] = '<p class="text-small-red">ふりがなをローマ字で入力してください。</p>';
	$chk_err++;	
}
else
{
	if (!ereg("[a-zA-Z]", $_POST[reg_firstname_furi]) || !ereg("[a-zA-Z]", $_POST[reg_firstname_furi])) {
		$error[name_furi] = '<p class="text-small-red">半角英数字以外の文字が含まれています。</p>';
		$chk_err++;	
	}	
}


if ( (empty($_POST[year])) || (empty($_POST[month])) || (empty($_POST[day])) )
{
	$error[birth_date] = '<p class="text-small-red">生年月日を入力してください</p>';
	$chk_err++;
}

if ( empty( $_POST[reg_sex] ) )
{
	$error[sex] = '<p class="text-small-red">性別を選択してください。</p>';
	$chk_err++;	
}

if ( empty( $_POST[reg_mail] ) )
{
	$error[mail] = '<p class="text-small-red">メールアドレスを入力してください。</p>';
	$chk_err++;	
}

if ( $_POST[reg_mail] != $_POST[reg_mail_cfm])
{
	$error[mail] = '<p class="text-small-red">確認用メールアドレスが一致しません</p>';
	$chk_err++;	
}


if ( empty( $_POST[type_addr] ) ||  ($_POST[type_addr] == "false") || ( $_POST[type_addr] == '0' ) )
{
	$error[type_addr] = '1';
	
	
	if ( empty( $_POST[reg_addrjp_zip1] )  || empty( $_POST[reg_addrjp_zip2]) )
	{
		$error[addr_type1_zip] = '<p class="text-small-red">郵便番号が相違しています。</p>';
		$chk_err++;	
	}
	
	
	if ( empty ( $_POST[reg_addrjp_pref] ) )
	{
		$error[addr_type1_pref] = '<p class="text-small-red">都道府県を選択してください。</p>';
		$chk_err++;	
	}
	
	if (empty(  $_POST[reg_addrjp_area] ))
	{
		$error[addr_type1_area] = '<p class="text-small-red">地域を選択してください</p>';
		$chk_err++;	
	}
	
}
else
{
	$error[type_addr] = '2';
	
	if ( empty($_POST[reg_addrovs1]) || empty($_POST[reg_addrovs2])  )	
	{
		$error[addr_type2] = '<p class="text-small-red">住所を入力してください。</p>';
		$chk_err++;	
	}
}

if ( empty($_POST[reg_mobile01]) || empty( $_POST[reg_mobile02]) || empty ($_POST[reg_mobile03]) )
{
	$error[mobile] = '<p class="text-small-red">電話番号が相違しています。</p>';
	$chk_err++;	
}


if ( empty($_POST[reg_backup_addr1]) )
{
	$error[addr_backup] = '<p class="text-small-red">住所を入力してください。</p>';
	$chk_err++;	
}

if ( empty($_POST[reg_backup_tel1]) || empty($_POST[reg_backup_tel2]) || empty($_POST[reg_backup_tel3]) )
{
	$error[mobile_backup] = '<p class="text-small-red">電話番号が相違しています。</p>';
	$chk_err++;	
}

if ( empty( $_POST[reg_hotel_select] ) ||  ($_POST[reg_hotel_select] == "false") || ( $_POST[reg_hotel_select] == '0' ) )
{
	$error[type_hotel] = '1';
	
	if ( empty( $_POST[reg_hotel_id]) )
	{	
		$error[hotel_type1_id] = '<p class="text-small-red">ホテルを選択してください</p>';
		$chk_err++;
	}
}
else
{
	$error[type_hotel] = '2';
	
	if ( empty($_POST[reg_hotel_name]) )
	{
		$error[hotel_type2_name] = '<p class="text-small-red">ホテル名を入力してください</p>';
		$chk_err++;	
	}
	
	/*if (empty($_POST[reg_hotel_tel]) )
	{
		$error[hotel_type2_tel] = '<p class="text-small-red">ホテル電話番号を入力してください</p>';		
		$chk_err++;	
	}
	
	if (empty($_POST[reg_hotel_addr]) )
	{
		$error[hotel_type2_addr] = '<p class="text-small-red">ホテル住所を入力してください</p>';
		$chk_err++;	
	
	}*/
	
}




if ( empty($_POST[reg_arrfrm_year])  || 
     empty($_POST[reg_arrfrm_month]) || 
	 empty($_POST[reg_arrfrm_day])   ||
	 empty($_POST[reg_arrto_year])   || 
     empty($_POST[reg_arrto_month])  || 
	 empty($_POST[reg_arrto_day])     )
{
	$error[hotel_stay1] = '<p class="text-small-red">宿泊期間を選択してください</p>';
	$chk_err++;	
}


if ( empty($_POST[reg_arrgo_year]) || empty($_POST[reg_arrgo_month]) || empty($_POST[reg_arrgo_day]))
{
	$error[hotel_stay2] = '<p class="text-small-red">到着日フライトを選択してください</p>';
	$chk_err++;	
}

	
for ($i =0;$i <  $qty_guest ; $i++)
{
	if (empty($_POST[inp_guest_firstname][$i]) || empty($_POST[inp_guest_lastname][$i]))
	{
		$error[guest][$i][name] = '<p class="text-small-red">お名前を入力してください</p>';
		$chk_err++;	
	}

	if (empty($_POST[inp_guest_sex][$i]))
	{
		$error[guest][$i][sex] = '<p class="text-small-red">性別を選択してください。</p>';
		$chk_err++;	
	}
}
	
/* check error  */
if ($chk_err > 0)
{
	$_SESSION[error]  = $error;	
		
	header("Location: booking_infoinput.php?");	
}
else
{

	header("Location: booking_infoinput_confirm.php"); 
}


?>

