<?
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

require_once($path."include/config.php");

$banner_link_lp  = '../product/images/banner/';
$banner_link_opt = '../product/images/banner_opt/';

/* produuct path setting */ 
$img  = '../product/images/souvenir/';
$link = '../product/index.php?product_id=';
/* produuct path setting */

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

$handle = opendir($img);

$product_top = '';
$product_bottom = '';
$i =0;

$souvenir_product_center = '';
$souvenir_product_image = '';

while (false !== ($file = readdir($handle))) 
{

	if  (strpos( '-'.$file , '-'.$_GET[souvenir_id].'-'  ) !== false )
	{
		$i++;
		
		if ($i==1){
			$souvenir_product_center = '<img src="../product/images/souvenir/'.$file.'" alt=""	/>';
			
			
		}
		
		$souvenir_product_image .= '<li  class="product-'.$i.'" ><img src="../product/images/souvenir/'.$file.'" alt="'.$i.'" /></li>';

	}
}


/* show product */
$product = $db->view_souvenir_product($_GET[souvenir_id]);

if ($product[country_iso3] != $site_country)
{
	$url = $_SERVER[REQUEST_URI];
	$refpath  = $db->showpath_bycountry($product[country_iso3]);
}

$breadcamp       = '<ul class="bread-camp"><li><a href="../index.php">TOP</a><span>&gt;</span></li><li><a href="./">'.$site_name.'TOP</a><span>&gt;</span></li><li>'.$product[souvenir_name].'</li></ul>';

	
	$product_category = $product[souvenir_theme];
	

	$buff_arr1 = explode(';',$product_category) ;

		
	$sql  = 'SELECT themesouvenir_id,themesouvenir_name FROM ';	
	$sql .= '(SELECT IF(themesouvenir_id<100,IF (themesouvenir_id<10,CONCAT(\'00\',themesouvenir_id),CONCAT(\'0\',themesouvenir_id)),themesouvenir_id) themesouvenir_id,themesouvenir_name FROM mbus_theme_souvenir ) AS a ';
	$sql .=  'WHERE ';				 
	foreach ($buff_arr1 as $text)
	{
		$sql .= '(themesouvenir_id = "'.$text.'") OR ';
	}
	$sql .= '(themesouvenir_id = "'.$buff_arr1[0].'") ORDER BY themesouvenir_name ';	
		

	
	$record = $db->db_query($sql);
	$souvenir_product_theme = '<ul class="search-result-category">';
		
	
	
	
	if ($product[store_type] == '1'){
		$souvenir_product_theme .= '<li>現地受取</li>'	;
	}
	else if ($product[store_type] == '2'){
		$souvenir_product_theme .= '<li>日本受取</li>'	;
	}
	
		
	while ($rec = mysql_fetch_array($record))
	{
		$souvenir_product_theme .= '<li>'.$rec['themesouvenir_name'].'</li>'	;
	}
	
	
	if ($product[status_book] == "1"){
		$souvenir_product_theme .= '<li>カード決済</li>';
	}
	else if ($product[status_book] == "2"){
		$souvenir_product_theme .= '<li>現地支払</li>';
	}

	$souvenir_product_theme .= '</ul>';
/* show product */
	//$product['souvenir_price'] = number_format($product[souvenir_price]);

	$product['show_rate'] = $db->show_rate($product[country_iso3]);

	//$product['souvenir_show_price']  = $rate[0][sign].' '.number_format($product[souvenir_price]);


	
	$product['souvenir_show_price'] =  $product['affiliate']?show_jp_price($product['souvenir_price']):show_location_price( $product['souvenir_price'] , 
				array('sign' => $product[sign],'rate' => $product[currency_rate], 'show' => $rate )	);




	$product['text_table'] =  str_replace('<th></th>','<th>&nbsp;</th>',str_replace('<th></th>','<th>&nbsp;</th>', jd_decode($product['text_table']) ));

	$product['text_top']   = trim(nl2br($product['text_top']));
	$product['text_bottom']   =  trim(nl2br($product['text_bottom']));
	




if ($product[affiliate]){
	$product['submiturl'] = $product[affiliate_url];	
}
else{
	$product['submiturl'] = 'souvenir_booking.php?id='.$_REQUEST[souvenir_id];	
}

$product['remark']   =  trim(nl2br($product['remark']));
$product['link'] = '../product/images/product/';
$product['category'] = $category;

$smarty = new Smarty;

include("../include/souvenir_country_right_menu.php");

$config[documentroot] = '../';


$smarty->assign("souvenir_product_theme",$souvenir_product_theme);

$smarty->assign("souvenir_product_img1",$souvenir_product_center);
$smarty->assign("souvenir_product_image_list",$souvenir_product_image);

$smarty->assign("product_temp_bottom",$product_bottom);
$smarty->assign("product_top",$product_top);
$smarty->assign("patetittle","product_souvenir");
$smarty->assign("config",$config);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("product_policy",$product_policy);
$smarty->assign("product_itiner",$table);
$smarty->assign("product",$product);
$smarty->assign("productdetail",nl2br($product[description]));

$smarty->display('country_souvenir_product_index.tpl');
?>
