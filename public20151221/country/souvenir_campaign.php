<?
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$rate = $db->get_currency_rate($site_country);

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

require_once($path."include/config.php");


function print_right_price_start_to_campan($sgd,$yen)
{
	if ($yen[affiliate]){
		return '<span class="txt-red-bold-price">\\'.number_format($sgd, (intval($sgd)==$sgd)?0:2 ).'';
	}
	else{
	
		if ($yen[show] == 1)
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd, (intval($sgd)==$sgd)?0:2).' ( 目安：\\'.number_format( round($yen[rate]*$sgd) ).')</span>';
		}
		else
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd, (intval($sgd)==$sgd)?0:2).'';
		}
	
	}
	 
}

if (empty($_GET[id]))
{
	header("Location: index.php");
	
}

$campaign = $db->view_promotion_souvenir_country($_GET[id]);

$campaign_id = $campaign[scampaign_id];
$campaign_tittle = $campaign[scampaign_tittle];
$campaign_content = nl2br($campaign[scampaign_detail]);


										
$breadcamp = breadcamp(array('TOP',$site_name.'TOP',$site_name.'おみやげTOP',$campaign_tittle),array('../index.php','index.php','souvenir.php'));
										
	
										
	$arr_product_list =  explode(',',$campaign[souvenir_list]); 
	
	
	$sql = 'SELECT souvenir_id,path,
			b.country_iso3,country_name_en,country_name_jp,souvenir_code,souvenir_name,
			short_desc,souvenir_price,a.update_date,rate,affiliate,c.sign 
			FROM mbus_souvenir a
			LEFT JOIN 
			mbus_country b ON a.country_iso3 = b.country_iso3
			LEFT JOIN mbus_currency c ON a.country_iso3 = c.country_iso3 
			AND b.country_iso3 = c.country_iso3 
			WHERE ';
	
	for ($i = 0; $i < count($arr_product_list); $i++){
		if (!empty($arr_product_list[$i])){
			$sql .= 'souvenir_id = "'.$arr_product_list[$i].'" OR ';		
		}	
	}
	
	$sql .= 'souvenir_id = "0" ';
	$sql .= 'ORDER BY field(souvenir_id,'.$campaign[souvenir_list].'0)';


	$result = $db->db_query($sql); $i=0;
	
	$product_campaign = '<ul class="campaign-product-unit">'; 
	

	while ($rec = mysql_fetch_array($result))
	{
		$product_list[id][$i]      = $rec[souvenir_id];
		$product_list[path][$i]    = $rec[path];
		$product_list[country][$i] = $rec[country_name_jp];
		$product_list[name][$i]    = $rec[souvenir_name];
		$product_list[desc][$i]    = $rec[short_desc];
		$product_list[affiliate][$i] = $rec[affiliate];
		
		
		$imgcheck =  trim( '../product/images/souvenir/'.$product_list[id][$i].'-1.jpg' ) ;
	
		if (!file_exists($imgcheck) )
		{
			$img = '<img alt="" src="../images/img_notfound.jpg" width="170" height="70" />';
		}
		else
		{
			
			$product_img = '../product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim($product_list[id][$i].'-1.jpg') ;
			$img = '<img alt="" src="'.$product_img.'" width="100"  />';
		}		
	

		$product_list[price][$i]   = print_right_price_start_to_campan( 
									$rec['souvenir_price'], 
									array( 'rate' => $rec[rate] , 
										   'sign' => $rec[sign] , 
										   'show' => $db->show_rate($rec['country_iso3']),
										   'affiliate' => $product_list[affiliate][$i] )
									) ;



		
		$product_list[desc][$i]    = $rec[short_desc];
		
		$product_campaign .= '<li>
								<div class="campaign-product-list">
									<div class="campaign-product-img">';
									
		
		$product_campaign .= '<a href="../'.$product_list[path][$i].'/souvenir_product.php?souvenir_id='.$product_list[id][$i].'">';
									
		
		
		$product_campaign .= $img.'</a></div>
									<div class="campaign-product-tittle" style="width:175px;">';
									
		$product_campaign .= '<a href="../'.$product_list[path][$i].'/souvenir_product.php?souvenir_id='.$product_list[id][$i].'">';
		
									
		$product_campaign .=  $product_list[name][$i] .'</a></div>	
									<p class="campaign-product-des">'.str_len($product_list[desc][$i],65).'</p>
									<p class="campaign-product-price"><span>'.$product_list[price][$i].'</span></p>
								</div>
							 </li>';  
	}
	
	
	$product_campaign .= "</ul>";			
					
					
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/souvenir_product.php?product_id=';


$smarty = new Smarty;
include("../include/souvenir_country_right_menu.php");

$config[documentroot] = $path;

$smarty->assign("campaign_id",$campaign_id);
$smarty->assign("campaign_tittle",$campaign_tittle);
$smarty->assign("campaign_content",$campaign_content);
$smarty->assign("campaign_list",$product_campaign);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);
$smarty->display('country_souvenir_campaign.tpl');
?>