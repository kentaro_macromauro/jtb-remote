<? 
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');


$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

require_once($path."include/config.php");
$config['allot'] = 'true';

$breadcamp = breadcamp(array('TOP',$site_name.'TOP','注文する'),array('../index.php','index.php'));

/* produuct path setting   */
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

include("../include/check_souvenir.php");

$customer = array();

/* customer order information */

$customer[inp_surname]   	  = $_REQUEST[inp_surname];
$customer[inp_firstname] 	  = $_REQUEST[inp_firstname];

$customer[inp_surname_furi]   = $_REQUEST[inp_surname_furi];
$customer[inp_firstname_furi] = $_REQUEST[inp_firstname_furi];

$customer[birthday_year]   = $_REQUEST[birthday_year];
$customer[birthday_month]  = $_REQUEST[birthday_month];
$customer[birthday_day]	   = $_REQUEST[birthday_day];

$customer[inp_sex]		   = $_REQUEST[inp_sex];
$customer[inp_email]	   = $_REQUEST[inp_email];

$customer[type_addr]	   = $_REQUEST[type_addr];

$customer[inp_addrjp_zip1] = $_REQUEST[inp_addrjp_zip1];
$customer[inp_addrjp_zip2] = $_REQUEST[inp_addrjp_zip2];
$customer[inp_addrjp_pref] = $_REQUEST[inp_addrjp_pref];
$customer[inp_addrjp_city] = $_REQUEST[inp_addrjp_city];
$customer[inp_addrjp_area] = $_REQUEST[inp_addrjp_area];
$customer[inp_addrjp_building] = $_REQUEST[inp_addrjp_building];

$customer[inp_addrovs1]	   = $_REQUEST[inp_addrovs1];
$customer[inp_addrovs2]	   = $_REQUEST[inp_addrovs2];

$customer[inp_tel1_1]	   = $_REQUEST[inp_tel1_1];
$customer[inp_tel1_2]  	   = $_REQUEST[inp_tel1_2];
$customer[inp_tel1_3]	   = $_REQUEST[inp_tel1_3];

$customer[inp_mobile1_1]   = $_REQUEST[inp_mobile1_1];
$customer[inp_mobile1_2]   = $_REQUEST[inp_mobile1_2];
$customer[inp_mobile1_3]   = $_REQUEST[inp_mobile1_3];


$customer[recive_address]  = $_REQUEST[recive_address];

$customer[inprec_surname]   = $_REQUEST[inprec_surname];
$customer[inprec_firstname] = $_REQUEST[inprec_firstname];

$customer[inprec_surname_furi]   = $_REQUEST[inprec_surname_furi];
$customer[inprec_firstname_furi] = $_REQUEST[inprec_firstname_furi];



$location_rec = $db->check_store_location($site_country);

/* recive data and insert to db */
/* next page sending email */
/* */


if ($location_rec == 1){
	
	$customer["reg_hotel_name"] 	= $_REQUEST["reg_hotel_name"];
	$customer["reg_hotel_tel"]      = $_REQUEST["reg_hotel_tel"];
	$customer["reg_hotel_addr"]     = $_REQUEST["reg_hotel_addr"];
	
	$customer["reg_arrfrm_year"]	= $_REQUEST["reg_arrfrm_year"];
	$customer["reg_arrfrm_month"]   = $_REQUEST["reg_arrfrm_month"];
	$customer["reg_arrfrm_day"]		= $_REQUEST["reg_arrfrm_day"];
	
	$customer["reg_arrto_year"]		= $_REQUEST["reg_arrto_year"];
	$customer["reg_arrto_month"]	= $_REQUEST["reg_arrto_month"];
	$customer["reg_arrto_day"]		= $_REQUEST["reg_arrto_day"];
	
	$customer["reg_arrgo_year"]		= $_REQUEST["reg_arrgo_year"];
	$customer["reg_arrgo_month"]	= $_REQUEST["reg_arrgo_month"];
	$customer["reg_arrgo_day"]      = $_REQUEST["reg_arrgo_day"];
	
	$customer["reg_arrgo_hh"]		= $_REQUEST["reg_arrgo_hh"];
	$customer["reg_arrgo_mm"] 		= $_REQUEST["reg_arrgo_mm"];
	
	
	$customer["air_no"]				= $_REQUEST["air_no"];
	
	$customer["sel_info"]			= $_REQUEST["sel_info"];
	$customer["inp_remark"]			= $_REQUEST["inp_remark"];
	
	if ($_REQUEST["reg_arrfrm_year"] && $_REQUEST["reg_arrfrm_month"] && $_REQUEST["reg_arrfrm_day"]){
		$customer["reg_arrfrm_date"]= check_date($_REQUEST["reg_arrfrm_year"],$_REQUEST["reg_arrfrm_month"],$_REQUEST["reg_arrfrm_day"]);
	}
	else{
		$customer["reg_arrfrm_date"] = '0000-00-00';	
	}
	
	if ($_REQUEST["reg_arrto_year"] && $_REQUEST["reg_arrto_month"] && $_REQUEST["reg_arrto_day"]){
		$customer["reg_arrto_date"]  = check_date($_REQUEST["reg_arrto_year"],$_REQUEST["reg_arrto_month"],$_REQUEST["reg_arrto_day"]);
	}
	else{
		$customer["reg_arrto_date"] = '0000-00-00';	
	}
	
		
	if ($_REQUEST["reg_arrgo_year"] && $_REQUEST["reg_arrgo_month"] && $_REQUEST["reg_arrgo_day"]){
		$customer["reg_arrgo_date"] = check_date($_REQUEST["reg_arrgo_year"],$_REQUEST["reg_arrgo_month"],$_REQUEST["reg_arrgo_day"]);
	}
	else{
		$customer["reg_arrgo_date"] = '0000-00-00';	
	}
	
	if ($_REQUEST["reg_arrgo_hh"]  &&  $_REQUEST["reg_arrgo_mm"]){
		$customer["reg_arrgo_time"] = check_time($_REQUEST["reg_arrgo_hh"] , $_REQUEST["reg_arrgo_mm"]);
	}
	else{
		$customer["reg_arrgo_time"] = '00:00';
	}

	
}
else if ($location_rec == 2){
		
	$customer["recive_address"]         = $_REQUEST["recive_address"];
	$customer["inprec_surname"]         = $_REQUEST["inprec_surname"];
	$customer["inprec_firstname"]       = $_REQUEST["inprec_firstname"];
	
	$customer["inprec_surname_furi"]    = $_REQUEST["inprec_surname_furi"];
	$customer["inprec_firstname_furi"]  = $_REQUEST["inprec_firstname_furi"];
	
	$customer["inprec_addrjp_zip1"]     = $_REQUEST["inprec_addrjp_zip1"];
	$customer["inprec_addrjp_zip2"]	    = $_REQUEST["inprec_addrjp_zip2"];
	$customer["inprec_addrjp_pref"]     = $_REQUEST["inprec_addrjp_pref"];
	$customer["inprec_addrjp_city"]     = $_REQUEST["inprec_addrjp_city"];
	$customer["inprec_addrjp_area"]     = $_REQUEST["inprec_addrjp_area"];
	$customer["inprec_addrjp_building"] = $_REQUEST["inprec_addrjp_building"];
	
	$customer["inprec_tel1_1"] 		    = $_REQUEST["inprec_tel1_1"];
	$customer["inprec_tel1_2"] 		    = $_REQUEST["inprec_tel1_2"];
	$customer["inprec_tel1_3"] 		    = $_REQUEST["inprec_tel1_3"];
	$customer["inp_remark"]			    = $_REQUEST["inp_remark"];	
	
}
else{
	header( "HTTP/1.1 301 Moved Permanently" ); 
	header('Location: souvenir.php');		
}


/* customer order information */

$_SESSION[cart_souvenir_customer] = $customer;


/* check error */
$chk_err = 0;

$error = array( 'name' => '',
				'name_furi' => '',
				'birth_date' => '',
				'sex' => '',
				'mail' => '',
				'type_addr' => '',
				
				'addr_type1_zip' => '',
				'addr_type1_pref' => '',
				'addr_type1_area' => '',
				
				'addr_type2' => '',
				'mobile' => '',

				
				'send_name' => '',
				'send_name_furi' => '',
				
				'send_type_addr' => '',
				'send_type1_zip' => '',
				
				'send_addr_type1_zip' => '',
				'send_addr_type1_pref' => '',
				'send_addr_type1_area' => '',
				
				'send_addr_type2' => ''				
		);


/* check customer name */
if (empty( $customer[inp_surname] ) || empty( $customer[inp_firstname]))
{

	$error[name] = '<p class="text-small-red">お名前を入力してください。</p>';
	$chk_err++;
}
/* check customer name */

/* check customer furi name */
if (empty($customer[inp_surname_furi]) || empty( $customer[inp_firstname_furi]))
{
	$error[name_furi] = '<p class="text-small-red">ふりがなをローマ字で入力してください。</p>';
	$chk_err++;	
}
else
{
	if (!ereg("[a-zA-Z]", $customer[inp_surname_furi]) || !ereg("[a-zA-Z]", $customer[inp_firstname_furi])) {
		$error[name_furi] = '<p class="text-small-red">半角英数字以外の文字が含まれています。</p>';
		$chk_err++;	
	}	
}

/* check customer furi name */


/* check customer birth day*/
if ( (empty($customer[birthday_year])) || (empty($customer[birthday_month])) || (empty($customer[birthday_day])) )
{
	$error[birth_date] = '<p class="text-small-red">生年月日を入力してください</p>';
	$chk_err++;
}
/* check customer birth day*/

/* check customer gender */
if ( empty( $customer[inp_sex]) ){
	$error[sex] = '<p class="text-small-red">性別を選択してください。</p>';
	$chk_err++;	
}
/* check customer gender */

/* check customer email*/
if ( empty( $customer[inp_email] ) )
{
	$error[mail] = '<p class="text-small-red">メールアドレスを入力してください。</p>';
	$chk_err++;	
}

if ( $customer[inp_email] != $_POST[inp_email_conf])
{
	$error[mail] = '<p class="text-small-red">確認用メールアドレスが一致しません</p>';
	$chk_err++;	
}
/* check customer email*/

/* check customer address */
if ( empty( $customer[type_addr] ) ||  ($customer[type_addr] == "false") || ( $customer[type_addr] == '0' ) )
{
	$error[type_addr] = '1'; //address in japan
	
	if ( empty($customer[inp_addrjp_zip1] )  || empty($customer[inp_addrjp_zip2]) )
	{
		$error[addr_type1_zip] = '<p class="text-small-red">郵便番号が相違しています。</p>';
		$chk_err++;	
	}
	
	
	if ( empty ($customer[inp_addrjp_pref] ) )
	{
		$error[addr_type1_pref] = '<p class="text-small-red">都道府県を選択してください。</p>';
		$chk_err++;	
	}
	
	if (empty(  $customer[inp_addrjp_area] ))
	{
		$error[addr_type1_area] = '<p class="text-small-red">地域を選択してください</p>';
		$chk_err++;	
	}
	
}
else
{
	$error[type_addr] = '2'; //address oversea
	
	if ( empty($customer[inp_addrovs1]) || empty($customer[inp_addrovs2])  )	
	{
		$error[addr_type2] = '<p class="text-small-red">住所を入力してください。</p>';
		$chk_err++;	
	}
}
/* check customer address */

/* check customer telephone */
if ( empty($customer[inp_mobile1_1]) || empty($customer[inp_mobile1_2]) || empty ($customer[inp_mobile1_3]) )
{
	$error[mobile] = '<p class="text-small-red">電話番号が相違しています。</p>';
	$chk_err++;	
}
/* check customer telephone */


$_SESSION[error] = $error;


if ($chk_err){
	
	header("Location: souvenir_cart_inp_address.php");

}
else{
	
	/* check souvenir product price */
	
	$souvenir_list = $_SESSION[cart_souvenir_checkout][souvenir_list];
	/* check souvenir product */
	$souvenir_list_id = array();
	$souvenir_list_qty = array();
	
	function zero_fill($data){	
		return $data<10?'0'.$data:$data ;	
	}	
	
	$souvenir_id_array = array();
	
	if (is_array($souvenir_list)){
		while ($obj = current($souvenir_list)){
				
				$souvenir_id_array[]  = key($souvenir_list);
				
				$cart_items[]  =  array( 'id' 	 => key($souvenir_list),  
										 'value' => $obj);
				
			
			next($souvenir_list);
		}
	}
	
	/* check souvenir product price */
	
	$souvenir_id_string = implode(',',$souvenir_id_array);
	
	
		
	/* check souvenir product price */
	/* check souvenir product */
	
	
	
	$souvenir_id_string = implode(',',$souvenir_id_array);
	
	$arr_input_order_detail = array();
	
	$sql = 'SELECT a.souvenir_id, 
					a.country_iso3, country_name_en,country_name_jp,
					rate,sign,code,souvenir_theme,
					souvenir_code,souvenir_name,
					store_type,
					souvenir_price,souvenir_allotment,
					short_desc, public, 
					status_book,affiliate_code,
					affiliate,affiliate_url,remark,
					b.text_top,b.text_table,b.text_bottom ,
					IF ( souvenir_allotment > 0 , \'1\',\'0\' ) AS sale_status  
					FROM mbus_souvenir a 
					LEFT JOIN mbus_souvenir_detail b ON a.souvenir_id = b.souvenir_id 
					LEFT JOIN mbus_country c ON a.country_iso3 = c.country_iso3 
					LEFT JOIN mbus_currency d ON a.country_iso3 = d.country_iso3 
					WHERE a.souvenir_id IN ('.$souvenir_id_string.') AND public = 1 AND affiliate = 0 AND 
					a.country_iso3 = \''.$site_country.'\' 
					ORDER BY souvenir_name';
		
		$result = $db->db_query($sql); $i = 0;
		
		$amount_summery = 0;
		$qty_summery    = 0;
		$country_iso3   = "";	
	
		while ($record = mysql_fetch_array($result)){
			
			$country_iso3       = $record[country_iso3];
			
			$souvenir_allotment = $record[souvenir_allotment];
			$souvenir_id		= $record[souvenir_id];
			$souvenir_name		= $record[souvenir_name];
			$souvenir_code		= $record[souvenir_code];
			$souvenir_allotment = $record[souvenir_allotment];
			//$store_type         = $record[store_type];  // recive : 1 == recive on spot 2 = recive in japan
			$paid_type			= $record[store_type];
			$status_book		= 0;
			
			//$status_book		= $record[status_book] ; // paid type : 1 = credit 2 = cash 
			
			if (( $_SESSION["cart_souvenir_checkout"]["cart_type"] == 4) || ($_SESSION["cart_souvenir_checkout"]["cart_type"] == 5 )){
				$paid_type = 3;
			}
			
			$price				= $record[souvenir_price];
			$sign				= $record[sign];
			
			
			$i++;
			
			foreach ($cart_items as $items){ // forech1 
				if ($items[id] == $souvenir_id){ // id matching 
	
					$qty = $items[value];
					$price = $price;
				
					$amount = number_format($qty * $price , (intval($price)==$price)?0:2);
					$amount_noneformat = ($qty * $price);
					
					$amount_summery += ($qty * $price);
					$qty_summery    +=  $qty;
					
					
					$arr_input_order_detail[] = array( $i, $souvenir_id, $souvenir_code, $price, $qty, $amount_noneformat);	
					
				$cart[souvenir_product][] = array('image'  => '<img src="../product/images/souvenir/'.$souvenir_id.'-1.jpg" style="width:56px;" alt="" />',
												  'name'   => $souvenir_name,
												  'price'  => $sign.' '.number_format($price , (intval($price)==$price)?0:2),
												  'qty'    => $qty,
												  'amount' => $sign.' '.$amount);
		
				}
			}
		}
		
		$cart[price_sum] = $sign.' '.number_format($amount_summery , (intval($amount_summery)==$amount_summery)?0:2);
		
	/* check souvenir product price */
	
	
	if ( !empty($_SESSION[cart_souvenir_customer][birthday_year]) && 
			 !empty($_SESSION[cart_souvenir_customer][birthday_month]) &&
			 !empty($_SESSION[cart_souvenir_customer][birthday_day]))
	{
			
			
			
		$customer_birthday = zero_fill($_SESSION[cart_souvenir_customer][birthday_year]).'-'.
								 zero_fill($_SESSION[cart_souvenir_customer][birthday_month]).'-'.
								 zero_fill($_SESSION[cart_souvenir_customer][birthday_day]);
			
	} 
	
	
	
	
	/* insert db for cache */	
		$db->db_start();
	
	
		$order_id = $db->db_newid('souvenir_order');
		
		$order_rec = $_SESSION[cart_souvenir_customer][recive_address]?'1':'0';
		
		if ($location_rec == 1){
			
				$arr_order_tb = array(	'order_id',
										'order_refid',
										'session_status',
										'country_iso3',
										'transection_id',
										'product_amount',
										'product_qty',
										'paid_type', //cash credit 
										'paid_status', //affilate
										'order_firstname',
										'order_surname',
										'order_furi_firstname',
										'order_furi_surname',
										'order_sex',
										'order_birthday',
										'order_email',
										'order_tel1',
										'order_tel2',
										'order_tel3',
										'order_mobile1',
										'order_mobile2',
										'order_mobile3',
										'order_addrtype',
										'order_addrjp_zip1',
										'order_addrjp_zip2',
										'order_addrjp_pref',
										'order_addrjp_city',
										'order_addrjp_area',
										'order_addrjp_building',
										'order_addrovs1',
										'order_addrovs2',
										'receive_type',
										
										'acc_name',
										'acc_tel',
										'acc_addr',
										'stay_from',
										'stay_to',
										'arrival_date',
										'arrival_time',
										'flight_no',
										'travel',
										
										'order_remark',
										
										'update_date',
										'update_by',
										'status_cms',
										'update_by_cms',
										'status_cms_paid');	
				
				
				$arr_order_val = array($order_id,
								'0',
								'0',
								$country_iso3,
								session_id(),
								$amount_summery,
								$qty_summery,
								$paid_type,
								$status_book,
								/*$store_type,
								$status_book,*/
								$_SESSION[cart_souvenir_customer][inp_firstname],
								$_SESSION[cart_souvenir_customer][inp_surname],
								$_SESSION[cart_souvenir_customer][inp_firstname_furi],
								$_SESSION[cart_souvenir_customer][inp_surname_furi],
								$_SESSION[cart_souvenir_customer][inp_sex]=='男性'?'1':'2',
								$customer_birthday,
								$_SESSION[cart_souvenir_customer][inp_email],
								$_SESSION[cart_souvenir_customer][inp_tel1_1],
								$_SESSION[cart_souvenir_customer][inp_tel1_2],
								$_SESSION[cart_souvenir_customer][inp_tel1_3],
								$_SESSION[cart_souvenir_customer][inp_mobile1_1],
								$_SESSION[cart_souvenir_customer][inp_mobile1_2],
								$_SESSION[cart_souvenir_customer][inp_mobile1_3],
								$_SESSION[cart_souvenir_customer][type_addr]?2:1,
								$_SESSION[cart_souvenir_customer][inp_addrjp_zip1],
								$_SESSION[cart_souvenir_customer][inp_addrjp_zip2],
								$_SESSION[cart_souvenir_customer][inp_addrjp_pref],
								$_SESSION[cart_souvenir_customer][inp_addrjp_city],
								$_SESSION[cart_souvenir_customer][inp_addrjp_area],
								$_SESSION[cart_souvenir_customer][inp_addrjp_building],
								$_SESSION[cart_souvenir_customer][inp_addrovs1],
								$_SESSION[cart_souvenir_customer][inp_addrovs2],

								$location_rec,
								$_SESSION[cart_souvenir_customer]["reg_hotel_name"],
								$_SESSION[cart_souvenir_customer]["reg_hotel_tel"],
								$_SESSION[cart_souvenir_customer]["reg_hotel_addr"],
								$_SESSION[cart_souvenir_customer]["reg_arrfrm_date"],
								$_SESSION[cart_souvenir_customer]["reg_arrto_date"],
								$_SESSION[cart_souvenir_customer]["reg_arrgo_date"],
								$_SESSION[cart_souvenir_customer]["reg_arrgo_time"],
								$_SESSION[cart_souvenir_customer]["air_no"],
								$_SESSION[cart_souvenir_customer]["sel_info"],
								
								$_SESSION[cart_souvenir_customer][inp_remark],
								
								
								'datetime',
								'0',
								'0',
						
								'0',
								'0');
			
			
			
		}
		else if ($location_rec == 2){
			
			
			$arr_order_tb = array(	'order_id',
								'order_refid',
								'session_status',
								'country_iso3',
								'transection_id',
								'product_amount',
								'product_qty',
								'paid_type', //cash credit
								'paid_status', //affilate
								'order_firstname',
								'order_surname',
								'order_furi_firstname',
								'order_furi_surname',
								'order_sex',
								'order_birthday',
								'order_email',
								'order_tel1',
								'order_tel2',
								'order_tel3',
								'order_mobile1',
								'order_mobile2',
								'order_mobile3',
								'order_addrtype',
								'order_addrjp_zip1',
								'order_addrjp_zip2',
								'order_addrjp_pref',
								'order_addrjp_city',
								'order_addrjp_area',
								'order_addrjp_building',
								'order_addrovs1',
								'order_addrovs2',
								'receive_type',
								'order_rec_firstname',
								'order_rec_surname',
								'order_rec_furi_firstname',
								'order_rec_furi_surname',
								'order_rec_addrjp_zip1',
								'order_rec_addrjp_zip2',
								'order_rec_addrjp_pref',
								'order_rec_addrjp_city',
								'order_rec_addrjp_area',
								'order_rec_addrjp_building',
								'order_rec_tel1',
								'order_rec_tel2',
								'order_rec_tel3',
								'order_remark',
								'order_rec',
								'update_date',
								'update_by',
								'status_cms',
								'update_by_cms',
								'status_cms_paid');
		
		
		$arr_order_val = array($order_id,
								'0',
								'0',
								$country_iso3,
								session_id(),
								$amount_summery,
								$qty_summery,
								$paid_type,
								$status_book,
								/*$store_type,
								$status_book,*/
								$_SESSION[cart_souvenir_customer][inp_firstname],
								$_SESSION[cart_souvenir_customer][inp_surname],
								$_SESSION[cart_souvenir_customer][inp_firstname_furi],
								$_SESSION[cart_souvenir_customer][inp_surname_furi],
								$_SESSION[cart_souvenir_customer][inp_sex]=='男性'?'1':'2',
								$customer_birthday,
								$_SESSION[cart_souvenir_customer][inp_email],
								$_SESSION[cart_souvenir_customer][inp_tel1_1],
								$_SESSION[cart_souvenir_customer][inp_tel1_2],
								$_SESSION[cart_souvenir_customer][inp_tel1_3],
								$_SESSION[cart_souvenir_customer][inp_mobile1_1],
								$_SESSION[cart_souvenir_customer][inp_mobile1_2],
								$_SESSION[cart_souvenir_customer][inp_mobile1_3],
								$_SESSION[cart_souvenir_customer][type_addr]?2:1,
								$_SESSION[cart_souvenir_customer][inp_addrjp_zip1],
								$_SESSION[cart_souvenir_customer][inp_addrjp_zip2],
								$_SESSION[cart_souvenir_customer][inp_addrjp_pref],
								$_SESSION[cart_souvenir_customer][inp_addrjp_city],
								$_SESSION[cart_souvenir_customer][inp_addrjp_area],
								$_SESSION[cart_souvenir_customer][inp_addrjp_building],
								$_SESSION[cart_souvenir_customer][inp_addrovs1],
								$_SESSION[cart_souvenir_customer][inp_addrovs2],
								
								$location_rec,
								
								$_SESSION[cart_souvenir_customer][inprec_firstname],
								$_SESSION[cart_souvenir_customer][inprec_surname],
								
								$_SESSION[cart_souvenir_customer][inprec_firstname_furi],
								$_SESSION[cart_souvenir_customer][inprec_surname_furi],
								
								$_SESSION[cart_souvenir_customer][inprec_addrjp_zip1],
								$_SESSION[cart_souvenir_customer][inprec_addrjp_zip2],
								$_SESSION[cart_souvenir_customer][inprec_addrjp_pref],
								$_SESSION[cart_souvenir_customer][inprec_addrjp_city],
								$_SESSION[cart_souvenir_customer][inprec_addrjp_area],
								$_SESSION[cart_souvenir_customer][inprec_addrjp_building],
								$_SESSION[cart_souvenir_customer][inprec_tel1_1],
								$_SESSION[cart_souvenir_customer][inprec_tel1_2],
								$_SESSION[cart_souvenir_customer][inprec_tel1_3],
								
								
								$_SESSION[cart_souvenir_customer][inp_remark],
								$order_rec,
								
								'datetime',
								'0',
								'0',
						
								'0',
								'0');
			
		}
		
	
		$db->set_insert('mbus_souvenir_order',$arr_order_tb,	$arr_order_val);
		
		
		$arr_order_tb_detail = array('order_id',
									'order_idx',
									'souvenir_id',
									'souvenir_code',
									'souvenir_price',
									'souvenir_qty',
									'souvenir_amount');
		
		
		for ($i = 0; $i< count($arr_input_order_detail); $i++){
		
			$arr_order_tb_detail_val = array($order_id,
											 $arr_input_order_detail[$i][0],
											 $arr_input_order_detail[$i][1],
											 $arr_input_order_detail[$i][2],
											 $arr_input_order_detail[$i][3],
											 $arr_input_order_detail[$i][4],
											 $arr_input_order_detail[$i][5]);
			
			
			
			
			
			
			$db->set_insert('mbus_souvenir_order_detail',$arr_order_tb_detail,	$arr_order_tb_detail_val);
			
			
		}
		
		$db->db_updateid($order_id,'souvenir_order');
		
		$db->db_commit();
		
		
		
		
	$_SESSION[order_id] = $order_id;
	
	
	
							
	/* insert db for cache */
	
	header("Location: souvenir_cart_inp_address_confirm.php"); 
	
}
?>

