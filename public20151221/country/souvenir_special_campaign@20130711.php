<?
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$rate = $db->get_currency_rate($site_country);

$entry_year = Date('Y');
$entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}

require_once($path."include/config.php");


function print_right_price_start_to_campan($sgd,$yen)
{
	if ($yen[affiliate]){
		return '<span class="txt-red-bold-price">\\'.number_format($sgd).'';
	}
	else{
	
		if ($yen[show] == 1)
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).' ( 目安：\\'.number_format( round($yen[rate]*$sgd) ).')</span>';
		}
		else
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'';
		}
	
	}
	 
}



										
$breadcamp = breadcamp(array('TOP',$site_name.'TOP','海外おみやげ'),array('../index.php','index.php'));
										
	
										
			
					
					
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/souvenir_product.php?product_id=';




$smarty = new Smarty;
include("../include/souvenir_country_right_menu.php");

$config[documentroot] = $path;

$smarty->assign("campaign_id",$campaign_id);
$smarty->assign("campaign_tittle",$campaign_tittle);
$smarty->assign("campaign_content",$campaign_content);
$smarty->assign("campaign_list",$product_campaign);

$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);
$smarty->display('country_souvenir_special_campaign.tpl');
?>