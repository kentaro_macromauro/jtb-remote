<? 
$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path."class/phpmailer/class.phpmailer.php");

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

$breadcamp = breadcamp(array('TOP','予約する'),array('index.php'));

require_once($path."include/config.php");
$config['allot'] = $_SESSION['checkout']['allot'];

/* produuct path setting   */
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

include("../include/check_souvenir.php");

function init_togender($init)
{
	switch ($init)
	{
		case 1: $data = '男性'; break;
		case 2: $data = '女性'; break;
		default    : $data = ''; break;
	}
	
	return $data;
}


if ($_REQUEST[page] != 'payment'){
	header( "HTTP/1.1 301 Moved Permanently" ); 
	header('Location: souvenir_cart_inp_address.php');
}
else{


	if (!empty( $_REQUEST[transection] )){
		
		$transection_id = $_REQUEST[transection];
		
		
		
		$sql = 'SELECT a.order_id,b.souvenir_id,b.souvenir_qty,a.paid_status,paid_type FROM mbus_souvenir_order a ,mbus_souvenir_order_detail b 
			    WHERE a.order_id = b.order_id AND md5(a.order_id) = "'.$transection_id.'" ';
				
		
		$result = $db->db_query($sql);
		

		
		$souvenir_product_list = array();
		$souvenir_arr_id	   = array();
		
		while ($record = mysql_fetch_array($result)){
			$order_id 	  = $record[order_id];
			$souvenir_id  = $record[souvenir_id];
			$souvenir_qty = $record[souvenir_qty];
			
			$souvenir_arr_id[] = $souvenir_id;
			
			$souvenir_product_list[$souvenir_id] = $souvenir_qty;
			
			//$paid_status 	  = $record[paid_status];
			$paid_type		  = $record[paid_type];
			
			
		}
		
		
		$sv_list = implode(',',$souvenir_arr_id);
		
		/* update confirm booking */
		
		$sql = 'UPDATE mbus_souvenir_order SET session_status = "1" WHERE md5(order_id) = "'.$transection_id.'" ';
		$db->db_query($sql);
		
		
		/* update souvenir mothly  */
			
		$sql  = 'SELECT a.souvenir_id , IF (b.amount ,b.amount+1, 1) amount FROM 
				(SELECT souvenir_id FROM mbus_souvenir WHERE souvenir_id IN ('.$sv_list.')) AS  a LEFT JOIN 
				(SELECT souvenir_id,amount FROM mbus_monthly_product_souvenir 
				WHERE souvenir_id IN ('.$sv_list.') AND mpro_month = DATE_FORMAT(NOW(),"%c") AND  
				mpro_year = DATE_FORMAT(NOW(),"%Y") ) AS b  ON a.souvenir_id = b.souvenir_id ';
			
		
		$result = $db->db_query($sql);
		
		$souvenir_list_month = array();
			
		while ($record  = mysql_fetch_array($result)){
			$souvenir_list_month[] = array( $record[souvenir_id] , $record[amount] );
		}
				
		$db->db_start();	
				
		$sql =  'DELETE FROM '._DB_PREFIX_TABLE.'monthly_product_souvenir 
				 WHERE souvenir_id IN ('.$sv_list.') AND mpro_month = DATE_FORMAT(NOW(),"%c") AND ';
		$sql .= 'mpro_year = DATE_FORMAT(NOW(),"%Y") '; 	
		
		
		
		$db->db_query($sql);
		
		foreach ($souvenir_list_month as $slm){
			$sql  = 'INSERT INTO '._DB_PREFIX_TABLE.'monthly_product_souvenir(souvenir_id,mpro_year,mpro_month,amount,update_date) ';
			$sql .= 'VALUES ("'.$slm[0].'",DATE_FORMAT(NOW(),"%Y"),DATE_FORMAT(NOW(),"%c"),'.$slm[1].',NOW() ) ';	

			$db->db_query($sql);
			
		}
		
		
		
		if (is_array($souvenir_product_list)){
			
			if  (($paid_type == 1) || ($paid_type == 2)){ 
			
				while ($req_qty = current($souvenir_product_list)){
	
					$sql = 'UPDATE mbus_souvenir SET souvenir_allotment = (souvenir_allotment - '.$req_qty.')
							WHERE souvenir_id = "'.key($souvenir_product_list).'" ';
					
					
					$db->db_query($sql);
	
					next($souvenir_product_list);
				}
			
			}
		}
		
		$db->db_commit();
		
		
		$mail = new PHPMailer();
		
		$mail->Encoding="base64";
		
		/* email message send to customer */
		
		
		$sql = 'SELECT a.order_id ,a.order_refid ,a.axis_refid ,a.session_status ,
				a.mailto_customer ,a.country_iso3 ,a.transection_id ,a.product_amount ,
				a.product_qty ,a.paid_type ,a.paid_status ,a.order_firstname ,a.order_surname ,
				a.order_furi_firstname ,a.order_furi_surname ,a.order_sex ,a.order_birthday ,
				a.order_email ,a.order_tel1 ,a.order_tel2 ,a.order_tel3 ,a.order_mobile1 ,
				a.order_mobile2 ,a.order_mobile3 ,a.order_addrtype ,a.order_addrjp_zip1 ,
				a.order_addrjp_zip2 ,a.order_addrjp_pref ,a.order_addrjp_city ,
				a.order_addrjp_area ,a.order_addrjp_building ,a.order_addrovs1 ,
				a.order_addrovs2 ,a.order_rec_firstname ,a.order_rec_surname ,
				a.order_rec_furi_firstname ,a.order_rec_furi_surname ,
				a.order_rec_addrjp_zip1 ,a.order_rec_addrjp_zip2 ,a.order_rec_addrjp_pref ,
				a.order_rec_addrjp_city ,a.order_rec_addrjp_area ,a.order_rec_addrjp_building ,
				a.order_rec_tel1 ,a.order_rec_tel2 ,
				a.order_rec_tel3 ,a.order_remark ,a.update_date ,a.update_by ,a.status_cms ,
				a.update_date_cms ,a.update_by_cms ,a.status_cms_paid,receive_type,
				acc_name,acc_tel,acc_addr,stay_from,stay_to,arrival_date,arrival_time,flight_no,travel,
				b.order_idx,b.souvenir_id,b.souvenir_code,b.souvenir_price,b.souvenir_qty,b.souvenir_amount ,
				souvenir_name,store_type,short_desc,souvenir_allotment,c.country_name_jp,c.country_name_en,order_rec,sign  
				FROM mbus_souvenir_order a LEFT JOIN 
				mbus_souvenir_order_detail b ON a.order_id = b.order_id LEFT JOIN
				mbus_country c ON a.country_iso3 = c.country_iso3 LEFT JOIN 
				mbus_currency d ON a.country_iso3 =  d.country_iso3  LEFT JOIN 
				mbus_souvenir e ON b.souvenir_id = e.souvenir_id
				WHERE md5(a.order_id) = \''.$transection_id.'\'
				ORDER BY order_refid';
		
		
		
		
		$result = $db->db_query($sql); $i=0;
		
		while ($record  = mysql_fetch_array($result)){
			
			$order_info[$i]  = $record;
			$i++;
		}
		
		
		
		
		/* email paid type subject */
		
		
		
		if ($paid_type==1){ //credit
		
			$mail->Subject = "【受付番号：".$order_info[0][order_id]."】JTB MyBus 予約内容確認 海外おみやげ";
			$mail->CharSet = 'UTF-8'; 
			
			$buffer  .= 'この度はJTB海外おみやげをご注文いただきまして誠にありがとうございます。<br/><br/>';
			$buffer  .= 'ご注文いただきました内容は下記の通りでございます。<br/>';
			$buffer  .= '商品お引渡しまで今しばらくお待ちください。<br/><br/>';
			$buffer  .= '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br/>';	
			
			$footer  = '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br/>';
			$footer .= '●3日以内に当社からの確認のEメールが届かない場合には、何らかの障害が発生した<br/>';
			$footer .= '可能性がありますので、お手数ですが、タイトルに「再送」とご表示の上でご再送いただけますようお願いいたします。<br/>';
			$footer .= 'メールを受信次第、可能な限り速やかにご連絡をいたします。<br/>';
			$footer .= '【※台湾フルーツ日本宅配のご注文は、本メールが受注確認メールとなります。】<br/>';
			
			
		}
		else{ //onrequest cash
		
			$mail->Subject = "【受付番号：".$order_info[0][order_id]."】JTB MyBus お問い合わせ内容確認 海外おみやげ";
			$mail->CharSet = 'UTF-8'; 
			
			$buffer .= 'この度はJTB海外おみやげをご注文いただきまして誠にありがとうございます。<br/><br/>';
						
			$buffer .= 'ご注文いただきました内容は下記の通りでございます。<br/>';
			$buffer .= '商品お引渡しまで今しばらくお待ちください。<br/><br/>';
						
			$buffer .= '【ご注意】<br/>';
			$buffer .= 'この商品のご購入はまだ確定しておりませんので、下記内容を必ずご確認ください。<br/><br/>';
			$buffer .= '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br/>';
			
			$footer = '━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━<br/>';
			$footer .= '●後日ご購入の確定のメールをお送りいたします（現在、ご購入は確定しておりませんのでご注意ください）。<br/>';
			$footer .= '尚、後日お送りするメール内容は、お引渡しの可否、またお引渡しが可能だった場合のお支払い方法・お引渡し方法等のご連絡が目的となります。<br/><br/>';

			$footer .= '●3日以内に当社からの確認のEメールが届かない場合には、何らかの障害が発生した<br/>';
			$footer .= '可能性がありますので、お手数ですが、タイトルに「再送」とご表示の上でご再送いただけますようお願いいたします。<br/>';
			$footer .= 'メールを受信次第、可能な限り速やかにご連絡をいたします。<br/>';
			
		}
		
		
		$footer .= signature_by_country( $order_info[0][country_iso3] );
		$sender =  email_by_country($order_info[0][country_iso3] ) ;
		
				
		
		$buffer .= '<b>■商品情報</b>'.'<br/>';
		
		$buffer .= '<table width="100%" cellspacing="0" cellpadding="1" border="0">';
		$buffer .= '<tr><td nowrap="" style="width:180px;"><b>オーダーID</b></td><td width="98%">'.$order_info[0][order_id].'</td></tr>';
		
		for ($i = 0 ; $i< count($order_info);  $i++) {
			$buffer .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
			$buffer .= '<tr><td>商品名 : </td><td>'.$order_info[$i]['souvenir_name'].'</td></tr>';
			$buffer .= '<tr><td>単価 : </td><td>'.$order_info[0]['sign'].' '.$order_info[$i]['souvenir_price'].'</td></tr>';
			$buffer .= '<tr><td>数量 : </td><td>'.$order_info[$i]['souvenir_qty'].'</td></tr>';
			$buffer .= '<tr><td>小計 : </td><td>'.$order_info[$i]['sign'].' '.$order_info[$i]['souvenir_amount'].'</td></tr>';
			$buffer .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		}
		
		$buffer .= '<tr><td>&nbsp;</td><td>------</td></tr>';
		$buffer .= '<tr><td>合計 : </td><td>'.$order_info[0]['sign'].' '.$order_info[0]['product_amount'].'</td></tr>';
		$buffer .= '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
		$buffer .= '</table><br/>';
		
		/* productName productPricePerUnit productQty productPriceSummery */
		/* cart content*/ 
		 
		/* content customer */
				
		$buffer .= '<b>■購入者情報</b>'.'<br/>';
		$buffer .= '<table width="100%" cellspacing="0" cellpadding="1" border="0">';
		$buffer .= '<tr><td nowrap="" style="width:180px;">購入者名（漢字）：</td><td width="98%">';
		$buffer .=  $order_info[0]['order_surname'].' '.$order_info[0]['order_firstname'].'</td></tr>';
		
		$buffer .= '<tr><td>購入者名（ローマ字）:</td><td>'.$order_info[0]['order_furi_surname'].' '.$order_info[0]['order_furi_firstname'].'</td></tr>';
		$buffer .= '<tr><td>生年月日 : </td><td>'.date_jp($order_info[0]['order_birthday']).'</td></tr>';
		$buffer .= '<tr><td>性別 : </td><td>';
		$buffer .= $order_info[0]['order_sex']?'男性':'女性';
		$buffer .= '</td></tr>';
		$buffer .= '<tr><td>メールアドレス（PC）: </td><td>'.$order_info[0]['order_email'].'</td></tr>';
		$buffer .= '<tr><td colspan="2">住所</td></tr>';
		
		if ( $order_info[0]['order_addrtype'] == 1){
			
			
			$buffer .= '<tr><td>郵便番号 : </td><td>〒'.$order_info[0]['order_addrjp_zip1'].'-'.$order_info[0]['order_addrjp_zip2'].'</td></tr>';
			$buffer .= '<tr><td>都道府県 : </td><td>'.$db->view_pref( $order_info[0]['order_addrjp_pref'] ).'</td></tr>';
			$buffer .= '<tr><td>市区町村 : </td><td>'.$order_info[0]['order_addrjp_city'].'</td></tr>';
			$buffer .= '<tr><td>町域 : </td><td>'.$order_info[0]['order_addrjp_area'].'</td></tr>';
			$buffer .= '<tr><td>建物名部屋番号 : </td><td>'.$order_info[0]['order_addrjp_building'].'</td></tr>';
		}
		else {
			$buffer .= '<tr><td>住所1 : </td><td>'.$order_info[0]['order_addrovs1'].'</td></tr>';
			$buffer .= '<tr><td>住所2 : </td><td>'.$order_info[0]['order_addrovs2'].'</td></tr>';
		}		
		
		
		$buffer .= '<tr><td>連絡先（電話）: </td><td>'.$order_info[0]['order_mobile1'].'-'.$order_info[0]['order_mobile2'].'-'.$order_info[0]['order_mobile3'].'</td></tr>';
		$buffer .= '<tr><td>連絡先（携帯）: </td><td>'.$order_info[0]['order_tel1'].'-'.$order_info[0]['order_tel2'].'-'.$order_info[0]['order_tel3'].'<br/></td></tr></table><br/>';
		
		
		
		/* check recive outsite japan */
		if ( $order_info[0][receive_type] == "1"){
			
			$buffer .= '<b>■滞在先情報</b>'.'<br/>';
			
						
			
			$buffer .= '<table width="100%" cellspacing="0" cellpadding="1" border="0">';
			
			$buffer .= '<tr><td nowrap="" style="width:180px;">滞在先名：</td><td>'.$order_info[0][acc_name].'</td></tr>';
			$buffer .= '<tr><td>滞在先TEL：</td><td>'.$order_info[0][acc_tel].'</td></tr>';
			$buffer .= '<tr><td>滞在先住所 : </td><td>'.$order_info[0][acc_addr].'</td></tr>';
			
			
			$buffer .= '<tr><td>滞在期間：</td><td>'.jp_strdate($order_info[0][stay_from]).' ～ '.jp_strdate($order_info[0][stay_to]).'</td></tr>';
			$buffer .= '<tr><td>到着日フライト：</td><td>'.jp_strdate($order_info[0][arrival_date]).' '.substr($order_info[0][arrival_time],0,5).'</td></tr>';
			
			
			$buffer .= '<tr><td>&nbsp;</td><td>フライトナンバー '.$order_info[0][flight_no].'</td></tr>';
			
			
			$buffer .= '<tr><td>ご旅行形態：</td><td>'.$order_info[0][travel].'</td></tr>';
			$buffer .= '</table><br/>';

			
		}
		else if ($order_info[0][receive_type] == "2"){	
		/* check recive outsite japan */
		
		/* check recive insite japan */
		
		
		$buffer .= '<b>■お届け先情報</b>'.'<br/>';
		$buffer .= '<table width="100%" cellspacing="0" cellpadding="1" border="0">';
		
		if ($order_info[0]['order_rec'] ){
			
			$buffer .= '<tr><td nowrap="" style="width:180px;">お届け先名（漢字）: </td><td>'.$order_info[0]['order_rec_surname'].' '.$order_info[0]['order_rec_firstname'].'</td></tr>';
			$buffer .= '<tr><td >お届け先名（ローマ字）: </td><td>'.$order_info[0]['order_rec_furi_surname'].' '.$order_info[0]['order_rec_furi_firstname'].'</td></tr>';
			
			$buffer .= '<tr><td colspan="2">お届け先住所</td></tr>';
			$buffer .= '<tr><td>郵便番号 :  </td><td>〒'.$order_info[0]['order_rec_addrjp_zip1'].'-'.$order_info[0]['order_rec_addrjp_zip2'].'</td></tr>';
			$buffer .= '<tr><td>都道府県 :  </td><td>'.$db->view_pref( $order_info[0]['order_rec_addrjp_pref'] ).'</td></tr>';
			$buffer .= '<tr><td>市区町村 :  </td><td>'.$order_info[0]['order_rec_addrjp_city'].'</td></tr>';
			$buffer .= '<tr><td>町域 :  </td><td>'.$order_info[0]['order_rec_addrjp_area'].'</td></tr>';
			$buffer .= '<tr><td>建物名部屋番号 :  </td><td>'.$order_info[0]['order_rec_addrjp_building'].'</td></tr>';
			
			
			$buffer .= '<tr><td>お届け先連絡先（電話）</td><td>'.$order_info[0]['order_rec_tel1'].'-'.$order_info[0]['order_rec_tel2'].'-'.$order_info[0]['order_rec_tel3'].'</td></tr></table><br/>';
			
	    }
		else{
			$buffer .= '<tr><td nowrap="" style="width:180px;">お届け先名（漢字）: </td><td>'.$order_info[0]['order_surname'].' '.$order_info[0]['order_firstname'].'</td></tr>';
			$buffer .= '<tr><td>お届け先名（ローマ字）: </td><td>'.$order_info[0]['order_furi_surname'].' '.$order_info[0]['order_furi_firstname'].'</td></tr>';
			$buffer .= '<tr><td colspan="2">お届け先住所</td></tr>';
			
			
			if ($order_info[0]['order_addrtype'] == 1){
				$buffer .= '<tr><td>郵便番号 : </td><td>〒'.$order_info[0]['order_addrjp_zip1'].'-'.$order_info[0]['order_addrjp_zip2'].'</td></tr>';
				$buffer .= '<tr><td>都道府県 : </td><td>'.$db->view_pref( $order_info[0]['order_addrjp_pref'] ).'</td></tr>';
				$buffer .= '<tr><td>市区町村 : </td><td>'.$order_info[0]['order_addrjp_city'].'</td></tr>';
				$buffer .= '<tr><td>町域 : </td><td>'.$order_info[0]['order_addrjp_area'].'</td></tr>';
				$buffer .= '<tr><td>建物名部屋番号 : </td><td>'.$order_info[0]['order_addrjp_building'].'</td></tr>';
			}
			else{
				$buffer .= '<tr><td>住所1 :  </td><td>'.$order_info[0]['order_addrovs1'].'</td></tr>';
				$buffer .= '<tr><td>住所2 :  </td><td>'.$order_info[0]['order_addrovs2'].'</td></tr>';
			}
			
			$buffer .= '<tr><td>お届け先連絡先（電話）</td><td>'.$order_info[0]['order_tel1'].'-'.$order_info[0]['order_tel2'].'-'.$order_info[0]['order_tel3'].'</td></tr></table><br/>';
		}
		
		/* check recive insite japan */
		
		}
		
		
		/* content customer */
		
		
		
		$buffer .= '<b>■その他の情報 </b>'.'<br/>'.nl2br($order_info[0]['order_remark']).'<br/><br/>';
		
		
		$buffer .= $footer;
		
		$mail->MsgHTML('<html><body><div style="width:600px; display:block;">'.$buffer.'</div></body></html>');
		$mail->AddAddress($order_info[0]['order_email']);
		
		

		$mail->AddAddress($sender);
		
		
		$mail->AddBcc('pornpen@auncon.co.jp');
		$mail->AddBcc('jedsadha@auncon.co.jp');
		
		
		//$mail->SetFrom($sender);
		$mail->SetFrom($sender);
		
		
	
		/* content customer */
		
		/* email signature*/
		/* email signature */
		
		/* email message send to customer */
		

		
		if (!$mail->Send())
			$result = $mail->ErrorInfo;
		
		
		
		if (is_array($souvenir_arr_id)){
		
			foreach ($souvenir_arr_id as $su){
				
				$cart_type = $_SESSION['cart_souvenir_checkout']['cart_type'];
				
				unset($_SESSION['cart_souvenir'][$su] );	
			}
			
			unset($_SESSION['cart_souvenir_checkout']);
				
				
			//if ( ($cart_type == 0) || ($cart_type == 2) ){
			if ($paid_type == 1){		
				/* update souvenir mothly end */		
				header( "HTTP/1.1 301 Moved Permanently" ); 
				header('Location: souvenir_thank.php?credit=1');
			}
			else{
				/* update souvenir mothly end */		
				header( "HTTP/1.1 301 Moved Permanently" ); 
				header('Location: souvenir_thank.php');
			}
		
		}
		else{
			header( "HTTP/1.1 301 Moved Permanently" ); 
			header('Location: souvenir_thank.php');
		}
		
		
	}
	else{
		header( "HTTP/1.1 301 Moved Permanently" ); 
		header('Location: souvenir_cart_inp_address.php');
	}
}

	
?>

