<?

if ( $_SERVER["SERVER_PORT"] != 443 ){
    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}


$path = '../';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate($site_country);

require_once($path."include/config.php");
$config['allot'] = 'true';

$breadcamp = breadcamp(array('TOP',$site_name.'TOP','注文する'),array('../index.php','index.php'));

/* produuct path setting   */
$img  = '../product/images/souvenir/';
$link = '../'.$countryname.'/product.php?product_id=';
/* produuct path setting   */

/* 
concept and flowchart

product can sale---------->receive on spot------->credit
			|				|
			|				--------------------->cash 
			|		
			-------------->receive in japan------>credit
							|
							--------------------->cash
					
on request product--------->recive on spot------->credit
			|				|
			|				--------------------->cash
			|		
			--------------->recive in japan------>credit
							|
							--------------------->cash
*/

function update_souvenir_cart(){
	$inp_qty = array();
	$inp_qty = $_REQUEST[inp_qty];
		
	if (is_array($inp_qty)){
					
		while ($obj = current($inp_qty)){
			$_SESSION[cart_souvenir][key($inp_qty)] = $obj;
			next($inp_qty);
		}
	}		
}

function get_souvenir_cart_list_id(){
	$inp_qty = $_REQUEST[inp_qty];
	$array_souvenir_list = array();
		
	if (is_array($inp_qty)){
					
		while ($obj = current($inp_qty)){//while0
			$array_souvenir_list[ key($inp_qty)] = $obj;
			
			next($inp_qty);
		}
	}	
	
	return $array_souvenir_list;
}

/* get all items */

/* update cart */
if ($_REQUEST[cart_update]){
	update_souvenir_cart();	
}
/* update cart */


$cart_souvenir = $_SESSION[cart_souvenir];

if (is_array($cart_souvenir)){//if cart have item
	
	
	/* check out */
	if ($_REQUEST[cart_checkout]){
		
		update_souvenir_cart();
		
		$cart_type = $_POST[cart_type];
		
		$souvenir_lsit = get_souvenir_cart_list_id();
		$_SESSION['cart_souvenir_checkout'] = array('cart_type' => $cart_type ,'souvenir_list' => $souvenir_lsit );

		header("Location: souvenir_cart_inp_address.php");	
	}
	/* check out */
	
	
	
	/* remove cart */
	$all_req = $_REQUEST;
	
	while ($req = current($all_req)){
		
		if (strpos( key($all_req),'cart_remove_item_') !== false ){
			
			$match = '';
			
			preg_match('/^cart_remove_item_(.*$)/',   key($all_req) , $match);
			
			$item_remove  =  $match[1];
						
			if ($cart_souvenir[$item_remove]){
				
				unset($cart_souvenir[$item_remove]);
				unset($_SESSION['cart_souvenir'][$item_remove]);
			}
		}
		
		next($all_req);
	}
	/* remove cart */
	
	
	
	
	
	$cart_items = array();
	$souvenir_id_array = array();

	while ($obj = current($cart_souvenir)){//while1
		$cart_items[]  =  array( 'id' 	 => key($cart_souvenir),  
								 'value' => $obj);
		
		$souvenir_id_array[] = key($cart_souvenir);
		next($cart_souvenir);
	}//while1
	
	$souvenir_id_string = implode(',',$souvenir_id_array);
	
	/* cart items = get all data on session */
	
	$sql = 'SELECT a.souvenir_id, 
				a.country_iso3, country_name_en,country_name_jp,
				rate,sign,code,souvenir_theme,
				souvenir_code,souvenir_name,
				store_type,
				souvenir_price,souvenir_allotment,
				short_desc, public, 
				status_book,affiliate_code,
				affiliate,affiliate_url,remark,
				b.text_top,b.text_table,b.text_bottom ,
				IF ( souvenir_allotment > 0 , \'1\',\'0\' ) AS sale_status  
				FROM mbus_souvenir a 
				LEFT JOIN mbus_souvenir_detail b ON a.souvenir_id = b.souvenir_id 
				LEFT JOIN mbus_country c ON a.country_iso3 = c.country_iso3 
				LEFT JOIN mbus_currency d ON a.country_iso3 = d.country_iso3 
				WHERE a.souvenir_id IN ('.$souvenir_id_string.') AND public = 1 AND affiliate = 0 AND 
				a.country_iso3 = \''.$site_country.'\' 
				ORDER BY souvenir_name';
	
	$result = $db->db_query($sql);

	$items_on_cart = array();
	
	for ($i=1;$i<=30;$i++){ $arr_num[] = $i;} //gen i for input selectbox

	while ($record = mysql_fetch_array($result)){//while2
		
		$souvenir_allotment = $record[souvenir_allotment];
		$souvenir_id		= $record[souvenir_id];
		$souvenir_allotment = $record[souvenir_allotment];
		$store_type         = $record[store_type];  // recive : 1 == recive on spot 2 = recive in japan
		$status_book		= $record[status_book]; // paid type : 1 = credit 2 = cash 
		$price				= $record[souvenir_price];
		foreach ($cart_items as $items){ /* forech1 */
			if ($items[id] == $souvenir_id){ /* id matching */
				/* product can sale */
		
				
				if ( $souvenir_allotment >= $items[value]){
					
					if ($store_type == 1){ /* recive on spot */
						if ($status_book == 1){/*paid type credit */
							$items_on_cart[0][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
												'price' => number_format($price,(intval($price)==$price)?0:2),
			 	'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
												'amount' => number_format($items[value] * $price ,(intval($price)==$price)?0:2), 
											    'detail' => $record );
						}/*paid type cash */
						else if ($status_book == 2) {/*paid type cash */
							$items_on_cart[1][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
														 'price' => number_format($price,(intval($price)==$price)?0:2),
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),																	
														 'amount' => number_format($items[value] *  $price,(intval($price)==$price)?0:2), 
														 'detail' => $record );
						}/*paid type credit */ 
					}/* recive on spot */
					else if ($store_type == 2){/* recive in japan */
						if ($status_book == 1){/*paid type credit */
							$items_on_cart[2][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
														 'price' => number_format($price,(intval($price)==$price)?0:2), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' => number_format($items[value] *  $price,(intval($price)==$price)?0:2), 
														 'detail' => $record );
							
							
						}/*paid type cash */
						else if ($status_book == 2) {/*paid type cash */
							$items_on_cart[3][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
														'price' => number_format($price,(intval($price)==$price)?0:2), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' => number_format($items[value] * $price,(intval($price)==$price)?0:2), 
														 'detail' => $record );
						}/*paid type credit */
					}/* recive in japan */
				}/* product can sale */
				else{ /* product on request */
					if ($store_type == 1){/* recive on spot */
						
						$items_on_cart[4][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
													 'price' => number_format($price,(intval($price)==$price)?0:2), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price,(intval($price)==$price)?0:2), 
														 'detail' => $record );
						
						/*if ($status_book == 1){/*paid type credit */
							/*$items_on_cart[4][] = array( 'id' => $items[id] , 'qty' => $items[value] , 'price' => number_format($price), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price), 
														 'detail' => $record );
						}/*paid type cash */
						/*else if ($status_book == 2) {/*paid type cash */
							/*$items_on_cart[5][] = array( 'id' => $items[id] , 'qty' => $items[value] , 'price' => number_format($price),
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price), 
														 'detail' => $record );
						}/*paid type credit */
						
						
						
						
					}/* recive on spot */
					else if ($store_type == 2){/* recive in japan*/
						
							$items_on_cart[5][] = array( 'id' => $items[id] , 'qty' => $items[value] , 
														 'price' => number_format($price,(intval($price)==$price)?0:2), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price,(intval($price)==$price)?0:2), 
														 'detail' => $record );
						
						/*						
						if ($status_book == 1){/*paid type credit */
							/*$items_on_cart[6][] = array( 'id' => $items[id] , 'qty' => $items[value] , 'price' => number_format($price), 
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price), 
														 'detail' => $record );
						}/*paid type cash */
						/*else if ($status_book == 2) {/*paid type cash */
							/*$items_on_cart[7][] = array( 'id' => $items[id] , 'qty' => $items[value] ,  'price' => number_format($price),
				'inp_qty' => input_selectbox_nonstart('inp_qty['.$items[id].']',$arr_num,$arr_num, $items[value] ,'search-left-selectbox w50 top'),
														 'amount' =>  number_format($items[value] *  $price), 
														 'detail' => $record );
						}/*paid type credit */
						
						
					}/* recive in japan*/
				
				}/* product on request */
			}/* id matching */
		} /* foreach1 */
	}//while2
	
	
	
	
	for ($i = 0; $i < 8 ; $i++){

		$cart_amount[$i] = 0 ;
		//$cart_qty[$i]	 = 0 ;
		
		if (is_array($items_on_cart[$i])){
			
			foreach ($items_on_cart[$i] as $items_c){
				
				$cart_amount[$i] +=  $items_c[qty] * $items_c[detail][souvenir_price] ;
				//$cart_qty[$i]	 +=  $items_c[qty];
			}
		}
		$cart_amount[$i] = number_format($cart_amount[$i],(intval($cart_amount[$i])==$cart_amount[$i])?0:2);
	}
	
}//if cart have item

/* setting */
include("../include/souvenir_country_right_menu.php");
$config[documentroot] = '../';
$smarty->assign("config",$config);
/* setting */


/* item on cart */
$smarty->assign("items_on_cart",$items_on_cart);
$smarty->assign("cart_amount",$cart_amount);
//$smarty->assign("cart_qty",$cart_qty);
/* item on cart */
$smarty->assign('currency',$currency[0]);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);


$smarty->display('souvenir_booking2_cart.tpl');
?>