// JavaScript Document



var monthNames = new Array('1月','2月','3月','4月','5月','6月',
						   '7月','8月','9月','10月','11月','12月');
						   
var dayInWeekNames = new Array('日','月','火','水','木','金','土');
					
var _prefix = 1;

var _code = 0;
	
function get_DaysInMonth(_year, _month) {
    return 32 - new Date(_year, _month, 32).getDate();
}

function get_MonthName(_month)
{
    return monthNames[_month];
}

function get_DayName(_day)
{
    return dayInWeekNames[_day];
}

function get_FirstDayofMonth(_year,_month)
{	
	return new Date(_year,_month,0).getDay();
}

function get_NumDayofWeek(_year,_month,_day)
{
	return new Date(_year,_month,_day).getDay();
}

function get_listMonthMenu(_month)
{
	buffer = '<select name="cal_menu'+_prefix+'" id="id_cal_menu'+_prefix+'" >';

	for (i = 0;i<= 11 ;i++)
	{
		if (_month == i)
		{
			buffer = buffer+'<option value="'+i+'" selected="selected" >'+get_MonthName(i)+'</option>';	
		}
		else 
		{
			buffer = buffer+'<option value="'+i+'">'+get_MonthName(i)+'</option>';
		}
	}
	
	buffer = buffer+'</select>';
	return buffer;
}

function get_listYearMenu(_year)
{
	buffer = '<select name="cal_menuY'+_prefix+'" id="id_cal_menuY'+_prefix+'" >';

	var yearTmpStart = _year;
    var yearTmpEnd   = _year+1;
	
	for (i = yearTmpStart;i<= yearTmpEnd ;i++)
	{
		if (_year == i)
		{
			buffer = buffer+'<option value="'+i+'" selected="selected" >'+i+'年</option>';	
		}
		else 
		{
			buffer = buffer+'<option value="'+i+'">'+i+'年</option>';
		}
	}
	
	buffer = buffer+'</select>';
	return buffer;
}
	

function get_calendar_detail(_year,_month)
{
	var numDayInMonth = get_DaysInMonth(_year,_month);
	var numFirstDayofMonth = get_FirstDayofMonth(_year,_month);
	var buffer = '';
	
	var _now_date = new Date();
	
	buffer = buffer+'<table width="100%" border="0" cellpadding="0" cellspacing="1" ><tr>';	
		
	for (i=0;i<=6;i++)
	{
		buffer = buffer+'<th>'+get_DayName(i) +'</th>';
	}
	
	buffer = buffer+"</tr><tr>";
	
	
	if (numFirstDayofMonth != 6)
	{		
		for (i=0;i<= numFirstDayofMonth;i++)
		{
			buffer = buffer+'<td></td>';
		}
	}	
		
	for (i=0 ; i< numDayInMonth ; i++) 
	{
	
		if (i != 0)
		{
			if (get_NumDayofWeek(_year,_month,i) == 6)
			{
				buffer = buffer+'</tr><tr>';
			}	
		}
			
			
		if (((i+1) == _now_date.getDate())&&(_month==_now_date.getMonth())&&(_year==_now_date.getFullYear()))
		{	
			buffer = buffer+'<td class="cal_date now_date" onclick="f_getCalResult('+i+')" >'+(i+1)+'</td>';
			
	
		}
		else
		{
			if ( _year<_now_date.getFullYear())
			{
				buffer = buffer+'<td class="befor_date">'+(i+1)+'</td>';	
			}
			else
			{
				
				
				
				if ( (_month<_now_date.getMonth()) )
				{
					if  ( _year > _now_date.getFullYear())
					{
						buffer = buffer+'<td class="cal_date" onclick="f_getCalResult('+i+')" >'+(i+1)+'</td>';
						
					}
					else
					{
						buffer = buffer+'<td class="befor_date">'+(i+1)+'</td>';
					}
				}
				else
				{
					
					
					
					if ((i+1) < _now_date.getDate()) 
					{
						if  ( _year > _now_date.getFullYear())
						{
							
							buffer = buffer+'<td class="cal_date" onclick="f_getCalResult('+i+')" >'+(i+1)+'</td>';
						}
						else
						{
							if (_month > _now_date.getMonth())
							{
								buffer = buffer+'<td class="cal_date" onclick="f_getCalResult('+i+')" >'+(i+1)+'</td>';
								
							}
							else
							{
								buffer = buffer+'<td class="befor_date">'+(i+1)+'</td>';
							}
						}
					}
					else
					{
						buffer = buffer+'<td class="cal_date" onclick="f_getCalResult('+i+')" >'+(i+1)+'</td>';
					}
					
				}
				
			}

		}
	}
				
	for (i = get_NumDayofWeek(_year,_month,i) ;i < 6;i++)
	{
		buffer = buffer+'<td></td>';
	}
		
	buffer = buffer+'</td></tr></table>';
	
	return buffer;
}
	
	
function get_calendar(_year,_month)	
{		
	var buffer = '<table id="id_calendar_mas">';
	
	buffer = buffer+'<tr><td>'+get_listYearMenu(_year)+' '+get_listMonthMenu(_month)+'</td></tr>';
	
	buffer = buffer+'<tr><td id="id_calendar_sub'+_prefix+'">';
		
	buffer = buffer+get_calendar_detail(_year,_month);
	
	buffer = buffer+'</td></tr></table>';

	return buffer;
}
	

	
$(document).ready(
	function(){	
		
		var _now_date = new Date();
		
	
		$('#id_cal').click(
			function()
			{
				if (_code == 0)
				{
					
								
					$('#id_cal').append('<div id="id_calbox1"></div>');
				
					$('#id_calbox1').hide();
					
					document.getElementById("id_calbox1").innerHTML = get_calendar(_now_date.getFullYear(),(_now_date.getMonth()));
					
					$('#id_calbox1').fadeIn("fast");
					
					_code = 1;

				}

				
				$('#id_cal_menu'+_prefix+',#id_cal_menuY'+_prefix).change(
					function()	
					{
						document.getElementById('id_calendar_sub'+_prefix).innerHTML = get_calendar_detail(
							document.getElementById('id_cal_menuY'+_prefix).value,
							document.getElementById('id_cal_menu'+_prefix).value
						);		
					}	

				);
				
				
				
				
				
		$(document).bind('click', function(e){
						var $clicked = $(e.target);
						if (!($clicked.is('#id_calbox1,#id_cal') || $clicked.parents().is
		('#id_calbox1,#id_cal'))) {
							
							$('#id_calbox1').fadeOut(100, 
							function()
							{
								$("#id_calbox1").remove();
								$('#id_temp_body').remove();
								
								_code = 0;
							}							
							);	
							
						}
						else {
							// click inside, to what you want, or nothing :)
						}
				});
				
				
			}
		);			
	}
);	



function get_zero(_value)
{	
	_value++;
	
	if ((_value) < 10)
	{
		_value = '0'+(_value);
	}
	return _value ; 
}

function f_getCalResult(_value)
{
	_dd = get_zero(_value);

	_mm = get_zero(document.getElementById('id_cal_menu'+_prefix).value);
	_yy = document.getElementById('id_cal_menuY'+_prefix).value; 
	
	
	$('select[name$=inp_yearmonth] option:selected').removeAttr('selected');
	
	
	value_month =   document.getElementById('id_cal_menu'+_prefix).value ;
	value_month++;
	value_year_month = _yy+'-'+ value_month ;
	
	value_day  =  _value;
	value_day++;
	
	
	$('select[name$=inp_yearmonth] option[value='+value_year_month+']').attr('selected','selected');       
	
	show_inp_day(value_year_month);
	
	
	$('select[name$=inp_day] option:selected').removeAttr('selected');
	$('select[name$=inp_day] option[value='+value_day+']').attr('selected','selected'); 

	
	$('#id_calbox1').fadeOut(100, 
		function()
		{
			$("#id_calbox1").remove();
			$('#id_temp_body').remove();
			_code = 0;
		}							
	);
}

