/*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2013 @ElmahdiMahmoud
	license: http://www.opensource.org/licenses/mit-license.php
*/ 

$(".dropdown-checkbox dt a").on('click', function () {
          $(".dropdown-checkbox dd ul").slideToggle('fast');
      });

      $(".dropdown-checkbox dd ul li a").on('click', function () {
          $(".dropdown-checkbox dd ul").hide();
      });

      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown-checkbox")) $(".dropdown-checkbox dd ul").hide();
      });


      $('.mutliSelect input[type="checkbox"]').on('click', function () {
        
          var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
              title = $(this).val() + ",";
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel').append(html);
              $(".hida").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida");
              $('.dropdown-checkbox dt a').append(ret);
              
          }
      });

//Batas 1	  

$(".dropdown-checkbox2 dt a").on('click', function () {
          $(".dropdown-checkbox2 dd ul").slideToggle('fast');
      });

      $(".dropdown-checkbox2 dd ul li a").on('click', function () {
          $(".dropdown-checkbox2 dd ul").hide();
      });

      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown-checkbox2")) $(".dropdown-checkbox2 dd ul").hide();
      });


      $('.mutliSelect2 input[type="checkbox"]').on('click', function () {
        
          var title = $(this).closest('.mutliSelect2').find('input[type="checkbox"]').val(),
              title = $(this).val() + ",";
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel2').append(html);
              $(".hida2").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida2");
              $('.dropdown-checkbox2 dt a').append(ret);
              
          }
      });

//Batas 2
$(".dropdown-checkbox3 dt a").on('click', function () {
          $(".dropdown-checkbox3 dd ul").slideToggle('fast');
      });

      $(".dropdown-checkbox3 dd ul li a").on('click', function () {
          $(".dropdown-checkbox3 dd ul").hide();
      });

      function getSelectedValue(id) {
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown-checkbox3")) $(".dropdown-checkbox3 dd ul").hide();
      });


      $('.mutliSelect3 input[type="checkbox"]').on('click', function () {
        
          var title = $(this).closest('.mutliSelect3').find('input[type="checkbox"]').val(),
              title = $(this).val() + ",";
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel3').append(html);
              $(".hida3").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida3");
              $('.dropdown-checkbox3 dt a').append(ret);
              
          }
      });
