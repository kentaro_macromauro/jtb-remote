// JavaScript Document
$(document).ready(
	function()
	{
		$('#campaign_tab1').click(
			function()
			{
				if (!$(this).attr('.active'))
				{
					$('#campaign_tab1').addClass('active');
					$('#campaign_tab2').removeClass('active');
					
					$('.campaign_list2').hide();
					$('.campaign_list1').show();
					
				}
			}
		);
		
		$('#campaign_tab2').click(
			function()
			{
				if (!$(this).attr('.active'))
				{
					$('#campaign_tab2').addClass('active');
					$('#campaign_tab1').removeClass('active');
					
					$('.campaign_list1').hide();
					$('.campaign_list2').show();
					
				}
			}
		);
		
		
		$('select[name$="inp_country"].top').change(
			function()
			{
				$.post('include/ajax_view_country.php',{ value : $(this).val()  },
				function(data) {
                    var json_data = JSON.parse(data);
                    $('.select_city').html(json_data.city);
                    $('.select_category').html(json_data.category);
                    $('.select_option').html(json_data.option);
                    $('.select_time').html(json_data.time);
				});
			}
		);

		$('select[name$="inp_country"]').change(
			function()
			{

				$.post('../include/ajax_view_country.php',{ value : $(this).val()  },
				function(data) {
                    var json_data = JSON.parse(data);
                    $('.select_city').html(json_data.city);
                    $('.select_category').html(json_data.category);
                    $('.select_option').html(json_data.option);
                    $('.select_time').html(json_data.time);
				});	
			}
		);
		
		$('select[name$="order"]').change(
			function()
			{
				document.frmSearch.submit();
			}
		);
		
		
		$('select[name$="inp_yearmonth"]').change(
			function()
			{
				show_inp_day($(this).val());
			}
		);
	}
)
function search_check1(){
	if(document.frmSearch1.inp_country.value == 0){
		window.alert('国を選択してください');
        return false;
	}
    return true;
}
function search_check2(){
	if(document.frmSearch2.inp_country.value == 0){
		window.alert('国を選択してください');
        return false;
	}
    return true;
}


function show_inp_day(_value)
{
	var _now_date = new Date();
	var _selday =  '<option selected="selected">日</option>';
				
	if (_value != '0')
	{
		var _datayear  = _value.substr(0,4) 
			
		var _datamonth = _value.substr(5,2)-1;	
					
		var DaysInMonth=[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			
		var LeapYear = false;
					
		var _day = DaysInMonth[_datamonth];
					
		if ( _datamonth == 1)
		{
			if ((_datayear%4)==0) 
			{
				if ((_datayear%100==0) && (_datayear%400)!=0) 
				{
					_day = DaysInMonth[_datamonth];
				}
				else 
				{
					_day = 29;
				}
			}
			else 
			{
				_day = DaysInMonth[_datamonth];
			}
						
		}
					
		for (_i = 1 ; _i <= _day ; _i++)
		{
			
			
			/*if ( ( _datamonth <_now_date.getMonth()) )
			{
				
				if  ( _datayear > _now_date.getFullYear())	
				{
					if (_i < 10)
					{
						_selday += '<option value="'+_i+'">0'+_i+'日</option>';
					}
					else
					{
						_selday += '<option value="'+_i+'">'+_i+'日</option>';
					}
				}
				else
				{
					if (_i < 10)
					{
						_selday += '<option value="'+_i+'">0'+_i+'日</option>';
					}
					else
					{
						_selday += '<option value="'+_i+'">'+_i+'日</option>';
					}
				}
				
			
			
			}
			else
			{
					
				if (_i < 10)
				{
					_selday += '<option value="'+_i+'">0'+_i+'日</option>';
				}
				else
				{
					_selday += '<option value="'+_i+'">'+_i+'日</option>';
				}
			
			}*/
			
			if (_i < 10)
			{
				_selday += '<option value="'+_i+'">0'+_i+'日</option>';
			}
			else
			{
				_selday += '<option value="'+_i+'">'+_i+'日</option>';
			}
			
		}
					
	}
				
	 $('select[name$="inp_day"]').html(_selday);
}
