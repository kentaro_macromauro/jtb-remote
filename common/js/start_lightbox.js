$(document).ready(
	function()
	{
		$('ul.product_review_gallery a').lightBox();	
		
		$('ul.product_review_gallery a').click(
			function()
			{
				$('#navtop-menu2').hide();	
				$('#navtop-menu3').hide();		
				$('#navtop-menu5').hide();	
				
				$('.navigation-bottom').hide();
			}
		);
		
		$('#jquery-overlay,#lightbox-secNav-btnClose').click(
			function()
			{
				$('#navtop-menu2').show();	
				$('#navtop-menu3').show();		
				$('#navtop-menu5').show();
				
				$('.navigation-bottom').show();
			}
		);
	}
);