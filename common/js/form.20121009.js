// JavaScript Document
$(document).ready(
	function()
	{
		
		$('.obj_select_datepicker select').change(
			function()
			{
				if($('input[value$=Next]').length)	
				{
					$('input[value$=Next]').parent().append('*Please click on "Confirmation" before select quantity.');
					
					$('input[value$=Next]').remove();
				}
			}
		);
		
		
		/* check product avalible */
		
		/*$('input[name$=confimation]').click(
			function()
			{
				
				$.post('../public/country/check_availability.php',{ product_id : $('input[name$=product_id]').val(),
																	inp_date  :  $('select[name$=inp_yearmonth]').val()+'-'+ $('select[name$=inp_day]').val(),
																	pack_adult : $('select[name$=inp_adult]').val() ,
																	pack_child : $('select[name$=inp_child]').val()},
				function(data) {
						
						if (data == "yes")
						{
							$('span.conf-text').html('Availiable.');
						}
						else
						{
							$('span.conf-text').html('Not Availiable.');
						}
				});	
				
			}
		);*/
		/* check product avalible */
		
		
		
		/*--form control--*/
		$('.toindex').click(
			function()
			{
				document.location.href = "index.php";	
			}
		);
		
		
		$('.gotoback').click(
			function()
			{
				$('form[name$=gotoback]').submit();
			}
		);
		
		$('.gotonext').click(
			function()
			{	
				
				$('form[name$=gotonext]').submit();
			}
		);
		
		
		$('.gotopay').click(
							
			function()
			{
			
					
				if ( $('input[name$=paid_type]:checked').val() == '1')
				{
					document.card.submit();	
				}
				else if ( $('input[name$=paid_type]:checked').val() == '2')
				{
					document.none_card.submit();
				}
				else
				{
					document.location.href = "booking_payment.php?msg=error";
				}
				
			}
		);
		
		/*--form control--*/
		
		
		
		
		_rate   = ( $('input[name$=currency_rate]').val());
		/*---form step1---*/
		$('select[name$="inp_adult"]').change(
			function()
			{
				
				_adultprice = $(this).parent().find('input[name$=inp_price_adult]').val() * $(this).val();
				
				
				if (  $(this).parent().find('input[name$=inp_price_adult]').val()  == 1)
				{
					_adultprice = 0;
					
				}
				
				$(this).parent().find('span.country').html(  $('input[name$="curcode"]').val()+" "+addCommas( Math.round(_adultprice) ));	
				$(this).parent().find('span.yen').html('（約 '+addCommas( Math.round(_adultprice*_rate) )+' 円）');	
				
				if (  _adultprice == 0  )
				{
					$(this).parent().find('span.country').html( "-" );	
					$(this).parent().find('span.yen').html('');	
				}
				
				$('#price_amount span.sgd').html( $('input[name$="curcode"]').val() +addCommas(booking_price_sum() ) );
				$('#price_amount span.yen').html( '（約 '+addCommas( Math.round(booking_price_sum() *_rate) )+' 円）' );
				
				
				if (booking_price_sum()==0)
				{
					$('#price_amount span.sgd').html( '-');
					$('#price_amount span.yen').html( '');
				}
				
			}
		);
		
		$('select[name$="inp_child"]').change(
			function()
			{
				
				_child1_price = $(this).parent().find('input[name$=inp_price_child]').val() * $(this).val();
				
				if ($(this).parent().find('input[name$=inp_price_child]').val()  == 1)
				{
					_child1_price = 0;
				}
				
				
				$(this).parent().find('span.country').html( $('input[name$="curcode"]').val()+" "+addCommas( Math.round(_child1_price) ));
				$(this).parent().find('span.yen').html('（約 '+addCommas( Math.round(_child1_price*_rate) )+' 円）');	
				
				
				if (  _child1_price == 0  )
				{
					$(this).parent().find('span.country').html("-");
					$(this).parent().find('span.yen').html('');	
				}
				
				
				$('#price_amount span.sgd').html( $('input[name$="curcode"]').val() +addCommas(booking_price_sum() ) );
				$('#price_amount span.yen').html( '（約 '+addCommas( Math.round(booking_price_sum() *_rate) )+' 円）' );
				
				if (booking_price_sum()==0)
				{
					$('#price_amount span.sgd').html( '-');
					$('#price_amount span.yen').html( '');
				}
			}
		);
		
		$('select[name$="inp_infant"]').change(
			function()
			{
				
				_child2_price = $(this).parent().find('input[name$=inp_price_infant]').val() * $(this).val();
					
				
				if ( $(this).parent().find('input[name$="inp_price_infant"]').val() == 1)
				{
					_child2_price = 0;
				}
				
				$(this).parent().find('span.country').html( $('input[name$="curcode"]').val() +" "+ addCommas( Math.round(_child2_price*_rate) ));
				$(this).parent().find('span.yen').html(addCommas( Math.round(_child2_price*_rate) ));	
				
				if (  _child2_price == 0  )
				{
					$(this).parent().find('span.country').html( "-");
					$(this).parent().find('span.yen').html('');	
				}
				
				
				$('#price_amount span.sgd').html( $('input[name$="curcode"]').val() +addCommas(booking_price_sum() ) );
				$('#price_amount span.yen').html( '（約 '+addCommas( Math.round(booking_price_sum() *_rate) ) +' 円）');
				
				if (booking_price_sum()==0)
				{
					$('#price_amount span.sgd').html( '-');
					$('#price_amount span.yen').html( '');
				}
			}
		);	
		/*---form step2---*/
		
		
		
		/*---form step3---*/
		
		
		if ( $('input[name$="reg_hotel_select"]').attr('checked') == true )
		{
		
			$('.hotel_auto').hide();
			$('.hotel_manual').show();
		}
		else
		{
			$('.hotel_auto').show();
			$('.hotel_manual').hide();
		}
		
		
		$('input[name$="reg_hotel_select"]').change(
			function()
			{
				if ( $(this).attr('checked') == true )
				{
					$('.hotel_auto').hide();
					$('.hotel_manual').show();
				}
				else
				{
					$('.hotel_auto').show();
					$('.hotel_manual').hide();
				}
			}
		);
		
	
		
		if ( $('input[name$="frm_step3_checkview"]').attr('checked') == false	)
		{
			$('.form3-content').find('input,select,textarea').attr('disabled','disabled') ;
			$('.form3-content').find('input[type$="button"]').attr('disabled','') ;
		}
		else
		{
			
		}

		
		
		if ( $('input[name$="type_addr"]').attr('checked') == true)
		{
			$('.regis_address_local').hide();
			$('.regis_address_oversea').show();
		}
		else
		{
			$('.regis_address_local').show();
			$('.regis_address_oversea').hide();
		}

		$('input[name$="type_addr"]').change(
			function()
			{
				if ( $(this).attr('checked') == true)
				{
					$('.regis_address_local').hide();
					$('.regis_address_oversea').show();
				}
				else
				{
					$('.regis_address_local').show();
					$('.regis_address_oversea').hide();
				}
			}
		);
		
		
		$('input[name$="frm_step3_checkview"]').change(
			function()
			{
				if ( $(this).attr('checked') == true	)
				{
					//$('.form3-content').show();
					$('.form3-content').find('input,select,textarea').attr('disabled','') ;
					$('.form3-content').find('input[type$="button"]').attr('disabled','') ;

				}
				else
				{
					//$('.form3-content').hide();
					$('.form3-content').find('input,select,textarea').attr('disabled','disabled') ;
				}
			}
		);
		/*---form step3---*/
	}
);

function booking_price_sum()
{
	_price_adult  = $('input[name$=inp_price_adult]').val() ;
	_price_child  = $('input[name$=inp_price_child]').val() ;
	_price_infant = $('input[name$=inp_price_infant]').val() ;
	
	if (_price_adult == 1)
	{
		_price_adult = 0;
	}
	
	if (_price_child == 1)
	{
		_price_child = 0;	
	}
	
	if (_price_infant == 1)
	{
		_price_infant = 0;	
	}
	
	_amount = (  Math.round( _price_adult * $('select[name$="inp_adult"]').val()) ) +  
			  (  Math.round( _price_child *  $('select[name$="inp_child"]').val()) ) + 
			  (  Math.round( _price_infant * $('select[name$="inp_infant"]').val()) )
			  
	return _amount;
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}