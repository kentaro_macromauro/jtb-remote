// JavaScript Document
function slider_right2()
{
	if (_state2 ==0)
	{
		_state2 =1;
		
		_copy = $('.slider-content-index2 li:first').html();
				$('.slider-content-index2').append('<li class="slider-list">'+_copy+'</li>');
				
				$('.slider-content-index2').animate({'left': '-='+_per_frame1+'px'},_slider_speed1,
				function()
				{

					
					
					$('.slider-content-index2 li:first').remove();
									
					$('.slider-content-index2').css({'left':'0px'});
					_state2 =0;
				}
				);		
	}
}


function slider_right()
{
	if (_state1 ==0)
	{
		_state1 =1;
		
		_copy = $('.slider-content-index li:first').html();
				$('.slider-content-index').append('<li class="slider-list">'+_copy+'</li>');
				
				$('.slider-content-index').animate({'left': '-='+_per_frame1+'px'},_slider_speed1,
				function()
				{

					
					
					$('.slider-content-index li:first').remove();
									
					$('.slider-content-index').css({'left':'0px'});
					_state1 =0;
				}
				);		
	}
}

function slider_left2()
{
	if (_state2 ==0)
	{
		_state2 =1;
		_copy = $('.slider-content-index2 li:last').html();
				$('.slider-content-index2').prepend('<li class="slider-list">'+_copy+'</li>');
				$('.slider-content-index2').css({'left':'-'+_per_frame1+'px'});
				
				$('.slider-content-index2').animate({'left': '+='+_per_frame1+'px'},_slider_speed1,
				function()
				{

					
					_state2 =0;
					$('.slider-content-index2 li:last').remove();
									
					$('.slider-content-index2').css({'left':'0px'});
				}
				);	
	}
}

function slider_left()
{
	if (_state1 ==0)
	{
		_state1 =1;
		_copy = $('.slider-content-index li:last').html();
				$('.slider-content-index').prepend('<li class="slider-list">'+_copy+'</li>');
				$('.slider-content-index').css({'left':'-'+_per_frame1+'px'});
				
				$('.slider-content-index').animate({'left': '+='+_per_frame1+'px'},_slider_speed1,
				function()
				{

					
					_state1 =0;
					$('.slider-content-index li:last').remove();
									
					$('.slider-content-index').css({'left':'0px'});
				}
				);	
	}
}

$(document).ready(
	function()
	{
		_per_frame1 = 177;
		_slider_speed1 = 700;
		_state1  = 0;
		_state2 = 0;
		
		_homeslider_timer = 5000;
		
		
		_element_width = $('.slider-content-index li').width();
		_element_lenth =  $('.slider-content-index li').length;
				
		_slider_width  = (_element_lenth * _element_width);
		$('.slider-content-index').css({'width':_slider_width+'px'});
		
		
		_element_width = $('.slider-content-index2 li').width();
		_element_lenth =  $('.slider-content-index2 li').length;
				
		_slider_width  = (_element_lenth * _element_width);
		$('.slider-content-index2').css({'width':_slider_width+'px'});
		
		
		$('.slider-right').click(
			function()
			{
				slider_right();
			}
		);
		
		$('.slider-left').click(
			function()
			{
				slider_left();
			}
		);

		
		$(document).everyTime(_homeslider_timer,'slider_right2',slider_right2);
		$(document).everyTime(_homeslider_timer,'slider_right',slider_right);
		
		
		$('.slider-right,.slider-left,.slider-content-index,.slider-right2,.slider-left2,.slider-content-index2').hover(
			function()
			{
				$(document).stopTime("slider_right2", slider_right2);	
				$(document).stopTime("slider_right", slider_right);	
			},
			function()
			{
				$(document).everyTime(_homeslider_timer,'slider_right2',slider_right2);
				$(document).everyTime(_homeslider_timer,'slider_right',slider_right);
			}
		);
		

		$('.slider-left2').click(
			function()
			{
				slider_left2();
			}
		);
		
		$('.slider-right2').click(
			function()
			{
				slider_right2();
			}
		);
		
		
	}
);