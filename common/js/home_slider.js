// JavaScript Document
function slider_top()
{
	if (_state ==0)
	{
		_state =1;
		_copy = $('.slider-content-mid li:first').html();
		
		$('.slider-content-mid').append('<li>'+_copy+'</li>');
		
		if ( _copy != '')
		{
		$('.slider-content-mid li:first .slider-group').fadeOut(
			function()
			{
				$('.slider-content-mid').animate({'top': '-='+_per_frame+'px'},_slider_speed,
				function()
				{
					
						
					$('.slider-content-mid li:first').remove();
								
					$('.slider-content-mid').css({'top':'0px'});
					_state=0;
								
				}
				);	
			}
		);
		}
		
	}
}

function slider_bottom()
{
	if (_state ==0)
	{
		_state =1;
		_copy = $('.slider-content-mid li:last').html();
		$('.slider-content-mid').prepend('<li>'+_copy+'</li>');
		$('.slider-content-mid').css({'top': '-'+_per_frame+'px'});
				
		$('.slider-content-mid li').slice(5, 6).find('.slider-group').fadeOut(
		function()
		{
			_obj = $(this);
			$('.slider-content-mid').animate({'top': '+='+_per_frame+'px'},_slider_speed,
				function()
				{
						
					$('.slider-content-mid').css({'top':'0px'});					
					$('.slider-content-mid li:last').remove();
							
					_obj.show();
					_state=0;
				}
			);
		});	
	}
}

$(document).ready(
	function()
	{
		_slider_speed  = 350;
		_per_frame = 85; //$('.slider-content li').height();
		_homeslider_timer = 5000;
		_copy = '';
		_obj ='';
		_state = 0;
		
		
		$('.slider-box-top,.slider-content,.slider-box-bottom').hover(
			function()
			{
				$(document).stopTime("slider_top", slider_top);	
			},
			function()
			{
				$(document).everyTime(_homeslider_timer,'slider_top',slider_top);
			}
		);
		
		$('.slider-box-top').click(
			function()
			{
				slider_top();
			}
		);

		
		$('.slider-box-bottom').click(
			function()
			{	
				slider_bottom();
			}
		);
		
		$(document).everyTime(_homeslider_timer,'slider_top',slider_top);
		
		
	}
);