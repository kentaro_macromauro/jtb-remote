// JavaScript Document
function banner_navigation_next()
{
	now_btn =  $('.banner-navigation-btn li.active').attr('id')  ;
	next_btn = '';
	now_case = '';
	next_case = '';
	new_obj = '';
	old_obj = '';
	

	if ( $('.banner-navigation li').length == 1)
	{
		
	}
	else if ( $('.banner-navigation li').length > 1)
	{
		if ($('.banner-navigation li').length == 2)
		{
			switch (now_btn)
			{	
				case 'banner-nav1' : next_btn = 'banner-nav2'; now_case = 'menu1'; next_case = 'menu2'; break;
				case 'banner-nav2' : next_btn = 'banner-nav1'; now_case = 'menu2'; next_case = 'menu1'; break;			
			}
		}
		
		if ($('.banner-navigation li').length == 3)
		{
			switch (now_btn)
			{
				case 'banner-nav1' : next_btn = 'banner-nav2'; now_case = 'menu1'; next_case = 'menu2';  break;
				case 'banner-nav2' : next_btn = 'banner-nav3'; now_case = 'menu2'; next_case = 'menu3';  break;
				case 'banner-nav3' : next_btn = 'banner-nav1'; now_case = 'menu3'; next_case = 'menu1';  break;		
			}	
		}
		
		
		$('.banner-navigation-btn #'+next_btn).addClass('active');
		$('.banner-navigation-btn #'+now_btn).removeClass('active');
		
		
		$('.banner-navigation-main a').attr('title',function(i,val){  
			if ( val  == next_case)
			{
				 new_obj = $(this) ;	
			}
				
			if (val ==  now_case)
			{
				 old_obj = $(this) ;	
			}
					
		}) ;
	
		new_obj.fadeIn('slow'); old_obj.fadeOut('slow');

	}
	
}


$(document).ready(
	function()
	{		
	 	if ( $('.banner-navigation li').length < 1)
		{
			$('.banner-navigation').hide();
		}
	
	 	/* banner images sub page */
		_nav_timer = 5000;
		
		if ( $('.banner-navigation-btn').length != 0)
		{
			$(document).everyTime(_nav_timer,'banner_navigation_next',banner_navigation_next);
			
			$('.banner-navigation').hover(
				function()
				{
					$(document).stopTime("banner_navigation_next", banner_navigation_next);	
				},
				function()
				{
					$(document).everyTime(_nav_timer,'banner_navigation_next',banner_navigation_next);
				}
			);
		}
	
		$('.banner-navigation-btn li').click( 
		function() 
		{ 
			old_nav =  $(this).parent().find('.active').attr('id') ;
			old_obj = '';
			new_obj = '';
		
			$('.banner-navigation-btn li').removeClass('active');  $(this).addClass('active'); 
		
			_case = ''; _case2 = '';
			switch ( $(this).attr('id') )
			{
				case 'banner-nav1' : _case = 'menu1'; break;
				case 'banner-nav2' : _case = 'menu2'; break;
				case 'banner-nav3' : _case = 'menu3'; break;
			}
			
			switch ( old_nav )
			{
				case 'banner-nav1' : _case2 = 'menu1'; break;
				case 'banner-nav2' : _case2 = 'menu2'; break;
				case 'banner-nav3' : _case2 = 'menu3'; break;
			}
			
			$('.banner-navigation-main a').attr('title',function(i,val){  
				if ( val  == _case)
				{
					 new_obj = $(this) ;	
				}
				
				if (val == _case2)
				{
					 old_obj = $(this) ;	
				}
				
			}) ;
			
			if ( old_obj.html() != new_obj.html())
			{
				new_obj.fadeIn('slow'); old_obj.fadeOut('slow');
			}
			
		});
		
		/* banner images sub page */
		
		/* hover to change category */ 
		_org = '';
		
		$('.category_text li a').hover(
			function()
			{
				_product_img =  $(this).parent().find('.product_id').html() ;	
				
				if ( '../product/images/product/'+_product_img+'-1' != $(this).parent().parent().parent().find('.category_image img.org_img').attr('src'))
				{
				
					$(this).parent().parent().parent().find('.category_image img.org_img').stop(true, true).fadeOut();
					
			    	$(this).parent().parent().parent().find('.category_image img.now').attr('src','../product/images/product/'+_product_img+'-1')  ;
					$(this).parent().parent().parent().find('.category_image img.now').stop(true, true).fadeIn();
				
				}
				
				
			},
			function()
			{
				if ( $(this).parent().parent().parent().find('.category_image img.now').attr('src') != 
					  $(this).parent().parent().parent().find('.category_image img.org_img').attr('src'))
				{
					$(this).parent().parent().parent().find('.category_image img.org_img').stop(true, true).fadeIn();
					$(this).parent().parent().parent().find('.category_image img.now').stop(true, true).fadeOut();
				}
				 
			}
		);
		/* hover to change category */
		
	
	}
);