<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($_SESSION['NPG']  == 'escva')
{
	if(!empty($_GET[send_excel]))
	{	
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="report.xls"');
	}
	else
	{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
<title>JTB Booking Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
</HEAD>
<? 
	}
?>
<BODY>
<!---->
<!---->
<div class="data">
<div class="table" align="center">
<!--Have line -->
<?
	if(!empty($_GET[send_excel]))
	{
?>
	<TABLE x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="0">
<?	
	}
	else
	{
?>
	<TABLE x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
<? 
	}

require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");
	
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();	
	
	$option_show = $_REQUEST[option_show];
	$start_date  = $_REQUEST[start_date];
	$end_date    = $_REQUEST[end_date];
	
	$option_type = $_REQUEST[option_type];
	$opiton_book = $_REQUEST[option_book];
	
	$raw_data = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'") ;
	
	
	$country_list = str_replace(",","','",$raw_data[1]);
	
	
	if ($option_show == "daily" || $option_show == "monthly")
	{
		if(empty($_GET[send_excel]))
		{
?>
<tr>
<td colspan="14" align=center height=30><b>JTB Monthly Reports</b></td></tr>
<tr>
    <th align=center style='width:40px; table-style:fix;'><strong>Item</strong></th>
    <th align=center nowrep style='width:120px; table-style:fix;'><strong>Update Date</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Status</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Order code</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Site code</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Currency</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Amount</strong></th> 	 	 	 	
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Telephone no</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Email</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Result</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>User Id</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>Optional</strong></th>
</tr>
<?
		}
		else
		{
?>
<tr><td colspan="12" align=center height=30><b>Axes Reports</b></td></tr>
<tr>
    <th align=center width=5% align=center nowrap><strong>Item</strong></th>
    <th align=center width=20% align=center nowrap><strong>Update Date</strong></th>
     <th align=center width=20% align=center nowrap><strong>Status</strong></th>
    <th align=center width=10% align=center nowrap><strong>Order code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Site code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Currency</strong></th>
    <th align=center width=10% align=center nowrap><strong>Amount</strong></th>
    <th align=center width=10% align=center nowrap><strong>Telephone</strong></th>
    <th align=center width=10% align=center nowrap><strong>Email</strong></th>
    <th align=center width=10% align=center nowrap><strong>Result</strong></th>
    <th align=center width=10% align=center nowrap><strong>User Id</strong></th>
    <th align=center width=10% align=center nowrap><strong>Optional</strong></th>
</tr>
<?
		}

		
		

	$sql = 'SELECT result_id,update_date,order_code,site_code,currency,amount,
			telephone_no,email,result, user_id,optional,IF (book_id,"On Request","") status_type
			FROM mbus_payment_result a LEFT JOIN (SELECT book_id FROM mbus_booking WHERE paid_type = 3 AND status_cms_paid = 2) b 
			ON order_code = b.book_id ';
	
	if (!empty( $start_date ) &&  !empty( $end_date))
	{
		$sql .= 'WHERE update_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" ';
		
	}	
	else
	{
		if ($option_show == "daily" )
		{
			$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m-%d\') = DATE_FORMAT(NOW(),\'%Y-%m-%d\') ';	
		}
		else if ($option_show == "monthly" )
		{
			$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m\') = DATE_FORMAT(NOW(),\'%Y-%m\') ';		
				
		}
	}

	$sql .= ' ORDER BY update_date DESC ';
	

	$result = $db->db_query($sql); $i= 0; 
	
	while ($record = mysql_fetch_array($result))
	{			
		$rec = $record;	 	 	 	 	 	 	 	 	 	
		?> 
		<tr>
            <td ><?=$rec[result_id]?></td>
            <td><?=$rec[update_date] ?></td>
            <td><?=urldecode($rec[status_type])?></td>
            <td><?=urldecode($rec[order_code])?></td>
            <td><?=urldecode($rec[site_code])  ?></td>
            <td><?=urldecode($rec[currency]) ?></td>   
            <td><?=urldecode($rec[amount]) ?></td>         
            <td><?=urldecode($rec[telephone_no])?></td>
            <td><?=urldecode($rec[email])?></td>
            <td><?=urldecode($rec[result])?></td>
            <td><?=urldecode($rec[user_id]) ?></td>
            <td><?=urldecode($rec[optional]) ?></td>
        </tr>
		<?			
		$i++;
	}
}
else
{
	header("HTTP/1.1 301 Moved Permanently"); 
	header("Location: booking_report_select.php");
}

?>

<? 
if(empty($_GET[send_excel]))
{
?>

<tr>
<td align=center colspan=12>
<input type=button name=send_excel value='Export Excel' onClick='fncSubmit(this);' />
<input type=button name=send_close value='Close' onClick='func_close(this);' />
</td>								
</tr>
</table>

<script language="javascript">
function fncSubmit(obj)
{
	
	val = 'making_axes_report.php?send_excel=Export&option_show=<? echo $option_show; ?>&start_date=<? echo $start_date; ?>&end_date=<? echo $end_date; ?>&option_type=<? echo $option_type; ?>&option_book=<? echo $opiton_book; ?>';
		
	document.location.href = val;

}

function func_close()
{
	window.close();
	//document.location.href = 'axes_payment_report_a01.php';
}
</script>


</BODY>
</HTML>

<? 
}
else
{
?>
<tr>
<td colspan="12" align=center height=30>&nbsp;</td></tr></TABLE>

</BODY>
</HTML>

<? 
}
}
else
{
	header("Location: login_report.php");
}
?>