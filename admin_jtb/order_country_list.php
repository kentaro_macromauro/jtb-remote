<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{	

	/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	/*----------connect DB--------------*/
	
	//print_r($_REQUEST);
	/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
		
	$data[0] = 'Order information list';
	$data[5] = 'order';
	/*$data[1] = breadcamp( array('Home','Order Country List'),array('index.php') ); */
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('id','100'),
					 tb_head('Name','250'),
					 tb_head('Status','100'),
					 tb_head('Date','100'));

	/*-----------find now page----------------*/
	$page_num = 100;
	//$page_count = $db->page_count("booking");						//include c_query.php	
	
	$page_count = $db->tt_count_level();	
	//echo $page_count;
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->tt_fetch_level($page_first,$page_num,$where);
	
	//print_r($record);
	
	$btn_edit ='';
	$rec_linkprefix = '';					

	
	$data[page_count]=$page_count;
	
	for ($i=0;$i< count($record[0]); $i++)
	{			
	
		//echo $record[1][$i].'=/'.$_SESSION["session_level"];		
		if($record[1][$i]==$_SESSION["session_level"]){
			
			//$btn_edit ='<img src="./common/images/edit.jpg" alt="Edit" border="0" onmouseover="this.src=\'./common/images/edit_on.jpg\'" onmouseout="this.src=\'./common/images/edit.jpg\'">';
			
			$btn_edit ='Click';			
			$rec_linkprefix = 'order_info_list.php';					
		}
		else{
			$btn_edit ='';
			$rec_linkprefix = '';								
		}
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),$record[1][$i],$record[3][$i],$btn_edit),
		array('','','',$rec_linkprefix),
		array(1,1,0,1));		
	}		
	
	$data[page_count]=$i;
	
	/*----set namepage for action check all and delete item-----*/
	foreach ($record[0] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	

	$arr_paid_status =  $db->tt_get_paid_status();	
	$data[8] = input_selectbox('c_status',$arr_paid_status[1],$arr_paid_status[0],$data[c_status],'----No select----','inp_cx1');	

	$arr_product_type = array("PKG","OPT","LPK");
	$data[9] = input_selectbox('product_type',$arr_product_type,$arr_product_type,$data[product_type],'----No select----','inp_cx1');		

	//$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	$data[4] = pagenavi( $page_now, $page_count ,'order_info_list.php?page=',$page_num); 
	
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_order_country_list","../content/themes_backend_jtb/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/

/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>