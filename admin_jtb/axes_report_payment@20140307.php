<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{
	if(!empty($_GET[send_excel])) {	 //if document type excel
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="report.xls"');
		?>       
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<meta http-equiv=Content-Type content="text/html; charset=utf-8"> 
        <?
	}
	else { //if document type html
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Credit Card Transaction Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<style type="text/css">
.btn {
  display: inline-block;
  *display: inline;
  padding: 4px 12px;
  margin-bottom: 0;
  *margin-left: .3em;
  font-size: 14px;
  line-height: 20px;
  color: #333333;
  text-align: center;
  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
  vertical-align: middle;
  cursor: pointer;
  background-color: #f5f5f5;
  *background-color: #e6e6e6;
  background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
  background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
  background-repeat: repeat-x;
  border: 1px solid #cccccc;
  *border: 0;
  border-color: #e6e6e6 #e6e6e6 #bfbfbf;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  border-bottom-color: #b3b3b3;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.btn:hover,
.btn:focus,
.btn:active,
.btn.active,
.btn.disabled,
.btn[disabled] {
  color: #333333;
  background-color: #e6e6e6;
  *background-color: #d9d9d9;
}

.btn:active,
.btn.active {
  background-color: #cccccc \9;
}

.btn:first-child {
  *margin-left: 0;
}

.btn:hover,
.btn:focus {
  color: #333333;
  text-decoration: none;
  background-position: 0 -15px;
  -webkit-transition: background-position 0.1s linear;
     -moz-transition: background-position 0.1s linear;
       -o-transition: background-position 0.1s linear;
          transition: background-position 0.1s linear;
}

.btn:focus {
  outline: thin dotted #333;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}

.btn.active,
.btn:active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.btn.disabled,
.btn[disabled] {
  cursor: default;
  background-image: none;
  opacity: 0.65;
  filter: alpha(opacity=65);
  -webkit-box-shadow: none;
     -moz-box-shadow: none;
          box-shadow: none;
}

body .btn-link,
.btn-link:active,
.btn-link[disabled] {
	color:#0088cc;
  background-color: transparent;
  background-image: none;
  -webkit-box-shadow: none;
     -moz-box-shadow: none;
          box-shadow: none;
}

.btn-link {
  color: #0088cc;
  cursor: pointer;
  border-color: transparent;
  -webkit-border-radius: 0;
     -moz-border-radius: 0;
          border-radius: 0;
}

.data .table table td a{text-decoration:underline;}

body .btn-link:hover,
.btn-link:focus {
  color: #005580;
  text-decoration: underline;
  background-color: transparent;
}

.btn-link[disabled]:hover,
.btn-link[disabled]:focus {
  color: #333333;
  text-decoration: none;
}



.btn-info {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #49afcd;
  *background-color: #2f96b4;
  background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));
  background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
  background-repeat: repeat-x;
  border-color: #2f96b4 #2f96b4 #1f6377;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-info:hover,
.btn-info:focus,
.btn-info:active,
.btn-info.active,
.btn-info.disabled,
.btn-info[disabled] {
  color: #ffffff;
  background-color: #2f96b4;
  *background-color: #2a85a0;
}

.btn-info:active,
.btn-info.active {
  background-color: #24748c \9;
}
</style>

</head>
<? 
	} // end if document type
?>
<body>
<!---->
<? 
	if (empty($_REQUEST[send_excel])){
		echo '<div style="padding-top:20px;"></div><ul class="breadcamp"><li class="home"><a href="./">HOME</a>>&nbsp;&nbsp;Credit Card Transaction Report </li></ul>';	
	}
?>
<!---->
<div class="data">
<div class="table" align="center">
<!--Have line -->
<?
	if(!empty($_GET[send_excel]))
	{
?>
	<table x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="0">
<?	
	}
	else
	{
?>
	<table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
<? 
	}

require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");
	
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();	
	
	$option_show = "log";
	/*$option_show = $_REQUEST[option_show];*/
	
	
	$option_type = $_REQUEST[option_type];
	$opiton_book = $_REQUEST[option_book];
	
	if (  (empty($_REQUEST[start_date])) || (empty($_REQUEST[end_date])) ){

		//$num_ofmonth = cal_days_in_month(CAL_GREGORIAN, date(n), date(Y));	
		$num_ofmonth = date('t', mktime(0, 0, 0, date(n), 1, date(Y))); 

		$start_date = date(Y).'-'.date(m).'-01';
		$end_date  = date(Y).'-'.date(m).'-'.$num_ofmonth;

	}
	else{
		$start_date  = $_REQUEST[start_date];
		$end_date    = $_REQUEST[end_date];
	}
	

	
	
	$raw_data = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'") ;
	
	
if (empty($_REQUEST[f_country] )){
	
	$country_list = str_replace(",","','",$raw_data[1]);
	
	if ($_SESSION[session_level] == "A")
	{
		$country_list .= "','TEST";
	}
}
else{
	$country_list = $_REQUEST[f_country];	
}










if ($option_show == "log"){ //if log report start;
	
	if(!$_GET[send_excel]){ //if document type = excel
?>
<tr>
<td colspan="16" align=center height=30><b>Credit Card Transaction Report From <?=$start_date?> To <?=$end_date?></b></td></tr>
<tr>
<td colspan="16" align="left" height="30">
<form action="?" method="get">
Country : 
<?
$arr_inputvalue = array('TWN','IDN','THA','VNM','KHM','MYS','SGP','HKG','AUS','NZL');
$arr_inputdata = array('Taiwan','Indonesia','Thailand','Vietnam','Cambodia','Malaysia','Singapore','Hong Kong','Australia','New Zealand');

echo input_selectbox('f_country',$arr_inputdata,$arr_inputvalue,$_REQUEST[f_country],$startselect='---Select All---','') ?>


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Status : <select name="sel_type"><option value="a" <? if ($_REQUEST[sel_type]=="a"){ ?> selected="selected" <? } ?>>ALL</option><option value="cb" <? if ($_REQUEST[sel_type]=="cb"){ ?> selected="selected" <? } ?> >Cannot Booking</option></select>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Date :  From
<input id="ifrom_date" class="textFieldSize60" name="start_date" value="<?=$start_date?>" type="text" readonly="readonly" />                                  
<a href="javascript:NewCssCal('ifrom_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0"></a>
To
<input id="ito_date" class="textFieldSize60" name="end_date" value="<?=$end_date?>" type="text" readonly="readonly" />
<a href="javascript:NewCssCal('ito_date','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0"></a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" name="btn_submit" class="btn type1" value="Search"  />
</form>
</td>
</tr>
<tr>
    <th align=center style='width:40px; table-style:fix;'><strong>Item</strong></th>
    <th align=center style='width:40px; table-style:fix;'>Mail</th>
    <th align=center style='width:40px; table-style:fix;'>Detail</th>
    <th align=center nowrep style='width:120px; table-style:fix;'><strong>Update Date</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Status</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Order code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Country</strong></th>    
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Site code</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Currency</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Amount</strong></th> 	 	 	 	
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Telephone no</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Email</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Result</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>Type</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>User Id</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>Optional</strong></th>
</tr>
<?	}
	else { // if document type = html
?>
<tr><td colspan="13" align=center height=30><b>Credit Card Transaction Report From <?=$start_date?> To <?=$end_date?></b></td></tr>
<tr>
    <th align=center width=5% align=center nowrap><strong>Item</strong></th>
    <th align=center width=20% align=center nowrap><strong>Update Date</strong></th>
    <th align=center width=20% align=center nowrap><strong>Status</strong></th>
    <th align=center width=10% align=center nowrap><strong>Order code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Country</strong></th>
    <th align=center width=10% align=center nowrap><strong>Site code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Currency</strong></th>
    <th align=center width=10% align=center nowrap><strong>Amount</strong></th>
    <th align=center width=10% align=center nowrap><strong>Telephone</strong></th>
    <th align=center width=10% align=center nowrap><strong>Email</strong></th>
    <th align=center width=10% align=center nowrap><strong>Result</strong></th>
    <th align=center width=10% align=center nowrap><strong>Type</strong></th>
    <th align=center width=10% align=center nowrap><strong>User Id</strong></th>
    <th align=center width=10% align=center nowrap><strong>Optional</strong></th>
</tr>
<?
	} //end if document type
	
	
	
	$sql = 'SELECT country_iso4 country_iso3,result_id,update_date,order_code,site_code,currency,amount,type,telephone_no,email,result,user_id,optional,
country_iso4,session_status,book_id,status_type FROM 
(SELECT a.*, IF (site_code=\'A019000014\',\'Test\',b.country_iso3)  country_iso4,b.session_status,b.book_id,
			 IF (paid_type=3,"On Request","") status_type  
				FROM 
				(SELECT * FROM (SELECT IF (site_code=\'A019000014\',\'Test\',country_iso3) country_iso3,
				result_id,update_date,order_code,site_code,currency,amount,type,telephone_no,email,result,
				user_id,optional
				FROM mbus_payment_result a LEFT JOIN 
				(SELECT book_id,country_iso3 FROM mbus_booking WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
				 ';
				
	if (!empty( $start_date ) && !empty( $end_date)){
		$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"  ) AS aa ';	
	}	
	else{
		$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m\') = DATE_FORMAT(NOW(),\'%Y-%m\') ) AS aa  ';
	}
	$sql .=  'WHERE ((country_iso3 IS NULL) || (country_iso3 IN (\''.$country_list.'\')))  
				ORDER BY update_date DESC ) AS a LEFT JOIN 
				mbus_booking AS b ON TRIM(a.user_id) = b.book_id  ) AS aa WHERE (country_iso4 IS NULL) || (country_iso4 IN (\''.$country_list.'\')) ';
	
	$result = $db->db_query($sql); $i= 0; 

	
	while ($record = mysql_fetch_array($result))
	{			
		$rec = $record;	 	 	 	 	 	 	 	 	 	
		?> 
        <? 
			
			
			
			if ($rec[book_id] == $rec[order_code]){
				if  (($rec[session_status] == '1') ){
					$style = '';
					$msgmail = 'Done';
				}
				else{
					if ( ($rec[result] == 'success') ) {
						$style = ' style="background:#FFC;" ';
						$msgmail = '<a href="axes_resend_mail.php?id='.md5($rec[book_id]).'" class="btn">Resend</a>';
					}
					else{
						$stye = '';	
						$msgmail = 'Done';
					}
				}
			}
        
		
			$cc = $rec[user_id];
								
			if (strlen( $rec[user_id] ) > 7 ){
					$cc = $rec[order_code];	
			}
		
		
		
			if ($_REQUEST[sel_type] == 'cb'){
				
				if ($rec[book_id] == $rec[order_code]){
					if  (($rec[session_status] == '1') ){
						$style = '';
						$msgmail = 'Done';
					}
					else{
						if ( ($rec[result] == 'success') ) {
							if(!$_GET[send_excel]){
								
								
								
							?>
								<tr>
								<td<?=$style?>><?=$rec[result_id] ?></td> 
								<td<?=$style?> align="center"><?=$msgmail ?></td>
								<td<?=$style?> align="center"><a href="axes_report_details.php?id=<?=$cc?>" class="btn btn-link"  target="_blank">Detail</a></td>
								<td<?=$style?>><?=$rec[update_date] ?></td>
								<td<?=$style?>><?=urldecode($rec[status_type])?></td>
								<td<?=$style?>><?=urldecode($rec[order_code])?></td>
								<td<?=$style?>><?=urldecode($rec[country_iso3]) ?></td>
								<td<?=$style?>><?=urldecode($rec[site_code]) ?></td>
								<td<?=$style?>><?=urldecode($rec[currency]) ?></td>   
								<td<?=$style?>><?=urldecode($rec[amount]) ?></td>         
								<td<?=$style?>><?=urldecode($rec[telephone_no])?></td>
								<td<?=$style?>><?=urldecode($rec[email])?></td>
								<td<?=$style?>><?=urldecode($rec[result])?></td>
								<td<?=$style?>><?=urldecode($rec[type])?></td>
								<td<?=$style?>><?=urldecode($rec[user_id]) ?></td>
								<td<?=$style?>><?=urldecode($rec[optional]) ?></td>
								</tr>	
							<?
							}
							else{
							?>
								<tr>
								<td><?=$rec[result_id] ?></td> 
								<td><?=$rec[update_date] ?></td>
								<td><?=urldecode($rec[status_type])?></td>
								<td><?=urldecode($rec[order_code])?></td>
								<td><?=urldecode($rec[site_code]) ?></td>
								<td><?=urldecode($rec[country_iso3]) ?></td>
								<td><?=urldecode($rec[currency]) ?></td>   
								<td><?=urldecode($rec[amount]) ?></td>         
								<td><?=urldecode($rec[telephone_no])?></td>
								<td><?=urldecode($rec[email])?></td>
								<td><?=urldecode($rec[result])?></td>
								<td><?=urldecode($rec[type])?></td>
								<td><?=urldecode($rec[user_id]) ?></td>
								<td><?=urldecode($rec[optional]) ?></td>
								</tr>
								<?	
							}
						}
					}
				}
				
			}
			else{
				if(!$_GET[send_excel]){
				?>
					<tr>
					<td<?=$style?>><?=$rec[result_id] ?></td> 
					<td<?=$style?> align="center"><?=$msgmail ?></td>
					<td<?=$style?> align="center"><a href="axes_report_details.php?id=<?=$cc?>" class="btn btn-link" target="_blank">Detail</a></td>
					<td<?=$style?>><?=$rec[update_date] ?></td>
					<td<?=$style?>><?=urldecode($rec[status_type])?></td>
					<td<?=$style?>><?=urldecode($rec[order_code])?></td>
					<td<?=$style?>><?=urldecode($rec[country_iso3]) ?></td>
					<td<?=$style?>><?=urldecode($rec[site_code]) ?></td>
					<td<?=$style?>><?=urldecode($rec[currency]) ?></td>   
					<td<?=$style?>><?=urldecode($rec[amount]) ?></td>         
					<td<?=$style?>><?=urldecode($rec[telephone_no])?></td>
					<td<?=$style?>><?=urldecode($rec[email])?></td>
					<td<?=$style?>><?=urldecode($rec[result])?></td>
					<td<?=$style?>><?=urldecode($rec[type])?></td>
					<td<?=$style?>><?=urldecode($rec[user_id]) ?></td>
					<td<?=$style?>><?=urldecode($rec[optional]) ?></td>
					</tr>
				<?
				}
				else{
				?>
					<tr>
						<td><?=$rec[result_id] ?></td> 
						<td><?=$rec[update_date] ?></td>
						<td><?=urldecode($rec[status_type])?></td>
						<td><?=urldecode($rec[order_code])?></td>
						<td><?=urldecode($rec[site_code]) ?></td>
						<td><?=urldecode($rec[country_iso3]) ?></td>
						<td><?=urldecode($rec[currency]) ?></td>   
						<td><?=urldecode($rec[amount]) ?></td>         
						<td><?=urldecode($rec[telephone_no])?></td>
						<td><?=urldecode($rec[email])?></td>
						<td><?=urldecode($rec[result])?></td>
						<td><?=urldecode($rec[type])?></td>
						<td><?=urldecode($rec[user_id]) ?></td>
						<td><?=urldecode($rec[optional]) ?></td>
					</tr>			
				<?	
				}
				
			}
		
        ?>			
		<?			
		$i++;
	}
}
 
	if(empty($_GET[send_excel])) //echo footer if document type html;
	{
		?>
		
		<tr>
		<td align=center colspan=16>
		<input type=button name=send_excel value='Export Excel' class="btn type1" onClick='fncSubmit(this);' />
		<input type=button name=send_close value='Close' class="btn type1" onClick='func_close(this);' />
		</td>								
		</tr>
		</table>
		
		<script language="javascript">
		function fncSubmit(obj)
		{
			
			val = 'axes_report_payment.php?send_excel=Export&option_show=<? echo $option_show; ?>&start_date=<? echo $start_date; ?>&end_date=<? echo $end_date; ?>&option_type=<? echo $option_type; ?>&option_book=<? echo $opiton_book; ?>&sel_type=<?=$_REQUEST[sel_type] ?>&f_country=<?=$_REQUEST[f_country]?>';
		
			document.location.href = val;
		
		}
		
		function func_close()
		{
			document.location.href = 'index.php';
		}
		</script>
		
		
		</body>
		</html>
		
		<? 
	}
	else{ //echo footer if document type excel;
		?>
		<tr>
        <? 
			if ($option_show == "log"){
		?>
		<td colspan="5" align=center height=30>&nbsp;</td></tr></TABLE>
		<? }else{ ?>
        <td colspan="13" align=center height=30>&nbsp;</td></tr></TABLE>
        
        <? 
		}
		?>
		</BODY>
		</html>
		
		<? 
	}// echo footer if document type;
} // if log report end;
?>