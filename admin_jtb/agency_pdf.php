<?	
require('pdf/tfpdf_class.php');
require_once("../www_config/setting.php");
require_once("../class_backend/include/c_query.php");

//print_r($_REQUEST);
	
$db = new c_db(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();			

class PDF extends tFPDF_Extend
{
	
	function frm_report2($ff_data,$data_guest)
	{
				
	   //$today = date('F j, Y');
	   $today = date('j F Y');

	   $this->AddFont('SJIS','','TakaoPGothic.ttf',true);
	   $this->Image('images/logo_pdf.jpg',36.5,25.4,225,45,'JPEG');
	   
	   $this->SetFont('times','IB',24);
	   $this->Text(420,54.2,'VOUCHER');
	   $this->SetFont('SJIS','',10);
	   
//Line =========================================================================	   	   
		$this->SetFillColor(192, 192 ,192);
		$this->Rect( 34.6,75.6 , 524.4, 10, 'F');
		
//Head =========================================================================	   	   

		$weeklist = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'); 
		
		$date_today = date('w',date('Y-m-d'));		
		$date_book = date('w',strtotime($ff_data['update_date']));		
		
		
		$date_today_week = $weeklist[$date_today];
		$date_book_week = $weeklist[$date_book];


	   //$this->Text(35,100, txt_jp("JTBロゴ"));
	   $this->Text(100,100, "JTB (THAILAND) LTD. (Bangkok)");
	   
	   $date_booking =str_replace('-',' ',date('d-M-Y',strtotime($ff_data['update_date'])));
	   //$this->Text(420,100, $date_booking.' ('.$date_week.')');
	   $this->Text(420,100, $today.' ('.$date_today_week.')');
	   
	   $this->Text(100,115, "54 Hairdhorn Bldg 4F Room 4B, North Sathron Rd, Kwang Silom KhetBangrak BKK10500 Thailand");
	   $this->Text(100,130, "TEL :");
	   $this->Text(300,130, "FAX :");
	   
	   $text_left ='35';	   
	   $product_jp_en =$ff_data['product_name_en'].' / '.$ff_data['product_name_jp'];
	   $web_id =$ff_data['jtb_bookind_id'];
	   $tour_name =$ff_data['reg_furi_firstname'].' '.$ff_data['reg_furi_lastname'];
	   
	   
	   $this->Text($text_left,150,'To : '.$product_jp_en);				
	   $this->Text($text_left,190,'Web ID : '.$web_id);				
	   $this->Text($text_left,205,'Tour ID : '.$tour_id);				
	   $this->Text($text_left,220,'Tour Name : '.$tour_name);				

	   $this->Text($text_left,245,$ff_data['product_name_en']);
	   
//Voucher =========================================================================	   

	   //,b.qty_adults,b.qty_child,b.qty_infant

	   $this->Text($text_left,270,'<Voucher>');	   
	   $this->Text($text_left,290,'Date : '.$date_booking.' ('.$date_book_week.')');	   
	   $this->Text($text_left,310,'Time : ');	   
	   $this->Text($text_left,330,'Number of Customer : Adult '.number_format($ff_data['qty_adults'],0).'  Child '.number_format($ff_data['qty_child'],0).'  Infant '.number_format($ff_data['qty_infant'],0));

//Guest =========================================================================
	   $this->Text($text_left,350,'First Name');	   
	   $this->Text($text_left+300,350,'Family Name');	   
	
		//$_guest[${$fg[guest_firstname]}][$ig] =$fg[guest_firstname];
		//$_guest[${$fg[guest_lastname]}][$ig] =$fg[guest_lastname];	
		
		$i=0;
		//$sex_g =array("1"=>"Ms","2"=>"Mr");
		$sex_g =array("1"=>"Mr","2"=>"Ms");
		$topg ='370';
		
		/**/
		foreach($data_guest as $k=>$v){		
		
		//echo "$k=>$v";

		   $this->Text($text_left,$topg,$i+'1'.'. '.$data_guest[$i][guest_firstname]);
		   $this->Text($text_left+150,$topg,$sex_g[$data_guest[$i][guest_sex]]);	   
		   $this->Text($text_left+300,$topg,$data_guest[$i][guest_lastname]);   
			
		   $i++;
		   $topg +='15';
		}	
	
	
//Payment Information ===========================================================

	   $this->Text($text_left,500,'Payment Information');	   
	   
	   $this->Text($text_left+20,530,'Payment : '.$ff_data['paid_name']);	   
	   
	   
	   //				left	top	 width	height  type
	   $this->Rect( $text_left,510 , 524.4, 140, 'B');
	
//=========================================================================
	
		$this->Text(432.2,788.6,'Reservation Manager');
		$this->Text(420,800,$shop_data[0]['shop_manager']);	
		$this->Line(395.8, 801.7, '559', 801.7 );
		
		//$this->Text($text_left,799.4,'02 652 5352');	$this->Text(159,799.4,'URL : www.pptours-bkk.com');
	   
	   	$this->Rect( 34.6,809.5 , 524.4, 10, 'F');	   
	}
}
				
	//==================================================================================
	$sql = "SELECT b.book_id,b.country_iso3,b.book_type,date(b.book_date) b_date,b.jtb_bookind_id,b.jtb_statuscode
					,b.transection_id,b.amount,b.currency,b.paid_type,b.paid_status
					,b.qty_adults,b.qty_child,b.qty_infant,b.qty_amount
					,b.reg_furi_firstname,b.reg_furi_lastname,b.reg_sex,b.reg_addrjp_zip1,b.reg_addrjp_zip2
					,reg_addrtype,reg_addrjp_city,reg_addrjp_area,reg_addrjp_pref,reg_addrjp_building
					,reg_tel1,reg_tel2,reg_tel3,reg_mobile1,reg_mobile2,reg_mobile3,reg_backup_addrovs1
					,reg_backup_tel1,reg_backup_tel3,reg_backup_tel3,reg_birthday					
					,reg_hotel_id,reg_arrfrm,reg_arrto,reg_arrgo,reg_airno,reg_info,b.remark
					,reg_addrtype,reg_addrovs1,reg_addrovs2
					,reg_hoteltype,reg_hotel_name,reg_hotel_tel,reg_hotel_addr1					
					,b.reg_firstname,b.reg_lastname,b.reg_email,d.product_id
					,b.amount,s.paid_name
					,p.city_iso3,p.product_code,p.product_name_jp,p.product_name_en
					,DATE(b.update_date) AS update_date					
				FROM mbus_booking b
				LEFT JOIN mbus_booking_detail d ON b.book_id=d.book_id
				LEFT JOIN mbus_product p ON p.product_id=d.product_id
				LEFT JOIN mbus_paidstatus s ON s.paid_id=b.paid_type
				WHERE b.book_id='".$_GET['b_id']."'
			";			
				
	//echo $sql; //exit;		
	$r0 = $db->db_query($sql);		
	$ff_data = mysql_fetch_array($r0);	
	
	//================================================================
	$sqlg = "SELECT * FROM mbus_booking_guest WHERE book_id='".$_GET['b_id']."'";						
	$ig=0;
	
	//echo $sqlg; //exit;		
	$rg = $db->db_query($sqlg);
	/**/
	while ($fg = mysql_fetch_array($rg)){		
					
			//guest_firstname,guest_lastname
			$data_guest[$ig]['guest_sex'] =$fg[guest_sex];
			$data_guest[$ig]['guest_firstname'] =$fg[guest_firstname];
			$data_guest[$ig]['guest_lastname'] 	=$fg[guest_lastname];	
			
			$ig++;		
	}
			
	//print_r($_guest);
	//================================================================
	$pdf=new PDF('P','pt','A4');
	
	//for($i=1;$i<=$count; $i++){
		
		$pdf->Write(0,'');
		$pdf->AddPage();	
		
		$pdf->frm_report2($ff_data,$data_guest);
	//}
	
	if($_GET['pdf_type'] != 'print'){
	$pdf->Output("./pdf_agency/".$ff_data['jtb_bookind_id'].".pdf");
	}else{
	$pdf->Output();
	}	
	


?>