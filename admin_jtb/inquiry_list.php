<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{
	if(!empty($_GET[send_excel])) {	 //if document type excel
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="report.xls"');
		?>       
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<meta http-equiv=Content-Type content="text/html; charset=utf-8"> 
        <?
	}
	else { //if document type html
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Rakunavi Support : Inquiry list</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<style type="text/css">
.btn {
  display: inline-block;
  *display: inline;
  padding: 4px 12px;
  margin-bottom: 0;
  *margin-left: .3em;
  font-size: 14px;
  line-height: 20px;
  color: #333333;
  text-align: center;
  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
  vertical-align: middle;
  cursor: pointer;
  background-color: #f5f5f5;
  *background-color: #e6e6e6;
  background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
  background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
  background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
  background-repeat: repeat-x;
  border: 1px solid #cccccc;
  *border: 0;
  border-color: #e6e6e6 #e6e6e6 #bfbfbf;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  border-bottom-color: #b3b3b3;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
  *zoom: 1;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.btn:hover,
.btn:focus,
.btn:active,
.btn.active,
.btn.disabled,
.btn[disabled] {
  color: #333333;
  background-color: #e6e6e6;
  *background-color: #d9d9d9;
}

.btn:active,
.btn.active {
  background-color: #cccccc \9;
}

.btn:first-child {
  *margin-left: 0;
}

.btn:hover,
.btn:focus {
  color: #333333;
  text-decoration: none;
  background-position: 0 -15px;
  -webkit-transition: background-position 0.1s linear;
     -moz-transition: background-position 0.1s linear;
       -o-transition: background-position 0.1s linear;
          transition: background-position 0.1s linear;
}

.btn:focus {
  outline: thin dotted #333;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}

.btn.active,
.btn:active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
     -moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
          box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
}

.btn.disabled,
.btn[disabled] {
  cursor: default;
  background-image: none;
  opacity: 0.65;
  filter: alpha(opacity=65);
  -webkit-box-shadow: none;
     -moz-box-shadow: none;
          box-shadow: none;
}

body .btn-link,
.btn-link:active,
.btn-link[disabled] {
	color:#0088cc;
  background-color: transparent;
  background-image: none;
  -webkit-box-shadow: none;
     -moz-box-shadow: none;
          box-shadow: none;
}

.btn-link {
  color: #0088cc;
  cursor: pointer;
  border-color: transparent;
  -webkit-border-radius: 0;
     -moz-border-radius: 0;
          border-radius: 0;
}

.data .table table td a{text-decoration:underline;}

body .btn-link:hover,
.btn-link:focus {
  color: #005580;
  text-decoration: underline;
  background-color: transparent;
}

.btn-link[disabled]:hover,
.btn-link[disabled]:focus {
  color: #333333;
  text-decoration: none;
}



.btn-info {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #49afcd;
  *background-color: #2f96b4;
  background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));
  background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);
  background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
  background-repeat: repeat-x;
  border-color: #2f96b4 #2f96b4 #1f6377;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-info:hover,
.btn-info:focus,
.btn-info:active,
.btn-info.active,
.btn-info.disabled,
.btn-info[disabled] {
  color: #ffffff;
  background-color: #2f96b4;
  *background-color: #2a85a0;
}

.btn-info:active,
.btn-info.active {
  background-color: #24748c \9;
}
</style>

</head>
<? 
	} // end if document type
?>
<body>
<!---->
<!---->
<? 
	if (empty($_REQUEST[send_excel])){
		echo '<div style="padding-top:20px;"></div><ul class="breadcamp"><li class="home"><a href="./">HOME</a>>&nbsp;&nbsp;Inquery list</li></ul>';	
	}
?>


<div class="data">
<div class="table" align="center">
<!--Have line -->
<?
	if(!empty($_GET[send_excel]))
	{
?>
	<table x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="0">
<?	
	}
	else
	{
?>
	
	<table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
<? 
	}

require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");
	
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();	
	
	$option_show = "log";
	
	$option_type = $_REQUEST[option_type];
	$opiton_book = $_REQUEST[option_book];
	
	if (  (empty($_REQUEST[start_date])) || (empty($_REQUEST[end_date])) ){

		//$num_ofmonth = cal_days_in_month(CAL_GREGORIAN, date(n), date(Y));	
		$num_ofmonth = date('t', mktime(0, 0, 0, date(n), 1, date(Y))); 

		$start_date = date(Y).'-'.date(m).'-01';
		$end_date  = date(Y).'-'.date(m).'-'.$num_ofmonth;

	}
	else{
		$start_date  = $_REQUEST[start_date];
		$end_date    = $_REQUEST[end_date];
	}
	
	
	//$raw_data = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'") ;
	
	

if ($option_show == "log"){ //if log report start;
	
	if(!$_GET[send_excel]){ //if document type = excel
?>
<tr>
<td colspan="9" align=center height=30><b>Rakunavi Support : Inquiry list</b></td></tr>
<!--<tr>
<td colspan="16" align="left" height="30">
<form action="?" method="get">
Country : 
<?
$arr_inputvalue = array('TWN','IDN','THA','VNM','KHM','MYS','SGP','HKG','AUS','NZL');
$arr_inputdata = array('Taiwan','Indonesia','Thailand','Vietnam','Cambodia','Malaysia','Singapore','Hong Kong','Australia','New Zealand');

echo input_selectbox('f_country',$arr_inputdata,$arr_inputvalue,$_REQUEST[f_country],$startselect='---Select All---','') ?>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" name="btn_submit" class="btn type1" value="Search"  />
</form>
</td>
</tr>-->
<tr>
	<th  style="width:50px; text-align:center; table-layout:fixed;"><strong>Detail</strong></th>
    <th  style="width:70px; text-align:center; table-layout:fixed;"><strong>Country</strong></th>
	<th  style="width:90px; text-align:center; table-layout:fixed;"><strong>City</strong></th>
    <th  style="width:100px; text-align:center; table-layout:fixed;"><strong>Depature Date</strong></th>
    <th  style="width:150px; text-align:center; table-layout:fixed;"><strong>JTB No.</strong></th>
    <th  style="width:150px; text-align:center; table-layout:fixed;"><strong>Name (Last,First)</strong></th>
    <th  style="width:150px; text-align:center; table-layout:fixed;"><strong>Furigana Name (Last,First)</strong></th>    
    <th  style="width:170px; text-align:center; table-layout:fixed;"><strong>Email</strong></th>
	 <th style="width:150px; text-align:center; table-layout:fixed;"><strong>Submitted Date</strong></th>
</tr>
<?	}
	else { // if document type = html
?>
<tr><td colspan="9" align=center height=30><b>Rakunavi Support : Inquiry list</b></td></tr>
<tr>
    <th align=center width=10% align=center nowrap><strong>Country</strong></th>
    <th align=center width=10% align=center nowrap><strong>City</strong></th>
    <th align=center width=10% align=center nowrap><strong>Depature Date</strong></th>
    <th align=center width=10% align=center nowrap><strong>JTB No.</strong></th>
    <th align=center width=20% align=center nowrap><strong>Name (Last,First)</strong></th>
    <th align=center width=20% align=center nowrap><strong>Furigana Name (Last,First)</strong></th>
    <th align=center width=30% align=center nowrap><strong>Email</strong></th>
	<th align=center width=30% align=center nowrap><strong>Inquiry</strong></th>
	<th align=center width=10% align=center nowrap><strong>Submitted Date</strong></th>
</tr>
<?
	} //end if document type
	
	$sql = 'SELECT c_id,country_iso3,branch,first_name,last_name,furi_first_name, furi_last_name,email,depature_date,jtb_no,comment,update_date FROM mbus_jtb_contact ';
	
	if ($_SESSION["session_country"] == 'SGP'){
		$sql .= 'WHERE country_iso3 = "SGP" ';
	}
	
	if ($_SESSION["session_country"] == 'THA'){
		$sql .= 'WHERE country_iso3 = "THA" ';
	}
	
	if ($_SESSION["session_country"] == 'TWN'){
		$sql .= 'WHERE country_iso3 = "TWN" ';
	}
	
	
	$sql .= 'ORDER BY depature_date DESC,update_date DESC';
	
	$result = $db->db_query($sql);
	$rec = array();
	
	while ($record = mysql_fetch_array($result)){
		$rec = $record ;	
		
		$country_name = '';
		$branch = '';
		switch ( $rec[country_iso3] ){
			case 'TWN' : $country_name = 'Taiwan'; break;	
			case 'THA' : $country_name = 'Thailand'; break;	
			case 'SGP' : $country_name = 'Singapore'; break;	
		}
		
		if ($rec[branch] == 'バンコク'){
			$branch = 'Bangkok'; 
		}
		else if ($rec[branch]  == 'プーケット'){
			$branch = 'Phuket';	
		}
		else if ($rec[branch]  == 'その他'){
			$branch = 'Other';	
		}
		else{
			if ($rec[branch]){
				$branch = $rec[branch];
			}
		}
		
		
		if(empty($_GET[send_excel])){
		?>
			<tr>
				<td style="text-align:center;"><a href="inquiry_list_detail.php?id=<?=$rec[c_id]?>" target="_blank" class="btn btn-link" >Detail</a></td>
				<td style="text-align:center;"><?=$country_name?></td>
				<td style="text-align:center;"><?=$branch?></td>
				<td style="text-align:center;"><?=($rec[depature_date])?></td>
				<td style="text-align:center;"><?=($rec[jtb_no])?></td>
				<td style="text-align:left;"><?=($rec[last_name].'　'.$rec[first_name]) ?></td>
				<td style="text-align:left;"><?=($rec[furi_last_name].'　'.$rec[furi_first_name]) ?></td>
				<td style="text-align:left;"><?=($rec[email])?></td>
				<td style="text-align:center;"><?=($rec[update_date])?></td>
			</tr>		
		<? 
		}
		else{
		?>
			<tr>
				<td style="text-align:center;"><?=$country_name?></td>
				<td style="text-align:center;"><?=$branch?></td>
				<td style="text-align:center;"><?=($rec[depature_date])?></td>
				<td style="text-align:center;"><?=($rec[jtb_no])?></td>
				<td style="text-align:left;"><?=($rec[last_name].'　'.$rec[first_name]) ?></td>
				<td style="text-align:left;"><?=($rec[furi_last_name].'　'.$rec[furi_first_name]) ?></td>
				<td style="text-align:left;"><?=($rec[email])?></td>
				<td style="text-align:left;"><?=($rec[comment])?></td>
				<td style="text-align:center;"><?=($rec[update_date])?></td>
			</tr>		
		<?	
		}
	}
	
	
	
 
	if(empty($_GET[send_excel])) //echo footer if document type html;
	{
		?>
		
		<tr>
		<td align=center colspan=16>
		<input type=button name=send_excel value='Export Excel' class="btn type1" onClick='fncSubmit(this);' />
		<input type=button name=send_close value='Close' class="btn type1" onClick='func_close(this);' />
		</td>								
		</tr>
		</table>
		
		<script language="javascript">
		function fncSubmit(obj)
		{
			
			val = 'inquiry_list.php?send_excel=Export';
		
			document.location.href = val;
		
		}
		
		function func_close()
		{
			document.location.href = 'index.php';
		}
		</script>
		
		
		</body>
		</html>
		
		<? 
	}
	else{ //echo footer if document type excel;
		?>
		<tr>
        <? 
			if ($option_show == "log"){
		?>
		<td colspan="5" align=center height=30>&nbsp;</td></tr></TABLE>
		<? }else{ ?>
        <td colspan="13" align=center height=30>&nbsp;</td></tr></TABLE>
        
        <? 
		}
		?>
		</BODY>
		</html>
		
		<? 
	}// echo footer if document type;
}
	
} // if log report end;
?>