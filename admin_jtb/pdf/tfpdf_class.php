<? 
require_once("tfpdf.php");
class tFPDF_Extend extends tFPDF
{
	function DashedRect($x1, $y1, $x2, $y2)
	{
		if ($x1 == $x2)
		{
			$nb = floor(($y2 - $y1)/5);
		}
		
		if ($y1 == $y2)
		{
			$nb = floor(($x2 - $x1)/5);
		}
		
		$longueur=abs($x1-$x2);
		$hauteur=abs($y1-$y2);
		
		if($longueur>$hauteur) 
		{
			$Pointilles=($longueur/$nb)/2; // length of dashes
		}
		else 
		{
			$Pointilles=($hauteur/$nb)/2;
		}
		
		for($i=$x1;$i<=$x2;$i+=$Pointilles+$Pointilles) 
		{
			for($j=$i;$j<=($i+$Pointilles);$j++)
			{
				if($j<=($x2-1)) 
				{
					$this->Line($j, $y1, $j+1, $y1); // upper dashes
					$this->Line($j, $y2, $j+1, $y2); // lower dashes
				}
			}
		}
		
		for($i=$y1;$i<=$y2;$i+=$Pointilles+$Pointilles) 
		{
			for($j=$i;$j<=($i+$Pointilles);$j++) 
			{
				if($j<=($y2-1)) 
				{
					$this->Line($x1, $j, $x1, $j+1); // left dashes
					$this->Line($x2, $j, $x2, $j+1); // right dashes
				}
			}
		}
	}
		
	function SJISMultiCell($w,$h,$txt,$border=0,$align='L',$fill=0)
	{
		//Output text with automatic or explicit line breaks
		$cw = $this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s{$nb-1}=="\n")
			$nb--;
		$b=0;
		if($border)
		{
			if($border==1)
			{
				$border='LTRB';
				$b='LRT';
				$b2='LR';
			}
			else
			{
				$b2='';
				if(is_int(strpos($border,'L')))
					$b2.='L';
				if(is_int(strpos($border,'R')))
					$b2.='R';
				$b=is_int(strpos($border,'T')) ? $b2.'T' : $b2;
			}
		}
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			//Get next character
			$c=$s{$i};
			$o=ord($c);
			if($o==10)
			{
				//Explicit line break
				$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				if($border and $nl==2)
					$b=$b2;
				continue;
			}
			if($o<128)
			{
				
				//ASCII
				//$l+=$cw[$c]; // fix by jedsadha 01/April/2010
				$l+=800;
				$n=1;
				if($o==32)
					$sep=$i;
			}
			elseif($o>=161 and $o<=223)
			{
				//Half-width katakana
				$l+=500;
				$n=1;
				$sep=$i;
			}
			else
			{
				//Full-width character
				$l+=1000;
				$n=2;
				$sep=$i;
			}
			if($l>$wmax)
			{
				//Automatic line break
				if($sep==-1 or $i==$j)
				{
					if($i==$j)
						$i+=$n;
					$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
				}
				else
				{
					$this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
					$i=($s[$sep]==' ') ? $sep+1 : $sep;
				}
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				if($border and $nl==2)
					$b=$b2;
			}
			else
			{
				$i+=$n;
				if($o>=128)
					$sep=$i;
			}
		}
		//Last chunk
		if($border and is_int(strpos($border,'B')))
			$b.='B';
		$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
		$this->x=$this->lMargin;
	}
}

function txt_jp($text)
{
		
	return 	iconv("SHIFT-JIS", "UTF-8", $text);
}
?>