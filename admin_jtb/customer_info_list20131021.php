<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

$config_action_status = array( '1' => 'Pending' , '2' => 'Send' , '3' => 'Cancel' );

if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/
	
	$data[0] = 'Customer Information';	/*----set h1 name-------*/
	$data[5] = 'customer';  /* system page name */
	
	$data[1] = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'");
	$level_pow =str_replace(",","','",$data[1][1]);	
	
	$page_num = 20;	
	
	//========================================================================
	$search_type = $_POST['search_type'];	
	if($search_type == 'search'){

		if(isset($_POST['start_date']) != NULL){
				$data['start_date']=$_POST['start_date'];
				$_SESSION["session_start_date"] =$_POST['start_date'];
		}	
		if(isset($_POST['end_date']) != NULL){
				$data['end_date']=$_POST['end_date'];
				$_SESSION["session_end_date"] =$_POST['end_date'];
		}		
		if(isset($_POST['c_keyword']) != NULL){
				$_SESSION["session_c_keyword"] =$_POST['c_keyword'];
		}		
		if(isset($_POST['c_status']) != NULL){
				$_SESSION["session_c_status"] =$_POST['c_status'];				
		}
		if(isset($_POST['paid_status']) != NULL){
				$_SESSION["session_order_status"] =$_POST['paid_status'];				
		}		
		if(isset($_POST['product_type']) != NULL){
				$_SESSION["session_product_type"] =$_POST['product_type'];
		}			
		if(isset($_POST['action_status']) != NULL){
				$_SESSION["session_action_status"] = $_POST['action_status'];
		}
	
	}elseif($search_type == 'reset' ){		

		unset($_SESSION["session_start_date"]);
		unset($_SESSION["session_end_date"]);
		unset($_SESSION["session_c_keyword"]);
		unset($_SESSION["session_c_status"]);
		unset($_SESSION["session_order_status"]);
		unset($_SESSION["session_product_type"]);
		
	}		
	//========================================================================	
		
	$where  =" and a.country_iso3 in('".$level_pow."') ";
							//Not On_request
	$where .=" and (a.status_cms ='1' or (a.status_cms='0' and a.paid_type !='3'))";
			
	if(!empty($_SESSION["session_c_status"])){
		$where .=" and a.paid_type ='".$_SESSION["session_c_status"]."' ";	
	}
	if(!empty($_SESSION["session_order_status"])){ 
		switch (trim($_SESSION["session_order_status"]) ){
			case 1 : 
				$where .=" and (a.status_cms_paid ='".$_SESSION["session_order_status"]."' AND (x.order_code IS NULL))"; break;
			case 2 : 
				$where .=" and (a.status_cms_paid ='".$_SESSION["session_order_status"]."' OR a.book_id = x.order_code)"; break;
			case 3 : 
				$where .=" and ( a.status_cms_paid ='".$_SESSION["session_order_status"]."' OR TRIM(x.type)='Cancellation') "; break; 
		}	
	}
	if(!empty($_SESSION["session_action_status"])){
		$where .=" and a.mailto_customer ='".$_SESSION["session_action_status"]."' ";
	}
	
		
	$where .=($_SESSION["session_c_keyword"]!="")? "AND (p.product_name_jp LIKE '%".$_SESSION["session_c_keyword"]."%' OR  (reg_email LIKE '%".$_SESSION["session_c_keyword"]."%') OR a.reg_furi_firstname LIKE '%".$_SESSION["session_c_keyword"]."%' OR a.reg_furi_lastname LIKE '%".$_SESSION["session_c_keyword"]."%')" : "";
	
	$where .=(!empty($_SESSION["session_product_type"]) && $_SESSION["session_product_type"]!="0")? "AND a.book_id IN(SELECT book_id FROM mbus_booking_detail 
															WHERE product_id IN(SELECT product_id FROM mbus_product 
																				WHERE product_type='".$_SESSION["session_product_type"]."'))" : "";	
	$where .=($_SESSION["session_start_date"]!="" && $_SESSION["session_end_date"]!="")? "and a.book_date BETWEEN '".$_SESSION["session_start_date"]."' and  '".$_SESSION["session_end_date"]."' " : "";
			

	
	//$page_count = $db->tt_count_booking($where);	
	$page_count = $db->tt_fetch_booking_count($where);
	
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->tt_fetch_booking($page_first,$page_num,$where);
	
	
	$btn_edit ='<img src="./common/images/edit.jpg" alt="edit" border="0" onmouseover="this.src=\'./common/images/edit_on.jpg\'" onmouseout="this.src=\'./common/images/edit.jpg\'" />';
	$btn_del ='<img class="btn_del" src="./common/images/delete.jpg" alt="delete" border="0" onmouseover="this.src=\'./common/images/delete_on.jpg\'" onmouseout="this.src=\'./common/images/delete.jpg\'" />';
	
	$link_edit = $data[5].'_detail.php?page='.$page_now.'&amp;id=';
	$link_del = $data[5].'_action.php?page='.$page_now.'&amp;id='; 
		
	$data[page_count]=$page_count;
	
	for ($i=0;$i< count($record['book_id']); $i++)
	{				
		$jtb_book_id =(empty($record['jtb_bookind_id'][$i])) ? $record['book_type'][$i].'<br/>'.$record['charge_type'][$i] : $record['book_type'][$i].'<br/>'.$record['charge_type'][$i].' '.$record['jtb_bookind_id'][$i];				
		$cur_sign =$db->get_data('sign','mbus_currency','country_iso3="'.$record['country_iso3'][$i].'"');		
		

		if ($record['book_refid'][$i])
			$book_record = $record['book_id'][$i].'<br/>('.$record['book_refid'][$i].')';
		else
			$book_record = $record['book_id'][$i];
			
		
		$data[3][$i]  = tb_normal( 
		array( $book_record  ,$jtb_book_id,$record['d_date'][$i],$record['product_name_jp'][$i],$record['reg_furi_firstname'][$i],$record['qty_amount'][$i],$cur_sign[0].' '.number_format($record['amount'][$i],2),$record['book_date'][$i],$config_paid_type[$record['paid_type'][$i]],
	$config_paid_status[$record['status_cms_paid'][$i]],$record['place'][$i],$record['mailto_customer'][$i],$btn_edit,$btn_del),
		array('','','','','','','','','','','','',$link_edit.$record['book_id'][$i],$link_del.$record['book_id'][$i].'&amp;action=delete'),
		array(2,1,0,0,1,2,1,1,1,1,1));						
		
	}
	
	/*----set namepage for action check all and delete item-----*/
	foreach ($record['book_id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	
	
	$data[8]  = input_selectbox2('c_status',$config_paid_type,$_SESSION[session_c_status],'----No select----','inp_cx1');		
	$data[9]  = input_selectbox2('product_type',$config_product_type,$_SESSION[session_product_type],'----No select----','inp_cx1');	
	$data[10] = input_selectbox2('paid_status',$config_paid_status,$_SESSION[session_order_status],'----No select----','inp_cx1');			
	$data[11] = input_selectbox2('action_status',$config_action_status,$_SESSION[session_action_status],'----No select----','inp_cx1');

	$data[4]  = pagenavi( $page_now, $page_count ,'customer_info_list.php?page=',$page_num); 
		
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_cus_info_list","../content/themes_backend_jtb/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>