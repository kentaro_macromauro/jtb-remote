<?	

require_once("include/header.php");
require('pdf/tfpdf_class.php');
//require_once("../www_config/setting.php");
require_once("../class_backend/include/c_query.php");

//print_r($_REQUEST);

if ($status == true)
{	

$db = new c_db(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();			

class PDF extends tFPDF_Extend
{
	
	function frm_report2($ff_data,$data_guest)
	{
				
	   //$today = date('F j, Y');
	   $today = date('j F Y');

	   $this->AddFont('SJIS','','TakaoPGothic.ttf',true);
	   //$this->Image('images/logo_pdf.jpg',36.5,25.4,225,45,'JPEG');
	   $this->Image('images/mybus_jtb.jpg',36.5,25.4,146,45,'JPEG');
	   
	   $this->SetFont('SJIS','',20);
	   $this->Text(420,54.2,'予約確認書');
	   $this->SetFont('SJIS','',10);
	   
//Line =========================================================================	   	   
		$this->SetFillColor(192, 192 ,192);
		$this->Rect( 34.6,75.6 , 524.4, 10, 'F');
		
		
//Address Branch =========================================================================	   
	
		$branch_name ='';
		$branch_addr ='';
		$branch_tel ='';
		$branch_fax ='';
		
		switch($ff_data['country_iso3'])
		{
			case 'TWN' : 
			
				$branch_name ='JTB台湾（世帝喜旅行社股份有限公司）';
				$branch_addr ='マイバスデスク 台湾台北市中山北路2段56號JTBラウンジ';
				$branch_tel ='+(886)-02-2521-7315';
				$branch_fax ='+(886)-02-8192-7111';			
			
			/*
			$addr_branch .= '============================================
				JTB台湾（世帝喜旅行社股份有限公司）
				マイバスデスク
				台湾台北市中山北路2段56號JTBラウンジ
				09：00～18：00（予約受付は17：30まで）年中無休
				Tel: +(886)-02-2521-7315 
				Fax:+(886)-02-8192-7111
				<a target="_blank" href="http://www.jtbtaiwan.com">http://www.jtbtaiwan.com</a>
				台湾のマイバスデスクはアンバサダーホテル（國賓飯店）向いのJTBラウンジ内です。
				=============================================
				●”マイバス”のオプショナルツアー（27コース）やお勧めレストラン情報（14箇所）、
				割引特典（12店舗）の掲載されたパンフレットは下記で入手できます。
				・台湾内各主要都市の主なホテル
				● マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
				';*/
	break;	
			
	case 'THA' : 
				
				$branch_name ='JTB (Thailand) Ltd （ジェイティービー　バンコク支店）';
				$branch_addr ='Mybus Operation Harindhorn Building 4th Floor 4B, 54 North Sathorn Road, ';
				$branch_addr2 ='Kwang Silom, Khet Bangrak, Bangkok 10500 Thailand';
				$branch_tel ='+(66)-2267-9236';
				$branch_fax ='+(66)-2266-3156';
			
			/*$addr_branch .= '=============================================
				JTB (Thailand) Ltd （ジェイティービー　バンコク支店）
				Mybus Operation
				Harindhorn Building 4th Floor 4B, 54 North Sathorn Road,
				Kwang Silom, Khet Bangrak, Bangkok 10500 Thailand
				Tel: +(66)-2267-9236
				Fax: +(66)-2266-3156
				<a target="_blank" href="http://www.mybus-asia.com/thailand/" >http://www.mybus-asia.com/thailand/</a>
					
				お問い合わせ、お申し込みは日本語で！
				Tel: +(66)-2632-9404
				=============================================
				
				=============================================
				JTB (THAILAND) LIMITED, Phuket Branch, Mybus Desk
				JTBプーケット営業所 マイバスデスク
				
				Tel. +66 (076) 261-740
				Fax. +66 (076) 261-748
				お電話での予約受付時間 9：00～18：00（年中無休）
						
				e-mail: toiawase_phuket.th@jtbap.com
				<a href="http://www.jtbphuket.com/" target="_blank">http://www.jtbphuket.com/</a>
				<a href="http://www.mybus-asia.com/thailand/" target="_blank">http://www.mybus-asia.com/thailand/</a>
				=============================================
				●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
				'; */
	break;	
			
	case 'VNM' : 
	
				$branch_name ='マイバスデスク・ホーチミン';
				$branch_addr ='9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .';
				$branch_tel ='84-8-3827-7493';
				$branch_fax ='84-8-3824-6515';	
	
				break;	
		
	case 'KHM' : 	
				$branch_name ='マイバスデスク・ホーチミン';
				$branch_addr ='9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .';
				$branch_tel ='84-8-3827-7493';
				$branch_fax ='84-8-3824-6515'; 
				
				break;
			
	case 'CAM' : 
	
				$branch_name ='マイバスデスク・ホーチミン';
				$branch_addr ='9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .';
				$branch_tel ='84-8-3827-7493';
				$branch_fax ='84-8-3824-6515'; 
				
				break;	
		
	case 'MYS' : 
	
				$branch_name ='JTB (M) SDN BHD （ジェイティービー　マレーシア支店）';
				$branch_addr ='ＫＬオフィス　(メリアホテル横、AMODAビル16階）';
				$branch_addr2 ='16.05-16.07 Level 16, Amoda Bldg. 22 Jalan Imbi 55100 Kuala Lumpur Malaysia ';									   
				$branch_tel ='+(603) 2142-8727';
				$branch_fax ='+(603) 2142-5344';

		/*$addr_branch .= '====================================
JTB (M) SDN BHD （ジェイティービー　マレーシア支店）
	
ＫＬオフィス　(メリアホテル横、AMODAビル16階）
16.05-16.07 Level 16, Amoda Bldg.
22 Jalan Imbi 55100 Kuala Lumpur
Malaysia 
Tel: +(603) 2142-8727
Fax: +(603) 2142-5344
		
ランカウイ：＋（604）966-4708
ペナン　　：＋（604）263-5788
コタキナバル：＋（6088）259-668
http://www.mybus-asia.com/malaysia/
======================================'; */break;
			
	case 'SGP' : 
	
				$branch_name ='JTB Pte Ltd Singapore';
				$branch_addr ='Mybus Desk/マイバスデスク（DFS免税品店内2階）';
				$branch_tel ='(65)-6735－2847';
				$branch_fax ='(65)-6733－8148';

		/*$addr_branch .= '===============================
JTB Pte Ltd Singapore
Mybus Desk/マイバスデスク
（DFS免税品店内2階）
Tel: (65)-6735－2847 
Fax: (65)-6733－8148
受付時間：10：00－17：30（年中無休）
<a target=_blank" href="http://www.jtbsingapore.com">http://www.jtbsingapore.com</a>
e-mail:opjtb.sg@jtbap.com
================================'; 
	*/break;	
			
	case 'HKG' : 
	
				$branch_name ='MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光）';
				$branch_addr ='Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., ';
				$branch_addr2 ='Tsim Sha Tsui, Kowloon, Hong Kong. ';
				$branch_tel ='(852)2375-7576';
				$branch_fax ='(852)2311-5803';

		/*$addr_branch .=  '============================================ 
MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光） 
Add     : Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., 
	  　　Tsim Sha Tsui, Kowloon, Hong Kong. 
Tel      : (852)2375-7576 (*日本語対応　年中無休09:00-18:00)
Fax     : (852)2311-5803 
Mail    : inbound@hk.jtb.cn 
Web    : <a target=_blank" href="http://www.mybus-asia.com/hongkong/">http://www.mybus-asia.com/hongkong/</a>
============================================= 
●”マイバス香港”は、 
　ビギナーからリピーター向けオプショナルツアー（44コース）、 
　便利で安心のスパ・マッサージ（8店舗）、 
　あの憧れのレストランをはじめ厳選のミールクーポン（43コース）、 
　割引特典レストラン（3店舗）をご用意しています。 
●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、 
　バリ、プーケット、ベトナム）で年中無休で運行中。 '; */break;
	
	case 'AUS' : 
	
				$branch_name ='JTB Australia Pty Ltd';
				$branch_addr ='ケアンズ マイバスデスク 61　Abbott　Street, Cairns';
				$branch_tel ='+61-(07)-4052-5872 ';
				$branch_fax ='';

		/*$addr_branch .=  '===========================================
JTB Australia Pty Ltd
		
ケアンズ マイバスデスク
電話　+61-(07)-4052-5872 
毎日 9：00～19：00
61　Abbott　Street, Cairns
	
ゴールドコーストマイバスデスク
電話　+61-(07)-5592-9491 
毎日 8：30～18：30 
Ground Floor, 3191 Surfers Paradise Boulevard
		
シドニー
電話　+61-(02)-9510-0313 
毎日 09:00 ～ 17:00
		
メルボルン
電話　+61(03)-8623-0091 
土日祝日を除く毎日 10:00 ～12:00/14:00 ～16:00 
Level 6, 10 Artemis Lane, Melbourne
(QVショッピングコンプレックス内) 
		
パース
電話　+61(08)-9218-9866 
毎日 09:00 ～ 12:00/13:00 ～ 17:30
Pan Pacific Perth内, 207 Adelaide Terrace, Perth
isdweb.au@jtbap.com
============================================'; */break;	
			
	case 'NZL' : 
	
				$branch_name ='JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）';
				$branch_addr ='Auckland Mybus Centre Level 5, 191 Queen St, Auckland';
				$branch_tel ='+(64)-9379-6415';
				$branch_fax ='+(64)-9309-7699';

		/*$addr_branch .=  '===================================================================
JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）
Auckland Mybus Centre
Level 5, 191 Queen St, Auckland
Tel: +(64)-9379-6415 
Fax: +(64)-9309-7699
<a target=_blank" href="http://www.jtb.co.nz/">http://www.jtb.co.nz/</a>
===================================================================
●マイバスセンター　オークランド支店は平日　9：00〜17：30の営業です。
（土日祝日はお休みとなります。）
'; */break;

		}		
//Head =========================================================================
		//$branch_addr_length =strlen($branch_addr);				
		$top_row =100;

		$weeklist = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'); 
		
		//$date_today = date('w',date('Y-m-d'));
		$date_today = date('w');
		$date_book = date('w',strtotime($ff_data['update_date']));		
				
		//echo $date_today; exit;
		$date_today_week = $weeklist[$date_today];
		$date_book_week = $weeklist[$date_book];
		
	   //$this->Text(35,100, txt_jp("JTBロゴ"));
	   $this->Text(35,$top_row,$branch_name);
	   
	   $date_booking =str_replace('-',' ',date('d-M-Y',strtotime($ff_data['update_date'])));
	   $this->Text(420,$top_row, $today.' ('.$date_today_week.')');
	   
	   //$this->Text(35,115,$branch_addr_length.' /'.$branch_addr);
	   $this->Text(35,$top_row+15,$branch_addr);
	   
	   if(!empty($branch_addr2)){
		   $this->Text(35,$top_row+30,$branch_addr2);
		   $top_row =$top_row+15;
		}
		
	   $this->Text(35,$top_row+30,'TEL : '.$branch_tel);
	   $this->Text(300,$top_row+30,'FAX : '.$branch_fax);
	   
	   $text_left ='35';	   
	   $sex_g =array("1"=>"Mr","2"=>"Ms");
	   
	   //$product_jp_en =$ff_data['product_name_en'].' / '.$ff_data['product_name_jp'];
	   $product_jp =$ff_data['product_name_jp'];
	   $product_en =$ff_data['product_name_en'];
	   
	   $web_id =$ff_data['jtb_bookind_id'];
	   $tour_name =$ff_data['reg_furi_firstname'].' '.$ff_data['reg_furi_lastname'];
	   	   
	   $this->Text($text_left,$top_row+50,'To : '.$sex_g[$ff_data[reg_sex]].' '.$tour_name);
	   //$this->Text($text_left+22,$top_row+70,''.$product_jp);
	   
	   $this->Text($text_left,$top_row+70,'Web ID (Booking ID) : '.$web_id);				
	   $this->Text($text_left,$top_row+90,'Tour ID : '.$ff_data['tour_id']);				
	   $this->Text($text_left,$top_row+105,'Tour Name : '.$product_en);
	   $this->Text($text_left+60,$top_row+120,''.$product_jp);
				   
				   
				   
				   				

	   $this->Text($text_left,$top_row+145,$ff_data['product_name_en']);
	   
//Voucher =========================================================================	   

	   //,b.qty_adults,b.qty_child,b.qty_infant

	   $this->Text($text_left,$top_row+170,'<Confirmation>');	   
	   $this->Text($text_left,$top_row+190,'Date : '.$date_booking.' ('.$date_book_week.')');	   
	   $this->Text($text_left,$top_row+210,'Time : ');	   
	   $this->Text($text_left,$top_row+230,'Number of Customer : Adult '.number_format($ff_data['qty_adults'],0).'  Child '.number_format($ff_data['qty_child'],0).'  Infant '.number_format($ff_data['qty_infant'],0));

//Guest =========================================================================
	   $this->Text($text_left,$top_row+250,'First Name');	   
	   $this->Text($text_left+300,$top_row+250,'Family Name');	   
	
		//$_guest[${$fg[guest_firstname]}][$ig] =$fg[guest_firstname];
		//$_guest[${$fg[guest_lastname]}][$ig] =$fg[guest_lastname];	
		
		$i=0;
		//$sex_g =array("1"=>"Ms","2"=>"Mr");
		
		$topg =$top_row+270;
		
		/**/
		foreach($data_guest as $k=>$v){		
		
		//echo "$k=>$v";

		   $this->Text($text_left,$topg,$i+'1'.'. '.$sex_g[$data_guest[$i][guest_sex]].' '.$data_guest[$i][guest_firstname]);
		   //$this->Text($text_left,$topg,$data_guest[$i][guest_firstname]);
		   
		   //$this->Text($text_left+150,$topg,$sex_g[$data_guest[$i][guest_sex]]);	   
		   $this->Text($text_left+300,$topg,$data_guest[$i][guest_lastname]);   
			
		   $i++;
		   $topg +='15';
		}	
		
//Payment Information ===========================================================

	   $this->Text($text_left,$top_row+400,'Payment Information');	   
	   
	   $this->Text($text_left+20,$top_row+430,'Payment : '.$ff_data['paid_name']);	   
	   
	   
	   //				left	top	 width	height  type
	   $this->Rect( $text_left,$top_row+410 , 524.4, 140, 'B');
	
//=========================================================================
	
		$this->Text(432.2,788.6,'');
		$this->Text(420,800,$shop_data[0]['shop_manager']);	
		$this->Line(395.8, 801.7, '559', 801.7 );
		
		//$this->Text($text_left,799.4,'02 652 5352');	$this->Text(159,799.4,'URL : www.pptours-bkk.com');
	   
	   	$this->Rect( 34.6,809.5 , 524.4, 10, 'F');	   
	}
}
				
	//==================================================================================
	$sql = "SELECT b.book_id,b.country_iso3,b.book_type,date(b.book_date) b_date,b.jtb_bookind_id,b.jtb_statuscode
					,b.transection_id,b.amount,b.currency,b.paid_type,b.paid_status,b.tour_id
					,b.qty_adults,b.qty_child,b.qty_infant,b.qty_amount
					,b.reg_furi_firstname,b.reg_furi_lastname,b.reg_sex,b.reg_addrjp_zip1,b.reg_addrjp_zip2
					,reg_addrtype,reg_addrjp_city,reg_addrjp_area,reg_addrjp_pref,reg_addrjp_building
					,reg_tel1,reg_tel2,reg_tel3,reg_mobile1,reg_mobile2,reg_mobile3,reg_backup_addrovs1
					,reg_backup_tel1,reg_backup_tel3,reg_backup_tel3,reg_birthday					
					,reg_hotel_id,reg_arrfrm,reg_arrto,reg_arrgo,reg_airno,reg_info,b.remark
					,reg_addrtype,reg_addrovs1,reg_addrovs2
					,reg_hoteltype,reg_hotel_name,reg_hotel_tel,reg_hotel_addr1					
					,b.reg_firstname,b.reg_lastname,b.reg_email,d.product_id
					,b.amount,s.paid_name
					,p.city_iso3,p.product_code,p.product_name_jp,p.product_name_en
					,DATE(b.update_date) AS update_date					
				FROM mbus_booking b
				LEFT JOIN mbus_booking_detail d ON b.book_id=d.book_id
				LEFT JOIN mbus_product p ON p.product_id=d.product_id
				LEFT JOIN mbus_paidstatus s ON s.paid_id=b.paid_type
				WHERE b.book_id='".$_GET['b_id']."'
			";			
				
	//echo $sql; //exit;		
	$r0 = $db->db_query($sql);		
	$ff_data = mysql_fetch_array($r0);	
	
	//================================================================
	$sqlg = "SELECT * FROM mbus_booking_guest WHERE book_id='".$_GET['b_id']."'";						
	$ig=0;
	
	//echo $sqlg; //exit;		
	$rg = $db->db_query($sqlg);
	/**/
	while ($fg = mysql_fetch_array($rg)){		
					
			//guest_firstname,guest_lastname
			$data_guest[$ig]['guest_sex'] =$fg[guest_sex];
			$data_guest[$ig]['guest_firstname'] =$fg[guest_firstname];
			$data_guest[$ig]['guest_lastname'] 	=$fg[guest_lastname];	
			
			$ig++;		
	}
			
	//print_r($_guest);
	//================================================================
	$pdf=new PDF('P','pt','A4');
	
	//for($i=1;$i<=$count; $i++){
		
		$pdf->Write(0,'');
		$pdf->AddPage();	
		
		$pdf->frm_report2($ff_data,$data_guest);
	//}

	if($_GET['pdf_type'] == 'print'){	
		
		$pdf->Output();
	}

	if($_GET['pdf_type'] == 'download'){
	
			require_once("../class_backend/c_themes.php");
			//require_once("../class_backend/c_query_sub.php");
			//echo "book_type=>>".$ff_data['book_type']; exit;
			
			//============================================================================			
			$sql = "SELECT * FROM mbus_admin_level WHERE level_id='".$_SESSION[session_userid]."'";						
			
			//echo $sql; //exit;		
			$rs = $db->db_query($sql);
			$ff = mysql_fetch_array($rs);
			
			$data['level_name'] =$ff[level_name];
			$data['level_pow'] 	=$ff[level_pow];
			
			
			//============================================================================						
			$pdf->Output('./pdf_'.strtolower($ff_data['book_type']).'/'.$ff_data['jtb_bookind_id'].'.pdf');
				
			if (file_exists( './pdf_'.strtolower($ff_data['book_type']).'/'.$ff_data['jtb_bookind_id'].'.pdf' )){
				
				$data['result'] ="Complete Create Making ".strtoupper($ff_data['book_type'])." Voucher";
			}
			else{
				
				$data['result'] ="Create Making ".strtoupper($ff_data['book_type'])." Voucher Faild..";
			}	
									
			$data['dir'] 			='./pdf_'.$ff_data['book_type'].'/';
			$data['c_booking_id'] 	=$ff_data['jtb_bookind_id'];
			$data['type'] 			=$ff_data['book_type'];
			$data['b_id'] 			=$_GET['b_id'];
			
			$themes = new c_themes("backend_cus_complete_vouchar","../content/themes_backend_jtb/");
			$themes->pbody($data);
			
	}
	
	if($_GET['pdf_type'] != 'print' ){

		/*
		$mysmarty->assign('dir','./pdf_'.$_GET['type'].'/');
		$mysmarty->assign('c_booking_id',$where['c_booking_id']);
	
		$mysmarty->assign('count',$count);
		$mysmarty->assign('type',$_GET['type']);
		$mysmarty->assign('c_id',$_GET['c_id']);

		$mysmarty->display( 'customer_complete_vouchar.html' );
		*/				
	}
}
else
{
	header("Location: index.php");
}
?>