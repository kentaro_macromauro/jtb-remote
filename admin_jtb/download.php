<? 

	//print_r($_GET);

	if (!empty($_GET[path])&&!empty($_GET[name]))
	{

		$folder = $_GET[path];		
		$file   = $_GET[name];
						
		$file_real = $folder.'/'.$file.'.pdf';
		
		if (file_exists($file_real))
		{
		
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public", false);
			header("Content-Description: File Transfer");
			header("Content-Type: application/force-download");
			header("Accept-Ranges: bytes");
			header("Content-Disposition: attachment; filename=\"" . $file.'.pdf' . "\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . filesize($file_real));

			// Send file for download
			if ($stream = fopen($file_real, 'rb')){
				while(!feof($stream) && connection_status() == 0){
					//reset time limit for big files
					set_time_limit(0);
					print(fread($stream,1024*8));
					flush();
				}
			    
				fclose($stream);
			}
		}
		else
		{
			echo 'File not found';
		}	
		
	}
	else
	{
		echo 'File not found';	
	}
?>