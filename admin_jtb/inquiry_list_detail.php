<? 
include("include/header.php");
include($path."class_backend/c_action.php");
include($path."class_backend/c_query_sub.php");

function date_jp($date)
{
	$arr_date = explode('-',$date);
	$date_jp  = ($arr_date[0]*1).'年'.($arr_date[1]*1).'月'.($arr_date[2]*1).'日'; 
	
	return $date_jp;
}

	
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();	


$c_id = $_REQUEST[id];

$sql = 'SELECT country_iso3,branch,first_name,last_name,furi_first_name, furi_last_name,email,depature_date,jtb_no,comment,update_date FROM mbus_jtb_contact WHERE  c_id  = "'.$c_id.'" ';

$result = $db->db_query($sql);

$rec = array();

while ($record = mysql_fetch_array($result)){
	$rec = $record ;
}


switch ( $rec[country_iso3] ){
	case 'TWN' : $country_name = '台湾'; break;	
	case 'THA' : $country_name = 'タイ'; break;	
	case 'SGP' : $country_name = 'シンガポール'; break;	
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
</head>
<style>
.table_data td:first-child { text-align:left; text-indent:10px; }
</style>
<body>
<!---->
<div class="data">
	<div class="table" align="center">
		<table x:str BORDER="0" width="800" align="center" class="table_data" cellpadding="0" cellspacing="1">
			<tr>
				<td colspan=2 align=center height=30 style='text-align:center;'><b>Inquiry list Details</b></td>
			</tr>
			<tr>
				<td align=right >Location </td>
				<td align=left >Country : <?=$country_name?>&nbsp;&nbsp;&nbsp;&nbsp;
					<? 
						if ( $rec[country_iso3] == 'THA'){
					?>  
						City : <?=$rec[branch]?>
					
					<? }else{
						echo '&nbsp;&nbsp;&nbsp;&nbsp';
					} ?>
				
				</td>
			</tr>
			<tr>
				<td align=right >氏名（漢字）</td>
				<td align=left ><?=$rec[last_name].'　'.$rec[first_name]?></td>
			</tr>
			<tr>
				<td align=right >氏名（フリガナ）</td>
				<td align=left  ><?=$rec[furi_last_name].'　'.$rec[furi_first_name]?></td>
			</tr>
			<tr>
				<td align=right >メールアドレス</td>
				<td align=left  ><?=$rec[email]?></td>
			</tr>
			<tr>
				<td align=right >日本出発日</td>
				<td align=left  ><?=date_jp($rec[depature_date])?></td>
			</tr>
			<tr>
				<td align=right >コースNo. </td>
				<td align=left  ><?=$rec[jtb_no]?></td>
			</tr>
			<tr>
				<td align=right >お問合せ内容 </td>
				<td align=left  ><?=nl2br($rec[comment])?></td>
			</tr>
			<tr>
				<td align=right valign="top" >Submitted Date</td>
				<td align=left  ><?=$rec[update_date]?></td>
			</tr>
			
			<tr>
				<td align=center colspan=7 style='text-align:center;'>
					<input type=button name=bu_close value='Close' onClick='func_close();'>
				</td>
			</tr>
		</table>
		</table>
	</div>
</div>
<script language="javascript">

function func_close()
{
	close();
	//location ='inquiry_list.php';
}
</script>
</body>
</html>