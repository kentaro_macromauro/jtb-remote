<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{
	if(!empty($_GET[send_excel])) {	 //if document type excel
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment; filename="report.xls"');
		?>       
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<meta http-equiv=Content-Type content="text/html; charset=utf-8"> 
        <?
	}
	else { //if document type html
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Axes Reports</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
</head>
<? 
	} // end if document type
?>
<BODY>
<!---->
<!---->
<div class="data">
<div class="table" align="center">
<!--Have line -->
<?
	if(!empty($_GET[send_excel]))
	{
?>
	<TABLE x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="0">
<?	
	}
	else
	{
?>
	<TABLE x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
<? 
	}

require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");
	
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();	
	
	$option_show = $_REQUEST[option_show];
	$start_date  = $_REQUEST[start_date];
	$end_date    = $_REQUEST[end_date];
	
	$option_type = $_REQUEST[option_type];
	$opiton_book = $_REQUEST[option_book];
	
	$raw_data = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'") ;
	
	
	$country_list = str_replace(",","','",$raw_data[1]);
	
	if ($_SESSION[session_level] == "A")
	{
		$country_list .= "','TEST";
	}
	
if ($option_show == "monthly"){
	
	if(empty($_GET[send_excel])){ //if document type = html
	?>
	<tr>
	<td colspan="5" align=center height=30><b>Axes Reports Log From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
	<tr>
    	<th align=center nowrap style='width:50px; table-style:fix;'><strong>Country</strong></th>
		<th align=center nowrep style='width:30px; table-style:fix;'><strong>Update Date</strong></th>
        <th align=center nowrap style='width:50px; table-style:fix;'><strong>Result</strong></th>
		<th align=center nowrap style='width:30px; table-style:fix;'><strong>Amount</strong></th> 
        <th align=center nowrap style='width:30px; table-style:fix;'><strong>Currency</strong></th>	 	 	 	
	</tr>
	<?	}
		else { // if document type = excel
	?>
	<tr><td colspan="5" align=center height=30><b>Axes Monthly Reports From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
	<tr>
		<th align=center width=20% align=center nowrap><strong>Country</strong></th>
		<th align=center width=20% align=center nowrap><strong>Update Date</strong></th>
         <th align=center width=10% align=center nowrap><strong>Result</strong></th>
		<th align=center width=10% align=center nowrap><strong>Amount</strong></th>
		<th align=center width=10% align=center nowrap><strong>Currency</strong></th>	
	</tr>
	<?
	} //end if document type
		
	$sql = "SELECT item,update_date,currency,result,date_stamp,country_iso3,country_name_jp,SUM(amount_int) amount_int  FROM 
			((SELECT '0' item, IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,IF (country_iso3 IS NULL,IF (site_code='A019000014',currency,''),currency) currency,SUM(amount_int) amount_int,result,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%M-%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE (DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."')) AS aa 
			GROUP BY update_date,site_code,currency,result,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) ) AS aa
			WHERE ((country_iso3 IS NULL) || (country_iso3 IN ('".$country_list."')))  
			GROUP BY item,update_date,currency,result,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp,item,country_iso3,result DESC	"	;	
			

			
	
	$result = $db->db_query($sql); $i=0; 
	
	while ($record = mysql_fetch_array($result))
	{
		$update_date   = $record[update_date];
		$country_name  = $record[country_name_jp];
		$amount        = number_format($record[amount_int],2,'.','');
		$currency      = $record[currency];
		$record_result = $record[result];
		
		if (empty($country_name)) $country_name = "Not found.";
        
		if ($record_result == "zz")
		{?>
			<tr>
				<td align="left" style="background:#EEE;"><?=$country_name?></td>
				<td align="left" colspan="2"style="background:#EEE;">Subtotal local currency </td>
				<td align="right" style="background:#EEE;"><?=number_format($amount,2)?></td>
				<td align="left" style="background:#EEE;"><?=$currency?></td>
			</tr>
		<?	
		}
		else
		{
		?>
			<tr>
				<td align="left"><?=$country_name?></td>
				<td align="left"><?=$update_date?></td>
				<td align="left"><?=$record_result?></td>
				<td align="right"><?=number_format($amount,2)?></td>
				<td align="left"><?=$currency?></td>
			</tr>
		<?	
		}
		
		
		
		$i++;
	}
	
	$sql = "SELECT aaa.* FROM 
			((SELECT '0' state,country_name_jp,aa.country_iso3,currency,amount_int,result,
			IF (bb.rate IS NULL,cc.rate,bb.rate) rate,IF (bb.code IS NULL,cc.code,bb.code ) code 
			FROM (SELECT country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result FROM 
			(SELECT '1' item, 
			IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',currency,''),currency) currency,
			SUM(amount_int) amount_int,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',result,''),result) result,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%M-%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."') AS aa 
			GROUP BY update_date,site_code,currency,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) AS aa
			GROUP BY country_name_jp,country_iso3,currency,result ) AS aa 
			LEFT JOIN mbus_currency bb ON aa.country_iso3 = bb.country_iso3 
			LEFT JOIN mbus_currency cc ON aa.currency     = cc.code )
			UNION ALL
			(SELECT state,country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result,rate,code FROM 
			((SELECT '1' state,country_name_jp,aa.country_iso3,currency,amount_int,'grand' result,
			IF (bb.rate IS NULL,cc.rate,bb.rate) rate,IF (bb.code IS NULL,cc.code,bb.code ) code 
			FROM (SELECT country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result FROM 
			(SELECT '1' item,
			IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',currency,''),currency) currency
			,SUM(amount_int) amount_int,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',result,''),result) result
			,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%M-%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."') AS aa 
			GROUP BY update_date,site_code,currency,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) AS aa
			GROUP BY country_name_jp,country_iso3,currency,result ) AS aa 
			LEFT JOIN mbus_currency bb ON aa.country_iso3 = bb.country_iso3 
			LEFT JOIN mbus_currency cc ON aa.currency     = cc.code )
			ORDER BY country_iso3,state,result DESC ) AS aaa 
			GROUP BY state,country_name_jp,country_iso3,currency,result,rate,code)) AS aaa 
			WHERE ((country_iso3 IS NULL) || (country_iso3 IN ('".$country_list."')))   
			ORDER BY country_iso3,state,result DESC ";
			
	$result = $db->db_query($sql);  $amount_all = 0;
	
	while ($record = mysql_fetch_array($result))
	{
		$country_name  = $record[country_name_jp];
		$record_result = $record[result];
		$amount	       = $record[amount_int];
		$currency      = $record[code];
		$rate 		   = 0;
		$rate          = $record[rate];
		
		$amount_all   += number_format($amount * $rate,2,'.','');	
		
		if ($record_result == 'grand'){ 
		?>
        	<tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;" colspan="2">Grand Total local currency</td>
                <td align="right" style="background:#FFC;"><?=number_format($amount,2)?></td>
                <td align="left"  style="background:#FFC;"><?=$currency?></td>
            </tr>
            <tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;" colspan="2">Grand Total local JPY</td>
                <td align="right" style="background:#FFC;"><?=number_format($amount * $rate,2)?></td>
                <td align="left"  style="background:#FFC;">JPY</td>
            </tr>
        <?
		} else{
		?>
        	<tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;">Total local currency </td>
                <td align="left"  style="background:#FFC;"><?=$record_result?></td>
                <td align="right" style="background:#FFC;"><?=number_format($amount,2)?></td>
                <td align="left"  style="background:#FFC;"><?=$currency?></td>
            </tr>
        <? }	        
	}
	
	$sql = 'SELECT rate FROM mbus_currency WHERE country_iso3="SGP"';
	$result = $db->db_query($sql);
	
	while ($record = mysql_fetch_array($result))
	{
		$sgp_rate = $record[rate];	
	}

	if ($_SESSION[session_level] == "A"){ 
	?> 
    	<tr>
            <td align="left"  style="background:#FFC;" colspan="3">All countries Grand total JPY </td>
            <td align="right" style="background:#FFC;"><?=number_format($amount_all,2)?></td>
            <td align="left"  style="background:#FFC;">JPY</td>
        </tr>
        <tr>
            <td align="left"  style="background:#FFC;" colspan="3">All countries Grand total SGD</td>
            <td align="right" style="background:#FFC;"><?=number_format(($amount_all / $sgp_rate),2)?></td>
            <td align="left"  style="background:#FFC;">SGD</td>
        </tr>
    <?
	}
	
}	
	
	
if ($option_show == "daily"){
	
	if(empty($_GET[send_excel])){ //if document type = html
	?>
	<tr>
	<td colspan="5" align=center height=30><b>Axes Reports Daily From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
	<tr>
    	<th align=center nowrap style='width:50px; table-style:fix;'><strong>Country</strong></th>
		<th align=center nowrep style='width:30px; table-style:fix;'><strong>Update Date</strong></th>
        <th align=center nowrap style='width:50px; table-style:fix;'><strong>Result</strong></th>
		<th align=center nowrap style='width:30px; table-style:fix;'><strong>Amount</strong></th> 
        <th align=center nowrap style='width:30px; table-style:fix;'><strong>Currency</strong></th>	 	 	 	
	</tr>
	<?	}
		else { // if document type = excel
	?>
	<tr><td colspan="5" align=center height=30><b>Axes Daily Reports From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
	<tr>
		<th align=center width=20% align=center nowrap><strong>Country</strong></th>
		<th align=center width=20% align=center nowrap><strong>Update Date</strong></th>
         <th align=center width=10% align=center nowrap><strong>Result</strong></th>
		<th align=center width=10% align=center nowrap><strong>Amount</strong></th>
		<th align=center width=10% align=center nowrap><strong>Currency</strong></th>	
	</tr>
	<?
	} //end if document type
		
			
	$sql = "SELECT item,update_date,site_code,currency,result,date_stamp,country_iso3,country_name_jp,amount_int  FROM 
			((SELECT '0' item, IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,currency,SUM(amount_int) amount_int,result,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%d/%m/%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m-%d') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE (DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."')) AS aa 
			GROUP BY update_date,site_code,currency,result,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) UNION ALL 
			(SELECT '1' item, IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,currency,SUM(amount_int) amount_int,'zz' result,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%d/%m/%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m-%d') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE (DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."')) AS aa 
			GROUP BY update_date,site_code,currency,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp)) AS aa
			WHERE ((country_iso3 IS NULL) || (country_iso3 IN ('".$country_list."')))   
			ORDER BY date_stamp,item,country_iso3,result DESC";

	$result = $db->db_query($sql); $i=0; 
	
	while ($record = mysql_fetch_array($result))
	{
		$update_date   = $record[update_date];
		$country_name  = $record[country_name_jp];
		$amount        = number_format($record[amount_int],2,'.','');
		$currency      = $record[currency];
		$record_result = $record[result];
		
		if (empty($country_name)) $country_name = "Not found.";
        
		if ($record_result == "zz")
		{?>
			<tr>
				<td align="left" style="background:#EEE;"><?=$country_name?></td>
				<td align="left" colspan="2"style="background:#EEE;">Subtotal local currency </td>
				<td align="right" style="background:#EEE;"><?=number_format($amount,2)?></td>
				<td align="left" style="background:#EEE;"><?=$currency?></td>
			</tr>
		<?	
		}
		else
		{
		?>
			<tr>
				<td align="left"><?=$country_name?></td>
				<td align="left"><?=$update_date?></td>
				<td align="left"><?=$record_result?></td>
				<td align="right"><?=number_format($amount,2)?></td>
				<td align="left"><?=$currency?></td>
			</tr>
		<?	
		}
		
		
		
		$i++;
	}
	
	$sql = "SELECT aaa.* FROM 
			((SELECT '0' state,country_name_jp,aa.country_iso3,currency,amount_int,result,
			IF (bb.rate IS NULL,cc.rate,bb.rate) rate,IF (bb.code IS NULL,cc.code,bb.code ) code 
			FROM (SELECT country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result FROM 
			(SELECT '1' item, 
			IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',currency,''),currency) currency,
			SUM(amount_int) amount_int,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',result,''),result) result,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%d/%m/%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m-%d') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."') AS aa 
			GROUP BY update_date,site_code,currency,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) AS aa
			GROUP BY country_name_jp,country_iso3,currency,result ) AS aa 
			LEFT JOIN mbus_currency bb ON aa.country_iso3 = bb.country_iso3 
			LEFT JOIN mbus_currency cc ON aa.currency     = cc.code )
			UNION ALL
			(SELECT state,country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result,rate,code FROM 
			((SELECT '1' state,country_name_jp,aa.country_iso3,currency,amount_int,'grand' result,
			IF (bb.rate IS NULL,cc.rate,bb.rate) rate,IF (bb.code IS NULL,cc.code,bb.code ) code 
			FROM (SELECT country_name_jp,country_iso3,currency,SUM(amount_int) amount_int,result FROM 
			(SELECT '1' item,
			IF (site_code='A019000014','Test',country_name_jp) country_name_jp,
			IF (site_code='A019000014','Test',country_iso3) country_iso3
			,update_date,site_code,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',currency,''),currency) currency
			,SUM(amount_int) amount_int,
			IF (country_iso3 IS NULL,IF (site_code='A019000014',result,''),result) result
			,date_stamp FROM 
			(SELECT country_name_jp,c.country_iso3,DATE_FORMAT(a.update_date,'%d/%m/%Y') AS update_date,result_id,
			order_code,site_code,currency,amount,REPLACE(amount,'%2e','.') amount_int,type,
			telephone_no,email,result, user_id,optional,IF (book_id,'On Request','') status_type,
			DATE_FORMAT(a.update_date,'%y-%m-%d') date_stamp  
			FROM mbus_payment_result a LEFT JOIN 
			(SELECT book_id FROM mbus_booking 
			WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN mbus_payment_connect c ON a.site_code = c.sitecode 
			LEFT JOIN mbus_country d ON c.country_iso3 = d.country_iso3 
			WHERE DATE_FORMAT(a.update_date,\"%Y-%m-%d\") BETWEEN '".$_REQUEST[start_date]."' AND '".$_REQUEST[end_date]."') AS aa 
			GROUP BY update_date,site_code,currency,date_stamp,country_iso3,country_name_jp 
			ORDER BY date_stamp) AS aa
			GROUP BY country_name_jp,country_iso3,currency,result ) AS aa 
			LEFT JOIN mbus_currency bb ON aa.country_iso3 = bb.country_iso3 
			LEFT JOIN mbus_currency cc ON aa.currency     = cc.code )
			ORDER BY country_iso3,state,result DESC ) AS aaa 
			GROUP BY state,country_name_jp,country_iso3,currency,result,rate,code)) AS aaa 
			WHERE ((country_iso3 IS NULL) || (country_iso3 IN ('".$country_list."')))   
			ORDER BY country_iso3,state,result DESC ";
	
	$result = $db->db_query($sql);  $amount_all = 0;
	
	while ($record = mysql_fetch_array($result))
	{
		$country_name  = $record[country_name_jp];
		$record_result = $record[result];
		$amount	       = $record[amount_int];
		$currency      = $record[code];
		$rate 		   = 0;
		$rate          = $record[rate];
		
		$amount_all   += number_format($amount * $rate,2,'.','');	
		
		if (empty($country_name)) $country_name = "Not found.";
		
		if ($record_result == 'grand'){ 
		?>
        
        
        	<tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;" colspan="2">Grand Total local currency</td>
                <td align="right" style="background:#FFC;"><?=number_format($amount,2)?></td>
                <td align="left"  style="background:#FFC;"><?=$currency?></td>
            </tr>
            <tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;" colspan="2">Grand Total local JPY</td>
                <td align="right" style="background:#FFC;"><?=number_format($amount * $rate,2)?></td>
                <td align="left"  style="background:#FFC;">JPY</td>
            </tr>
        <?
		} else{
		?>
        	<tr>
                <td align="left"  style="background:#FFC;"><?=$country_name?></td>
                <td align="left"  style="background:#FFC;">Total local currency </td>
                <td align="left"  style="background:#FFC;"><?=$record_result?></td>
                <td align="right" style="background:#FFC;"><?=number_format($amount,2)?></td>
                <td align="left"  style="background:#FFC;"><?=$currency?></td>
            </tr>
        <? }	        
	}
	
	$sql = 'SELECT rate FROM mbus_currency WHERE country_iso3="SGP"';
	$result = $db->db_query($sql);
	
	while ($record = mysql_fetch_array($result))
	{
		$sgp_rate = $record[rate];	
	}

	if ($_SESSION[session_level] == "A"){ 
	?> 
    	<tr>
            <td align="left"  style="background:#FFC;" colspan="3">All countries Grand total JPY </td>
            <td align="right" style="background:#FFC;"><?=number_format($amount_all,2)?></td>
            <td align="left"  style="background:#FFC;">JPY</td>
        </tr>
        <tr>
            <td align="left"  style="background:#FFC;" colspan="3">All countries Grand total SGD</td>
            <td align="right" style="background:#FFC;"><?=number_format(($amount_all / $sgp_rate),2)?></td>
            <td align="left"  style="background:#FFC;">SGD</td>
        </tr>
    <?
	}
	
}




if ($option_show == "log"){ //if log report start;
	
	if(!$_GET[send_excel]){ //if document type = excel
?>
<tr>
<td colspan="13" align=center height=30><b>Axes Reports Log From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
<tr>
    <th align=center style='width:40px; table-style:fix;'><strong>Item</strong></th>
    <th align=center nowrep style='width:120px; table-style:fix;'><strong>Update Date</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Status</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Order code</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Site code</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Currency</strong></th>
    <th align=center nowrap style='width:80px; table-style:fix;'><strong>Amount</strong></th> 	 	 	 	
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Telephone no</strong></th>
    <th align=center nowrap style='width:30px; table-style:fix;'><strong>Email</strong></th>
    <th align=center nowrap style='width:40px; table-style:fix;'><strong>Result</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>Type</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>User Id</strong></th>
    <th align=center nowrap style='width:20px; table-style:fix;'><strong>Optional</strong></th>
</tr>
<?	}
	else { // if document type = html
?>
<tr><td colspan="13" align=center height=30><b>Axes Log Reports From <?=$_REQUEST[start_date]?> To <?=$_REQUEST[end_date]?></b></td></tr>
<tr>
    <th align=center width=5% align=center nowrap><strong>Item</strong></th>
    <th align=center width=20% align=center nowrap><strong>Update Date</strong></th>
     <th align=center width=20% align=center nowrap><strong>Status</strong></th>
    <th align=center width=10% align=center nowrap><strong>Order code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Site code</strong></th>
    <th align=center width=10% align=center nowrap><strong>Currency</strong></th>
    <th align=center width=10% align=center nowrap><strong>Amount</strong></th>
    <th align=center width=10% align=center nowrap><strong>Telephone</strong></th>
    <th align=center width=10% align=center nowrap><strong>Email</strong></th>
    <th align=center width=10% align=center nowrap><strong>Result</strong></th>
    <th align=center width=10% align=center nowrap><strong>Type</strong></th>
    <th align=center width=10% align=center nowrap><strong>User Id</strong></th>
    <th align=center width=10% align=center nowrap><strong>Optional</strong></th>
</tr>
<?
	} //end if document type

	$sql = 'SELECT * FROM (SELECT IF (site_code=\'A019000014\',\'Test\',country_iso3) country_iso3,result_id,update_date,order_code,site_code,currency,amount,type,
			telephone_no,email,result, user_id,optional,IF (book_id,"On Request","") status_type
			FROM mbus_payment_result a 
			LEFT JOIN (SELECT book_id FROM mbus_booking WHERE paid_type = 3 AND status_cms_paid = 2) b ON order_code = b.book_id 
			LEFT JOIN  mbus_payment_connect c ON a.site_code = c.sitecode ';
	
	if (!empty( $start_date ) &&  !empty( $end_date)){
		$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m-%d\') BETWEEN "'.$start_date.'" AND "'.$end_date.'"  ) AS aa ';
		
	}	
	else{
	
		$sql .= 'WHERE DATE_FORMAT(update_date,\'%Y-%m\') = DATE_FORMAT(NOW(),\'%Y-%m\') ) AS aa  ';
		
	}

	$sql .= 'WHERE ((country_iso3 IS NULL) || (country_iso3 IN (\''.$country_list.'\'))) ';
	$sql .= ' ORDER BY update_date DESC ';


	$result = $db->db_query($sql); $i= 0; 
	
	while ($record = mysql_fetch_array($result))
	{			
		$rec = $record;	 	 	 	 	 	 	 	 	 	
		?> 
		<tr>
            <td><?=$rec[result_id] ?></td>
            <td><?=$rec[update_date] ?></td>
            <td><?=urldecode($rec[status_type])?></td>
            <td><?=urldecode($rec[order_code])?></td>
            <td><?=urldecode($rec[site_code]) ?></td>
            <td><?=urldecode($rec[currency]) ?></td>   
            <td><?=urldecode($rec[amount]) ?></td>         
            <td><?=urldecode($rec[telephone_no])?></td>
            <td><?=urldecode($rec[email])?></td>
            <td><?=urldecode($rec[result])?></td>
            <td><?=urldecode($rec[type])?></td>
            <td><?=urldecode($rec[user_id]) ?></td>
            <td><?=urldecode($rec[optional]) ?></td>
        </tr>
		<?			
		$i++;
	}
}
 
	if(empty($_GET[send_excel])) //echo footer if document type html;
	{
		?>
		
		<tr>
		<td align=center colspan=13>
		<input type=button name=send_excel value='Export Excel' onClick='fncSubmit(this);' />
		<input type=button name=send_close value='Close' onClick='func_close(this);' />
		</td>								
		</tr>
		</table>
		
		<script language="javascript">
		function fncSubmit(obj)
		{
			
			val = 'making_axes_report.php?send_excel=Export&option_show=<? echo $option_show; ?>&start_date=<? echo $start_date; ?>&end_date=<? echo $end_date; ?>&option_type=<? echo $option_type; ?>&option_book=<? echo $opiton_book; ?>';
				
			document.location.href = val;
		
		}
		
		function func_close()
		{
			document.location.href = 'axes_payment.php';
		}
		</script>
		
		
		</BODY>
		</HTML>
		
		<? 
	}
	else{ //echo footer if document type excel;
		?>
		<tr>
        <? 
			if ($option_show == "log"){
		?>
		<td colspan="5" align=center height=30>&nbsp;</td></tr></TABLE>
		<? }else{ ?>
        <td colspan="13" align=center height=30>&nbsp;</td></tr></TABLE>
        
        <? 
		}
		?>
		</BODY>
		</html>
		
		<? 
	}// echo footer if document type;
} // if log report end;
?>