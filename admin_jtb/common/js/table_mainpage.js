// JavaScript Document
function f_gotopage(_url)
{
	document.location.href = _url;	
}

function mb_strlen(str) {
				var len = 0;
				for(var i = 0; i < str.length; i++) {
					len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? 2 : 1;
				}
				return len;
}

function check_text_len(_input,_output,_length,_alert)
{
	$(_input).keydown(
					
		function()
		{
			check_text_len_child(_input,_output,_length,_alert);
		}
	);
}



function check_text_len_child(_input,_output,_length,_alert)
{
	txt_len = mb_strlen($(_input).val());
						
	if (txt_len > _length)
	{
		$(_output).html(txt_len+'/'+_length+'&nbsp;'+_alert);
		$(_output).css({'color':'#F00'});					
	}
	else
	{
		$(_output).html(txt_len+'/'+_length);
		$(_output).css({'color':'#666'});	
	}
}


var frm_name ;

$(document).ready(
	function()
	{		
		$(document).find("input, button").filter(':submit').click(
		function(event) 
		{
			event.preventDefault();
		});
		
		$('.sub1').bind("mouseenter",
			function()
			{				
				$(this).find('.menu_sub').show();
			}
			).bind("mouseleave",
				function()
				{
					$(this).find('.menu_sub').hide();		
				}
			);
			
			$('.chkbox_all').click(
			function()
			{		
				if ($(this).attr('checked')==true)
				{
					$('.chkbox').attr('checked','true');
				}
				else
				{
					$('.chkbox').attr('checked','');
				}
				
		});
				
		$('#btn_edit').click(
				function()
				{
					_bufftxt = $('#ref_id').val(); 
					
					_j = 0;
					for (_i=0;_i<_bufftxt.length;_i++)
					{	
						if (_bufftxt.substr(_i,1)==',')
						{
							_numid = (_bufftxt.substr(_i-(_j),_j)) ;
							
							if ($('#id_chk'+_numid).attr('checked')==true)
							{
								f_gotopage(_nowpage+'_edit.php?id='+_numid);
								break;
							}
							
							_j = 0;
						}
						else
						{
							_j++;
						}
					}				
		});
			
		$('#btn_new').click(
			function()
			{
				f_gotopage(_nowpage+'_new.php');
		});
		
		$('#btn_close,#btn_close2').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
			$('#btn_close_cc').click(
			function()
			{
				f_gotopage(_nowpage2);
			}
		);
		
		$('#btn_save,#btn_save2').click(
			function()
			{
				
				_sel_page = fillter(_nowpage);
				_result_msg  = ''; _check   = 0; $('.msg-error').remove();
				
				switch (_sel_page)
				{
					case 'country_action' : 
						
						_result  =  req($('input[name$="inp_jtb"]').val(),'inpreq-code'); 
						msg_error($('input[name$="inp_jtb"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_iso2"]').val(),'inpreq-code'); 
						msg_error($('input[name$="inp_iso2"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_iso3"]').val(),'inpreq-code'); 
						msg_error($('input[name$="inp_iso3"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_en"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_en"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_jp"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_jp"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_child_age"]').val(),'inpreq-age'); 
						msg_error($('input[name$="inp_child_age"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_path"]').val(),'inpreq-path'); 
						msg_error($('input[name$="inp_name_path"]'),_result['msg']);
						_check  += _result['count'];
						
						
						if (_check == 0){
							form_submit();
						}
						
						break;
						
					case 'city_action' : 					
						
						_result  =  req($('select[name$="inp_country"]').val(),'selreq-country'); 
						msg_error($('select[name$="inp_country"]'),_result['msg']);
						_check  += _result['count'];
						
						
						_result  =  req($('input[name$="inp_iso3"]').val(),'inpreq-code'); 
						msg_error($('input[name$="inp_iso3"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_en"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_en"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_jp"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_jp"]'),_result['msg']);
						_check  += _result['count'];
						
					   
					   if (_check == 0){
							form_submit();
						}
						
					   break;
					   
					case 'hotel_action' : 
						_result  =  req($('select[name$="inp_country"]').val(),'selreq-country'); 
						msg_error($('select[name$="inp_country"]'),_result['msg']);
						_check  += _result['count'];
						
						
						_result  =  req($('select[name$="inp_city"]').val(),'selreq-city'); 
						msg_error($('select[name$="inp_city"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_code"]').val(),'inpreq-code'); 
						msg_error($('input[name$="inp_code"]'),_result['msg']);
						_check  += _result['count'];
						
							_result  =  req($('input[name$="inp_name_en"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_en"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_name_jp"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_name_jp"]'),_result['msg']);
						_check  += _result['count'];
						
						 if (_check == 0){
							form_submit();
						}
						
						break;
					
					case 'bannerlp_action' : 	
						
						_result  =  req($('.selreq-country').val(),'selreq-country'); 
						msg_error($('.selreq-country'),_result['msg']);
						_check  += _result['count'];

						_result  =  req($('.selreq-public-order').val(),'selreq-public-order'); 
						msg_error($('.selreq-public-order'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('.selreq-product').val(),'selreq-product'); 
						msg_error($('.selreq-product'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('.inpreq-name').val(),'inpreq-name'); 
						msg_error($('.inpreq-name'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
						
						break;
						
					case 'banneropt_action' : 	
						
						_result  =  req($('select[name$="inp_country_pro"]').val(),'selreq-country'); 
						msg_error($('select[name$="inp_country_pro"]'),_result['msg']);
						_check  += _result['count'];

						_result  =  req($('select[name$="inp_product"]').val(),'selreq-public-order'); 
						msg_error($('select[name$="inp_product"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('select[name$="inp_public"]').val(),'selreq-product'); 
						msg_error($('select[name$="inp_public"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('input[name$="inp_banner_name"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_banner_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
						
						break;
						
					case 'time_action':
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-time'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
						
						break;
						
					case 'day_action' : 
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-day'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
						
						break;
						
					case 'category_action' : 
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-cat'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
					case 'category2_action' :
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-cat2'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
					case 'option_action': 
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-opiton'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
					
						break;
					
					case 'option2_action': 
						
						_result  =  req($('input[name$="inp_name"]').val(),'inpreq-opiton'); 
						msg_error($('input[name$="inp_name"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
						case 'country_news_action': 
						
												
						_result  =  req($('input[name$="inp_subject"]').val(),'inpreq-subject'); 
						msg_error($('input[name$="inp_subject"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('textarea[name$="inp_description"]').val(),'inpreq-content'); 
						msg_error($('textarea[name$="inpreq-content"]'),_result['msg']);
						_check  += _result['count'];
						
						
						_result  =  req($('input[name$="txt_calendar"]').val(),'inpreq-day'); 
						msg_error($('input[name$="txt_calendar"]'),_result['msg']);
						_check  += _result['count'];
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
						case 'branch_content_action': 
						
										
						_result  =  req($('input[name$="inp_subject"]').val(),'inpreq-subject'); 
						msg_error($('input[name$="inp_subject"]'),_result['msg']);
						_check  += _result['count'];
						
										
						_result  =  req($('textarea[name$="inp_short_description"]').val(),'inpreq-short_content'); 
						msg_error($('textarea[name$="inp_short_description"]'),_result['msg']);
						_check  += _result['count'];
						
						_result  =  req($('textarea[name$="inp_description"]').val(),'inpreq-content'); 
						msg_error($('textarea[name$="inpreq-content"]'),_result['msg']);
						_check  += _result['count'];
						
										
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
						case 'promotion_action': 
						
										
						_result  =  req($('input[name$="inp_banner_name"]').val(),'inpreq-name'); 
						msg_error($('input[name$="inp_banner_name"]'),_result['msg']);
						_check  += _result['count'];
						
					
										
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
						case 'currency': 
						
										
						_result  =  req($('input[name$="inp_rate"]').val(),'inpreq-rate'); 
						msg_error($('input[name$="inp_rate"]'),_result['msg']);
						_check  += _result['count'];
						
					
										
						
						if (_check == 0){
							form_submit();
						}
					
						break;
						
					default : if (_check == 0){ form_submit(); }  break;
				}



			}
		);
		$('#btn_download').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
		$('#btn_restore').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = ''; _valdata = ''; 
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							
							_chkdata = _chkdata+ _numid+ ',';
						    _valdata = _valdata + $('input:radio[name=rad'+_numid+']:checked').val() + ',' ;
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
					var msg = "Are you sure you want to restores?";    
					
					if ( confirm(msg) )
					{
						$('#action_id').val(_chkdata);
						$('#val_id').val(_valdata);
						
						document.restore_obj.submit();
					}
				}
		});
				
		$('#btn_delete').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
					var msg = "Are you sure you want to delete?";    
					
					if ( confirm(msg) )
					{
						$('#action_id').val(_chkdata);
						document.del_obj.submit();
					}
				}
		});
		
		$('#btn_active').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
						$('#enabled_action_id').val(_chkdata);
						document.enabled_obj.submit();
				}
		});
		
		
		$('#btn_unactive').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
						$('#disabled_action_id').val(_chkdata);
						document.disabled_obj.submit();
				}
		});
		
				$('#btn_back').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
		

		$('#main_toggle').toggle(
			function()
			{
				$('.toggle_sub').show();
			},
			function()
			{
				$('.toggle_sub').hide();
			}
		);
		
		
		$('.tb_toggle').click(
			function()
			{
				_textid = $(this).text();
				
				if ( $('.subtb'+_textid).css('display') == 'none' )
				{
					$('.subtb'+_textid).show();
				}
				else 
				{
					$('.subtb'+_textid).hide();
				}
			}
		);
		
		
		$('input:radio').click(
			function()
			{
				_name = ( $(this).attr('name'));
				_name = _name.replace("rad", "");
				
				if ($(this).val() > 0)
				{
					$('#id_chk'+_name).attr("checked",true);
				}
				else
				{
					$('#id_chk'+_name).attr("checked",false);
				}

			}
							 
		);
				
		$('select[name$="inp_country"]').change(
			function()
			{											
				$.post('ajax_view_country.php',{ value : $(this).val()  },
				function(data) {
				 						
					$('#field_city').html(data);										
				});				
				
			}
		);
		
		
		$('select[name$="inp_country_pro"]').change(
																							
			function()
			{					
				
				$.post('ajax_view_product.php',{ value : $(this).val()  },
				function(data) {
				 	

					$('#field_product').html(data);
				});								
			}
		);		
		
		
		
		
		$('select[name$="filter_country"]').change(
			function()
			{
				
				product_select_post();
				/*$.post('ajax_fillter_country.php',{ value : $(this).val()  },
				function(data) {					
					$('select[name$="fillter_city"]').parent().html(data);										
				});	*/
			}
		);
		
		$('select[name$="filter_city"]').change(
			function()
			{
				product_select_post();
			}
		);
		
		$('select[name$="filter_theme"]').change(
			function()
			{
				product_select_post();
			}
		);
		
		$('input[name$="inpSearch"]').click(
			function()
			{
				product_select_post();
			}
		);
		
		//$('input[name$="inp_min"]').numeric();
		//$('input[name$="inp_max"]').numeric();
		
	});


function product_select_post()
{
	
	f_gotopage( _nowpage+'.php?filter_country='+$('select[name$="filter_country"]').val()+
							 '&filter_city='+   $('select[name$="filter_city"]').val() +
							 '&filter_theme='+  $('select[name$="filter_theme"]').val() +
							 '&filter_code='+   $('input[name$="filter_code"]').val() + 
							 '&filter_name_en='+ $('input[name$="filter_name_en"]').val() +
							 '&filter_name_jp='+ $('input[name$="filter_name_jp"]').val()
	);	
}

function fillter(nowpage)
{
	_buffer = '';
	
	for (i=0;i<nowpage.length;i++)
	{
		if ( nowpage.substr(i,4) == '.php')
		{
			data =  nowpage.substr(0,i) ;	 break;
		}
		
	}

	return data;
}

function msg_error(obj,text)
{
	obj.parent().append('<span class="msg-error">'+text+'</span>') ;
}

function req(obj,type)
{
	_datareq = [];	
	_error = {'selreq-country': 'Please select country.',
			  'selreq-city': 'Please select city',
			  'selreq-product' : 'Please select product.',
			  'selreq-public-order' : 'Please select public order.',
			  'inpreq-name' : 'Please input name.',
			  'inpreq-link' : 'Please input link.',
			  'inpreq-time' : 'Please input time.',
			  'inpreq-day'  : 'Please input day',
			  'inpreq-cat'  : 'Please input theme.',
			  'inpreq-cat2' : 'Please input category.',
			  'inpreq-opiton' : 'Please input option name.',
			  'inpreq-code' : 'Please input code.',
			  'inpreq-age'  : 'Please input age.',
			  'inpreq-path' : 'Please input path.',
			  'inpreq-subject' : 'Please input subject.',
			  'inpreq-content' : 'Please input content.',
			  'inpreq-short_content' : 'Please input short content.',
			  'inpreq-rate' : 'Please input currency rate.',

			  };
	
	if ((obj == '0')||( jQuery.trim(obj) == ''))
	{	
		_datareq = {'type': 'error' , 'msg' : _error[type], 'count' : 1};
		return _datareq;
		
	}
	else
	{
		_datareq = {'type':'true' , 'msg' :'', 'count' : 0};
		return _datareq;
	}	
}


function form_submit()
{
	frm_name =   $(document).find("form").attr("name") ;		
	$('#'+frm_name).submit(); 
}