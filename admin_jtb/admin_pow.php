<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

	//print_r($_REQUEST);
/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
	$data[0] = 'Administrators Management';
	$data[5] = 'admin';  /* system page name */
	$data[1] = breadcamp( array('Home','Administrators Management'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('Book no','100'),
					 tb_head('Name','250'),
					 tb_head('Status','100'),
					 tb_head('Date','100'));

	/*-----------find now page----------------*/
	$page_num = 10;	
	$page_count = $db->count_admin();	
	//echo $page_count;
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fetch_admin($page_first,$page_num);
	
	$btn_edit ='<img class="btn_edit" src="./common/images/edit.jpg" alt="Edit" border="0" onmouseover="this.src=\'./common/images/edit_on.jpg\'" onmouseout="this.src=\'./common/images/edit.jpg\'">';
	
	$btn_del ='<img class="btn_del" src="./common/images/delete.jpg" alt="Delete" border="0" onmouseover="this.src=\'./common/images/delete_on.jpg\'" onmouseout="this.src=\'./common/images/delete.jpg\'">';	
		
	//$rec_linkprefix = $data[5].'_detail.php?page='.$page_now.'&id=';
	$linkprefix_edit = $data[5].'_edit.php?id=';
	$linkprefix_delete = $data[5].'_action.php?action=delete&id=';
	
	$data[page_count]=$page_count;
	
	for ($i=0;$i< count($record[0]); $i++)
	{			
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),$record[3][$i],$record[1][$i],$record[2][$i], $record[6][$i],$btn_edit,$btn_del),
		array('','','','','',$linkprefix_edit.$record[0][$i],$linkprefix_delete.$record[0][$i]),
		array(2,0,1,0,1,1,1));		
	}
	
	/*----set namepage for action check all and delete item-----*/
	foreach ($record[0] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	
	$data[7] = $data[5].'_action.php?page='.$page_now;

	$arr_paid_status =  $db->tt_get_paid_status();	
	$data[8] = input_selectbox('c_status',$arr_paid_status[1],$arr_paid_status[0],$data[c_status],'----No select----','inp_cx1');	

	$arr_product_type = array("PKG","OPT","LPK");
	$data[9] = input_selectbox('product_type',$arr_product_type,$arr_product_type,$data[product_type],'----No select----','inp_cx1');		

	//$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	$data[4] = pagenavi( $page_now, $page_count ,'admin_pow.php?page=',$page_num);		
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_admin_pow","../content/themes_backend_jtb/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}

?>