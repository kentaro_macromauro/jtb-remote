<? 
require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");

if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Management Screen TOP</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css">
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css">
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript">
$(document).ready(
	function(){
		
		$(this).submit( function( event ) { 
			event.preventDefault();
		});
		
		$('input[name$="btn_submit"]').click(function(){
			if (( jQuery.trim($('input[name$="start_date"]').val()) != "") && ( jQuery.trim($('input[name$="end_date"]').val()) != "")){
				output_report.submit();
			}	
			else{
				alert('Please select the date of Report.');
			}
		});
	}
);
</script>
<LINK REL="SHORTCUT ICON" HREF="./ppp.icon">
</head>
<body>
<!--header include-->
<?
	$_url = $_SERVER['REQUEST_URI'];
    //echo "url=".$_url;
    
    $str = str_replace("order_info_list.php", "", "$_url", $count);
    $str2 = str_replace("customer_info_list.php", "", "$_url", $count2);    
	//echo $count.' / '.$count2;
    
    if($count == 0 && $count2 == 0){
		unset($_SESSION["session_start_date"]);
		unset($_SESSION["session_end_date"]);
		unset($_SESSION["session_c_keyword"]);
		unset($_SESSION["session_c_status"]);
		unset($_SESSION["session_order_status"]);
		unset($_SESSION["session_product_type"]);
    }
?>
<div class="body">
  <div class="main">
    <div class="main">
      <div class="header" >
        <div class="sub_header">
          <div class="logout">
            <p><a href="logout.php" class="btnlogout">Logout</a></p>
            <p>User : <? echo $_SESSION["session_name"]  ; ?> Level : <? echo $_SESSION["session_level_name"]  ; ?></p>
          </div>
        </div>
      </div>
      <!-- start breadcrumb -->
      <ul class="breadcamp">
        <li class="home"><a href="./">HOME</a> &gt; Axes Payment Souvenir Report</li>
      </ul>
      <!-- end breadcrumb -->
      <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
      <!--start content-->
      <div class="content">
        <div class="sub_head">
          <ul class="head_report">
            <li class="icon_report">Axes Payment Souvenir Report</li>
          </ul>
        </div>
        <form name="output_report" method="get" target="_blank" action="./making_axes_report_souvenir.php">
          <div id="search">
            <table width="60%">
              <tr>
                <th width="100" style="text-align:left; padding-left:20px;" >Date</th>
                <td><input id="ifrom_date" class="textFieldSize60" name="start_date" value="<? echo $data[start_date]; ?>" type="text" readonly="readonly" />
                  <a href="javascript:NewCssCal('ifrom_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0"></a></td>
                <td><input id="ito_date" class="textFieldSize60" name="end_date" value="<? echo $data[end_date]; ?>" type="text" readonly="readonly" />
                  <a href="javascript:NewCssCal('ito_date','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0"></a></td>
                <td><input type="image" src="./common/images/search.jpg" name="btn_submit" id="button" value="Make it" border="0" onmouseover="this.src='./common/images/search_on.jpg'" onmouseout="this.src='./common/images/search.jpg'" /></td>
              </tr>
            </table>
          </div>
          <table width="60%">
            <tr>
              <th width="100" style="text-align:left; padding-left:20px;" >Output</th>
              <td><input name="option_show" type="radio" value="log"  />
                ログ/log</td>
              <td><input name="option_show" type="radio" value="daily" checked="checked" />
                日別/Daily</td>
              <td><input name="option_show" type="radio" value="monthly" />
                月別/Monthly</td>
            </tr>
          </table>
          <table width="60%">
            <tr>
              <th width="100" style="text-align:left; padding-left:20px;" >Output TYPE</th>
              <td colspan="3"><input name="option_type" type="radio" value="OP" checked="checked"  />
                OPT</td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </div>
  <!--end content-->
</div>
</div>
<div style="height:600px;"></div>
<div class="clear"></div>
<div class="footer">(C) My-bus 2012.</div>
</div>
</body>
</html>
<?		
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>