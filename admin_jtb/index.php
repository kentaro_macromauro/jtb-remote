<? 
require_once("../www_config/setting.php");
require_once("../class_backend/include/c_query.php");
require_once("../class_backend/c_query_sub.php");
require_once("../class_backend/c_session.php");
require_once("../class_backend/c_cookie.php");
require_once("../class_backend/c_themes.php");

$status = false;

//print_r($_REQUEST);
//print_r($_SESSION);

if (check_session())
{
	if ($_SESSION["session_code"] == _ADMIN_TYPE1_)
	{ 
		$status = true;
	}
	else
	{
		$status = false;	
	}
	/*check premission if premission is pass "status = true" */
}

if ($status == true)
{
	$themes = new c_themes("backend_index","../content/themes_backend_jtb/");
	$themes->pbody();
}
else
{
	header("Location: logout.php");
}
?>

