<? 

ob_start(); 
@ini_set('display_errors'     , 'off');
@ini_set('default_charset'    , 'utf-8');

require_once("include/header.php");
require_once($path."class_backend/c_action.php");
require_once($path."class_backend/c_query_sub.php");


/* mail setting : start*/ 
function address_mail_from_country_code($country_iso3){
	switch( $country_iso3){
			case 'TWN' :  $address_mail = 'option_yoyaku.tw@jtbap.com'; break;	
			case 'IDN' :  $address_mail = 'reservation_jtbbali.id@jtbap.com'; break;	
			case 'THA' : if ($city_iso3 == 'HKT'){ 
							$address_mail = 'toiawase_phuket.th@jtbap.com';  
						 }
						 else{
							 $address_mail = 'reservation_mybus.th@jtbap.com';      	 
						 } break;	
			case 'VNM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
			case 'CAM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
			case 'KHM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
			
			case 'MYS' :  $address_mail = 'mybus.my@jtbap.com'; break;
			case 'SGP' :  $address_mail = 'opjtb.sg@jtbap.com'; break;	
			case 'HKG' :  $address_mail = 'inbound@hk.jtb.cn';  break;
			case 'AUS' :  $address_mail = 'isdweb.au@jtbap.com'; break;	
			case 'NZL' :  $address_mail = 'webnz.nz@jtbap.com'; break;			
	}
	return $address_mail;
}

/* mail setting : end */

/* precen setting : start */
function percen_case($country_iso3)
{
	switch( $country_iso3){
			case 'THA' :  $data = 'ツアー参加前日・・・50％
ツアー当日・・・・・・100％'; break;	

			case 'AUS' :  $data = 'ツアー参加前々日・・・・・・・・・・無料
ツアー参加前日の15時まで・・・50％
ツアー参加前日の15時以降・・100％'; break;	
								   
			case 'NZL' :  $data = 'ツアー参加前々日・・・・・・・・・・50％
ツアー参加前日17時以降・・・100％
ツアー当日・・・・・・・・・・・・・・・100％'; break;	

			case 'VNM' : $data = 'ツアー参加前日10:00～16:59・・・30％
ツアー参加前日17:00以降・・・・・100％'; break;
								   
			default : 	  $data = 'ツアー参加前々日・・・30％
ツアー参加前日・・・・・50％
ツアー当日・・・・・・・・100％'; break;
	}
	return $data;
}
/* precen setting : end */

if ($status == true)
{	

	//------- data connect : start
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	//------- data connect : end
	
	if (!empty( $_REQUEST[b_id] )) 
	{
	
		$sql = 'SELECT country_iso3 FROM mbus_booking WHERE book_id = "'.$_REQUEST[b_id].'" ';
			
		$res = $db->db_query($sql);
			
		while ($record = mysql_fetch_array($res)){
			$country_iso3 = $record[country_iso3];	
		}
	}
	
	
	$key_id =(!empty($_GET['b_id'])) ? $_GET['b_id'] : $_POST[bs_id];	
	
	list($data[level_name],$data[level_pow]) = $db->get_data("level_name,level_pow","mbus_admin_level","level='".$_SESSION[session_level]."'");	
	
	//------- Send email : start 
	if(!empty($_POST[bs_id])){
	
		require_once($path."class_backend/phpmailer/class.phpmailer.php");			
		
		$mail = new PHPMailer();
		$mail->IsMail();
		$mail->CharSet  = 'UTF-8'; 										
		
		//---------file attachment : start 
		$attachment1 = $_FILES['upfile_1']['tmp_name']; 
		$attachment_name1 = $_FILES['upfile_1']['name'];	
		
		$attachment2 = $_FILES['upfile_2']['tmp_name']; 
		$attachment_name2 = $_FILES['upfile_2']['name'];
		
		$attachment3 = $_FILES['upfile_3']['tmp_name']; 
		$attachment_name3 = $_FILES['upfile_3']['name'];				
		
		if(!empty($attachment1)) $mail->AddAttachment($attachment1,$attachment_name1);
		if(!empty($attachment2)) $mail->AddAttachment($attachment2,$attachment_name2);
		if(!empty($attachment3)) $mail->AddAttachment($attachment3,$attachment_name3);		
		//---------file attachment : end 

		$sql = 'SELECT country_iso3 FROM mbus_booking WHERE book_id = "'.$_REQUEST[bs_id].'" ';
		
		$res = $db->db_query($sql);
		
		while ($record = mysql_fetch_array($res)){
			$country_iso3 = $record[country_iso3];	
		}
		
		$address_mail = address_mail_from_country_code($country_iso3);

		$mail->SetFrom($address_mail);
		$mail->AddAddress($_POST[mail_address]);
		$mail->AddCC($_POST[mail_cc]);
		$mail->AddBCC($_POST[mail_bcc]);
		
		$mail_body ='<html><body>'.nl2br($_POST[mail_body]).'</body></html>';				

		$mail->Subject = $_POST[mail_title];
		$mail->MsgHTML($mail_body);
		
		
		
		if($mail->Send()) {
		
			if(!empty($_POST[bs_mail_status])){
	
				$arr_column =array('status_cms','update_date_cms','update_by_cms');
				$arr_value =array($_POST[bs_mail_status],"datetime", $user_id);
				
				$db->set_update(_DB_PREFIX_TABLE.'booking',
							$arr_column,
							$arr_value,
							'book_id',$_POST[bs_id]);					
			}
			
			$data[status_sendmail] ='Send email complete';	
		}
		else {
			
			$data[status_sendmail] ='Send email fail';			
		}
		
	}					
	//------- Send email : end 			
	
	$ff =$db->tt_view_booking2($key_id);
	
	$data[jtb_bookind_id] =$ff[jtb_bookind_id];			
	
	$data[mail_format] ='';
	$data[mail_title] ='';
	$data[inp_mail] ='';
	$data[book_id] =$ff[book_id];
	
	$sqlg = "SELECT *,DATE_FORMAT(guest_birthday,'%Y年%c月%e日') guestbirthday,DATE_FORMAT(guest_noexpire,'%Y年%c月%e日') guestnoexpire  
			 FROM mbus_booking_guest WHERE book_id='".$key_id."'";			
			 
			 
	$ig=0;
		
	$rg = $db->db_query($sqlg);
	
	$sex_g =array("1"=>"Mr","2"=>"Ms");
	$config_sex_jp = array("1"=>"男性","2"=>"女性");	
	$guest_all ='';
	
	$guest_name = '';
	
	while ($fg = mysql_fetch_array($rg)) {						
			$guest_all .=$sex_g[$fg[guest_sex]].' '.$fg[guest_firstname].' '.$fg[guest_lastname].chr(13).'		';										
			
			if ($ig == 0)
			{
				$guest_name  .= $sex_g[$fg[guest_sex]].' '.$fg[guest_firstname].' '.$fg[guest_lastname].chr(13);
			}
			else
			{
				$guest_name  .= '           '.$sex_g[$fg[guest_sex]].' '.$fg[guest_firstname].' '.$fg[guest_lastname].chr(13);	
			}
			
			$guest_detail .='■参加者情報'.chr(13)
							.'参加者名（ローマ字）: '.$sex_g[$fg[guest_sex]].' '.$fg[guest_firstname].' '.$fg[guest_lastname].chr(13)
							.'参加者性別 : '.$config_sex_jp[$fg[guest_sex]].chr(13)
							.'参加者年齢 : '.$fg[guest_age].'歳'.chr(13)
							.'参加者生年月日 : '.$fg[guestbirthday].chr(13)
							.'参加者パスポート番号 : '.$fg[guest_no].chr(13)
							.'参加者パスポートExpirity date : '.$fg[guestnoexpire].chr(13).chr(13);
			$ig++;
	}
			
	//================================================================	
	/*$weeklist = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'); 
	$sex_r =array("1"=>"Mr","2"=>"Ms");	
	
	$date_book = date('w',strtotime($ff['reg_arrgo']));		
	$date_book_week = $weeklist[$date_book];
	$date_booking =str_replace('-',' ',date('d-M-Y',strtotime($ff['reg_arrgo'])));*/
	$data_booking = $ff[bookdate];
	
	$data[reg_email] =$ff[reg_email];
	$data[b_id] =$ff[book_id];
	$dd=explode("-",$ff[reg_arrgo]); 
	
	$data[send] =$_POST[bs_id];

	$c_sign =$db->get_data("sign","mbus_currency","country_iso3='".$ff[country_iso3]."'");		
	
	
	//=======================================================================
	$sql2 ="SELECT * FROM "._DB_PREFIX_TABLE."mail_order ORDER BY idx_order";	
	
	
	$record2 = $db->db_query($sql2);	
		
	$j=0;			
	while($fm=mysql_fetch_array($record2)){
		
		$j++;		
		
		if($fm[mail_id]==$_GET[mail_id]){
			$bgcolor ='#544741';	
			
			//$link = '<option value="001" selected="selected">001</option>'
			$link ='<li class="first">'.$fm[mail_name].'</li>';
			
			$data[mail_title] 	=$fm[mail_title];
			$data[inp_mail] 	.=$fm[mail_body];
			
			$data[inp_mail_id] 		=$fm[mail_id];
			$data[inp_mail_status] 	=$fm[mail_status];			
			
			
			if($ff[reg_addrtype]==1){
			
				$city_name = $db->view_pref($ff[reg_addrjp_pref]);
			
				$cus_addr ='郵便番号 : '.$ff[reg_addrjp_zip1].' - '.$ff[reg_addrjp_zip2].chr(13)
							.'都道府県 : '.$city_name.chr(13)
							.'市区町村 : '.$ff[reg_addrjp_city].chr(13)
							.'町域 : '.$ff[reg_addrjp_area].chr(13)
							.'建物名部屋番号 : '.$ff[reg_addrjp_building].chr(13)
							;
				
			}else{
				
				$cus_addr ='Address : '.$ff[reg_addrovs1].chr(13)
							.'Country  : '.$ff[reg_addrovs2].chr(13)
							;
			}			
			
			$tel =(!empty($ff[reg_tel1])) ? $ff[reg_tel1].'-'.$ff[reg_tel2].'-'.$ff[reg_tel3] : '';
			$mobile =(!empty($ff[reg_mobile1])) ? $ff[reg_mobile1].'-'.$ff[reg_mobile2].'-'.$ff[reg_mobile3] : '';
			$em_tel =(!empty($ff[reg_backup_tel1])) ? $ff[reg_backup_tel1].'-'.$ff[reg_backup_tel2].'-'.$ff[reg_backup_tel3] : '';			
			
			$date_frm = date('w',strtotime($ff['reg_arrfrm']));		
			$date_frm_week = $weeklist[$date_frm];
			$date_frm1 =str_replace('-',' ',date('d-M-Y',strtotime($ff['reg_arrfrm'])));
		
			$date_to = date('w',strtotime($ff['reg_arrto']));		
			$date_to_week = $weeklist[$date_to];
			$date_to1 =str_replace('-',' ',date('d-M-Y',strtotime($ff['reg_arrto'])));			
			
			
			
			$data_name = $ff[reg_furi_firstname].' '.$ff[reg_furi_lastname];
			$data_tour_name = $ff[product_name_jp];
			
			$ff_pd =$db->get_data("price_adult,price_child","mbus_booking_detail","book_id='".$_REQUEST[b_id]."'");
			
			$tour_price = '大人 '.$c_sign[0].' '.number_format($ff_pd[0],2).' / 子供 '.$c_sign[0].' '.number_format($ff_pd[1],2);
			$summary_price = $c_sign[0].' '.number_format($ff[amount],2);
			
			$participants = $guest_detail;
				//Allotment / No Allotment / Cancel Order ==============================================================					
					
				$ff_pd =$db->get_data("price_adult,price_child","mbus_booking_detail","book_id='".$_REQUEST[b_id]."'");
								
				$booking_info_all ='■申込情報'.chr(13)
								.'申込者名（漢字） : '.$ff[reg_firstname].' '.$ff[reg_lastname].chr(13)
								.'申込者名（ローマ字） : '.$sex_r[$ff[reg_sex]].' '.$ff[reg_furi_firstname].' '.$ff[reg_furi_lastname]		.chr(13)
								.'生年月日 : '.$ff[regbirthday].chr(13)
								.'性別 : '.$config_sex_jp[$ff[reg_sex]].chr(13)
								.'メールアドレス（PC）: '.$ff[reg_email].chr(13)
								
								.'住所'.chr(13)
								.$cus_addr		
								
								.'連絡先（電話）: '.$tel.chr(13)
								.'連絡先（携帯）: '.$mobile.chr(13)
								.'住所（緊急連絡先）: '.$ff[reg_backup_addrovs1].chr(13)
								.'電話番号（緊急連絡先）: '.$em_tel.chr(13).chr(13)
								
								.'■参加情報'.chr(13)
								.'宿泊ホテル'.chr(13)
								.'滞在先名 : '.$ff[reg_hotel_name].chr(13)
								.'滞在先TEL : '.$ff[reg_hotel_tel].chr(13)
								.'滞在先住所 : '.$ff[reg_hotel_addr1].chr(13)
								.'宿泊期間 : '.$ff[regarrfrm].'～'.$ff[regarrto].chr(13) 

								.'到着日フライト : '.$ff[regarrgo].' '.$ff[reg_arrgo_time].chr(13)
								
								.'フライトナンバー : 1100'.$ff[reg_airno].chr(13)
								.'ご旅行のご形態 : '.$ff[reg_info].chr(13).chr(13)															

								.$guest_detail									
								
								.'■その他の情報'.chr(13)
								.$ff['remark'].chr(13).chr(13) ;																		
		
		}
		else{
			$link ='<li><a href="./order_mail.php?b_id='.$ff[book_id].'&amp;mail_id='.$fm[mail_id].'">'.$fm[mail_name].'</a></li>';
		}
		
		#E-mail Format
		$data[mail_format] .=''.$link.'';	
	}		
	
	
		$data[mail_format] = '<ul class="tb_order_mail">'.$data[mail_format].'</ul>';
		
		
	//---------signature : start					
		$signature ='';
		
		switch($ff[country_iso3])
		{
			case 'TWN' : $signature .= '============================================'.chr(13)
		.'JTB台湾（世帝喜旅行社股份有限公司）'.chr(13)
		.'マイバスデスク'.chr(13)
		.'台湾台北市中山北路2段56號JTBラウンジ'.chr(13)
		.'09：00～18：00（予約受付は17：30まで）年中無休'.chr(13)
		.'Tel: +(886)-02-2521-7315 '.chr(13)
		.'Fax:+(886)-02-8192-7111'.chr(13)
		.'http://www.jtbtaiwan.com'.chr(13)
		.'台湾のマイバスデスクはアンバサダーホテル（國賓飯店）向いのJTBラウンジ内です。'.chr(13)
		.'============================================='.chr(13)
		.'●”マイバス”のオプショナルツアー（27コース）やお勧めレストラン情報（14箇所）、'.chr(13)
		.'割引特典（12店舗）の掲載されたパンフレットは下記で入手できます。'.chr(13)
		.'・台湾内各主要都市の主なホテル'.chr(13)
		.'● マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
		'; break;	
		
			case 'IDN' : $signature .= '============================================
PT. JTB INDONESIA （JTBバリ支店）

Jl. Bypass Ngurah Rai No.88
Kelan Abian,Tuban, Kuta, 
Bali 80362 - Indonesia
マイバスほっとダイヤル：0361-708555（バリ島内 / 09：00～19：00）
緊急連絡先：081-2382-9139, 081-23802511（上記時間外）
WEBサイト：http://www.mybus-asia.com/indonesia/
=============================================
●バリ島でのお客様用カウンターデスク
・DFSギャラリア・バリ2階 （営業時間 10：00～18：00）
・グランド・ハイアット・バリ内 （営業時間 08：00～20：00）
●マイバスはアジアと豪州9ヵ国 （香港（マカオ）、台湾、タイ（プーケット）、マレーシア、シンガポール、バリ、ベトナム、オーストラリア、ニュージーランド）で年中無休で運行中。
';		 break;
			
			case 'THA' : $signature .= '============================================='.chr(13)
		.'JTB (Thailand) Ltd （ジェイティービー　バンコク支店）'.chr(13)
		.'Mybus Operation'.chr(13)
		.'Silom Complex Branch 5th FL.,Room 506-507'.chr(13)
		.'191 Silom Rd.,Silom Khet Bangrak, Bangkok 10500 Thailand'.chr(13)
		.'Tel: タイ国内からおかけの場合 (0)22300490'.chr(13)
		.'タイ国外からおかけの場合 +6622300490'.chr(13)
		.'Fax:(0)2344-4678'.chr(13)
		.'http://www.mybus-asia.com/thailand/'.chr(13)
		
		.'お問い合わせ、お申し込みは日本語で！'.chr(13)
		.'Tel: +(66)-2632-9404'.chr(13)
		.'============================================='.chr(13)
		
		.'============================================='.chr(13)
		.'JTB (THAILAND) LIMITED, Phuket Branch, Mybus Desk'.chr(13)
		.'JTBプーケット営業所 マイバスデスク'.chr(13)
		
		.'Tel. タイ国内からおかけの場合 (0)76261740'.chr(13)
		.'タイ国外からおかけの場合 +6676261740'.chr(13)
		.' Fax. +66 (076) 261-748'.chr(13)
		.'お電話での予約受付時間 9：00～18：00（年中無休）'.chr(13)
		
		.'e-mail: toiawase_phuket.th@jtbap.com'.chr(13)
		.'http://www.jtbphuket.com/'.chr(13)
		.'http://www.mybus-asia.com/thailand/'.chr(13)
		.'============================================='.chr(13)
		.'●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
		'; break;	
			
			case 'VNM' : $signature .= '	
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。

'; 

break;	
			
			case 'CAM' : $signature  .= '
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。
'; 

break;	
			
			case 'KHM' : $signature .= '	
■ＭY BUSツアー連絡先
JTBベトナム　ホーチミン支店 
9 DONG KHOI ST. DISTRICT 1,HO CHI MINH CITY,VIETNAM  

電話番号:+84-90-8913-923 
営業時間:9:00～19:00（電話対応のみ、年中無休） 
営業時間外緊急連絡先:+84-90-2330-175

JTBベトナム　ハノイ支店
M FLOOR, SOFITEL PLAZA HANOI,1 THANH NIEN ROAD, BA DINH DISTRICT,HANOI,VIETNAM

電話番号:+84-90-4627-889
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-6235-990

JTBベトナム　ダナン支店
150 HO XUAN HUONG ST., NGU HANH SON DIST., DA NANG, VIETNAM

電話番号:+84-90-2476-722
営業時間:9:00～19:00（電話対応のみ、年中無休）
営業時間外緊急連絡先:+84-90-2476-722

エフサンツーリスト (カンボジア発着のツアー対応）
No.0011,Mondol 3,Khum Slakram,Siem Reap Province,Kingdom of Cambodia

電話番号:+855-63-765-034
営業時間:08:00-12:00/14:00-17:00（月～金）・08:00-12:00（土）/ 日曜、祝日休み
営業時間外緊急連絡先:
後藤　一/MR.GOTO Hajime　　携帯電話　+855-12-353-615
永澤　彩/MS.NAGASAWA Aya　　携帯電話　+855-99-482-752
中田　修平/MR.NAKADA　Shuhei　　携帯電話　+855-95-701-710
寶田　ありさ /MS.TAKARADA Arisa　　携帯電話　+855-78-246-122
井上　/MS.INOUE　　携帯電話　+855-17-789-517

※営業時間外のご連絡は参加日当日のキャンセルのご連絡、または病気・事故等の緊急時に限らせていただきます。
'; 

break;
			
			
			case 'MYS' : $signature .= '===================================='.chr(13)
		.'JTB (M) SDN BHD （ジェイティービー　マレーシア支店）'.chr(13)
		
		.'ＫＬオフィス　(メリアホテル横、AMODAビル16階）'.chr(13)
		.'16.05-16.07 Level 16, Amoda Bldg.'.chr(13)
		.'22 Jalan Imbi 55100 Kuala Lumpur'.chr(13)
		.'Malaysia '.chr(13)
		.'Tel: +(603) 2142-8727'.chr(13)
		.'Fax:+(603) 2142-5344'.chr(13)
		
		.'ランカウイ：＋（604）966-4708'.chr(13)
		.'ペナン　　：＋（604）263-5788'.chr(13)
		.'コタキナバル：＋（6088）259-668'.chr(13)
		.'http://www.mybus-asia.com/malaysia/'.chr(13)
		.'======================================'; break;
			
			case 'SGP' : $signature .= '==============================='.chr(13)
		.'JTB Pte Ltd Singapore'.chr(13)
		.'Mybus Desk/マイバスデスク'.chr(13)
		.'（DFS免税品店内1階）'.chr(13)
		.'Tel: (65)-6735－2847 '.chr(13)
		.'受付時間：10：00－17：30（年中無休）'.chr(13)
		.'http://www.mybus-asia.com/singapore/'.chr(13)
		.'e-mail:opjtb.sg@jtbap.com'.chr(13)
		.'================================'; 
			break;	
		
			case 'HKG' : $signature .=  '============================================ '.chr(13)
		.'MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光） '.chr(13)
		.'Add     : Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., '.chr(13)
		.'	  　　Tsim Sha Tsui, Kowloon, Hong Kong. '.chr(13)
		.'Tel      : (852)2375-7576 (*日本語対応　年中無休09:00-18:00)'.chr(13)
		.'Fax     : (852)2311-5803 '.chr(13)
		.'Mail    : inbound@hk.jtb.cn '.chr(13)
		.'Web    : http://www.mybus-asia.com/hongkong/'.chr(13)
		.'============================================= '.chr(13)
		.'●”マイバス香港”は、 '.chr(13)
		.'ビギナーからリピーター向けオプショナルツアー（44コース）、 '.chr(13)
		.'便利で安心のスパ・マッサージ（8店舗）、 '.chr(13)
		.'あの憧れのレストランをはじめ厳選のミールクーポン（43コース）、 '.chr(13)
		.'割引特典レストラン（3店舗）をご用意しています。 '.chr(13)
		.'●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、 '.chr(13)
		.'バリ、プーケット、ベトナム）で年中無休で運行中。 '; break;
			
		
		
			case 'AUS' : $signature .=  '==========================================='.chr(13)
		.'JTB Australia Pty Ltd'.chr(13)
		
		.'ケアンズ マイバスデスク'.chr(13)
		.'電話　+61-(07)-4052-5872 '.chr(13)
		.'毎日 9：00～19：00'.chr(13)
		.'61　Abbott　Street, Cairns'.chr(13)
		
		.'ゴールドコーストマイバスデスク'.chr(13)
		.'電話　+61-(07)-5592-9491 '.chr(13)
		.'毎日 8：30～18：30 '.chr(13)
		.'Ground Floor, 3191 Surfers Paradise Boulevard'.chr(13)
		
		.'シドニー'.chr(13)
		.'電話　+61-(02)-9510-0313 '.chr(13)
		.'毎日 09:00 ～ 17:00'.chr(13)
		
		.'メルボルン'.chr(13)
		.'電話　+61(03)-8623-0091 '.chr(13)
		.'土日祝日を除く毎日 10:00 ～12:00/14:00 ～16:00 '.chr(13)
		.'Level 6, 10 Artemis Lane, Melbourne'.chr(13)
		.'(QVショッピングコンプレックス内) '.chr(13)
		
		.'パース'.chr(13)
		.'電話　+61(08)-9218-9866 '.chr(13)
		.'毎日 09:00 ～ 12:00/13:00 ～ 17:30'.chr(13)
		.'Ground Floor, 273 Hay Street, East Perth'.chr(13)
		.'isdweb.au@jtbap.com'.chr(13)
		.'============================================'; break;	
			
			case 'NZL' : $signature .=  '==================================================================='.chr(13)
		.'JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）'.chr(13)
		.'Auckland Mybus Centre'.chr(13)
		.'Level 5, 191 Queen St, Auckland'.chr(13)
		.'Tel: +(64)-9379-6415 '.chr(13)
		.'Fax:+(64)-9309-7699'.chr(13)
		.'http://www.jtb.co.nz/'.chr(13)
		.'==================================================================='.chr(13)
		.'●マイバスセンター　オークランド支店は平日　9：00〜17：30の営業です。'.chr(13)
		.'（土日祝日はお休みとなります。）'; break;	
		
		}					
		
		//---------signature : end
		
		
		$other = str_replace('&lt;br /&gt;',chr(13),$ff['remark']).chr(13);
		$other = str_replace('<br />',chr(13),$other).chr(13);
		
	

		$data[inp_mail] = str_replace('{percen-case}',percen_case($country_iso3),$data[inp_mail]);
		$data[inp_mail] = str_replace('{booking-info-all}',$booking_info_all,$data[inp_mail]);
		$data[inp_mail] = str_replace('{name}',$sex_r[$ff[reg_sex]].' '.$data_name,$data[inp_mail]);
		$data[inp_mail] = str_replace('{tour-name}',$data_tour_name,$data[inp_mail]);
		$data[inp_mail] = str_replace('{signature}',$signature,$data[inp_mail]);
		$data[inp_mail] = str_replace('{booking-date}',$ff[bookdate],$data[inp_mail]);
		$data[inp_mail] = str_replace('{tour-price}',$tour_price,$data[inp_mail]);
		$data[inp_mail] = str_replace('{summary-price}',$summary_price,$data[inp_mail]);
		$data[inp_mail] = str_replace('{participants-name}',$guest_name,$data[inp_mail]);
		$data[inp_mail] = str_replace('{$other}',$other,$data[inp_mail]);
	
		//========================================================				
		$data['attach_file_1'] ='';
			
		//========================================================
		
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_order_mail","../content/themes_backend_jtb/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
		
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>