<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH;
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');



$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();


require_once($path."include/config.php");

/* produuct path setting   */
$img  = 'product/images/product/';
$link = 'product.php?product_id=';
/* produuct path setting   */

$breadcamp = '<p><a href="./" style="color: #000; font-size: 12px;">TOP</a>';
if (!empty($_GET[id]))
{
	$id= $_GET[id];
	$filepath = ($path."www_config/html/".$id);
	$c = file_get_contents($filepath);
	if (empty($c))
	{
		header('Location: index.php');		
	}
	$title = mb_ereg_replace('^.*<h2>(.*)h2.*$','\1',$c);
    $title = str_replace('</','',$title);
    $company_name = mb_ereg_replace('^.*<h1.*?>(.*)h1.*$','\1',$c);
    $company_name = str_replace('</','',$company_name);

	$c = mb_ereg_replace('^.*<!--country info-->(.*)<!--end country info-->.*$','\1',$c);

    if($title == '会社概要'){
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<a href="page.php?id=branch.html" style="color: #000; font-size: 12px;">JTB　アジア・パシフィック　会社概要</a>';
        $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;'.$company_name;
        $breadcamp .= '</p>';
    }else{
        if (!empty($title)){
            if (strlen($title) < 1024){
                $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;<font style="color: #000; font-size: 12px;">'.$title.'</font>';
            }
            else{
                if (trim($id) == 'sitemap.html'){
                    $breadcamp .= '&nbsp;&nbsp;>&nbsp;&nbsp;サイトマップ';
                }
                $breadcamp .= '</p>';
            }
        }
        else{
            $breadcamp .= '</p>';
        }
    }
}
else
{
	if (empty($c))
	{
		header('Location: index.php');		
	}
	$c = 'not found information.';	
}

$smarty = new Smarty;

$site_country = 'ALL';

$smarty->template_dir = 'webapp/templates';
$smarty->compile_dir = 'webapp/templates_c';

$smarty->assign("config",$config);

$smarty->assign("data_tempage",$c);
$smarty->assign("breadcamp",$breadcamp);
$smarty->assign("config",$config);

$smarty->display('jtb_page.tpl');
?>