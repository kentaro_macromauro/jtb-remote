<?

/* For Menu Style */
$menu_selected_style = null;

if(strpos($_SERVER['REQUEST_URI'],'souvenir') == true || strpos($_SERVER['REQUEST_URI'],'user_guide') == true){
    $menu_selected_style['souvenir']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da;"';
}else if(strpos($_SERVER['REQUEST_URI'],'online_catalog') == true){
    $menu_selected_style['online_catalog']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da;"';
}else if(strpos($_SERVER['REQUEST_URI'],'opt.php') == true || strpos($_SERVER['REQUEST_URI'],'booking') == true  || strpos($_SERVER['REQUEST_URI'],'product.php') == true){
    $menu_selected_style['opt']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da;"';
}else if(strpos($_SERVER['REQUEST_URI'],'hotel.php') == true){
    $menu_selected_style['hotel']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da;"';
}else if(strpos($_SERVER['REQUEST_URI'],'special_tour.php') == true || strpos($_SERVER['REQUEST_URI'],'special_content_detail.php') == true || strpos($_SERVER['REQUEST_URI'],'info_top.php') == true || strpos($_SERVER['REQUEST_URI'],'news') == true || strpos($_SERVER['REQUEST_URI'],'branch_content_detail') == true){
    $menu_selected_style['local']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da; border-right: none;"';
}else{
    $menu_selected_style['top']    = 'style="border-left: 1px solid #c9c9ca; background: #d8d8da; border-right: none;"';
}

?>