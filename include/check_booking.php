<? 

$qty_adult  = $_SESSION['checkout'][qty_adult];
$qty_child  = $_SESSION['checkout'][qty_child];
$qty_infant = $_SESSION['checkout'][qty_infant];



$qty_guest = ($qty_adult+$qty_child+$qty_infant);

$guest_hidden_html = '';


if ($qty_guest> 0)
{
	
	$guest_data = array();
	for ($i =0;$i <  $qty_guest ; $i++)
	{
		
		$guest_data[$i] = array( 'firstname' => $_POST[inp_guest_firstname][$i],
						 		  'lastname'  => $_POST[inp_guest_lastname][$i],
								  'sex'		  => $_POST[inp_guest_sex][$i],
								  'age'		  => $_POST[inp_guest_age][$i],
								  'birth_year' => $_POST[inp_guest_birth_year][$i],
								  'birth_month' => $_POST[inp_guest_birth_month][$i],
								  'birth_day'  => $_POST[inp_guest_birth_day][$i],
								  'sn'		   => $_POST[inp_guest_sn][$i],
								  'expire_year' => $_POST[inp_guest_expire_year][$i],
								  'expire_month' => $_POST[inp_guest_expire_month][$i],
								  'expire_day' => $_POST[inp_guest_expire_day][$i],
								);

	}
}

$addrjp_pref_name = "";
if (!empty($_POST[reg_addrjp_pref]))
{
	$addrjp_pref_name = $db->view_pref(trim($_POST[reg_addrjp_pref]));
}



$chk_err = 0;

if (  empty( $_SESSION['register']['reg']['firstname'] )  ||  empty( $_SESSION['register']['reg']['lastname'] )  )
{
	$chk_err++;
}

if (  empty( $_SESSION['register']['reg']['furi_firstname'] )  ||  empty( $_SESSION['register']['reg']['furi_lastname'] )  )
{
	$chk_err++;
}


if (  empty( $_SESSION['register']['reg']['birth_year'] ) ||  
	  empty( $_SESSION['register']['reg']['birth_month'] )  || 
	  empty( $_SESSION['register']['reg']['birth_day'] ) )
{
	$chk_err++;
}

if ( empty( $_SESSION['register']['reg']['sex'] ) )
{
	$chk_err++;
}

if ( empty( $_SESSION['register']['reg']['email'] ) )				
{
	$chk_err++;	
}		


if ( empty( $_SESSION['register']['reg']['type_addr'] ) ||  
     ( $_SESSION['register']['reg']['type_addr'] == "false") || 
     ( $_SESSION['register']['reg']['type_addr'] == '0' ) )
{

	
	if ( empty( $_SESSION['register']['reg']['addrjp_zip1'] )  || empty( $_SESSION['register']['reg']['addrjp_zip2'] ) )
	{
		$chk_err++;	
	}
	
	
	if ( empty ( $_SESSION['register']['reg']['addrjp_pref'] ) )
	{
		$chk_err++;	
	}
	
	if (empty(  $_SESSION['register']['reg']['addrjp_area']))
	{
		$chk_err++;	
	}
	
}
else
{
	if ( empty( $_SESSION['register']['reg']['addrovs1']   ) || empty( $_SESSION['register']['reg']['addrovs2']      )  )	
	{
		$chk_err++;		
	}
}
				
if ( empty( $_SESSION['register']['reg']['mobile1'] )  || 
	 empty( $_SESSION['register']['reg']['mobile2'] )  || 
	 empty( $_SESSION['register']['reg']['mobile3'] )  )
{
	$chk_err++;	
}


if ( empty( $_SESSION['register']['reg']['backup_addr1']) )
{
	$chk_err++;
}


if ( empty( $_SESSION['register']['reg']['backup_tel1'] )  || 
	 empty( $_SESSION['register']['reg']['backup_tel2'] )  || 
	 empty( $_SESSION['register']['reg']['backup_tel3'] )  )
{
	$chk_err++;	
}



	
if (empty ($_SESSION['register']['reg']['hotel_name'] )  )
{
	$chk_err++;	
}
	

if (empty ($_SESSION['register']['reg']['sel_info']))
{
	$chk_err++;	
}



$qty_adult  = $_SESSION['checkout'][qty_adult];
$qty_child  = $_SESSION['checkout'][qty_child];
$qty_infant = $_SESSION['checkout'][qty_infant];


$qty_guest = ($qty_adult+$qty_child+$qty_infant);


for ($i = 0; $i < count( $qty_guest) ; $i++)
{
	
	
	if ( empty($_SESSION['register']['guest'][$i]['firstname']) || 
		 empty($_SESSION['register']['guest'][$i]['lastname'])
		) 
	{
		$chk_err++;	
	}
	
	if ( empty($_SESSION['register']['guest'][$i]['sex'])) 
	{
		$chk_err++;	
	}
	
}


if ($chk_err > 0)
{
	header("Location: index.php?");		
}

?>