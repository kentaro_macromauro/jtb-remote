<?
/* pkg and opt at path right menu*/

function print_right_price_min_max($min,$max)
{
	return '<span class="txt-red-bold"> '.$min.'</span><span class="yan"> 円 ～</span>
	<span class="txt-red-bold"> '.$max.'</span><span class="yan"> 円</span>';
}


function print_right_price_start_to($sgd,$yen)
{
	if ($yen[show] == 1)
	{
		if ($sgd == 0)
		{
			return '';
		}
		else
		{
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'～( 目安：\\'.number_format( round($yen[rate]*$sgd) ).'～)</span>';
		}
	}
	else
	{
		if ($sgd == 0)
		{
			return '';
		}
		else
		{
		
			return '<span class="txt-red-bold-price">'.$yen[sign].number_format($sgd).'～';
		}
	}
}

/* right menu opt */
$result = $db->front_show_product('OPT',$site_country,$prv_year,$prv_month,"%",0,3);


$opt_count = count( $result['product_id']);

for ($i=0; $i<count( $result['product_id']); $i++)
{
	
	$opt_top5_id[$i]	   = $result['product_id'][$i];
	$opt_top5_name[$i]	   = str_len($result['product_name_jp'][$i],32) ;
	$opt_top5_country[$i]  = $result['country_name_jp'][$i];
	$opt_top5_path[$i]     = $result['path'][$i];
	
	$opt_top5_pic[$i]      = $path.'product/images/index.php?root=product&amp;width=112&amp;name='.trim($result['product_id'][$i].'-1.jpg'); 
	$opt_top5_check[$i]    = $img.$result['product_id'][$i].'-1.jpg'; 
	
	$opt_top5_detail[$i]   = $result['short_desc'][$i];
	$opt_top5_price[$i]    = print_right_price_start_to($result['price_min'][$i], array( 'rate' => $result[rate][$i] , 'sign' => $result[sign][$i] , 'show' =>  $db->show_rate($result['country_iso3'][$i])  )  );
}
/* right menu opt */


/* pkg and opt at path right menu*/

/* set tempage */

$smarty = new Smarty;
/* right menu */
for ($i=0; $i< $opt_count; $i++)
{
	if (!file_exists($opt_top5_check[$i]) )
	{
		$top5_pic1 = $path.'images/img_notfound.jpg';
	}
	else
	{
	
		$top5_pic1 = $opt_top5_pic[$i];
	}
	
	if (!file_exists($lp_top5_check[$i]) )
	{
		$top5_pic2 = $path.'images/img_notfound.jpg';
	}
	else
	{
		$top5_pic2 = $lp_top5_pic[$i];
	}

	$smarty->assign("opt_top5_link_".($i+1)	  ,'product.php?product_id='.$opt_top5_id[$i]);
	$smarty->assign("opt_top5_name_".($i+1)   ,str_len($opt_top5_name[$i],32));
	$smarty->assign("opt_top5_country_".($i+1),$opt_top5_country[$i]);
	$smarty->assign("opt_top5_pic_".($i+1)    ,$top5_pic1);
	$smarty->assign("opt_top5_detail_".($i+1) ,str_len($opt_top5_detail[$i],48));
	$smarty->assign("opt_top5_price_".($i+1)  ,$opt_top5_price[$i]);
}
/* right menu */




/* souvenir right menu */

$result = $db->front_show_souvenir($site_country,$prv_year,$prv_month,"%",0,3);


$souvenir_count = count( $result['souvenir_id']);

for ($i=0; $i<count( $result['souvenir_id']); $i++)
{
	$lp_top5_id[$i]	      = $result['souvenir_id'][$i];
	$lp_top5_name[$i]     = $result['souvenir_name'][$i] ;
	$lp_top5_country[$i]  = $result['country_name_jp'][$i];
	$lp_top5_path[$i]     = $result['path'][$i];
	
	$lp_top5_pic[$i]      = $path.'product/images/index.php?root=souvenir&amp;width=112&amp;name='.trim($result['souvenir_id'][$i].'-1.jpg'); 
	$lp_top5_check[$i]    = $img.$result['souvenir_id'][$i].'-1.jpg'; 
	
	$lp_top5_detail[$i]   = $result['short_desc'][$i];
		
	
	
	$lp_top5_price[$i]    =  $result['affiliate'][$i]?show_jp_price($result['souvenir_price'][$i]):show_location_price( $result['souvenir_price'][$i] , 
				array('sign' => $result[sign][$i],'rate' => $result[rate][$i], 'show' => $rate )	);
	
	$lp_top5_price[$i]    =  '<span class="txt-red-bold-price">'.$lp_top5_price[$i].'</span>';
	
	
}



/* pkg and opt at path right menu*/

/* set tempage */
/* right menu */
for ($i=0; $i< $souvenir_count; $i++)
{
	if (!file_exists($lp_top5_check[$i]) )
	{
		$top5_pic2 = $path.'images/img_notfound.jpg';
	}
	else
	{
		$top5_pic2 = $lp_top5_pic[$i];
	}
	
	$smarty->assign("lp_top5_link_".($i+1)	 ,'souvenir_product.php?souvenir_id='.$lp_top5_id[$i]);
	$smarty->assign("lp_top5_name_".($i+1)   ,str_len($lp_top5_name[$i],32));
	$smarty->assign("lp_top5_country_".($i+1),$lp_top5_country[$i]);
	$smarty->assign("lp_top5_pic_".($i+1)    ,$lp_top5_pic[$i]);
	$smarty->assign("lp_top5_detail_".($i+1) ,str_len($lp_top5_detail[$i],48));
	$smarty->assign("lp_top5_price_".($i+1)  ,$lp_top5_price[$i]);
}

/* right menu */


/* check cart souvenir stock */
	$cs = $_SESSION[cart_souvenir];
	$cs_array = array();

	while ($obj = current($cs)){
				
		$cs_array[] = key($cs);
		next($cs);
	}
	
	$si = implode(',',$cs_array);
	
	$sql = 'SELECT COUNT(*) sscount 
				FROM mbus_souvenir a 
				LEFT JOIN mbus_souvenir_detail b ON a.souvenir_id = b.souvenir_id 
				LEFT JOIN mbus_country c ON a.country_iso3 = c.country_iso3 
				LEFT JOIN mbus_currency d ON a.country_iso3 = d.country_iso3 
				WHERE a.souvenir_id IN ('.$si.') AND public = 1 AND affiliate = 0 AND 
				a.country_iso3 = \''.$site_country.'\' 
				ORDER BY souvenir_name';
				
	$result = $db->db_query($sql);
	while ($record = mysql_fetch_array($result)){
		$sv_count = $record[sscount]; 
	}
	
	$smarty->assign("basket_count", $sv_count);
/* check cart souvenir stock */
?>