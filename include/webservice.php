<? 
define('_WEBSERVICE_URL_'  , 'http://123.100.239.36/JTB.ClientAPI/Client.asmx/Call');
define('_WEBSERVICE_TIME_', 30);

class c_websv
{
	protected $data_params;

	public function __construct($params)
	{
		$this->data_params = $params;
	}
	
	public function getall()
	{
		$ch = curl_init(_WEBSERVICE_URL_);
		curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
		curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
		curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
		curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
		curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
		curl_setopt($ch,    CURLOPT_HEADER,             true);
		curl_setopt($ch,    CURLOPT_POST,               true);
		curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
		curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     _WEBSERVICE_TIME_);
		curl_setopt($ch,    CURLOPT_POSTFIELDS,         json_encode( $this->data_params));
		curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf8"));
		$result = curl_exec($ch);
		
		$first = strpos( $result ,'{"d":');
		$result = substr($result,$first, strlen($result));
		$raw  = json_decode($result, true);
		
		return $raw;	
	}
}	
?>