<?
require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");

if (!empty($_POST[value]))
{
    $path = DEV_PATH.'../';
	require_once($path."www_config/setting.php");
	require_once($path."class/include/c_query.php");
	require_once($path."class/c_session.php");
	require_once($path."class/c_cookie.php");
	require_once($path."class/c_common.php");
	require_once($path."class/c_themes.php");
	require_once($path."class/c_query_sub.php");

	/*----------connect DB--------------*/
		$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
		$db->db_connect();
	/*----------connect DB--------------*/

	$result = $db->select_city($_POST[value]);

    if ($_POST[value] == 'AUS'){
        $data['category'] = '<select class="form-control" name="inp_category">';
        $data['category'] .= '<option value="0">-Please Select-</option>';
        $data['category'] .= '<option value="25">海</option>';
        $data['category'] .= '<option value="26">陸</option>';
        $data['category'] .= '<option value="24">世界遺産</option>';
        $data['category'] .= '<option value="31">ワイナリー</option>';
        $data['category'] .= '<option value="27">市内観光</option>';
        $data['category'] .= '<option value="28">コアラ抱っこ</option>';
        $data['category'] .= '</select>';

        $data['option'] = '<select class="form-control" name="inp_option">';
        $data['option'] .= '<option value="0">-Please Select-</option>';
        $data['option'] .= '<option value="14">キャンペーン中</option>';
        $data['option'] .= '<option value="15">即予約可</option>';
        $data['option'] .= '<option value="16">日本語ガイド</option>';
        $data['option'] .= '<option value="17">家族割引</option>';
        $data['option'] .= '<option value="18">1人参加可能</option>';
        $data['option'] .= '<option value="19">期間限定</option>';
        $data['option'] .= '<option value="20">JTB限定</option>';
        $data['option'] .= '<option value="21">夕食付き</option>';
        $data['option'] .= '<option value="22">ランチ付き</option>';
        $data['option'] .= '<option value="23">英語ガイド</option>';
        $data['option'] .= '</select>';

        $data['time'] = '<select class="form-control" name="inp_time">';
        $data['time'] .= '<option value="0">-Please Select-</option>';
        $data['time'] .= '<option value="1">終日</option>';
        $data['time'] .= '<option value="2">午前</option>';
        $data['time'] .= '<option value="3">午後</option>';
        $data['time'] .= '<option value="4">夜のツアー</option>';
        $data['time'] .= '<option value="5">帰国日早朝参加</option>';
        $data['time'] .= '<option value="6">短時間</option>';
        $data['time'] .= '</select>';
    }else{
        $record = $db->view_theme_all();
        $data['category'] = input_selectbox('inp_category',$record['data'],$record['value'],'','-Please Select-',"form-control") ;

        $arr_option = $db->view_option_all();
        $data['option'] = input_selectbox('inp_option',$record['data'],$record['value'],'','-Please Select-',"form-control") ;

        $record = $db->viw_time_all();
        $data['time'] = input_selectbox('inp_time',$record['data'],$record['value'],'','-Please Select-',"form-control") ;
    }
    $data['country'] = $_POST[value];

}
if  ( ( is_array($result['data']) ) &&  ( is_array($result['value']) ) )
{
	$data['city'] = input_selectbox('inp_city',$result['data'],$result['value'],'','-Please Select-',"form-control") ;
}
else
{
    $data['city'] = '<select name="inp_city" class="form-control"><option value="0" >-Please Select-</option></select>';
}

echo json_encode($data);
?>