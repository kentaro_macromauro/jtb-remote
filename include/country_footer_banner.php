<?
/* For Footer banner */
$footer_banner_path  = $path.$country_path.'/images/footer_banner/';
if($site_country == 'ALL'){
    $footer_banner_path  = $path.'images/top_banner/';
}
$record = $db->fetch_footer_banner(0,20, $site_country,'yes');
$footer_banner_img = '';
for ($i=0; $i< count( $record['footer_banner_id']) ;$i++){
    if($i >= 0 && $i < 4 ){
        if($i == 0){
            $footer_banner_img .= '<div class="row">';
        }
        $footer_banner_img .= '<div class="col-md-3 col-sm-3 col-xs-6 footer-thumbnail">';
        if($record['jump'][$i] == 1){
            $target = '';
            if($record['new_window'][$i] == 1){
                $target = 'target="_blank"';
            }
            $footer_banner_img .= '<a href="'.$record['link'][$i].'" '.$target.'><img src="'.$footer_banner_path.$record['footer_banner_id'][$i].'-1.jpg"  class="img-responsive"></a>';
        }else{
            $footer_banner_img .= '<img src="'.$footer_banner_path.$record['footer_banner_id'][$i].'-1.jpg" class="img-responsive">';
        }
        $footer_banner_img .= '</div>';
        if($i == 3 || $i == count( $record['footer_banner_id']) -1){
            $footer_banner_img .= '</div>';
        }
    }
    if($i >= 4 && $i < 8 ){
        if($i == 4){
            $footer_banner_img .= '<div style="height: 5px"></div>';
            $footer_banner_img .= '<div class="row">';
        }
        $footer_banner_img .= '<div class="col-md-3 col-sm-3 col-xs-6 footer-thumbnail">';
        if($record['jump'][$i] == 1){
            $target = '';
            if($record['new_window'][$i] == 1){
                $target = 'target="_blank"';
            }
            $footer_banner_img .= '<a href="'.$record['link'][$i].'" '.$target.'><img src="'.$footer_banner_path.$record['footer_banner_id'][$i].'-1.jpg"  class="img-responsive"></a>';
        }else{
            $footer_banner_img .= '<img src="'.$footer_banner_path.$record['footer_banner_id'][$i].'-1.jpg" class="img-responsive">';
        }
        $footer_banner_img .= '</div>';
        if($i == 7 || $i == count( $record['footer_banner_id']) -1){
            $footer_banner_img .= '</div>';
        }
    }
}?>