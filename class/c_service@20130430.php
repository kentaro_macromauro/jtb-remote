<? 

define('_WEBSERVICE_URL_'  , 'https://www.ap-otms.com/API/Client.asmx/Call');			
define('_WEBSERVICE_TIME_', 30);

class c_websv
{
	protected $data_params;

	public function __construct($params)
	{
		$this->data_params = $params;
	}
	
	public function getall()
	{
		
		
		$ch = curl_init( 'https://www.ap-otms.com/API/Client.asmx/Call');
		curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
		curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
		curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
		curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
		curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
		curl_setopt($ch,    CURLOPT_HEADER,             true);
		curl_setopt($ch,    CURLOPT_POST,               true);
		curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
		
		//curl_setopt($ch,    CURLOPT_SSL_VERIFYPEER,     false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "GeoTrustGlobalCA.crt");
		
		
		curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     30);
		curl_setopt($ch,    CURLOPT_POSTFIELDS,         json_encode( $this->data_params));
		curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf8",'Authorization: Basic '.base64_encode("MBC101C1:MBC101C1") ));
		$result = curl_exec($ch);
		
		
		$first = strpos( $result ,'{"d":');
		$result = substr($result,$first, strlen($result));
		$raw  = json_decode($result, true);
		
		return $raw;	
	}
}

?>