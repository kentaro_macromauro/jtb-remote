<? 
function check_session()
{
	$status = false;
	
	if (session_id() == $_SESSION["session_id"])	
	{
		$status = true;
	}
		
	return $status;
}
	
function set_session($data)
{
	$_SESSION["session_id"] = session_id();
	$_SESSION["session_name"] = $data[0];
	$_SESSION["session_code"] = $data[1];
	$_SESSION["session_userid"] = $data[2];	
	$_SESSION["session_path"]   = $data[3];
	$_SESSION["session_country"] = $data[4];
	
		
}
	
function clear_session()
{
	unset($_SESSION["session_id"]); 
	unset($_SESSION["session_code"]); 
	unset($_SESSION["session_name"]);
	unset($_SESSION["session_userid"]);
	unset($_SESSION["session_path"]);
	unset($_SESSION["session_country"]);
}
	
	
?>