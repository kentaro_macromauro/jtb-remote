﻿<?
class c_themes
{
	protected $_path,$_root;
	
	public function __construct($_path,$root='content/themes_frontend/')
	{
		$this->_path = $_path;
		$this->_root = $root;
	}
	
	public function pbody($data = NULL)
	{		
		switch ($this->_path)
		{					
			case "backend_article_input":include($this->_root."tb_input_article.tpl"); break;
			case "backend_input_product":include($this->_root."tb_input_product.tpl"); break;
			
			case "backend_table_souvenir": include($this->_root."tb_data_souvenir.tpl"); break;
			case "backend_input_souvenir": include($this->_root."tb_input_souvenir.tpl"); break;
			
			case "backend_input_product_pkg" : include($this->_root."tb_input_product_pkg.tpl"); break;
			
			
			case "backend_input_product_rev":include($this->_root."tb_input_product_rev.tpl"); break;
			case "backend_table_product_rev":include($this->_root."tb_data_product_review.tpl"); break;
			
			
			case "backent_input_product_next":include($this->_root."tb_input_product_next.tpl"); break;
			case "backend_input_banner" :include($this->_root."tb_input_banner.tpl"); break;

            case "backend_input_top_banner" :include($this->_root."tb_input_top_banner.tpl"); break;
            case "backend_input_footer_banner" :include($this->_root."tb_input_footer_banner.tpl"); break;

			case 'backend_input_hotel' : include($this->_root."tb_input_hotel.tpl"); break;
			
			case "backend_login":include($this->_root."login.tpl"); break;
			case "backend_index":include($this->_root."index.tpl"); break;
			case "backend_table_approve":include($this->_root."tb_data_approve.tpl"); break;
			case "backend_table":include($this->_root."tb_data.tpl");  break;
			case "backend_table_readonly":include($this->_root."tb_data_readonly.tpl");  break;
			
			case "backend_table_product":include($this->_root."tb_data_product.tpl");  break;
            case "backend_table_api_product":include($this->_root."tb_data_api_product.tpl");  break;
            case "backend_product_detail":include($this->_root."tb_data_product_detail.tpl");  break;

			case "backend_table_article":include($this->_root."tb_data_article.tpl");  break;
			case "backend_input":include($this->_root."tb_input.tpl"); break;	
			
			case "backend_input_branch_content":include($this->_root."tb_input_branch_content.tpl"); break;
			
			case "backend_search":include($this->_root."tb_input_search.tpl"); break;
			
			
			
			case "backend_input_promotion" : include($this->_root."tb_input_promotion.tpl"); break;
			
			case "backend_input_souvenir_campaign" : include($this->_root."tb_input_souvenir_campaign.tpl"); break;
			
			case "backend_input_rate" : include($this->_root."tb_input_rate.tpl"); break;


			case "backend_input_photosnap_promotion" : include($this->_root."tb_input_promotion_photosnap.tpl"); break;
			
			case "backend_input_photosnap_souvenir_campaign" : include($this->_root."tb_input_souvenir_campaign_photosnap.tpl"); break;
		}
	}
		
}
?>