<?php
class auto_query extends c_db
{
	/* all unit */
	public function __construct($s_host,$s_name,$s_user,$s_pass = null)
	{
		parent::__construct($s_host,$s_name,$s_user,$s_pass);
	}
	/* all unit*/
	
	public function create_session()
	{
		$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'session value(NULL); ';
		
		$insert_id = $this->db_query($sql);
		
		return $insert_id;
	}
	
	public function find_age($country_iso3)
	{
		$sql = 'SELECT 	infant_age,child_age FROM '._DB_PREFIX_TABLE.'country WHERE country_iso3 = "'.$country_iso3.'" ';
			  
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$child_age = $record[child_age];	
			$infant_age = $record[infant_age];
		}
		
		return array( $child_age , $infant_age );
	}
	
	
	public function paymentcode($country_iso3)
	{
		$sql = 'SELECT country_iso3,sitecode,telephone,correncycode FROM '._DB_PREFIX_TABLE.
		       'payment_connect WHERE country_iso3 = "'.$country_iso3.'" ';
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$country_iso3 	= $record[country_iso3];
			$sitecode 		= $record[sitecode];
			$telephone 		= $record[telephone];
			$correncycode 	= $record[correncycode];
		}
		
		return array( 'country' => $country_iso3 , 'sitecode' => $sitecode ,'tel' => $telephone ,'code' => $correncycode);
	}
	
	public function view_country_jp($country_iso3)
	{
		$sql  = 'SELECT country_name_jp FROM '._DB_PREFIX_TABLE.country.' ';
		$sql .= 'WHERE country_iso3 = "'.$country_iso3.'" ';
			
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$country_name_jp  = $record[country_name_jp];
		}
		
		
		
		return $country_name_jp;
	}
	
	/* hotel group */
	public function view_hotel_Group($group_id)
	{
		$sql = 'SELECT a.country_iso3,a.group_id,jtb_groupid,group_name,group_id_ref,hotel_id 
				FROM '._DB_PREFIX_TABLE.'grouphotel a 
				LEFT JOIN '._DB_PREFIX_TABLE.'grouphotel_detail b ON 
				a.country_iso3 = b.country_iso3 AND a.group_id = b.group_id 
				WHERE a.group_id = "'.$group_id.'" 
				ORDER by country_iso3,group_id,group_id_ref';
				
		$result = $this->fetch_query($sql);
		
		return $result;	
	}
	
	
	public function hotel_group_count($country_code="")
	{
		$sql=  'SELECT COUNT(*) dd_count FROM '._DB_PREFIX_TABLE.'grouphotel ';
		
		if ($country_code != "")
		{
			$sql .= 'WHERE country_code = "'.$country_code.'" ';	
		}
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd_count = $record[dd_count];
		}
		
		return $dd_count;
	}
	
	
	public function fetch_hotel_group($page_first,$page_num,$country_code="")
	{
		
		$sql  = 'SELECT a.country_iso3,group_id,group_name,a.update_date,admin_name update_by FROM '._DB_PREFIX_TABLE.'grouphotel a ';
		$sql .= 'LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON a.update_by = b.admin_id ';
		
		if ($country_code != "")
		{
			$sql .= 'WHERE a.country_iso3 = "'.$country_code.'" ';	
		}
		
		$sql .= 'ORDER BY a.country_iso3,update_date desc,jtb_groupid,group_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec[group_id][$i] 	 = $record[group_id];
			$rec[country_iso3][$i] = $record[country_iso3];
			
			$rec[group_name][$i]  = $record[group_name];
			$rec[update_date][$i] = $record[update_date];
			$rec[update_by][$i]	 = $record[update_by];
			
			$i++;
		}
		
		return $rec ;
		
	}
	
	public function view_hotel_list($page_id)
	{

		$sql = 'SELECT hotel_id,country_iso3,city_iso3,hotel_code,hotel_name_jp,hotel_name_en FROM '._DB_PREFIX_TABLE.'hotel ';
		$sql .= 'WHERE country_iso3 = "'.$page_id.'" ';
		
		
		$result = $this->db_query($sql); $i= 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$value[$i]  = $record[hotel_id];
			$data[$i] 	= $record[hotel_name_jp];
			
			$i++;
		}
		
		return array('value' => $value,'data' => $data);
	}	
	/* hotel group */
	
	/* find_group */
	
	public function get_product_code($product_id)
	{
		$sql = 'SELECT product_code FROM '._DB_PREFIX_TABLE.'product WHERE product_id = "'.$product_id.'" ';
		$result = $this->db_query($sql);
		
		
		
		while ($record = mysql_fetch_array($result))
		{
			$product_code = $record[product_code];
			
		}
		
		
		
		return $product_code;
	}
	
	public function get_product_type($product_id)
	{
		$sql = 'SELECT product_type FROM '._DB_PREFIX_TABLE.'product WHERE product_id = "'.$product_id.'" ';
		$result = $this->db_query($sql);
		
		
		
		while ($record = mysql_fetch_array($result))
		{
			$product_type = $record[product_type];
			
		}
		
		
		
		return $product_type;
	}
	
	/* find_group */
	
	/*Branch SP Contents start*/
	public function count_branch_content($iso3="",$public="")
	{
		$sql  = 'SELECT COUNT(*) dd FROM '._DB_PREFIX_TABLE.'branch_content ';
		if (!empty($iso3))
		{
			$sql .= 'WHERE country_iso3 = "'.$iso3.'" ';
		}
		
		if (!empty($public))
		{
			$sql .= "AND ";
			
			if ($public == 'yes')
			{
				$sql .= ' public = "1" ';
			}
			
			if ($public == 'no') 
			{
				$sql .= ' public = "0" ';	
			}
			
		}
		
		$result = $this->db_query($sql);
		while ($record = mysql_fetch_array($result))
		{
			$count = $record[dd];	
		}

		return $count;	
	}
	
	
	
	public function fetch_branch_content($page_first,$page_num,$iso3="",$public="")
	{
		$sql = 'SELECT branchct_id,a.country_iso3,country_name_jp,branchct_subject,branchct_countent_short,public,a.update_date   
				FROM '._DB_PREFIX_TABLE.'branch_content a LEFT JOIN '._DB_PREFIX_TABLE.'country b ON  a.country_iso3 = b.country_iso3 ' ;
				
		if (!empty($iso3))
		{
			$sql .= 'WHERE a.country_iso3 = "'.$iso3.'" ';
		}
		
		if (!empty($public))
		{
			$sql .= "AND ";
			
			if ($public == 'yes')
			{
				$sql .= ' public = "1" ';
			}
			
			if ($public == 'no') 
			{
				$sql .= ' public = "0" ';	
			}
			
		}
		
		$sql .= 'ORDER BY a.country_iso3,update_date DESC,branchct_subject LIMIT '.$page_first.','.$page_num;

	

		
		$result = $this->db_query($sql);

		$i =0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  			 	= $rec['branchct_id'];
			$record['country'][$i]  	 	= $rec['country_iso3'];
			$record['country_name_jp'][$i]  = $rec['country_name_jp'];
			$record['subject'][$i]		 	= $rec['branchct_subject'];
			$record['short_content'][$i] 	= $rec['branchct_countent_short'];
			$record['update_date'][$i]  	= $rec['update_date'];
			$record['public'][$i]   	 	= $rec['public'];
			$i++;
		}
		
		return $record;
	}
	
	
	public function view_branch_content($id)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_TABLE.'branch_content WHERE branchct_id = "'.trim($id).'" ';
		
		$result = $this->db_query($sql);
		
		while ($rec = mysql_fetch_array($result))
		{
			$record = $rec;
		}
	
		return $record ;
	}
	
	/*Branch SP Contents end*/
	
	/* Photosnap */	
	public function front_show_photosnap_slider($product_type,$country_iso3)
	{
		$sql = 'SELECT country_name_jp,a.country_iso3,product_id,product_name_jp,price_min,price_max, price_adult,price_child ,price_infant, 
				short_desc,rev_status,rev_date,path,rate,sign FROM 
				(SELECT country_name_jp,a.country_iso3,CONCAT(\'%;\',product_id,\';%\') product_idc,product_id,product_name_jp,price_min,price_max, 
				price_adult,price_child ,price_infant, 
				short_desc,rev_status,rev_date,path FROM '._DB_PREFIX_TABLE.'product a,'._DB_PREFIX_TABLE.'country b
				WHERE a.country_iso3 = b.country_iso3 AND a.public = 1) As a ,
				(SELECT CONCAT(\';\',product_list) product_list FROM '._DB_PREFIX_TABLE.'photosnap WHERE country_iso3 = "'.$country_iso3.
				'" AND product_type = "'.$product_type.'") AS b ,
				mbus_currency c 
				WHERE product_list LIKE product_idc AND 
				c.country_iso3 = a.country_iso3';
			
		
			
				
		$result = $this->db_query($sql);
		
		$i =0; 
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i]	   = $rec[product_id];
			$record['path'][$i]			   = $rec[path];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['short_desc'][$i]      = $rec[short_desc];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]       = $rec[price_max];
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$record['price_adult'][$i] 	   = $rec[price_adult];
			$record['price_child'][$i]     = $rec[price_child];
			$record['price_infant'][$i]    = $rec[price_infant];
			$record['country_iso3'][$i]    = $rec[country_iso3];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			$record['currency_rate'][$i]   = $rec[rate];
			$record['currency_sign'][$i]   = $rec[sign];
			
			$i++;
		}
		
		
		
		
		$sql = 'SELECT product_list FROM '._DB_PREFIX_TABLE.'photosnap WHERE country_iso3 = "'.$country_iso3.
			   '" AND product_type = "'.$product_type.'"';
			   
			   
		
		$result = $this->db_query($sql);
		
		$i = 0 ;
		
		while ($rec = mysql_fetch_array($result))
		{
			$product_list = $rec[product_list];	
		}
		
		
		 $j = 0;
		$arr_templist  = explode(';',$product_list);
		
		for ($i = 0; $i < count($arr_templist) ; $i++)
		{
			if ($arr_templist[$i] != 0)
			{
				$arr_list[$j]  = $arr_templist[$i];
				$j ++;
			}
		}
		
	
		$arrive_data = array();
		
		for ($i = 0; $i <count($arr_list); $i++)
		{
			if ($arr_list[$i] != '')
			{
			 $focus                              = array_search($arr_list[$i], $record['product_id']);
			 $arrive_data['product_id'][$i]		 =	$record['product_id'][$focus];
			 $arrive_data['product_name_jp'][$i] =	$record['product_name_jp'][$focus];
			 $arrive_data['path'][$i]			 =  $record['path'][$focus];
			 $arrive_data['short_desc'][$i]		 =	$record['short_desc'][$focus];
			 $arrive_data['price_min'][$i]		 =	$record['price_min'][$focus];
			 $arrive_data['price_max'][$i]		 =	$record['price_max'][$focus];
			 $arrive_data['rev_status'][$i]		 =	$record['rev_status'][$focus];
			 $arrive_data['rev_date'][$i]		 =	$record['rev_date'][$focus];
			 $arrive_data['price_adult'][$i]	 =	$record['price_adult'][$focus];
			 $arrive_data['price_child'][$i]	 =	$record['price_child'][$focus];
			 $arrive_data['price_infant'][$i]	 = 	$record['price_infant'][$focus];
			 $arrive_data['country_iso3'][$i]    =  $record['country_iso3'][$focus];
			 $arrive_data['country_name_jp'][$i] =  $record['country_name_jp'][$focus];
			 $arrive_data['currency_rate'][$i]   =  $record['currency_rate'][$focus];
			 $arrive_data['currency_sign'][$i]   =  $record['currency_sign'][$focus];
			 
			 
			}
		}
		
	
		
		return $arrive_data;
		
		
		
		
	}
	
	
	
	public function page_count_photosnap()
	{
		return 20;
	}
	
	
	public function fetch_photosnap($country_iso3code,$type)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_TABLE.'photosnap WHERE country_iso3 = "'.trim($country_iso3code).'" AND product_type = "'.$type.'" ';
	
		$result = $this->db_query($sql);
		
		while ($rec = mysql_fetch_array($result))
		{
			$record = $rec;	
		}
		
		return $record;
	}
	
	/* Photosnap end */
	
	
	
	/*Country News */
	public function count_news($iso3="",$public="")
	{
		$sql  = 'SELECT COUNT(*) dd FROM '._DB_PREFIX_TABLE.'country_news ';
		if (!empty($iso3))
		{
			$sql .= 'WHERE country_iso3 = "'.$iso3.'" ';
		}
		
		if (!empty($public))
		{
			$sql .= "AND ";
			
			if ($public == 'yes')
			{
				$sql .= ' public = "1" ';
			}
			
			if ($public == 'no') 
			{
				$sql .= ' public = "0" ';	
			}
			
		}
		
		$result = $this->db_query($sql);
		while ($record = mysql_fetch_array($result))
		{
			$count = $record[dd];	
		}

		return $count;
	}
	
	public function fetch_news($page_first,$page_num,$iso3="",$public="")
	{
		$sql = 'SELECT news_id,a.country_iso3,country_name_jp,news_date,news_subject,icon,public  
				FROM '._DB_PREFIX_TABLE.'country_news a LEFT JOIN '._DB_PREFIX_TABLE.'country b ON a.country_iso3 = b.country_iso3 ';
				
		if (!empty($iso3))
		{
			$sql .= 'WHERE a.country_iso3 = "'.$iso3.'" ';
		}
		
		if (!empty($public))
		{
			$sql .= "AND ";
			
			if ($public == 'yes')
			{
				$sql .= ' public = "1" ';
			}
			
			if ($public == 'no') 
			{
				$sql .= ' public = "0" ';	
			}
			
		}
		
		$sql .= 'ORDER BY country_iso3,news_date DESC,a.update_date DESC,news_subject LIMIT '.$page_first.','.$page_num;


		
		$result = $this->db_query($sql);

		$i =0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  		       = $rec['news_id'];
			$record['country'][$i]         = $rec['country_iso3'];
			$record['country_name_jp'][$i] = $rec['country_name_jp'];
			$record['date'][$i]  	       = $rec['news_date'];
			$record['subject'][$i]	       = $rec['news_subject'];
			$record['icon'][$i]  	       = $rec['icon'];
			$record['public'][$i]          = $rec['public'];
			$i++;
		}
		
		return $record;
	}
	
	
	public function view_news($id)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_TABLE.'country_news WHERE news_id = "'.trim($id).'" ';
		
		$result = $this->db_query($sql);
		
		while ($rec = mysql_fetch_array($result))
		{
			$record = $rec;
		}
		
		
	
		return $record ;
	}

	/*Country News */
	
	
	/* japan city */
	
	public function view_pref($page_id)
	{
		$sql = 'SELECT name FROM '._DB_PREFIX_TABLE.'pref ';
		$sql .= 'WHERE id = '.$page_id;

		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$name = $record[name];	
		}
		
		return $name;
	}
		
	
	public function view_japan_city()
	{
		$sql = 'SELECT id,name FROM '._DB_PREFIX_TABLE.'pref ORDER BY rank';
		$result = $this->db_query($sql);
		
		$i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[$i] = array( 'id' => $rec['id'], 'name' =>  $rec['name']);
			$i++;
		}
		
		return $record;		
	}
	
	/* japan city */
	
	/* hotel */
	public function select_hotel($country="")
	{
		if (!empty($country))
		{
			$sql = 'SELECT hotel_id,hotel_code,hotel_name_jp,group_id FROM '._DB_PREFIX_TABLE.'hotel 
					WHERE country_iso3 = "'.$country.'"
					ORDER BY hotel_name_jp';
		}
		else
		{
			$sql = 'SELECT hotel_id,hotel_code,hotel_name_jp,group_id FROM '._DB_PREFIX_TABLE.'hotel 
					ORDER BY hotel_name_jp';
		}
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record[$i] = array('id' => $rec['hotel_id'],
								'name' => $rec['hotel_name_jp'],
								'code' => $rec['hotel_code'],
								'group' => $rec['group_id'],
						);
			$i++;
		}
		
		return $record ;
	}
	
	public function view_hotel($page_id)
	{
		$raw = array(  'rec'  => array( 'hotel_id','country_iso3','city_iso3','hotel_code','hotel_name_en','hotel_name_jp','pic','hotel_tel',
									    'address1','address2','gmap','group_id'), 
					   'field' => _DB_PREFIX_TABLE.'hotel',
					   'compare' => 'hotel_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		$record = mysql_fetch_array($result);

		return $record;
	}
	
	
	public function fetch_hotel($page_first,$page_num)
	{
		$sql = 'SELECT hotel_id,country_iso3,hotel_code,city_iso3,hotel_name_en,hotel_name_jp,group_id  
				FROM '._DB_PREFIX_TABLE.'hotel ORDER BY country_iso3,city_iso3,hotel_name_en,hotel_name_jp 
				LIMIT '.$page_first.','.$page_num;

		
		$result = $this->db_query($sql);

		$i =0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  		= $rec['hotel_id'];
			$record['country'][$i]  = $rec['country_iso3'];
			$record['city'][$i]  	= $rec['city_iso3'];
			$record['code'][$i]		= $rec['hotel_code'];
			$record['name_en'][$i]  = $rec['hotel_name_en'];
			$record['name_jp'][$i]  = $rec['hotel_name_jp'];
			$record['group_id'][$i] = $rec['group_id'];
			$i++;
		}
		
		return $record;
	}	
	/* hotel */
	/* site service */
	
	
	public function page_search_lp_count($country,$city,$keyword,$category,$option2,$review,$time)
	{

		$sql  = 'SELECT COUNT(*) ddcount   
				 FROM '._DB_PREFIX_TABLE.'product a LEFT JOIN '._DB_PREFIX_TABLE.'country b ON a.country_iso3 = b.country_iso3 
				 LEFT JOIN (SELECT product_id,sum(amount) amount FROM 
				 '._DB_PREFIX_TABLE.'monthly_product GROUP BY product_id ) as c 
				 ON a.product_id = c.product_id ';
		$sql .= 'WHERE product_type = "PKG" AND ';
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" ) AND a.public = 1 ';
		
		
		if (!empty($country))
		{
		
			$sql .= ' AND a.country_iso3 LIKE "%'.$country.'%" ';
		}
		
		if (!empty($city))
		{
		
			$sql .= ' AND city_iso3 LIKE "%'.$city.'%" ';
		}
		
		if (!empty($category))
		{
			$sql .= ' AND product_category = "'.$category.'" ';
		}
		
		if (!empty($option2))
		{
			$sql .= ' AND option_id2 LIKE "%'.$option2.'%" ';
		}
		
		if (!empty($review))
		{
			if ($review == 2)
			{
				$sql .= ' AND rev_status = "0" ';
			}
			else
			{
				$sql .= ' AND rev_status = "1" ';
			}
		}
		
		if (!empty($time))
		{
			$sql .= ' AND status_day LIKE "%'.$time.'%" ';
		}
		
		
		$result = $this->db_query($sql); 
		
		while ($record = mysql_fetch_array($result))
		{
			$rec = $record[ddcount];	
		}
		
		return $rec;
	}
	
	
	
	
	public function page_search_lp($country,$city,$keyword,$category,$option2,$review,$time,$order,$page_first,$page_num)
	{
		
	    $sql_order = '';
		
		switch ($order)
		{
			case '1' : $sql_order = 'ORDER BY price_min DESC,a.country_iso3,product_name_jp'; break;	
			case '2' : $sql_order = 'ORDER BY price_min,a.country_iso3,product_name_jp';      break;
			case '3' : $sql_order = 'ORDER BY amount DESC,a.country_iso3,product_name_jp';    break;
			default  : $sql_order = 'ORDER BY amount DESC,a.country_iso3,product_name_jp';    break;	
		}
		
		$sql  = 'SELECT a.product_id,country_name_jp,path,a.country_iso3,city_iso3,product_code,product_category,product_name_jp, 
				 price_min,day_quantity,day_cutoff,rev_status,option_id2, 
				 description,short_desc,description,template_id,keyword_search ,remark,public,amount,rev_status,
				 category_id,category_name,rate,sign  
				 FROM '._DB_PREFIX_TABLE.'product a 
				 LEFT JOIN '._DB_PREFIX_TABLE.'country b ON a.country_iso3 = b.country_iso3 
				 LEFT JOIN '._DB_PREFIX_TABLE.'currency e ON a.country_iso3 = e.country_iso3 
				 
				 LEFT JOIN '._DB_PREFIX_TABLE.'category d ON a.product_category = d.category_id 
				 
				 LEFT JOIN (SELECT product_id,sum(amount) amount FROM 
				 '._DB_PREFIX_TABLE.'monthly_product GROUP BY product_id ) as c 
				 ON a.product_id = c.product_id ';
		$sql .= 'WHERE product_type = "PKG" AND ';
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" ) AND a.public = 1  ';
		
		
		if (!empty($country))
		{
		
			$sql .= ' AND a.country_iso3 LIKE "%'.$country.'%" ';
		}
		
		if (!empty($city))
		{
		
			$sql .= ' AND city_iso3 LIKE "%'.$city.'%" ';
		}
		
		if (!empty($category))
		{
			
			$sql .= ' AND product_category = "'.$category.'" ';
		}
		
		if (!empty($option2))
		{
			$sql .= ' AND option_id2 LIKE "%'.$option2.'%" ';
		}
		
		if (!empty($review))
		{
			if ($review == 2)
			{
				$sql .= ' AND rev_status = "0" ';
			}
			else
			{
				$sql .= ' AND rev_status = "1" ';
			}
		}
		
		if (!empty($time))
		{
			$sql .= ' AND status_day LIKE "%'.$time.'%" ';
		}
		

		
		$sql .=  $sql_order;
		$sql .= ' LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); 
		
		$i=0;
		while ($record = mysql_fetch_array($result))
		{
			$rec[$i] = $record;	
			
			$i++;
		}
		
		
		
		return $rec;
	}
	
	
	public function page_search_opt_count($country,$city,$keyword,$theme,$option1,$review,$time)
	{
		$sql_order = '';
		
		
		$sql  = 'SELECT COUNT(*) ddcount  
				 FROM '._DB_PREFIX_TABLE.'product a 
				 LEFT JOIN (SELECT product_id,sum(amount) amount FROM 
				 '._DB_PREFIX_TABLE.'monthly_product GROUP BY product_id ) as b 
				 ON a.product_id = b.product_id , '._DB_PREFIX_TABLE.'country b ';
		$sql .= 'WHERE a.country_iso3 = b.country_iso3 AND  product_type = "OPT" AND ';	
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" ) AND a.public =1 ';
		
		
		if (!empty($country))
		{
		
			$sql .= ' AND a.country_iso3 LIKE "%'.$country.'%" ';
		}
		
		if (!empty($city))
		{
		
			$sql .= ' AND city_iso3 LIKE "%'.$city.'%" ';
		}
		
		
		
		
		if (is_array($theme))
		{
			$sql .= ' AND ( ';
			
			for ($i = 0; $i < count($theme) ; $i++)
			{
				$sql .= ' product_theme LIKE "%'.$theme[$i].'%" ';	
				
				if ( (count($theme) > 1)&&($i != (count($theme)-1) ) )
				{
					$sql .= ' OR '	;
				}
			}
			
			$sql .= ' ) ';
			
		}
		else
		{
			if (!empty($theme))
			{
				$sql .= ' AND product_theme LIKE "%'.$theme.'%" ';
			}
		}
		
		
		if (!empty($option1))
		{
			$sql .= ' AND option_id1 LIKE "%'.$option1.'%" ';
		}
		
		if (!empty($review))
		{
			if ($review == 2)
			{
				$sql .= ' AND rev_status = "0" ';
			}
			else
			{
				$sql .= ' AND rev_status = "1" ';
			}
		}
		
		if (!empty($time))
		{
			$sql .= ' AND status_time LIKE "%'.$time.'%" ';
		}
		

		
		$result = $this->db_query($sql); 
		
		while ($record = mysql_fetch_array($result))
		{
			$rec= $record[ddcount];	
			
		}
		
		return $rec;	
	}	
	
	public function page_search_opt($country,$city,$keyword,$theme,$option1,$review,$time,$order,$page_first,$page_num)
	{
		
		
		
	    $sql_order = '';
		
		switch ($order)
		{
			case '1' : $sql_order = 'ORDER BY price_min DESC,a.country_iso3,product_name_jp'; break;	
			case '2' : $sql_order = 'ORDER BY price_min,a.country_iso3,product_name_jp'; break;
			case '3' : $sql_order = 'ORDER BY amount DESC,a.country_iso3,product_name_jp'; break;
			default : $sql_order = 'ORDER BY amount DESC,a.country_iso3,product_name_jp'; break;	
		}
		
		$sql  = 'SELECT a.product_id,country_name_jp,path,a.country_iso3,city_iso3,product_code,product_theme,product_name_jp, 
				 product_theme,product_theme,price_min,day_quantity,day_cutoff,rev_status,option_id1, 
				 description,short_desc,description,template_id,keyword_search ,remark,public,amount,rev_status ,rate,sign  
				 FROM '._DB_PREFIX_TABLE.'product a  
				 LEFT JOIN '._DB_PREFIX_TABLE.'currency e ON a.country_iso3 = e.country_iso3 
				 LEFT JOIN (SELECT product_id,sum(amount) amount FROM 
				 '._DB_PREFIX_TABLE.'monthly_product GROUP BY product_id ) as c 
				 ON a.product_id = c.product_id ,'._DB_PREFIX_TABLE.'country b ';
		$sql .= 'WHERE a.country_iso3 = b.country_iso3 AND product_type = "OPT" AND ';
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" ) AND a.public = 1 ';
		
		
		if (!empty($country))
		{
		
			$sql .= ' AND a.country_iso3 LIKE "%'.$country.'%" ';
		}
		
		if (!empty($city))
		{
		
			$sql .= ' AND city_iso3 LIKE "%'.$city.'%" ';
		}
		
		
		if (is_array($theme))
		{
			$sql .= ' AND ( ';
			
			for ($i = 0; $i < count($theme) ; $i++)
			{
				$sql .= ' product_theme LIKE "%'.$theme[$i].'%" ';	
				
				if ( (count($theme) > 1)&&($i != (count($theme)-1) ) )
				{
					$sql .= ' OR '	;
				}
			}
			
			$sql .= ' ) ';
			
		}
		else
		{
			if (!empty($theme))
			{
				$sql .= ' AND product_theme LIKE "%'.$theme.'%" ';
			}
		}
		
		if (!empty($option1))
		{
			$sql .= ' AND option_id1 LIKE "%'.$option1.'%" ';
		}
		
		if (!empty($review))
		{
			if ($review == 2)
			{
				$sql .= ' AND rev_status = "0" ';
			}
			else
			{
				$sql .= ' AND rev_status = "1" ';
			}
		}
		
		if (!empty($time))
		{
			$sql .= ' AND status_time LIKE "%'.$time.'%" ';
		}
		
		
		$sql .=  $sql_order;
		$sql .= ' LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql); 
		
		$i=0;

		while ($record = mysql_fetch_array($result))
		{
			$rec[$i] = $record;	
			
			$i++;
		}
		
		return $rec;
	}
	
	
	public function page_search_count($country,$city,$category,$keyword,$status_time='')
	{
		$sql = 'SELECT COUNT(*) num 
				FROM '._DB_PREFIX_TABLE.'product 
				WHERE country_iso3 LIKE "%'.$country.'%" AND ';
		$sql .= 'city_iso3 LIKE "%'.$city.'%" AND ';
		$sql .= 'product_category LIKE "%'.$category.'%" AND ';
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" )';
		
		if (($status_time != '')&&($status_time != '0'))
		{
			$sql .= ' AND (status_time = "'.$status_time.'") ';
		}
	
	
		$result = $this->db_query($sql);
		
		
		while ($rec = mysql_fetch_array($result))
		{
			$data 		= $rec['num'];
			
			
		}
		
		return $data ;	
	}	
	
	public function page_search($country,$city,$category,$keyword,$status_time='',$page_first,$page_num,$order='')
	{
		switch ($order)
		{
			case 1 : $addon = '';	break;
			case 2 : $addon = 'DESC'; break;
			default : $addon = ''; 
		}

		$sql = 'SELECT product_id,country_iso3,city_iso3,product_code,product_type,product_name_en,product_name_jp, 
				product_category,status_time,status_book,price_min,price_max,day_quantity,day_cutoff,
				description,short_desc,description,template_id,keyword_search ,remark,public 
				FROM '._DB_PREFIX_TABLE.'product 
				WHERE country_iso3 LIKE "%'.$country.'%" AND ';
		$sql .= 'city_iso3 LIKE "%'.$city.'%" AND ';
		$sql .= 'product_category LIKE "%'.$category.'%" AND ';
		$sql .= '(product_name_jp LIKE "%'.$keyword.'%" OR ';
		$sql .= 'description LIKE "%'.$keyword.'%" OR ';
		$sql .= 'keyword_search LIKE "%'.$keyword.'%" ) ';
		
		if (($status_time != '')&&($status_time != '0'))
		{
			$sql .= ' AND (status_time = "'.$status_time.'") ';
		}
		
		$sql .= 'ORDER BY price_min '.$addon.',price_max '.$addon.',product_name_en LIMIT '.$page_first.','.$page_num;
	
		//echo $sql.'    : test system'; exit();
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i] 		= $rec['product_id'];
			$record['country_iso3'][$i] 	= $rec['country_iso3'];
			$record['city_iso3'][$i]		= $rec['city_iso3'];
			$record['product_code'][$i] 	= $rec['product_code'];
			$record['product_type'][$i] 	= $rec['product_type'];
			$record['product_name_en'][$i]  = $rec['product_name_en'];
			$record['product_name_jp'][$i]  = $rec['product_name_jp'];
			$record['product_category'][$i] = $rec['product_category'];
			$record['status_time'][$i]  	= $rec['status_time'];
			$record['status_book'][$i] 		= $rec['status_book'];
			$record['price_min'][$i] 		= $rec['price_min'];
			$record['price_max'][$i] 		= $rec['price_max'];
			$record['day_quantity'][$i] 	= $rec['day_quantity'];
			$record['day_cutoff'][$i] 		= $rec['day_cutoff'];
			$record['description'][$i]		= $rec['description'];
			$record['short_desc'][$i] 		= $rec['short_desc'];
			$record['description'][$i] 		= $rec['description'];

			$record['keyword_search'][$i] 	= $rec['keyword_search'];
			$record['remark'][$i] 			= $rec['remark'];
			
			$i++;
			
			
		}
		
		return $record ;	
	}
	
	/* site service */
	
	public function get_currency_rate($country_iso3)
	{
		$sql = 'SELECT rate,code,sign FROM '._DB_PREFIX_TABLE.'currency WHERE country_iso3 ="'.$country_iso3.'" ';
		
		$rate = $this->fetch_query($sql);
		/*$result = $this->db_query($sql);
		
		while ($rec = mysql_fetch_array($result))
		{
			$rate = $rec['rate'];
		}
		*/
		return $rate;
	}
	
	/* top category page */
	
	public function fatch_country_category($page_first,$page_num)
	{
		$sql=  'SELECT lcat_id,lcat_index,country_iso3,lcat_type,lcat_name,
				lcat_category,a.update_date,admin_name update_by
				FROM 
				'._DB_PREFIX_TABLE.'country_category a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				ORDER BY country_iso3,lcat_index,lcat_name LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  	 		 = $rec['lcat_id'];
			$record['lcat_index'][$i]  	 = $rec['lcat_index'];
			$record['country_iso3'][$i]  = $rec['country_iso3'];
			$record['lcat_type'][$i]  	 = $rec['lcat_type'];
			$record['lcat_name'][$i]   	 = $rec['lcat_name'];
			$record['update_date'][$i]   = $rec['update_date'];
			$record['update_by'][$i]	 = $rec['update_by'];
			$i++;
		}

		return $record;
	}
	
	public function select_theme()
	{
		$raw = array(  'rec'  => array( 'theme_id','theme_name' ), 
					   'field' => _DB_PREFIX_TABLE.'theme' );
		
		
		$result = $this->view_select($raw);
		
		$i = 0;
		while ( $rec = mysql_fetch_array($result))
		{
			
			$record['data'][$i]  = $rec['theme_name'];
			$record['value'][$i] = $rec['theme_id']; 
			$i++;
		}
		
		
		return $record;
	}
	
	public function view_country_category($page_id)
	{
		$raw = array(  'rec'  => array( 'lcat_id','lcat_index','country_iso3','lcat_type','lcat_name','lcat_category' ), 
					   'field' => _DB_PREFIX_TABLE.'country_category',
					   'compare' => 'lcat_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		$record = mysql_fetch_array($result);

		return $record;
		
	}
	
	/* top category page */
	
	/* policy */
	public function fatch_policy($page_first,$page_num)
	{
		$sql  = 'SELECT policy_id,policy_title_en,policy_title_jp,policy_title_short_en,
				 policy_title_short_jp,public,a.update_date,admin_name update_by FROM ';
		$sql .= _DB_PREFIX_TABLE.'policy a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				ORDER BY policy_id,policy_title_en,policy_title_jp LIMIT '.$page_first.','.$page_num;
				
		$result = $this->db_query($sql);
		
		$i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  	 		 		 = $rec['policy_id'];
			$record['policy_title_en'][$i] = $rec['policy_title_en'];
			$record['policy_title_jp'][$i] = $rec['policy_title_jp'];
			$record['policy'][$i]  	 		 	 = $rec['policy'];
			$record['update_date'][$i]  	 	 = $rec['update_date'];
			$record['update_by'][$i]  	 		 = $rec['update_by'];
			
			$i++;
		}
		
		return $record;
		
	}
	
	public function view_policy($policy_id)
	{
		$sql = 'SELECT a.policy_id,policy_title_en,policy_title_jp,
				policy_title_short_en,policy_title_short_jp,seq,policy_content_en,policy_content_jp,public  
				FROM '._DB_PREFIX_TABLE.'policy a LEFT JOIN 
				'._DB_PREFIX_TABLE.'policy_detail b ON
				a.policy_id = b.policy_id 
				WHERE a.policy_id = "'.$policy_id.'"
				ORDER BY a.policy_id,b.seq'	;
				
				
				
		$result = $this->db_query($sql);
		
		$i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['id'][$i]  	 		 		 = $rec['policy_id'];
			$record['policy_title_en'][$i] 	     = $rec['policy_title_en'];
			$record['policy_title_jp'][$i] 	     = $rec['policy_title_jp'];
			$record['policy_title_short_en'][$i] = $rec['policy_title_short_en'];
			$record['policy_title_short_jp'][$i] = $rec['policy_title_short_jp'];
			$record['public'][$i]  	 		 	 = $rec['public'];
			$record['seq'][$i]					 = $rec['seq'];
			$record['policy_content_en'][$i]     = $rec['policy_content_en'];
			$record['policy_content_jp'][$i]	 = $rec['policy_content_jp'];
			
			$i++;
		}
		
		return $record;
	}
	/* policy */
	
	/* mybus lp page */
	public function front_show_product_lp($country,$yyyy,$mm,$option="",$page_first,$page_num)
	{
		$sql = 'SELECT a.country_iso3,path,country_name_jp,a.product_id,product_name_jp,price_min,price_max, price_adult,price_child ,price_infant, 
				short_desc,amount,rev_status,rev_date FROM '._DB_PREFIX_TABLE.'product a 
				LEFT JOIN (SELECT * FROM '._DB_PREFIX_TABLE.'monthly_product WHERE mpro_year = "'.$yyyy.'" AND mpro_month = "'.$mm.'" ) AS b ON 
				a.product_id = b.product_id ,'._DB_PREFIX_TABLE.'country c ';
		
		if ($country == "ALL")
		{
			$sql .= 'WHERE ';
		}
		else
		{
			$sql .= 'WHERE a.country_iso3 = "'.$country.'" AND ';
		}
		
		$sql .= 'a.product_type = "PKG" AND 
				option_id2 = "'.$option.'" AND 
				a.country_iso3 = c.country_iso3 AND 
				a.public = 1 
				ORDER BY amount DESC,product_name_jp LIMIT '.$page_first.','.$page_num;
	


		$result = $this->db_query($sql);
		
		$i =0; 
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i]	   = $rec[product_id];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['short_desc'][$i]      = $rec[short_desc];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]       = $rec[price_max];
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$record['price_adult'][$i] 	   = $rec[price_adult];
			$record['price_child'][$i]     = $rec[price_child];
			$record['price_infant'][$i]    = $rec[price_infant];
			$record['country_iso3'][$i]	   = $rec[country_iso3];
			$record['path'][$i]   		   = $rec[path];
			$record['country_name_jp'][$i] = $rec[country_name_jp];

			$i++;
		}
				
		return $record;	
	}
	/* mybus lp page */


	/* mybus op lp country page */
	public function front_show_product($product_type,$country,$yyyy,$mm,$category="%",$page_first,$page_num)
	{
		
		
		$sql = 'SELECT a.country_iso3,path,country_name_jp,a.product_id,product_name_jp,price_min,price_max, price_adult,price_child ,price_infant, 
				short_desc,amount,rev_status,rev_date,rate,sign FROM '._DB_PREFIX_TABLE.'product a 
				LEFT JOIN (SELECT * FROM '._DB_PREFIX_TABLE.'monthly_product WHERE mpro_year = "'.$yyyy.'" AND mpro_month = "'.$mm.'" ) AS b ON 
				a.product_id = b.product_id ,'._DB_PREFIX_TABLE.'country c ,'._DB_PREFIX_TABLE.'currency e ';
		
		if ($country == "ALL")
		{
			$sql .= 'WHERE ';
		}
		else
		{
			$sql .= 'WHERE a.country_iso3 = "'.$country.'" AND ';
		}
		
		$sql .= 'a.product_type = "'.$product_type.'" AND 
				product_theme LIKE "%'.$category.'%" AND 
				a.country_iso3 = c.country_iso3 AND a.country_iso3 = e.country_iso3 AND 
				a.public = 1 
				ORDER BY amount DESC,product_name_jp LIMIT '.$page_first.','.$page_num;
	


		$result = $this->db_query($sql);
		
		$i =0; 
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i]	   = $rec[product_id];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['short_desc'][$i]      = $rec[short_desc];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]       = $rec[price_max];
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$record['price_adult'][$i] 	   = $rec[price_adult];
			$record['price_child'][$i]     = $rec[price_child];
			$record['price_infant'][$i]    = $rec[price_infant];
			$record['country_iso3'][$i]	   = $rec[country_iso3];
			$record['path'][$i]   		   = $rec[path];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			
			$record['rate'][$i]            = $rec[rate];
			$record['sign'][$i]            = $rec[sign];

			$i++;
		}
				
		return $record;
				
	}
	/* mybus op lp country page */
	
	public function get_theme()
	{
		$sql = 'SELECT 	IF(theme_id<100,IF (theme_id<10,CONCAT(\'00\',theme_id),CONCAT(\'0\',theme_id)),theme_id) theme_id,
				theme_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'theme a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY theme_name ';
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[1][$i]  = $rec['theme_id'];
			$record[0][$i]  = $rec['theme_name'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	
	
	/* category */
	
	public function get_category()
	{
		
		$sql = 'SELECT category_id,category_name FROM '._DB_PREFIX_TABLE.'category a 
				LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY category_name';

		/*$sql = 'SELECT 	IF(category_id<100,IF (category_id<10,CONCAT(\'00\',category_id),CONCAT(\'0\',category_id)),category_id) category_id,category_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'category a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY category_name ';
		*/
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[1][$i]  = $rec['category_id'];
			$record[0][$i]  = $rec['category_name'];
			$i++;
		}
		
		
		
		return $record;
	}
		
	public function fetch_category($page_first,$page_num)
	{
		
		$sql = 'SELECT 	category_id,category_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'category a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY category_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['category_id'];
			$record[1][$i]  = $rec['category_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	public function fetch_theme($page_first,$page_num)
	{
		
		$sql = 'SELECT 	theme_id,theme_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'theme a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY theme_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['theme_id'];
			$record[1][$i]  = $rec['theme_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	public function view_category($page_id)
	{
		$raw = array(  'rec'  => array( 'category_id' , 'category_name' ), 
					   'field' => _DB_PREFIX_TABLE.'category',
					   'compare' => 'category_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
		
	}
	
	public function view_theme($page_id)
	{
		$raw = array(  'rec'  => array( 'theme_id' , 'theme_name' ), 
					   'field' => _DB_PREFIX_TABLE.'theme',
					   'compare' => 'theme_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
		
	}
	
	public function view_theme_all()
	{
		$sql = 'SELECT theme_name,theme_id FROM '._DB_PREFIX_TABLE.'theme ORDER BY theme_id';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['theme_name'];
			$rec['value'][$i] = $record['theme_id'];
			$i++;
		}
		
	
		return $rec;	
	}
	
	public function view_category_all()
	{
		
		$sql = 'SELECT category_name,category_id FROM '._DB_PREFIX_TABLE.'category ORDER BY category_id';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['category_name'];
			$rec['value'][$i] = $record['category_id'];
			$i++;
		}
	
		return $rec;
	}
	/* category */
	
	/* option */
	
	
	
	public function fetch_option($page_first,$page_num)
	{
		$sql = 'SELECT 	ops_id,ops_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'option a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY ops_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['ops_id'];
			$record[1][$i]  = $rec['ops_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		return $record;
	}
	
	public function fetch_option2($page_first,$page_num)
	{
		$sql = 'SELECT 	ops2_id,ops2_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'option2 a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY ops2_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['ops2_id'];
			$record[1][$i]  = $rec['ops2_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		return $record;
	}
	
	
	
	/* option all */
	
	public function view_option_all()
	{
		
		$sql = 'SELECT ops_name,ops_id FROM '._DB_PREFIX_TABLE.'option ORDER BY ops_id';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['ops_name'];
			$rec['value'][$i] = $record['ops_id'];
			$i++;
		}
		
	
		return $rec;
	}
	
	
	public function view_option2_all()
	{
		
		$sql = 'SELECT ops2_name,ops2_id FROM '._DB_PREFIX_TABLE.'option2 ORDER BY ops2_id';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['ops2_name'];
			$rec['value'][$i] = $record['ops2_id'];
			$i++;
		}
		
	
		return $rec;
	}
	
	
	/* option all */
	
	
	
	public function view_option($page_id)
	{
		$raw = array(  'rec'  => array( 'ops_id' , 'ops_name' ), 
					   'field' => _DB_PREFIX_TABLE.'option',
					   'compare' => 'ops_id',
					   'id'    => $page_id );
		
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
	}
	
	public function view_option2($page_id)
	{
		$raw = array(  'rec'  => array( 'ops2_id' , 'ops2_name' ), 
					   'field' => _DB_PREFIX_TABLE.'option2',
					   'compare' => 'ops2_id',
					   'id'    => $page_id );
		
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
	}
	
	
	/* option */
	
	/* day */
	public function fetch_day($page_first,$page_num)
	{
		$sql = 'SELECT day_id,day_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'day a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY day_id,day_order,day_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['day_id'];
			$record[1][$i]  = $rec['day_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	
	public function viw_day_all()
	{
		
		$sql = 'SELECT day_name,day_id FROM '._DB_PREFIX_TABLE.'day ORDER BY day_id,day_name';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['day_name'];
			$rec['value'][$i] = $record['day_id'];
			$i++;
		}
		
	
		return $rec;
	}
	
	
	
	public function view_day($page_id)
	{
		$raw = array(  'rec'  => array( 'day_id' , 'day_name' ), 
					   'field' => _DB_PREFIX_TABLE.'day',
					   'compare' => 'day_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
	}
	/* day */
	
	/* time */
	public function fetch_time($page_first,$page_num)
	{
		
		$sql = 'SELECT 	time_id,time_name,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'time a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY time_order,time_name LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['time_id'];
			$record[1][$i]  = $rec['time_name'];
			$record[2][$i]  = $rec['update_date'];
			$record[3][$i]  = $rec['update_by'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	
	public function viw_time_all()
	{
		
		$sql = 'SELECT time_name,time_id FROM '._DB_PREFIX_TABLE.'time ORDER BY time_id,time_order';

		$result = $this->db_query($sql);  $i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record['time_name'];
			$rec['value'][$i] = $record['time_id'];
			$i++;
		}
		
	
		return $rec;
	}
	
	
	
	public function view_time($page_id)
	{
		$raw = array(  'rec'  => array( 'time_id' , 'time_name' ), 
					   'field' => _DB_PREFIX_TABLE.'time',
					   'compare' => 'time_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][1]];
		}
		return $rec;
		
	}
	
	
	
	public function get_time()
	{
		$raw = array(  'rec'  => array( 'time_id' , 'time_name' ), 
					   'field' => _DB_PREFIX_TABLE.'time');
		
		
		$result = $this->fetch_select($raw);
		$i = 0;
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record[$raw[rec][0]];
			$rec[1][$i] = $record[$raw[rec][1]];	
			
			$i++;
		}
		return $rec;	
	}
	
	
	/* time */
	/* country */
	public function get_country()
	{
		$raw = array(  'rec'  => array('country_id', 'country_iso3' ,  'country_name_jp' ,'country_name_en' ), 
					   'field' => _DB_PREFIX_TABLE.'country');
		
		
		$result = $this->fetch_select($raw);
		$i = 0;
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record[$raw[rec][1]];
			$rec[1][$i] = $record[$raw[rec][2]];	
			$rec[2][$i] = $record[$raw[rec][3]];	
			
			$i++;
		}
		return $rec;	
	}
	
	public function showpath_bycountry($country_iso)
	{
		$sql = 'SELECT path FROM '._DB_PREFIX_TABLE.'country WHERE country_iso3 = "'.$country_iso.'" LIMIT 0,1';	
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$path = $record[path];
			
		}
		
		return $path ;
	}
	
	public function fetch_country($page_first,$page_num)
	{
		
		$sql = 'SELECT 	a.country_iso3,country_name_en,country_name_jp,country_id,a.update_date,admin_name update_by, child_age 
				FROM '._DB_PREFIX_TABLE.'country a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY country_iso3 LIMIT '.$page_first.','.$page_num;
		
	
		$result = $this->db_query($sql);
		
		
		$i =0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[0][$i]  = $rec['country_id'];
			$record[1][$i]  = $rec['country_iso3'];
			$record[2][$i]  = $rec['country_name_en'];
			$record[3][$i]  = $rec['country_name_jp'];
			$record[4][$i]  = $rec['update_date'];
			$record[5][$i]  = $rec['update_by'];
			$record[6][$i]  = $rec['child_age'];
			$i++;
		}
		
		
		
		return $record;
	}
	
	public function view_country($page_id)
	{
		$raw = array(  'rec'  => array( 'jtb_country_id' , 'country_iso2', 'country_iso3' , 'country_name_en', 'country_name_jp' ,'path', 'child_age','infant_age' ), 
					   'field' => _DB_PREFIX_TABLE.'country',
					   'compare' => 'country_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][0]];
			$rec[1] = $record[$raw[rec][1]];	
			$rec[2] = $record[$raw[rec][2]];	
			$rec[3] = $record[$raw[rec][3]];
			$rec[4] = $record[$raw[rec][4]];
			$rec[5] = $record[$raw[rec][5]];
			$rec[6] = $record[$raw[rec][6]];
			$rec[7] = $record[$raw[rec][7]];
		}
		return $rec;
		
	}
	/* country */
	
	/* city */
	
	public function fetch_city($page_first,$page_num)
	{
		$sql = 'SELECT 	a.city_id,city_iso3,city_name_en,city_name_jp,a.country_iso3,c.country_name_en,a.update_date,admin_name update_by,other_status 
				FROM '._DB_PREFIX_TABLE.'city a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id LEFT JOIN '._DB_PREFIX_TABLE.'country c ON a.country_iso3 = c.country_iso3 
				ORDER BY country_iso3,city_iso3 LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record['city_id'];
			$rec[1][$i] = $record['country_name_en'];
			$rec[2][$i] = $record['city_iso3'];
			$rec[3][$i] = $record['city_name_en'];
			$rec[4][$i] = $record['city_name_jp'];
			$rec[5][$i] = $record['update_date'];
			$rec[6][$i] = $record['update_by'];
			$rec[7][$i] = $record['other_status'];
			$i++;
			
		}
		return $rec;
	}
	
	
	public function select_city($country_iso3)
	{
		$raw = array(  'rec'  => array( 'city_id','country_iso3','city_name_en', 'city_name_jp','city_iso3'  ), 
					   'field' => _DB_PREFIX_TABLE.'city',
					   'compare' => 'country_iso3',
					   'id'    => $country_iso3 );
		
		
		$result = $this->view_select($raw);
		
		$rec = array();
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][]  = $record[$raw[rec][3]];
			$rec['value'][] = $record[$raw[rec][4]];
		}
		
		
		
		return $rec;
		
		
		
	}
	
	
	public function view_city($page_id)
	{
		$raw = array(  'rec'  => array( 'city_id','country_iso3','city_name_en', 'city_name_jp','city_iso3' , 'other_status' ), 
					   'field' => _DB_PREFIX_TABLE.'city',
					   'compare' => 'city_id',
					   'id'    => $page_id );
		
		
		$result = $this->view_select($raw);
		
		$rec = array();
		while ($record = mysql_fetch_array($result))
		{
			$rec[0] = $record[$raw[rec][0]];
			$rec[1] = $record[$raw[rec][1]];
			$rec[2] = $record[$raw[rec][2]];
			$rec[3] = $record[$raw[rec][3]];
			$rec[4] = $record[$raw[rec][4]];
			$rec[5] = $record[$raw[rec][5]];

		}
		return $rec;	
	}
	/* city */
	
	
	
	/* product */
	public function view_review($product_id)
	{
		$sql  = 'SELECT product_id,revimg_id,revimg_text FROM '._DB_PREFIX_TABLE.'product_review ';
		$sql .= 'WHERE product_id = "'.$product_id.'" ORDER BY product_id,revimg_id';
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$i  			  = $record['revimg_id'];
			$revimg_text[$i]  = $record['revimg_text'];
		}
		
		return $revimg_text;
	}
	
	public function count_product($filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_category LIKE "%'.$theme_val.'%" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}
		
		
		$sql = 'SELECT COUNT(*) dd  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 '.$where;
				
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
		
	}
	
	public function fatch_product($page_first,$page_num,$filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_category LIKE "%'.$theme_val.'%" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}

		
		$sql = 'SELECT product_id,
				b.country_iso3,a.city_iso3,country_name_en,country_name_jp,city_name_en,city_name_jp,
				product_code,product_name_en,product_name_jp,
				price_min,price_max,
				a.update_date,
				rev_status,rev_date  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 ';
		
		$sql .= $where;
				
		$sql .=	'ORDER BY product_code DESC,country_iso3,city_iso3,product_name_en,product_name_jp 
				LIMIT '.$page_first.','.$page_num;
				

		$result = $this->db_query($sql);
		$i = 0;
		
		
		
		
		
		while ($rec = mysql_fetch_array($result))
		{

			$record['id'][$i] 			   = $rec[product_id];
			$record['country_iso3'][$i]    = $rec[country_iso3];
			$record['city_iso3'][$i] 	   = $rec[city_iso3];
			$record['country_name_en'][$i] = $rec[country_name_en];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			$record['city_name_en'][$i]    = $rec[city_name_en];
			$record['city_name_jp'][$i]    = $rec[city_name_jp];
			$record['product_code'][$i]    = $rec[product_code];
			$record['product_name_en'][$i] = $rec[product_name_en];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]	   = $rec[price_max];	
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$i++;
		}
		
		return $record;	
	}
	
	public function view_product_policy($product_id)
	{
		$sql = 'SELECT a.product_id,a.product_code,default_policy,
				b.policy_id,b.policy_title_en,b.policy_title_jp,
				b.policy_title_short_en,b.policy_title_short_jp,b.public,
				b.seq seq1,b.policy_content_en,b.policy_content_jp
				,c.seq seq2,description_en,description_jp FROM 
				'._DB_PREFIX_TABLE.'product_policy a LEFT JOIN 
				(SELECT a.policy_id,policy_title_en,policy_title_jp,
				policy_title_short_en,policy_title_short_jp,public,
				seq,policy_content_en,policy_content_jp 
				FROM 
				'._DB_PREFIX_TABLE.'policy a LEFT JOIN 
				'._DB_PREFIX_TABLE.'policy_detail b ON a.policy_id = b.policy_id ) b 
				ON a.policy_id = b.policy_id
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'product_policy_map c 
				ON a.product_id = c.product_id  AND 
				a.policy_id = a.policy_id AND
				b.policy_id = c.policy_id 
				WHERE a.product_id = "'.$product_id.'" AND 
				b.public = "1"
				ORDER BY product_id,policy_id,seq2,seq1 ';	
				
		$result = $this->db_query($sql);	$i=0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i] 			 = $rec['product_id'];
			$record['product_code'][$i] 		 = $rec['product_code'];
			$record['default_policy'][$i] 		 = $rec['default_policy'];
			$record['policy_id'][$i] 			 = $rec['policy_id'];
			$record['policy_title_en'][$i] 		 = $rec['policy_title_en'];
			$record['policy_title_jp'][$i] 		 = $rec['policy_title_jp'];
			$record['policy_title_short_en'][$i] = $rec['policy_title_short_en'];
			$record['policy_title_short_jp'][$i] = $rec['policy_title_short_jp'];
			$record['seq1'][$i] 				 = $rec['seq1'];
			$record['policy_content_en'][$i] 	 = $rec['policy_content_en'];
			$record['policy_content_jp'][$i] 	 = $rec['policy_content_jp'];
			$record['seq2'][$i] 				 = $rec['seq2'];
			$record['description_en'][$i] 		 = $rec['description_en'];
			$record['description_jp'][$i] 		 = $rec['description_jp'];
			$i++;
		}

		
		return $record;
	}
	
	public function view_product_itiner($product_id)
	{
		$sql = 'SELECT a.product_id,seq,time_en,time_jp,in_day,itiner_name_jp,itiner_name_en  
				FROM '._DB_PREFIX_TABLE.'product_itinerary a 
				WHERE active = 1 AND product_id ="'.$product_id.'" ';
				
		$result = $this->db_query($sql); $i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i] 	  = $rec['product_id'];
			$record['seq'][$i]		  	  = $rec['seq'];
			$record['time_en'][$i]	  	  = $rec['time_en'];
			$record['time_jp'][$i]    	  = $rec['time_jp'];
			$record['in_day'][$i]	  	  = $rec['in_day'];
			$record['itiner_name_jp'][$i] = $rec['itiner_name_jp'];
			$record['itiner_name_en'][$i] = $rec['itiner_name_en'];
			
			$i++;
		}
		
		return $record;
	}
	
	public function view_product($product_id)
	{
		$sql = 'SELECT a.product_id,path,
				c.country_iso3,product_type,country_name_en,country_name_jp,
				a.city_iso3,city_name_en,city_name_jp,
				product_code,product_name_en,product_name_jp,
				product_theme,product_category,option_id1,option_id2,		
				status_time,status_day,
				status_book,
				price_min,price_max,price_adult,price_child,price_infant,
				description_en,short_desc,description,template_id,keyword_search,
				text_top,text_table,text_bottom,remark ,
				pax_min,pax_max,day_quantity,day_cutoff, 
				rev_status,rev_tittle,rev_desc,
				rate,sign,code , public ,public_start,public_end  
				FROM '._DB_PREFIX_TABLE.'product a 
				LEFT JOIN '._DB_PREFIX_TABLE.'product_detail b
				ON a.product_id = b.product_id
				LEFT JOIN '._DB_PREFIX_TABLE.'city c 
				ON a.city_iso3 = c.city_iso3
				LEFT JOIN '._DB_PREFIX_TABLE.'country d
				ON a.country_iso3 = d.country_iso3 
				LEFT JOIN mbus_currency e 
				ON a.country_iso3 = e.country_iso3
				WHERE a.product_id = "'.$product_id.'" ';
					   
					   

		$result = $this->db_query($sql);

		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'] 		= $rec['product_id'];
			$record['country_iso3'] 	= $rec['country_iso3'];
			$record['product_type']	= $rec['product_type'];
			$record['country_name_en']  = $rec['country_name_en'];
			$record['country_name_jp']  = $rec['country_name_jp'];
			$record['country_path']		= $rec['path'];
			$record['city_iso3'] 		= $rec['city_iso3'];
			$record['city_name_en']		= $rec['city_name_en'];
			$record['city_name_jp']     = $rec['city_name_jp'];
			$record['product_code'] 	= $rec['product_code'];
			$record['product_name_en'] 	= $rec['product_name_en'];
			$record['product_name_jp'] 	= $rec['product_name_jp'];
			$record['product_theme']	= $rec['product_theme'];
			$record['option_id1']		= $rec['option_id1'];
			$record['option_id2']		= $rec['option_id2'];
			$record['product_category'] = $rec['product_category'];
			$record['status_time'] 		= $rec['status_time'];
			$record['status_day']		= $rec['status_day'];
			$record['status_book'] 		= $rec['status_book'];
			$record['price_min'] 		= $rec['price_min'];
			$record['price_max'] 		= $rec['price_max'];
			$record['price_adult'] 		= $rec['price_adult'];
			$record['price_child']		= $rec['price_child'];
			$record['price_infant']		= $rec['price_infant'];
			$record['short_desc'] 		= $rec['short_desc'];
			$record['description']		= $rec['description'];
			$record['template_id'] 		= $rec['template_id'];
			$record['keyword_search'] 	= $rec['keyword_search'];
			$record['text_top'] 		= $rec['text_top'];
			$record['text_table'] 		= $rec['text_table'];
			$record['text_bottom'] 		= $rec['text_bottom'];
			$record['remark']			= $rec['remark'];
			$record['pax_min']			= $rec['pax_min'];
			$record['pax_max']			= $rec['pax_max'];
			$record['day_quantity']     = $rec['day_quantity'];
			$record['day_cutoff']		= $rec['day_cutoff'];
			$record['description_en']   = $rec['description_en'];
			$record['rev_tittle']		= $rec['rev_tittle'];
			$record['rev_desc']			= $rec['rev_desc'];
			$record['rev_status']		= $rec['rev_status'];
			$record['rate']				= $rec['rate'];
			$record['code']				= $rec['code'];
			$record['sign']				= $rec['sign'];
			$record['public']			= $rec['public'];
			$record['public_start']		= $rec['public_start'];
			$record['public_end ']		= $rec['public_end '];
			
		}
		
		return $record;
		
	}
	
	public function view_product_opt($country_iso3)
	{
		
		$sql  = 'SELECT product_id,a.city_iso3,product_code,product_name_en,product_name_jp FROM '._DB_PREFIX_TABLE.'product a,'._DB_PREFIX_TABLE.'city b ';
		$sql .= 'WHERE UPPER(b.country_iso3) = UPPER("'.$country_iso3.'") AND a.city_iso3 = b.city_iso3 AND a.product_type ="OPT" AND public = "1" 
				 ORDER BY product_code ';
		
		$result = $this->db_query($sql);
		
		$rec = array(); $i=0;
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record[product_code].' : '.$record[product_name_jp];
			$rec['value'][$i] = $record[product_id];
			$i++;
		}
		
		return $rec;		
	}	

	public function view_product_pkg($country_iso3)
	{
		
		$sql  = 'SELECT product_id,a.city_iso3,product_code,product_name_en,product_name_jp FROM '._DB_PREFIX_TABLE.'product a,'._DB_PREFIX_TABLE.'city b ';
		$sql .= 'WHERE UPPER(b.country_iso3) = UPPER("'.$country_iso3.'") AND a.city_iso3 = b.city_iso3 AND a.product_type ="PKG" AND public = "1" 
				 ORDER BY product_code ';

		$result = $this->db_query($sql);
		
		$rec = array(); $i=0;
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record[product_code].' : '.$record[product_name_jp];
			$rec['value'][$i] = $record[product_id];
			$i++;
		}
		
		return $rec;		
	}
	
	public function select_product_top($type="")
	{
		$sql  = 'SELECT a.country_iso3,b.country_name_jp,product_id,a.city_iso3,product_code,product_name_en,product_name_jp 
				 FROM '._DB_PREFIX_TABLE.'product a,'._DB_PREFIX_TABLE.'country b ';
		$sql .= 'WHERE a.country_iso3 = b.country_iso3  AND a.public = 1  ';
		
		if (!empty($type))
		{
			$sql .= 'AND product_type = "'.$type.'" ';
		}
		
		$sql .= 'ORDER BY a.country_iso3,product_code ';

		
		$result = $this->db_query($sql);
		
		
		$rec = array(); $i=0;
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = '['.$record[country_name_jp].'] '. $record[product_code].' : '.$record[product_name_jp];
			$rec['value'][$i] = $record[product_id];
			$i++;
		}
		
		return $rec;	
	}
	
	
	public function select_product($country_iso3,$type="")
	{
		
		$sql  = 'SELECT product_id,a.city_iso3,product_code,product_name_en,product_name_jp FROM '._DB_PREFIX_TABLE.'product a,'._DB_PREFIX_TABLE.'city b ';
		$sql .= 'WHERE UPPER(b.country_iso3) = UPPER("'.$country_iso3.'") AND a.city_iso3 = b.city_iso3 AND a.country_iso3 = b.country_iso3 AND a.public = 1 ';
		
		if (!empty($type))
		{
			$sql .= 'AND product_type = "'.$type.'" ';
		}
		
		
		
		$result = $this->db_query($sql);
		
		
		$rec = array(); $i=0;
		while ($record = mysql_fetch_array($result))
		{
			$rec['data'][$i]  = $record[product_code].' : '.$record[product_name_jp];
			$rec['value'][$i] = $record[product_id];
			$i++;
		}
		
		return $rec;		
	}	
	
	/* product */
	
	/* product pkg */
	public function count_product_pkg($filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				//$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_category = "'.$filter[theme].'" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
			$optarr[] = '( a.product_type = "PKG" ) ';
			
			
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}
		
		
		$sql = 'SELECT COUNT(*) dd  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3  AND b.country_iso3 = a.country_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 '.$where;
				
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
		
	}
	
	
	public function fatch_product_pkg($page_first,$page_num,$filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				//$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_theme = "'.$filter[theme].'" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
			$optarr[] = '( a.product_type = "PKG" ) ';
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}

		
		$sql = 'SELECT product_id,
				b.country_iso3,a.city_iso3,country_name_en,country_name_jp,city_name_en,city_name_jp,
				product_code,product_name_en,product_name_jp,
				price_min,price_max,
				a.update_date,
				rev_status,rev_date  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3  AND b.country_iso3 = a.country_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 ';
		
		$sql .= $where;
				
		$sql .=	'ORDER BY product_code DESC,country_iso3,city_iso3,product_name_en,product_name_jp 
				LIMIT '.$page_first.','.$page_num;

		
		$result = $this->db_query($sql);
		$i = 0;
		
		
		
		while ($rec = mysql_fetch_array($result))
		{

			$record['id'][$i] 			   = $rec[product_id];
			$record['country_iso3'][$i]    = $rec[country_iso3];
			$record['city_iso3'][$i] 	   = $rec[city_iso3];
			$record['country_name_en'][$i] = $rec[country_name_en];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			$record['city_name_en'][$i]    = $rec[city_name_en];
			$record['city_name_jp'][$i]    = $rec[city_name_jp];
			$record['product_code'][$i]    = $rec[product_code];
			$record['product_name_en'][$i] = $rec[product_name_en];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]	   = $rec[price_max];	
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$i++;
		}
		
		return $record;	
	}

	/* product pkg */	
	
	/* product opt */
	public function count_product_opt($filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			if (!empty($filter[fpublic]))
			{
				if ($filter[fpublic] == "on")
				{
					$optarr[] = 'a.public = "1" ';	
				}
				
				if ($filter[fpublic] == "off")
				{
					$optarr[] = 'a.public = "0" ';	
				}
			}
			
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_theme LIKE "%'.$theme_val.'%" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
			$optarr[] = '( a.product_type = "OPT" ) ';
			
			
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}
		
		
		$sql = 'SELECT COUNT(*) dd  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3  AND b.country_iso3 = a.country_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 '.$where;
				
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
		
	}
	
	
	public function fatch_product_opt($page_first,$page_num,$filter="")
	{
		$optarr = array();

		if (is_array($filter))
		{
			if (!empty($filter[fpublic]))
			{
				if ($filter[fpublic] == "on")
				{
					$optarr[] = 'a.public = "1" ';	
				}
				
				if ($filter[fpublic] == "off")
				{
					$optarr[] = 'a.public = "0" ';	
				}
			}
			
			if (!empty($filter[country]))
			{
				$optarr[] = 'a.country_iso3 = "'.$filter[country].'" ';	
			}
			
			if (!empty($filter[city]))
			{
				$optarr[] = 'b.city_iso3 = "'.$filter[city].'" ';
			}
			
			if (!empty($filter[theme]))
			{
				$theme_val = str_pad($filter[theme], 3, "0", STR_PAD_LEFT) ;				
				
				$optarr[] = '( a.product_theme LIKE "%'.$theme_val.'%" ) ';
			}
			
			if (!empty($filter[code]))
			{
							
				
				$optarr[] = '( a.product_code LIKE "%'.trim($filter[code]).'%" ) ';
			}
			
			
			if (!empty($filter[name_en]))
			{
				$optarr[] = '( a.product_name_en LIKE "%'.trim($filter[name_en]).'%" ) ';	
			}
			
			if (!empty($filter[name_jp]))
			{
				$optarr[] = '( a.product_name_jp LIKE "%'.trim($filter[name_jp]).'%" ) ';	
			}	
			
			$optarr[] = '( a.product_type = "OPT" ) ';
			
		}
		
		if (count($optarr) > 0)
		{
			$where = ' WHERE ';
			
			for ($i = 0; $i< count($optarr) ;$i++)
			{
				$where .= $optarr[$i];
				
				if ($i != (count($optarr)-1) ) 
				{
					$where .= ' AND ';
				}
			}
		}

		
		$sql = 'SELECT product_id,
				b.country_iso3,a.city_iso3,country_name_en,country_name_jp,city_name_en,city_name_jp,
				product_code,product_name_en,product_name_jp,
				price_min,price_max,
				a.update_date,
				rev_status,rev_date,a.public data_public    
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3  AND b.country_iso3 = a.country_iso3 
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 ';
		
		$sql .= $where;
				
		$sql .=	'ORDER BY product_code DESC,country_iso3,city_iso3,product_name_en,product_name_jp 
				LIMIT '.$page_first.','.$page_num;
				
				


		$result = $this->db_query($sql);
		$i = 0;
		
		
		
		while ($rec = mysql_fetch_array($result))
		{

			$record['id'][$i] 			   = $rec[product_id];
			$record['country_iso3'][$i]    = $rec[country_iso3];
			$record['city_iso3'][$i] 	   = $rec[city_iso3];
			$record['country_name_en'][$i] = $rec[country_name_en];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			$record['city_name_en'][$i]    = $rec[city_name_en];
			$record['city_name_jp'][$i]    = $rec[city_name_jp];
			$record['product_code'][$i]    = $rec[product_code];
			$record['product_name_en'][$i] = $rec[product_name_en];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]	   = $rec[price_max];	
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$record['public'][$i]		   = $rec[data_public];
			$i++;
		}
		
		return $record;	
	}
	/* product opt */
	
	/* temph */
	public function fatch_temph($page_first,$page_num)
	{
		$sql = 'SELECT temph_id id,temph_name name,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template_header a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY id,update_date LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql);
	
		$i = 0;
		
		
		while ($rec = mysql_fetch_array($result))
		{
			$id 	 		 = $rec[id];
			$name   		 = $rec[name];
			$update_date 	 = $rec[update_date];
			$update_by       = $rec[admin_name];
			
			$record[0][$i] = $id;
			$record[1][$i] = $name;
			$record[2][$i] = $update_date;
			$record[3][$i] = $update_by;
			$i++;
		}
		
		return $record;
	}
	
	public function view_temph($page_id)
	{
		$sql = 'SELECT temph_id id,temph_name name,temph_detail detail,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template_header a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				WHERE temph_id = "'.$page_id.'"';
			

			
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$name = $rec[name];
			$detail = $rec[detail];
			
			$record[0] = $name;
			$record[1] = $detail;
			
			$i++;
		}
		
		return $record;	
	}
	/* temph */
	
	/* tempf */
	public function fatch_tempf($page_first,$page_num)
	{
		$sql = 'SELECT tempf_id id,tempf_name name,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template_footer a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY id,update_date LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql);
	
		$i = 0;
		
		
		while ($rec = mysql_fetch_array($result))
		{
			$id 	 		 = $rec[id];
			$name   		 = $rec[name];
			$update_date 	 = $rec[update_date];
			$update_by       = $rec[admin_name];
			
			$record[0][$i] = $id;
			$record[1][$i] = $name;
			$record[2][$i] = $update_date;
			$record[3][$i] = $update_by;
			$i++;
		}
		
		return $record;
	}
	
	public function view_tempf($page_id)
	{
		$sql = 'SELECT tempf_id id,tempf_name name,tempf_detail detail,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template_footer a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				WHERE tempf_id = "'.$page_id.'"';
			

			
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$name = $rec[name];
			$detail = $rec[detail];
			
			$record[0] = $name;
			$record[1] = $detail;
			
			$i++;
		}
		
		return $record;	
	}
	/* tempf */
	
	
	/* cms */
	public function view_cms($page_id)
	{
		$sql = 'SELECT cms_id,cms_name,cms_detail,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'cms a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				WHERE cms_id = "'.$page_id.'"';
			
			
			
			
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$cms_name 	= $rec[cms_name];
			$cms_detail = $rec[cms_detail];
			
			$record[0] 	= $cms_name;
			$record[1] 	= $cms_detail;
			
			$i++;
		}
		
		return $record;	
	}
	
	public function fatch_cms($page_first,$page_num)
	{
			
		
		$sql = 'SELECT cms_id,cms_name,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'cms a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY cms_id,update_date LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql);

		$i = 0;
		
		
		while ($rec = mysql_fetch_array($result))
		{
			$cms_id 	 = $rec[cms_id];
			$cms_name   = $rec[cms_name];
			$update_date 	 = $rec[update_date];
			$update_by       = $rec[admin_name];
			
			$record[0][$i] = $cms_id;
			$record[1][$i] = $cms_name;
			$record[2][$i] = $update_date;
			$record[3][$i] = $update_by;
			$i++;
		}
		
		return $record;
	
	}
	/* cms */
	
	/* template */

	public function template_count()
	{
		$sql = 'SELECT COUNT(*) dd FROM '._DB_PREFIX_TABLE.'template '	;
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
	}
	
	
	public function view_template($page_id)
	{
		$sql = 'SELECT template_id,template_name,template_detail,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id 
				WHERE template_id = "'.$page_id.'"';
			
			
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$template_name = $rec[template_name];
			$template_detail = $rec[template_detail];
			
			$record[0] = $template_name;
			$record[1] = $template_detail;
			
			$i++;
		}
		
		return $record;	
	}
	
	public function fatch_template($page_first,$page_num)
	{
		$sql = 'SELECT template_id,template_name,a.update_date,a.update_by,admin_name 
				FROM '._DB_PREFIX_TABLE.'template a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id ORDER BY template_id,update_date LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql);

		$i = 0;
		
		
		while ($rec = mysql_fetch_array($result))
		{
			$template_id 	 = $rec[template_id];
			$template_name   = $rec[template_name];
			$update_date 	 = $rec[update_date];
			$update_by       = $rec[admin_name];
			
			$record[0][$i] = $template_id;
			$record[1][$i] = $template_name;
			$record[2][$i] = $update_date;
			$record[3][$i] = $update_by;
			$i++;
		}
		
		return $record;
		
		
	}
	
	/*  template  */
	
	/* itinerary */
	public function fetch_itinerary($page_first,$page_num)
	{
		$sql = 'SELECT 	a.itiner_id,itiner_name_en,itiner_name_jp,a.update_date,admin_name update_by
				FROM '._DB_PREFIX_TABLE.'itinerary_obj a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON 
				a.update_by = b.admin_id  
				ORDER BY itiner_name_en,itiner_name_jp LIMIT '.$page_first.','.$page_num;

		$result = $this->db_query($sql);
		$i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['itiner_id'][$i] 		= $rec['itiner_id'];
			$record['itiner_name_en'][$i] 	= $rec['itiner_name_en'];
			$record['itiner_name_jp'][$i] 	= $rec['itiner_name_jp'];
			$record['update_date'][$i] 		= $rec['update_date'];
			$record['update_by'][$i] 		= $rec['update_by'];
			
			$i++;
		}
		
		return $record;
	}
	
	public function view_itinerary($page_id)
	{
		$sql  = 'SELECT itiner_id,itiner_name_en,itiner_name_jp FROM mbus_itinerary_obj ';
		$sql .= 'WHERE itiner_id = "'.$page_id.'" ';

		
		$result = $this->db_query($sql); 
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['itiner_name_en'] = $rec['itiner_name_en'];
			$record['itiner_name_jp'] = $rec['itiner_name_jp'];
		}
		
		return $record;
		
		
	}
	
	/* itinerary */
	
	
	/* promotion */
	public function show_promotion($select_country,$select_type)
	{
		$sql = 'SELECT promo_list FROM '._DB_PREFIX_TABLE.'promo_photosnap WHERE country_iso3 = "'.$select_country.'" AND set_page = "'.$select_type.'" ';
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$arr_record = $record[promo_list];	
		}
		
		return $arr_record ;
			
	}
	
	
	public function fetch_promotion_country($country,$type)
	{
		if ($country == "ALL")
		{
			$sql  = 'SELECT promo_id,a.country_iso3,promo_tittle,path FROM '._DB_PREFIX_TABLE.'promotion a LEFT JOIN '._DB_PREFIX_TABLE.'country b 
					 ON a.country_iso3 = b.country_iso3 WHERE 
					 public = 1 ';	
			$sql .= 'AND product_type ="'.$type.'" '; 
			$sql .= 'ORDER BY a.update_date DESC LIMIT 0,6';
		}
		else
		{		
			$sql  = 'SELECT promo_id,a.country_iso3,promo_tittle,path FROM '._DB_PREFIX_TABLE.'promotion a LEFT JOIN '._DB_PREFIX_TABLE.'country b 
					 ON a.country_iso3 = b.country_iso3 WHERE a.country_iso3 = "'.$country.'" AND 
					 public = 1 ';
			$sql .= 'AND product_type ="'.$type.'" '; 		
			$sql .=	'ORDER BY a.update_date DESC';
		}
		
			
		
	
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record[promo_id][$i] 	  = $rec[promo_id];
			$record[promo_tittle][$i] = $rec[promo_tittle];
			$record[path][$i]         = $rec[path];
			$i++;
		}
	
		
		return $record;		
	}
	
	public function view_promotion_country($product_id)
	{
		$sql = 'SELECT promo_id,a.country_iso3,path,product_type,promo_tittle,promo_detail,product_list FROM '.
			   _DB_PREFIX_TABLE.'promotion a LEFT JOIN '._DB_PREFIX_TABLE.'country b ON a.country_iso3 = b.country_iso3 
			   WHERE promo_id = "'.trim($product_id).'" AND public = "1" ';
			   
		$result = $this->db_query($sql); 
		
		while ($rec = mysql_fetch_array($result))
		{
			$result = $rec;	
		}
		
	
		return $result;
	}
	
	public function view_promotion($product_id)
	{
		$sql = 'SELECT promo_id,country_iso3,product_type,promo_tittle,promo_detail,product_list,public product_public  
				FROM '._DB_PREFIX_TABLE.'promotion 
				WHERE promo_id = "'.trim($product_id).'"  ';
		
	
		$result = $this->db_query($sql); $i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$result = $rec;	
		}
		
		return $result;
	}
	
	public function fetch_promotion($page_first,$page_num)
	{
		$sql = 'SELECT promo_id ,a.country_iso3,country_name_jp ,product_type ,promo_tittle ,promo_detail ,product_list ,public product_public,a.update_date,admin_name  
		        FROM '._DB_PREFIX_TABLE.'promotion a 
				LEFT JOIN '._DB_PREFIX_TABLE.'administrators b ON a.update_by = b.admin_id 
				LEFT JOIN '._DB_PREFIX_TABLE.'country c ON a.country_iso3 = c.country_iso3 
				ORDER BY a.country_iso3,product_type,a.update_date DESC LIMIT '.$page_first.','.$page_num;
		
		$result = $this->db_query($sql); $i = 0;
		
		while ($rec = mysql_fetch_array($result))
		{
			$record[promo_id][$i]     = $rec[promo_id];		
			$record[country_iso3][$i] = $rec[country_iso3];
			
			$record[country_name][$i] = $rec[country_name_jp];
			$record[product_type][$i] = $rec[product_type];
			$record[promo_tittle][$i] = $rec[promo_tittle];
			$record[promo_detail][$i] = $rec[promo_detail];
			$record[product_list][$i] = $rec[product_list];
			$record[product_public][$i]  = $rec[product_public];
			$record[update_date][$i]  = $rec[update_date];
			$record[update_by][$i]    = $rec[admin_name];
			
			$i++;
		}
		
		return $record ;
	}
	/* promotion */
	
	/* banner opt */
	
	public function fetch_banner_opt($page_first,$page_num)
	{
		$sql =  'SELECT opt_id,public_id,a.country_iso3,public_id,opt_name,a.update_by,a.update_date,admin_name 
				FROM '._DB_PREFIX_TABLE.'opt a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id ORDER BY a.country_iso3,public_id,opt_name,a.update_date LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0][$i] = $rec[opt_id];
			$record[1][$i] = $rec[country_iso3];
			$record[2][$i] = $rec[public_id];
			
			$record[3][$i] = $rec[opt_name];
			
			$record[4][$i] = $rec[update_date];
			$record[5][$i] = $rec[admin_name];
			
			$i++;
	
		}
		
		return $record;
	}
	
	public function fetch_banner_opt_country($country,$page_first,$page_num)
	{
		$sql =  'SELECT opt_id,public_id,a.country_iso3,public_id,product_id,opt_name,a.update_by,a.update_date,admin_name 
				FROM '._DB_PREFIX_TABLE.'opt a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id AND a.country_iso3 = "'.$country.'" ORDER BY a.country_iso3,public_id,opt_name,a.update_date LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record['banner_id'][$i] 	= $rec[opt_id];
			
			$record['banner_name'][$i]  = $rec[opt_name];
			$record['product_id'][$i]   = $rec[product_id];
			
			$i++;
		}
		
		return $record;
		
	}										  
	
	public function view_banner_opt($page_id)
	{
		$sql =  'SELECT opt_id,public_id,a.country_iso3,public_id,opt_name,url_link,a.update_by,a.update_date,admin_name,a.product_id 
				FROM '._DB_PREFIX_TABLE.'opt a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id WHERE opt_id ="'.$page_id.'" ';
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0] = $rec[opt_id];
			$record[1] = $rec[country_iso3];
			$record[2] = $rec[public_id];
			
			$record[3] = $rec[opt_name];
			$record[4] = $rec[url_link];
			
			$record[5] = $rec[update_date];
			$record[6] = $rec[admin_name];
			$record[7] = $rec[product_id];
			
			$i++;
	
		}
		
		return $record;
	}
	
	/* banner opt */
	/* banner lp */
	
	public function fetch_banner_lp_country($country,$page_first,$page_num)
	{
		$sql =  'SELECT lp_id,public_id,a.country_iso3,public_id,product_id,lp_name,a.update_by,a.update_date,admin_name 
				FROM '._DB_PREFIX_TABLE.'lp a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id AND a.country_iso3 = "'.$country.'" ORDER BY 
				a.country_iso3,public_id,lp_name,a.update_date LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record['banner_id'][$i] 	= $rec[lp_id];
			
			$record['banner_name'][$i]  = $rec[lp_name];
			$record['product_id'][$i]   = $rec[product_id];
			
			$i++;
		}
		
		return $record;
		
	}	
	
	
	
	
	public function fetch_banner_lp($page_first,$page_num)
	{
		$sql =  'SELECT lp_id,public_id,a.country_iso3,public_id,lp_name,a.update_by,a.update_date,admin_name 
				FROM '._DB_PREFIX_TABLE.'lp a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id ORDER BY a.country_iso3,public_id,lp_name,a.update_date LIMIT '.$page_first.','.$page_num;
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0][$i] = $rec[lp_id];
			$record[1][$i] = $rec[country_iso3];
			$record[2][$i] = $rec[public_id];
			
			$record[3][$i] = $rec[lp_name];
			
			$record[4][$i] = $rec[update_date];
			$record[5][$i] = $rec[admin_name];
			
			$i++;
	
		}
		
		return $record;
	}
	
	public function view_banner_lp($page_id)
	{
		$sql =  'SELECT lp_id,public_id,a.country_iso3,public_id,lp_name,url_link,a.update_by,a.update_date,admin_name,a.product_id 
				FROM '._DB_PREFIX_TABLE.'lp a LEFT JOIN '._DB_PREFIX_TABLE.'administrators b 
		        ON a.update_by = b.admin_id WHERE lp_id ="'.$page_id.'" ';
		
		
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0] = $rec[lp_id];
			$record[1] = $rec[country_iso3];
			$record[2] = $rec[public_id];
			
			$record[3] = $rec[lp_name];
			$record[4] = $rec[url_link];
			
			$record[5] = $rec[update_date];
			$record[6] = $rec[admin_name];
			$record[7] = $rec[product_id];			
			
			$i++;
	
		}
		
		return $record;
	}
	
	/* banner lp */
	
	
	
	
	
	/* admin */
	public function admin_login($login,$pass)
	{
		$sql  = 'SELECT admin_id,admin_user,admin_type,pre_code,country_iso3 FROM '._DB_PREFIX_TABLE.'administrators a,
				'._DB_PREFIX_TABLE.'permission b ';
		$sql .= 'WHERE admin_user = "'.mysql_real_escape_string($login).'" AND ';
		$sql .= 'admin_pass = SHA1("'.mysql_real_escape_string($pass).'") AND ' ; 
		$sql .= 'admin_type = pre_type ';
		$sql .= 'LIMIT 0,1';
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$pre_code = $record[pre_code];
			$admin_id = $record[admin_id];
			$country_iso3 = $record[country_iso3];
		}
		
		return array($pre_code,$admin_id,$country_iso3);
	}
	
	/* admin */
	
	/* only opt front and country category 20120416	*/
	public function front_show_product_op($product_type,$country,$yyyy,$mm,$theme=array(),$option=array(),$time=array(), $page_first,$page_num)
	{
		
		
		$sql = 'SELECT a.country_iso3,path,country_name_jp,a.product_id,product_name_jp,price_min,price_max, price_adult,price_child ,price_infant, 
				short_desc,amount,rev_status,rev_date,rate,sign FROM '._DB_PREFIX_TABLE.'product a 
				LEFT JOIN (SELECT * FROM '._DB_PREFIX_TABLE.'monthly_product WHERE mpro_year = "'.$yyyy.'" AND mpro_month = "'.$mm.'" ) AS b ON 
				a.product_id = b.product_id ,'._DB_PREFIX_TABLE.'country c ,'._DB_PREFIX_TABLE.'currency e ';
		
		if ($country == "ALL")
		{
			$sql .= 'WHERE ';
		}
		else
		{
			$sql .= 'WHERE a.country_iso3 = "'.$country.'" AND ';
		}
		
		$sql .= 'a.product_type = "'.$product_type.'" AND ';
		
		if ( (count($theme) > 0) || (count($option) > 0) || (count($time) > 0) )
		{
			$sql .= '( ';
			if (count($theme) > 0)
			{
				$sql .= ' ( ' ;
					for ($i = 0; $i<count($theme);$i++)
					{
						 $sql .= 'product_theme LIKE "%'.$theme[$i].'%" '; 
						 
						 
						 if ((count($theme) > 1) && ( ($i) != (count($theme) -1) ))
						 {
							$sql .= ' OR ';	
						 }
					}
				$sql .= ' ) ';
			}
			
			if (count($option) > 0)
			{
				if (count($theme) > 0)
				{
					$sql .= ' OR ';	
				}
				$sql .= ' ( ';
				
				for ($i = 0; $i < count($option) ; $i++)
				{
					
					$sql .= 'option_id1 = "'.$option[$i]. '" ';	
										
					if ((count($option) > 1) && ( ($i) != (count($option) -1) ))
					{
						$sql .= ' OR ';	
					}
					
					
				}
				$sql .= ' ) ';
				
			}
			
			if (count($time) > 0)
			{
				if  ((count($theme) > 0) || (count($time) > 0) )	
				{
					$sql .= 'OR ';
				}
				
				$sql .= ' ( ';
				
				for ($i = 0; $i < count($time) ; $i++)
				{
					$sql .= 'status_time = "'.$time[$i].'" ';
					
					if ((count($time) > 1) && ( ($i) != (count($time) -1) ))
					{
						$sql .= ' OR ';	
					}	
				}
				
				$sql .= ' ) ';
			}
			
			$sql .= ' ) AND ';
		}
		$sql .= 'a.country_iso3 = c.country_iso3 AND a.country_iso3 = e.country_iso3 AND public = true 
				 ORDER BY amount DESC,product_name_jp LIMIT '.$page_first.','.$page_num;
	

		
		$result = $this->db_query($sql);
		
		$i =0; 
		
		while ($rec = mysql_fetch_array($result))
		{
			$record['product_id'][$i]	   = $rec[product_id];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['short_desc'][$i]      = $rec[short_desc];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]       = $rec[price_max];
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];
			$record['price_adult'][$i] 	   = $rec[price_adult];
			$record['price_child'][$i]     = $rec[price_child];
			$record['price_infant'][$i]    = $rec[price_infant];
			$record['country_iso3'][$i]	   = $rec[country_iso3];
			$record['path'][$i]   		   = $rec[path];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			
			$record['rate'][$i]            = $rec[rate];
			$record['sign'][$i]            = $rec[sign];

			$i++;
		}
				
		return $record;
				
	}
	/* only opt front and country category 20120416 */

}




?>