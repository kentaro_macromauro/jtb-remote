<?php
function upload_file($file,$mypath)
{
	$file1 = strtolower($file['name']);
	$fileupload = strtolower($mypath.'/'.$file['name']);
	if ($file1 != '')
	{
		if (!in_array(preg_replace('/(.*)[.]([^.]+)/is','$2',basename($file1)),array('exe','vbs','bat','com','php','jar'))) 
		{	
			if ($file['name'] != '')
			{	
				$file_size=$file["size"];
				if ($file_size > _LimitFileSize_)
				{	
					return false;  
				}
				else
				{  
					if ( move_uploaded_file($file['tmp_name'],$fileupload))
					{ 
						return true; 
					}
					else
					{
						return false;	
					}
				}  
			}
			else
			{
				return false;	
			}
		}
		else
		{ 
			return false; 	
		}
	}
	else
	{
		return false;	
	}
}


function mail_file( $to, $subject, $messagehtml, $from, $fileatt, $replyto="" ) 
{
		// handles mime type for better receiving
		$ext = strrchr( $fileatt , '.');
		$ftype = "";
		if ($ext == ".doc") $ftype = "application/msword";
		if ($ext == ".jpg") $ftype = "image/jpeg";
		if ($ext == ".gif") $ftype = "image/gif";
		if ($ext == ".zip") $ftype = "application/zip";
		if ($ext == ".pdf") $ftype = "application/pdf";
		if ($ftype=="") $ftype = "application/octet-stream";

		// read file into $data var
		$file = fopen($fileatt, "rb");
		$data = fread($file,  filesize( $fileatt ) );
		fclose($file);

		// split the file into chunks for attaching
		$content = chunk_split(base64_encode($data));
		$uid = md5(uniqid(time()));

		// build the headers for attachment and html
		$h = "From: $from\r\n";
		if ($replyto) $h .= "Reply-To: ".$replyto."\r\n";
		$h .= "MIME-Version: 1.0\r\n";
		$h .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$h .= "This is a multi-part message in MIME format.\r\n";
		$h .= "--".$uid."\r\n";
		$h .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		//$h .= "Content-type:text/html; charset=iso-8859-1\r\n";
		$h .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$h .= $messagehtml."\r\n\r\n";
		$h .= "--".$uid."\r\n";
		$h .= "Content-Type: ".$ftype."; name=\"".basename($fileatt)."\"\r\n";
		$h .= "Content-Transfer-Encoding: base64\r\n";
		$h .= "Content-Disposition: attachment; filename=\"".basename($fileatt)."\"\r\n\r\n";
		$h .= $content."\r\n\r\n";
		$h .= "--".$uid."--";

		// send mail
		return mail( $to, $subject, strip_tags($messagehtml), str_replace("\r\n","\n",$h) ) ;

}

?>