<? 
/* Cart */

function show_price($sgd,$yen)
{
	return $yen[sign].number_format($sgd).'～( \\'.number_format(  round($yen[rate]*$sgd)   ).'～)';
}

function jp_strdate($date)
{
	$arr_date =	explode('-',$date);
	return $arr_date[0].'年'.$arr_date[1].'月'.$arr_date[2].'日';
}

function delete_product($product_array,$select)
{
	$buffer = array();
	
	for ($i = 0; $i < count($product_array) ; $i++)
	{
		if ($product_array[$i] != $select)
		{
			array_push($buffer,	$product_array[$i]);
		}
	}
	
	return $buffer;
}

function gender_toint($data)
{
	switch ($data)
	{
		case '男性': $init = 1; break;
		case '女性': $init = 2; break;
		default    : $init = 0; break;
	}
	
	return $init;
}
/* Cart */

/* etc function */

function jd_decode($text)
{
	
	$text = str_replace("&quot;",'"', $text);
	$text = str_replace("&#039;", "'", $text);
	$text = str_replace('&lt;', '<', $text);
	$text = str_replace("&gt;",'>',$text);
	$text = str_replace("&amp;",'&',$text);
	return $text;
}

function check_img($img)
{
	if (!file_exists($img) )
	{
		return  '../images/img_notfound.jpg';
	}
	else
	{
		return  $img;
	}	

}

function str_len($text,$max)
{
	if ( strlen($text) > $max )
	{
		return mb_substr($text,0, $max,'UTF-8' ).'...';	
	}
	else
	{
		return $text;	
	}
}

function number_format_zero2($i)
{
	if ($i < 100)
	{
		if ($i < 10)
		{
		
			return '00'.$i;
		}
		else
		{
			return '0'.$i;
		}
	}
	else
	{
		return $i;
	}
}

function number_format_zero($i)
{
	if ($i < 10)
	{
		return '0'.$i;
	}
	else
	{
		return $i;
	}
}

function compare_text($str1,$str2)
{
	$pos = false;
	
	$pos = strpos($str1,number_format_zero($str2));
	
	if ($pos !== false)
	{
		return true;	
	}
	else
	{
		return false;	
	}
}
function dateto_array($date)
{
	$j = 0;
	for ($i=0;$i<strlen($date);$i++)
	{
		if (substr($date,$i,1)=='-')
		{
			if ($j == 0)
			{
				$arr_date[0] = 	substr($date,$j,($i-$j));
			}
			else
			{
				$arr_date[1] = 	substr($date,$j,($i-$j));
			}
			
			$j = $i+1;
		}
	
	}
	$arr_date[2] = 	substr($date,$j,($i-$j));
	
	return $arr_date;
}
/* etc function */
/* function show page navigation footer bar footer bar limit 10 and data bypage 
exsample echo page_navi(10,1120,'id=',100);*/
function pagenavi_start($page_count,$page_num,$page_now) /*page = $_GET[now] */
{
	if (empty($page_now))
	{
		return 1;	
	}
	else
	{	
		if (ceil($page_count/$page_num) < $page_now)
		{		
			return 1;	
		}
		else
		{
			return $page_now;	
		}
	}
}
function pagenavi_first($page_now,$page_num)
{
	return  (($page_now-1) *$page_num);
}
function pagenavi($now,$num,$prefix,$show_bypage=11)
{	
	$last_page =  ceil($num/$show_bypage);  
	
	$buffer  = '<div class="number"><div class="page_navi">';
   	$buffer .= '<span class="page_link_noaction">';
	$buffer .= '<strong>Page</strong> '.$now.' of '.$last_page.'</span>';

	
	if ($num == 0)
	{
		$buffer .=  '<span class="page_nonelink pagination_num">1</span>';
	}
else
{	

	if ($num < $show_bypage)
	{
		$buffer .= '<span class="page_nonelink pagination_num">1</span>';
	}
	else
	{
		
		$page_buffer = '';
		
		for ($i=1;$i <= $last_page;$i++)
		{
			if ($i == $now )
			{
				$page_buffer[$i] = '<span class="page_nonelink pagination_num">'.$i.'</span>';	
			}
			else
			{
				$page_buffer[$i] = '<a class="page_link pagination_num" href="'.$prefix.$i.'">'.$i.'</a>';	
			}
		}	
	}
	
	if (($now <= 5)||($num <= 10))
	{	
		if ($now > 1)
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';	
		}
	
		/*case1*/		
		
		for ($i = 1;$i<=10;$i++)
		{
			
			$buffer .= $page_buffer[$i];
			
			
		}

		
		if (($now != $last_page)&&($last_page > 5))
		{
			$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			
		}
		
		if ($last_page < 6)
		{
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		 
	}
	else
	{
		/*case2*/
		if  ( (($last_page-$now) != 0 ) && (($last_page-$now) < 5 ) )
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case21*/
			if ($now <= ($last_page-4))
			{	
				
				for ($i = ($last_page-9); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
			}
			else
			{
				for ($i = ($last_page-8); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
				
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		else
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case22*/
			if ($now != $last_page)
			{
				for ($i = ($now-5) ; $i <= ($now+4) ;$i++)	
				{
					
					$buffer .= $page_buffer[$i];	
				}
			}
			else
			{
				for ($i = ($now-8) ; $i <= ($now) ;$i++)	
				{

					$buffer .= $page_buffer[$i];	
				}
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}		
	}
}
	$buffer .= '</div></div>';
	return $buffer;
}
/* function show page navigation footer bar */
/* breadcamp */
function breadcamp($array_name,$array_link)
{
	$buffer = '<ul class="breadcamp">';	
	
	for ($i = 0;$i < count($array_name) ; $i++)
	{
		if ($array_link[$i] != '')
		{
			$buffer .= '<li><a href="'.$array_link[$i].'">'.$array_name[$i].'</a><span>&gt;</span></li>';	
		}
		else
		{
			$buffer .= '<li>'.$array_name[$i].'</li>';	
		}
	}
	
	$buffer .= '</ul>';
	
	return $buffer;
}
/*echo bread_camp(array('home','nav','sub_nav'),array('index.html','nav.html'));*/
function tb_head($name,$width='',$style='')
{
	if ($width == '')
		return '<th class="tittle" style="'.$style.'" >'.$name.'</th>';
	else
		return '<th width="'.$width.'" class="tittle" style="'.$style.'" >'.$name.'</th>';	
}
/*** View Data ***/
function tb_normal($data,$link,$align)
{	
	$buffer = '<tr valign="top" >'; 
	
	for ($i=0;$i<count($data);$i++)
	{	
		switch ($align[$i])
		{	
			case  0: $ali='left';  break;
			case  1: $ali='center'; break;
			case  2: $ali='right'; break;
			default: $ali='left';
		}
		
		if ( ($data[0] & 1) )
		{
			$class = 'even';	
		}
		else
		{
			$class = 'odd';	
		}


		
		if ($link[$i] == '')
		$buffer .= '<td align="'.$ali.'" class="'.$class.'">'.$data[$i].'</td>'; 
		else
		$buffer .= '<td align="'.$ali.'" class="'.$class.'"><a href="'.$link[$i].'">'.$data[$i].'</a></td>'; 
	}
	
	$buffer .= '</tr>';
	return $buffer;
}
/* echo tb_normal(array('data1','data2','data3'),array('aaa','','bbb'),array(0,1,2) );*/
function f_loop($arr_data)
{
	foreach ($arr_data as $raw_data)
	{
		echo $raw_data;
	}
}
function checkbox($id)
{
	return '<input name="chk'.$id.'" id="id_chk'.$id.'" type="checkbox" class="chkbox" value="'.$id.'" />'	;
}
/*** View Data ***/

/*** Input Data ***/
function table_data($tb_th,$tb_td,$width_th='',$width_td='',$req=0)
{
	
	if (($width_th == '')||($width_th == 0))
	{
		$width_th = 100	;
	}
	
	if (($width_td == '')||($width_td == 0))
	{
		$width_td = 800	;
	}
	
	
	
	if ($req == 1)
	{
		$msg = '<span class="msg-req">*</span>';	
	}
	else
	{
		$msg = '';	
	}
	
	return '<tr><th width="'.$width_th.'" valign="top" align="left">'.$tb_th.$msg.'</th><td width="'.$width_td.'" valign="top" align="left">'.$tb_td.'</td></tr>';
	
	
}
function input_selectbox($name,$arr_data,$arr_value,$focus,$startselect='---Please Select----',$class="inp_width")
{
	$buffer = '<select name="'.$name.'" class="'.$class.'">';
	$buffer .= '<option value="0" >'.$startselect.'</option>';
	
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
		}
		else
		{
			$buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';	
		}
	}
    $buffer .= '</select>';
	
	return $buffer;
}
function input_textbox($name,$value='',$class="inp_width")
{
	$buffer = '<input name="'.$name.'" class="'.$class.'" type="text" value="'.$value.'" />';
	return $buffer;
}
function input_hiddenbox($name,$value='')
{
	$buffer = '<input type="hidden" name="'.$name.'" value="'.$value.'" />'	;
	return $buffer;
}
function input_passwordbox($name,$value='')
{
	$buffer = '<input name="'.$name.'" class="inp_width" type="password" value="'.$value.'" />';
	return $buffer;
}
function input_memobox($name,$value='',$other='',$class="inp_width",$row=15)
{
	$buffer = '<textarea name="'.$name.'" class="'.$class.'" '.$other.'  rows="'.$row.'">'.$value.'</textarea>';	
	return $buffer;
}
function input_chkbox($name,$status=false)
{
	if ($status == true)
	{
		$buffer = '<input type="checkbox" name="'.$name.'" checked="checked" />';
	}
	else
	{	
		$buffer = '<input type="checkbox" name="'.$name.'" />';	
	}
	return $buffer;
}
function input_radio($name,$arr_data,$arr_value,$focus)
{
	$buffer = '';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		else
		{
			$buffer .= '<input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
	}
	return $buffer;
}
function input_radio_line($name,$arr_data,$arr_value,$focus)
{
	$buffer = '<ul class="select-radio-list">';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{ 
			$buffer .= '<li><input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
		else
		{
			$buffer .= '<li><input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
	}
	$buffer .= '</ul>';
	return $buffer;
}

function input_file($name)
{
	return '<input name="'.$name.'" type="file" value="upload"  />';
}
function input_link($name,$link)
{
	return '<a href="'.$link.'" >'.$name.'</a>';	
}
function input_img($name,$img)
{
	return '<img src="'.$img.'" name="'.$name.'"  />';	
}
function create_form($name,$action,$enctype=false)
{
	if ($enctype==true)
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" enctype="multipart/form-data">';	
	}
	else
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" >';	
	}
}
/*** Input Data ***/
function str_to_arr($data,$prefix=',')
{
	$j = 0; $k=0; $arr_data = '';
	for ($i=0;$i<strlen($data);$i++)
	{
		if (substr($data,$i,1)==$prefix)
		{
			$numid = (substr($data,$i-($j),$j)) ;
			$j=0;
			$arr_data[$k] = $numid;
			$k++;
		}
		else
		{
			$j++;
		}
	}
	
	return $arr_data;
}
/*** Input Data WhereCase ***/
function where_from_array($arr_id,$prefix)
{
	$dub = '';
	foreach ($arr_id as $idx)
	{
		if ($arr_id[count($arr_id)-1] == $idx)
		{
			$dub .= '('.$prefix.' = "'.$idx.'") ';
		} 
		else
		{
			$dub .= '('.$prefix.' = "'.$idx.'") OR ';
		}
	}	
	
	return $dub;
}
/*substr case \/ */
function text_first_prifix($text,$prefix)
{
	$precount = strlen($prefix) ;
	
	for ($i = 0;$i <strlen($text);$i++)
	{
		
		if (substr($text,$i,$precount)==$prefix)
		{
			$text2 = substr($text,0,$i);
		}
	}
	
	return $text2;	
}
function open_tinymce()
{
	include("../admin/include/tiny_mce.php");
	return $buffer;
}
function date_picker($data='')
{
	return '<div style="position:relative; width:350px; float:left; " class="p_ddpicker">
      <input name="txt_calendar" id="id_calendar" type="text" value="'.$data.'" />
      &nbsp;<a id="id_cal"><img src="images/btn/calendar_2.png" alt="calendar" /></a> </div>';
	
}
function them_search($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{

	
	$buffer = '<div class="height3"></div>
          		<!-- search result-->
					<div class="search-result">	
						<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
						<img src=';
		
	$product_img  = $path.'product/images/product/'.$data[$i][product_id].'-1.jpg'	;
	
						
	if (!file_exists($product_img) )
	{
		$buffer .= '"'.$path.'images/img_notfound.jpg"';
	}
	else
	{
		$buffer .= '"'.$path.'product/images/index.php?root=product&amp;width=170&amp;name='.trim( $data[$i][product_id].'-1.jpg').'" '	;
	}
	
						
	$buffer .= ' width="150" height="100" alt="" /></a>  
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
							<strong>'.str_len($data[$i][product_name_jp],30).'</strong></a></p>
							
							<p >'.str_len($data[$i][description],210).'</p>
							
							
							<div class="price" >
							<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～( ¥'.number_format($yen[rate] * $data[$i][price_min]).'～)</span>
							</div>
						</div>';
		
		if ($data[$i][rev_status] == '1')
		{
			$buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][product_id].'" class="review">Review</a>';
		}
				
		if ($type == "OPT")		
		{		
			$buffer .= '<ul class="search-result-category">	';
						
						if (is_array($category_ofproduct))
						{
							
							foreach ($category_ofproduct as $text)
							{
								
								$buffer .= '<li>'.$text.'</li>';	
							}
						}
			$buffer .= '</ul>';
		}
		else if ($type == "PKG")
		{
			$buffer .= '<ul class="search-result-category">	';					
			$buffer .= '<li>'.$data[$i][category_name].'</li>';	
			$buffer .= '</ul>';
		}
		
		$buffer .= '</div>
				<!-- search result-->';
				
	return $buffer;
}
function days_in_month($month, $year)
{
	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
} 
?>
