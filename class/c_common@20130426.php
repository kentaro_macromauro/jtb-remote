<? 
/* Cart */
/* 20130314 */
function show_jp_price($price){
	
	
	
	if (($price== 0)|| empty($price)){
		$data = '\\ -';
	}
	else{
		$data = '\\'.number_format($price);
	}
	
	return $data;
}

function show_location_price($sgd,$yen){
	
	if (($sgd == 0)||(empty($sgd))){
		$data .=  $yen[sign].'- ';
		
		if ($yen[show] == 1){
			$data .= '( \\'.'- )';
		}
	}
	else{
		$data = $yen[sign].number_format($sgd);
	
		if ($yen[show] == 1){
			$data .= '(目安：\\'.number_format(round($yen[rate]*$sgd)).')';
		}
	}
	
	return $data;
}

function input_selectbox_nonstart($name,$arr_data,$arr_value,$focus,$class="inp_width")
{
	$buffer = '<select name="'.$name.'" class="'.$class.'">';
	
	if (empty($focus)){
		$focus = '1';
	}
	
	
	for ($i=0;$i<count($arr_data);$i++){
		if ($focus == $arr_value[$i]){
			$buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
		}
		else{
			$buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';	
		}
	}
	
	$buffer .= '</select>';
	
	return $buffer;
}
/* 20130314 */

function show_price($sgd,$yen)
{
	
	
	if (($sgd == 0)||(empty($sgd)))
	{
		
		$data .=  $yen[sign].'- ';
		
		if ($yen[show] == 1)
		{
			$data .= '( 目安：\\'.'- )';
		}
	}
	else
	{
		$data = $yen[sign].number_format($sgd).'～';
	
		if ($yen[show] == 1)
		{
			$data .= '( 目安：\\'.number_format(  round($yen[rate]*$sgd)   ).'～)';
		}
	}
	
	
	
	return $data;
}

function jp_strdate($date)
{
	$arr_date =	explode('-',$date);
	return $arr_date[0].'年'.$arr_date[1].'月'.$arr_date[2].'日';
}

function delete_product($product_array,$select)
{
	$buffer = array();
	
	for ($i = 0; $i < count($product_array) ; $i++)
	{
		if ($product_array[$i] != $select)
		{
			array_push($buffer,	$product_array[$i]);
		}
	}
	
	return $buffer;
}

function gender_toint($data)
{
	switch ($data)
	{
		case '男性': $init = 1; break;
		case '女性': $init = 2; break;
		default    : $init = 0; break;
	}
	
	return $init;
}
/* Cart */

/* etc function */

function jd_decode($text)
{
	
	$text = str_replace("&quot;",'"', $text);
	$text = str_replace("&#039;", "'", $text);
	$text = str_replace('&lt;', '<', $text);
	$text = str_replace("&gt;",'>',$text);
	$text = str_replace("&amp;",'&',$text);
	return $text;
}

function check_img($img)
{
	if (!file_exists($img) )
	{
		return  '../images/img_notfound.jpg';
	}
	else
	{
		return  $img;
	}	

}

function str_len($text,$max)
{
	if ( mb_strlen(trim($text),'UTF-8') > $max )
	{
		return mb_substr($text,0, $max,'UTF-8' ).'...';	
	}
	else
	{
		return $text;	
	}
}

function number_format_zero2($i)
{
	if ($i < 100)
	{
		if ($i < 10)
		{
		
			return '00'.$i;
		}
		else
		{
			return '0'.$i;
		}
	}
	else
	{
		return $i;
	}
}

function number_format_zero($i)
{
	if ($i < 10)
	{
		return '0'.$i;
	}
	else
	{
		return $i;
	}
}

function compare_text($str1,$str2)
{
	$pos = false;
	
	$pos = strpos($str1,number_format_zero($str2));
	
	if ($pos !== false)
	{
		return true;	
	}
	else
	{
		return false;	
	}
}
function dateto_array($date)
{
	$j = 0;
	for ($i=0;$i<strlen($date);$i++)
	{
		if (substr($date,$i,1)=='-')
		{
			if ($j == 0)
			{
				$arr_date[0] = 	substr($date,$j,($i-$j));
			}
			else
			{
				$arr_date[1] = 	substr($date,$j,($i-$j));
			}
			
			$j = $i+1;
		}
	
	}
	$arr_date[2] = 	substr($date,$j,($i-$j));
	
	return $arr_date;
}
/* etc function */
/* function show page navigation footer bar footer bar limit 10 and data bypage 
exsample echo page_navi(10,1120,'id=',100);*/
function pagenavi_start($page_count,$page_num,$page_now) /*page = $_GET[now] */
{
	if (empty($page_now))
	{
		return 1;	
	}
	else
	{	
		if (ceil($page_count/$page_num) < $page_now)
		{		
			return 1;	
		}
		else
		{
			return $page_now;	
		}
	}
}
function pagenavi_first($page_now,$page_num)
{
	return  (($page_now-1) *$page_num);
}
function pagenavi($now,$num,$prefix,$show_bypage=11)
{	
	$last_page =  ceil($num/$show_bypage);  
	
	$buffer  = '<div class="number"><div class="page_navi">';
   	$buffer .= '<span class="page_link_noaction">';
	$buffer .= '<strong>Page</strong> '.$now.' of '.$last_page.'</span>';

	
	if ($num == 0)
	{
		$buffer .=  '<span class="page_nonelink pagination_num">1</span>';
	}
else
{	

	if ($num < $show_bypage)
	{
		$buffer .= '<span class="page_nonelink pagination_num">1</span>';
	}
	else
	{
		
		$page_buffer = '';
		
		for ($i=1;$i <= $last_page;$i++)
		{
			if ($i == $now )
			{
				$page_buffer[$i] = '<span class="page_nonelink pagination_num">'.$i.'</span>';	
			}
			else
			{
				$page_buffer[$i] = '<a class="page_link pagination_num" href="'.$prefix.$i.'">'.$i.'</a>';	
			}
		}	
	}
	
	if (($now <= 5)||($num <= 10))
	{	
		if ($now > 1)
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';	
		}
	
		/*case1*/		
		
		for ($i = 1;$i<=10;$i++)
		{
			
			$buffer .= $page_buffer[$i];
			
			
		}

		
		if (($now != $last_page)&&($last_page > 5))
		{
			$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			
		}
		
		if ($last_page < 6)
		{
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		 
	}
	else
	{
		/*case2*/
		if  ( (($last_page-$now) != 0 ) && (($last_page-$now) < 5 ) )
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case21*/
			if ($now <= ($last_page-4))
			{	
				
				for ($i = ($last_page-9); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
			}
			else
			{
				for ($i = ($last_page-8); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
				
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		else
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case22*/
			if ($now != $last_page)
			{
				for ($i = ($now-5) ; $i <= ($now+4) ;$i++)	
				{
					
					$buffer .= $page_buffer[$i];	
				}
			}
			else
			{
				for ($i = ($now-8) ; $i <= ($now) ;$i++)	
				{

					$buffer .= $page_buffer[$i];	
				}
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}		
	}
}
	$buffer .= '</div></div>';
	return $buffer;
}
/* function show page navigation footer bar */
/* breadcamp */
function breadcamp($array_name,$array_link)
{
	$buffer = '<ul class="breadcamp">';	
	
	for ($i = 0;$i < count($array_name) ; $i++)
	{
		if ($array_link[$i] != '')
		{
			$buffer .= '<li><a href="'.$array_link[$i].'">'.$array_name[$i].'</a><span>&gt;</span></li>';	
		}
		else
		{
			$buffer .= '<li>'.$array_name[$i].'</li>';	
		}
	}
	
	$buffer .= '</ul>';
	
	return $buffer;
}
/*echo bread_camp(array('home','nav','sub_nav'),array('index.html','nav.html'));*/
function tb_head($name,$width='',$style='')
{
	if ($width == '')
		return '<th class="tittle" style="'.$style.'" >'.$name.'</th>';
	else
		return '<th width="'.$width.'" class="tittle" style="'.$style.'" >'.$name.'</th>';	
}
/*** View Data ***/
function tb_normal($data,$link,$align)
{	
	$buffer = '<tr valign="top" >'; 
	
	for ($i=0;$i<count($data);$i++)
	{	
		switch ($align[$i])
		{	
			case  0: $ali='left';  break;
			case  1: $ali='center'; break;
			case  2: $ali='right'; break;
			default: $ali='left';
		}
		
		if ( ($data[0] & 1) )
		{
			$class = 'even';	
		}
		else
		{
			$class = 'odd';	
		}


		
		if ($link[$i] == '')
		$buffer .= '<td align="'.$ali.'" class="'.$class.'">'.$data[$i].'</td>'; 
		else
		$buffer .= '<td align="'.$ali.'" class="'.$class.'"><a href="'.$link[$i].'">'.$data[$i].'</a></td>'; 
	}
	
	$buffer .= '</tr>';
	return $buffer;
}
/* echo tb_normal(array('data1','data2','data3'),array('aaa','','bbb'),array(0,1,2) );*/
function f_loop($arr_data)
{
	foreach ($arr_data as $raw_data)
	{
		echo $raw_data;
	}
}
function checkbox($id)
{
	return '<input name="chk'.$id.'" id="id_chk'.$id.'" type="checkbox" class="chkbox" value="'.$id.'" />'	;
}
/*** View Data ***/

/*** Input Data ***/
function table_data($tb_th,$tb_td,$width_th='',$width_td='',$req=0)
{
	
	if (($width_th == '')||($width_th == 0))
	{
		$width_th = 100	;
	}
	
	if (($width_td == '')||($width_td == 0))
	{
		$width_td = 800	;
	}
	
	
	
	if ($req == 1)
	{
		$msg = '<span class="msg-req">*</span>';	
	}
	else
	{
		$msg = '';	
	}
	
	return '<tr><th width="'.$width_th.'" valign="top" align="left">'.$tb_th.$msg.'</th><td width="'.$width_td.'" valign="top" align="left">'.$tb_td.'</td></tr>';
	
	
}
function input_selectbox($name,$arr_data,$arr_value,$focus,$startselect='---Please Select----',$class="inp_width")
{
	$buffer = '<select name="'.$name.'" class="'.$class.'">';
	$buffer .= '<option value="0" >'.$startselect.'</option>';
	
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
		}
		else
		{
			$buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';	
		}
	}
    $buffer .= '</select>';
	
	return $buffer;
}
function input_textbox($name,$value='',$class="inp_width")
{
	$buffer = '<input name="'.$name.'" class="'.$class.'" type="text" value="'.$value.'" />';
	return $buffer;
}
function input_hiddenbox($name,$value='')
{
	$buffer = '<input type="hidden" name="'.$name.'" value="'.$value.'" />'	;
	return $buffer;
}
function input_passwordbox($name,$value='')
{
	$buffer = '<input name="'.$name.'" class="inp_width" type="password" value="'.$value.'" />';
	return $buffer;
}
function input_memobox($name,$value='',$other='',$class="inp_width",$row=15)
{
	$buffer = '<textarea name="'.$name.'" class="'.$class.'" '.$other.'  rows="'.$row.'">'.$value.'</textarea>';	
	return $buffer;
}
function input_chkbox($name,$status=false)
{
	if ($status == true)
	{
		$buffer = '<input type="checkbox" name="'.$name.'" checked="checked" />';
	}
	else
	{	
		$buffer = '<input type="checkbox" name="'.$name.'" />';	
	}
	return $buffer;
}
function input_radio($name,$arr_data,$arr_value,$focus)
{
	$buffer = '';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		else
		{
			$buffer .= '<input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
	}
	return $buffer;
}
function input_radio_line($name,$arr_data,$arr_value,$focus)
{
	$buffer = '<ul class="select-radio-list">';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{ 
			$buffer .= '<li><input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
		else
		{
			$buffer .= '<li><input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
	}
	$buffer .= '</ul>';
	return $buffer;
}

function input_file($name)
{
	return '<input name="'.$name.'" type="file" value="upload"  />';
}
function input_link($name,$link)
{
	return '<a href="'.$link.'" >'.$name.'</a>';	
}
function input_img($name,$img)
{
	return '<img src="'.$img.'" name="'.$name.'"  />';	
}
function create_form($name,$action,$enctype=false)
{
	if ($enctype==true)
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" enctype="multipart/form-data">';	
	}
	else
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" >';	
	}
}
/*** Input Data ***/
function str_to_arr($data,$prefix=',')
{
	$j = 0; $k=0; $arr_data = '';
	for ($i=0;$i<strlen($data);$i++)
	{
		if (substr($data,$i,1)==$prefix)
		{
			$numid = (substr($data,$i-($j),$j)) ;
			$j=0;
			$arr_data[$k] = $numid;
			$k++;
		}
		else
		{
			$j++;
		}
	}
	
	return $arr_data;
}
/*** Input Data WhereCase ***/
function where_from_array($arr_id,$prefix)
{
	$dub = '';
	foreach ($arr_id as $idx)
	{
		if ($arr_id[count($arr_id)-1] == $idx)
		{
			$dub .= '('.$prefix.' = "'.$idx.'") ';
		} 
		else
		{
			$dub .= '('.$prefix.' = "'.$idx.'") OR ';
		}
	}	
	
	return $dub;
}
/*substr case \/ */
function text_first_prifix($text,$prefix)
{
	$precount = strlen($prefix) ;
	
	for ($i = 0;$i <strlen($text);$i++)
	{
		
		if (substr($text,$i,$precount)==$prefix)
		{
			$text2 = substr($text,0,$i);
		}
	}
	
	return $text2;	
}
function open_tinymce()
{
	include("../admin/include/tiny_mce.php");
	return $buffer;
}
function date_picker($data='')
{
	return '<div style="position:relative; width:350px; float:left; " class="p_ddpicker">
      <input name="txt_calendar" id="id_calendar" type="text" value="'.$data.'" />
      &nbsp;<a id="id_cal"><img src="images/btn/calendar_2.png" alt="calendar" /></a> </div>';
	
}
function them_search($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{

	
	$buffer = '<div class="height3"></div>
          		<!-- search result-->
					<div class="search-result">	
						<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
						<img src=';
		
	$product_img  = $path.'product/images/product/'.$data[$i][product_id].'-1.jpg'	;
	
						
	if (!file_exists($product_img) )
	{
		$buffer .= '"'.$path.'images/img_notfound.jpg"';
	}
	else
	{
		$buffer .= '"'.$path.'product/images/index.php?root=product&amp;width=170&amp;name='.trim( $data[$i][product_id].'-1.jpg').'" '	;
	}
	
				
	$buffer .= ' width="150" height="100" alt="" /></a>  
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
							<strong>'.str_len($data[$i][product_name_jp],30).'</strong></a></p>
							
							<p >'.str_len($data[$i][short_desc],145).'</p>
							
							
							<div class="price" >';
							
							
							//var_dump($yen);
				if ( $yen[show] == 1)
				{
							
							
					
					if ($data[$i][price_min] == 0)
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- ( 目安：\\'.'- )</span>';
					}
					else
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～( 目安：\\'.number_format($yen[rate] * $data[$i][price_min]).'～)</span>';
					
					}
					
							
				}
				else
				{
					
					
					if ($data[$i][price_min] == 0)
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- </span>';
					}
					else
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～ </span>';
					}
				}
							
							
							
		$buffer .= '</div></div>';
		
		if ($data[$i][rev_status] == '1')
		{
			$buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][product_id].'" class="review">Review</a>';
		}
				
		if ($type == "OPT")		
		{		
			$buffer .= '<ul class="search-result-category">	';
						
						if (is_array($category_ofproduct))
						{
							
							foreach ($category_ofproduct as $text)
							{
								
								$buffer .= '<li>'.$text.'</li>';	
							}
						}
			$buffer .= '</ul>';
		}
		else if ($type == "PKG")
		{
			$buffer .= '<ul class="search-result-category">	';					
			$buffer .= '<li>'.$data[$i][category_name].'</li>';	
			$buffer .= '</ul>';
		}
		
		$buffer .= '</div>
				<!-- search result-->';
				
	return $buffer;
}
function days_in_month($month, $year)
{
	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
} 

function date_jp($date)
{
	$arr_date = explode('-',$date);
	$date_jp  = ($arr_date[0]*1).'年'.($arr_date[1]*1).'月'.($arr_date[2]*1).'日'; 
	
	return $date_jp;
}

/* 20130325 */
function them_search_souvenir($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{

	
	
	$buffer = '<div class="height3"></div>
          		<!-- search result-->
					<div class="search-result">	
						<a href="'.$path.$location.'/souvenir_product.php?souvenir_id='.$data[$i][souvenir_id].'">
						<img src=';
		
	$product_img  = $path.'product/images/souvenir/'.$data[$i][souvenir_id].'-1.jpg';
	
						
	if (!file_exists($product_img) )
	{
		$buffer .= '"'.$path.'images/img_notfound.jpg"';
	}
	else
	{
		$buffer .= '"'.$path.'product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $data[$i][souvenir_id].'-1.jpg').'" '	;
	}
	
				
	$buffer .= ' width="105" height="105" alt="" /></a>  
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$path.$location.'/souvenir_product.php?souvenir_id='.$data[$i][souvenir_id].'">
							<strong>'.str_len($data[$i][souvenir_name],30).'</strong></a></p>
							
							<p >'.str_len($data[$i][short_desc],145).'</p>
							
							
							<div class="price" >';
							
				if ($data[$i]['affiliate']){
					
					
					$buffer .= '<span class="txt-red-bold-price">\\'.number_format($data[$i][souvenir_price]).'</span>';
					
				}	
				else{
							
					if ( $yen[show] == 1)
					{
	
						if ($data[$i][souvenir_price] == 0)
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- ( 目安：\\'.'- )</span>';
						}
						else
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][souvenir_price]).' ( 目安：\\'.number_format($yen[rate] * $data[$i][souvenir_price]).')</span>';
						
						}
						
								
					}
					else
					{
						
						if ($data[$i][souvenir_price] == 0)
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- </span>';
						}
						else
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][souvenir_price]).' </span>';
						}
					}
							
				}
							
		$buffer .= '</div></div>';
		
		if ($data[$i][rev_status] == '1')
		{
			$buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][souvenir_id].'" class="review">Review</a>';
		}
				
		if ($type == "OPT")		
		{		
			$buffer .= '<ul class="search-result-category">	';
						
						if ($data[$i][store_type] == "1"){
							$buffer .= '<li>現地受取</li>';
						}
						else if ($data[$i][store_type] == "2"){
							$buffer .= '<li>日本受取</li>';
						}
						
						
						if (is_array($category_ofproduct))
						{
							
							foreach ($category_ofproduct as $text)
							{
								
								$buffer .= '<li>'.$text.'</li>';	
							}
						}
						
						
						if ($data[$i][status_book] == "1"){
							$buffer .= '<li>カード決済</li>';
						}
						else if ($data[$i][status_book] == "2"){
							$buffer .= '<li>現地支払</li>';
						}
						
						
						
			$buffer .= '</ul>';
		}
		else if ($type == "PKG")
		{
			$buffer .= '<ul class="search-result-category">	';					
			$buffer .= '<li>'.$data[$i][souvenir_theme].'</li>';	
			$buffer .= '</ul>';
		}
		
		$buffer .= '</div>
				<!-- search result-->';
				
	return $buffer;
}

function check_date($yy,$mm,$dd){
	$icheck = 0;
	
	if (!$yy){
		$icheck++;
	}
	
	if (!$mm){
		$icheck++;
	}
	
	if (!$dd){
		$icheck++;
	}
	
	
	
	if ($yy < 1000){
		$icheck++;
		
	}
	
	
	if ($mm > 13){
		$icheck++;
	}
	
	if ($mm > 31){
		$icheck++;
	}
	
	
	
	if ($icheck){
		return '0000-00-00';	
	}
	else{
		
		if ($mm < 10){
			$mm = '0'.$mm;	
		}
		
		if ($dd < 10){
			$dd = '0'.$dd;	
		}
		
		return $yy.'-'.$mm.'-'.$dd;	
	}
}


function check_time($hh,$mm){
	if ( is_numeric($hh) && is_numeric($mm) ){
		
		if ( ( $hh >= 0 ) && ( $hh <= 23 ) ){
			
			if ( ($mm >= 0) && ($mm <= 59 ) ){
				
				if ($hh < 10){
					$hh = '0'.$hh;	
				}
				
				if ($mm < 10){
					$mm = '0'.$mm;	
				}
				
				return $hh.':'.$mm;
				
			}
			else{
				return '00:00';	
			}
			
		}
		else{
			return '00:00';	
		}
		
	}
	else{
		return '00:00';	
	}
}


function email_by_country($iso3){

	switch($iso3)
	{
		case 'TWN' :  $address_mail = 'stc_trading.tw@jtbap.com'; break;	
		
		case 'IDN' :  $address_mail = 'reservation_jtbbali.id@jtbap.com'; break;
		
		case 'THA' : if ($city_iso3 == 'HKT')
					 { 
						$address_mail = 'toiawase_phuket.th@jtbap.com';  
					 }
					 else
					 {
						 $address_mail = 'reservation_mybus.th@jtbap.com';      	 
					 } 
		break;	
		
		case 'VNM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'KHM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;
		
		case 'CAM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'MYS' :  $address_mail = 'mybus.my@jtbap.com'; break;
		 
		case 'SGP' :  $address_mail = 'orchard.sg@jtbap.com'; break;	
		
		case 'HKG' :  $address_mail = 'inbound@hk.jtb.cn';  break;
		
		case 'AUS' :  $address_mail = 'isdweb.au@jtbap.com'; break;	
		
		case 'NZL' :  $address_mail = 'webnz.nz@jtbap.com'; break;			
	}
	
	return trim($address_mail);
}

function signature_by_country($iso3){
	switch( $iso3)
		{
			case 'TWN' : $mail_tempage = '============================================
											JTB台湾（世帝喜旅行社股份有限公司）
											マイバスデスク
											台湾台北市中山北路2段56號JTBラウンジ
											09：00～18：00（予約受付は17：30まで）年中無休
											Tel: +(886)-02-2521-7315 
											Fax:+(886)-02-8192-7111
											<a target="_blank" href="http://www.jtbtaiwan.com">http://www.jtbtaiwan.com</a>
											台湾のマイバスデスクはアンバサダーホテル（國賓飯店）向いのJTBラウンジ内です。
											=============================================
											●”マイバス”のオプショナルツアー（27コース）やお勧めレストラン情報（14箇所）、
											割引特典（12店舗）の掲載されたパンフレットは下記で入手できます。
											・台湾内各主要都市の主なホテル
											● マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
											'; break;	
														
			case 'THA' : $mail_tempage = '=============================================
											JTB (Thailand) Ltd （ジェイティービー　バンコク支店）
											Mybus Operation
											Harindhorn Building 4th Floor 4B, 54 North Sathorn Road,
											Kwang Silom, Khet Bangrak, Bangkok 10500 Thailand
											Tel: +(66)-2267-9236
											Fax:+(66)-2266-3156
											<a target="_blank" href="http://www.mybus-asia.com/thailand/" >http://www.mybus-asia.com/thailand/</a>
												
											お問い合わせ、お申し込みは日本語で！
											Tel: +(66)-2632-9404
											=============================================
											
											=============================================
											JTB (THAILAND) LIMITED, Phuket Branch, Mybus Desk
											JTBプーケット営業所 マイバスデスク
											
											Tel. +66 (076) 261-740
											Fax. +66 (076) 261-748
											お電話での予約受付時間 9：00～18：00（年中無休）
													
											e-mail: toiawase_phuket.th@jtbap.com
											<a href="http://www.jtbphuket.com/" target="_blank">http://www.jtbphuket.com/</a>
											<a href="http://www.mybus-asia.com/thailand/" target="_blank">http://www.mybus-asia.com/thailand/</a>
											=============================================
											●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、バリ、プーケット、ベトナム）で年中無休で運行中。
											'; break;	
														
				case 'VNM' : $mail_tempage = '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
												
												case 'KHM' : $mail_tempage .= '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
												
												
				case 'CAM' : $mail_tempage = '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
													
				case 'MYS' : $mail_tempage = '====================================
											JTB (M) SDN BHD （ジェイティービー　マレーシア支店）
												
											ＫＬオフィス　(メリアホテル横、AMODAビル16階）
											16.05-16.07 Level 16, Amoda Bldg.
											22 Jalan Imbi 55100 Kuala Lumpur
											Malaysia 
											Tel: +(603) 2142-8727
											Fax:+(603) 2142-5344
													
											ランカウイ：＋（604）966-4708
											ペナン　　：＋（604）263-5788
											コタキナバル：＋（6088）259-668
											http://www.mybus-asia.com/malaysia/
											======================================'; break;
											
				case 'IDN' : $mail_tempage = '============================================
											PT. JTB INDONESIA （JTBバリ支店）
											
											Jl. Bypass Ngurah Rai No.88
											Kelan Abian,Tuban, Kuta, 
											Bali 80362 - Indonesia
											マイバスほっとダイヤル：0361-708555（バリ島内 / 09：00～19：00）
											緊急連絡先：081-2382-9139, 081-23802511（上記時間外）
											WEBサイト：http://www.mybus-asia.com/indonesia/
											=============================================
											●バリ島でのお客様用カウンターデスク
											・DFSギャラリア・バリ2階 （営業時間 10：00～18：00）
											・グランド・ハイアット・バリ内 （営業時間 08：00～20：00）
											●マイバスはアジアと豪州9ヵ国 （香港（マカオ）、台湾、タイ（プーケット）、マレーシア、シンガポール、バリ、ベトナム、オーストラリア、ニュージーランド）で年中無休で運行中。
											';
											break;	
														
				case 'SGP' : $mail_tempage = '===============================
											JTB Pte Ltd Singapore
											Mybus Desk/マイバスデスク
											（DFS免税品店内2階）
											Tel: (65)-6735－2847 
											Fax:(65)-6733－8148
											受付時間：10：00－17：30（年中無休）
											<a target=_blank" href="http://www.mybus-asia.com/singapore/">http://www.mybus-asia.com/singapore/</a>
											e-mail:opjtb.sg@jtbap.com
											================================'; 
												break;	
														
				case 'HKG' : $mail_tempage =  '============================================ 
											MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光） 
											Add     : Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., 
												  　　Tsim Sha Tsui, Kowloon, Hong Kong. 
											Tel      : (852)2375-7576 (*日本語対応　年中無休09:00-18:00)
											Fax     : (852)2311-5803 
											Mail    : inbound@hk.jtb.cn 
											Web    : <a target=_blank" href="http://www.mybus-asia.com/hongkong/">http://www.mybus-asia.com/hongkong/</a>
											============================================= 
											●”マイバス香港”は、 
											　ビギナーからリピーター向けオプショナルツアー（44コース）、 
											　便利で安心のスパ・マッサージ（8店舗）、 
											　あの憧れのレストランをはじめ厳選のミールクーポン（43コース）、 
											　割引特典レストラン（3店舗）をご用意しています。 
											●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、 
											　バリ、プーケット、ベトナム）で年中無休で運行中。 '; break;
												
				case 'AUS' : $mail_tempage =  '===========================================
											JTB Australia Pty Ltd
													
											ケアンズ マイバスデスク
											電話　+61-(07)-4052-5872 
											毎日 9：00～19：00
											61　Abbott　Street, Cairns
												
											ゴールドコーストマイバスデスク
											電話　+61-(07)-5592-9491 
											毎日 8：30～18：30 
											Ground Floor, 3191 Surfers Paradise Boulevard
													
											シドニー
											電話　+61-(02)-9510-0313 
											毎日 09:00 ～ 17:00
													
											メルボルン
											電話　+61(03)-8623-0091 
											土日祝日を除く毎日 10:00 ～12:00/14:00 ～16:00 
											Level 6, 10 Artemis Lane, Melbourne
											(QVショッピングコンプレックス内) 
													
											パース
											電話　+61(08)-9218-9866 
											毎日 09:00 ～ 12:00/13:00 ～ 17:30
											Ground Floor, 273 Hay Street, East Perth
											isdweb.au@jtbap.com
											============================================'; break;	
														
				case 'NZL' : $mail_tempage =  '===================================================================
											JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）
											Auckland Mybus Centre
											Level 5, 191 Queen St, Auckland
											Tel: +(64)-9379-6415 
											Fax:+(64)-9309-7699
											<a target=_blank" href="http://www.jtb.co.nz/">http://www.jtb.co.nz/</a>
											===================================================================
											●マイバスセンター　オークランド支店は平日　9：00〜17：30の営業です。
											（土日祝日はお休みとなります。）'; break;			
		}
		
		return nl2br($mail_tempage);
		
	
}

/* 20130325 */
?>