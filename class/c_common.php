<? 
/* Cart */
/* 20130314 */
function show_jp_price($price){
	
	
	
	if (($price== 0)|| empty($price)){
		$data = '\\ -';
	}
	else{
		$data = '\\'.number_format($price);
	}
	
	return $data;
}

function show_location_price($sgd,$yen){
	
	if (($sgd == 0)||(empty($sgd))){
		$data .=  $yen[sign].'- ';
		
		if ($yen[show] == 1){
			$data .= '( \\'.'- )';
		}
	}
	else{
		//$data = $yen[sign].number_format($sgd);
		
		$num_decimals = (intval($sgd) == $sgd) ? 0 :2;
		$data = $yen[sign].number_format($sgd,$num_decimals);
	
		if ($yen[show] == 1){
			$data .= '(目安：\\'.number_format(round($yen[rate]*$sgd)).')';
		}
	}
	
	return $data;
}

function input_selectbox_nonstart($name,$arr_data,$arr_value,$focus,$class="inp_width")
{
	$buffer = '<select name="'.$name.'" class="'.$class.'">';
	
	if (empty($focus)){
		$focus = '1';
	}
	
	
	for ($i=0;$i<count($arr_data);$i++){
		if ($focus == $arr_value[$i]){
			$buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
		}
		else{
			$buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';	
		}
	}
	
	$buffer .= '</select>';
	
	return $buffer;
}
/* 20130314 */

function show_price($sgd,$yen)
{
	
	
	if (($sgd == 0)||(empty($sgd)))
	{
		
		$data .=  $yen[sign].'- ';
		
		if ($yen[show] == 1)
		{
			$data .= '( 目安：\\'.'- )';
		}
	}
	else
	{
		$data = $yen[sign].number_format($sgd).'～';
	
		if ($yen[show] == 1)
		{
			$data .= '( 目安：\\'.number_format(  round($yen[rate]*$sgd)   ).'～)';
		}
	}
	
	
	
	return $data;
}

function jp_strdate($date)
{
	$arr_date =	explode('-',$date);
	return $arr_date[0].'年'.$arr_date[1].'月'.$arr_date[2].'日';
}

function delete_product($product_array,$select)
{
	$buffer = array();
	
	for ($i = 0; $i < count($product_array) ; $i++)
	{
		if ($product_array[$i] != $select)
		{
			array_push($buffer,	$product_array[$i]);
		}
	}
	
	return $buffer;
}

function gender_toint($data)
{
	switch ($data)
	{
		case '男性': $init = 1; break;
		case '女性': $init = 2; break;
		default    : $init = 0; break;
	}
	
	return $init;
}
/* Cart */

/* etc function */

function jd_decode($text)
{
	
	$text = str_replace("&quot;",'"', $text);
	$text = str_replace("&#039;", "'", $text);
	$text = str_replace('&lt;', '<', $text);
	$text = str_replace("&gt;",'>',$text);
	$text = str_replace("&amp;",'&',$text);
	return $text;
}

function check_img($img)
{
	if (!file_exists($img) )
	{
		return  '../images/img_notfound.jpg';
	}
	else
	{
		return  $img;
	}	

}

function str_len($text,$max)
{
	if ( mb_strlen(trim($text),'UTF-8') > $max )
	{
		return mb_substr($text,0, $max,'UTF-8' ).'...';	
	}
	else
	{
		return $text;	
	}
}

function number_format_zero2($i)
{
	if ($i < 100)
	{
		if ($i < 10)
		{
		
			return '00'.$i;
		}
		else
		{
			return '0'.$i;
		}
	}
	else
	{
		return $i;
	}
}

function number_format_zero($i)
{
	if ($i < 10)
	{
		return '0'.$i;
	}
	else
	{
		return $i;
	}
}

function compare_text($str1,$str2)
{
	$pos = false;
	
	$pos = strpos($str1,number_format_zero($str2));
	
	if ($pos !== false)
	{
		return true;	
	}
	else
	{
		return false;	
	}
}
function dateto_array($date)
{
	$j = 0;
	for ($i=0;$i<strlen($date);$i++)
	{
		if (substr($date,$i,1)=='-')
		{
			if ($j == 0)
			{
				$arr_date[0] = 	substr($date,$j,($i-$j));
			}
			else
			{
				$arr_date[1] = 	substr($date,$j,($i-$j));
			}
			
			$j = $i+1;
		}
	
	}
	$arr_date[2] = 	substr($date,$j,($i-$j));
	
	return $arr_date;
}
/* etc function */
/* function show page navigation footer bar footer bar limit 10 and data bypage 
exsample echo page_navi(10,1120,'id=',100);*/
function pagenavi_start($page_count,$page_num,$page_now) /*page = $_GET[now] */
{
	if (empty($page_now))
	{
		return 1;	
	}
	else
	{	
		if (ceil($page_count/$page_num) < $page_now)
		{		
			return 1;	
		}
		else
		{
			return $page_now;	
		}
	}
}
function pagenavi_first($page_now,$page_num)
{
	return  (($page_now-1) *$page_num);
}
function pagenavi($now,$num,$prefix,$show_bypage=11)
{	
	$last_page =  ceil($num/$show_bypage);

	$buffer  = '<div class="number"><div class="page_navi">';
   	$buffer .= '<span class="page_link_noaction">';
	$buffer .= '<strong>Page</strong> '.$now.' of '.$last_page.'</span>';

	
	if ($num == 0)
	{
		$buffer .=  '<span class="page_nonelink pagination_num">1</span>';
	}
else
{	

	if ($num < $show_bypage)
	{
		$buffer .= '<span class="page_nonelink pagination_num">1</span>';
	}
	else
	{
		
		$page_buffer = '';
		
		for ($i=1;$i <= $last_page;$i++)
		{
			if ($i == $now )
			{
				$page_buffer[$i] = '<span class="page_nonelink pagination_num">'.$i.'</span>';	
			}
			else
			{
				$page_buffer[$i] = '<a class="page_link pagination_num" href="'.$prefix.$i.'">'.$i.'</a>';	
			}
		}	
	}
	
	if (($now <= 5)||($num <= 10))
	{	
		if ($now > 1)
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';	
		}
	
		/*case1*/		
		
		for ($i = 1;$i<=10;$i++)
		{
			
			$buffer .= $page_buffer[$i];
			
			
		}

		
		if (($now != $last_page)&&($last_page > 5))
		{
			$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			
		}
		
		if ($last_page < 6)
		{
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		 
	}
	else
	{
		/*case2*/
		if  ( (($last_page-$now) != 0 ) && (($last_page-$now) < 5 ) )
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case21*/
			if ($now <= ($last_page-4))
			{	
				
				for ($i = ($last_page-9); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
			}
			else
			{
				for ($i = ($last_page-8); $i <= ($last_page) ; $i++)
				{
					
					$buffer .= $page_buffer[$i];	
					
				}
				
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}
		else
		{
			$buffer .= '<a class="page_link pagination_first" href="'.$prefix.'1">&lt;&lt;First...</a><a class="page_link pagination_prev" href="'.$prefix.($now-1).'">&lt;&lt;</a>';
			/*case22*/
			if ($now != $last_page)
			{
				for ($i = ($now-5) ; $i <= ($now+4) ;$i++)	
				{
					
					$buffer .= $page_buffer[$i];	
				}
			}
			else
			{
				for ($i = ($now-8) ; $i <= ($now) ;$i++)	
				{

					$buffer .= $page_buffer[$i];	
				}
			}
			
			if ($now != $last_page)
			{
				$buffer .= '<a class="page_link pagination_next" href="'.$prefix.($now+1).'">&gt;&gt;</a><a class="page_link pagination_last" href="'.$prefix.$last_page.'">...Last&gt;&gt;</a>';
			}
		}		
	}
}
	$buffer .= '</div></div>';
	return $buffer;
}
function res_pagenavi($now,$num,$prefix,$show_bypage=15)
{
    $last_page =  ceil($num/$show_bypage);

    $buffer  = '<nav><ul class="pagination pg-fc">';
    if ($num == 0)
    {
        $buffer .= '<li class="active"><a>1</a></li>';
    }
    else
    {
        if ($num < $show_bypage)
        {
            $buffer .= '<li class="active"><a>1</a></li>';
        }
        else
        {
            $page_buffer = '';

            for ($i=1;$i <= $last_page;$i++)
            {
                if ($i == $now )
                {
                    $page_buffer[$i] = '<li class="active"><a href="#">'.$i.'</a></li><li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>';
                }
                else
                {
                    $page_buffer[$i] = '<li class="pagination-space"><a href="'.$prefix.$i.'">'.$i.'</a></li><li><a class="pagination-space" style="padding: 0px; border: none; border-radius: 0px; background: transparent;">&nbsp;</a></li>';
                }
            }
        }
        if (($now <= 5)||($num <= 10))
        {
            if ($now > 1)
            {
                $buffer .= '<li class="pagination-space"><a href="'.$prefix.'1">&lt;&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            <li class="pagination-space"><a href="'.$prefix.($now-1).'">&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>';
            }

            /*case1*/

            for ($i = 1;$i<=10;$i++)
            {
                $buffer .= $page_buffer[$i];
            }

            if (($now != $last_page)&&($last_page > 5))
            {

                $buffer .= '<li class="pagination-space"><a href="'.$prefix.($now+1).'">&gt;</a>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            <li class="pagination-space"><a href="'.$prefix.$last_page.'">&gt;&gt;</a></li>';

            }

            if ($last_page < 6)
            {
                if ($now != $last_page)
                {
                    $buffer .= '<li class="pagination-space"><a href="'.$prefix.($now+1).'">&gt;</a></li>
                                <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                                <li class="pagination-space"><a href="'.$prefix.$last_page.'">&gt;&gt;</a></li>';
                }
            }

        }
        else
        {
            /*case2*/
            if  ( (($last_page-$now) != 0 ) && (($last_page-$now) < 5 ) )
            {
                $buffer .= '<li class="pagination-space"><a href="'.$prefix.'1">&lt;&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            <li class="pagination-space"><a href="'.$prefix.($now-1).'">&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            ';

                /*case21*/
                if ($now <= ($last_page-4))
                {

                    for ($i = ($last_page-9); $i <= ($last_page) ; $i++)
                    {

                        $buffer .= $page_buffer[$i];

                    }
                }
                else
                {
                    for ($i = ($last_page-8); $i <= ($last_page) ; $i++)
                    {

                        $buffer .= $page_buffer[$i];

                    }

                }

                if ($now != $last_page)
                {
                    $buffer .= '<li class="pagination-space"><a href="'.$prefix.($now+1).'">&gt;</a></li>
                                <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                                <li class="pagination-space"><a href="'.$prefix.$last_page.'">&gt;&gt;</a></li>';
                }
            }
            else
            {
                $buffer .= '<li class="pagination-space"><a href="'.$prefix.'1">&lt;&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            <li class="pagination-space"><a href="'.$prefix.($now-1).'">&lt;</a></li>
                            <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                            ';

                /*case22*/
                if ($now != $last_page)
                {
                    for ($i = ($now-5) ; $i <= ($now+4) ;$i++)
                    {

                        $buffer .= $page_buffer[$i];
                    }
                }
                else
                {
                    for ($i = ($now-8) ; $i <= ($now) ;$i++)
                    {

                        $buffer .= $page_buffer[$i];
                    }
                }

                if ($now != $last_page)
                {

                    $buffer .= '<li class="pagination-space"><a href="'.$prefix.($now+1).'">&gt;</a></li>
                                <li><a class="pagination-space" style="padding: 0px; border: none; background: transparent;">&nbsp;</a></li>
                                <li class="pagination-space"><a href="'.$prefix.$last_page.'">&gt;&gt;</a></li>';

                }
            }
        }
    }
    $buffer .= '</ul></nav>';
    return $buffer;
}

/* function show page navigation footer bar */
/* breadcamp */
function breadcamp($array_name,$array_link)
{
	$buffer = '<ul class="breadcamp">';	
	
	for ($i = 0;$i < count($array_name) ; $i++)
	{
		if ($array_link[$i] != '')
		{
			$buffer .= '<li><a href="'.$array_link[$i].'">'.$array_name[$i].'</a><span>&gt;</span></li>';	
		}
		else
		{
			$buffer .= '<li>'.$array_name[$i].'</li>';	
		}
	}
	
	$buffer .= '</ul>';
	
	return $buffer;
}
/*echo bread_camp(array('home','nav','sub_nav'),array('index.html','nav.html'));*/
function output_breadcamp($array_name,$array_link)
{
    $buffer = '<p>';
    for ($i = 0;$i < count($array_name) ; $i++)
    {
        if($i != 0){
            $buffer .= '&nbsp;&nbsp;>&nbsp;&nbsp;';
        }
        if ($array_link[$i] != '')
        {
            $buffer .= '<a href="'.$array_link[$i].'" style="color: #000; font-size: 12px;">'.$array_name[$i].'</a>';
        }
        else
        {
            $buffer .= $array_name[$i];
        }
    }
    $buffer .= '</p>';

    return $buffer;
}

function tb_head($name,$width='',$style='')
{
	if ($width == '')
		return '<th class="tittle" style="'.$style.'" >'.$name.'</th>';
	else
		return '<th width="'.$width.'" class="tittle" style="'.$style.'" >'.$name.'</th>';	
}
/*** View Data ***/
function tb_normal($data,$link,$align)
{	
	$buffer = '<tr valign="top" >'; 
	
	for ($i=0;$i<count($data);$i++)
	{	
		switch ($align[$i])
		{	
			case  0: $ali='left';  break;
			case  1: $ali='center'; break;
			case  2: $ali='right'; break;
			default: $ali='left';
		}
		
		if ( ($data[0] & 1) )
		{
			$class = 'even';	
		}
		else
		{
			$class = 'odd';	
		}


		
		if ($link[$i] == '')
		$buffer .= '<td align="'.$ali.'" class="'.$class.'" style="word-break:break-all;">'.$data[$i].'</td>';
		else
            if(is_array($link[$i])){
                $buffer .= '<td align="'.$ali.'" class="'.$class.'" style="word-break:break-all;"><a href="'.$link[$i]['url'].'" target="_blank">'.$data[$i].'</a></td>';
            }else{
                $buffer .= '<td align="'.$ali.'" class="'.$class.'" style="word-break:break-all;"><a href="'.$link[$i].'">'.$data[$i].'</a></td>';
            }
	}
	
	$buffer .= '</tr>';
	return $buffer;
}
/* echo tb_normal(array('data1','data2','data3'),array('aaa','','bbb'),array(0,1,2) );*/
function f_loop($arr_data)
{
	foreach ($arr_data as $raw_data)
	{
		echo $raw_data;
	}
}
function checkbox($id)
{
	return '<input name="chk'.$id.'" id="id_chk'.$id.'" type="checkbox" class="chkbox" value="'.$id.'" />'	;
}
/*** View Data ***/

/*** Input Data ***/
function table_data($tb_th,$tb_td,$width_th='',$width_td='',$req=0)
{
	
	if (($width_th == '')||($width_th == 0))
	{
		$width_th = 100	;
	}
	
	if (($width_td == '')||($width_td == 0))
	{
		$width_td = 800	;
	}
	
	
	
	if ($req == 1)
	{
		$msg = '<span class="msg-req">*</span>';	
	}
	else
	{
		$msg = '';	
	}
	
	return '<tr><th width="'.$width_th.'" valign="top" align="left">'.$tb_th.$msg.'</th><td width="'.$width_td.'" valign="top" align="left">'.$tb_td.'</td></tr>';
	
	
}
function input_selectbox($name,$arr_data,$arr_value,$focus,$startselect='---Please Select----',$class="inp_width")
{
	$buffer = '<select name="'.$name.'" class="'.$class.'">';
	$buffer .= '<option value="0" >'.$startselect.'</option>';
	
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
		}
		else
		{
			$buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';	
		}
	}
    $buffer .= '</select>';
	
	return $buffer;
}
function input_option($arr_data,$arr_value,$focus)
{
    $buffer = null;
    for ($i=0;$i<count($arr_data);$i++)
    {
        if ($focus == $arr_value[$i])
        {
            $buffer .= '<option value="'.$arr_value[$i].'" selected="selected" >'.$arr_data[$i].'</option>';
        }
        else
        {
            $buffer .= '<option value="'.$arr_value[$i].'" >'.$arr_data[$i].'</option>';
        }
    }
    return $buffer;
}
function input_textbox($name,$value='',$class="inp_width")
{
	$buffer = '<input name="'.$name.'" class="'.$class.'" type="text" value="'.$value.'" />';
	return $buffer;
}
function input_hiddenbox($name,$value='')
{
	$buffer = '<input type="hidden" name="'.$name.'" value="'.$value.'" />'	;
	return $buffer;
}
function input_passwordbox($name,$value='')
{
	$buffer = '<input name="'.$name.'" class="inp_width" type="password" value="'.$value.'" />';
	return $buffer;
}
function input_memobox($name,$value='',$other='',$class="inp_width",$row=15)
{
	$buffer = '<textarea name="'.$name.'" class="'.$class.'" '.$other.'  rows="'.$row.'">'.$value.'</textarea>';	
	return $buffer;
}
function input_chkbox($name,$status=false)
{
	if ($status == true)
	{
		$buffer = '<input type="checkbox" name="'.$name.'" checked="checked" />';
	}
	else
	{	
		$buffer = '<input type="checkbox" name="'.$name.'" />';	
	}
	return $buffer;
}
function input_radio($name,$arr_data,$arr_value,$focus)
{
	$buffer = '';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{
			$buffer .= '<input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		else
		{
			$buffer .= '<input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /> '.$arr_data[$i].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
	}
	return $buffer;
}
function input_radio_line($name,$arr_data,$arr_value,$focus)
{
	$buffer = '<ul class="select-radio-list">';
	for ($i=0;$i<count($arr_data);$i++)
	{
		if ($focus == $arr_value[$i])
		{ 
			$buffer .= '<li><input type="radio" name="'.$name.'" checked="checked" value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
		else
		{
			$buffer .= '<li><input type="radio" name="'.$name.'"  value="'.$arr_value[$i].'" /><label> '.$arr_data[$i].'</label></li>';
		}
	}
	$buffer .= '</ul>';
	return $buffer;
}

function input_file($name)
{
	return '<input name="'.$name.'" type="file" value="upload"  />';
}
function input_link($name,$link)
{
	return '<a href="'.$link.'" >'.$name.'</a>';	
}
function input_img($name,$img)
{
	return '<img src="'.$img.'" name="'.$name.'"  />';	
}
function create_form($name,$action,$enctype=false)
{
	if ($enctype==true)
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" enctype="multipart/form-data">';	
	}
	else
	{
		return '<form action="'.$action.'" name="'.$name.'" method="post" >';	
	}
}
/*** Input Data ***/
function str_to_arr($data,$prefix=',')
{
	$j = 0; $k=0; $arr_data = '';
	for ($i=0;$i<strlen($data);$i++)
	{
		if (substr($data,$i,1)==$prefix)
		{
			$numid = (substr($data,$i-($j),$j)) ;
			$j=0;
			$arr_data[$k] = $numid;
			$k++;
		}
		else
		{
			$j++;
		}
	}
	
	return $arr_data;
}
/*** Input Data WhereCase ***/
function where_from_array($arr_id,$prefix)
{
	$dub = '';
	foreach ($arr_id as $idx)
	{
		if ($arr_id[count($arr_id)-1] == $idx)
		{
			$dub .= '('.$prefix.' = "'.$idx.'") ';
		} 
		else
		{
			$dub .= '('.$prefix.' = "'.$idx.'") OR ';
		}
	}	
	
	return $dub;
}
/*substr case \/ */
function text_first_prifix($text,$prefix)
{
	$precount = strlen($prefix) ;
	
	for ($i = 0;$i <strlen($text);$i++)
	{
		
		if (substr($text,$i,$precount)==$prefix)
		{
			$text2 = substr($text,0,$i);
		}
	}
	
	return $text2;	
}
function open_tinymce()
{
	include("../admin/include/tiny_mce.php");
	return $buffer;
}
function date_picker($data='')
{
	return '<div style="position:relative; width:350px; float:left; " class="p_ddpicker">
      <input name="txt_calendar" id="id_calendar" type="text" value="'.$data.'" />
      &nbsp;<a id="id_cal"><img src="images/btn/calendar_2.png" alt="calendar" /></a> </div>';
	
}
function them_search($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{
	$buffer = '<div class="height3"></div>
          		<!-- search result-->
					<div class="search-result">	
						<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
						<img src=';
		
	$product_img  = $path.'product/images/product/'.$data[$i][product_id].'-1.jpg'	;
	
						
	if (!file_exists($product_img) )
	{
		$buffer .= '"'.$path.'images/img_notfound.jpg"';
	}
	else
	{
		$buffer .= '"'.$path.'product/images/index.php?root=product&amp;width=170&amp;name='.trim( $data[$i][product_id].'-1.jpg').'" '	;
	}
	
				
	$buffer .= ' width="150" height="100" alt="" /></a>  
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
							<strong>'.str_len($data[$i][product_name_jp],30).'</strong></a></p>
							
							<p >'.str_len($data[$i][short_desc],145).'</p>
							
							
							<div class="price" >';
							
							
							//var_dump($yen);
				if ( $yen[show] == 1)
				{
							
							
					
					if ($data[$i][price_min] == 0)
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- ( 目安：\\'.'- )</span>';
					}
					else
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～( 目安：\\'.number_format($yen[rate] * $data[$i][price_min]).'～)</span>';
					
					}
					
							
				}
				else
				{
					
					
					if ($data[$i][price_min] == 0)
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- </span>';
					}
					else
					{
						$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～ </span>';
					}
				}
							
							
							
		$buffer .= '</div></div>';
		
		if ($data[$i][rev_status] == '1')
		{
			$buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][product_id].'" class="review">Review</a>';
		}
				
		if ($type == "OPT")		
		{		
			$buffer .= '<ul class="search-result-category">	';
						
						if (is_array($category_ofproduct))
						{
							
							foreach ($category_ofproduct as $text)
							{
								
								$buffer .= '<li>'.$text.'</li>';	
							}
						}
			$buffer .= '</ul>';
		}
		else if ($type == "PKG")
		{
			$buffer .= '<ul class="search-result-category">	';					
			$buffer .= '<li>'.$data[$i][category_name].'</li>';	
			$buffer .= '</ul>';
		}
		
		$buffer .= '</div>
				<!-- search result-->';
				
	return $buffer;
}

function get_jtb_product_url($product_code, $area_code, $country_iso3, $city_iso3)
{
    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $product_url = 'http://www.jtb.co.jp/kaigai_opt/srh/prddetail/';

    $product_id = 'p'.$product_code;
    $area_code = 'areaCd='.$area_code;
    $country_code = 'countryCd='.$db->get_country_iso2($country_iso3);
    $city_code = 'cityCd='.$city_iso3;
    $aff_code = 'aff='.aff_code_by_country($country_iso3);

//    $product_link = $product_url.$product_id.'?'.$area_code.'&'.$country_code.'&'.$city_code.'&'.$aff_code;
    $product_link = $product_url.$product_id.'?'.$area_code.'&'.$country_code.'&'.$aff_code;

    return $product_link;
}

function opt_search($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{
    $product_img  = $path.'product/images/product/'.$data[$i][product_id].'-1.jpg'	;
    if(DEV_ENV){
        $dev_path = '/develop/';
    }else{
        $dev_path = $path;
    }
    $product_detail_link = $dev_path.$location.'/product.php?product_id='.$data[$i][product_id];
    $cell_color = 'style=""';
    if($data[$i]['data_type'] == 'API'){
        $cell_color = 'style="background-color:#F0F8FF;"';
        $area_code = 'ASI';
        if($data[$i]['country_iso3'] == 'AUS' || $data[$i]['country_iso3'] == 'NZL' ){
            $area_code = 'OCE';
        }
        $product_img = $data[$i]['picture_url'];
        $product_detail_link = get_jtb_product_url($data[$i]['product_code'], $area_code, $data[$i]['country_iso3'], $data[$i]['city_iso3']).'" target="_blank;"';
    }

    $product_country_name = $data[$i][country_name_jp];
    $product_name = $data[$i][product_name_jp];
    $product_description = $data[$i][short_desc];
    $product_money_sign = $data[$i][sign];
    $product_price = $data[$i][price_min];


    $buffer = '<div class="ts">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-5">
                            <div class="ts-thumbnail list-img-box">
                                <a href="'; $buffer .= $product_detail_link; $buffer .= '">
                                <!--
                                <div class="row off-hundread">
                                    <div class="col-md-12 col-sm-12 col-xs-9">
                                        <div class="off"><b>本日100%</b> <span>OFF</span><sup class="fa-white fa-question-circle"></sup> </div>
                                        <img src="{$config.documentroot}images/tour-search/off-rect.png" class="img-responsive" alt="kyoto">
                                    </div>
                                    <div class="col-xs-3"></div>
                                </div>
                                -->
                                <img src="'; $buffer .= $product_img;
                                $buffer .= '" class="img-responsive" alt="tour search"></a>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6 col-xs-7"'.$cell_color.">"; $buffer .='
                            <div class="ts-description">
                                <div class="ts-list-cn-name">'; $buffer .= $product_country_name; $buffer .= '</div>
                                <div class="ts-list-title"><a href="'; $buffer .= $product_detail_link; $buffer .= '">'; $buffer .= $product_name; $buffer .= ' </a></div>
                                <div class="ts-list-desc">'; $buffer .= $product_description; $buffer .= '</div>

                                <div class="ts-des-btn">';
                                    foreach($category_ofproduct as $key => $val){
                                          $buffer .= '<div class="btn btn-grey-ts">'.$val.'</div>';
                                    }
                                $buffer .= '</div>

                                <div class="ts-price-mobile">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <h5>　</h5>
                                            <h5>'; $buffer .= $product_money_sign.$product_price; $buffer .= '〜&nbsp;&nbsp;&nbsp;</h5>
                                        </div>
                                        <div class="col-xs-6">
                                            <a href="'; $buffer .= $product_detail_link; $buffer .= '" class="btn btn-blue-ts">';$buffer .= '詳細はこちら</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="ts-prices">
                                <div class="tour-price" style="border-bottom: 1px solid #b5b5b5;">
                                    <h5>　</h5>
                                    <h5>'; $buffer .= $product_money_sign.$product_price; $buffer .= '〜&nbsp;&nbsp;&nbsp;</h5>
                                </div>
                                <div class="tour-price logo" style="">
                                    <div style="height: 10px;"></div>
<!--
                                    <img src="/images/tour-search/clock-md.png" class="img-responsives" alt="tour search">
                                    <span>午前</span>
-->
                                    <div style="height: 12px;"></div>
                                    <a href="'; $buffer .= $product_detail_link; $buffer .= '" class="btn btn-blue-ts">';$buffer .= '詳細はこちら</a>
                                </div>
                                <div class="tour-price tab">
                                    <div style="height: 16px;"></div>
                                    <img src="/images/tour-search/clock-md.png" class="img-responsives" alt="tour search">
                                    <span>午前</span>
                                    <div style="height: 12px;"></div>
                                    <a href="'; $buffer .= $product_detail_link; $buffer .= '" class="btn btn-blue-ts">';$buffer .= '詳細はこちら</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
return $buffer;

/*
					<div class="search-result">
						<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
						<img src=';
*/


    if (!file_exists($product_img) )
    {
        $buffer .= '"'.$path.'images/img_notfound.jpg"';
    }
    else
    {
        $buffer .= '"'.$path.'product/images/index.php?root=product&amp;width=170&amp;name='.trim( $data[$i][product_id].'-1.jpg').'" '	;
    }


    $buffer .= ' width="150" height="100" alt="" /></a>
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$path.$location.'/product.php?product_id='.$data[$i][product_id].'">
							<strong>'.str_len($data[$i][product_name_jp],30).'</strong></a></p>

							<p >'.str_len($data[$i][short_desc],145).'</p>


							<div class="price" >';


    //var_dump($yen);
    if ( $yen[show] == 1)
    {



        if ($data[$i][price_min] == 0)
        {
            $buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- ( 目安：\\'.'- )</span>';
        }
        else
        {
            $buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～( 目安：\\'.number_format($yen[rate] * $data[$i][price_min]).'～)</span>';

        }


    }
    else
    {


        if ($data[$i][price_min] == 0)
        {
            $buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- </span>';
        }
        else
        {
            $buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][price_min]).'～ </span>';
        }
    }



    $buffer .= '</div></div>';

    if ($data[$i][rev_status] == '1')
    {
        $buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][product_id].'" class="review">Review</a>';
    }

    if ($type == "OPT")
    {
        $buffer .= '<ul class="search-result-category">	';

        if (is_array($category_ofproduct))
        {

            foreach ($category_ofproduct as $text)
            {

                $buffer .= '<li>'.$text.'</li>';
            }
        }
        $buffer .= '</ul>';
    }
    else if ($type == "PKG")
    {
        $buffer .= '<ul class="search-result-category">	';
        $buffer .= '<li>'.$data[$i][category_name].'</li>';
        $buffer .= '</ul>';
    }

    $buffer .= '</div>
				<!-- search result-->';

    return $buffer;
}

function days_in_month($month, $year)
{
	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
} 

function date_jp($date)
{
	$arr_date = explode('-',$date);
	$date_jp  = ($arr_date[0]*1).'年'.($arr_date[1]*1).'月'.($arr_date[2]*1).'日'; 
	
	return $date_jp;
}

/* 20130325 */
function them_search_souvenir($data,$i,$category_ofproduct,$path='../',$location,$yen="",$type)
{
    if(DEV_ENV){
        $dev_path = '/develop/';
    }else{
        $dev_path = '';
    }
    $souvenir_detail_link = 'souvenir_product.php?souvenir_id='.$data[$i][souvenir_id];

	$buffer = '<div class="height3"></div>
          		<!-- search result-->
					<div class="search-result">	
						<a href="'.$souvenir_detail_link.'">
						<img src=';
		
	$product_img  = $path.'product/images/souvenir/'.$data[$i][souvenir_id].'-1.jpg';
	
						
	if (!file_exists($product_img) )
	{
		$buffer .= '"'.$path.'images/img_notfound.jpg"';
	}
	else
	{
		$buffer .= '"'.$path.'product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $data[$i][souvenir_id].'-1.jpg').'" '	;
	}
	
				
	$buffer .= ' width="105" height="105" alt="" /></a>  
						<div class="search-result-content">
							<p>'.$data[$i][country_name_jp].'</p>
							<p class="serach-result-tittle">
							<a href="'.$souvenir_detail_link.'">
							<strong>'.str_len($data[$i][souvenir_name],30).'</strong></a></p>
							
							<p >'.str_len($data[$i][short_desc],145).'</p>
							
							
							<div class="price" >';
							
				if ($data[$i]['affiliate']){
					
					
					$buffer .= '<span class="txt-red-bold-price">\\'.number_format($data[$i][souvenir_price],
							(intval($data[$i][souvenir_price])==$data[$i][souvenir_price])?0:2  ).'</span>';
					
				}	
				else{
							
					if ( $yen[show] == 1)
					{
	
						if ($data[$i][souvenir_price] == 0)
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- ( 目安：\\'.'- )</span>';
						}
						else
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][souvenir_price] , 
																											  
							(intval($data[$i][souvenir_price])==$data[$i][souvenir_price])?0:2    ).' ( 目安：\\'.number_format($yen[rate] * $data[$i][souvenir_price]).')</span>';
						
						}
						
								
					}
					else
					{
						
						if ($data[$i][souvenir_price] == 0)
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].'- </span>';
						}
						else
						{
							$buffer .= '<span class="txt-red-bold-price">'.$yen[sign].number_format($data[$i][souvenir_price],
								(intval($data[$i][souvenir_price])==$data[$i][souvenir_price])?0:2																			  
																											  
																							).' </span>';
						}
					}
							
				}
							
		$buffer .= '</div></div>';
		
		if ($data[$i][rev_status] == '1')
		{
			$buffer .= '<a href="'.$path.$location.'/review.php?product_id='.$data[$i][souvenir_id].'" class="review">Review</a>';
		}
				
		if ($type == "OPT")		
		{		
			$buffer .= '<ul class="search-result-category">	';
						
						if ($data[$i][store_type] == "1"){
							$buffer .= '<li>現地受取</li>';
						}
						else if ($data[$i][store_type] == "2"){
							$buffer .= '<li>日本受取</li>';
						}
						
						
						if (is_array($category_ofproduct))
						{
							
							foreach ($category_ofproduct as $text)
							{
								
								$buffer .= '<li>'.$text.'</li>';	
							}
						}
						
						
						if ($data[$i][status_book] == "1"){
							$buffer .= '<li>カード決済</li>';
						}
						else if ($data[$i][status_book] == "2"){
							$buffer .= '<li>現地支払</li>';
						}
						
						
						
			$buffer .= '</ul>';
		}
		else if ($type == "PKG")
		{
			$buffer .= '<ul class="search-result-category">	';					
			$buffer .= '<li>'.$data[$i][souvenir_theme].'</li>';	
			$buffer .= '</ul>';
		}
		
		$buffer .= '</div>
				<!-- search result-->';
				
	return $buffer;
}

function check_date($yy,$mm,$dd){
	$icheck = 0;
	
	if (!$yy){
		$icheck++;
	}
	
	if (!$mm){
		$icheck++;
	}
	
	if (!$dd){
		$icheck++;
	}
	
	
	
	if ($yy < 1000){
		$icheck++;
		
	}
	
	
	if ($mm > 13){
		$icheck++;
	}
	
	if ($mm > 31){
		$icheck++;
	}
	
	
	
	if ($icheck){
		return '0000-00-00';	
	}
	else{
		
		if ($mm < 10){
			$mm = '0'.$mm;	
		}
		
		if ($dd < 10){
			$dd = '0'.$dd;	
		}
		
		return $yy.'-'.$mm.'-'.$dd;	
	}
}


function check_time($hh,$mm){
	if ( is_numeric($hh) && is_numeric($mm) ){
		
		if ( ( $hh >= 0 ) && ( $hh <= 23 ) ){
			
			if ( ($mm >= 0) && ($mm <= 59 ) ){
				
				if ($hh < 10){
					$hh = '0'.$hh;	
				}
				
				if ($mm < 10){
					$mm = '0'.$mm;	
				}
				
				return $hh.':'.$mm;
				
			}
			else{
				return '00:00';	
			}
			
		}
		else{
			return '00:00';	
		}
		
	}
	else{
		return '00:00';	
	}
}


function email_by_country($iso3){

	switch($iso3)
	{
		case 'TWN' :  $address_mail = 'stc_trading.tw@jtbap.com'; break;	
		
		case 'IDN' :  $address_mail = 'reservation_jtbbali.id@jtbap.com'; break;
		
		case 'THA' :  $address_mail = 'bkk_mybus.th@jtbap.com'; break;	
		
		case 'VNM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'KHM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;
		
		case 'CAM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;	
		
		case 'MYS' :  $address_mail = 'mybus.my@jtbap.com'; break;
		 
		case 'SGP' :  $address_mail = 'orchard.sg@jtbap.com'; break;	
		
		case 'HKG' :  $address_mail = 'inbound@hk.jtb.cn';  break;
		
		case 'AUS' :  $address_mail = 'travelbox.au@jtbap.com'; break;	
		
		case 'NZL' :  $address_mail = 'webnz.nz@jtbap.com'; break;			
	}
	
	return trim($address_mail);
}
function opt_email_by_country($iso3, $city_iso3 = null){

    switch($iso3)
    {
        case 'TWN' :  $address_mail = 'option_yoyaku.tw@jtbap.com'; break;

        case 'IDN' :  $address_mail = 'reservation_jtbbali.id@jtbap.com'; break;

        case 'THA' : if ($city_iso3 == 'HKT')
        {
            $address_mail = 'toiawase_phuket.th@jtbap.com';
        }
        else
        {
            $address_mail = 'reservation_mybus.th@jtbap.com';
        }
            break;

        case 'VNM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;

        case 'KHM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;

        case 'CAM' :  $address_mail = 'lookdesk.vn@jtbap.com'; break;

        case 'MYS' :  $address_mail = 'mybus.my@jtbap.com'; break;

        case 'SGP' :  $address_mail = 'orchard.sg@jtbap.com'; break;

        case 'HKG' :  $address_mail = 'inbound@hk.jtb.cn';  break;

        case 'AUS' :  $address_mail = 'isdweb.au@jtbap.com'; break;

        case 'NZL' :  $address_mail = 'webnz.nz@jtbap.com'; break;
    }

    return trim($address_mail);
}
function aff_code_by_country($iso3){

    switch($iso3)
    {
        case 'TWN' :  $aff_code = 'AP_TW001'; break;

        case 'IDN' :  $aff_code = 'AP_ID001'; break;

        case 'THA' :  $aff_code = 'AP_TH001'; break;

        case 'VNM' :  $aff_code = 'AP_VN001'; break;

        case 'KHM' :  $aff_code = 'AP_KH001'; break;

        case 'MYS' :  $aff_code = 'AP_MY001'; break;

        case 'SGP' :  $aff_code = 'AP_SG001'; break;

        case 'AUS' :  $aff_code = 'AP_AU001'; break;

        case 'NZL' :  $aff_code = 'AP_NZ001'; break;

        case 'PHL' :  $aff_code = 'AP_PH001'; break;

        case 'MMR' :  $aff_code = 'AP_MM001'; break;
    }

    return $aff_code;
}

function signature_by_country_souvenir($iso3){
	switch( $iso3)
		{
			case 'TWN' : $mail_tempage = '============================================
											エスティーシー商事（JTB台湾グループ）
											9:00～17：30　（台湾時間/土・日・祝を除く）
                                            Tel：886-2-2581-3268
											<a target="_blank" href="http://www.mybus-asia/taiwan">http://www.mybus-asia/taiwan</a>
										  =============================================
											'; break;	
														
			  case 'THA' : $mail_tempage = '=============================================
											JTB (Thailand) Ltd （ジェイティービー　バンコク支店）
											Mybus Operation
											Silom Complex Branch 5th FL.,Room 506-507 
											191 Silom Rd.,Silom Khet Bangrak, Bangkok 10500 Thailand
											Tel: タイ国内からおかけの場合 (0)22300490
											タイ国外からおかけの場合 +6622300490
											Fax: (0)2344-4678
											<a target="_blank" href="http://www.mybus-asia.com/thailand/">http://www.mybus-asia.com/thailand/</a>
											=============================================
																						'; break;	
														
				case 'VNM' : $mail_tempage = '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
												
												case 'KHM' : $mail_tempage .= '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
												
												
				case 'CAM' : $mail_tempage = '－－－－－－－－－－－－－－－－－－－－－－－－
											マイバスデスク・ホーチミン
											9 Dong Khoi St, District1, Ho Chi Minh City,Vietnam .
											Tel: 84-8-3827-7493
											Fax: 84-8-3824-6515 
											ーーーーーーーーーーーーーーーーーーーーーーーーーー'; break;	
													
				case 'MYS' : $mail_tempage = '====================================
											JTB (M) SDN BHD （ジェイティービー　マレーシア支店）
												
											ＫＬオフィス　(メリアホテル横、AMODAビル16階）
											16.05-16.07 Level 16, Amoda Bldg.
											22 Jalan Imbi 55100 Kuala Lumpur
											Malaysia 
											Tel: +(603) 2142-8727
											Fax:+(603) 2142-5344
													
											ランカウイ：＋（604）966-4708
											ペナン　　：＋（604）263-5788
											コタキナバル：＋（6088）259-668
											http://www.mybus-asia.com/malaysia/
											======================================'; break;
											
				case 'IDN' : $mail_tempage = '============================================
											PT. JTB INDONESIA （JTBバリ支店）
											
											Jl. Bypass Ngurah Rai No.88
											Kelan Abian,Tuban, Kuta, 
											Bali 80362 - Indonesia
											マイバスほっとダイヤル：0361-708555（バリ島内 / 09：00～19：00）
											緊急連絡先：081-2382-9139, 081-23802511（上記時間外）
											WEBサイト：http://www.mybus-asia.com/indonesia/
											=============================================
											●バリ島でのお客様用カウンターデスク
											・DFSギャラリア・バリ2階 （営業時間 10：00～18：00）
											・グランド・ハイアット・バリ内 （営業時間 08：00～20：00）
											●マイバスはアジアと豪州9ヵ国 （香港（マカオ）、台湾、タイ（プーケット）、マレーシア、シンガポール、バリ、ベトナム、オーストラリア、ニュージーランド）で年中無休で運行中。
											';
											break;	
														
				case 'SGP' : $mail_tempage = '===============================
											JTB Pte Ltd Singapore
											Tel: (65)-6305-5050
											08:45-17:45(月～金) シンガポール時間
											e-mail: orchard.sg@jtbap.com
											================================'; 
												break;	
														
				case 'HKG' : $mail_tempage =  '============================================ 
											MY BUS (HONG KONG) LTD. （マイバス香港　日本語定期観光） 
											Add     : Suites 710, 7th Fl., Whalf T&T Centre, 7 Canton Rd., 
												  　　Tsim Sha Tsui, Kowloon, Hong Kong. 
											Tel      : (852)2375-7576 (*日本語対応　年中無休09:00-18:00)
											Fax     : (852)2311-5803 
											Mail    : inbound@hk.jtb.cn 
											Web    : <a target=_blank" href="http://www.mybus-asia.com/hongkong/">http://www.mybus-asia.com/hongkong/</a>
											============================================= 
											●”マイバス香港”は、 
											　ビギナーからリピーター向けオプショナルツアー（44コース）、 
											　便利で安心のスパ・マッサージ（8店舗）、 
											　あの憧れのレストランをはじめ厳選のミールクーポン（43コース）、 
											　割引特典レストラン（3店舗）をご用意しています。 
											●マイバスはアジア7カ国8都市（香港、台湾、タイ、マレーシア、シンガポール、 
											　バリ、プーケット、ベトナム）で年中無休で運行中。 '; break;
												
				case 'AUS' : $mail_tempage =  '===========================================
											JTB Australia Pty Ltd
													
											ケアンズ マイバスデスク
											電話　+61-(07)-4052-5872 
											毎日 9：00～19：00
											61　Abbott　Street, Cairns
												
											ゴールドコーストマイバスデスク
											電話　+61-(07)-5592-9491 
											毎日 8：30～18：30 
											Ground Floor, 3191 Surfers Paradise Boulevard
													
											シドニー
											電話　+61-(02)-9510-0313 
											毎日 09:00 ～ 17:00
													
											メルボルン
											電話　+61(03)-8623-0091 
											土日祝日を除く毎日 10:00 ～12:00/14:00 ～16:00 
											Level 6, 10 Artemis Lane, Melbourne
											(QVショッピングコンプレックス内) 
													
											パース
											電話　+61(08)-9218-9866 
											毎日 09:00 ～ 12:00/13:00 ～ 17:30
											Ground Floor, 273 Hay Street, East Perth
											travelbox.au@jtbap.com
											============================================'; break;	
														
				case 'NZL' : $mail_tempage =  '===================================================================
											JTB New Zealand Ltd（ジェイティービー　ニュージーランド　オークランド支店）
											Auckland Mybus Centre
											Level 5, 191 Queen St, Auckland
											Tel: +(64)-9379-6415 
											Fax:+(64)-9309-7699
											<a target=_blank" href="http://www.jtb.co.nz/">http://www.jtb.co.nz/</a>
											===================================================================
											●マイバスセンター　オークランド支店は平日　9:00～17:30の営業です。
											（土日祝日はお休みとなります。）'; break;			
		}
		
		return nl2br($mail_tempage);
}

function fetch_sales_ranking($country_iso3, $path, $ranking_cnt){

    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $product_path = '../'.$db->showpath_bycountry($country_iso3).'/product.php?product_id=';
    $image_path = $path.$db->showpath_bycountry($country_iso3).'/images/ranking/';
    $mba_img_path  = '../product/images/product/';

    //In case of AUS
    if($country_iso3 == "AUS"){
        $sales_ranking[0]['id']           = 13;
        $sales_ranking[1]['id']           = 4;
        $sales_ranking[2]['id']           = 90;
        $sales_ranking[3]['id']           = 216;
        $sales_ranking[0]['product_name'] = 'グリーン島ディスカバリー(ランチ付き)';
        $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
        $sales_ranking[0]['product_link'] = 'http://www.jtb.com.au/tour/cairns/detail.php?id=13';
        $sales_ranking[1]['product_name'] = '選べる世界遺産ブルーマウンテンズ1日観光';
        $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
        $sales_ranking[1]['product_link'] = 'http://www.jtb.com.au/tour/sydney/detail.php?id=4';
        $sales_ranking[2]['product_name'] = 'カランビンワイルドライフサンクチュアリー';
        $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
        $sales_ranking[2]['product_link'] = 'http://www.jtb.com.au/tour/goldcoast/detail.php?id=90';
        $sales_ranking[3]['product_name'] = 'アウトバックコネクション';
        $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
        $sales_ranking[3]['product_link'] = 'http://www.jtb.com.au/tour/ayersrock/detail.php?id=216';

        return $sales_ranking;
    }
    //Except for AUS

    $result = $db->fetch_sales_ranking($country_iso3);

    $show_list = $result['product_list'];

    $product_data = explode(',',$show_list);

    $i = 0;
    foreach($product_data as $val){
        $product_ids = explode(':',$val);
        $product_id[$i]['product_id'] = $product_ids[0];
        $product_id[$i]['product_code'] = $product_ids[1];
        $i++;
    }
    $i = 0;
    $ranking_list = array();
    foreach($product_id as $val){
        $sql = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
         FROM mbus_product a WHERE country_iso3 = "'.$country_iso3.'" AND public = 1 AND product_id = "'.$val["product_id"].'" AND product_code = "'.$val["product_code"].'"';
        $result = $db->db_query($sql);

        while ($record  = mysql_fetch_array($result)){
            $product_list[$i] = $record;
        }
        if(!empty($product_list[$i])){
            $ranking_list[$i] = $product_list[$i];
            if($product_list[$i]['data_type'] == 'API'){
                $ranking_list[$i]['data_origin'] = 'API';
            }else{
                $ranking_list[$i]['data_origin'] = 'MBA';
            }
        }else{
            $sql2 = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
             FROM mbus_product a WHERE country_iso3 = "'.$country_iso3.'" AND public = 1 AND product_code = "'.$val["product_code"].'"';
            $result2 = $db->db_query($sql2);

            while ($record2  = mysql_fetch_array($result2)){
                $product_list[$i] = $record2;
            }
            if(!empty($product_list[$i])){
                $ranking_list[$i] = $product_list[$i];
                $ranking_list[$i]['data_origin'] = 'API';
            }else{
                $ranking_list[$i] = null;
            }
        }
        $i++;
    }

    $i = 0;
    foreach($ranking_list as $val){
        if(!empty($val)){
            if($val['data_origin'] == 'API'){
                $sales_ranking[$i]['api_flg']      = 1;
            }else{
                $sales_ranking[$i]['api_flg']      = 0;
            }
            $sales_ranking[$i]['id'] = $val['product_id'];
            $sales_ranking[$i]['link_type']    = ($sales_ranking[$i]['api_flg'] == 1) ? 'target="_blank"' : '';
            $sales_ranking[$i]['product_name'] = $val['product_name_jp'];
            $sales_ranking[$i]['product_picture'] = ($sales_ranking[$i]['api_flg'] == 1) ? $val['picture_url'] : $mba_img_path.$sales_ranking[$i]['id'].'-1.jpg';
            $sales_ranking[$i]['product_link'] = ($sales_ranking[$i]['api_flg'] == 1) ? get_jtb_product_url($val['product_code'],'',$country_iso3,'') : $product_path.$sales_ranking[$i]['id'];
            $i++;
        }
    }
    array_splice($sales_ranking, $ranking_cnt);
    return $sales_ranking;
    /*

        switch($country_iso3){
            case 'SGP' :
                $sales_ranking[0]['id']           = 248433;
                $sales_ranking[1]['id']           = 1293;
                $sales_ranking[2]['id']           = 248433;
                $sales_ranking[3]['id']           = 268168;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 0;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = '日本語トラム利用ナイトサファリ（往復送迎/食事つき）';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,'') : $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = $sales_ranking[1]['product_data']['product_name_jp'];
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = '日本語トラム利用ナイトサファリ（往復送迎/夕食なし）';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'シンガポールフライヤーにも乗車！シンガポール魅力満載市内観光（飲茶ランチつき！！）';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'IDN' :
                $sales_ranking[0]['id']           = 200002676;
                $sales_ranking[1]['id']           = 308241;
                $sales_ranking[2]['id']           = 308228;
                $sales_ranking[3]['id']           = 308562;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 1;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = 'よくばり！ツアー';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = 'パサールセンゴルディナー';
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = 'インドネシアンディナー ブンブバリ';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'レンボンガン島deマングローブ＆シュノーケリングツアー';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'AUS' :
                $sales_ranking[0]['id']           = 13;
                $sales_ranking[1]['id']           = 4;
                $sales_ranking[2]['id']           = 90;
                $sales_ranking[3]['id']           = 216;
                $sales_ranking[0]['product_name'] = 'グリーン島ディスカバリー(ランチ付き)';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = 'http://www.jtb.com.au/tour/cairns/detail.php?id=13';
                $sales_ranking[1]['product_name'] = '選べる世界遺産ブルーマウンテンズ1日観光';
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = 'http://www.jtb.com.au/tour/sydney/detail.php?id=4';
                $sales_ranking[2]['product_name'] = 'カランビンワイルドライフサンクチュアリー';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = 'http://www.jtb.com.au/tour/goldcoast/detail.php?id=90';
                $sales_ranking[3]['product_name'] = 'アウトバックコネクション';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = 'http://www.jtb.com.au/tour/ayersrock/detail.php?id=216';
                break;
            case 'NZL' :
                $sales_ranking[0]['id']           = 288527;
                $sales_ranking[1]['id']           = 2061;
                $sales_ranking[2]['id']           = 288274;
                $sales_ranking[3]['id']           = 288276;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 0;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = 'ミルフォードサウンド1日観光';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = $sales_ranking[1]['product_data']['product_name_jp'];
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = 'オークランド夜景＆ディナー';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'ホエール（鯨）/イルカウォッチングツアー';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'THA' :
                $sales_ranking[0]['id']           = 200002702;
                $sales_ranking[1]['id']           = 605;
                $sales_ranking[2]['id']           = 200002695;
                $sales_ranking[3]['id']           = 200002829;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 0;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = '世界遺産アユタヤ観光ツアー';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = $sales_ranking[1]['product_data']['product_name_jp'];
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = 'バンコク市内観光(午前・午後）';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'マイバスガイド付き専用車プラン バンコク市内/郊外';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'VNM' :
                $sales_ranking[0]['id']           = 311554;
                $sales_ranking[1]['id']           = 311614;
                $sales_ranking[2]['id']           = 311414;
                $sales_ranking[3]['id']           = 311598;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 1;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = '世界遺産ハロン湾クルーズ１日観光';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = '悠久の大河メコン川とミトー1日観光';
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = 'クチの地下トンネル観光';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'ホーチミン半日市内観光（午前発／午後発）';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'KHM' :
                $sales_ranking[0]['id']           = 311456;
                $sales_ranking[1]['id']           = 311465;
                $sales_ranking[2]['id']           = 311468;
                $sales_ranking[3]['id']           = 311462;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 1;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = 'トンレサップ湖クルーズツアー（午前発／午後発）';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = '雄大！世界遺産アンコールワットの朝日鑑賞';
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = '定番！世界遺産 アンコールトム＆アンコールワット1日観光';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'ベンメリア遺跡観光（午前発／午後発）';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'MYS' :
                $sales_ranking[0]['id']           = 200002518;
                $sales_ranking[1]['id']           = 207623;
                $sales_ranking[2]['id']           = 160585;
                $sales_ranking[3]['id']           = 192556;
                $sales_ranking[0]['api_flg']      = 1;
                $sales_ranking[1]['api_flg']      = 1;
                $sales_ranking[2]['api_flg']      = 1;
                $sales_ranking[3]['api_flg']      = 1;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['link_type']    = ($sales_ranking[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[1]['link_type']    = ($sales_ranking[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[2]['link_type']    = ($sales_ranking[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[3]['link_type']    = ($sales_ranking[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $sales_ranking[0]['product_name'] = 'クアラルンプール市内観光（ショッピングなし）';
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = ($sales_ranking[0]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[0]['id'],'',$country_iso3,''): $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = 'ガイド専用車チャーター';
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = ($sales_ranking[1]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[1]['id'],'',$country_iso3,''): $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = 'カヤック DE ジャングル';
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = ($sales_ranking[2]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[2]['id'],'',$country_iso3,''): $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = 'マングローブリバークルーズとホタルツアー';
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = ($sales_ranking[3]['api_flg'] == 1) ? get_jtb_product_url($sales_ranking[3]['id'],'',$country_iso3,''): $product_path.$sales_ranking[3]['id'];
                break;
            case 'TWN' :
                $sales_ranking[0]['id']           = 870;
                $sales_ranking[1]['id']           = 506;
                $sales_ranking[2]['id']           = 531;
                $sales_ranking[3]['id']           = 524;
                $sales_ranking[0]['product_data'] = $db->view_product($sales_ranking[0]['id']);
                $sales_ranking[1]['product_data'] = $db->view_product($sales_ranking[1]['id']);
                $sales_ranking[2]['product_data'] = $db->view_product($sales_ranking[2]['id']);
                $sales_ranking[3]['product_data'] = $db->view_product($sales_ranking[3]['id']);
                $sales_ranking[0]['product_name'] = $sales_ranking[0]['product_data']['product_name_jp'];
                $sales_ranking[0]['product_picture'] = $image_path.$sales_ranking[0]['id'].'.jpg';
                $sales_ranking[0]['product_link'] = $product_path.$sales_ranking[0]['id'];
                $sales_ranking[1]['product_name'] = $sales_ranking[1]['product_data']['product_name_jp'];
                $sales_ranking[1]['product_picture'] = $image_path.$sales_ranking[1]['id'].'.jpg';
                $sales_ranking[1]['product_link'] = $product_path.$sales_ranking[1]['id'];
                $sales_ranking[2]['product_name'] = $sales_ranking[2]['product_data']['product_name_jp'];
                $sales_ranking[2]['product_picture'] = $image_path.$sales_ranking[2]['id'].'.jpg';
                $sales_ranking[2]['product_link'] = $product_path.$sales_ranking[2]['id'];
                $sales_ranking[3]['product_name'] = $sales_ranking[3]['product_data']['product_name_jp'];
                $sales_ranking[3]['product_picture'] = $image_path.$sales_ranking[3]['id'].'.jpg';
                $sales_ranking[3]['product_link'] = $product_path.$sales_ranking[3]['id'];
                break;
        }
*/
        return $sales_ranking;
    }

    function fetch_recommend_tour($country_iso3, $path, $ranking_cnt){

        $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
        $db->db_connect();

        $product_path = '../'.$db->showpath_bycountry($country_iso3).'/product.php?product_id=';
        $image_path = $path.$db->showpath_bycountry($country_iso3).'/images/recommend/';
        $mba_img_path  = '../product/images/product/';

            //In case of AUS
            if($country_iso3 == "AUS"){
                $recommend_tour[0]['id']           = 64;
                $recommend_tour[1]['id']           = 26;
                $recommend_tour[2]['id']           = 7;
                $recommend_tour[3]['id']           = 132;
                $recommend_tour[0]['product_name'] = 'キュランダ1日観光デラックスコース';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = 'http://www.jtb.com.au/tour/cairns/detail.php?id=64';
                $recommend_tour[1]['product_name'] = '(IEC)ポートスティーブンス【海】エコツアー';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = 'http://www.jtb.com.au/tour/sydney/detail.php?id=26';
                $recommend_tour[2]['product_name'] = '世界遺産・土ボタルツアー【日本語/英語】';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = 'http://www.jtb.com.au/tour/goldcoast/detail.php?id=7';
                $recommend_tour[3]['product_name'] = 'シーニックフライト【英語ガイド】';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = 'http://www.jtb.com.au/tour/ayersrock/detail.php?id=132';

                return $recommend_tour;
            }
            //Except for AUS

            $result = $db->fetch_recommend_ranking($country_iso3);

            $show_list = $result['product_list'];

            $product_data = explode(',',$show_list);

            $i = 0;
            foreach($product_data as $val){
                $product_ids = explode(':',$val);
                $product_id[$i]['product_id'] = $product_ids[0];
                $product_id[$i]['product_code'] = $product_ids[1];
                $i++;
            }
            $i = 0;
            $recommend_list = array();
            foreach($product_id as $val){
                $sql = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
                 FROM mbus_product a WHERE country_iso3 = "'.$country_iso3.'" AND public = 1 AND product_id = "'.$val["product_id"].'" AND product_code = "'.$val["product_code"].'"';
                $result = $db->db_query($sql);

                while ($record  = mysql_fetch_array($result)){
                    $product_list[$i] = $record;
                }
                if(!empty($product_list[$i])){
                    $recommend_list[$i] = $product_list[$i];
                    if($product_list[$i]['data_type'] == 'API'){
                        $recommend_list[$i]['data_origin'] = 'API';
                    }else{
                        $recommend_list[$i]['data_origin'] = 'MBA';
                    }
                }else{
                    $sql2 = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
                     FROM mbus_product a WHERE country_iso3 = "'.$country_iso3.'" AND public = 1 AND product_code = "'.$val["product_code"].'"';
                    $result2 = $db->db_query($sql2);

                    while ($record2  = mysql_fetch_array($result2)){
                        $product_list[$i] = $record2;
                    }
                    if(!empty($product_list[$i])){
                        $recommend_list[$i] = $product_list[$i];
                        $recommend_list[$i]['data_origin'] = 'API';
                    }else{
                        $recommend_list[$i] = null;
                    }
                }
                $i++;
            }

            $i = 0;
            foreach($recommend_list as $val){
                if(!empty($val)){
                    if($val['data_origin'] == 'API'){
                        $recommend_tour[$i]['api_flg']      = 1;
                    }else{
                        $recommend_tour[$i]['api_flg']      = 0;
                    }
                    $recommend_tour[$i]['id'] = $val['product_id'];
                    $recommend_tour[$i]['link_type']    = ($recommend_tour[$i]['api_flg'] == 1) ? 'target="_blank"' : '';
                    $recommend_tour[$i]['product_name'] = $val['product_name_jp'];
                    $recommend_tour[$i]['product_picture'] = ($recommend_tour[$i]['api_flg'] == 1) ? $val['picture_url'] : $mba_img_path.$recommend_tour[$i]['id'].'-1.jpg';
                    $recommend_tour[$i]['product_link'] = ($recommend_tour[$i]['api_flg'] == 1) ? get_jtb_product_url($val['product_code'],'',$country_iso3,'') : $product_path.$recommend_tour[$i]['id'];
                    $i++;
                }
            }
            array_splice($recommend_tour, $ranking_cnt);
            return $recommend_tour;

            $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
            $db->db_connect();

            $product_path = '../'.$db->showpath_bycountry($iso3).'/product.php?product_id=';
            $image_path = $path.$db->showpath_bycountry($iso3).'/images/recommend/';

        /*

        switch($country_iso3){
            case 'SGP' :
                $recommend_tour[0]['id']           = 236469;
                $recommend_tour[1]['id']           = 268168;
                $recommend_tour[2]['id']           = 200000025;
                $recommend_tour[3]['id']           = 289462;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'ナイトセントーサ＆音と水と光のショー';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = 'シンガポールフライヤーにも乗車！シンガポール魅力満載市内観光（飲茶ランチつき！！）';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'マイバス インドネシア･ビンタン島へ行く';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = 'マレーシア・ジョホールバル観光';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'IDN' :
                $recommend_tour[0]['id']           = 200003353;
                $recommend_tour[1]['id']           = 2203;
                $recommend_tour[2]['id']           = 200001054;
                $recommend_tour[3]['id']           = 200003352;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 0;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'ウブドでお散歩、タマンアユン＆タナロット寺院観光';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = $recommend_tour[1]['product_data']['product_name_jp'];;
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'ニュークタゴルフ';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = '帰国日プラン フリータイム、マッサージ＆デイナー';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'AUS' :
                $recommend_tour[0]['id']           = 64;
                $recommend_tour[1]['id']           = 26;
                $recommend_tour[2]['id']           = 7;
                $recommend_tour[3]['id']           = 132;
                $recommend_tour[0]['product_name'] = 'キュランダ1日観光デラックスコース';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = 'http://www.jtb.com.au/tour/cairns/detail.php?id=64';
                $recommend_tour[1]['product_name'] = '(IEC)ポートスティーブンス【海】エコツアー';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = 'http://www.jtb.com.au/tour/sydney/detail.php?id=26';
                $recommend_tour[2]['product_name'] = '世界遺産・土ボタルツアー【日本語/英語】';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = 'http://www.jtb.com.au/tour/goldcoast/detail.php?id=7';
                $recommend_tour[3]['product_name'] = 'シーニックフライト【英語ガイド】';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = 'http://www.jtb.com.au/tour/ayersrock/detail.php?id=132';
                break;
            case 'NZL' :
                $recommend_tour[0]['id']           = 288274;
                $recommend_tour[1]['id']           = 288258;
                $recommend_tour[2]['id']           = 288536;
                $recommend_tour[3]['id']           = 288260;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'オークランド夜景＆ディナー';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = 'ワイトモ・ロトルア1日観光';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'ダウトフルサウンド１日観光';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = 'ベイオブアイランド  1日観光';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'THA' :
                $recommend_tour[0]['id']           = 233114;
                $recommend_tour[1]['id']           = 200002710;
                $recommend_tour[2]['id']           = 220174;
                $recommend_tour[3]['id']           = 200002709;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'コーラルアイランド１日観光とエレファントトレッキング';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = 'クルーズでアユタヤ観光';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'パンガー湾巡り';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = '色々見タイ！ニューハーフショー（ノパラット古典舞踊  &  マンボーショー）';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'VNM' :
                $recommend_tour[0]['id']           = 311606;
                $recommend_tour[1]['id']           = 311632;
                $recommend_tour[2]['id']           = 311655;
                $recommend_tour[3]['id']           = 200002644;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = '寺院巡りと精進料理（午前・午後）';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = 'チョロン（中華街）観光';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'サイゴンリバー・デイナークルーズ';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = '＜ダナン発＞午後から夜までホイアン観光';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'KHM' :
                $recommend_tour[0]['id']           = 311456;
                $recommend_tour[1]['id']           = 311462;
                $recommend_tour[2]['id']           = 311463;
                $recommend_tour[3]['id']           = 311468;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'トンレサップ湖クルーズツアー（午前発／午後発）';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = 'ベンメリア遺跡観光（午前発／午後発）';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = 'バンテアイスレイ遺跡とアキラの地雷博物館（午前発／午後発）';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = '定番！世界遺産 アンコールトム＆アンコールワット1日観光';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'MYS' :
                $recommend_tour[0]['id']           = 207625;
                $recommend_tour[1]['id']           = 206366;
                $recommend_tour[2]['id']           = 160461;
                $recommend_tour[3]['id']           = 200002518;
                $recommend_tour[0]['api_flg']      = 1;
                $recommend_tour[1]['api_flg']      = 1;
                $recommend_tour[2]['api_flg']      = 1;
                $recommend_tour[3]['api_flg']      = 1;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['link_type']    = ($recommend_tour[0]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[1]['link_type']    = ($recommend_tour[1]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[2]['link_type']    = ($recommend_tour[2]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[3]['link_type']    = ($recommend_tour[3]['api_flg'] == 1) ? 'target="_blank"' : '';
                $recommend_tour[0]['product_name'] = 'オランウータン・サンクチュアリ';
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = ($recommend_tour[0]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[0]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = '世界遺産キナバル公園とキャノピーウォーク';
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = ($recommend_tour[1]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[1]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = '世界遺産マラッカと名物ニョニャ料理';
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = ($recommend_tour[2]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[2]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = 'クアラルンプール市内観光（ショッピングなし）';
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = ($recommend_tour[3]['api_flg'] == 1) ? get_jtb_product_url($recommend_tour[3]['id'],'',$country_iso3,'') : $product_path.$recommend_tour[3]['id'];
                break;
            case 'TWN' :
                $recommend_tour[0]['id']           = 505;
                $recommend_tour[1]['id']           = 1936;
                $recommend_tour[2]['id']           = 1272;
                $recommend_tour[3]['id']           = 506;
                $recommend_tour[0]['product_data'] = $db->view_product($recommend_tour[0]['id']);
                $recommend_tour[1]['product_data'] = $db->view_product($recommend_tour[1]['id']);
                $recommend_tour[2]['product_data'] = $db->view_product($recommend_tour[2]['id']);
                $recommend_tour[3]['product_data'] = $db->view_product($recommend_tour[3]['id']);
                $recommend_tour[0]['product_name'] = $recommend_tour[0]['product_data']['product_name_jp'];
                $recommend_tour[0]['product_picture'] = $image_path.$recommend_tour[0]['id'].'.jpg';
                $recommend_tour[0]['product_link'] = $product_path.$recommend_tour[0]['id'];
                $recommend_tour[1]['product_name'] = $recommend_tour[1]['product_data']['product_name_jp'];
                $recommend_tour[1]['product_picture'] = $image_path.$recommend_tour[1]['id'].'.jpg';
                $recommend_tour[1]['product_link'] = $product_path.$recommend_tour[1]['id'];
                $recommend_tour[2]['product_name'] = $recommend_tour[2]['product_data']['product_name_jp'];
                $recommend_tour[2]['product_picture'] = $image_path.$recommend_tour[2]['id'].'.jpg';
                $recommend_tour[2]['product_link'] = $product_path.$recommend_tour[2]['id'];
                $recommend_tour[3]['product_name'] = $recommend_tour[3]['product_data']['product_name_jp'];
                $recommend_tour[3]['product_picture'] = $image_path.$recommend_tour[3]['id'].'.jpg';
                $recommend_tour[3]['product_link'] = $product_path.$recommend_tour[3]['id'];
                break;
        }
*/
    return $recommend_tour;
}
function fetch_recommend_souvenir($iso3, $path){
    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $souvenir_path = '../'.$db->showpath_bycountry($iso3).'/souvenir_product.php?souvenir_id=';
    $image_path = $path.$db->showpath_bycountry($iso3).'/images/recommend_souvenir/';
    switch($iso3){
        case 'SGP' :
            $recommend_souvenir[0]['id']           = 407;
            $recommend_souvenir[1]['id']           = 847;
            $recommend_souvenir[2]['id']           = 410;
	        $recommend_souvenir[3]['id']           = 994;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
	        $recommend_souvenir[3]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[3]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
	        $recommend_souvenir[3]['souvenir_name'] = $recommend_souvenir[3]['souvenir_data'][0]['souvenir_name'];
	        $recommend_souvenir[3]['souvenir_picture'] = $image_path.$recommend_souvenir[3]['id'].'.jpg';
	        $recommend_souvenir[3]['souvenir_link'] = $souvenir_path.$recommend_souvenir[3]['id'];
            break;
        case 'IDN' :
            $recommend_souvenir[0]['id']           = 933;
            $recommend_souvenir[1]['id']           = 934;
            $recommend_souvenir[2]['id']           = 948;
            $recommend_souvenir[3]['id']           = 939;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[3]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[3]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            $recommend_souvenir[3]['souvenir_name'] = $recommend_souvenir[3]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[3]['souvenir_picture'] = $image_path.$recommend_souvenir[3]['id'].'.jpg';
            $recommend_souvenir[3]['souvenir_link'] = $souvenir_path.$recommend_souvenir[3]['id'];
            break;
        case 'AUS' :
            $recommend_souvenir[0]['id']           = 859;
            $recommend_souvenir[1]['id']           = 860;
            $recommend_souvenir[2]['id']           = 861;
            $recommend_souvenir[3]['id']           = 863;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[3]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[3]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            $recommend_souvenir[3]['souvenir_name'] = $recommend_souvenir[3]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[3]['souvenir_picture'] = $image_path.$recommend_souvenir[3]['id'].'.jpg';
            $recommend_souvenir[3]['souvenir_link'] = $souvenir_path.$recommend_souvenir[3]['id'];
            break;
        case 'NZL' :
            $recommend_souvenir[0]['id']           = 1002;
            $recommend_souvenir[1]['id']           = 998;
            $recommend_souvenir[2]['id']           = 999;
            $recommend_souvenir[3]['id']           = 1000;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[3]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[3]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            $recommend_souvenir[3]['souvenir_name'] = $recommend_souvenir[3]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[3]['souvenir_picture'] = $image_path.$recommend_souvenir[3]['id'].'.jpg';
            $recommend_souvenir[3]['souvenir_link'] = $souvenir_path.$recommend_souvenir[3]['id'];
            break;
        case 'THA' :
            $recommend_souvenir[0]['id']           = 416;
            $recommend_souvenir[1]['id']           = 418;
            $recommend_souvenir[2]['id']           = 419;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            break;
        case 'VNM' :
            break;
        case 'KHM' :
            break;
        case 'MYS' :
            $recommend_souvenir[0]['id']           = 983;
            $recommend_souvenir[1]['id']           = 984;
            $recommend_souvenir[2]['id']           = 985;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            break;
        case 'TWN' :
            $recommend_souvenir[0]['id']           = 950;
            $recommend_souvenir[1]['id']           = 959;
            $recommend_souvenir[2]['id']           = 929;
            $recommend_souvenir[3]['id']           = 978;
            $recommend_souvenir[0]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[0]['id']);
            $recommend_souvenir[1]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[1]['id']);
            $recommend_souvenir[2]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[2]['id']);
            $recommend_souvenir[3]['souvenir_data'] = $db->view_souvenir($recommend_souvenir[3]['id']);
            $recommend_souvenir[0]['souvenir_name'] = $recommend_souvenir[0]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[0]['souvenir_picture'] = $image_path.$recommend_souvenir[0]['id'].'.jpg';
            $recommend_souvenir[0]['souvenir_link'] = $souvenir_path.$recommend_souvenir[0]['id'];
            $recommend_souvenir[1]['souvenir_name'] = $recommend_souvenir[1]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[1]['souvenir_picture'] = $image_path.$recommend_souvenir[1]['id'].'.jpg';
            $recommend_souvenir[1]['souvenir_link'] = $souvenir_path.$recommend_souvenir[1]['id'];
            $recommend_souvenir[2]['souvenir_name'] = $recommend_souvenir[2]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[2]['souvenir_picture'] = $image_path.$recommend_souvenir[2]['id'].'.jpg';
            $recommend_souvenir[2]['souvenir_link'] = $souvenir_path.$recommend_souvenir[2]['id'];
            $recommend_souvenir[3]['souvenir_name'] = $recommend_souvenir[3]['souvenir_data'][0]['souvenir_name'];
            $recommend_souvenir[3]['souvenir_picture'] = $image_path.$recommend_souvenir[3]['id'].'.jpg';
            $recommend_souvenir[3]['souvenir_link'] = $souvenir_path.$recommend_souvenir[3]['id'];
            break;
    }
    return $recommend_souvenir;
}
function fetch_spot($iso3, $path){
    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $spot_path = '../'.$db->showpath_bycountry($iso3).'/search_opt.php?inp_country='.$iso3.'&inp_city=';
    $image_path = $path.$db->showpath_bycountry($iso3).'/images/spot/';
    switch($iso3){
        case 'SGP' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            break;
        case 'IDN' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            break;
        case 'AUS' :
            $spot[0]['spot_id'] = 'sydney';
            $spot[1]['spot_id'] = 'cairns';
            $spot[2]['spot_id'] = 'melbourne';
            $spot[3]['spot_id'] = 'goldcoast';
            $spot[0]['spot_name'] = 'シドニー';
            $spot[0]['spot_picture'] = $image_path.$spot[0]['spot_id'].'.jpg';
            $spot[0]['spot_link'] = 'http://www.jtb.com.au/tour/'.$spot[0]['spot_id'];
            $spot[1]['spot_name'] = 'ケアンズ';
            $spot[1]['spot_picture'] = $image_path.$spot[1]['spot_id'].'.jpg';
            $spot[1]['spot_link'] = 'http://www.jtb.com.au/tour/'.$spot[1]['spot_id'];
            $spot[2]['spot_name'] = 'メルボルン';
            $spot[2]['spot_picture'] = $image_path.$spot[2]['spot_id'].'.jpg';
            $spot[2]['spot_link'] = 'http://www.jtb.com.au/tour/'.$spot[2]['spot_id'];
            $spot[3]['spot_name'] = 'ゴールドコースト';
            $spot[3]['spot_picture'] = $image_path.$spot[2]['spot_id'].'.jpg';
            $spot[3]['spot_link'] = 'http://www.jtb.com.au/tour/'.$spot[2]['spot_id'];
            break;
        case 'NZL' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            $spot[3]['spot_id']   = $city_data['value'][4];
            $spot[3]['spot_name'] = $city_data['data'][4];
            $spot[3]['spot_picture'] = $image_path.$city_data['value'][4].'.jpg';
            $spot[3]['spot_link'] = $spot_path.$city_data['value'][4];
            break;
        case 'THA' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            $spot[3]['spot_id']   = $city_data['value'][3];
            $spot[3]['spot_name'] = $city_data['data'][3];
            $spot[3]['spot_picture'] = $image_path.$city_data['value'][3].'.jpg';
            $spot[3]['spot_link'] = $spot_path.$city_data['value'][3];
            break;
        case 'VNM' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            break;
        case 'KHM' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            break;
        case 'MYS' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            $spot[3]['spot_id']   = $city_data['value'][3];
            $spot[3]['spot_name'] = $city_data['data'][3];
            $spot[3]['spot_picture'] = $image_path.$city_data['value'][3].'.jpg';
            $spot[3]['spot_link'] = $spot_path.$city_data['value'][3];
            break;
        case 'TWN' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            $spot[3]['spot_id']   = $city_data['value'][3];
            $spot[3]['spot_name'] = $city_data['data'][3];
            $spot[3]['spot_picture'] = $image_path.$city_data['value'][3].'.jpg';
            $spot[3]['spot_link'] = $spot_path.$city_data['value'][3];
            break;
        case 'PHL' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            $spot[1]['spot_id']   = $city_data['value'][1];
            $spot[1]['spot_name'] = $city_data['data'][1];
            $spot[1]['spot_picture'] = $image_path.$city_data['value'][1].'.jpg';
            $spot[1]['spot_link'] = $spot_path.$city_data['value'][1];
            $spot[2]['spot_id']   = $city_data['value'][2];
            $spot[2]['spot_name'] = $city_data['data'][2];
            $spot[2]['spot_picture'] = $image_path.$city_data['value'][2].'.jpg';
            $spot[2]['spot_link'] = $spot_path.$city_data['value'][2];
            break;
        case 'MMR' :
            $city_data = $db->select_city($iso3);
            $spot[0]['spot_id']   = $city_data['value'][0];
            $spot[0]['spot_name'] = $city_data['data'][0];
            $spot[0]['spot_picture'] = $image_path.$city_data['value'][0].'.jpg';
            $spot[0]['spot_link'] = $spot_path.$city_data['value'][0];
            break;
    }
    return $spot;
}
/* 20130325 */
function fetch_site_info($iso3){
    $branch_info = null;

    $branch_info['souv_company'] = '';
    switch($iso3){
        case 'SGP' :
            $branch_info['name'] = 'JTB MYBUS SINGAPORE';
            $branch_info['branch_name'] = 'JTBシンガポール';
            $branch_info['site_code'] = 'A012000015';
            $branch_info['opt_time'] = '10:00-17:30(年中無休)/シンガポール時間';
            $branch_info['opt_tel'] = '65-6735-2847';
            $branch_info['opt_email'] = 'opjtb.sg@jtbap.com';
            $branch_info['souv_time'] = '08:45-17:45(月～金)/シンガポール時間';
            $branch_info['souv_tel'] = '65-6305-5050';
            $branch_info['souv_email'] = 'orchard.sg@jtbap.com';
            break;
        case 'IDN' :
            $branch_info['name'] = 'JTB MYBUS BALI / INDONESIA';
            $branch_info['branch_name'] = 'JTBバリ支店';
            $branch_info['site_code'] = 'A012000017';
            $branch_info['opt_time'] = '9:00～17:30（年中無休）/インドネシア時間';
            $branch_info['opt_tel'] = '886-(0)2-2521-7315';
            $branch_info['opt_email'] = 'reservation_jtbbali.id@jtbap.com';
            $branch_info['souv_time'] = '9:00～17：30（月～金）/インドネシア時間';
            $branch_info['souv_tel'] = '886-(0)2-2522-3094';
            $branch_info['souv_email'] = 'reservation_jtbbali.id@jtbap.com';
            break;
        case 'AUS' :
            $branch_info['name'] = 'JTB MYBUS AUSTRALIA';
            $branch_info['branch_name'] = 'JTBオーストラリア';
            $branch_info['site_code'] = 'A012000021';
            $branch_info['opt_time'] = '月～金 9:00-17:00(土日祝祭日休業)/シドニー時間';
            $branch_info['opt_tel'] = '61-(0)29510-0370';
            $branch_info['opt_email'] = 'isdweb.au@jtbap.com';
            $branch_info['souv_time'] = '';
            $branch_info['souv_tel'] = '';
            $branch_info['souv_email'] = '';
            break;
        case 'NZL' :
            $branch_info['name'] = 'JTB MYBUS NEW ZEALAND';
            $branch_info['branch_name'] = 'JTBニュージーランド';
            $branch_info['site_code'] = 'A012000022';
            $branch_info['opt_time'] = '月～金 9:00-17:30(土日祝祭日休業)/ニュージーランド時間';
            $branch_info['opt_tel'] = '64-(0)9-379-6415';
            $branch_info['opt_email'] = 'webnz.nz@jtbap.com';
            $branch_info['souv_time'] = '';
            $branch_info['souv_tel'] = '';
            $branch_info['souv_email'] = '';
            break;
        case 'THA' :
            $branch_info['name'] = 'JTB MYBUS THAILAND';
            $branch_info['branch_name'] = 'JTBタイ';
            $branch_info['site_code'] = 'A012000018';
            $branch_info['opt_time'] = '月～金9:00-17:30(土日祝祭日休業)/タイ時間';
            $branch_info['opt_tel'] = '66-2632-9404';
            $branch_info['opt_email'] = 'reservation_mybus.th@jtbap.com';
            $branch_info['souv_time'] = '月～金9:00-17:30(土日祝祭日休業)/タイ時間';
            $branch_info['souv_tel'] = '66-2267-9236';
            $branch_info['souv_email'] = 'bkk_mybus.th@jtbap.com';
            break;
        case 'VNM' :
            $branch_info['name'] = 'JTB MYBUS VIET NAM';
            $branch_info['branch_name'] = 'JTBベトナム';
            $branch_info['site_code'] = 'A012000016';
            $branch_info['opt_time'] = '月～金 9:00-17:30, 土 9:00-13:00(日曜,祝祭日休業)/ベトナム時間';
            $branch_info['opt_tel'] = '84-(0)8-3827-7493';
            $branch_info['opt_email'] = 'lookdesk.vn@jtbap.com';
            $branch_info['souv_time'] = '';
            $branch_info['souv_tel'] = '';
            $branch_info['souv_email'] = '';
            break;
        case 'KHM' :
            $branch_info['name'] = 'JTB MYBUS VIET NAM';
            $branch_info['branch_name'] = 'JTBベトナム';
            $branch_info['site_code'] = 'A012000016';
            $branch_info['opt_time'] = '月～金 9:00-17:30, 土 9:00-13:00(日曜,祝祭日休業)/ベトナム時間';
            $branch_info['opt_tel'] = '84-(0)8-3827-7493';
            $branch_info['opt_email'] = 'lookdesk.vn@jtbap.com';
            $branch_info['souv_time'] = '';
            $branch_info['souv_tel'] = '';
            $branch_info['souv_email'] = '';
            break;
        case 'MYS' :
            $branch_info['name'] = 'JTB MYBUS MALAYSIA';
            $branch_info['branch_name'] = 'JTBマレーシア';
            $branch_info['site_code'] = 'A012000019';
            $branch_info['opt_time'] = '平日 9:00-17:30, 土曜 9:00-12:30(日曜,祝祭日休業)/マレーシア時間';
            $branch_info['opt_tel'] = '60-(0)3-2142-8727';
            $branch_info['opt_email'] = 'mybus.my@jtbap.com';
            $branch_info['souv_time'] = '';
            $branch_info['souv_tel'] = '';
            $branch_info['souv_email'] = '';
            break;
        case 'TWN' :
            $branch_info['name'] = 'JTB MYBUS TAIWAN';
            $branch_info['branch_name'] = 'JTB台湾';
            $branch_info['site_code'] = 'A012000020';
            $branch_info['opt_time'] = '9:00～17:30（年中無休）/台湾時間';
            $branch_info['opt_tel'] = '886-2-2581-2239';
            $branch_info['opt_email'] = 'option_yoyaku.tw@jtbap.com';
            $branch_info['souv_company'] = 'エスティーシー商事（JTB台湾グループ）';
            $branch_info['souv_time'] = '9:00～17：30（台湾時間/土・日・祝を除く）';
            $branch_info['souv_tel'] = '886-2-2581-3268';
            $branch_info['souv_email'] = 'stc_trading.tw@jtbap.com';
        break;
    }
    return $branch_info;
}
function fetch_stripe_amount($currency, $amount){
    switch($currency){
        case 'AUD':
            //1AUD = 100cent
            $stripe_amount = 100 * $amount;
            break;
        case 'HKD':
            //1HKD = 100cent
            $stripe_amount = 100 * $amount;
            break;
        case 'USD':
            //1USD = 100cent
            $stripe_amount = 100 * $amount;
            break;
        case 'MYR':
            //1MYR = 100sen
            $stripe_amount = 100 * $amount;
            break;
        case 'NZD':
            //1NZD = 100cent
            $stripe_amount = 100 * $amount;
            break;
        case 'SGD':
            //1SGD = 100cent
            $stripe_amount = 100 * $amount;
            break;
        case 'THB':
            //1THB = 100 satan
            $stripe_amount = 100 * $amount;
            break;
        case 'TWD':
            //1TWD = 100分
            $stripe_amount = 100 * $amount;
            break;
    }
    return $stripe_amount;
}
function fetch_local_amount($currency_code, $amount, $new_currency){

    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $sql = 'SELECT rate FROM '._DB_PREFIX_TABLE.'currency WHERE code = "'.$currency_code.'" ';
    $result = $db->db_query($sql);
    while ($record = mysql_fetch_array($result))
    {
        $rate = $record['rate'];
    }
    $new_amount = $amount * $rate;

    return $new_amount;
}

?>