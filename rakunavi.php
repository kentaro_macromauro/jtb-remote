<? 

if ( $_SERVER["SERVER_PORT"] != 443 ){
    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}

include('www_config/setting.php');
include('class/include/c_query.php');
include('class/c_query_sub.php');
include('class/c_common.php');
include('class/phpmailer/class.phpmailer.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

function fpostmsg($msg){
	echo ($msg)?'<p class="txtError">'.$msg.'</p>':'';	
}


	$code = $_REQUEST[code];
	
	switch ($code){
		case 'sg-thank' :  $code = 'sg'; break;	
		case 'th-thank'  :  $code = 'th'; break;
		case 'tw-thank' :  $code = 'tw'; break;
	}
	
	if (empty($code)){
		header( "HTTP/1.1 301 Moved Permanently"); 
		header("Location: ./rakunavi-sg");	
	}	



	$jp_txt_country = array('sg'  => 'シンガポール',
								  'th'  => 'タイ',
								  'tw' => '台湾');
	
	$en_txt_country = array('sg' => 'singapore',
										'th' => 'thailand',
										'tw' => 'taiwan');
	
	$err_txt = array( 'last_name' =>  '氏名（姓）を入力してください。',
					 		 'first_name' => '氏名（名）を入力してください。',
							 'last_name_furi' => 'フリガナ（姓）を入力してください。' ,
							 'first_name_furi'  => 'フリガナ（名）はカナで入力してください。',
							 'email_empty' => 'メールアドレスを入力してください。', 
							 'email_empty_cfm' => 'メールアドレス（確認用）を入力してください',
							 'email_fail' 	  => 'メールアドレスの形式が正しくありません。' ,
							 'email_same'  => 'メールアドレスが一致していません。',
							 'checkin_date' => 'ご質問の受付はご出発の３日前までです。',
							 'jtb_no' => 'コースNo.（コース）を入力してください。',
							 'jtb_no_prefix' => 'コースNo.（コース）は半角英数で入力してください。',
							 'msg'	=> 'お問い合わせ内容を入力してください。',
							 'conf1'	=> 'ルックJTBのお申込み完了後にお問い合わせください。',
							 'conf2'	=> '個人情報の取り扱いに関する同意にチェックがされていません。',
							 'branch' => '都市を入力してください。');
	
	
	
	
	$staff_email = array('sg' => 'jtbmillenia.sg@jtbap.com',
						 		   'th' => 'sclounge.th@jtbap.com',
								   'tw' => 'rakunavi.tw@jtbap.com');
	
	$send_email = array('sg' => 'jtbmillenia.sg@jtbap.com',
						 		   'th' => 'sclounge.th@jtbap.com',
								   'tw' => 'rakunavi.tw@jtbap.com');
	
	$step = 1;

	$arr_year = array();

	for ($i = date('Y');  $i <= (date('Y')+2) ; $i++){
		$arr_year[] = $i;
	}
	
	$arr_date_mm = array();
	for ($i = 1; $i <=12 ; $i++){
		$arr_date_mm[] = $i;
	} 
	
	$arr_date_dd = array();
	for ($i = 1; $i <=31 ; $i++){
		$arr_date_dd[] = $i;
	} 
	
	

	$select_year =  input_selectbox('input_date_yy'	,	$arr_year,	
																  			$arr_year,	
																			$_REQUEST[input_date_yy],'---',$class="inp_min");
	$select_month =  input_selectbox('input_date_mm',	$arr_date_mm,	
									 											$arr_date_mm,
																			$_REQUEST[input_date_mm],'---',$class="inp_min");
	$select_day =  input_selectbox('input_date_dd',	$arr_date_dd,
								   											$arr_date_dd,
																			$_REQUEST[input_date_dd] ,'---',$class="inp_min");
	
	
	if ($_REQUEST[btn_confirm]){
		
		$chk_err = 0;
		$msg_err = array();
		
		//check text error
		$req = array( 'inp_firstname' , 'inp_lastname' , 
					 		'inp_furifirstname' , 'inp_furilastname' , 
							'inp_email', 'inp_confemail',
							'input_date_yy', 'input_date_mm' , 'input_date_dd' ,
							'input_jtb_no1', 'input_jtb_no2' , 'input_jtb_no3',
							'inp_content_note' );
		
		
		
		foreach ($req as $lp_req){
			$_REQUEST[$lp_req] = trim($_REQUEST[$lp_req]);	
		}
		
		//first name last name
		if (empty( $_REQUEST[inp_firstname])) {
			$msg_err[first_name] = $err_txt[first_name];
			$chk_err++;
		}	
		
		if (empty($_REQUEST[inp_lastname])){
			$msg_err[last_name] = $err_txt[last_name];
			$chk_err++;
		}	

		//first name last name furi
		if (empty($_REQUEST[inp_furifirstname])){
			$msg_err[first_name_furi] = $err_txt[first_name_furi];
			$chk_err++;
		}	
		
		if (empty($_REQUEST[inp_furilastname] )){
			$msg_err[last_name_furi] = $err_txt[last_name_furi];
			$chk_err++;
		}	
		//check text error
		
		//email
		
		if (empty($_REQUEST[inp_email])   ||  empty($_REQUEST[inp_confemail] ) ){
		
			if (empty($_REQUEST[inp_email])){
				$msg_err[email_empty1] = $err_txt[email_empty];
				$chk_err++;
			}
			else{
				if (empty($_REQUEST[inp_confemail] )){
					$msg_err[email_empty2] = $err_txt[email_empty_cfm];
					$chk_err++;
				}
			}
			
		}
		else{	
			if ( $_REQUEST[inp_email]  != $_REQUEST[inp_confemail]  ){
				$msg_err[email_same] = $err_txt[email_same];
				
				$chk_err++;
			}
			else{
				if(!filter_var($_REQUEST[inp_email] , FILTER_VALIDATE_EMAIL)){
					$msg_err[email_fail] = $err_txt[email_fail];
					$chk_err++;
				}
			}
		}
		//email
		
		//date
		if (empty($_REQUEST[input_date_yy]) || empty($_REQUEST[input_date_mm]) || empty($_REQUEST[input_date_dd])){
			$msg_err[checkin_date] = $err_txt[checkin_date];
			$chk_err++;
		}
		else{
			$date_yy 	= $_REQUEST[input_date_yy];
			$date_mm = $_REQUEST[input_date_mm];
			$date_dd	= $_REQUEST[input_date_dd];
			
			$chk_date = $date_yy.'-'.$date_mm.'-'.$date_dd;
			
			$sql = 'SELECT DATEDIFF(check_date,now_date) date_diff FROM ( SELECT DATE_FORMAT(now(),\'%Y-%m-%d\') now_date,\''.$chk_date.'\' check_date FROM  DUAL) AS curdate LIMIT 0,1'	;
			
			$result = $db->db_query($sql);

			while ($record = mysql_fetch_array($result)){
				$chkdate = $record[date_diff];
			}
			
			if ($chkdate < 4){
				$msg_err[checkin_date] = $err_txt[checkin_date];
				$chk_err++;
			}
			
		}
		//date
		
		
		
		//no
		if (empty($_REQUEST[input_jtb_no1])){
			$msg_err[jtb_no] = $err_txt[jtb_no];
			$chk_err++;
		}
		else{
			
				
				 if  ( !preg_match('/^[a-zA-Z0-9,]+$/', $_REQUEST[input_jtb_no1])  ){
					 
					
					 
					 $msg_err[jtb_no] = $err_txt[jtb_no_prefix];
					 $chk_err++;

				}
				else{
					    
						
						 if (mb_strlen ($_REQUEST[input_jtb_no1] ,'UTF-8')  <  5 ){
						 		 $msg_err[jtb_no] = $err_txt[jtb_no_prefix];
					 			$chk_err++;
						 			
						 }
					     else{
							 
							if     ( ( trim ($_REQUEST[input_jtb_no2] ) != '') && ( trim ($_REQUEST[input_jtb_no3] ) == '') ){
									
								// 
								 if (    !preg_match('/^[a-zA-Z0-9,]+$/', $_REQUEST[input_jtb_no2])  ) {
																																						  
												$msg_err[jtb_no] = $err_txt[jtb_no_prefix];
												$chk_err++;																									  
									}
									else{
												
												if   ((mb_strlen ($_REQUEST[input_jtb_no2] ,'UTF-8')  <  3 ) ){
														$msg_err[jtb_no] = $err_txt[jtb_no_prefix];
														$chk_err++;	
												 }
										
									}//

							}
							else{
							 		
											if     ( ( trim ($_REQUEST[input_jtb_no2] ) == '') && ( trim ($_REQUEST[input_jtb_no3] ) == '') ){
												
											}
											else{
												   if (    !preg_match('/^[a-zA-Z0-9,]+$/', $_REQUEST[input_jtb_no2])   ||  !preg_match('/^[a-zA-Z0-9,]+$/', $_REQUEST[input_jtb_no3]) ) {
																																										  
																$msg_err[jtb_no] = $err_txt[jtb_no_prefix];
																$chk_err++;																									  
													}
													else{
																
																if   ((mb_strlen ($_REQUEST[input_jtb_no2] ,'UTF-8')  <  3 )   ||   (mb_strlen ($_REQUEST[input_jtb_no3] ,'UTF-8')  <  2 ) ){
																		$msg_err[jtb_no] = $err_txt[jtb_no_prefix];
																		$chk_err++;	
																 }
														
													}//
													
											}//
											
								}
						
						 }
				}
				
				
			
		}
		//no
		
		//no
		if (empty($_REQUEST[inp_content_note])){
			$msg_err[msg] = $err_txt[msg];
			$chk_err++;
		}
		//no
		
		//conf
		if (empty($_REQUEST[inp_confirm1]) ){
			$msg_err[conf1] = $err_txt[conf1];
			$chk_err++;		
		}
		
		if (empty($_REQUEST[inp_confirm2])){
			$msg_err[conf2] = $err_txt[conf2];
			$chk_err++;	
		}
		
		if ($code == 'th'){
			//onlythai
			if (empty($_REQUEST[inp_branch])){
				$msg_err[branch] = $err_txt[branch];
				$chk_err++;
			}
			//onlythai
		}
		
		if ($chk_err){
			$step = 1;	
			
		}
		else{
			$step =2;	
		}
	}
	
	if ($_REQUEST[btn_back]){
		$step = 1;	
	}
	
	if ($_REQUEST[btn_submit]){
		
		$dep_date = $_REQUEST[input_date_yy].'-'.$_REQUEST[input_date_mm].'-'.$_REQUEST[input_date_dd];
		
		
		
		$jtb_no2 = $_REQUEST[input_jtb_no2];
		$jtb_no3 = $_REQUEST[input_jtb_no3];
		
			
		if (trim ($jtb_no2)  == ''){
					$jtb_no2 = '***';
		}
							
		if (trim ($jtb_no3)  == ''){
					$jtb_no3 = '***';
		}
							
		
		$jtb_no		= $_REQUEST[input_jtb_no1].'–'.$jtb_no2.'–'.$jtb_no3;
		
		
		$branch  = empty($_REQUEST[inp_branch])?'':$_REQUEST[inp_branch];
		
		switch ($code){
			case 'sg' : $country_code = 'SGP'; break;	
			case 'th'  : $country_code = 'THA'; break;
			case 'tw' : $country_code = 'TWN'; break;
		}
		
		 $dbfield = array('country_iso3','branch',
						  		 'first_name','last_name',
						  		 'furi_first_name','furi_last_name',
								 'email','depature_date',
								 'jtb_no',
								 'comment',
								 'update_date');
		 
		 $dbdata = array($country_code,$branch,
						 		 $_REQUEST[inp_firstname],$_REQUEST[inp_lastname],
								 $_REQUEST[inp_furifirstname],$_REQUEST[inp_furilastname],
								 $_REQUEST[inp_email],$dep_date,
								 $jtb_no,
								 $_REQUEST[inp_content_note],
								 'datetime');
		
		 $db->set_insert('mbus_jtb_contact',$dbfield,$dbdata);
		
		 
		 /* email */
		 $subject = '【'.$jp_txt_country[$code].' ルックJTB - Rakuなびサポート】 コースNo '.$_REQUEST[input_jtb_no1].'  '.$_REQUEST[input_date_mm].'月'.$_REQUEST[input_date_dd].'日発';
		 
		 $content = '
		この度はルックJTB - Rakuなびサポートをご利用いただきまして誠にありがとうございます。<br/>
		下記の通り、お問合せを受付いたしました。<br/>
		※受領メールの確認が各国の土日・祝祭日のため、４８時間以内に返信ができない場合がございます。<br/>
		※回答にお時間をいただくことや、ご希望に添えない場合がありますので、予めご了承ください。<br/><br/>
		
		お客様名：'.$_REQUEST[inp_lastname].'　'.$_REQUEST[inp_firstname].'　（'.$_REQUEST[inp_furilastname].'　'.$_REQUEST[inp_furifirstname].'） 様<br/>
		出発日：'.$_REQUEST[input_date_yy].'年 '.$_REQUEST[input_date_mm].'月 '.$_REQUEST[input_date_dd].'日<br/>
		LOOK JTBコースナンバー：'.$jtb_no.'<br/>
		お問い合わせ内容：<br/>
		'.nl2br($_REQUEST[inp_content_note]).'<br/>';
		
		if ($code == 'th'){
			$content .= '都市: '.$branch.'<br/>';
		}
		
		$content .= '<br/>
		*****************************<br/>
		LOOK  JTB Raku ナビサポート<br/>
		担当：'.$jp_txt_country[$code].'<br/>
		Email：'.$staff_email[$code].'<br/>
		*****************************';

		 $mail = new PHPMailer();
		 $mail->AddAddress($_REQUEST[inp_email]);
		 $mail->AddCC($send_email[$code]);
		 
		 $mail->AddBcc('pornpen@auncon.co.jp');
		 $mail->AddBcc('jedsadha@auncon.co.jp');
		 
		 
		 $mail->CharSet = 'UTF-8'; 
		 
		 $mail->Subject = $subject;
		 $mail->MsgHTML('<html><body><div style="width:600px; display:block;">'.$content.'</div></body></html>');
		 $mail->SetFrom($staff_email[$code]);
		//$mail->SetFrom( 'rakunavi@mybus-asia.com' );
		
		 if (!$mail->Send()){
			 
		 }
		 /* email*/
		 
		 header('HTTP/1.1 301 Moved Permanently');
  		 header('Location: rakunavi-'.$code.'-thank');
	}
	
	if (strpos($_REQUEST[code],'thank') !== false){
		$step = 3;	
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ルックJTB　RAKUなびサポート（<?=$jp_txt_country[$code]?>）</title>
<link type="text/css" href="common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="common/raku.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="common/js/rakunavi.js"></script>
</head>
<body>

<!--conent-->
<div class="content">
	<!--header-->
	<div class="header">
		<h1 class="txtHeaderTop">ルックJTB　RAKUなびサポート（<?=$jp_txt_country[$code]?>）</h1>
		<span class="logoTop"><img src="images/lookjtb/logo.jpg" alt="ルックJTB×Mybusアジア・パシフィック"  /></span>
	</div>
	<!--header-->
	
	
	<!--content -->
	<div class="txtContent">
		
		<!--content top-->
		<div class="txtContentTop">
				<div class="rakunaviMainImage">
					<p class="rakunaviMainTxtTop" >直接現地スタッフから最新の情報をゲットできるから、旅行プランが、もっと楽しく立てられます！<br/>ちょっと聞いておきたいことや不安に感じていることまで、旅のご相談にお応えします。  </p>
					
					<p class="rakunaviMainTxtBottom">お問合せの前に・・<br/>
気候や服装、レストランやオプショナルツアー予約について、子供服や子供用品<br/>
が買えるお店など、<a  href="javascript:w=window.open('http://www.mybus-asia.com/faq-<?=$code?>','','width=980,height=600,scrollbars=1');w.focus();" >良くある質問</a>をご確認ください。</p>
				
				</div>
		</div>
		<!--content top-->
		
		<!--content form step1-->
		<div class="breakLine"></div>		
		<!--contentbottom-->
		<div class="txtContentBottom">
		
			
		
		
			<? if ($step==1){ ?>
			
			<div class="txtContentNoteHeader" id="txtContentNoteHeader"><?=$jp_txt_country[$code]?> 日本出発前 現地メールお問合せ</div>
			
			<? 
				if ($_REQUEST[btn_back]){ ?>
					<script type="text/javascript">
						$("html, body").animate({ scrollTop: $('#txtContentNoteHeader').offset().top }, 300);
					</script>	
			<?	}
			
			?>
			
			<p class="txtContentNote"> 				
				※お問い合わせの前に、必ず<a  href="javascript:w=window.open('http://www.mybus-asia.com/faq-<?=$code?>','','width=980,height=600,scrollbars=1');w.focus();" >よくある質問</a>をご確認ください。<br/>
				※ルックＪＴＢでお申込みされたお客様の限定サービスとなります。<br/>
				※ご予約記録が確認できない場合、ご返答できかねますのでご了承ください。<br/>
				※ご出発の前日から起算して3日前以前にご質問ください。<br/>
				※お問合せ内容の確認が各国の土日・祝祭日により、４８時間以内に返信ができない場合がございます。<br/>
				※お問合せ受領メールが30分以内に届かない場合は再度ご入力ください。<br/>
				※回答にお時間をいただくことや、ご希望に添えない場合がありますので、予めご了承ください。 </p>
			 <? }
			 else if ($step==2){ ?>
			 <div class="m_top5"></div>
			 <p class="txtContentNote">以下の内容でよろしければ送信ボタンを押してください。</p>
			 <? } ?>
			 
			 
		<? if ($step==1){ 
			if (count($msg_err)){
				echo '<div class="msgError" id="msgError">入力の誤り、または必須項目の未入力があります。内容を再度ご確認ください。<br/>';
				echo '<ul class="subError">';
				foreach ($msg_err as $txt_msg_err){
					
				
					
					echo '<li>'.$txt_msg_err.'</li>';
				}
				echo '</ul></div>';
				
				?>
					<script type="text/javascript">
						$("html, body").animate({ scrollTop: $('#msgError').offset().top }, 300);
					</script>
				<?
				
			}
			
			?>
			 
			 
			 
		</div>
		<!--contentbottom-->
		
		
		<!--content form step1-->
     <form method="post" action="rakunavi-<?=$code?>">
	
	<div class="inquery">
		<table border="0" cellpadding="0" cellspacing="0" class="inquery_form">
			<tr>
				<th>氏名（漢字）<span class="req">*必須</span></th>
				<td> 姓
					<input type="text" name="inp_lastname" class="inp-name" value="<?=$_REQUEST[inp_lastname]?>"   placeholder="例）山田"  />
					名
					<input type="text" name="inp_firstname" class="inp-name" value="<?=$_REQUEST[inp_firstname]?>"    placeholder="例）太郎" />
					
					<? 
						fpostmsg($msg_err[first_name]);
						fpostmsg($msg_err[last_name]);
					?>
					
				</td>
			</tr>
			<tr class="event">
				<th>氏名（フリガナ）<span class="req">*必須</span></th>
				<td> 姓
					<input type="text" name="inp_furilastname"  class="inp-name" value="<?=$_REQUEST[inp_furilastname]?>"  placeholder="例）ヤマダ"   />
					名
					<input type="text" name="inp_furifirstname"  class="inp-name" value="<?=$_REQUEST[inp_furifirstname]?>"  placeholder="例）タロウ"   />
				
					<? 
						fpostmsg($msg_err[first_name_furi]);
						fpostmsg($msg_err[last_name_furi]);
					?>
				</td>
			</tr>
			<tr>
				<th>メールアドレス<span class="req">*必須</span></th>
				<td>
					<input type="text" name="inp_email" class="inp-email" value="<?=$_REQUEST[inp_email]?>"  placeholder="例） yamada@●●●.co.jp"  /> 
					<span class="m_left10" >*半角英数 </span>
					<? 
						fpostmsg($msg_err[email_empty1]);
						fpostmsg($msg_err[email_fail]);
					?>
					
					
			   </td>
			</tr>
			<tr class="event">
				<th>メールアドレス（確認）<span class="req">*必須</span></th>
				<td>
					<input type="text" name="inp_confemail"  class="inp-email"  value="<?=$_REQUEST[inp_confemail]?>" placeholder="例） yamada@●●●.co.jp"  />
					<span class="m_left10">*確認のため再入力</span> 
					
					<?
						fpostmsg($msg_err[email_empty2]);
						fpostmsg($msg_err[email_same]);
					?>
					</td>
			</tr>
			<tr>
				<th>日本出発日<span class="req">*必須</span></th>
				<td> 
					<div class="floatLeft"><?=$select_year?> 年 <?=$select_month?> 月 <?=$select_day?> 日 
					
					<?
						fpostmsg($msg_err[checkin_date]);
					?>
					</div>
					 <div class="p_ddpicker m_left10 floatLeft" ><div class=""  id="id_cal"><span class="cal">カレンダーから選択</span> </div></div></td>
			</tr>
			<tr class="event">
				<th>コースNo. <span class="req">*必須</span></th>
				<td>
					
				
					<input type="text" name="input_jtb_no1" class="inp_min5" value="<?=$_REQUEST[input_jtb_no1]?>"  placeholder="例）ENNE4"   maxlength="5" />
					<span style="padding-left:5px; padding-right:5px;">–</span>
					<input type="text" name="input_jtb_no2" class="inp_min3"  value="<?=$_REQUEST[input_jtb_no2]?>" placeholder="例）UAA"   maxlength="3" />
					<span style="padding-left:5px; padding-right:5px;">–</span>
					<input type="text" name="input_jtb_no3" class="inp_min2"  value="<?=$_REQUEST[input_jtb_no3]?>"  placeholder="例）BH"   maxlength="2" />
					<p>*コース-航空-ホテル　半角英数</p>
                    <p>*航空コード-ホテルコードがないコースにお申し込みの場合は、コースNoのみ入力要</p>
                    
					<? 
						fpostmsg($msg_err[jtb_no]);
					?>
					
				</td>
			</tr>
			<tr>
				<th>お問合せ内容 <span class="req">*必須</span></th>
				<td>
					<textarea name="inp_content_note" class="inp-textarea"  rows="5"><?=$_REQUEST[inp_content_note]?></textarea>
					<span class="m_left10 ">*全角1000文字以内</span> 
					
					<? 
						fpostmsg($msg_err[msg]);
					?>	
				</td>
			</tr>
			
			<? if ($code == 'th'){ ?>
			<!--only thailand-->
			
			<tr class="event">
				<th>都市 <span class="req">*必須</span></th>
				<td>
					<input type="radio" name="inp_branch" value="バンコク"    <?=($_REQUEST[inp_branch]=='バンコク')?'checked="checked"':'' ?>  /> バンコク 
					<input type="radio" name="inp_branch" value="プーケット"  <?=($_REQUEST[inp_branch]=='プーケット')?'checked="checked"':'' ?> /> プーケット
					<input type="radio" name="inp_branch" value="その他"  <?=($_REQUEST[inp_branch]=='その他')?'checked="checked"':'' ?> /> その他
					<? 
						fpostmsg($msg_err[branch]);
					?>
				</td>
			</tr>
			
			<!--only thailand-->
			<? } ?>
			
		</table>
		<div class="contentConfirmPage">
		
			<?  
			 	 $check_conf1 = '';
				 $check_conf2 = '';
				
					if ($_REQUEST[inp_confirm1] ==  'ルックJTBで申込みを完了しています。'){
						$check_conf1 = 'checked="checked"';
					}
							
					if ($_REQUEST[inp_confirm2] == '入力された個人情報はプライバシーポリシーのとおり取扱われることに同意します。'){
							$check_conf2 = 'checked="checked"';
					}
				  
			?>
		
			<input type="checkbox" name="inp_confirm1" <?=$check_conf1 ?> value="ルックJTBで申込みを完了しています。"    />
			ルックJTBで申込みを完了しています。<br/>
			<input type="checkbox" name="inp_confirm2" <?=$check_conf2 ?> value="入力された個人情報はプライバシーポリシーのとおり取扱われることに同意します。" />
		入力された個人情報は<a  href="javascript:w=window.open('http://www.mybus-asia.com/privacy-<?=$code?>','','width=980,height=600,scrollbars=1');w.focus();" >プライバシーポリシー</a>のとおり取扱われることに同意します。   
		
		</div>
		
		
			
		
		<div class="groupBtnStep1">
		<input type="submit" name="btn_confirm" class="btn btnConfirm"  value="確認画面へ進む" />
		
		
		
		<!--goblesign-->
		<div style="height:30px;"></div>
        <div style="float:right; margin-right:5px; height:100px;">
            <span id="ss_gmo_img_wrapper_100-50_image_ja">
            <a href="https://jp.globalsign.com/" target="_blank">
            <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
            </a>
            </span>
            <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
         </div>
		 <div style="height:40px;"></div>
		<!--goblesign-->
		
		</div>
	</div>
	
	</form>
	<!--content form step1-->
	<? } 
	else if ($step==2){ ?>	
	</div>
		<!--contentbottom-->
	
	<!--content form step2-->
	<form method="post" action="rakunavi-<?=$code?>">
		
		
	<div class="inquery">
		<table border="0" cellpadding="0" cellspacing="0" class="inquery_form">
			<tr>
				<th>氏名（漢字）</th>
				<td><?=$_REQUEST[inp_lastname].'　'.$_REQUEST[inp_firstname] ?>	
					<input type="hidden" name="inp_lastname" value="<?=$_REQUEST[inp_lastname]?>" />
					<input type="hidden" name="inp_firstname" value="<?=$_REQUEST[inp_firstname]?>" />
				</td>
			</tr>
			<tr class="event">
				<th>氏名（フリガナ）</th>
				<td><?=$_REQUEST[inp_furilastname].'　'.$_REQUEST[inp_furifirstname] ?>
					<input type="hidden" name="inp_furilastname" value="<?=$_REQUEST[inp_furilastname]?>" />
					<input type="hidden" name="inp_furifirstname" value="<?=$_REQUEST[inp_furifirstname]?>" />
				</td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td>
					<?=$_REQUEST[inp_email] ?>
					<input type="hidden" name="inp_email" value="<?=$_REQUEST[inp_email]?>" />
					<input type="hidden" name="inp_confemail" value="<?=$_REQUEST[inp_email]?>"  />
				</td>
			</tr>
			<tr class="event">
				<th>日本出発日</th>
				<td> <?=$_REQUEST[input_date_yy]?>年 <?=$_REQUEST[input_date_mm]?>月 <?=$_REQUEST[input_date_dd]?>日 
				
					<input type="hidden" name="input_date_yy" value="<?=$_REQUEST[input_date_yy]?>"  />
					<input type="hidden" name="input_date_mm" value="<?=$_REQUEST[input_date_mm]?>"  />
					<input type="hidden" name="input_date_dd" value="<?=$_REQUEST[input_date_dd]?>"  />
				</td>
			</tr>
			<tr>
				<th>コースNo. </th>
				<td>
                
                		<? 
								$jtb_no2 = $_REQUEST[input_jtb_no2];
								$jtb_no3 = $_REQUEST[input_jtb_no3];
							
								if (trim ($jtb_no2)  == ''){
									$jtb_no2 = '***';
								}
								
								if (trim ($jtb_no3)  == ''){
									$jtb_no3 = '***';
								}
								
						?>
                
                
                
                
					<?=$_REQUEST[input_jtb_no1].'–'.$jtb_no2.'–'.$jtb_no3 ?>			
					<input type="hidden" name="input_jtb_no1" value="<?=$_REQUEST[input_jtb_no1]?>" />
					<input type="hidden" name="input_jtb_no2" value="<?=$_REQUEST[input_jtb_no2]?>" />
					<input type="hidden" name="input_jtb_no3" value="<?=$_REQUEST[input_jtb_no3]?>" />
				
				</td>
			</tr>
			<tr class="event">
				<th>お問合せ内容 </th>
				<td class="fixwrop">
				<?=nl2br($_REQUEST[inp_content_note])?>
				<textarea name="inp_content_note"  class="input_hide"><?=$_REQUEST[inp_content_note]?></textarea>
				</td>
			</tr>
			<? if ($code == 'th'){ ?>
			<!--only thailand-->
			<tr>
				<th>都市 </th>
				<td>
					<?=$_REQUEST[inp_branch] ?>
					<input type="hidden" name="inp_branch"  value="<?=$_REQUEST[inp_branch]?>"  />
				</td>
			</tr>
			<!--only thailand-->
			<? } ?>
			
		</table>
		
		<div class="groupBtnStep1">
		
		<div class="grupBtnStep2">
			<input type="hidden" name="inp_confirm1" value="<?=$_REQUEST[inp_confirm1] ?>"  />
			<input type="hidden" name="inp_confirm2" value="<?=$_REQUEST[inp_confirm2] ?>"  />
			
			<input type="submit"  name="btn_back" value="戻って修正する " class="btn btnBack" />
			<span class="btnSpace"></span>
			<input type="submit"  name="btn_submit" value="送信する" class="btn btnSubmit"  />
		</div>
		<div class="clear"></div>
		
		
		<!--goblesign-->
		<div style="height:30px;"></div>
        <div style="float:right; margin-right:5px; height:100px;">
            <span id="ss_gmo_img_wrapper_100-50_image_ja">
            <a href="https://jp.globalsign.com/" target="_blank">
            <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
            </a>
            </span>
            <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
         </div>
		 <div style="height:40px;"></div>
		<!--goblesign-->
		
		
		
		</div>
	</div>
	</form>
	<!--content form step2-->
	<? } else if ($step==3){ ?>	
	
	
	</div>
	
	
	<div class="inquery">
		<!--contentbottom-->
		<p class="m_left10">	お問合せいただき誠にありがとうございました。<br/>
		回答まで今しばらくお待ちくださいませ。<br/><br/>
			
		※お問合せ内容の確認が各国の土日・祝祭日により、４８時間以内に返信ができない場合がございます。<br/>
		※お問合せ受領メールが30分以内ぬ届かない場合は再度ご入力ください。<br/>
		※回答にお時間をいただくことや、ご希望に添えない場合がありますので、予めご了承ください。
		</p>
		
		<div class="marginCenter mLinkDone">
			Look JTB Rakuなびサポートページへ戻る<br/>
			<a href="http://www.jtb.co.jp/lookjtb/rakunavi/" target="_blank">http://www.jtb.co.jp/lookjtb/rakunavi/</a>
		</div>
		
		<!--goblesign-->
		<div style="height:30px;"></div>
        <div style="float:right; margin-right:5px; height:100px;">
            <span id="ss_gmo_img_wrapper_100-50_image_ja">
            <a href="https://jp.globalsign.com/" target="_blank">
            <img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
            </a>
            </span>
            <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js"></script>
         </div>
		 <div style="height:40px;"></div>
		<!--goblesign-->
		
		
		<div class="groupBtnStep1"></div>
	</div>
		
		
		
		
		
		
	
	<!--content form step3-->
	<!--content form step3-->
	
	<? } ?>
		
		
		
		
		
		
		
		
		
		
	</div>
	<!--content -->
	
	
	
</div>
	
	
<!--footer-->
<div class="footer">
	<div class="footerContent">
		<p>Copyright © 2013 MyBus-Asia.com All rights reserved</p>
	</div>
</div>
<!--footer-->
</body>
</html>