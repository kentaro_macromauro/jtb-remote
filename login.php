<? 
ob_start(); 
@ini_set('display_errors'     , 'off');
@ini_set('upload_max_filesize', '100M');
@ini_set('default_charset'    , 'utf-8');
session_start();


	$action = "";
	
	if (!empty($_POST[inp_user]) && !empty($_POST[inp_pass]))
	{
		if (($_POST[inp_user] == "admin") && ($_POST[inp_pass] == "mybus2012"))
		{
			$action = "login";	
			$_SESSION['NPG'] = "demo";
			header('Location: hongkong/');
			
		}
		else
		{
			$action = "";	
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My-Bus</title>
</head>
<style type="text/css">
* {
	padding:0px;
	margin:0px;
}
div, span, input {
	padding:0px;
	margin:0px;
}
.center {
	width:450px;
	margin:auto;
	height:450px;
}
.header {
	padding-top:25px;
	padding-bottom:10px;
}
.login {
	padding-top:25px;
	padding-left:30px;
	padding-bottom:10px;
}
.sub_login {
	height:25px;
	padding-bottom:10px;
}
.sub_login span {
	width:110px;
	display:block;
	float:left;
	line-height:20px;
}
.sub_login input {
	width:200px;
}
.sub_submit {
	padding-left:110px;
}
.sub_submit .btn {
	width:70px;
	height:25px;
}
</style>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('input[name$="inp_user"]').focus();	
	}
);
</script>
<body>
<div class="center">
  <div class="header"> <img alt="Mybus Logo" src="images/mybus-logo.jpg"> <img alt="JTB Logo" src="images/logo.jpg"> </div>
  <form action="?" method="post">
    <div class="login">
      <div class="sub_login"><span>USER NAME : </span>
        <input type="text" name="inp_user" value=""  />
      </div>
      <div style="clear:both;"></div>
      <div class="sub_login"><span>PASSWORD : </span>
        <input type="password" name="inp_pass" value="" />
      </div>
      <div class="sub_submit">
        <input type="submit" class="btn" value="Login" />
      </div>
    </div>
  </form>
</div>
</body>
</html>