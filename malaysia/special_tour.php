<?
include("include/setting.php");

require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../';

require_once($path . "www_config/setting.php");

require_once($path . "class/include/c_query.php");
require_once($path . "class/c_query_sub.php");
require_once($path . "class/c_common.php");
require_once($path . 'webapp/libs/Smarty.class.php');
require_once($path . 'class/c_service.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path . "include/config.php");

$config['allot'] = 'true';

$breadcamp = output_breadcamp(array('TOP',$site_name, $site_name.'オプショナルツアー','予約する'),array('../','./','./opt.php'));

/* For Menu Style */
include("../include/country_top_navi.php");

/* per service */
$smarty = new Smarty;
$smarty->assign("config",$config);
$smarty->assign("menu_selected_style",$menu_selected_style);
$smarty->display('country_special_tour.tpl');
?>