﻿<?php

include("../include/setting.php");

require_once($_SERVER['DOCUMENT_ROOT']."/dev_define.php");
$path = DEV_PATH.'../../';


require_once($path . "www_config/setting.php");

require_once($path . "class/include/c_query.php");
require_once($path . "class/c_query_sub.php");
require_once($path . "class/c_common.php");
require_once($path . 'webapp/libs/Smarty.class.php');
require_once($path . 'class/c_service.php');
require_once($path."class/phpmailer/class.phpmailer.php");

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

require_once($path . "include/config.php");


$packetname = $_POST['packetname'];
$username1 = $_POST['username1'];
$username2 = $_POST['username2'];
$hotel = $_POST['hotel'];
$tlp = $_POST['tlp'];
$email = $_POST['email'];
$date0 = $_POST['date0'];
$date1 = $_POST['date1'];
$date2 = $_POST['date2'];
$itemcost1 = $_POST['itemcost1'];
$adult = $_POST['adult'];
$kid = $_POST['kid'];
$totalcost = $_POST['totalcost'];
$itemname1 = $_POST['itemname1'];
$itemname2 = $_POST['itemname2'];
$toUser = $email;




//メール送信
date_default_timezone_set('Asia/Tokyo');
$now = date('Y年m月d日 H時i分s秒');


if ($_POST['date3'] != 'undefined年undefined月undefined日'){
    $date3 = $_POST['date3'];
    $itemname3 = $_POST['itemname3'];


    $data = array(
                "packetname"     => $packetname,
                 "date1"  => $date1,
                "date2"   => $date2,
                "date3"   => $date3

            );


    $mail_tempage = '
    この度はJTBツアーにお問合せいただきまして誠にありがとうございます。
    お問い合わせいただきました内容は下記の通りでございます。
    ご確認の程お願い申し上げます。
    ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    氏名　（漢字）'.$username1.'
    氏名　（フリガナ）'.$username2.'
    メールアドレス '.$email.'
    日本出発日 '.$date0.'
    宿泊先名（ホテル名）'.$hotel.'
    お客様の電話番号 '.$tlp.'


    '.$packetname.'
    '.$itemname1.$date1.'
    '.$itemname2.$date2.'
    '.$itemname3.$date3.'

    価格'.$itemcost1.'
    '.$adult.'
    '.$kid.'
    '.$totalcost.'



    ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ●お問合せいただいた後、3日以内に商品のご予約メールが届きます。
    現在、ご予約・手配は完了しておりませんのでご注意ください。
    尚、後日お送りするメール内容は、ご予約の可否、またご予約が可能だった場合のお支払
    い方法・日時・集合時間・料金・クーポン受け渡し（一部の商品）・当日の注意（パスポート
    持参、その他）等のご連絡が目的となります。
    その他旅行条件に関する重要な事項が記載されておりますので、必ずご一読ください。

    ●3日以内に当社からのEメールが届かない場合には、何らかの障害が発生した
    可能性がありますので、お手数ですが、タイトルに「再送」とご表示の上でご再送いただけ
    ますようお願いいたします。
    メールを受信次第、可能な限り速やかにご連絡をいたします。
    ';

    $html = nl2br($mail_tempage);


}else{
    $data = array(
        "packetname"     => $packetname,
        "date1"  => $date1,
        "date2"   => $date2
    );
    $mail_tempage = '

    この度はJTBツアーにお問合せいただきまして誠にありがとうございます。
    お問い合わせいただきました内容は下記の通りでございます。
    ご確認の程お願い申し上げます。
    ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    氏名（漢字） '.$username1.'
    氏名（フリガナ） '.$username2.'
    メールアドレス  '.$email.'
    日本出発日  '.$date0.'
    宿泊先名（ホテル名） '.$hotel.'
    お客様の電話番号  '.$tlp.'

    '.$packetname.'
    '.$itemname1.$date1.'
    '.$itemname2.$date2.'

    価格 : '.$itemcost1.'
    '.$adult.'
    '.$kid.'
    '.$totalcost.'

    ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ●お問合せいただいた後、3日以内に商品のご予約メールが届きます。
    現在、ご予約・手配は完了しておりませんのでご注意ください。
    尚、後日お送りするメール内容は、ご予約の可否、またご予約が可能だった場合のお支払
    い方法・日時・集合時間・料金・クーポン受け渡し（一部の商品）・当日の注意（パスポート
    持参、その他）等のご連絡が目的となります。
    その他旅行条件に関する重要な事項が記載されておりますので、必ずご一読ください。

    ●3日以内に当社からのEメールが届かない場合には、何らかの障害が発生した
    可能性がありますので、お手数ですが、タイトルに「再送」とご表示の上でご再送いただけ
    ますようお願いいたします。
    メールを受信次第、可能な限り速やかにご連絡をいたします。
    ';

    $html = nl2br($mail_tempage);

}

$jtb_email = email_by_country($site_country);

$mail = new PHPMailer();
$mail->AddAddress($toUser);
$mail->AddAddress($jtb_email);
$mail->AddBcc('nagatomo_i.hq@jtbap.com');
$mail->CharSet = 'UTF-8';
$mail->Subject =  "この度はJTBツアーにお問合せいただきまして誠にありがとうございます。";
$mail->MsgHTML('<html><body><div style="width:600px; display:block;">'.$html.'</div></body></html>');
$mail->SetFrom($jtb_email);

if (!$mail->Send()){
    $result = $mail->ErrorInfo;
}
?>