<? 
include("include/setting.php");

if ($site_country == 'AUS'){
	
	$redirect_link = '';	
	
	switch ($_REQUEST[id]){
		
		case 'info.html' 		: $redirect_link = 'http://www.jtb.com.au/travelinfo/general/'; 	break; 
		case 'sydney.html' 		: $redirect_link = 'http://www.jtb.com.au/travelinfo/sydney/'; 		break;
		case 'gold_coast.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/goldcoast/'; 	break;
		case 'cairns.html' 		: $redirect_link = 'http://www.jtb.com.au/travelinfo/cairns/'; 		break;
		case 'melbourne.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/melbourne/'; 	break;
		case 'perth.html' 		: $redirect_link = 'http://www.jtb.com.au/travelinfo/perth/'; 		break;
		case 'ayersrock.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/ayersrock/'; 	break;
		case 'adelaide.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/adelaide/'; 	break;
		case 'tasmania.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/tasmania/'; 	break;
		case 'brisbane.html' 	: $redirect_link = 'http://www.jtb.com.au/travelinfo/brisbane/'; 	break;
		case 'darwin.html' 		: $redirect_link = 'http://www.jtb.com.au/travelinfo/darwin/'; 		break;
		case 'hamilton_island.html' : $redirect_link = 'http://www.jtb.com.au/travelinfo/hamilton/'; break;
		case 'tourist_desk.html' 	: $redirect_link = 'http://www.jtb.com.au/guide/tourdesk/'; 	break;
		case 'qa.html'			: $redirect_link = 'http://www.jtb.com.au/guide/faq/'; 				break;
		

	}
	
	if ($redirect_link){
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header('Location: '.$redirect_link); 
	}
}


include("../public/country/page.php");



?>