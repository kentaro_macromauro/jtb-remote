<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Souvenir Special Campaign Page (CMS) : New';
	$sys_name = 'souvenir_campaign';
	$data[2] = breadcamp( array('Home','Souvenir Special Campaign Page (CMS)','New'),array('index.php',$sys_name.'.php') ); 
	
	
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'&page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	
	$entrydate = Date('YmdHis');
	
	
	
	if ($_SESSION["session_country"] != 'ALL')
	{
	
		$arr_country = array(0 => array($_SESSION["session_country"]) , 1 => array( $db->view_country_jp($_SESSION["session_country"]) ));
	}
	else
	{
		$arr_country = array(0 => array('ALL') , 1 => array('ALL'));
	
		$result_country = $db->get_country();
	
		for ($i = 0; $i < count($result_country[0] ) ; $i++)
		{
			array_push($arr_country[0],$result_country[0][$i]);
			array_push($arr_country[1],$result_country[1][$i]);
		}
		
	}
	
	
	$data[3][inp_country] = input_selectbox('inp_country_promo',$arr_country[1],$arr_country[0],'','-------Please Select-------','inp_cx1 selreq-country');
	
	$data[3][inp_product_type] = input_selectbox('inp_type_promo',array("OPT"),array("OPT"),'','Please Select','inp_cx0');
	$data[3][inp_product_content] = input_memobox('inp_desc','','','inp_cx4',10);


	$data[3][inp_product_id]   = '<li><select name="inp_product[]" class="inp_cx4 selreq-product"><option value="0" >------------------------------------------Please Select------------------------------------------</option></select></li>';	


	$data[3][inp_product_name] = input_textbox('inp_banner_name','','inp_cx3').input_hiddenbox('entrydate',$entrydate);
	$data[3][inp_public]       = input_chkbox('inp_public','true').' Yes';
 
 	function upload_product($img,$entrydate)
	{
		
		return  '
				 	<p class="product-img'.$img.'"></p>
				 	<input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
				 	<input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />
				 ';
				
	}
	
	
	$data[3][inp_banner] =  upload_product(1,$entrydate);
	
	
	$data[3]['script']   = '$(\'select[name$="inp_country_promo"],select[name$="inp_type_promo"]\').change(
								function()
								{					
									$.post(\'ajax_view_souvenir_custom.php\',
									{ 
									  value1 : $(\'select[name$="inp_country_promo"]\').val(), 
									  value2 : $(\'select[name$="inp_type_promo"]\').val()  
									},
									function(data) 
									{
										
										$(\'select[name$="inp_product[]"]\').parent().html(data);
									});								
								}
							);
	
							
	
							$(\'input[name$="inp_addproduct"]\').click(
								function()
								{
									
									_list = ( $(this).parent().parent().find(\'ul li:first-child\').html())  ;
									
									$(this).parent().parent().find(\'ul\').append( \'<li>\' +_list + \'<\/li>\' );
								
								}
							);';
	
	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];
	

	$themes = new c_themes("backend_input_souvenir_campaign","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

