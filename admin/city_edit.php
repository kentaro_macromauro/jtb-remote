<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'City : Update';
	$sys_name = 'city';
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	

	

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_city($_GET[id]);
	
	
	$record = $db->get_country();
	
	$arr_data  = $record[1];
	$arr_value = $record[0];	
	
	if ($raw_data[5] == '1') { $status = true; }
	else{ $status = false;}
	
	
	
	$data[3][0] = table_data('Country',input_selectbox('inp_country',$arr_data,$arr_value,$raw_data[1],'Please Select','inp_cx0'),'','',1);
	$data[3][1] = table_data('ISO3 Code',input_textbox('inp_iso3',$raw_data[4],'inp_cx'),'','',1);
	$data[3][2] = table_data('City Name EN',input_textbox('inp_name_en',$raw_data[2],'inp_cx2'),'','',1);
	$data[3][3] = table_data('City Name JP',input_textbox('inp_name_jp',$raw_data[3],'inp_cx2'),'','',1);
	$data[3][4] = table_data('Status',input_chkbox('inp_chkbox', $status  ).' Other ');
		

	$data[2] = breadcamp( array('Home','City',$raw_data[2]),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>