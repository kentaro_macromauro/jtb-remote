<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'branch_content';
	$tb_name  = _DB_PREFIX_TABLE.'branch_content';
	
	$path = 'tmp/';
	$path_move  = '../article_img/'.$_SESSION[session_path].'/content/';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('branchct_id','country_iso3','branchct_subject','branchct_countent_short','branchct_countent','public','update_date','update_by'); 
			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_country]))
				{
					$inp_country      = $_POST[inp_country];
					$inp_subject      = $_POST[inp_subject];
					$inp_short_description = $_POST[inp_short_description];
					$inp_description  = $_POST[inp_description];
					$inp_public       = $_POST[inp_public];
					$public = '0';
					
					if ($inp_public == true)
					{
						$public = '1';
					}
					else
					{
						$public = '0';	
					}
					
										
					$arr_record = array($inp_country,$inp_subject, $inp_short_description, $inp_description,$public,'datetime', $user_id);
					
					array_shift($arr_carrer);
					
					$db->set_insert($tb_name,$arr_carrer,$arr_record);
					$record_id = $db->insert_id();
					
					
					@mkdir($path);
					
					
						
					@mkdir($path_move);
					$start_pos = strlen($path);
						
					foreach (glob($path.'*') as $filename)
					{

						if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							
							$file_count =  substr($filename,19,strlen($filename));
							$file_name  = explode('-',$file_count);
							
							@unlink($path_move.$record_id.'-'.$file_name[0]);
							@unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');
								
							copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
							@unlink($filename);
								
						}
					}
					
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$idx		 	= $arr_carrer[0];
					array_shift($arr_carrer);

					$inp_subject      = $_POST[inp_subject];
					$inp_description  = $_POST[inp_description];
					$inp_short_description = $_POST[inp_short_description]; 
					$inp_public       = $_POST[inp_public];
					$public = '0';

					
					if ($inp_public == true)
					{
						$public = '1';
					}
					else
					{
						$public = '0';	
					}
							
					
					$db->set_update($tb_name,array('branchct_subject','branchct_countent_short','branchct_countent','public','update_date','update_by'),
									array( $inp_subject,$inp_short_description , $inp_description, $public,'datetime', $user_id),
									'branchct_id',$page_id);	
					
					
					
					@mkdir($path);
						
					@mkdir($path_move);
					$start_pos = strlen($path);
						
					foreach (glob($path.'*') as $filename)
					{
						
							
						if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							$file_count = substr($filename,19,strlen($filename));
							$file_name  = explode('-',$file_count);
								
							@unlink($path_move.$page_id.'-'.$file_name[0]);
							@unlink($path_move.$page_id.'-'.$file_name[0].'.jpg');
								
							copy($filename,$path_move.$page_id.'-'.$file_name[0].'.jpg');
							
							@unlink($filename);
							
						}
					}		
					
					
					
					
				}

			break;
			
			case 'active': 
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_public_array($tb_name,$arr_carrer[0],$arr_id);		
			
			
			break;
			
			case 'unactive':
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_unpublic_array($tb_name,$arr_carrer[0],$arr_id);		
			break;
			
			
			case 'delete' :
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
				@mkdir($path_move);
				
				
				
				foreach (glob($path_move.'*') as $filename)
				{
					$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
					$file   = $record[0];	
					
					if (in_array($file,$arr_del,true))
					{

						unlink($filename);
					}
				}
			
			
			break;
		}
	}

	
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>