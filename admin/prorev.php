<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

function code_public($status)
{
	if ($status == 1) {return 'YES';}
	else  {return 'NO';}
}

/*------------------------------------view table-------------------------------------------------------------------*/

	$data[0] = 'Product Review';	/*----set h1 name-------*/
	$data[5] = 'prorev';  /* system page name */
	$data[1] = breadcamp( array('Home','Product Review'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('Publish','30'),
					 tb_head('Product Id','60'),
					 tb_head('Country','40'),
					 tb_head('City','40'), 
					 tb_head('Product Name (EN)','273'),
					 tb_head('Product Name (JP)','273')) ;
	/*-----------set header table-------------*/

	/*---fillter---*/
	$data['filter'] = '<form action="product.php" method="get">
						<table class="filter_option" ><tr><td width="170">Country : <select name="filter_country"><option value="">---Select All---</option>';
	$option_contry = $db->get_country();
	
	
	/*for ($i=0; $i<count($option_contry[0]); $i++)
	{
		if ( trim($_GET[filter_country]) == trim($option_contry[0][$i]) )
		{
			$data['filter'] .= '<option value="'.$option_contry[0][$i].'" selected="selected" >'.$option_contry[1][$i].'</option>';
		}
		else
		{
			$data['filter'] .= '<option value="'.$option_contry[0][$i].'">'.$option_contry[1][$i].'</option>';
		}
	}
	
	
	$data['filter'] .= '</select></td>';
	$data['filter'] .= '<td width="170">City : <select name="filter_city"><option value="">---Select All---</option>';
	
	$option_city = $db->select_city(trim($_GET[filter_country]));
	for ($i=0; $i<count($option_city[value]); $i++)
	{
		if ( trim($_GET[filter_city]) == trim($option_city[value][$i]) )
		{
			$data['filter'] .= '<option value="'.$option_city[value][$i].'" selected="selected" >'.$option_city[data][$i].'</option>';	
		}
		else
		{
			$data['filter'] .= '<option value="'.$option_city[value][$i].'" >'.$option_city[data][$i].'</option>';
		}
	}
	$data['filter'] .= '</select> </li>';
	*/
	
	
	if ( ($_SESSION[session_country] == "ALL") /*&&  !empty($_SESSION[session_country])*/ )				
	{				
		$data['filter'] = '<form action="product.php" method="get"><table class="filter_option" ><tr>';	
		
		$data['filter'] .= '<td width="170">Country : <select name="filter_country"><option value="">---Select All---</option>';
		$option_contry = $db->get_country();
		for ($i=0; $i<count($option_contry[0]); $i++)
		{
			if ( trim($_GET[filter_country]) == trim($option_contry[0][$i]) )
			{
				$data['filter'] .= '<option value="'.$option_contry[0][$i].'" selected="selected" >'.$option_contry[1][$i].'</option>';
			}
			else
			{
				$data['filter'] .= '<option value="'.$option_contry[0][$i].'">'.$option_contry[1][$i].'</option>';
			}
		}
		$data['filter'] .= '</select>';
		
		$data['filter'] .= '</td>';
		$data['filter'] .= '<td width="170">City : <select name="filter_city"><option value="">---Select All---</option>';
		
		$option_city = $db->select_city( trim($_GET[filter_country]) );
	
	}
	else
	{
		
		
		$countryjp = $db->view_country_jp($_SESSION["session_country"]);
	
		
		$data['filter']  = '<form action="product.php" method="get"><table class="filter_option" ><tr>';
		
		$data['filter'] .= '<td width="170">Country : '.$countryjp.'</td>';
		$data['filter'] .= '<td width="200">City : <select name="filter_city"><option value="">---Select All---</option>';
		
		$option_city = $db->select_city(trim($_SESSION[session_country]));
	}
	
	for ($i=0; $i<count($option_city[value]); $i++)
	{
		if ( trim($_GET[filter_city]) == trim($option_city[value][$i]) )
		{
			$data['filter'] .= '<option value="'.$option_city[value][$i].'" selected="selected" >'.$option_city[data][$i].'</option>';	
		}
		else
		{
			$data['filter'] .= '<option value="'.$option_city[value][$i].'" >'.$option_city[data][$i].'</option>';
		}
	}
	
	
	$data['filter'] .= '</select> </li></td>';
	

	
	
	
	$data['filter'] .= '<td width="270">Product ID : <input type="text" name="filter_code" value="'.$_GET[filter_code].'" /></td>';	
	$data['filter'] .= '<td colspan="2">Theme : <select name="filter_theme"><option value="">---Select All---</option>';

	
	/*$option_category = $db->select_category();
	
	
	for ($i=0; $i<count($option_category[value]); $i++)
	{
		if ( trim($_GET[filter_theme]) == trim($option_category[value][$i]) )
		{
			$data['filter'] .= '<option value="'.$option_category[value][$i].'" selected="selected" >'.$option_category[data][$i].'</option>';	
		}
		else
		{
			$data['filter'] .= '<option value="'.$option_category[value][$i].'" >'.$option_category[data][$i].'</option>';
		}
	}
	$data['filter'] .= '</select> </td></tr>'; */
	
	
	
	
	$data['filter'] .= '<tr><td colspan="2">Product Name (EN) : <input type="text" name="filter_name_en" value="'.$_GET[filter_name_en].'" /></td>';
	$data['filter'] .= '<td colspan="2">Product Name (JP) : <input type="text" name="filter_name_jp" value="'.$_GET[filter_name_jp].'" /></td>';
	
	
	
	$data['filter'] .= '<td><div style="padding-left:45px;"><input type="submit" name="inpSearch" class="btn-search" value="Search" /></div></li> </td></tr></table></form>';
	
	$filter = array();
	
	
	if ($_SESSION[session_country]=="ALL")
	{ 
		$filter_country = trim($_GET[filter_country]);
	}
	else
	{	
		$filter_country = $_SESSION[session_country]; 
	}
	
	
	
	
	
	$filter = array( 'country' => $filter_country,
					  'city' => trim($_GET[filter_city]),
					  'theme' => trim($_GET[filter_theme]),
					  'code' => trim($_GET[filter_code]),
					  'name_en' => trim($_GET[filter_name_en]),
					  'name_jp' => trim($_GET[filter_name_jp]),
					  'time' => trim( $_GET[filter_time] ),
                      'data_type' => 'MBA'
                      );
	



	/*---fillter---*/

	/*-----------find now page----------------*/
	$page_num = 20;
	
	
	$page_count = $db->count_product($filter);
	
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fatch_product($page_first,$page_num,$filter);	

	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&filter_country='.trim($_GET[filter_country]).'&filter_city='.trim($_GET[filter_city]).'&filter_theme='.trim($_GET[filter_theme]).'&filter_code='.trim($_GET[filter_code]).
					  '&filter_name_en='.trim($_GET[filter_name_en]).'&filter_name_jp='.trim($_GET[filter_name_jp]).
					  '&id=';
	
	for ($i=0;$i< count($record['id']); $i++)
	{
		$link  = $rec_linkprefix.$record['id'][$i];
		
		
		if ($record['rev_status'][$i] == 0)
		{
			$other_status = '<div class="bg_noneapprove">&nbsp;</div>';	
		}
		else
		{
			$other_status = '<div class="bg_approve">&nbsp;</div>';	
		}
		
		
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record['id'][$i]), 
			   $other_status,
			   
			   $record['product_code'][$i], 
			   $record['country_iso3'][$i], 
			   $record['city_iso3'][$i],
			   $record['product_name_en'][$i],
			   $record['product_name_jp'][$i]),
		array('','',$link,$link,$link,$link,$link,$link,$link),
		array(2,1,0,1,1,0,0,0));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?filter_country='.trim($_GET[filter_country]).'&filter_city='.trim($_GET[filter_city]).
												'&filter_theme='.trim($_GET[filter_theme]).   
												'&filter_code='.trim($_GET[filter_code]).
												'&filter_name_en='.trim($_GET[filter_name_en]).
												'&filter_name_jp='.trim($_GET[filter_name_jp]).
												'&page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table_product_rev","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>