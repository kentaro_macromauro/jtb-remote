<?

require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$page_num = 100;
$page = $_GET['page'];
if (!$_GET['page']){
    $page = 1;
}
$previous_page = $page - 1;
$next_page = $page + 1;

$offset = $page_num * $previous_page;

$data_count_sql = 'SELECT count(*) dd_count FROM `mbus_api_result` `a`
			LEFT JOIN `mbus_country` `b` ON `a`.`country_iso3` = `b`.`country_iso3`
			ORDER BY `a`.`update_date` DESC';
$result=  $db->db_query($data_count_sql);
while ($record = mysql_fetch_array($result)){
    $data_num = $record['dd_count'];
}

$page_html = null;
$page_html .= ($page == 1)? '': '<a class="page_link pagination_num" href="?page='.$previous_page.'"><< 前</a>';
$page_html .= ($data_num > ($page_num*$page))? '<a class="page_link pagination_num" href="?page='.$next_page.'">次 >></a>' : '';

$sql = 'SELECT `a`.`result_id` AS `result_id`, `b`.`country_name_jp` AS `country_name`, `a`.`data_no` AS `data_no`, `a`.`result` AS `result`, `a`.`update_date` AS `date`
			FROM `mbus_api_result` `a`
			LEFT JOIN `mbus_country` `b` ON `a`.`country_iso3` = `b`.`country_iso3`
			ORDER BY `a`.`update_date` DESC LIMIT '.$offset.', '.$page_num;

$result=  $db->db_query($sql);
$data = array();

while ($record = mysql_fetch_array($result)){
    $data[] = $record;
}
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>API Result Report</title>
        <script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
    </head>

    <style type="text/css">
        tr.border-bottom td{ border-top:#333 solid 1px;}
        body table.table_data tr.alert td{background:#FFC;}
    </style>

    <link type="text/css" href="common/css/main.css" rel="stylesheet" />
    <body>

    <div class="data">
        <div class="table" align="center">
            <table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
                <tr>
                    <td colspan=16 align=center height=30><b>API Result Report</b>
                    </td>
                </tr>
                <tr>
                    <th align=center>No</th>
                    <th align=center>Country</th>
                    <th align=center>Data No.</th>
                    <th align=center>Result</th>
                    <th align=center>Date</th>
                </tr>
                <? foreach ($data as $txt){ ?>
                    <tr>
                        <td align="center"><?=$txt['result_id']?></td>
                        <td align="left"><?=$txt['country_name']?></td>
                        <td align="right"><?=$txt['data_no']?></td>
                        <td align="right"><?=$txt['result']?></td>
                        <td align="center"><?=$txt['date']?></td>
                    </tr>
                <? } ?>
            </table>
        </div>
        <div class="number" style="text-align: center">
            <div class="page_navi">
                <?=$page_html?>
            </div>
        </div>
        <p style="text-align:right">Copyright © 2013 MyBus-Asia.com All rights reserved </p>
    </div>

    </body>
    </html>
