<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
    $data[1] = 'FOOTER Banner : New';
    $sys_name = 'banner_footer';
    $data[2] = breadcamp( array('Home','Footer Banner','New'),array('index.php',$sys_name.'.php') ); //array name, array link

    $data[0] = array($sys_name.'_action.php',$sys_name);

    if (!empty($_GET[page]))
    {
        $page_back = $data[0][0].'&page='.$_GET[page];
    }
    else
    {
        $page_back = $data[0][0];
    }

    $page_submit = $data[0][0];
    $data[0][0]  = $page_submit;

    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $entrydate = Date('YmdHis');

    if($_SESSION[session_country] == 'ALL'){
        $data[3][] = 'TOP';
    }else{
        $data[3][] = $db->view_country_jp($_SESSION[session_country]);
    }
    $data[3][] = input_textbox('inp_idx','','inp_cx01 inpreq-idx').'&nbsp;(1-99)&nbsp;&nbsp;1 is most high';
    $data[3][] = input_radio_line('inp_jump',array('Yes', 'No'), array(1,0), 1);
    $data[3][] = input_chkbox('inp_new_window').'&nbsp;Yes';
    $data[3][] = input_textbox('inp_link','','inp_cx3 inpreq-name').input_hiddenbox('entrydate',$entrydate).input_hiddenbox('inp_country_pro',$_SESSION[session_country]);
    $data[3]['inp_public']  = input_chkbox('inp_public',true).'&nbsp;Yes';

    function upload_product($img,$entrydate,$width,$height)
    {
        return  '
				 	<p class="product-img'.$img.'"></p><p class="inp_img"></p>
				 	<input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
				 	<input type="button" id="buttonUpload" class="button " onclick="return banner_img('.$img.','.$entrydate.','.$width.','.$height.');" value="Upload" />
				 ';
    }

    $data[3][] = upload_product(1,$entrydate, 750, 159);

    $data[4] 	= array(0,'insert');
    $data[5]    = $data[0][0];

    $themes = new c_themes("backend_input_footer_banner","../content/themes_backend/");
    $themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>