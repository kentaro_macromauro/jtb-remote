<? 

require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");

	$sql = 'SELECT * FROM 
			(SELECT a.order_id ,a.order_refid ,a.axis_refid ,a.session_status , DATE_FORMAT(a.update_date,\'%Y-%m-%d\') order_day,
			DATE_FORMAT(a.update_date,\'%Y-%m-%d\') order_day_dd,DATE_FORMAT(a.update_date,\'%h:%i\') order_day_tt, 																				
			a.mailto_customer ,a.country_iso3 ,a.transection_id ,a.product_amount ,
			a.product_qty ,a.paid_type ,a.paid_status ,a.order_firstname ,a.order_surname ,
			a.order_furi_firstname ,a.order_furi_surname ,a.order_sex ,a.order_birthday ,
			a.order_email ,a.order_tel1 ,a.order_tel2 ,a.order_tel3 ,a.order_mobile1 ,
			a.order_mobile2 ,a.order_mobile3 ,a.order_addrtype ,a.order_addrjp_zip1 ,
			a.order_addrjp_zip2 ,a.order_addrjp_pref ,a.order_addrjp_city ,
			a.order_addrjp_area ,a.order_addrjp_building ,a.order_addrovs1 ,
			a.order_addrovs2 ,a.order_rec_firstname ,a.order_rec_surname ,
			a.order_rec_furi_firstname ,a.order_rec_furi_surname  ,
			a.order_rec_addrjp_zip1 ,a.order_rec_addrjp_zip2 ,a.order_rec_addrjp_pref ,
			a.order_rec_addrjp_city ,a.order_rec_addrjp_area ,a.order_rec_addrjp_building ,
			a.order_rec_tel1 ,a.order_rec_tel2 ,
			a.order_rec_tel3 ,a.order_remark ,a.update_date ,a.update_by ,a.status_cms ,
			a.update_date_cms ,a.update_by_cms ,a.status_cms_paid,
			b.order_idx,b.souvenir_id,b.souvenir_code,b.souvenir_price,b.souvenir_qty,b.souvenir_amount ,
			souvenir_name,store_type,short_desc,souvenir_allotment,c.country_name_jp,order_rec,sign,code 
			FROM mbus_souvenir_order a LEFT JOIN 
			mbus_souvenir_order_detail b ON a.order_id = b.order_id LEFT JOIN
			mbus_country c ON a.country_iso3 = c.country_iso3 LEFT JOIN 
			mbus_currency d ON a.country_iso3 =  d.country_iso3  LEFT JOIN 
			mbus_souvenir e ON b.souvenir_id = e.souvenir_id
			) AS aa WHERE ((session_status = 1) OR (session_status = 0)) ';
			
	if ($_REQUEST[f_country]){
		$sql .= $_REQUEST[f_country]=="ALL"||!$_REQUEST[f_country]?'':'AND country_iso3 ="'.$_REQUEST[f_country].'" ';	
	}
			
			
	//$sql .= 'WHERE update_date BETWEEN \'2013-03-01\' AND \'2013-03-30\' ';
	$sql .= 'ORDER BY order_id DESC,order_refid';
			
			
			
			$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
			$db->db_connect();


			$result=  $db->db_query($sql);
			$data = array();
			
			while ($record = mysql_fetch_array($result)){
				$data[] = $record;	
			}
			
			$buff_old_id =0;
	

			
?>
<? 
if ($_REQUEST[export]=='1'){ // export to excel
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="souvenir_report.xls"');
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<body>
	<TABLE x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
		<tr><th colspan=18 align=center height=30>Souvenir Transaction Report</th></tr>
        <tr>
           <th align=center rowspan=2>Order Id</th>
           <th align=center rowspan=2>Date</th>
           <th align=center rowspan=2>Time</th>
           <th align=center rowspan=2>Country</th>   
           <th align=center rowspan=2>Recive</th>
           <th align=center rowspan=2>Paid Type</th>
           <th align=center colspan=6>Order Detail</th>
           <th align=center colspan=6>Customer Detail</th>
           <th align=center colspan=4>Sending information</th>
           <th align=center rowspan=2>Remark</th>
         </tr>
		 <tr>
            <th>Name</th>
            <th>Price</th>
            <th>QTY</th>
            <th>Amount</th>
            <th>Sum Amount</th>
            <th>Sign</th>  
  
            <th>Name</th>
            <th>Name</th>
            
            <th>Email</th>
            <th>Mobile</th>
            <th>Telephone</th> 
            <th>Address</th> 
            
            
            <th>Name</th>
            <th>Name</th>
            <th>Address</th>
            <th>Telephone</th>

         </tr> 

<? $temp_day = '';
			
	foreach ($data as $txt){ ?>
             
                
    <tr>

	<td align=center >
	<?=$txt[order_id] ?>
	</td>
	<td align=left>
	<?=$txt[order_day_dd] ?>
	</td>
    <td align=left>
	<?=$txt[order_day_tt] ?>
	</td>
    <td align=left>
    <?=$txt[country_name_jp] ?>
    </td>
	<td align=center>
	<?
	if ($buff_old_id!=$txt[order_id])
		echo $txt[store_type]==1?'Local country':'In Japan' 
	?>
    </td>
    <td align="center">
    <? 
	if ($txt[session_status] == 0){
		echo 'not complete';
	}
	else{
			
		switch ($txt[paid_type] )
		{
			case 1 : echo ($buff_old_id==$txt[order_id])?'':"Credit" ;  break;
			case 2 : echo ($buff_old_id==$txt[order_id])?'':"Cash" ; break;
			case 3 : echo ($buff_old_id==$txt[order_id])?'':"On Request" ; break;
			default : echo "&nbsp;" ; break;
		}
	}
	?>
    </td>
		<td align=left><?='['.$txt[souvenir_code].'] '.$txt[souvenir_name] ?></td>
        <td align=right><?=$txt[souvenir_price] ?></td>
        <td align=right><?=$txt[souvenir_qty] ?></td>
		<td align=right><?=$txt[souvenir_amount] ?></td>
        <td align=right><?=($buff_old_id==$txt[order_id])?'':$txt[product_amount]?></td>
        <td align=right><?=$txt[sign] ?></td>
        
		<td align=left >
				
		
				<?=($buff_old_id==$txt[order_id])?'':$txt[order_surname].' '.$txt[order_firstname] ?>
              
        </td>
        <td align=left><?=($buff_old_id==$txt[order_id])?'':$txt[order_furi_surname].' '.$txt[order_furi_firstname] ?>
        <td align=left><?=($buff_old_id==$txt[order_id])?'':$txt[order_email] ?></td>
        <td align=left><?=($buff_old_id==$txt[order_id])?'':$txt[order_mobile1].'-'.$txt[order_mobile2].'-'.$txt[order_mobile3] ?></td>
        <td align=left><?=($buff_old_id==$txt[order_id])?'':$txt[order_tel1].'-'.$txt[order_tel2].'-'.$txt[order_tel3] ?></td>
        <td align=left>
        
        	<? 
				if ($buff_old_id!=$txt[order_id]){
					if ($txt[order_addrtype] == 2 ){
						echo  $txt[order_addrovs1].'　'.$txt[order_addrovs2];
					}
					else{
						
						echo '〒'.$txt[order_addrjp_zip1].'-'.$txt[order_addrjp_zip2].'　'.$db->view_pref(trim($txt[order_addrjp_pref]));
						echo '　'.$txt[order_addrjp_city].'　'.$txt[order_addrjp_area].'　'.$txt[order_addrjp_building];
						
						
						
					}
				}
			?>
        
        </td>
        
        <td align=left >
        	<? if ($txt[order_rec]){ 
        
					echo ($buff_old_id==$txt[order_id])?'':$txt[order_rec_surname].' '.$txt[order_rec_firstname];
        		
				}else{ 
        			echo ($buff_old_id==$txt[order_id])?'':$txt[order_surname].' '.$txt[order_firstname];
            	
            	} ?>
        </td>
        <td align=left >
		
        	<? if ($txt[order_rec]){ 
			
					echo ($buff_old_id==$txt[order_id])?'':$txt[order_rec_furi_surname].' '.$txt[order_rec_furi_firstname];
        
			 }else{ 
            		echo ($buff_old_id==$txt[order_id])?'':$txt[order_furi_surname].' '.$txt[order_furi_firstname];
            
             } ?>
        </td>
        <td align=left >
		
        <? if ($txt[order_rec]){ 
        
				 if ($buff_old_id!=$txt[order_id]){
				
								
							echo '〒'.$txt[order_rec_addrjp_zip1].'-'.$txt[order_rec_addrjp_zip2].'　'.$db->view_pref(trim($txt[order_rec_addrjp_pref]));
							echo '　'.$txt[order_rec_addrjp_city].'　'.$txt[order_rec_addrjp_area].'　'.$txt[order_rec_addrjp_building];
	
				}
			
			}else{  
				if ($buff_old_id!=$txt[order_id]){
					if ($txt[order_addrtype] == 2 ){
						echo  $txt[order_addrovs1].'　'.$txt[order_addrovs2];
					}
					else{
						
						echo '〒'.$txt[order_addrjp_zip1].'-'.$txt[order_addrjp_zip2].'　'.$db->view_pref(trim($txt[order_addrjp_pref]));
						echo '　'.$txt[order_addrjp_city].'　'.$txt[order_addrjp_area].'　'.$txt[order_addrjp_building];
					}
				}
			 } ?>
		</td>
        <td align=left >
		
        <? if ($txt[order_rec]){ 
				echo ($buff_old_id==$txt[order_id])?'':$txt[order_rec_tel1].'-'.$txt[order_rec_tel2].'-'.$txt[order_rec_tel3] ;
        		
            }else{ 
				echo ($buff_old_id==$txt[order_id])?'':'mobile:'.$txt[order_mobile1].'-'.$txt[order_mobile2].'-'.$txt[order_mobile3].'<br/>' ;
           	 	echo ($buff_old_id==$txt[order_id])?'':'telphone:'.$txt[order_tel1].'-'.$txt[order_tel2].'-'.$txt[order_tel3];
        } ?>
        </td>
        <td align=left><?=($buff_old_id==$txt[order_id])?'':$txt[order_remark] ?></td>
        
        
	</tr><? $temp_day = $txt[order_day]; $buff_old_id=$txt[order_id]; } ?>        
    </TABLE>
</body>
</html>

<? //export to excel
}
else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Souvenir Transaction Report</title>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
</head>

<style type="text/css">
tr.border-bottom td{ border-top:#333 solid 1px;}
body table.table_data tr.alert td{background:#FFC;}
</style>

<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<body>

<div class="data">
	<div class="table" align="center">
    	<table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
            <tr>
                <td colspan=14 align=center height=30><b>Souvenir Transaction Report</b>
                	<span style="float:right;"><a style="text-decoration:underline;" href="./souvenir_order_report.php?export=1" target="_blank">Export to Excel</a></span>
                </td>
                 
            </tr>
           
           <tr>
           	<td colspan="14" >
            	<form method="get" name="frm" action="?">
                <? $arr_country = array("ALL","TWN","IDN","THA","VNM","KHM","MYS","SGP","HKG","AUS","NZL"); $s=0; ?>
                
            	Filter : Country : <select name="f_country" id="country_name">
                                    <option value="ALL" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>  >---Select All---</option>
                                    <option value="TWN" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>台湾 [Taiwan]</option>
                                    <option value="IDN" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>インドネシア [Indonesia]</option>
                                    <option value="THA" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>タイ [Thailand]</option>
                                    <option value="VNM" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>ベトナム [Vietnam]</option>
                                    <option value="KHM" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>カンボジア [Cambodia]</option>
                                    <option value="MYS" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>マレーシア [Malaysia]</option>
                                    <option value="SGP" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>シンガポール [	Singapore]</option>
                                    <option value="HKG" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>香港 [Hong Kong]</option>
                                    <option value="AUS" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>オーストラリア [Australia]</option>
                                    <option value="NZL" <?=$_REQUEST[f_country]==$arr_country[$s]?'selected="selected"':''; $s++; ?>>ニュージーランド  [New Zealand]</option>
                                   </select>
                     <input type="submit" style="display:none;" value="Submit"  name="submit" />
                                   
               </form>
               <script type="text/javascript">
			   	$(document).ready( 
					function(){
						$('#country_name').change(
							function(){
								$('input[name$="submit"]').click();				   
							}
						);	
					}
				);
			   </script>
               
               
               
            </td>
            
           </tr>
            
            <tr>
                <th width="" nowrap="" align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Order Id</b></th>
                <th nowrap="" align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Order Date</b></th>
                <th nowrap="" align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Country</b></th>
                
                <th nowrap="" align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Recive</b></th>
                <th nowrap="" align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Paid Type</b></th>
                <th nowrap="" align="center" style="width:70px; table-style:fix;" colspan="5" ><b>Order Detail</b></th>
   
                <th align="center" style="width:250px; table-style:fix;" nowrep="" rowspan="2"><b>Customer</b></th>
                <th align="center" style="width:70px; table-style:fix;" rowspan="2"><b>Detail</b></th> 

            </tr>
			<tr>
            	<th><b>Name</b></th>
                <th><b>Price</b></th>
                <th><b>QTY</b></th>
                <th><b>Amount</b></th>
                <th><b>Sum Amount</b></th>
            </tr>        
        	
            <? $temp_day = '';
			
				foreach ($data as $txt){ ?>
                
                	<?  if ($temp_day != $txt[order_day]){
						
							echo '<tr><th colspan="12" align="left" style=" border-top:#333 solid 1px;" >'.$txt[order_day].'</th></tr>';
						
						}

					?>
                
            		<tr <?=($buff_old_id==$txt[order_id])?'class="':'class="border-bottom' ?> <? if ($txt[session_status] == 0) echo 'alert' ?>" >

					<td align="center" >
					<?=($buff_old_id==$txt[order_id])?'':$txt[order_id] ?>
					</td>
					<td align="left">
					<?=($buff_old_id==$txt[order_id])?'':$txt[update_date] ?>
					</td>
                    <td align="left">
                    <?=($buff_old_id==$txt[order_id])?'':$txt[country_name_jp] ?>
                    </td>
					<td align="center">
					<?
						if ($buff_old_id!=$txt[order_id])
						 echo $txt[store_type]==1?'Local country':'In Japan' 
					?>
                    </td>
                    <td align="center">
                    <? 
						if ($txt[session_status] == 0){
							echo 'not complete';
						}
						else{
					
							switch ($txt[paid_type] )
							{
								case 1 : echo ($buff_old_id==$txt[order_id])?'':"Credit" ;  break;
								case 2 : echo ($buff_old_id==$txt[order_id])?'':"Cash" ; break;
								case 3 : echo ($buff_old_id==$txt[order_id])?'':"On Request" ; break;
								default : echo "&nbsp;" ; break;
							}
						}
					?>
                    </td>
                    
                    
					<td align="left"><?='['.$txt[souvenir_code].'] '.$txt[souvenir_name] ?></td>
                   
                    
                    <td align="right"><?=$txt[souvenir_price].' '.$txt[sign]?></td>
                    
                    <td align="right"><?=$txt[souvenir_qty] ?></td>

					<td align="right"><?=$txt[souvenir_amount].' '.$txt[sign]; ?></td>
                    
                    
                    <td align="right"><?=($buff_old_id==$txt[order_id])?'':$txt[product_amount].' '.$txt[sign]?></td>
					 
                     
                     <td align="left" >
                     
					<?=($buff_old_id==$txt[order_id])?'':$txt[order_surname].' '.$txt[order_firstname].' <br/>'.$txt[order_email].
					'<br/>'.$txt[order_mobile1].'-'.$txt[order_mobile2].'-'.$txt[order_mobile3]
					?>
					</td>
                    <td align="center">
                    <?=($buff_old_id==$txt[order_id])?'':'<a href="souvenir_order_report_detail.php?id='.$txt[order_id].'" target="_blank" >Detail</a>' ?>
                    </td>
					</tr><? 
					
						$temp_day = $txt[order_day];
						$buff_old_id=$txt[order_id];
					} ?>
                    
                    
                    
        </table>
    
	</div>
    <p style="text-align:right">Copyright © 2013 MyBus-Asia.com All rights reserved </p>
</div>



</body>
</html>
<? } ?>