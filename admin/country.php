<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	

	if  ($_SESSION[session_country] != "ALL") 
	{
		header("Location: index.php");
	}

/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
	$data[0] = 'Country';	/*----set h1 name-------*/
	$data[5] = 'index'; 
	$current_page = 'country';
	
	/* system page name */
	$data[1] = breadcamp( array('Home','Country'),array('index.php') ); 
	/*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('ISO3 Code','80'),
					 tb_head('Country Name En','240'),
					 tb_head('Country Name Jp','240'),
					 '<th class="tittle" width="120" colspan="2">Child Age</th>',
					 '<th class="tittle" width="120" colspan="2">Infant Age</th>');
			
			

	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->page_count("country");	
	
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fetch_country($page_first,$page_num);	
	
	
	$rec_linkprefix = $current_page.'_edit.php?page='.$page_now.'&id=';

	
	for ($i=0;$i< count($record[0]); $i++)
	{
		if ($record[8][$i] == 1)
		{
			$menu_infant = 'Show';	
		}
		else
		{
			$menu_infant = 'Hide';	
		}
		
		if ($record[9][$i] == 1)
		{
			$menu_child = 'Show';	
		}
		else
		{
			$menu_child = 'Hide';	
		}
		
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   $record[1][$i], 
			   $record[2][$i], 
			   $record[3][$i], 
			   '<div style="width:30px;">'.$record[6][$i].'</div>',
			   $menu_child,
			   '<div style="width:30px;">'.$record[7][$i].'</div>',
			   $menu_infant,
			   ),
		array('',
			  $rec_linkprefix.$record[0][$i], 
			  $rec_linkprefix.$record[0][$i], 
			  $rec_linkprefix.$record[0][$i], 
			  $rec_linkprefix.$record[0][$i], 
			  $rec_linkprefix.$record[0][$i],
			  $rec_linkprefix.$record[0][$i],
			  $rec_linkprefix.$record[0][$i]),
		array(2,1,0,0,1,1,1,1)
		);
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record[0] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $current_page.'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$current_page.'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table_readonly","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>