<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Itinerary : Update';
	$sys_name = 'itinerary';
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	

	

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_itinerary($_GET[id]);
	
	
	$data[3][] = table_data('Itinerary Name En',$raw_data['itiner_name_en'] /*input_textbox('inp_itinerary_name_en',$raw_data['itiner_name_en'],'inp_cx3')*/);
	$data[3][] = table_data('Itinerary Name JP',input_textbox('inp_itinerary_name_jp',$raw_data['itiner_name_jp'],'inp_cx3'));


	$data[2] = breadcamp( array('Home','Theme',$raw_data['itiner_name_en']),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>