<?

require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");

$sql = 'select `a`.`product_code` AS `product_code`,`b`.`country_name_en`
			AS `country_name_en`,`c`.`city_name_en` AS `city_name_en`,`a`.`country_iso3` 
			AS `country_iso3`,`a`.`city_iso3` AS `city_iso3`,`a`.`product_name_en` 
			AS `product_name_en`,`a`.`product_name_jp` AS `product_name_jp`,
			(case `a`.`status_book` when 1 then _utf8\'credit\' when 2 then _utf8\'cash\' 
			when 3 then _utf8\'credit+cash\' end) AS `paid_type`,`a`.`price_min` AS `min_price`,
			`a`.`price_max` AS `max_price`,`a`.`pax_min` AS `min_pax`,`a`.`pax_max` AS `max_pax`,
			`a`.`day_cutoff` AS `cut_of_day`,`a`.`remark` AS `remark`,IF (a.public=1,"YES","NO") publish ,
			CONCAT(\'http://www.mybus-asia.com/\',LOWER(b.country_name_en),\'/product.php?product_id=\',a.product_id) url
			from ((`mbus_product` `a` left join `mbus_country` `b` 
			on((`a`.`country_iso3` = `b`.`country_iso3`))) left join 
			`mbus_city` `c` on(((`a`.`country_iso3` = `c`.`country_iso3`) 
			and (`a`.`city_iso3` = `c`.`city_iso3`))))
            where `a`.`data_type` = "API"
			order by `a`.`country_iso3`,`a`.`city_iso3`,product_code';

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$result=  $db->db_query($sql);
$data = array();

while ($record = mysql_fetch_array($result)){
    $data[] = $record;
}
?>
<?
if ($_REQUEST[export]=='1'){ // export to excel
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename="export_tour.xls"');
    ?>
    <html xmlns:o="urn:schemas-microsoft-com:office:office"
          xmlns:x="urn:schemas-microsoft-com:office:excel"
          xmlns="http://www.w3.org/TR/REC-html40">
    <body>
    <TABLE x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
        <tr>
            <th colspan="16" align="center" height="30">Export Data : Tour</th>
        </tr>
        <tr>
            <th align=center>product_code</th>
            <th align=center>country_name_en</th>
            <th align=center>country_iso3</th>
            <th align=center>city_iso3</th>
            <th align=center>product_name_jp</th>
            <th align=center>min_price</th>
            <th align=center>max_price</th>
            <th align=center>min_pax</th>
            <th align=center>max_pax</th>
            <th align=center>cut_of_day</th>
            <th align=center>remark</th>
            <th align=center>Publish</th>
            <th align=center>url</th>
        </tr>
        <? foreach ($data as $txt){ ?>
            <tr>
                <td align="center" height="20"><?=$txt[product_code]?></td>
                <td align="left" height="20"><?=$txt[country_name_en]?></td>
                <td align="left" height="20"><?=$txt[country_iso3]?></td>
                <td align="left" height="20"><?=$txt[city_iso3]?></td>
                <td align="left" height="20"><?=$txt[product_name_jp]?></td>
                <td align="left" height="20"><?=$txt[min_price]?></td>
                <td align="left" height="20"><?=$txt[max_price]?></td>
                <td align="right" height="20"><?=$txt[min_pax]?></td>
                <td align="right" height="20"><?=$txt[max_pax]?></td>
                <td align="right" height="20"><?=$txt[cut_of_day]?></td>
                <td align="left" height="20"><?=$txt[remark]?></td>
                <td align="left" height="20"><?=$txt[publish]?></td>
                <td align="left" height="20"><?=get_jtb_product_url($txt['product_code'], '', $txt['country_iso3'], $txt['city_iso3'])?></td>
            </tr>
        <? } ?>
    </TABLE>
    </body>
    </html>

    <? //export to excel
}
else{
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Export Data : Tour</title>
        <script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
    </head>

    <style type="text/css">
        tr.border-bottom td{ border-top:#333 solid 1px;}
        body table.table_data tr.alert td{background:#FFC;}
    </style>

    <link type="text/css" href="common/css/main.css" rel="stylesheet" />
    <body>

    <div class="data">
        <div class="table" align="center">
            <table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
                <tr>
                    <td colspan=16 align=center height=30><b>Export Data : Tour</b>
                        <span style="float:right;"><a style="text-decoration:underline;" href="?export=1" target="_blank">Export to Excel</a></span>
                    </td>
                </tr>
                <tr>
                    <th align=center>Code</th>
                    <th align=center>Country</th>
                    <th align=center style="width:150px; table-layout:fixed">Product_Name_JP</th>
                    <th align=center>Min_Price</th>
                    <th align=center>Max_Price</th>
                    <th align=center>Min_Pax</th>
                    <th align=center>Max_Pax</th>
                    <th align=center>Cut_of_day</th>
                    <th align=center>Publish</th>
                    <th align=center>URL</th>
                </tr>
                <? foreach ($data as $txt){ ?>
                    <tr>
                        <td align="center"><?=$txt[product_code]?></td>
                        <td align="left"><?=$txt[country_name_en]?></td>
                        <td align="left"><?=$txt[product_name_jp]?></td>
                        <td align="right"><?=$txt[min_price]?></td>
                        <td align="right"><?=$txt[max_price]?></td>
                        <td align="right"><?=$txt[min_pax]?></td>
                        <td align="right"><?=$txt[max_pax]?></td>
                        <td align="right"><?=$txt[cut_of_day]?></td>

                        <td align="center"><?=$txt[publish]?></td>
                        <td align="left"><a href="<?=get_jtb_product_url($txt['product_code'], '', $txt['country_iso3'], $txt['city_iso3'])?>" target="_blank"><?=get_jtb_product_url($txt['product_code'], '', $txt['country_iso3'], $txt['city_iso3'])?></a></td>
                    </tr>
                <? } ?>
            </table>

        </div>
        <p style="text-align:right">Copyright © 2013 MyBus-Asia.com All rights reserved </p>
    </div>



    </body>
    </html>
<? } ?>