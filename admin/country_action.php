<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'country';
	$tb_name  = _DB_PREFIX_TABLE.'country';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('country_id','jtb_country_id','country_iso2','country_iso3','country_name_en','country_name_jp','path','child_age','infant_age','child_menu','infant_menu','update_date','update_by',); 
			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_iso2]))
				{
					$jtb_country_id  = $_POST[inp_jtb];
					$country_iso2    = $_POST[inp_iso2];
					$country_iso3    = $_POST[inp_iso3];
					$country_name_en = $_POST[inp_name_en];
					$country_name_jp = $_POST[inp_name_jp]; 
					$country_path	 = $_POST[inp_name_path];
					$child_age       = $_POST[inp_child_age];
					$infant_age      = $_POST[inp_infant_age];
					$pu_child		 = $_POST[pu_child];
					$pu_infant		 = $_POST[pu_infant];
					
					if ($pu_child == 'on')
					{
						$pu_child = 1;	
					}
					else
					{
						$pu_child = 0;	
					}
					
					
					if ($pu_infant == 'on')
					{
						$pu_infant = 1;	
					}
					else
					{
						$pu_infant = 0;	
					}
					
					
					
					$arr_record = array($jtb_country_id,$country_iso2,$country_iso3,$country_name_en,$country_name_jp,$country_path,$child_age,$infant_age,$pu_child,$pu_infant,'datetime',$user_id);
					
					array_shift($arr_carrer);
					
					$db->set_insert(_DB_PREFIX_TABLE.'country',$arr_carrer,$arr_record);
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$idx		 	= $arr_carrer[0];
					array_shift($arr_carrer);
					
					
					$jtb_country_id  = $_POST[inp_jtb];
					$country_iso2    = $_POST[inp_iso2];
					$country_iso3    = $_POST[inp_iso3];
					$country_name_en = $_POST[inp_name_en];
					$country_name_jp = $_POST[inp_name_jp]; 
					$country_path	 = $_POST[inp_name_path];
					$child_age       = $_POST[inp_child_age];
					$infant_age      = $_POST[inp_infant_age];
					$pu_child		 = $_POST[pu_child];
					$pu_infant		 = $_POST[pu_infant];
					
					if ($pu_child == 'on')
					{
						$pu_child = 1;	
					}
					else
					{
						$pu_child = 0;	
					}
					
					
					if ($pu_infant == 'on')
					{
						$pu_infant = 1;	
					}
					else
					{
						$pu_infant = 0;	
					}
					

					$db->set_update($tb_name,$arr_carrer,
									array($jtb_country_id,$country_iso2,$country_iso3,$country_name_en,$country_name_jp,$country_path,$child_age,$infant_age, $pu_child,$pu_infant ,'datetime', $user_id),
									$idx,$page_id);	
				}

			break;
				
			case 'delete' :
			
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>