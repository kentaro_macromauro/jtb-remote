<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Souvenir : Update';
	$sys_name = 'souvenir';
	$base_url = ", document_base_url : 'http://127.0.0.1/my-bus/html/'";
	$data[0] = array($sys_name.'_action.php'.'?filter_country='.trim($_GET[filter_country]).'&filter_city='.trim($_GET[filter_city]).
					'&filter_theme='.$_GET[filter_theme].'&filter_code='.trim($_GET[filter_code]).'&page='.$_GET[page].
					'&filter_name_en='.trim($_GET[filter_name_en]).'&filter_name_jp='.trim($_GET[filter_name_jp]).'&filter_public='.trim($_GET[filter_public]),'');
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	$page_back = $data[0][0];		
		

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$obj = $db->view_souvenir($obj_id);
	$raw_data = $obj[0];
	
	
	
	$arr_country =  $db->get_country() ;
	

	
	if  ($_SESSION[session_country] == "ALL")
	{	
		$data[3][inp_country] = input_selectbox('inp_country',$arr_country[1],$arr_country[0],$raw_data['country_iso3'],'-------Please Select-------','inp_cx1').'<span class="msg-req">*</span><span class="msg-error"></span>' ;
	}
	else
	{
		$data[3][inp_country] = '<select name="inp_country" style="display:none;"><option value="'.$raw_data[country_iso3].'">'.$raw_data[country_iso3].'</option></select>'.$raw_data[country_name_jp] ;
	}
	
	$arr_city = $db->select_city($raw_data['country_iso3']);
	
	$data[3][inp_city] = '<p id="field_city">'.input_selectbox('inp_city',$arr_city['data'],$arr_city['value'],$raw_data[city_iso3],'-------Please Select-------','inp_cx1').'<span class="msg-req">*</span></p>';

	
	$data[3][inp_souvenir_code] = input_textbox('inp_souvenir_code', $raw_data[souvenir_code] ,'inp_cx2');
	
	
	/* select type */
	
	$sql = 'SELECT souvenirtype_id,souvenirtype_idx,souvenirtype_name_en,
			souvenirtype_name_jp FROM mbus_souvenir_type ORDER BY souvenirtype_idx';
	$result = $db->db_query($sql);
	
	$arr_souvenir_type[val] = array();
	$arr_souvenir_type[name] = array();
	
	while ($record = mysql_fetch_array($result)){
		$arr_souvenir_type[val][] = $record[souvenirtype_id];
		$arr_souvenir_type[name][] = $record[souvenirtype_name_jp];
	}
	
	
	
	$data[3][inp_souvenir_type] = input_selectbox('inp_souvenir_type',
												   $arr_souvenir_type[name],
												   $arr_souvenir_type[val],
												   $raw_data[store_type],
												   '---Please Select---',
												   'inp_cx1');
	/* select type */
	
	
	$data[3][affiliate] 		= input_chkbox('affiliate',$raw_data['affiliate']).'&nbsp;Yes'; 
	$data[3][affiliate_code]	= input_textbox('affiliate_code',$raw_data[affiliate_code],'inp_cx2');
	$data[3][affiliate_url]		= input_textbox('affiliate_url' ,$raw_data[affiliate_url] ,'inp_cx3');
	$data[3][inp_souvenir_name] = input_textbox('inp_souvenir_name',$raw_data[souvenir_name],'inp_cx3');
	
	$data[3][inp_short_description] = input_memobox('inp_short_description',$raw_data[short_desc],'','inp_cx3',2);

	/* box theme */
	$arr_theme = $db->get_theme();
		
	$buff_arr1 = explode(';',$raw_data['souvenir_theme']);
	
	/* Select Theme */
	
	
	/* select left */
	$sql  =  'SELECT themesouvenir_id,themesouvenir_name FROM ';
	$sql .=  '(SELECT IF(themesouvenir_id<100,IF (themesouvenir_id<10,CONCAT(\'00\',themesouvenir_id),
			  CONCAT(\'0\',themesouvenir_id)),themesouvenir_id) themesouvenir_id,themesouvenir_name,
			  themesouvenir_idx FROM mbus_theme_souvenir ) AS a ';
	$sql .=  'WHERE ';
	
	foreach ($buff_arr1 as $text)
	{
		$sql .= '(themesouvenir_id  =  "'.$text.'") OR ';
	}
	$sql .= '(themesouvenir_id = "'.$buff_arr1[0].'") ORDER BY themesouvenir_idx ';
	
	$record = $db->db_query($sql);
	
	
	

	$buffer_left = '';
	$data_select = '';
	
	while ($rec = mysql_fetch_array($record))
	{
		$buffer_left .= ' <option value="'.$rec['themesouvenir_id'].'">'.$rec['themesouvenir_name'].'</option>'	;
		$data_select .= ' <option value="'.$rec['themesouvenir_id'].'" selected="selected" >'.$rec['themesouvenir_name'].'</option>'	;
	}	
	/* select left */
	
	/* select right */
	
	$sql  = 'SELECT themesouvenir_id,themesouvenir_name FROM ';
	$sql .=  '( SELECT IF(themesouvenir_id<100,IF (themesouvenir_id<10,CONCAT(\'00\',themesouvenir_id),
			  CONCAT(\'0\',themesouvenir_id)),themesouvenir_id) themesouvenir_id,themesouvenir_name,
			  themesouvenir_idx FROM mbus_theme_souvenir ) AS a ';
	$sql .=  'WHERE ';	
	foreach ($buff_arr1 as $text)
	{
		$sql .= '(themesouvenir_id != "'.$text.'") AND ';
	}
	$sql .= '(themesouvenir_id != "'.$buff_arr1[0].'") ORDER BY themesouvenir_idx ';
	
	$record = $db->db_query($sql);
	
	
	
	$buffer_right = '';
	
	while ($rec = mysql_fetch_array($record))
	{
		$buffer_right .= ' <option value="'.$rec['themesouvenir_id'].'">'.$rec['themesouvenir_name'].'</option>'	;
	}
	
	/* select right */
	
	/* Select Theme */
	
	/* theme */
	$box_theme = '<script type="text/javascript">
					function fnMoveSelect(select, target) {
						$(\'#\' + select).children().each(function() {
							if (this.selected) {
								
								
								$(\'#\' + target).append(this);
								$(this).attr({selected: false});
								
								if (target == "category_id")
								{
									$("#category_list").append(\'<option value="\'+$(this).val()+\'" selected="selected">\'+$(this).text()+\'</option>\');
								}
								else
								{
									$("#category_list").find(\'option\').remove() ;
									$("#category_list").append( $(\'#category_id\').html() );
									$("#category_list").find(\'option\').attr({selected: true});
								}
								
							}
						});
						// IE7再描画不具合対策
						if ($.browser.msie && $.browser.version >= 7) {
							$(\'#\' + select).hide();
							$(\'#\' + select).show();
							$(\'#\' + target).hide();
							$(\'#\' + target).show();
						}
					}
					</script>';
	
	$box_theme .= '<select name="category_select" id="category_id" style="height: 120px; min-width: 200px; float:left;" onchange="" size="10" multiple="multiple">			';
				   
				   
	$box_theme .= $buffer_left.'</select>
				   <div class="box_move" style="float:left;">                            
				   <a class="btn-normal" href="javascript:;" name="on_select" onclick="fnMoveSelect(\'category_id_unselect\',\'category_id\'); return false;" >&nbsp;&nbsp;&lt;-&nbsp;Select&nbsp;&nbsp;</a>
				   <br/><br/>
				   <a class="btn-normal" href="javascript:;" name="un_select" onclick="fnMoveSelect(\'category_id\',\'category_id_unselect\'); return false;">&nbsp;&nbsp;Deselect&nbsp;-&gt;&nbsp;&nbsp;</a>
 				   </div>
				   
				   <select name="category_unselect" id="category_id_unselect" onchange="" size="10" style="height: 120px; min-width: 200px; float:left;" multiple="multiple">';
				   
	$box_theme  .= $buffer_right;
	$box_theme  .= '</select><select name="category_select[]" id="category_list" multiple="multiple" style="display:none;">';
	$box_theme  .= $data_select;
    $box_theme  .=  '</select>';
	
	
	$data[3][inp_theme] = $box_theme;
	
	
	
	
	/* theme */
	
	/* paid type */
	
	$data[3][inp_paid_type] = input_radio('inp_status', 
										 array('クレジットカード', '現地にて現金でお支払い'/*, 'クレジットカード + 現地にて現金でお支払い'*/),
										 array(1,2/*,3*/),$raw_data[status_book]);
	
	/* paid type */
	
	
	$data[3][inp_price]      =  input_textbox('inp_price',$raw_data[souvenir_price],'inp_cx1'); 
	$data[3][inp_allotment]  = input_textbox('inp_allotment',$raw_data[souvenir_allotment],'inp_cx1');

	
	$table_content = $raw_data[text_table];
	
	
	$data[3][inp_title_description]  = '<textarea name="detail_top" class="inp_cx4" rows="10">'.$raw_data[text_top].'</textarea>';
	$data[3][inp_footer_description] = '<textarea name="detail_bottom" class="inp_cx4" rows="10">'.$raw_data[text_bottom].'</textarea>';
	
	
	
	
	
	

	function table_to_array($data)
	{
		preg_match_all('/<th>([^`]*?)<\/th>/', $data, $matches1);
		
		preg_match_all('/<td>([^`]*?)<\/td>/', $data, $matches2);
		
		preg_match_all('/<tr>([^`]*?)<\/tr>/', $data, $matches3);

		$result = array ( 'th'=> $matches1[1],'td'=>$matches2[1] , 'tr'=>$matches3[1]);

		return  $result ; 
	}
	
	
	$tab_table_buff =  jd_decode($raw_data['text_table']) ; 
	
	$table_data = table_to_array($tab_table_buff);
	

	/* record 1 */
	$record_tb  = '';
	
	if ( is_array( $table_data) )
	{
		$rectr = count($table_data['tr']) ;
		
		if ( $rectr   < 4)
		{
			$record_tb  = '';
	
			for ($i = 0 ; $i<3 ; $i++)
			{
			
				$record_tb  .= '<tr><td class="left"><input type="text" name="col1-left[]" value="'.$table_data['th'][$i].'" /></td>
								<td class="right"><input type="text" name="col1-right[]" value="'.$table_data['td'][$i].'" /></td></tr>';
			}
		
		}
		else
		{

			$record_tb  = '';
			
			for ($i =0 ; $i <  $rectr  ; $i++)
			{
				$record_tb  .= '<tr><td class="left"><input type="text" name="col1-left[]" value="'.$table_data['th'][$i].'" /></td>
								<td class="right"><input type="text" name="col1-right[]" value="'.$table_data['td'][$i].'" /></td></tr>';
			}
		}
	
	}
	
	$data[3][inp_table_description] = '<div class="table-mid"><table border="0" cellpadding="0" cellspacing="0" class="table-content">'.$record_tb.'</table><div class="add-more"><input type="button" name="add_more" value="Add" /></div></div></div>
	 <script type="text/javascript">
			$(\'input[name$="add_more"]\').click(
				function(){
					$(this).parent().parent().find(\'.table-content\').append(\'<tr><td class="left"><input type="text" name="col1-left[]" value="" /></td><td class="right"><input type="text" name="col1-right[]" value="" /></td></tr>\' );
				});</script>';
	/* record 1 */
		
	
	$data[3][inp_publish]  = input_chkbox('inp_publish',$raw_data['public']).'&nbsp;Yes'; 
	$data[3][inp_remark]   = input_memobox('inp_remark',$raw_data[remark],'','inp_cx4',2);
	
	
	
	
	//$data[3][]  = input_hiddenbox('inp_product_code',$raw_data['product_code']).$raw_data['product_code'];  
	$entrydate = Date('YmdHis');
 
 
 	function upload_product($img,$entrydate,$productimg)
	{
		
		return  '<p class="product-img'.$img.'">'.$productimg.'</p>
				 <input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'">
				 <input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />
				 &nbsp;|&nbsp;&nbsp;<input type="button" class="button" onclick="return product_del_img('.$img.','.$entrydate.');" value="Delete" />';
				
	}
	
	
	$path_img  = '../product/images/souvenir/';
	$star_start = strlen($path_img);
	
	foreach (glob($path_img.'*') as $filename)
	{
		$file_select =  substr( $filename , $star_start , strlen( $filename )) ;
		
		$select = explode('-',$file_select);
		
		if ($obj_id == $select[0])
		{
			
			switch ($select[1])
			{
				case 1  : $img_upload[1]  = '<img src="'.$filename.'?'.time().'" width="133" height="100" /><div class="height5"></div>'; break;
				case 2  : $img_upload[2]  = '<img src="'.$filename.'?'.time().'" width="133" height="100" /><div class="height5"></div>'; break;
				case 3  : $img_upload[3]  = '<img src="'.$filename.'?'.time().'" width="133" height="100" /><div class="height5"></div>'; break;
			}
		}
	}

 
 	$data[3][inp_upload_img][] = upload_product(1,$entrydate,$img_upload[1]);
	$data[3][inp_upload_img][] = upload_product(2,$entrydate,$img_upload[2]);
	$data[3][inp_upload_img][] = upload_product(3,$entrydate,$img_upload[3]);
	
	$data[3][entrydate] = input_hiddenbox('entrydate',$entrydate);
	
	
	

	
	
	$data[2] = breadcamp( array('Home','Souvenir',$raw_data['souvenir_name']),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input_souvenir","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>