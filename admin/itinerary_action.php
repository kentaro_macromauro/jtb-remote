<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'itinerary';
	$tb_name  = _DB_PREFIX_TABLE.'itinerary_obj';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('itiner_id','itiner_name_en','itiner_name_jp','update_date','update_by'); 
			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_itinerary_name_en]))
				{
					$inp_itinerary_name_en  = $_POST[inp_itinerary_name_en];
					$inp_itinerary_name_jp  = $_POST[inp_itinerary_name_jp];

					$arr_record = array($inp_itinerary_name_en,$inp_itinerary_name_jp,'datetime', $user_id);
					
					array_shift($arr_carrer);
					
					$db->set_insert(_DB_PREFIX_TABLE.'itinerary_obj',$arr_carrer,$arr_record);
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$idx		 	= $arr_carrer[0];
					array_shift($arr_carrer);
					array_shift($arr_carrer);
					
					
					$inp_itinerary_name_jp  = $_POST[inp_itinerary_name_jp];

					$db->set_update($tb_name,$arr_carrer,
									array($inp_itinerary_name_jp,'datetime', $user_id),
									$idx,$page_id);	
				}

			break;
				
			case 'delete' :
			
		
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>