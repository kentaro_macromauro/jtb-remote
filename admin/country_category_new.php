<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Top Category Page : New';
	$sys_name = 'country_category';

	$data[2] = breadcamp( array('Home','Product','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[page]))
	{
		$page_back = $sys_name.'.php&page='.$_GET[page];	
	}
	else
	{
		$page_back = $sys_name.'.php';	
	}


	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$arr_country =  $db->get_country() ;
	
	$data[3][] = table_data('Country',input_selectbox('inp_country',$arr_country[1],$arr_country[0],'','-------Please Select-------','inp_cx1')) ;
	$data[3][] = table_data('Page Type',input_radio('inp_lcat_type', array('PKG','OPT'),array('PKG','OPT') ));
	$data[3][] = table_data('Category Name',input_textbox('inp_lcat_name','','inp_cx3'));
	
	
	$arr_category = $db->get_category();
	

	$box_theme = '<script type="text/javascript">
					function fnMoveSelect(select, target) {
						$(\'#\' + select).children().each(function() {
							if (this.selected) {
								
								
								$(\'#\' + target).append(this);
								$(this).attr({selected: false});
								
								if (target == "category_id")
								{
									$("#category_list").append(\'<option value="\'+$(this).val()+\'" selected="selected">\'+$(this).text()+\'</option>\');
								}
								else
								{
									$("#category_list").find(\'option\').remove() ;
									$("#category_list").append( $(\'#category_id\').html() );
									$("#category_list").find(\'option\').attr({selected: true});
								}
								
							}
						});
						// IE7再描画不具合対策
						if ($.browser.msie && $.browser.version >= 7) {
							$(\'#\' + select).hide();
							$(\'#\' + select).show();
							$(\'#\' + target).hide();
							$(\'#\' + target).show();
						}
					}
					</script>';
	
	$box_theme .= '<select name="category_select" id="category_id" style="height: 120px; min-width: 200px; float:left;" onchange="" size="10" multiple="multiple">			
				   </select>
				   <div class="box_move" style="float:left;">                            
				   <a class="btn-normal" href="javascript:;" name="on_select" onclick="fnMoveSelect(\'category_id_unselect\',\'category_id\'); return false;" >&nbsp;&nbsp;&lt;-&nbsp;Select&nbsp;&nbsp;</a>
				   <br/><br/>
				   <a class="btn-normal" href="javascript:;" name="un_select" onclick="fnMoveSelect(\'category_id\',\'category_id_unselect\'); return false;">&nbsp;&nbsp;Deselect&nbsp;-&gt;&nbsp;&nbsp;</a>
 				   </div> 
				   <select name="category_unselect" id="category_id_unselect" onchange="" size="10" style="height: 120px; min-width: 200px; float:left;" multiple="multiple">';
				   
				  for ($i = 0; $i < count( $arr_category[0]) ;$i++)
				  {
					  
					 $box_theme .=  ' <option label="'.$arr_category[0][$i].'" value="'.$arr_category[1][$i].'">'.$arr_category[0][$i].'</option>';
					  
				  }

	$box_theme .= '</select>
	
	
	
	<select name="category_select[]" id="category_list" multiple="multiple" style="display:none;">';

	 
	 $box_theme .=  '</select>';
 
    $input_selecttemp  = '<div class="tab-select">
 						  <ul class="tab-menu">
						  	<li id="tab-menu1" class="first active">Table + Conetent</li>
						  	<li id="tab-menu2" class="">Table Only</li>
					      	<li id="tab-menu3" class="last">Text Only</li>
						  </ul>
				       </div><input type="hidden" name="inp_tempage" class="inp_tempage" value="1" />';

	
	$data[3][] = table_data('Theme',$box_theme);	
	$data[3][] = table_data('Order By',input_selectbox('inp_orderby',array(1,2,3,4,5,6,7,8,9,10),array(1,2,3,4,5,6,7,8,9,10),'',
																										 '--','')) ;
 	
	

	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input","../content/themes_backend/");

	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

