<? 

require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");

	
	
	$sql = 'SELECT a.order_id ,a.order_refid ,a.axis_refid ,a.session_status ,
			a.mailto_customer ,a.country_iso3 ,a.transection_id ,a.product_amount ,
			a.product_qty ,a.paid_type ,a.paid_status ,a.order_firstname ,a.order_surname ,
			a.order_furi_firstname ,a.order_furi_surname ,a.order_sex ,a.order_birthday ,
			a.order_email ,a.order_tel1 ,a.order_tel2 ,a.order_tel3 ,a.order_mobile1 ,
			a.order_mobile2 ,a.order_mobile3 ,a.order_addrtype ,a.order_addrjp_zip1 ,
			a.order_addrjp_zip2 ,a.order_addrjp_pref ,a.order_addrjp_city ,
			a.order_addrjp_area ,a.order_addrjp_building ,a.order_addrovs1 ,
			a.order_addrovs2 ,a.order_rec_firstname ,a.order_rec_surname ,
			a.order_rec_furi_firstname ,a.order_rec_furi_surname ,
			a.order_rec_addrjp_zip1 ,a.order_rec_addrjp_zip2 ,a.order_rec_addrjp_pref ,
			a.order_rec_addrjp_city ,a.order_rec_addrjp_area ,a.order_rec_addrjp_building ,
			a.order_rec_tel1 ,a.order_rec_tel2 ,
			a.order_rec_tel3 ,a.order_remark ,a.update_date ,a.update_by ,a.status_cms ,
			a.update_date_cms ,a.update_by_cms ,a.status_cms_paid,receive_type,
			acc_name,acc_tel,acc_addr,stay_from,stay_to,arrival_date,arrival_time,flight_no,travel,
			b.order_idx,b.souvenir_id,b.souvenir_code,b.souvenir_price,b.souvenir_qty,b.souvenir_amount ,
			souvenir_name,store_type,short_desc,souvenir_allotment,c.country_name_jp,c.country_name_en,order_rec,sign  
			FROM mbus_souvenir_order a LEFT JOIN 
			mbus_souvenir_order_detail b ON a.order_id = b.order_id LEFT JOIN
			mbus_country c ON a.country_iso3 = c.country_iso3 LEFT JOIN 
			mbus_currency d ON a.country_iso3 =  d.country_iso3  LEFT JOIN 
			mbus_souvenir e ON b.souvenir_id = e.souvenir_id
			WHERE a.order_id = "'.$_REQUEST[id].'" AND session_status = 1 
			ORDER BY order_refid';
			
		
			
			$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
			$db->db_connect();


			$result=  $db->db_query($sql);
			$data = array();
			
			while ($record = mysql_fetch_array($result)){
				$data[] = $record;	
			}


			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transection Souvenir Report</title>
</head>

<style type="text/css">
tr.border-bottom td{ border-top:#333 solid 1px;}
</style>

<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<body>

<div class="data">
	
    
    
    
    <div class="data">
        <div class="table" align="center">
        
            <table x:str BORDER="0" width="800" align="center" class="table_data" cellpadding="0" cellspacing="1">
        
           			 <tr><td colspan=2 align=center height=30 style='text-align:center;'><b>Souvenir Order Details</b></td></tr>
                     <tr><td align=left colspan=2><strong>Order Information </strong></td></tr>
                     
                     <tr><td align=left style='width:150px; table-style:fix;'>Order Id</td>
                     	 <td align=left  style='width:550px; table-style:fix;'><strong><?=$data[0][order_id] ?></strong></td>
                         
                     </tr>
                     <tr>
                     	<td align="left">Order Date</td>
                        <td align="left"><?=$data[0][update_date] ?></td>
                     </tr>
                     
                     <tr>
                     	<td align="left">Country</td>
                        <td align="left"><?=$data[0][country_name_jp].' '.$data[0][country_name_en] ?></td>
                     </tr>
					 <tr>
                     	<td align="left">Recive Location</td>
                        <td align="left"><?=$data[0][store_type]==1?'Local country':'In Japan' ?></td>
                     </tr>                    
                     <tr>
                     	<td align="left">Detail</td>
                        <td align="left">
                        	<table>
                            	<tr>
                                    <th style="width:250px; table-layout:fixed;">Name</th>
                                    <th style="width:100px; table-layout:fixed;">Price</th>
                                    <th style="width:100px; table-layout:fixed;">Qty</th>
                                    <th style="width:100px; table-layout:fixed;">Amount</th>
                                </tr>
                                <? 
									foreach ($data as $val){
								?>
                                	<tr>	
                                       <td align="left"><?='['.$val[souvenir_code].'] '.$val[souvenir_name] ?></td>
                                       <td align="right"><?=number_format($val[souvenir_price],2).' '.$val[sign] ?></td>
                                       <td align="right"><?=number_format($val[souvenir_qty]) ?></td>
                                       <td align="right"><?=number_format($val[souvenir_amount],2).' '.$val[sign] ?></td>
                                    </tr>
                                <?		
									}
								?>
                                
                                <tr><th colspan="3"  align="left">Amount</th><td align="right">
									<?=number_format($data[0][product_amount] ,2 ).' '.$data[0][sign] ?></td>
                                </tr>
                            </table>
                        </td>
                     </tr> 
                     <tr>
                     	<td align="left">Paid Type</td>
                        <td align="left"><?
						
						
						
						
						switch ($data[0][paid_type] )
						{
							case 1 : echo "Credit" ;  break;
							case 2 : echo "Cash" ; break;
							case 3 : echo "On Request" ; break;
							default : echo "" ; break;
						}
						?></td>
                     </tr>
                     
                     
                     <tr><td colspan=2 align=left style='text-align:left;'><b>Souvenir Customer Detail</b></td></tr>
                     
                     
                     <tr>
                     	<td align="left">Name [surname firstname]</td>
                        <td><?=$data[0][order_surname].' '.$data[0][order_firstname] ?></td>
                     </tr>
                     <tr>
                     	<td align="left" > [surname firstname]</td>
                        <td><?=$data[0][order_furi_surname].' '.$data[0][order_furi_firstname] ?></td>
                     </tr>
                     <tr>
                     	<td align="left">Sex</td>
                        <td><?=($data[0][order_sex]==1)?'male':'female'?></td>
                     </tr>
                     <tr>
                     	<td align="left">Address</td>
                        <td>
                        	<? 
							if ($data[0][order_addrtype] == 2 ){
								echo $data[0][order_addrovs1].'<br/>'.$data[0][order_addrovs2];
							}
							else{
								
						
								
								echo '<table cellspacing="0" class="regis_address_local_cfm">
										<tr >
										  <th style="width:100px; table-layout:fixed;" align="left" >郵便番号</th>
										  <td  style="width:300px; table-layout:fixed;" align="left"> 〒'.$data[0][order_addrjp_zip1].'-'.$data[0][order_addrjp_zip2].' </td>
										</tr>
										<tr>
										  <th  align="left">都道府県 </th>
										  <td align="left">'.$db->view_pref(trim($data[0][order_addrjp_pref])).'</td>
										</tr>
										<tr>
										  <th align="left">市区町村 </th>
										  <td align="left">'.$data[0][order_addrjp_city].'</td>
										</tr>
										<tr>
										  <th align="left"> 町域 </th>
										  <td align="left">'.$data[0][order_addrjp_area].'</td>
										</tr>
										<tr>
										  <th align="left">建物名
											部屋番号 </th>
										  <td align="left">'.$data[0][order_addrjp_building].'</td>
										</tr>
									  </table>';
							}
						
						?>
                        
                        </td>
                     </tr>
                      <tr>
                     	<td align="left">Email</td>
                        <td><?=$data[0][order_email] ?></td>
                     </tr>
                      <tr>
                     	<td align="left">Mobile</td>
                        <td><?=$data[0][order_mobile1].'-'.$data[0][order_mobile2].'-'.$data[0][order_mobile3] ?></td>
                     </tr>
                      <tr>
                     	<td align="left">Telephone</td>
                        <td><?=$data[0][order_tel1].'-'.$data[0][order_tel2].'-'.$data[0][order_tel3] ?></td>
                     </tr>
                 
                        <!--Shipping information-->
                   		<? 
		/* check recive outsite japan */
		$buffer = ''; 
		

		if ( $data[0][receive_type] == "1"){
			
			
			
			$buffer .= '<tr><td colspan="2"><b>■Accommodation information</b></td></tr>';

						
			
			
			
			$buffer .= '<tr><td nowrap="" style="width:180px;">Accommodation name：</td><td>'.$data[0][acc_name].'</td></tr>';
			$buffer .= '<tr><td>Telephone：</td><td>'.$data[0][acc_tel].'</td></tr>';
			$buffer .= '<tr><td>Address : </td><td>'.$data[0][acc_addr].'</td></tr>';
			
			
			$buffer .= '<tr><td>Stay：</td><td>'.jp_strdate($data[0][stay_from]).' ～ '.jp_strdate($data[0][stay_to]).'</td></tr>';
			$buffer .= '<tr><td>Arrival Date Flight：</td><td>'.jp_strdate($data[0][arrival_date]).' '.substr($data[0][arrival_time],0,5).'</td></tr>';
			
			
			$buffer .= '<tr><td>&nbsp;</td><td>Flight No. '.$data[0][flight_no].'</td></tr>';
			
			
			$buffer .= '<tr><td>Travel Form：</td><td>'.$data[0][travel].'</td></tr>';
			

			
		}
		else if ($data[0][receive_type] == "2"){	
		/* check recive outsite japan */
		
		/* check recive insite japan */
		
		
		$buffer .= '<tr><td colspan="2"><b>■Sending information</td></tr>' ;

		
		if ($data[0]['order_rec'] ){
			
			$buffer .= '<tr><td nowrap="" style="width:180px;">Name [surname firstname]</td><td>'.$data[0]['order_rec_surname'].' '.$data[0]['order_rec_firstname'].'</td></tr>';
			$buffer .= '<tr><td >[surname firstname]</td><td>'.$data[0]['order_rec_furi_surname'].' '.$data[0]['order_rec_furi_firstname'].'</td></tr>';
			
			$buffer .= '<tr><td >Address</td><td><table cellspacing="0" class="regis_address_local_cfm">';
			$buffer .= '<tr><th style="width:100px; table-layout:fixed;" align="left" valign="top" >郵便番号 :  </th>
			<td style="width:300px; table-layout:fixed;" align="left">〒'.$data[0]['order_rec_addrjp_zip1'].'-'.$data[0]['order_rec_addrjp_zip2'].'</td></tr>';
			$buffer .= '<tr><th align="left" valign="top">都道府県 :  </th><td>'.$db->view_pref( $data[0]['order_rec_addrjp_pref'] ).'</td></tr>';
			$buffer .= '<tr><th align="left" valign="top">市区町村 :  </th><td>'.$data[0]['order_rec_addrjp_city'].'</td></tr>';
			$buffer .= '<tr><th align="left" valign="top">町域 :  </th><td>'.$data[0]['order_rec_addrjp_area'].'</td></tr>';
			$buffer .= '<tr><th align="left" valign="top">建物名部屋番号 :  </th><td>'.$data[0]['order_rec_addrjp_building'].'</td></tr></table></td>';
			
			
			$buffer .= '<tr><td>Telephone</td><td>'.$data[0]['order_rec_tel1'].'-'.$data[0]['order_rec_tel2'].'-'.$data[0]['order_rec_tel3'].'</td></tr>';
			
	    }
		else{
			
			
			$buffer .= '<tr><td nowrap="" style="width:180px;">Name [surname firstname]</td><td>'.$data[0]['order_surname'].' '.$data[0]['order_firstname'].'</td></tr>';
			$buffer .= '<tr><td>[surname firstname]</td><td>'.$data[0]['order_furi_surname'].' '.$data[0]['order_furi_firstname'].'</td></tr>';
			$buffer .= '<tr><td >Address</td><td><table cellspacing="0" class="regis_address_local_cfm">';
			
			
			if ($data[0]['order_addrtype'] == 1){
				$buffer .= '<tr><th style="width:100px; table-layout:fixed;" align="left" valign="top">郵便番号 : </th>
				<td style="width:300px; table-layout:fixed;">〒'.$data[0]['order_addrjp_zip1'].'-'.$data[0]['order_addrjp_zip2'].'</td></tr>';
				$buffer .= '<tr><th align="left" valign="top">都道府県 : </th><td>'.$db->view_pref( $data[0]['order_addrjp_pref'] ).'</td></tr>';
				$buffer .= '<tr><th align="left" valign="top">市区町村 : </th><td>'.$data[0]['order_addrjp_city'].'</td></tr>';
				$buffer .= '<tr><th align="left" valign="top">町域 : </th><td>'.$data[0]['order_addrjp_area'].'</td></tr>';
				$buffer .= '<tr><th align="left" valign="top">建物名部屋番号 : </th><td>'.$data[0]['order_addrjp_building'].'</td></tr></table></td>';
			}
			else{
				$buffer .= '<tr><th style="width:100px; table-layout:fixed;" align="left" valign="top">住所1 :  </th><td style="width:300px; table-layout:fixed;">'.$data[0]['order_addrovs1'].'</td></tr>';
				$buffer .= '<tr><th align="left" valign="top">住所2 :  </th><td>'.$data[0]['order_addrovs2'].'</td></tr></table></td>';
			}
			
			$buffer .= '<tr><td>Telephone</td><td>'.$data[0]['order_tel1'].'-'.$data[0]['order_tel2'].'-'.$data[0]['order_tel3'].'</td></tr>';
		}
		
		/* check recive insite japan */
		
		}
		
		echo $buffer;
						?>
                        <!--Shipping information--> 
                       
              <tr><td align="left" colspan="2"><b>■Remark</b></td></tr> 
             <tr><td align="left" valign="top" colspan="2"><?=nl2br($data[0][order_remark])?></td></tr> 
             </table>
        </div>
        <p style="text-align:right">Copyright © 2013 MyBus-Asia.com All rights reserved </p>
</div>

    	 
    
	</div>
</div>



</body>
</html>
