<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Itinerary : New';
	$sys_name = 'itinerary';

	$data[2] = breadcamp( array('Home','Itinerary','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[page]))
	{
		$page_back = $sys_name.'.php&page='.$_GET[page];	
	}
	else
	{
		$page_back = $sys_name.'.php';	
	}


	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$arr_country =  $db->get_country() ;
	
	$data[3][] = table_data('Itinerary Name En',input_textbox('inp_itinerary_name_en','','inp_cx3'));
	$data[3][] = table_data('Itinerary Name JP',input_textbox('inp_itinerary_name_jp','','inp_cx3'));
	

	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input","../content/themes_backend/");

	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

