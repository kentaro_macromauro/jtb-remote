<? 
require_once("include/header.php");
if ($status == true)
{
	$data[1] = 'Country : New';
	$sys_name = 'country';
	$data[2] = breadcamp( array('Home','Country','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'&page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;
	
	$width_th = 130;
	$width_td = 870;
	
	$data[3][] = table_data('JTB Code',input_textbox('inp_jtb','','inp_cx'),$width_th,$witdh_td,1);
	$data[3][] = table_data('ISO2 Code',input_textbox('inp_iso2','','inp_cx'),$width_th,$witdh_td,1);
	$data[3][] = table_data('ISO3 Code',input_textbox('inp_iso3','','inp_cx'),$width_th,$witdh_td,1);
	$data[3][] = table_data('Country Name EN',input_textbox('inp_name_en','','inp_cx2'),$width_th,$witdh_td,1);
	$data[3][] = table_data('Country Name JP',input_textbox('inp_name_jp','','inp_cx2'),$width_th,$witdh_td,1);
	$data[3][] = table_data('Child Age',input_textbox('inp_child_age','','inp_cx').' Year',$width_th,$witdh_td,1);
	$data[3][] = table_data('Infant Age',input_textbox('inp_infant_age','','inp_cx').' Year',$width_th,$witdh_td,1);
	$data[3][] = table_data('Country Path',input_textbox('inp_name_path','','inp_cx3'),$width_th,$witdh_td,1);
	$data[3][] = table_data('Booking Menu ',input_chkbox('pu_child',1).' Show Child Age &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.input_chkbox('pu_infant',1).' Show Infant Age');


	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];
	
	

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

