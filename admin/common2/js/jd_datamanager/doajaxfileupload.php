<?php
	require_once("setting.php");
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{
			/*$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);
			//for security reason, we force to remove all uploaded file
			@unlink($_FILES['fileToUpload']);	*/
			
			$fileupload = _PATH_UPLOAD_.$_FILES['fileToUpload']['name'];
			
			if ( !in_array(preg_replace('/(.*)[.]([^.]+)/is','$2',basename(strtolower($fileupload))),array('png','jpg','jpeg','gif','xbm','xpm','wbmp'))) 
			{
				$error = 'The file format is not supported.';
			}
			else
			{
			
				if ($file_size=$_FILES["fileToUpload"]["size"] > _LimitFileSize_)
				{
					$error = 'Cannot upload large files than 2Mb';
				}
				else
				{
				/*if ($file_size > _limit_size_)
				{
					$data[57] = "Cannot upload large files than 2Mb";
				}
				else
				{*/
					
					move_uploaded_file($_FILES['fileToUpload']['tmp_name'],strtolower($fileupload));
					/*
					if (  move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$fileupload)) 
					{
						$data[57] = $_FILES['uploadfile_name1']['name'];
						$db->ref_id_upd("upload1");
					}*/
				/*}*/
				}
			}
	
	
	}		
	
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
	

?>