// JavaScript Document
function f_gotopage(_url)
{
	document.location.href = _url;	
}

function mb_strlen(str) {
				var len = 0;
				for(var i = 0; i < str.length; i++) {
					len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? 2 : 1;
				}
				return len;
}

function check_text_len(_input,_output,_length,_alert)
{
	$(_input).keydown(
					
		function()
		{
			check_text_len_child(_input,_output,_length,_alert);
		}
	);
}



function check_text_len_child(_input,_output,_length,_alert)
{
	txt_len = mb_strlen($(_input).val());
						
	if (txt_len > _length)
	{
		$(_output).html(txt_len+'/'+_length+'&nbsp;'+_alert);
		$(_output).css({'color':'#F00'});					
	}
	else
	{
		$(_output).html(txt_len+'/'+_length);
		$(_output).css({'color':'#666'});	
	}
}


var frm_name ;

$(document).ready(
	function()
	{
		$(document).find("input, button").filter(':submit').click(
		function(event) 
		{
			event.preventDefault();
		});
		
		$('.sub1').bind("mouseenter",
			function()
			{				
				$(this).find('.menu_sub').show();
			}
			).bind("mouseleave",
				function()
				{
					$(this).find('.menu_sub').hide();		
				}
			);
			
			$('.chkbox_all').click(
			function()
			{		
				if ($(this).attr('checked')==true)
				{
					$('.chkbox').attr('checked','true');
				}
				else
				{
					$('.chkbox').attr('checked','');
				}
				
		});
				
		$('#btn_edit').click(
				function()
				{
					_bufftxt = $('#ref_id').val(); 
					
					_j = 0;
					for (_i=0;_i<_bufftxt.length;_i++)
					{	
						if (_bufftxt.substr(_i,1)==',')
						{
							_numid = (_bufftxt.substr(_i-(_j),_j)) ;
							
							if ($('#id_chk'+_numid).attr('checked')==true)
							{
								f_gotopage(_nowpage+'_edit.php?id='+_numid);
								break;
							}
							
							_j = 0;
						}
						else
						{
							_j++;
						}
					}				
		});
			
		$('#btn_new').click(
			function()
			{
				f_gotopage(_nowpage+'_new.php');
		});
		
		$('#btn_close').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
		$('#btn_save').click(
			function()
			{

								
				frm_name =   $(document).find("form").attr("name") ;	
					$('#'+frm_name).submit();

			}
		);
		$('#btn_download').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
		$('#btn_restore').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = ''; _valdata = ''; 
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							
							_chkdata = _chkdata+ _numid+ ',';
						    _valdata = _valdata + $('input:radio[name=rad'+_numid+']:checked').val() + ',' ;
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
					var msg = "Are you sure you want to restores?";    
					
					if ( confirm(msg) )
					{
						$('#action_id').val(_chkdata);
						$('#val_id').val(_valdata);
						
						document.restore_obj.submit();
					}
				}
		});
				
		$('#btn_delete').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
					var msg = "Are you sure you want to delete?";    
					
					if ( confirm(msg) )
					{
						$('#action_id').val(_chkdata);
						document.del_obj.submit();
					}
				}
		});
		
		$('#btn_active').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
						$('#enabled_action_id').val(_chkdata);
						document.enabled_obj.submit();
				}
		});
		
		
		$('#btn_unactive').click(
			function()
			{
				_bufftxt = $('#ref_id').val(); 
				
				_j = 0;	_chkdata = '';
				for (_i=0;_i<_bufftxt.length;_i++)
				{	
					if (_bufftxt.substr(_i,1)==',')
					{
						_numid = (_bufftxt.substr(_i-(_j),_j)) ;
						
						if ($('#id_chk'+_numid).attr('checked')==true)
						{
							_chkdata = _chkdata+_numid+ ',';
						}
						_j = 0;
					}
					else
					{
						_j++;
					}
				}	
					
				if (_chkdata.length > 0)
				{
						$('#disabled_action_id').val(_chkdata);
						document.disabled_obj.submit();
				}
		});
		
				$('#btn_back').click(
			function()
			{
				f_gotopage(_nowpage);
			}
		);
		
		

		$('#main_toggle').toggle(
			function()
			{
				$('.toggle_sub').show();
			},
			function()
			{
				$('.toggle_sub').hide();
			}
		);
		
		
		$('.tb_toggle').click(
			function()
			{
				_textid = $(this).text();
				
				if ( $('.subtb'+_textid).css('display') == 'none' )
				{
					$('.subtb'+_textid).show();
				}
				else 
				{
					$('.subtb'+_textid).hide();
				}
			}
		);
		
		
		$('input:radio').click(
			function()
			{
				_name = ( $(this).attr('name'));
				_name = _name.replace("rad", "");
				
				if ($(this).val() > 0)
				{
					$('#id_chk'+_name).attr("checked",true);
				}
				else
				{
					$('#id_chk'+_name).attr("checked",false);
				}

			}
							 
		);
		
		
		$('select[name$="inp_country"]').change(
			function()
			{
				
				
				$.post('ajax_view_country.php',{ value : $(this).val()  },
				function(data) {
				 	$('#field_city').html(data);
				});
				
				
			}
		);
		
		$('input[name$="inp_min"]').numeric();
		$('input[name$="inp_max"]').numeric();
		
	});


	
	

