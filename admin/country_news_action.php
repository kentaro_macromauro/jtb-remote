<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'country_news';
	$tb_name  = _DB_PREFIX_TABLE.'country_news';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('news_id','country_iso3','news_date','news_subject','news_countent','icon','public','update_date','update_by'); 
			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_country]))
				{
					$inp_country      = $_POST[inp_country];
					$inp_subject      = $_POST[inp_subject];
					$inp_description  = $_POST[inp_description];
					$txt_calendar     = $_POST[txt_calendar];
					$inp_public       = $_POST[inp_public];
					$inp_status	      = $_POST[inp_status];
					$public = '0';
					$status = '0';
					
					//var_dump ($_POST); exit();
					
					if ($inp_public == true)
					{
						$public = '1';
					}
					else
					{
						$public = '0';	
					}
					
					
					if ($inp_status == true)
					{
						$status = '1';	
					}
					else
					{
						$status = '0';	
					}
					
					
					$arr_record = array($inp_country,$txt_calendar, $inp_subject,$inp_description, $status, $public,'datetime', $user_id);
					
					array_shift($arr_carrer);
					
					$db->set_insert($tb_name,$arr_carrer,$arr_record);
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$idx		 	= $arr_carrer[0];
					array_shift($arr_carrer);

					$inp_subject      = $_POST[inp_subject];
					$inp_description  = $_POST[inp_description];
					$txt_calendar     = $_POST[txt_calendar];
					$inp_public       = $_POST[inp_public];
					$inp_status	      = $_POST[inp_status];
					$public = '0';
					$status = '0';
			
					
					if ($inp_public == true)
					{
						$public = '1';
					}
					else
					{
						$public = '0';	
					}
					
					
					if ($inp_status == true)
					{
						$status = '1';	
					}
					else
					{
						$status = '0';	
					}
					
			
					
					
					$db->set_update($tb_name,array('news_date','news_subject','news_countent','icon','public','update_date','update_by'),
									array( $txt_calendar,$inp_subject,$inp_description,$status,$public,'datetime', $user_id),
									'news_id',$page_id);	
				}

			break;
			
			case 'active': 
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_public_array($tb_name,$arr_carrer[0],$arr_id);		
			
			
			break;
			
			case 'unactive':
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_unpublic_array($tb_name,$arr_carrer[0],$arr_id);		
			break;
			
			
			case 'delete' :
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
			break;
		}
	}

	
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>