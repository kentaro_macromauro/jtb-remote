<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/;
	$data[0] = 'Special content (CMS)';	/*----set h1 name-------*/
	$data[5] = 'branch_content';  /* system page name */
	$data[1] = breadcamp( array('Home','Special content (CMS)'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('Country','65'),
					 tb_head('Publish','65'),
					 tb_head('Subject','525'),
					 tb_head('Update Date')
					 ) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->count_branch_content($_SESSION[session_country],NULL);	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fetch_branch_content($page_first,$page_num,$_SESSION[session_country],NULL);		  
	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';

	
	for ($i=0;$i< count($record['id']); $i++)
	{
		
		if ( $record['public'][$i] == 0)
		{
			$other_status = '<div class="bg_noneapprove">&nbsp;</div>';	
		}
		else
		{
			$other_status = '<div class="bg_approve">&nbsp;</div>';	
		}
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record['id'][$i]), $record['country_name_jp'][$i], $other_status, $record['subject'][$i], $record['update_date'][$i]),
		array('','',$rec_linkprefix.$record['id'][$i],$rec_linkprefix.$record['id'][$i], $rec_linkprefix.$record['id'][$i], $rec_linkprefix.$record['id'][$i] ),
		array(2,1,1,1,0,1));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table_approve","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>