<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
	
	$sql = 'SELECT a.country_iso3,country_name_en,souvenir_code,CONCAT(souvenir_name,\'\') souvenir_name,
			IF (store_type=1,\'Local\',\'Japan\') receipt,
			souvenir_price,
			IF (store_type=1,sign,\'¥\') currency,
			souvenir_allotment allotment,
			IF (public=1,\'YES\',\'NO\') publish,
			(case `a`.`status_book` when 1 then _utf8\'credit\' when 2 then _utf8\'cash\' 
			when 3 then _utf8\'credit+cash\' end) AS `paid_type`,
			IF (affiliate=1,\'YES\',\'NO\') affiliate,
			affiliate_code,affiliate_url ,
			CONCAT(\'http:/\/\www.mybus-asia.com\/\',LOWER(path),\'\/souvenir_product.php?souvenir_id=\',a.souvenir_id) url
			FROM mbus_souvenir a 
			LEFT JOIN mbus_country b ON a.country_iso3 = b.country_iso3 
			LEFT JOIN mbus_currency c ON a.country_iso3 = c.country_iso3
			ORDER BY souvenir_code';
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();


	$result=  $db->db_query($sql);
	$data = array();
			
	while ($record = mysql_fetch_array($result)){
		$data[] = $record;	
	}			
?>
<? 
if ($_REQUEST[export]=='1'){ // export to excel
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="export_souvenir.xls"');
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<body>
	<TABLE x:str BORDER="1" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
    	<tr>
        	<th colspan="14" align="center" height="30">Export Data : Souvenir</th>
        </tr>
        <tr>
        	<th align=center>country_iso3</th>
            <th align=center>country_name_en</th>
            <th align=center>souvenir_code</th>
            <th align=center>souvenir_name</th>
            <th align=center>receipt</th>
            <th align=center>souvenir_price</th>
            <th align=center>currency</th>
            <th align=center>allotment</th>
            <th align=center>publish</th>
            <th align=center>paid_type</th>
            <th align=center>affiliate</th>
            <th align=center>affiliate_code</th>
            <th align=center>affiliate_url</th>
            <th align=center>url</th>
        </tr>
        <? foreach ($data as $txt){ ?>        
        <tr>
            <td align="left" height="20"><?=$txt[country_name_en]?></td>
            <td align="left" height="20"><?=$txt[country_iso3]?></td>
            <td align="left" height="20"><?=$txt[souvenir_code]?></td>
            <td align="left" height="20"><?=$txt[souvenir_name]?></td>
            <td align="left" height="20"><?=$txt[receipt]?></td>
            <td align="right" height="20"><?=$txt[souvenir_price]?></td>
            <td align="left" height="20"><?=$txt[currency]?></td>
            <td align="left" height="20"><?=$txt[allotment]?></td>
            <td align="left" height="20"><?=$txt[publish]?></td>
            <td align="left" height="20"><?=$txt[paid_type]?></td>
            <td align="left" height="20"><?=$txt[affiliate]?></td>
            <td align="left" height="20"><?=$txt[affiliate_code]?></td>
            <td align="left" height="20"><?=$txt[affiliate_url]?></td>
            <td align="left" height="20"><?=$txt[url]?></td>
        </tr>       
         <? } ?>       
    </TABLE>
</body>
</html>

<? //export to excel
}
else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Export Data : Souvenir</title>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
</head>

<style type="text/css">
tr.border-bottom td{ border-top:#333 solid 1px;}
body table.table_data tr.alert td{background:#FFC;}
</style>

<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<body>

<div class="data">
	<div class="table" align="center">
    	<table x:str BORDER="0" width="1165" align="center" class="table_data" cellpadding="0" cellspacing="1">
            <tr>
                <td colspan=14 align=center height=30><b>Export Data : Souvenir</b>
                	<span style="float:right;"><a style="text-decoration:underline;" href="?export=1" target="_blank">Export to Excel</a></span>
                </td>  
         </tr>
         <tr>
            <th align=center>Country</th>
            <th align=center>Code</th>
            <th align=center>Souvenir_Name</th>
            <th align=center>Receipt</th>
            <th align=center colspan="2">Price</th>
            <th align=center>Allotment</th>
            <th align=center>Publish</th>
            <th align=center>Paid_type</th>
            <th align=center>Affiliate</th>
            <th align=center>Affiliate_code</th>

            <th align=center>URL</th>
        </tr>
        <? foreach ($data as $txt){ ?>        
        <tr>
        	<td align="left" ><?=$txt[country_name_en]?></td>
            <td align="left" ><?=$txt[souvenir_code]?></td>
            <td align="left" ><?=$txt[souvenir_name]?></td>
            <td align="left" ><?=$txt[receipt]?></td>
            <td align="right"><?=$txt[souvenir_price]?></td>
            <td align="left" ><?=$txt[currency]?></td>
            <td align="left" ><?=$txt[allotment]?></td>
            <td align="left" ><?=$txt[publish]?></td>
            <td align="left" ><?=$txt[paid_type]?></td>
            <td align="left" ><?=$txt[affiliate]?></td>
            <td align="left" ><?=$txt[affiliate_code]?></td>

            <td align="left" ><a href="<?=$txt[url]?>" target="_blank"><?=$txt[url]?></a></td>
        </tr>       
         <? } ?>       
        </table>
    
	</div>
    <p style="text-align:right">Copyright © 2013 MyBus-Asia.com All rights reserved </p>
</div>



</body>
</html>
<? } ?>