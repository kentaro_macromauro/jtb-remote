<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'country_category';
	$tb_name  = _DB_PREFIX_TABLE.'country_category';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
				
		$arr_carrer = array('lcat_id','country_iso3','lcat_type','lcat_name','lcat_category','lcat_index','update_date','update_by');
		
		switch ($action)
		{
			case 'insert' : 
				if (!empty($_POST['inp_country']))
				{					
					$country_iso3	  	 = $_POST[inp_country];
					
					$lcat_type    		 = $_POST[inp_lcat_type];
					$lcat_name 	         = $_POST[inp_lcat_name];
					
					$select_category_arr = $_POST[category_select];
					$select_category = '';
					
					foreach ($select_category_arr as $text)
					{
						$select_category .= $text.';';
					}

					$order_by            = $_POST[inp_orderby];
					
					$update_date    = 'datetime';
					$update_by      = $user_id;
					
					array_shift($arr_carrer);
					
					$arr_lcat = array( $country_iso3,$lcat_type,$lcat_name,$select_category,$order_by,$update_date,$update_by );
					
					$db->set_insert($tb_name,$arr_carrer,$arr_lcat);
					
				}
				
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{	
					$country_iso3	  	 = $_POST[inp_country];
					
					$lcat_type    		 = $_POST[inp_lcat_type];
					$lcat_name 	         = $_POST[inp_lcat_name];
					
					$select_category_arr = $_POST[category_select];
					$select_category = '';
					
					foreach ($select_category_arr as $text)
					{
						$select_category .= $text.';';
					}

					$order_by            = $_POST[inp_orderby];
					
					$update_date    = 'datetime';
					$update_by      = $user_id;
								
					$arr_lcat = array( $country_iso3,$lcat_type,$lcat_name,$select_category,$order_by,$update_date,$update_by );
						
					array_shift($arr_carrer);
											
					$db->set_update($tb_name,$arr_carrer,$arr_lcat,'lcat_id',$page_id);
	
				}

			break;
				
			case 'delete' :

			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>