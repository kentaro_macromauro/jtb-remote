<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
	$data[0] = 'Special Campaign Page (CMS)';	/*----set h1 name-------*/
	$data[5] = 'promotion';  /* system page name */
	$data[1] = breadcamp( array('Home','Special Campaign Page (CMS)'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('Country',50),
					 tb_head('Type ',50),
					 tb_head('Publish',50),
					 tb_head('Campaign Name','440'),
					 tb_head('Update Date','70'),
					 tb_head('Update By','70')) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	
	$page_num = 20;
	
	if ( $_SESSION["session_country"] == "ALL" ){
		$page_count = $db->page_count("promotion");	
	}
	else{
		$sql = 'SELECT COUNT(*) dd_count FROM mbus_promotion WHERE country_iso3 = "'.$_SESSION["session_country"].'" ';
	}
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	
	if ( $_SESSION["session_country"] == "ALL" ){
	
		$record = $db->fetch_promotion($page_first,$page_num);	
	}
	else{
		$record = $db->fetch_promotion_incountry($page_first,$page_num,$_SESSION[session_country]);
		
	}
	
	
	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';

	//echo count($record[promo_id]);

	for ($i=0;$i< count($record[promo_id]); $i++)
	{
		 if ($record[product_public][$i] == '1')
		 {
		 	$approve ='<div class="bg_approve"> </div>' ;  
		 }
		 else
		 {
			$approve = '<div class="bg_noneapprove"> </div>';    
	     }
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record[promo_id][$i]), 
			   $record[country_name][$i], 
			   $record[product_type][$i], 
			   $approve,
			   $record[promo_tittle][$i],
			   $record[update_date][$i],
			   $record[update_by][$i]),
		array('','',
			  $rec_linkprefix.$record[promo_id][$i], 
			  $rec_linkprefix.$record[promo_id][$i], 
			  $rec_linkprefix.$record[promo_id][$i], 
			  $rec_linkprefix.$record[promo_id][$i], 
			  $rec_linkprefix.$record[promo_id][$i], 
			  $rec_linkprefix.$record[promo_id][$i],
			  ),
		array(2,1,1,1,1,0,0));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record[promo_id] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table_approve","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
	
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>