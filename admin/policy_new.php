<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Policy : New';
	$sys_name = 'policy';
	$data[2] = breadcamp( array('Home','Policy','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'&page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$record = $db->get_country();
	
	$arr_data  = $record[1];
	$arr_value = $record[0];	
	
	
	
	
	$data[3][] = table_data('Policy titles EN',input_textbox('inp_title_en','','inp_cx3'));
	$data[3][] = table_data('Policy titles JP',input_textbox('inp_title_jp','','inp_cx3'));
	
	$data[3][] = table_data('Policy titles EN (Short)',input_textbox('inp_title_short_en','','inp_cx2'));
	$data[3][] = table_data('Policy titles JP (Short)',input_textbox('inp_title_short_jp','','inp_cx2'));
	
	
	$data[3][] = table_data('Policy contents EN',input_memobox('inp_content_en[]','','','inp_cx4',5));
	$data[3][] = table_data('Policy contents JP',input_memobox('inp_content_jp[]','','','inp_cx4',5));
	$data[3][] = '<tr class="last"><th>&nbsp;</th><td><div style=" padding-left:470px;"><input type="button" class="addfield" value="Add" /></div></td></tr>';
	
	$data[3][] = '<script type="text/javascript">
					var _field = 2;
					var _field_data = \'\';
					$(\'.addfield\').click(
						function()
						{
							_field_data = \'<tr><th align="left" width="100" valign="top">Policy contents EN \'+_field+\'</th><td align="left" width="800" valign="top"><textarea rows="5" class="inp_cx4" name="inp_content_en[]"/></td></tr><tr><th align="left" width="100" valign="top">Policy contents JP \'+_field+\'</th><td align="left" width="800" valign="top"><textarea rows="5" class="inp_cx4" name="inp_content_jp[]"/></td></tr>\';

							$(\'table.table_data .last\').before(_field_data);	
							_field++;
						}
					);
					</script>';


	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

