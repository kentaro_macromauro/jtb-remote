<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/

	$data[0] = 'Top Category Page';	/*----set h1 name-------*/
	$data[5] = 'country_category';  /* system page name */
	$data[1] = breadcamp( array('Home','Top Category Page'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(
					 tb_head('Country','50'),
					 tb_head('Type','50'),
					 tb_head('Order','50'),
					 tb_head('Category Name','430'), 
					 tb_head('Update Date','70'),
					 tb_head('Update By','70')) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->page_count('country_category');	
	
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fatch_country_category($page_first,$page_num);	

	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';
	
	for ($i=0;$i< count($record['id']); $i++)
	{
		$link  = $rec_linkprefix.$record['id'][$i];
		
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record['id'][$i]), 
			   $record['country_iso3'][$i], 
			   $record['lcat_type'][$i],
			   $record['lcat_index'][$i], 
			   $record['lcat_name'][$i],
			   $record['update_date'][$i],
			   $record['update_by'][$i]),
		array('','',$link,$link,$link,$link,$link,$link,$link),
		array(2,1,1,1,1,0,0,2,2));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>