<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Country : Update';
	$sys_name = 'country';
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	
	$width_th = 120;
	$width_td = 850;
	

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_country($_GET[id]);
	
	/*$data[3][0] = table_data('JTB Code',input_textbox('inp_jtb',$raw_data[0],'inp_cx'),$width_th,$witdh_td,1);
	$data[3][1] = table_data('ISO2 Code',input_textbox('inp_iso2',$raw_data[1],'inp_cx'),$width_th,$witdh_td,1);*/
	$data[3][2] = table_data('ISO3 Code',$raw_data[2].input_hiddenbox('inp_iso3',$raw_data[2]).
							 input_hiddenbox('inp_jtb',$raw_data[0]).
							 input_hiddenbox('inp_iso2',$raw_data[1]).
							 input_hiddenbox('inp_name_path',$raw_data[5]));
	$data[3][3] = table_data('Country Name EN',input_textbox('inp_name_en',$raw_data[3],'inp_cx2'),$width_th,$witdh_td,1);
	$data[3][4] = table_data('Country Name JP',input_textbox('inp_name_jp',$raw_data[4],'inp_cx2'),$width_th,$witdh_td,1);
	$data[3][5] = table_data('Child Age',input_textbox('inp_child_age',$raw_data[6],'inp_cx').' Year',$width_th,$witdh_td,1);
	$data[3][6] = table_data('Infant Age',input_textbox('inp_infant_age',$raw_data[7],'inp_cx').' Year',$width_th,$witdh_td,1);
	$data[3][7] = table_data('Booking Menu ',input_chkbox('pu_child',$raw_data[8]).' Show Child Age &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.input_chkbox('pu_infant',$raw_data[9]).' Show Infant Age');
	
	/*$data[3][7] = table_data('Country Path',input_textbox('inp_name_path',$raw_data[5],'inp_cx3'),$width_th,$witdh_td,1);*/

	$data[2] = breadcamp( array('Home','Country',$raw_data[2]),array('index.php',$sys_name.'.php') ); 

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>