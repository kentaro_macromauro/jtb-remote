<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'souvenir';
	$tb_name  = _DB_PREFIX_TABLE.'souvenir';
	
	$path = 'tmp/';
	$path_move  = '../product/images/souvenir/';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/



	if (!empty($_POST[action]))
	{	
	
		
		
		$field_souvenir_detail = array('souvenir_id','text_top','text_table','text_bottom');

		switch ($action)
		{
			case 'insert' : 
				
			
				if (!empty($_POST['inp_country'])){			
								
					$country_iso3  	 	= $_POST['inp_country'];
					$city		    	= $_POST['inp_city'];
					$souvenir_code 		= $_POST['inp_souvenir_code'];
					$souvenir_name  	= $_POST['inp_souvenir_name'];
					
					$short_description  = $_POST['inp_short_description'];
					
					$souvenir_type  	= $_POST['inp_souvenir_type'];					
					
					$arr_category	    = $_POST['category_select'];
					$theme 				= '';
					
					if (is_array($arr_category)){					
						foreach ($arr_category as $val){
							$theme .= $val.';';	
						}
					}
					
					
					$price 				= $_POST['inp_price'];
					$allotment			= $_POST['inp_allotment'];
					$status		    	= $_POST['inp_status'];
					

					$text_top			= $_POST['detail_top'];
					$col1_left			= $_POST['col1-left'];
					$col1_right			= $_POST['col1-right'];
					$text_bottom		= $_POST['detail_bottom'];
					$remark				= $_POST['inp_remark'];
					
					$public			= $_POST['inp_publish']?'1':'0';
					
					$affiliate          = $_POST['affiliate']?'1':'0';
					$affiliate_code     = $_POST['affiliate_code'];
					$affiliate_url		= $_POST['affiliate_url'];
					
					
					$field_souvenir   = array('country_iso3',
									   'city_iso3',
									   'souvenir_code',
									   'store_type',
									   'souvenir_name',
									   'souvenir_theme',
									   'souvenir_price',
									   'souvenir_allotment',
									   'short_desc',
									   'public',
									   'status_book',
									   'affiliate',
									   'affiliate_code',
									   'affiliate_url',
									   'remark');
		
					$arr_top	= array($country_iso3,
										$city,
										$souvenir_code,
										$souvenir_type,
										$souvenir_name,									
										$theme,
										$price,
										$allotment,
										$short_description,
										$public,
										$status,
										$affiliate,
										$affiliate_code,
										$affiliate_url,
										$remark);
					
					$db->db_start();
					
					$db->set_insert('mbus_souvenir',$field_souvenir,$arr_top);
					
					
					$record_id = $db->insert_id();
										
					$text_table = '';
					
					$record = count( $_POST['col1-left'] ) ;
					if ( $record  < count( $_POST['col1-right'] )  ){
						$record = count( $_POST['col1-right']);
					}
					
					$text_table_left1_array  = $_REQUEST['col1-left'];
					$text_table_right1_array = $_REQUEST['col1-right'];
									   
					for ($i =0 ;$i <  $record; $i++){
					   if ( ( $text_table_left1_array[$i] != '') || ( $text_table_right1_array[$i] != '' ) )										   
						 $text_table .= '<tr><th>'.$text_table_left1_array[$i].'</th><td>'.$text_table_right1_array[$i].'</td></tr>';  
					}
					
					$arr_detail	 = array($record_id,$text_top,$text_table,$text_bottom);
					
					
					$db->set_insert('mbus_souvenir_detail',$field_souvenir_detail,$arr_detail);
					
					$db->db_commit();
					
					
					
					@mkdir($path);
					@mkdir($path_move);
															
					$start_pos = strlen($path);
						
					foreach (glob($path.'*') as $filename)
					{
							
						$file_count =  substr($filename,19,strlen($filename));
							
						
						if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							$file_count =  substr($filename,19,strlen($filename));
								
								
							$file_name  = explode('-',$file_count);
							
							@unlink($path_move.$record_id.'-'.$file_name[0]);
							
							copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
							@unlink($filename);
								
						}
					}
					
					
					
					
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{	
				
					$country_iso3  	 	= $_POST['inp_country'];
					$city		    	= $_POST['inp_city'];
					$souvenir_code 		= $_POST['inp_souvenir_code'];
					$souvenir_name  	= $_POST['inp_souvenir_name'];
					
					$short_description  = $_POST['inp_short_description'];
					$souvenir_type  	= $_POST['inp_souvenir_type'];					
					$arr_category	    = $_POST['category_select'];
					
					$theme 				= '';
					
					if (is_array($arr_category)){					
						foreach ($arr_category as $val){
							$theme .= $val.';';	
						}
					}
					
					$price 				= $_POST['inp_price'];
					$allotment			= $_POST['inp_allotment'];
					$status		    	= $_POST['inp_status'];
					

					$text_top			= $_POST['detail_top'];
					$col1_left			= $_POST['col1-left'];
					$col1_right			= $_POST['col1-right'];
					$text_bottom		= $_POST['detail_bottom'];
					$remark				= $_POST['inp_remark'];
					
					$public				= $_POST['inp_publish']?'1':'0';
					
					$affiliate          = $_POST['affiliate']?'1':'0';
					$affiliate_code     = $_POST['affiliate_code'];
					$affiliate_url		= $_POST['affiliate_url'];
					
					$record_id			= $_POST['page_id'];
					
					$field_souvenir     = array('country_iso3',
											    'city_iso3',
											    'souvenir_code',
											    'souvenir_name',
											    'store_type',
											    'souvenir_theme',
											    'souvenir_price',
											    'souvenir_allotment',
											    'short_desc',
											    'public',
											    'status_book',
												'affiliate',
											    'affiliate_code',
											    'affiliate_url',
											    'remark');
					
					
					$arr_souvenir		= array($country_iso3,
												$city,
												$souvenir_code,
												$souvenir_name,
												$souvenir_type,
												$theme,
												$price,
												$allotment,
												$short_description,
												$public,
												$status,
												$affiliate,
												$affiliate_code,
												$affiliate_url,
												$remark);
		
					$field_souvenir_detail = array('souvenir_id','text_top','text_table','text_bottom');
					
					/* descripiton */
					$text_table = '';
					
					$record = count( $_POST['col1-left'] ) ;
					if ( $record  < count( $_POST['col1-right'] )  ){
						$record = count( $_POST['col1-right']);
					}
					
					$text_table_left1_array  = $_REQUEST['col1-left'];
					$text_table_right1_array = $_REQUEST['col1-right'];
									   
					for ($i =0 ;$i <  $record; $i++){
					   if ( ( $text_table_left1_array[$i] != '') || ( $text_table_right1_array[$i] != '' ) )										   
						 $text_table .= '<tr><th>'.$text_table_left1_array[$i].'</th><td>'.$text_table_right1_array[$i].'</td></tr>';  
					}
					
					$arr_detail	 = array($record_id,$text_top,$text_table,$text_bottom);
					/* description */
					
					
							
					$db->db_start();

					$db->set_update('mbus_souvenir',$field_souvenir,$arr_souvenir,'souvenir_id',$record_id);
					$db->set_update('mbus_souvenir_detail',$field_souvenir_detail,$arr_detail,'souvenir_id',$record_id);
					
					$db->db_commit();
										
					
					
					//check befor insert new image
					@mkdir($path);
					@mkdir($path_move);
					
					
					
															
					$start_pos = strlen($path);
						
					foreach (glob($path.'*') as $filename)
					{
							
						$file_count =  substr($filename,19,strlen($filename));
							
						
						if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							$file_count =  substr($filename,19,strlen($filename));
								
								
							$file_name  = explode('-',$file_count);
							
							@unlink($path_move.$record_id.'-'.$file_name[0]);
							
							copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
							@unlink($filename);
								
						}
					}
				}

			break;
				
			case 'delete' :
			
				
						$action_id = $_POST[action_id];
						$arr_id = str_to_arr($action_id);
						$db->db_start();
						$db->set_delete_array('mbus_souvenir','souvenir_id',$arr_id);		
						$db->set_delete_array('mbus_souvenir_detail','souvenir_id',$arr_id);
						
						
						$db->db_commit();
						
						$start_pos = strlen($path_move);
						
						
						foreach (glob($path_move.'*') as $filename)
						{
							$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
							$filelist[] = 	$record[0];
						}
			
						
			
						foreach ($arr_id as $id)
						{
							if (in_array($id,$filelist,true))
							{
								
								$arr_del[] = $id;
							}
						}
						
						
						if (is_array($arr_del))
						{
						
							@mkdir($path_move);
							
							
							
							foreach (glob($path_move.'*') as $filename)
							{
								$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
								$file   = $record[0];	
								
								if (in_array($file,$arr_del,true))
								{
			
									unlink($filename);
								}
							}
						}

			break;
			
			case 'active' : 
					$action_id = $_POST[action_id];
					$arr_id = str_to_arr($action_id);
					$db->set_public_array('mbus_souvenir','souvenir_id',$arr_id);		
					
			break;
			
			case 'unactive': 
			
					$action_id = $_POST[action_id];
					$arr_id = str_to_arr($action_id);
					
					$db->set_unpublic_array('mbus_souvenir','souvenir_id',$arr_id);		
					
			break;
			
			
		}
	}
	
	
	
	header("Location: ".$sys_name.".php?page=".$_GET[page].'&filter_country='.$_GET[filter_country].'&filter_city='.$_GET[filter_city].
			'&filter_theme='  .$_GET[filter_theme].'&filter_code='.$_GET[filter_code].'&filter_name_jp='.$_GET[filter_name_jp].
			'&filter_name_en='.$_GET[filter_name_en].'&filter_public='.trim($_GET[filter_public]));
	
	
	
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>