<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'City : New';
	$sys_name = 'city';
	$data[2] = breadcamp( array('Home','City','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'&page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$record = $db->get_country();
	
	$arr_data  = $record[1];
	$arr_value = $record[0];	
	
	$data[3][0] = table_data('Country',input_selectbox('inp_country',$arr_data,$arr_value,'','Please Select','inp_cx0'),'','',1);
	$data[3][1] = table_data('ISO3 Code',input_textbox('inp_iso3','','inp_cx'),'','',1);
	$data[3][2] = table_data('City Name EN',input_textbox('inp_name_en','','inp_cx2'),'','',1);
	$data[3][3] = table_data('City Name JP',input_textbox('inp_name_jp','','inp_cx2'),'','',1);
	$data[3][4] = table_data('Status',input_chkbox('inp_chkbox' ).' Other ');
	


	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

