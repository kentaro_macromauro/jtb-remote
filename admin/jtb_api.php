<?
require_once("include/header.php");
if ($status == true)
{

    $path = '../';
    require_once($path . "class/c_query_sub.php");

    define('_JTB_API_URL_'  , 'http://www.jtb.co.jp/kaigai_opt/TourActivitySearchReceipt.do');
    define('_WEBSERVICE_TIME_', 30);

    class jtb_api
    {
        protected $data_params;

        public function __construct($param)
        {
            $this->area = $param['area'];
            $this->country = $param['country'];
            $this->country_iso3 = $param['country'];

            $this->username = $param['username'];
            $this->password = $param['password'];
            $this->call_date = $param['call_date'];
            $this->sales_channel_id = $param['sales_channel_id'];
            $this->search_page_data_count = $param['search_page_data_count'];
            $this->search_page_no = $param['search_page_no'];
            $this->sort_order = $param['sort_order'];
        }

        function generate_post_xml()
        {
            $XmlStr = <<<EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Header>
  <AuthHeader>
    <Username>{$this->username}</Username>
    <Password>{$this->password}</Password>
  </AuthHeader>
</soap:Header>
    <soap:Body>
    <TourActivitySearchRQ>
      <CALL_DATE>{$this->call_date}</CALL_DATE>
      <SALES_CHANNEL_ID>{$this->sales_channel_id}</SALES_CHANNEL_ID>
      <SEARCH_PAGE_DATACOUNT>{$this->search_page_data_count}</SEARCH_PAGE_DATACOUNT>
      <SEARCH_PAGE_NO>{$this->search_page_no}</SEARCH_PAGE_NO>
      <SORT_ORDER>{$this->sort_order}</SORT_ORDER>
      <AREA_CD>{$this->area}</AREA_CD>
      <COUNTRYS>
      <COUNTRY_CD>{$this->country}</COUNTRY_CD>
      </COUNTRYS>
    </TourActivitySearchRQ>
  </soap:Body>
</soap:Envelope>
EOF;
            return $XmlStr;
        }
        function check_city_code($country_iso3, $city_iso3){
            $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
            $db->db_connect();
            $sql = 'SELECT count(*) dd FROM '._DB_PREFIX_TABLE.'city WHERE country_iso3 = "'.$country_iso3.'" AND city_iso3 = "'.$city_iso3.'"';
            $result = $db->db_query($sql);

            while ($record = mysql_fetch_array($result))
            {
                $check_city =  $record['dd'];
            }
            return $check_city;
        }

        public function getall()
        {

            $ch = curl_init(_JTB_API_URL_);
            curl_setopt($ch,    CURLOPT_AUTOREFERER,        true);
            curl_setopt($ch,    CURLOPT_COOKIESESSION,      true);
            curl_setopt($ch,    CURLOPT_FAILONERROR,        false);
            curl_setopt($ch,    CURLOPT_FOLLOWLOCATION,     false);
            curl_setopt($ch,    CURLOPT_FRESH_CONNECT,      true);
            curl_setopt($ch,    CURLOPT_POST,               true);
            curl_setopt($ch,    CURLOPT_RETURNTRANSFER,     true);
            curl_setopt($ch,    CURLOPT_CONNECTTIMEOUT,     _WEBSERVICE_TIME_);
            curl_setopt($ch,    CURLOPT_POSTFIELDS,          $this->generate_post_xml());
            curl_setopt($ch,    CURLOPT_HTTPHEADER, array("Content-Type: application/xml; charset=utf8")); //XMLを送信
            $result = curl_exec($ch);
            curl_close($ch);

            $result= str_replace('&', '&amp;', $result);

            $doc = new DOMDocument("1.0", "utf-8");
            $doc->loadXML($result);

            $response['RETURNCODE'] = $doc->getElementsByTagName("RETURNCODE")->item(0)->nodeValue;
            $response['DATA_TOTAL_COUNT'] = $doc->getElementsByTagName("DATA_TOTAL_COUNT")->item(0)->nodeValue;

            $returnData = $doc->getElementsByTagName("TOUR_DETAILS");
            $response['TOUR_DATA'] = array();
            $i = 0;
            foreach($returnData as $returnNode){
                $response['TOUR_DATA'][$i]["TOUR_CD"] = $returnData->item($i)->getElementsByTagName("TOUR_CD")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TOUR_ID"] = $returnData->item($i)->getElementsByTagName("TOUR_ID")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TOUR_LANGUAGE"] = $returnData->item($i)->getElementsByTagName("TOUR_LANGUAGE")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["GUIDE_FLG"] = $returnData->item($i)->getElementsByTagName("GUIDE_FLG")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["AREA_CD"] = $returnData->item($i)->getElementsByTagName("AREA_CD")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["AREA_NAME"] = $returnData->item($i)->getElementsByTagName("AREA_NAME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["COUNTRY_CD"] = $returnData->item($i)->getElementsByTagName("COUNTRY_CD")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["COUNTRY_NAME"] = $returnData->item($i)->getElementsByTagName("COUNTRY_NAME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["COUNTRY_CD_ISO"] = $returnData->item($i)->getElementsByTagName("COUNTRY_CD_ISO")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DEPT_CITY_CD"] = $returnData->item($i)->getElementsByTagName("DEPT_CITY_CD")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DEPT_CITY_NAME"] = $returnData->item($i)->getElementsByTagName("DEPT_CITY_NAME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TOUR_TITLE"] = $returnData->item($i)->getElementsByTagName("TOUR_TITLE")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TOUR_OVERVIEW"] = $returnData->item($i)->getElementsByTagName("TOUR_OVERVIEW")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["EFFECTIVE_DATE_FROM"] = $returnData->item($i)->getElementsByTagName("EFFECTIVE_DATE_FROM")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["EFFECTIVE_DATE_TO"] = $returnData->item($i)->getElementsByTagName("EFFECTIVE_DATE_TO")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["LOWEST_SELLING_PRICE"] = $returnData->item($i)->getElementsByTagName("LOWEST_SELLING_PRICE")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DISCOUNT_AMOUNT"] = $returnData->item($i)->getElementsByTagName("DISCOUNT_AMOUNT")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DISCOUNT_RATE"] = $returnData->item($i)->getElementsByTagName("DISCOUNT_RATE")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DISCOUNT_INFORMATION"] = $returnData->item($i)->getElementsByTagName("DISCOUNT_INFORMATION")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["LOCAL_CURRENCY_CD"] = $returnData->item($i)->getElementsByTagName("LOCAL_CURRENCY_CD")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["LOCAL_CURRENCY_NAME"] = $returnData->item($i)->getElementsByTagName("LOCAL_CURRENCY_NAME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["PICTURE_FILE_NAME"] = $returnData->item($i)->getElementsByTagName("PICTURE_FILE_NAME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["PICTURE_URL"] = $returnData->item($i)->getElementsByTagName("PICTURE_URL")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TIME_ZONE"] = $returnData->item($i)->getElementsByTagName("TIME_ZONE")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DEPARTURE_TIME"] = $returnData->item($i)->getElementsByTagName("DEPARTURE_TIME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["ARRIVAL_TIME"] = $returnData->item($i)->getElementsByTagName("ARRIVAL_TIME")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TOUR_DURATION"] = $returnData->item($i)->getElementsByTagName("TOUR_DURATION")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["AVAILABILITY"] = $returnData->item($i)->getElementsByTagName("AVAILABILITY")->item(0)->nodeValue;
                for($k = 0; $k < $returnData->item($i)->getElementsByTagName("CATEGORYS")->length; $k++){
                    $response['TOUR_DATA'][$i]["CATEGORYS"][$k]["CATEGORY_NAME"] = $returnData->item($i)->getElementsByTagName("CATEGORYS")->item($k)->getElementsByTagName("CATEGORY_NAME")->item(0)->nodeValue;
                    $response['TOUR_DATA'][$i]["CATEGORYS"][$k]["SUB_CATEGORY_NAME"] = $returnData->item($i)->getElementsByTagName("CATEGORYS")->item($k)->getElementsByTagName("SUB_CATEGORY_NAME")->item(0)->nodeValue;
                }
                $response['TOUR_DATA'][$i]["TRANSFER_AIRPORT_TO_HOTEL"] = $returnData->item($i)->getElementsByTagName("TRANSFER_AIRPORT_TO_HOTEL")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["TRANSFER_HOTEL_TO_AIRPORT"] = $returnData->item($i)->getElementsByTagName("TRANSFER_HOTEL_TO_AIRPORT")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["ACCOMMODATION_FLG"] = $returnData->item($i)->getElementsByTagName("ACCOMMODATION_FLG")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["OPERATOR"] = $returnData->item($i)->getElementsByTagName("OPERATOR")->item(0)->nodeValue;
                $response['TOUR_DATA'][$i]["DELETE_FLG"] = $returnData->item($i)->getElementsByTagName("DELETE_FLG")->item(0)->nodeValue;
                $i++;
            }
            return $response;
        }
        public function process_db($tour_data){
            //▽Delete
            $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
            $db->db_connect();

            $table_name = _DB_PREFIX_TABLE.'product';
            $country_iso3 = $db->get_country_iso3($this->country);

            $action = 'data_type = "API" AND country_iso3 = "'. $country_iso3 .'"';

            $db->set_delete_custom($table_name, $action);
            $db->db_disconnect();
            //△Delete

            //データベース処理
            $i = 0;
            foreach($tour_data['TOUR_DATA'] as $tour_val){
                $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
                $db->db_connect();
                $record_id = $db->db_newid('product');

                if(!empty($tour_val["CATEGORYS"])){
                    $product_theme = '';
                    foreach($tour_val["CATEGORYS"] as $val){
                        $category_data = $db->get_theme_mapping($val["CATEGORY_NAME"], $val["SUB_CATEGORY_NAME"]);
                        if(!strstr($product_theme, $category_data['theme'])){
                            $product_theme .= $category_data['theme'];
                        }
                        $product_option = $category_data['option'];
                        $product_time = $category_data['time'];
                    }
                }
                $record_id = $record_id;
                $country_iso3 = $tour_val["COUNTRY_CD_ISO"];
                $city_iso3 = $tour_val["DEPT_CITY_CD"]; //Cityが存在しない時の処理
                if($this->check_city_code($tour_val["COUNTRY_CD_ISO"], $tour_val["DEPT_CITY_CD"]) > 0){
                    $city_iso3 = $tour_val["DEPT_CITY_CD"];
                }else{
                    $city_iso3 = 'OTH';
                }

                $product_code = $tour_val["TOUR_ID"];
                $product_type = 'OPT';
                $data_type = 'API';
                $group_id = 0;
                $product_name_en = '';
                $product_name_jp = $tour_val["TOUR_TITLE"];
                $picture_url = $tour_val["PICTURE_URL"];
                $product_category = $product_theme;
                $status_time = $product_time;
                $status_book = 0;
                $option = $product_option;
                $option2 = 0;
                $price_min = $tour_val["LOWEST_SELLING_PRICE"];
                $price_max  = $tour_val["LOWEST_SELLING_PRICE"];
                $short_desc = $tour_val["TOUR_OVERVIEW"];
                $description = $tour_val["TOUR_OVERVIEW"];
                $description_en = $tour_val["TOUR_OVERVIEW"];
                $template_id = 0;
                $keyword_search = '';
                $remark = '';
                $public = 1;
                $public_start = 'NOW()';
                $public_end = '';
                $update_date = 'NOW()';
                $update_by = 999;
                $pax_min = 1;
                $pax_max = 99;
                $day_quantity = 0;
                $day_cutoff = 999;

                $field_product = array(
                    'product_id',
                    'country_iso3',
                    'city_iso3',
                    'product_code',
                    'product_type',
                    'data_type',
                    'group_id',
                    'product_name_en',
                    'product_name_jp',
                    'picture_url',
                    'product_theme',
                    'status_time',
                    'status_book',
                    'option_id1',
                    'option_id2',
                    'price_min',
                    'price_max',
                    'short_desc',
                    'description',
                    'description_en',
                    'template_id',
                    'keyword_search',
                    'remark',
                    'public',
                    'public_start',
                    'public_end',
                    'update_date',
                    'update_by',
                    'pax_min',
                    'pax_max',
                    'day_quantity',
                    'day_cutoff',
                );

                $arr_product = array(
                    $record_id,
                    $country_iso3,
                    $city_iso3,
                    $product_code,
                    $product_type,
                    $data_type,
                    $group_id,
                    $product_name_en,
                    $product_name_jp,
                    $picture_url,
                    $product_category,
                    $status_time,
                    $status_book,
                    $option,
                    $option2,
                    $price_min,
                    $price_max,
                    $short_desc,
                    $description,
                    $description_en,
                    $template_id,
                    $keyword_search,
                    $remark,
                    $public,
                    $public_start,
                    $public_end,
                    $update_date,
                    $update_by,
                    $pax_min,
                    $pax_max,
                    $day_quantity,
                    $day_cutoff,
                );
                $db->db_start();

                $db->set_insert(_DB_PREFIX_TABLE.'product',$field_product,$arr_product);
                $db->db_updateid($record_id,'product');

                $db->db_commit();

                $i++;
            }
            $field_result = array(
                'country_iso3',
                'result',
                'data_no',
                'update_date'
            );
            $arr_result = array(
                $country_iso3,
                'success',
                $i,
                'datetime'
            );

            $db->set_insert(_DB_PREFIX_TABLE.'api_result',$field_result,$arr_result);

            return 'Data Updated';
        }
    }
    /* sample */
//VN
//TH
//KH
//NZ ->OCE
//SG
//MY

    if(empty($_GET['country_iso2']) || empty($_GET['area_code'])){
        echo 'Parameter is not correct';
        return;
    }

//ID
    $params = '';
    $params['area'] = $_GET['area_code'];
    $params['country'] = $_GET['country_iso2'];

    $params['username'] = base64_encode('jtbap');
    $params['password'] = base64_encode('FbcsD!');
    date_default_timezone_set('UTC');
    $params['call_date'] = date("Y-m-j　H:i:s");
    $params['sales_channel_id'] = 'AP';
    $params['search_page_data_count'] = '9999';
    $params['search_page_no'] = '1';
    $params['sort_order'] = 'R';

    $c_websv = new jtb_api($params);
    $response_data = $c_websv->getall();

    if($response_data['RETURNCODE'] == 99){
        print_r('System Error');
    }elseif($response_data['RETURNCODE'] == 20){
        print_r('Auth Error');
    }elseif($response_data['RETURNCODE'] == 10){
        print_r('Request Error');
    }elseif($response_data['RETURNCODE'] == 00){
        $result = $c_websv->process_db($response_data);
        echo $result;
    }
}else{
    header("Location: index.php");
}

?>