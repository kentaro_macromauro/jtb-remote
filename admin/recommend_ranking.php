<?
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{

    $data[1] = 'OPT : Recommended Ranking';
    $sys_name = 'recommend_ranking';
    $data[0] = array($sys_name.'_action.php',$sys_name);

    $obj_id ='1';


    if (!empty($_GET[page]))
    {
        $page_back = $data[0][0].'?page='.$_GET[page];
    }
    else
    {
        $page_back = $data[0][0];
    }

    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $result = $db->fetch_recommend_ranking($_SESSION["session_country"]);

    $show_list = $result['product_list'];

    $product_data = explode(',',$show_list);

    $i = 0;
    foreach($product_data as $val){
        $product_ids = explode(':',$val);
        $product_id[$i]['product_id'] = $product_ids[0];
        $product_id[$i]['product_code'] = $product_ids[1];
        $i++;
    }

    $country = $_SESSION["session_country"];

    $i = 0;
    foreach($product_id as $val){
        $sql = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
         FROM mbus_product a WHERE country_iso3 = "'.$country.'" AND public = 1 AND product_id = "'.$val["product_id"].'" AND product_code = "'.$val["product_code"].'"';
        $result = $db->db_query($sql);

        while ($record  = mysql_fetch_array($result)){
            $product_list[$i] = $record;
        }
        //product_id and product_code is same
        if(empty($product_list[$i])){
            $sql2 = 'SELECT product_id, country_iso3, product_code, product_name_jp, picture_url, data_type
             FROM mbus_product a WHERE country_iso3 = "'.$country.'" AND public = 1 AND product_code = "'.$val["product_code"].'"';
            $result2 = $db->db_query($sql2);
            while ($record2  = mysql_fetch_array($result2)){
                $product_list[$i] = $record2;
            }
        }
        $i++;
    }

    $sql = 'SELECT a.product_id AS product_id, a.country_iso3 AS country_iso3, a.product_code AS product_code, a.product_name_jp AS product_name_jp, a.picture_url AS picture_url, a.data_type AS data_type, b.country_name_jp AS country_name_jp
        FROM mbus_product a
        LEFT JOIN mbus_country b ON a.country_iso3 = b.country_iso3
        WHERE a.country_iso3 = "'.$country.'" AND a.public = 1';
    $result = $db->db_query($sql);

    $rec = array(); $i=0;
    //Get all data
    while ($record  = mysql_fetch_array($result)){
        $rec['data'][$i] = '['.$record[country_name_jp].'] ['.$record[product_name_jp].'] ['.$record[product_code].']';
        $rec['value'][$i] = $record[product_id];
        $i++;
    }
    $product_result = $rec;

    $data[3][] = table_data('Ranking #1','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[0]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #2','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[1]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #3','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[2]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #4','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[3]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #5','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[4]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #6','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[5]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #7','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[6]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #8','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[7]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #9','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[8]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);
    $data[3][] = table_data('Ranking #10','<p id="field_product">'.input_selectbox( 'inp_product[]', $product_result['data'] ,$product_result['value'] ,$product_list[9]['product_id'],'-------Please Select-------','inip_cx1').'</p>','','',0);

    $data[2] = breadcamp( array('Home','OPT : Recommended Ranking'),array('index.php') );

    $data[4] 	= array($obj_id,'update');
    $data[5]    = $page_back;

    $themes = new c_themes("backend_input","../content/themes_backend/");
    $themes->pbody($data);
}
else
{
    header("Location: login.php");
}
?>