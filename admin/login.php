<? 

if ( $_SERVER["SERVER_PORT"] != 443 ){
    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}

require_once("include/header.php");
require_once($path."class/c_query_sub.php");


if (!check_session())
{
	if ( (!empty($_POST['inp_user'])) && (!empty($_POST['inp_pass'])) )
	{
		
		
		$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
		$db->db_connect();
		$data = $db->admin_login($_POST['inp_user'],$_POST['inp_pass']);
		$admin_code = $data[0];
		$admin_id   = $data[1];
		$country_path = $db->showpath_bycountry($data[2]);
		$country_code = trim($data[2]);

		
		
		if ($admin_code == _ADMIN_TYPE1_)
		{
			set_session(array($_POST['inp_user'],$admin_code,$admin_id,$country_path,$country_code));
			
			$admin_level = $db->admin_login_jtb($_POST['inp_user'],$_POST['inp_pass']);
			
			$_SESSION["session_level"] = $admin_level;
			
			
			$status = true;	
		}

		$data[0] = '<div class="clear"><label class="xlInput">&nbsp;</label><p class="msg-error"> Error : Invalid username or password </p></div>';
	}
	else
	{ 
		$status = false; 
		if (!empty($_POST['loginkey']))
		{
			$data[0] = '<div class="clear"><label class="xlInput">&nbsp;</label><p class="msg-error"> Error : Invalid username or password </p></div>';
		}
	}	
}

if ($status == true)
{
	header("Location: index.php");
}
else
{
	$themes = new c_themes("backend_login","../content/themes_backend/");
	$themes->pbody($data);	
}
?>

