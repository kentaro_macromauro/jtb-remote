<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Banner LP : Update';
	$sys_name = 'bannerlp';

	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_banner_lp($obj_id);
	
	$arr_country =  $db->get_country();
	
	$entrydate = Date('YmdHis');


	$data[3][] = input_selectbox('inp_country_pro',$arr_country[1],$arr_country[0],$raw_data[1],'Please Select','inp_cx1 selreq-country');
	
	
	
	
	$product_result = $db->select_product($raw_data[1]);
	
	
	$data[3][] = '<p id="field_product">'.input_selectbox( 'inp_product', $product_result['data'] ,$product_result['value'] ,$raw_data[7],'Please Select','inip_cx1 selreq-product').'<span class="msg-req">*</span></p>';
	
	

	$data[3][] = input_selectbox('inp_public',array('1','2','3'),array('1','2','3'),$raw_data[2],'none',"inp_cx1 selreq-public-order");

	$data[3][] = input_textbox('inp_banner_name',$raw_data[3],'inp_cx3 inpreq-name');
	$data[3][] = input_textbox('inp_link',$raw_data[4],'inp_cx3 inpreq-link').input_hiddenbox('entrydate',$entrydate);


 
 	function upload_product($img,$entrydate,$productimg)
	{
		
		return  '<p class="product-img'.$img.'">'.$productimg.'</p>
				 <input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
				 <input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />';
				
	}
		
	$path = $db->showpath_bycountry($raw_data[1]);
	
	$path_img  = '../'.$path.'/images/banner/';
	$star_start = strlen($path_img);
	
	
	foreach (glob($path_img.'*') as $filename)
	{
		$file_select =  substr( $filename , $star_start , strlen( $filename )) ;
		
		$select = explode('-',$file_select);
		
				
		if ($obj_id == $select[0])
		{			
			
			$img_upload = '<img src="'.$filename.'?'.time().'"  height="100" /><div class="height5"></div>';
		}
	}
			
	$data[3][] =  upload_product(1,$entrydate,$img_upload);
	
	
	
	
	
	
	
	$data[2] = breadcamp( array('Home','Banner LP',$raw_data[3]),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input_banner","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>