<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Souvenir : New';
	$sys_name = 'souvenir';
	$base_url = ", document_base_url : 'http://127.0.0.1/my-bus/html/'";
	$data[2] = breadcamp( array('Home','Souvenir','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[page])){
		$page_back = $sys_name.'.php&page='.$_GET[page];	
	}
	else{
		$page_back = $sys_name.'.php';	
	}


	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$arr_country =  $db->get_country() ;
	
	
	$data[3][inp_country] = input_selectbox('inp_country',$arr_country[1],$arr_country[0],'','-------Please Select-------','inp_cx1') ;
	$data[3][inp_city] = '<p id="field_city">
								<select name="inp_city" class="inp_cx1">
									<option value="0" >-------Please Select-------</option>
								</select>
						  </p>';
						  
	$data[3][inp_souvenir_code] =  input_textbox('inp_souvenir_code','','inp_cx2');
	
	$sql = 'SELECT souvenirtype_id,souvenirtype_idx,souvenirtype_name_en,souvenirtype_name_jp FROM mbus_souvenir_type ORDER BY souvenirtype_idx';
	$result = $db->db_query($sql);
	
	$arr_souvenir_type[val] = array();
	$arr_souvenir_type[name] = array();
	
	while ($record = mysql_fetch_array($result)){
		$arr_souvenir_type[val][] = $record[souvenirtype_id];
		$arr_souvenir_type[name][] = $record[souvenirtype_name_jp];
	}
	
	
	
	
	
	$data[3][inp_souvenir_type] = input_selectbox('inp_souvenir_type',
												   $arr_souvenir_type[name],
												   $arr_souvenir_type[val],
												   '',
												   '---Please Select---',
												   'inp_cx1');
	
	$data[3][affiliate] 		= input_chkbox('affiliate').'&nbsp;Yes'; 
	
	
	$data[3][affiliate_code]	= input_textbox('affiliate_code','','inp_cx2');
	$data[3][affiliate_url]		= input_textbox('affiliate_url','','inp_cx3');
	
	
	$data[3][inp_souvenir_name] = input_textbox('inp_souvenir_name','','inp_cx3');
	
	
	$arr_theme = $db->get_theme_souvenir();
	
	$box_theme = '<script type="text/javascript">
					function fnMoveSelect(select, target) {
						$(\'#\' + select).children().each(function() {
							if (this.selected) {
								
								
								$(\'#\' + target).append(this);
								$(this).attr({selected: false});
								
								if (target == "category_id")
								{
									$("#category_list").append(\'<option value="\'+$(this).val()+\'" selected="selected">\'+$(this).text()+\'</option>\');
								}
								else
								{
									$("#category_list").find(\'option\').remove() ;
									$("#category_list").append( $(\'#category_id\').html() );
									$("#category_list").find(\'option\').attr({selected: true});
								}
								
							}
						});
						// IE7再描画不具合対策
						if ($.browser.msie && $.browser.version >= 7) {
							$(\'#\' + select).hide();
							$(\'#\' + select).show();
							$(\'#\' + target).hide();
							$(\'#\' + target).show();
						}
					}
					</script>';
	
	$box_theme .= '<select name="category_select" id="category_id" style="height: 120px; min-width: 200px; float:left;" onchange="" size="10" multiple="multiple">			
				   </select>
				   <div class="box_move" style="float:left;">                            
				   <a class="btn-normal" href="javascript:;" name="on_select" onclick="fnMoveSelect(\'category_id_unselect\',\'category_id\'); return false;" >&nbsp;&nbsp;&lt;-&nbsp;Select&nbsp;&nbsp;</a>
				   <br/><br/>
				   <a class="btn-normal" href="javascript:;" name="un_select" onclick="fnMoveSelect(\'category_id\',\'category_id_unselect\'); return false;">&nbsp;&nbsp;Deselect&nbsp;-&gt;&nbsp;&nbsp;</a>
 				   </div>
				   
				   <select name="category_unselect" id="category_id_unselect" onchange="" size="10" style="height: 120px; min-width: 200px; float:left;" multiple="multiple">';
				   
				  for ($i = 0; $i < count( $arr_theme[0]) ;$i++)
				  {
					  
					 $box_theme .=  ' <option label="'.$arr_theme[0][$i].'" value="'.$arr_theme[1][$i].'">'.$arr_theme[0][$i].'</option>';
					  
				  }

	$box_theme .= '</select><select name="category_select[]" id="category_list" multiple="multiple" style="display:none;"></select>';


	/* select option  */
	$arr_option = $db->view_option_all();
	$option_data =  $arr_option['data'];
	$option_value = $arr_option['value'];
	/* select option  */

	$data[3][inp_theme] = $box_theme;		
	$data[3][inp_paid_type] = input_radio('inp_status', 
												   array('クレジットカード',
														 '現地にて現金でお支払い',
														/* 'クレジットカード + 現地にて現金でお支払い'*/),
												   array(1,2/*,3*/), 1);
	
	
	$data[3][inp_price]  = input_textbox('inp_price','','inp_cx1');
	$data[3][inp_allotment]  = input_textbox('inp_allotment','','inp_cx1');
	$data[3][inp_title_description] = '<textarea name="detail_top" class="inp_cx4" rows="10"></textarea>';
	$data[3][inp_table_description] = '<div class="table-mid">
										<table border="0" cellpadding="0" cellspacing="0" class="table-content">
										<tr>
											<td class="left"><input type="text" name="col1-left[]" value="" /></td>
											<td class="right"><input type="text" name="col1-right[]" value="" /></td>
										</tr>
										<tr>
											<td class="left"><input type="text" name="col1-left[]" value="" /></td>
											<td class="right"><input type="text" name="col1-right[]" value="" /></td>
										</tr>
										<tr>
											<td class="left"><input type="text" name="col1-left[]" value="" /></td>
											<td class="right"><input type="text" name="col1-right[]" value="" /></td>
										</tr>
										</table>
											<div class="add-more"><input type="button" name="add_more" value="Add" /></div>
										</div>';
										
	$data[3][inp_table_description] .= '<script type="text/javascript">
											$(\'input[name$="add_more"]\').click(
												function()
												{
													
													 $(this).parent().parent().find(\'.table-content\').append(\'<tr><td class="left"><input type="text" name="col1-left[]" value="" /></td><td class="right"><input type="text" name="col1-right[]" value="" /></td></tr>\' );
												}
											);
										</script>';
										
	$data[3][inp_footer_description] = '<textarea name="detail_bottom" class="inp_cx4" rows="10"></textarea>';
	
	$data[3][inp_publish]  = input_chkbox('inp_publish').'&nbsp;Yes'; 
	
	$data[3][inp_remark]   = input_memobox('inp_remark','','','inp_cx4',2);
	$data[3][inp_short_description] = input_memobox('inp_short_description','','','inp_cx3',2);

	$entrydate = Date('YmdHis');
 
 	function upload_product($img,$entrydate)
	{
		
		return  '
				 	<p class="product-img'.$img.'"></p>
				 	<input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'">
				 	<input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />
				 &nbsp;|&nbsp;&nbsp;<input type="button" class="button" onclick="return product_del_img('.$img.','.$entrydate.');" value="Delete" />			 
				 ';
				
	}	
 
 	$data[3][inp_upload_img][] = upload_product(1,$entrydate);
	$data[3][inp_upload_img][] = upload_product(2,$entrydate);
	$data[3][inp_upload_img][] = upload_product(3,$entrydate);
	$data[3][entrydate] 	   = input_hiddenbox('entrydate',$entrydate);

	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input_souvenir","../content/themes_backend/");
	
	$themes->pbody($data);
	
}
else{
	header("Location: index.php");
}
?>