<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{

    $data[1] = 'TOP Banner : Update';
    $sys_name = 'banner_top';
    $data[2] = breadcamp( array('Home','TOP Banner','Update'),array('index.php',$sys_name.'.php') ); //array name, array link
    $data[0] = array($sys_name.'_action.php'.'?page='.trim($_GET[page]).'&id='.trim($_GET[id]),$sys_name);

    if (!empty($_GET[id]))
    {
        $top_banner_id = $_GET[id];
    }
    else
    {
        header("Location: ".$data[0][0]);
    }
    if (!empty($_GET[page]))
    {
        $page_back = $data[0][0].'?page='.$_GET[page];
    }
    else
    {
        $page_back = $data[0][0];
    }

    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $raw_data = $db->view_top_banner($top_banner_id);

    $entrydate = Date('YmdHis');

    if($_SESSION[session_country] == 'ALL'){
        $data[3][] = 'TOP';
    }else{
        $data[3][] = $db->view_country_jp($_SESSION[session_country]);
    }
    $data[3][] = input_textbox('inp_idx',$raw_data['banner_idx'],'inp_cx01 inpreq-idx').'&nbsp;(1-99)&nbsp;&nbsp;1 is most high';
    $data[3][] = input_radio_line('inp_jump',array('Yes', 'No'), array(1,0), $raw_data['jump']);
    $data[3][] = input_chkbox('inp_new_window', $raw_data['new_window']).'&nbsp;Yes';
    $data[3][] = input_textbox('inp_link',$raw_data['link'],'inp_cx3 inpreq-name').input_hiddenbox('entrydate',$entrydate).input_hiddenbox('inp_country_pro',$_SESSION[session_country]);;
    $data[3]['inp_public']  = input_chkbox('inp_public',$raw_data['public']).'&nbsp;Yes';

    function upload_product($img,$entrydate,$top_banner_img)
    {
        return  '
				 	<p class="product-img'.$img.'">'.$top_banner_img.'</p><p class="inp_img"></p>
				 	<input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
                    <input class="tmp_inp_img" name="inp_img" type="hidden" value="'.$entrydate.'">
				 	<input type="button" id="buttonUpload" class="button " onclick="return banner_img('.$img.','.$entrydate.',670,300);" value="Upload" />';
    }

    $country_path = $db->showpath_bycountry($_SESSION[session_country]);
    $path_move  = '../'.$country_path.'/images/top_banner/';
    if($_SESSION[session_country] == 'ALL'){
        $path_move  = '../images/top_banner/';
    }
    $file   = $top_banner_id.'-1.jpg';

    $top_banner_img  = '<img src="'.$path_move.$file.'?'.time().'" width="300" /><div class="height5"></div>';

    $data[3][] = upload_product(1,$entrydate,$top_banner_img);

    $data[4] 	= array($top_banner_id,'update');
    $data[5]    = $page_back;


    $themes = new c_themes("backend_input_top_banner","../content/themes_backend/");
    $themes->pbody($data);
}
else
{
    header("Location: login.php");
}
?>