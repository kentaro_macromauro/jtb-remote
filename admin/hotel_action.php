<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'hotel';
	$tb_name  = _DB_PREFIX_TABLE.'hotel';
	
	$path = 'tmp/';
		
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
	
		$tb_name = _DB_PREFIX_TABLE.'hotel';
		$arr_carrer = array('hotel_id','country_iso3','city_iso3','hotel_code','hotel_name_en','hotel_name_jp','hotel_tel','address1','address2','gmap',
							'update_date','update_by','group_id');
				
		switch ($action)
		{
			case 'insert' : 

				if (!empty($_POST['inp_name_en']))
				{
					$country_iso3		= $_POST['inp_country'];
					$city_iso3			= $_POST['inp_city'];
					$hotel_code			= $_POST['inp_code'];
					$hotel_name_en		= $_POST['inp_name_en'];
					$hotel_name_jp		= $_POST['inp_name_jp'];
					$hotel_tel			= $_POST['inp_phone'];
					$address1			= $_POST['inp_address1'];
					$address2			= $_POST['inp_address2'];
					$gmap				= $_POST['inp_map'];
					$group_id			= $_POST['inp_group_id'];
					
					
					array_shift($arr_carrer);	
					
					$arr_rec = array($country_iso3,$city_iso3,$hotel_code,$hotel_name_en,$hotel_name_jp,$hotel_tel,$address1,$address2,
									 $gmap,'datetime', $user_id , $group_id);					
					
					$db->set_insert(_DB_PREFIX_TABLE.'hotel',$arr_carrer,$arr_rec);
					
					$record_id = $db->insert_id();					
					$country_path = $db->showpath_bycountry($country_iso3);
					
					$path_move  = '../'.$country_path.'/images/hotel/';
					
					
					
					@mkdir($path);
					@mkdir($path_move);
															
						$start_pos = strlen($path);
						
						foreach (glob($path.'*') as $filename)
						{
							
							$file_count =  substr($filename,19,strlen($filename));
							
							
							if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
							{
								$file_count =  substr($filename,19,strlen($filename));
								
								
								$file_name  = explode('-',$file_count);
								
								@unlink($path_move.$record_id.'-'.$file_name[0]);
								
								copy($filename,$path_move.$record_id.'-'.$file_name[0]);
								
								@unlink($filename);
								
							}
						}						
				}
				
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$record_id  = $_POST[page_id];
					
					$arr_carrer	= array('hotel_code','hotel_name_en',
										'hotel_name_jp','hotel_tel','address1','address2','gmap',
										'update_date','update_by','group_id');
					
					
					$country_iso3		= $_POST['inp_country'];
					
					$hotel_code			= $_POST['inp_code'];
					$hotel_name_en		= $_POST['inp_name_en'];
					$hotel_name_jp		= $_POST['inp_name_jp'];
					$hotel_tel			= $_POST['inp_phone'];
					$address1			= $_POST['inp_address1'];
					$address2			= $_POST['inp_address2'];
					$gmap				= $_POST['inp_map'];
					$group_id			= $_POST['inp_group_id'];
					
					
					$db->set_update(_DB_PREFIX_TABLE.'hotel',$arr_carrer,
									array($hotel_code,$hotel_name_en,$hotel_name_jp,$hotel_tel,$address1,$address2,$gmap,'datetime', $user_id , $group_id),
									'hotel_id',$page_id );	
					
					$country_path = $db->showpath_bycountry($country_iso3);
					
					$path_move  = '../'.$country_path.'/images/hotel/';
					
					
					
					@mkdir($path);
					@mkdir($path_move);
				
					
					$start_pos = strlen($path);
																		
						/*if ( $old_iso3 != $country_iso3 )
						{									
							@unlink($path_old.$record_id.'-1');

						}*/
						
						foreach (glob($path.'*') as $filename)
						{
							
							$file_count =  substr($filename,19,strlen($filename));
								
							if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
							{
								$file_count =  substr($filename,19,strlen($filename));
								
								$file_name  = explode('-',$file_count);
																							
								@unlink($path_move.$record_id.'-'.$file_name[0]);
																
								copy($filename,$path_move.$record_id.'-'.$file_name[0]);
								
								@unlink($filename);
								
							}
						}
				}
			break;
				
			case 'delete' :
			
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
		
			$db->set_delete_array($tb_name,'hotel_id',$arr_id);		
						
			$arr_del = $arr_id;
			
			$qty = 'SELECT path FROM '._DB_PREFIX_TABLE.'country';
			
			$result = $db->db_query($qty); $i=0;
			
			while ($record = mysql_fetch_array($result))
			{
				$country_path[$i] = $record[path];
				$i++;
			}			
			
			if (is_array($arr_del))
			{
			  
				for ($i = 0; $i<count($country_path); $i++)
				{
					
					$path_move  = '../'.$country_path[$i].'/images/hotel/';
					$start_pos  = strlen($path_move);
					
					@mkdir($path_move);
					
					foreach (glob($path_move.'*') as $filename)
					{
						
						$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
						$file   = $record[0];	
						
						if (in_array($file,$arr_del,true))
						{
						
							unlink($filename);
						}
					}
					
				}	
				
			}

			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>