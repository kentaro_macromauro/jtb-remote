<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'productopt';
	$tb_name  = _DB_PREFIX_TABLE.'product';
	
	$path = 'tmp/';
	$path_move  = '../product/images/product/';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
	
		$field_product = array('product_id',
							   'city_iso3',
							   'product_code',
							   'product_name_en',
							   'product_name_jp',
							   'product_theme',
							   'status_time',
							   'status_book',
							   'price_min',
							   'price_max',
							   'short_desc',
							   'description',
							   'description_en',
							   'template_id',
							   'keyword_search',
							   'remark',
							   'public',
							   'public_start',
							   'public_end',
							   'update_date',
							   'update_by',
							   'product_type',
							   'country_iso3',
							   'pax_min',
							   'pax_max',
							   'day_quantity',
							   'day_cutoff',
							   'option_id1',
							   );
		
		
		$field_product_detail = array('product_id','text_top','text_table','text_bottom');
		
		
		
		switch ($action)
		{
			case 'insert' : 
				if (!empty($_POST['inp_city']))
				{			
					
				
				
					$city_iso3	  	 = $_POST[inp_city];
					$product_code 	 = $_POST[inp_product_code];
					$product_type    = $_POST[inp_product_type];
					$product_name_en = $_POST[inp_name_en];
					$product_name_jp = $_POST[inp_name_jp];
					
					$product_category_arr = $_POST[category_select];
					$product_category = '';
					
					foreach ($product_category_arr as $text)
					{
						$product_category .= $text.';';
					}

					
					$status_time = $_POST[inp_time];
					$status_book = $_POST[inp_status];
					$price_min   = $_POST[inp_min];
					$price_max   = $_POST[inp_max];
					$short_desc  = $_POST[inp_shortdesc];
					$description = $_POST[inp_desc];
					$description_en = $_POST[inp_desc_en];
					
					$template_id = $_POST[inp_tempage];

					$keyword_search = $_POST[inp_keyword];
					$remark         = $_POST[inp_remark];
					
					//$public         = $_POST['inp_public'];
					
					if ($_POST['inp_public'] == 'on')
					{
						$public = 1;	
					}
					else
					{
						$public = 0;
					}
					
					
					
					
					
					$public_start   = 'NOW()';
					$public_end     = '';
					
					$update_date    = 'NOW()';
					$update_by      = $user_id;
					
					$country_iso3   = $_POST[inp_country];
					
						switch ($template_id)
						{
							case '1' : $text_top   = $_POST[detail_top1];
									   
									   $text_table_left1_array = $_POST['col1-left'];
									   $text_table_right1_array = $_POST['col1-right'];
									   
									   $text_table  = '';
									   
									   $record = count( $_POST['col1-left'] ) ;
									   if ( $record  < count( $_POST['col1-right'] )  )
									   {
										  $record = count( $_POST['col1-right']);
									   }
									   
									   
									   for ($i =0 ;$i <  $record; $i++)
									   {
										   if ( ( $text_table_left1_array[$i] != '') || ( $text_table_right1_array[$i] != '' ) )										   
											 $text_table .= '<tr><th>'.$text_table_left1_array[$i].'</th><td>'.$text_table_right1_array[$i].'</td></tr>';  
									   }
									   $text_bottom = $_POST[detail_bottom];
							
									   break;
									   
							case '2' : $text_top = '';
									
									   $text_table_left2_array = $_POST['col2-left'];
									   $text_table_right2_array = $_POST['col2-right'];	
									   $text_table = '';
									   
									   $record = count( $_POST['col2-left'] ) ;
									   if ( $record  < count( $_POST['col2-right'] )  )
									   {
										  $record = count( $_POST['col2-right']);
									   }
									   
									   
									   for ($i =0 ;$i <  $record ; $i++)
									   {	
									   		if ( ( $text_table_left2_array[$i] != '') || ( $text_table_right2_array[$i] != '' ) )	
											{
											 $text_table .= '<tr><th>'.$text_table_left2_array[$i].'</th><td>'.$text_table_right2_array[$i].'</td></tr>';  
											}
									   }
									   
									   $text_bottom = '';
							  
									   break;
									   
							case '3' : $text_top   = $_POST['detail_top3'];
									   $text_table = '';
									   $text_bottom = '';
								
									   break;
									   
						}
					
						
						$record_id = $db->db_newid('product');
						
						$pax_min	   = $_POST['inp_min_pax'];
						$pax_max   	   = $_POST['inp_max_pax'];
						$day_quantity  = $_POST['inp_day_quantity'];
						$day_cutoff    = $_POST['inp_day_cutoff'];	
						$option 	   = $_POST['inp_option'];
						
					    $arr_product = array( $record_id,
											$city_iso3,
											$product_code,
											$product_name_en,
											$product_name_jp,
											$product_category,
											$status_time,
											$status_book,
											$price_min,
											$price_max,
											$short_desc,
											$description,
											$description_en,
											$template_id,
											$keyword_search,
											$remark,
											$public,
											$public_start,
											$public_end,
											$update_date,
											$update_by,
											$product_type,
											$country_iso3,
											
											$pax_min,
											$pax_max,
											$day_quantity,
											$day_cutoff,
											$option,
											
											);
						
						
						
						$arr_product_detail = array( $record_id,$text_top,$text_table,$text_bottom);

						$db->db_start();
						
						$db->set_delete(_DB_PREFIX_TABLE.'product','product_id',($record_id));
						$db->set_delete(_DB_PREFIX_TABLE.'product_detail','product_id',($record_id));
						$db->set_delete(_DB_PREFIX_TABLE.'product_policy','product_id',($record_id));
						$db->set_delete(_DB_PREFIX_TABLE.'product_policy_map','product_id',($record_id));
						$db->set_delete(_DB_PREFIX_TABLE.'product_itinerary','product_id',($record_id));
						
						/* insert policy */
						
						$product_id = ''; $buffer = '';

						if (is_array( $_POST[inp_policy] ))
						{
							$arr_policy = array();
							
							
							for ($i =0; $i<count( $_POST[inp_policy]) ; $i++)
							{
								if (!empty( $_POST[inp_policy][$i]))
								{
									if (!in_array($_POST[inp_policy][$i],$arr_policy))
									{
										array_push(	$arr_policy,$_POST[inp_policy][$i]);
									}
								}
							}
							
							if ( count( $arr_policy ) > 0 )
							{
								
							
								$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_policy VALUES';
								
								foreach ( $arr_policy as $policy_id)
								{
									$sql .= '("'.$record_id.'","'.$product_code.'","'.$policy_id.'",0,NOW(),"'.$user_id.'"),';
								}
								
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
							
							
								$db->db_query($sql);
							
							}
							
							$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_policy_map VALUES';
							
							for ($i =0 ;$i<count( $_POST[inp_policy]) ; $i++)
							{
								
								$policy_select_id = $_POST['inp_policy'][$i];
								$seq              = ($i+1);
								$product_id     = $record_id;
								$description_en   = $_POST['inp_policy_detail_en'][$i];
								$description_jp   = $_POST['inp_policy_detail_jp'][$i];
								
								if ( !empty($policy_select_id))
								{
									$sql .= '("'.$product_id.'","'.$product_code.'","'.$policy_select_id.'","'.$seq.'","'.
											   htmlspecialchars($description_en).'","'.htmlspecialchars($description_jp).'",NOW(),"'.$user_id.'"),';
								}
							}
							
							if ( count( $arr_policy ) > 0 )
							{
								
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
							
								$db->db_query($sql);
							}
						}
						
						
						/* insert policy */
						
						/* insert itinerary */
						
						
						if (is_array($_POST[inp_itinerary_day]) )
						{
							$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_itinerary VALUES';
							
							for ($i=0; $i<count($_POST[inp_itinerary_day]); $i++)
							{
								$seq 		    = ($i+1);
								$product_id     = $record_id;
								$time_en  	 	= $_POST[inp_itinerary_time_en][$i];
								$time_jp  		= $_POST[inp_itinerary_time_jp][$i];
								$now_day  		= $_POST[inp_itinerary_day][$i];
								$itiner_name_en = $_POST[inp_itinerary_data_en][$i];
								$itiner_name_jp = $_POST[inp_itinerary_data_jp][$i];
								$active 		= '1';
								
								if ( !empty( $_POST[inp_itinerary_day][$i] ))
								{
									$sql .= '("'.$product_id.'","'.$product_code.'","'.$seq.'","'.htmlspecialchars($time_en).'","'.
											 htmlspecialchars($time_jp).'","'.$now_day.'","'.
										     htmlspecialchars($itiner_name_en).'","'.
											 htmlspecialchars($itiner_name_jp).'","'.$active.'",NOW(),"'.$user_id.'"),';
								}
							}
							
							if (!empty( $_POST[inp_itinerary_day][0] ) )
							{
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
								
							
								$db->db_query($sql); 
							}
							
					
							
						}
						/* insert itinerary */						

						$db->set_insert(_DB_PREFIX_TABLE.'product',$field_product,$arr_product);
						$db->set_insert(_DB_PREFIX_TABLE.'product_detail',$field_product_detail,$arr_product_detail);
						
						$db->db_updateid($record_id,'product');

						$db->db_commit();

						@mkdir($path);
						
						@mkdir($path_move);
						$start_pos = strlen($path);
						
						foreach (glob($path.'*') as $filename)
						{
							
							if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
							{
								$file_count =  substr($filename,19,strlen($filename));
								$file_name  = explode('-',$file_count);
								
								@unlink($path_move.$record_id.'-'.$file_name[0]);
								@unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');
								
								copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
								@unlink($filename);
								
							}
						}
						
						//exit();
				}
				
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{	
				
					
				
					$record_id       = $_POST[page_id];			
					$product_code 	 = $_POST[inp_product_code];
					$product_name_jp = $_POST[inp_name_jp];
					
					$city_code       = $_POST[inp_city];
					
					$product_category_arr = $_POST[category_select];
					$product_category = '';
					
					foreach ($product_category_arr as $text)
					{
						$product_category .= $text.';';
					}

					
					$status_time = $_POST[inp_time];
					$status_book = $_POST[inp_status];
					$price_min   = $_POST[inp_min];
					$price_max   = $_POST[inp_max];
					$short_desc  = $_POST[inp_shortdesc];
					$description = $_POST[inp_desc];
					$template_id = $_POST[inp_tempage];

					$keyword_search = $_POST[inp_keyword];
					$remark         = $_POST[inp_remark];
					
					
					
					
					if ( $_SESSION[session_country] == "ALL")
					{
						$country_code = $_POST[inp_country];	
					}
					else
					{
						$country_code = $_SESSION[session_country]; 	
					}
					
					//$public         = $_POST['inp_public'];
					
					if ($_POST['inp_public'] == 'on')
					{
						$public = 1;	
					}
					else
					{
						$public = 0;
					}
					
					$public_start   = 'NOW()';
					$public_end     = '';
					
					$update_date    = 'NOW()';
					$update_by      = $user_id;
					
						switch ($template_id)
						{
							case '1' : $text_top   = $_POST[detail_top1];
									   
									   $text_table_left1_array = $_POST['col1-left'];
									   $text_table_right1_array = $_POST['col1-right'];
									   
									   $text_table  = '';
									   
									   
									   $rectr = count( $_POST['col1-left'] ) ;
									   if ( $rectr  < count( $_POST['col1-right'] )  )
									   {
										  $rectr = count( $_POST['col1-right'] );
									   }
									   
									   
									   for ($i =0 ;$i < $rectr; $i++)
									   {
										   if ( ( $text_table_left1_array[$i] != '') || ( $text_table_right1_array[$i] != '' ) )									
											 $text_table .= '<tr><th>'.$text_table_left1_array[$i].'</th><td>'.$text_table_right1_array[$i].'</td></tr>';  
									   }
									   $text_bottom = $_POST[detail_bottom];
							
									   break;
									   
							case '2' : $text_top = '';
									
									   $text_table_left2_array = $_POST['col2-left'];
									   $text_table_right2_array = $_POST['col2-right'];	
									   $text_table = '';
									   
									   $rectr = count( $_POST['col2-left'] ) ;
									   if ( $rectr  < count( $_POST['col2-right'] )  )
									   {
										  $rectr = count( $_POST['col2-right'] );
									   }
									   
									   
									   for ($i =0 ;$i < $rectr; $i++)
									   {	
									   		if ( ( $text_table_left2_array[$i] != '') || ( $text_table_right2_array[$i] != '' ) )	
											 $text_table .= '<tr><th>'.$text_table_left2_array[$i].'</th><td>'.$text_table_right2_array[$i].'</td></tr>';  
									   }
									   
									   $text_bottom = '';
							  
									   break;
									   
							case '3' : $text_top   = $_POST['detail_top3'];
									   $text_table = '';
									   $text_bottom = '';
								
									   break;
									   
						}
						
						$pax_min	   = $_POST['inp_min_pax'];
						$pax_max	   = $_POST['inp_max_pax'];
						$day_quantity  = $_POST['inp_day_quantity'];
						$day_cutoff    = $_POST['inp_day_cutoff'];
						$option 	   = $_POST['inp_option'];
			
						
					
					$field_product = array('country_iso3',
										   'city_iso3',
										   
										   'product_code',
										   'product_name_jp',
										   'product_theme',
										   'status_time',
										   'status_book',
										   'price_min',
										   'price_max',
										   'short_desc',
										   'description',
										   'template_id',
										   'keyword_search',
										   'remark',
										   'public',
										   'public_start',
										   'public_end',
										   'update_date',
										   'update_by',
										   
										   'pax_min',
										   'pax_max',
										   'day_quantity',
										   'day_cutoff',
										   'option_id1',
										  
										   
										   );
					
					
						
				    $arr_product = array( 	$country_code,
										 
										 	$city_code,
										 
										 	$product_code,
											$product_name_jp,
											$product_category,
											$status_time,
											$status_book,
											$price_min,
											$price_max,
											$short_desc,
											$description,
											$template_id,
											$keyword_search,
											$remark,
											$public,
											$public_start,
											$public_end,
											$update_date,
											$update_by,
											$pax_min,
											$pax_max,
											$day_quantity,
											$day_cutoff,
											$option,
											
											);
					
					
				
						
					array_shift($field_product_detail);
					
					$db->db_start();
					
					
					
					$db->set_delete(_DB_PREFIX_TABLE.'product_policy','product_id',($record_id));
					$db->set_delete(_DB_PREFIX_TABLE.'product_policy_map','product_id',($record_id));
					$db->set_delete(_DB_PREFIX_TABLE.'product_itinerary','product_id',($record_id));
						
					
					
					/* insert policy */
						
						$product_id = ''; $buffer = '';

						if (is_array( $_POST[inp_policy] ))
						{
							$arr_policy = array();
							
							
							for ($i =0; $i<count( $_POST[inp_policy]) ; $i++)
							{
								if (!empty( $_POST[inp_policy][$i]))
								{
									if (!in_array($_POST[inp_policy][$i],$arr_policy))
									{
										array_push(	$arr_policy,$_POST[inp_policy][$i]);
									}
								}
							}
							
							if ( count( $arr_policy ) > 0 )
							{
		
							
								$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_policy VALUES';
								
								foreach ( $arr_policy as $policy_id)
								{
									$sql .= '("'.$record_id.'","'.$product_code.'","'.$policy_id.'",0,NOW(),"'.$user_id.'"),';
								}
								
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
							
							
								$db->db_query($sql);
							
							}
							
							$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_policy_map VALUES';
							
							for ($i =0 ;$i<count( $_POST[inp_policy]) ; $i++)
							{
								
								$policy_select_id = $_POST['inp_policy'][$i];
								$seq              = ($i+1);
								$description_en   = $_POST['inp_policy_detail_en'][$i];
								$description_jp   = $_POST['inp_policy_detail_jp'][$i];
								
								if ( !empty($policy_select_id))
								{
									$sql .= '("'.$record_id.'","'.$product_code.'","'.$policy_select_id.'","'.$seq.'","'.
											   htmlspecialchars($description_en).'","'.htmlspecialchars($description_jp).'",NOW(),"'.$user_id.'"),';
								}
							}
							
							if ( count( $arr_policy ) > 0 )
							{
								
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
							
								$db->db_query($sql);
							}
						}
						
						
					/* insert policy */
					
					/* insert itinerary */
						
						
						if (is_array($_POST[inp_itinerary_day]) )
						{
							$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'product_itinerary VALUES';
							
							for ($i=0; $i<count($_POST[inp_itinerary_day]); $i++)
							{
								$seq 		    = ($i+1);
								$product_id     = $record_id;
								$time_en  	 	= $_POST[inp_itinerary_time_en][$i];
								$time_jp  		= $_POST[inp_itinerary_time_jp][$i];
								$now_day  		= $_POST[inp_itinerary_day][$i];
								$itiner_name_en = $_POST[inp_itinerary_data_en][$i];
								$itiner_name_jp = $_POST[inp_itinerary_data_jp][$i];
								$active 		= '1';
								
								if ( !empty( $_POST[inp_itinerary_day][$i] ))
								{
									$sql .= '("'.$product_id.'","'.$product_code.'","'.$seq.'","'.htmlspecialchars($time_en).'","'.
											 htmlspecialchars($time_jp).'","'.$now_day.'","'.
										     htmlspecialchars($itiner_name_en).'","'.
											 htmlspecialchars($itiner_name_jp).'","'.$active.'",NOW(),"'.$user_id.'"),';
								}
							}
							
							
							
							
							if (!empty( $_POST[inp_itinerary_day][0] ) )
							{
								$sql =  substr($sql,0,strlen($sql)-1);
								$sql .= ';';
								
							
								$db->db_query($sql); 
							}
							
					
							
						}
						/* insert itinerary */
					
						
					$db->set_update(_DB_PREFIX_TABLE.'product',$field_product,$arr_product,'product_id',$record_id);

					$db->set_update(_DB_PREFIX_TABLE.'product_detail',$field_product_detail,
									array($text_top,$text_table,$text_bottom) ,'product_id',$record_id);
						
					$db->db_commit();
						
						
						
					@mkdir($path);
						
					@mkdir($path_move);
					$start_pos = strlen($path);
						
					foreach (glob($path.'*') as $filename)
					{
						
							
						if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							$file_count =  substr($filename,19,strlen($filename));
							$file_name  = explode('-',$file_count);
								
							@unlink($path_move.$record_id.'-'.$file_name[0]);
							@unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');
								
							copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
							
							@unlink($filename);
							
						}
					}					
					
				}

			break;
				
			case 'delete' :
			
			
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			$db->db_start();
			$db->set_delete_array($tb_name,$field_product[0],$arr_id);		
			$db->set_delete_array(_DB_PREFIX_TABLE.'product_detail',$field_product[0],$arr_id);
			$db->set_delete_array(_DB_PREFIX_TABLE.'product_itinerary',$field_product[0],$arr_id);
			$db->set_delete_array(_DB_PREFIX_TABLE.'product_policy',$field_product[0],$arr_id);
			$db->set_delete_array(_DB_PREFIX_TABLE.'product_policy_map',$field_product[0],$arr_id);
			
			
			$db->db_commit();
			
			$start_pos = strlen($path_move);
			
			
			foreach (glob($path_move.'*') as $filename)
			{
				$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
				$filelist[] = 	$record[0];
			}

			

			foreach ($arr_id as $id)
			{
				if (in_array($id,$filelist,true))
				{
					
					$arr_del[] = $id;
				}
			}
			
			
			if (is_array($arr_del))
			{
			
				@mkdir($path_move);
				
				
				
				foreach (glob($path_move.'*') as $filename)
				{
					$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
					$file   = $record[0];	
					
					if (in_array($file,$arr_del,true))
					{

						unlink($filename);
					}
				}
			}
			
			break;
			
			case 'active' : 
					$action_id = $_POST[action_id];
					$arr_id = str_to_arr($action_id);
					
					$db->set_public_array($tb_name,'product_id',$arr_id);		
					
			
			break;
			
			case 'unactive': 
			
					$action_id = $_POST[action_id];
					$arr_id = str_to_arr($action_id);
					
					$db->set_unpublic_array($tb_name,'product_id',$arr_id);		
					
					
			break;
			
			
		}
	}
	
	
	
	header("Location: ".$sys_name.".php?page=".$_GET[page].'&filter_country='.$_GET[filter_country].'&filter_city='.$_GET[filter_city].
			'&filter_theme='  .$_GET[filter_theme].'&filter_code='.$_GET[filter_code].'&filter_name_jp='.$_GET[filter_name_jp].
			'&filter_name_en='.$_GET[filter_name_en].'&filter_public='.trim($_GET[filter_public]));
	
	
	
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>