<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'time';
	$tb_name  = _DB_PREFIX_TABLE.'time';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('time_id','time_name','update_date','update_by'); 
			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_name]))
				{
					$time_name  = $_POST[inp_name];
				
					$arr_record = array($time_name,'datetime', $user_id);
					
					array_shift($arr_carrer);
					
					$db->set_insert(_DB_PREFIX_TABLE.'time',$arr_carrer,$arr_record);
				}
			break;
			
			case 'update' :
				if (!empty($_POST[inp_name]))
				{
					$idx		 	= $arr_carrer[0];
					array_shift($arr_carrer);
					
					
					$time_name  = $_POST[inp_name];

					$db->set_update($tb_name,$arr_carrer,
									array($time_name,'datetime', $user_id),
									$idx,$page_id);	
				}

			break;
				
			case 'delete' :
			
		
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			$db->set_delete_array($tb_name,$arr_carrer[0],$arr_id);		
			
			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>