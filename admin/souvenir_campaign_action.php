<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'souvenir_campaign';
	$tb_name  = _DB_PREFIX_TABLE.'souvenir_campaign';
	
	$path = 'tmp/';
		
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$tb_name = _DB_PREFIX_TABLE.'souvenir_campaign';
		$arr_carrer = array('scampaign_id',
							'country_iso3',
							'scampaign_tittle',
							'scampaign_detail',
							'souvenir_list',
							'public',
							'update_date',
							'update_by');				
		switch ($action)
		{
			case 'insert' : 
						
				if (!empty($_POST['inp_banner_name']))
				{
					$banner_name    = $_POST[inp_banner_name];
					$description    = $_POST[inp_desc];
					$country_id     = $_POST[inp_country_promo];
					$arr_product_id = $_POST[inp_product];
					
					if (!empty($_POST[inp_public])){
						$inp_public = '1';
					}
					else{
						$inp_public  = '0';
					}
					
					$product_list = '';
					foreach ($arr_product_id as $list)
					{
						$product_list .= $list.',';	
					}
					
					array_shift($arr_carrer);
					
					$arr_promotion = array($country_id,$banner_name,$description,$product_list,$inp_public,'datetime', $user_id  );
					
					
					$db->set_insert($tb_name,$arr_carrer,$arr_promotion);
					$record_id = $db->insert_id();		
					
					
					$country_path = $db->showpath_bycountry($country_id);
					
					@mkdir($path);
					@mkdir($path_move);
					
					if ($country_id == "ALL")
					{
						$path_move  = '../public/images/souvenir_banner/';
						
					}
					else
					{
				
						$path_move  = '../'.$country_path.'/images/souvenir_banner/';
					}
					
						$start_pos = strlen($path);
						
						foreach (glob($path.'*') as $filename)
						{
							
							$file_count =  substr($filename,19,strlen($filename));
							
							
							if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
							{
								$file_count =  substr($filename,19,strlen($filename));
								
								
								$file_name  = explode('-',$file_count);
								
								@unlink($path_move.$record_id.'-'.$file_name[0]);
								@unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');
								
								copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
								@unlink($filename);
								
							}
						}
								
				}
				
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$record_id 	     = $_POST[page_id];
					
					
					$banner_name    = $_POST[inp_banner_name];
					$description    = $_POST[inp_desc];
					$country_id     = $_POST[inp_country_promo];
					$arr_product_id = $_POST[inp_product];
					
					if (!empty($_POST[inp_public])){
						$inp_public = '1';
					}
					else{
						$inp_public  = '0';
					}
					
					
					if (!empty($_POST[inp_public]))
					{
						$inp_public = '1';
					}
					else
					{
						$inp_public  = '0';
					}
					
					$product_list = '';
					foreach ($arr_product_id as $list){
						$product_list .= $list.',';	
					}
					
					
					$raw_data = $db->view_souvenir_campaign($_POST[page_id]);
					
					
					$old_path     = $db->showpath_bycountry($raw_data[country_iso3]);

					$country_path = $db->showpath_bycountry($country_id);
	
					
					if ($country_id == "ALL")
					{
						$path_move  = '../public/images/souvenir_banner/';
					}
					else
					{
						$path_move  = '../'.$country_path.'/images/souvenir_banner/';
					}
					
										
					$path_old   ='../'.$old_path.'/images/souvenir_banner/';
					
					
					
				    array_shift($arr_carrer);
				    $db->set_update($tb_name,$arr_carrer,
									array($country_id,$banner_name,$description,  $product_list  ,$inp_public ,'datetime', $user_id),
									'scampaign_id',$page_id);	
					
					
					@mkdir($path);
					@mkdir($path_move);
										
						$start_pos = strlen($path);
																		
						if ( $raw_data[country_iso3] != $country_id )
						{									
							@unlink($path_old.$record_id.'-1');
							@unlink($path_old.$record_id.'-1.jpg');

						}
						
						foreach (glob($path.'*') as $filename)
						{
							
							$file_count =  substr($filename,19,strlen($filename));
								
							if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
							{
								$file_count =  substr($filename,19,strlen($filename));
								
								$file_name  = explode('-',$file_count);
																							
								@unlink($path_move.$record_id.'-'.$file_name[0]);
								@unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');
																
								copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');
								
								@unlink($filename);
								
							}
						}
				}
			break;
			
			case 'active': 
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_public_array($tb_name,$arr_carrer[0],$arr_id);		
			
			
			break;
			
			case 'unactive':
				$action_id = $_POST[action_id];
				$arr_id = str_to_arr($action_id);
				$db->set_unpublic_array($tb_name,$arr_carrer[0],$arr_id);		
			break;
			
				
			case 'delete' :
			
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
		
			$db->set_delete_array($tb_name,'scampaign_id',$arr_id);		
						
			$start_pos = strlen($path_move);
									
			$arr_del = $arr_id;
			
			$qty = 'SELECT path FROM '._DB_PREFIX_TABLE.'country';
			
			$result = $db->db_query($qty); $i=0;
			
			while ($record = mysql_fetch_array($result))
			{
				$country_path[$i] = $record[path];
				$i++;
			}			
			
			if (is_array($arr_del))
			{
			  
				for ($i = 0; $i<count($country_path); $i++)
				{
					
					$path_move  = '../'.$country_path[$i].'/images/souvenir_banner/';
					$start_pos  = strlen($path_move);
					
					@mkdir($path_move);
					
					foreach (glob($path_move.'*') as $filename)
					{
						
						$record = explode('-', substr($filename,$start_pos,strlen($filename)) );
						$file   = $record[0];	
						
						if (in_array($file,$arr_del,true))
						{
						
							unlink($filename);
						}
					}
					
				}	
				
			}

			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>