// ver 0.1 

var _root = 'common/js/jd_datamanager/';
var _url  = 'http://mybus.sakura.ne.jp/';
var _path = _url+'article_img/';


function show_img()
{
	_width = $(window).width();
	_height = $(window).height();
		
	if (_width < $('.jed_ddedit').width())
	{
		_width = $(document).width();
	}
	if (_height < $(document).height())
	{
		_height = $(document).height();
		
	}
	
	
	if (_width  <= $('.jed_ddedit').width()){ _boxleft = $('.jed_ddedit').width(); }
	else{_boxleft  =  ($(window).width() - $('.jed_ddedit').width() ) / 2 ;}
	
	if (_height <= $('.jed_ddedit').height()){ _boxtop =$('.jed_ddedit').height(); }
	else{_boxtop = ( $(window).height() - $('.jed_ddedit').height() ) / 2;}
	
	if ( _boxtop >  $(window).height())
	{
		_boxtop = 0;	
	}
	
	$('.jed_ddedit').css({left:_boxleft+'px',top:_boxtop+'px'});
	//$('.jed_presentbox1').css({height:_height+'px',width:_width+'px'});
	
	_top = $(window).scrollTop();
	
	//if (_top < $(window).height() )
	//{
		$('.jed_ddedit').css({top:(_top+_boxtop)+'px'});
	//}

	var _page = _root+'ajax.php';

	$('#loading').show();
	$.post(_page, { state: "showimg" },function(data) {   					   
	  $('#loading').hide();
	  $('.img_pocket').html(data);
	  dom_action();
	});
	
}


function ajaxFileUpload()
{
	
	
	var _upload_action = _root+'doajaxfileupload.php';
	
	
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});

		$.ajaxFileUpload
		(
			{
				url:_upload_action,
				secureuri:false,
				fileElementId:'fileToUpload',
				dataType: 'json',
				data:{name:'logan', id:'id'},
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							show_img();
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		
		return false;

}

function dom_action()
{
	var _page = _root+'ajax.php';
	
	$('.img_pocket li').unbind("click");
	$('#btn_insert').unbind("click");
	$('#btn_del').unbind("click");
	
	$('.img_pocket li').click(
			function()
			{
				$('.img_pocket li').removeClass('hover');
				$(this).addClass('hover');
				$('.pic_name').val( _path + $(this).find('span').html() ) ;
			}
		);	
		
		$('#btn_insert').click(
			function()
			{
					if($('.img_pocket .hover span').html())
					{
						//_img = '<img src="'+_url + $('.pic_name').val()+'" alt="'+$('.pic_desc').val()+'" />';
						_img = '<img src="' + $('.pic_name').val()+'" alt="'+$('.pic_desc').val()+'" />';
						
						_parent_name = $('#obj_parent_name').val();
						_backup = $(_parent_name).val();
						
						$(_parent_name).val(_backup+ _img);
					}
			}
		);
		
		$('#btn_del').click(
			function()
			{
				var msg = 'Are you sure you want to delete "'+$('.img_pocket .hover span').html()+'" ?';    
				if ($('.img_pocket .hover span').html())
				{
					if ( confirm(msg) )
					{
						$.post(_page, { state: "delimg", file_name : $('.img_pocket .hover span').html() },function(data) {   					   
						
							show_img();
							$('.pic_name').val('');
							
							//alert(data);
						});	
					}
				}
			}
		);
}

$(document).ready(
	function()
	{
		$('.jed_ddedit').hide();
		
		$('.btn_insertimg1').click(
			function()
			{
				
				
				$('.jed_ddedit').hide();
				$('#obj_parent_name').val('#tidybox');
				
				
				$('.jed_ddedit').show();
				show_img();
			}
		);
		
		
		$('.box_close').click(
			function()
			{	
				$('.jed_ddedit').hide();
			}
		);
	}
);

/* upload product pictures */
function product_img(_numimg,_entrydate)
{
		
	var _upload_action = _root+'productupload_img.php?img='+_numimg+'&entrydate='+_entrydate;	
	
	var dummy = new Date().getTime();	
	
	$("#loading").ajaxStart(function(){ $(this).show(); }).ajaxComplete(function(){$(this).hide();});
	
	$.ajaxFileUpload
	(
		{
			url:_upload_action,
			secureuri:false,
			fileElementId:'productToimg'+_numimg,
			dataType: 'json',
			data:{name:'logan', id:'id'},
			success: function (data, status)
			{
				
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert(data.error);
					}else
					{
							
								
						$('.product-img'+_numimg).html('<img src="'+_url+'admin/tmp/'+data.msg+'?i='+dummy+'" height="100" /><div class="height5"></div>');												
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;
}

$(document).ready(
	function()
	{
		//$('.product-img1,.product-img2,.product-img3,.product-img4,.product-img5,.product-img6,.product-img7,.product-img8,.product-img9,.product-img10').hide();	
	}
);
