<?php
	require_once("setting.php");
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{
						
			$fileupload = _PATH_UPLOAD_.strtolower($_FILES[$fileElementName]['name']);
			
			//$error = $fileupload;
			
			if ( !in_array(preg_replace('/(.*)[.]([^.]+)/is','$2',basename(strtolower($fileupload))),array('png','jpg','jpeg','gif','xbm','xpm','wbmp'))) 
			{
				$error = 'The file format is not supported.';
			}
			else
			{
			
				if ($file_size=$_FILES[$fileElementName]["size"] > _LimitFileSize_)
				{
					$error = 'Cannot upload large files than 2Mb';
				}
				else
				{
				
					move_uploaded_file($_FILES[$fileElementName]['tmp_name'],$fileupload);
					
				}
			}
	
	
	}		
	
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
	

?>