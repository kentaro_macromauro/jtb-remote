<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Policy : Update';
	$sys_name = 'policy';
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}
	

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_policy($_GET[id]);
	
		
	$data[3][0] = table_data('Policy titles EN',/*input_textbox('inp_title_en',*/$raw_data['policy_title_en'][0]/*,'inp_cx2')*/);
	$data[3][1] = table_data('Policy titles JP',input_textbox('inp_title_jp',$raw_data['policy_title_jp'][0],'inp_cx2'));
	
	$data[3][2] = table_data('Policy titles EN (Short)',/*input_textbox('inp_title_short_en',*/$raw_data['policy_title_short_en'][0]/*,'inp_cx3')*/);
	$data[3][3] = table_data('Policy titles JP (Short)',input_textbox('inp_title_short_jp',$raw_data['policy_title_short_jp'][0],'inp_cx3'));

	
	for ($i=0 ; $i < count($raw_data['id']) ; $i++)
	{
		if ($i > 0)
		{
			$number = $i+1;
		}
		
		$data[3][] = table_data('Policy contents EN '.$number,input_memobox('inp_content_en[]',$raw_data['policy_content_en'][$i],'','inp_cx4',5));
		$data[3][] = table_data('Policy contents JP '.$number,input_memobox('inp_content_jp[]',$raw_data['policy_content_jp'][$i],'','inp_cx4',5));
	
	}
	
	$data[3][] = '<tr class="last"><th>&nbsp;</th><td><div style=" padding-left:470px;"><input type="button" class="addfield" value="Add" /></div></td></tr>';

	$data[3][] = '<script type="text/javascript">
					var _field = '.($i+1).';
					var _field_data = \'\';
					$(\'.addfield\').click(
						function()
						{
							_field_data = \'<tr><th align="left" width="100" valign="top">Policy contents EN \'+_field+\'</th><td align="left" width="800" valign="top"><textarea rows="5" class="inp_cx4" name="inp_content_en[]"/></td></tr><tr><th align="left" width="100" valign="top">Policy contents JP \'+_field+\'</th><td align="left" width="800" valign="top"><textarea rows="5" class="inp_cx4" name="inp_content_jp[]"/></td></tr>\';

							$(\'table.table_data .last\').before(_field_data);	
							_field++;
						}
					);
					</script>';

	$data[2] = breadcamp( array('Home','Policy', $raw_data['policy_title_en'][0] ),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>