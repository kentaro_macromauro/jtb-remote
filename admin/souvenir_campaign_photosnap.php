<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Souvenir Special Campaign Banner';
	$sys_name = 'souvenir_campaign_photosnap';

	$data[0] = array($sys_name.'_action.php','');
	$page_back = $data[0][0];
	
	


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();

		
	if ($_SESSION["session_country"] != 'ALL')
	{
		$arr_country[1] = array ( $db->view_country_jp($_SESSION["session_country"]) );
		$arr_country[0] = array( $_SESSION["session_country"] ); 
	}
	else
	{
		$arr_country[1] = array('MY-BUS Top','台湾','インドネシア','タイ','ベトナム','カンボジア','マレーシア','シンガポール','香港','オーストラリア','ニュージーランド');
		$arr_country[0] = array('ALL','TWN','IDN','THA','VNM','KHM','MYS','SGP','HKG','AUS','NZL');
	}
		
	
	if ($_SESSION["session_country"] != 'ALL')
	{
		$arr_pagetype[1] = array('オプショナルツアー');
	}
	else
	{
		$arr_pagetype[1] = array('Index','オプショナルツアー');
		
	}
	
	$arr_pagetype[0] = array('TOP','OPT','PKG');
	
	$entrydate = Date('YmdHis');
	
	
	
	
	if (!empty($select_type))
	{
	
		if ($select_type == "TOP")
		{
			$inp_promo_data1 = $data->fetch_promotion_country($select_country,'OPT');
			$inp_promo_data2 = $data->fetch_promotion_country($select_country,'OPT');
			$inp_promo_data3 = $data->fetch_promotion_country($select_country,'PKG');
		}
		else
		{
			$inp_promo_data1 = $data->fetch_promotion_country($select_country,$select_type);
			$inp_promo_data2 = $data->fetch_promotion_country($select_country,$select_type);
			$inp_promo_data3 = $data->fetch_promotion_country($select_country,$select_type);
		}
	}
	
	
	
	
	$req = '<span class="msg-req">*</span>';
	
	$data[3]['inp_country']    = input_selectbox('inp_country',$arr_country[1],$arr_country[0],'','---------Please Select---------','inp_cx1');
	$data[3]['inp_page_type']  = input_selectbox('inp_page_type',$arr_pagetype[1],$arr_pagetype[0],'','---------Please Select---------','inp_cx1');
	$data[3]['inp_banner1']	   = input_selectbox('inp_promotion1',$arr_promotion1[1],$arr_promotion1[0],'','---Please Select---','inp_cx4');
	$data[3]['inp_banner2']	   = input_selectbox('inp_promotion2',$arr_promotion2[1],$arr_promotion2[0],'','---Please Select---','inp_cx4');
	$data[3]['inp_banner3']	   = input_selectbox('inp_promotion3',$arr_promotion3[1],$arr_promotion3[0],'','---Please Select---','inp_cx4');
	

	
	$data[3]['script']		   = '	 $(\'select[name$="inp_country"]\').change( function(){
	
									if ( $(this).val()  == "ALL")
									{  
										$.post(\'ajax_view_souvenir_campaign.php\',
												{ 
												  select_country : $(\'select[name$="inp_country"]\').val()
												},
												function(data) 
												{
													
													$(\'#table_record_data\').html(data);
												});	

									}
									else
									{   
										
	
												$.post(\'ajax_view_souvenir_campaign.php\',
												{ 
												  select_country : $(\'select[name$="inp_country"]\').val()
												 
												},
												function(data) 
												{
													
													$(\'#table_record_data\').html(data);
												});								
									
									
									} });';
	
	
	$data[2] = breadcamp( array('Home','Special Campaign Banner'),array('index.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input_photosnap_souvenir_campaign","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>