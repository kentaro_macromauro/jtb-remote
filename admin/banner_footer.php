<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/;
	$data[0] = 'FOOTER Banner';	/*----set h1 name-------*/
	$data[5] = 'banner_footer';  /* system page name */
	$data[1] = breadcamp( array('Home','Footer Banner'),array('index.php') ); /*---create breadcamp---*/

	/*-----------set header table-------------*/
	$data[2] = array(
                     tb_head('idx','65'),
                     tb_head('image','240'),
                     tb_head('jump','65'),
                     tb_head('new_window','65'),
                     tb_head('link','210'),
                     tb_head('publish','65')
    ) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->count_footer_banner($_SESSION[session_country],NULL);
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);

    $data['data_count'] = $page_count;
    $data['country_iso'] = $_SESSION[session_country];
	/*-----------find now page----------------*/

    /*-----------create main table data-------*/
	$record = $db->fetch_footer_banner($page_first,$page_num,$_SESSION[session_country],NULL);
	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';

    //for the image
    $country_path = $db->showpath_bycountry($_SESSION[session_country]);
    $path  = '../'.$country_path.'/images/footer_banner/';
    if($_SESSION[session_country] == 'ALL'){
        $path  = '../images/footer_banner/';
    }

	for ($i=0;$i< count($record['footer_banner_id']); $i++)
	{
		if ( $record['public'][$i] == 0)
		{
			$publish_status = '<div class="bg_noneapprove">&nbsp;</div>';
		}
		else
		{
            $publish_status = '<div class="bg_approve">&nbsp;</div>';
		}
        if ( $record['new_window'][$i] == 0)
        {
            $window_status = '<div class="bg_noneapprove">&nbsp;</div>';
        }
        else
        {
            $window_status = '<div class="bg_approve">&nbsp;</div>';
        }
        if ( $record['jump'][$i] == 0)
        {
            $jump_status = '<div class="bg_noneapprove">&nbsp;</div>';
        }
        else
        {
            $jump_status = '<div class="bg_approve">&nbsp;</div>';
        }

        $image_name = $record['footer_banner_id'][$i].'-1.jpg';
        $image_tag =  '<img src="'.$path.$image_name.'?'.time().'" width="240px" />';
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record['footer_banner_id'][$i]), $record['banner_idx'][$i], $image_tag, $jump_status, $window_status, $record['link'][$i], $publish_status),
		array('','','',$rec_linkprefix.$record['footer_banner_id'][$i],'','', $rec_linkprefix.$record['footer_banner_id'][$i],'' ),
		array(1,1,1,0,1,1,1,1));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['footer_banner_id'] as $raw_id)
	{
		$data[6] .= $raw_id.',';
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table_approve","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>