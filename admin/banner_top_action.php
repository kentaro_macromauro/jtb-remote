<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");


if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'banner_top';
	$tb_name  = _DB_PREFIX_TABLE.'top_banner';

    $path = 'tmp/';

/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{
		$arr_carrer = array('banner_idx','country_iso3','jump','new_window','link','public','update_date','update_by');

		switch ($action)
		{
			case 'insert' : 
				if (!empty($_POST['inp_img']))
				{
                    $public_id    = 0;
					if($_POST[inp_public] == 'on'){
                        $public_id    = 1;
                    }

                    $new_window    = 0;
                    if($_POST[inp_new_window] == 'on'){
                        $new_window    = 1;
                    }

                    $banner_idx   = $_POST[inp_idx];
                    if(empty($_POST[inp_idx])){
                        $banner_idx = 1;
                    }

					$country_iso3 = $_POST[inp_country_pro];
                    $jump         = $_POST[inp_jump];
					$link         = $_POST[inp_link];

					$arr_banner = array($banner_idx,$country_iso3,$jump,$new_window,$link,$public_id,'datetime', $user_id);

					$db->set_insert($tb_name,$arr_carrer,$arr_banner);
					
					$record_id = $db->insert_id();

                    $country_path = $db->showpath_bycountry($country_iso3);
                    $path_move  = '../'.$country_path.'/images/top_banner/';
                    if($country_iso3 == 'ALL'){
                        $path_move  = '../images/top_banner/';
                    }

                    @mkdir($path);
                    @mkdir($path_move);

                    $start_pos = strlen($path);
						
                    foreach (glob($path.'*') as $filename)
                    {

                        $file_count =  substr($filename,19,strlen($filename));

                        if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
                        {
                            $file_count =  substr($filename,19,strlen($filename));


                            $file_name  = explode('-',$file_count);

                            @unlink($path_move.$record_id.'-'.$file_name[0]);
                            @unlink($path_move.$record_id.'-'.$file_name[0].'.jpg');

                            copy($filename,$path_move.$record_id.'-'.$file_name[0].'.jpg');

                            @unlink($filename);

                        }
                    }
				}

			break;
			case 'update' :
				if (!empty($_POST[top_banner_id]))
				{
                    $action_id   = $_POST[top_banner_id];

                    $raw_data = $db->view_top_banner($action_id);

                    $public_id    = 0;
                    if($_POST[inp_public] == 'on'){
                        $public_id    = 1;
                    }

                    $new_window    = 0;
                    if($_POST[inp_new_window] == 'on'){
                        $new_window    = 1;
                    }
                    $banner_idx   = $_POST[inp_idx];
                    if(empty($_POST[inp_idx])){
                        $banner_idx = 1;
                    }

                    $country_iso3 = $_POST[inp_country_pro];
                    $jump         = $_POST[inp_jump];
                    $link         = $_POST[inp_link];

                    $arr_banner = array($banner_idx,$country_iso3,$jump,$new_window,$link,$public_id,'datetime', $user_id);

					$db->set_update($tb_name,$arr_carrer,$arr_banner,'top_banner_id',$action_id);

                    //check image upload
                    $country_path = $db->showpath_bycountry($country_iso3);
                    $path_move  = '../'.$country_path.'/images/top_banner/';
                    if($country_iso3 == 'ALL'){
                        $path_move  = '../images/top_banner/';
                    }

					//check befor insert new image
					@mkdir($path);
					@mkdir($path_move);

					$start_pos = strlen($path);
					foreach (glob($path.'*') as $filename)
					{
                        if ( substr($filename,$start_pos,14) == $_POST[entrydate] )
						{
							@unlink($path_move.$action_id.'-1.jpg');

							copy($filename,$path_move.$action_id.'-1.jpg');

							@unlink($filename);
						}
					}
				}
			break;
				
			case 'delete' :

                $action_id = $_POST[action_id];
                $arr_id = str_to_arr($action_id);

                $db->set_delete_array($tb_name,'top_banner_id',$arr_id);

                $country_path = $db->showpath_bycountry($country_iso3);
                $path_move  = '../'.$country_path.'/images/top_banner/';
                if($country_iso3 == 'ALL'){
                    $path_move  = '../images/top_banner/';
                }

                $start_pos = strlen($path_move);

                @mkdir($path_move);

                foreach ($arr_id as $ids)
                {
                    $file   = $ids.'-1.jpg';

                    unlink($path_move.$file);
                }
                break;

            case 'active' :
                $action_id = $_POST[action_id];
                $arr_id = str_to_arr($action_id);

                $db->set_public_array($tb_name,'top_banner_id',$arr_id);

                break;

            case 'unactive':
                $action_id = $_POST[action_id];
                $arr_id = str_to_arr($action_id);

                $db->set_unpublic_array($tb_name,'top_banner_id',$arr_id);

                break;
        }
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>