<? 

require_once("../www_config/setting.php");
require_once("../class/include/c_query.php");
require_once("../class/c_query_sub.php");
require_once("../class/c_session.php");
require_once("../class/c_cookie.php");
require_once("../class/c_themes.php");

$status = false;

if (check_session())
{
	if ($_SESSION["session_code"] == _ADMIN_TYPE1_)
	{ 
		$status = true; 

	}
	else
	{
		$status = false;	
	}
	/*check premission if premission is pass "status = true" */
}


if ($status == true)
{

	$themes = new c_themes("backend_index","../content/themes_backend/");
	$themes->pbody();
}
else
{
	header("Location: logout.php");
}
?>

