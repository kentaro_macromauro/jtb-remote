<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
	$data[0] = 'Policy';	/*----set h1 name-------*/
	$data[5] = 'policy';  /* system page name */
	$data[1] = breadcamp( array('Home','Policy'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(
					 tb_head('Tittles EN','280'),
					 tb_head('Tittles JP','280'),
					 tb_head('Update Date','70'),
					 tb_head('Update By','100')) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->page_count("policy");	
	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fatch_policy($page_first,$page_num);	
	
	
	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';

	
	for ($i=0;$i< count($record['id']); $i++)
	{
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
			   checkbox($record['id'][$i]), $record['policy_title_en'][$i], $record['policy_title_jp'][$i], 
			       $record['update_date'][$i], $record['update_by'][$i]),
		array('','',
			  $rec_linkprefix.$record['id'][$i], $rec_linkprefix.$record['id'][$i], $rec_linkprefix.$record['id'][$i], $rec_linkprefix.$record['id'][$i]),
		array(2,1,0,0,0,0));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>