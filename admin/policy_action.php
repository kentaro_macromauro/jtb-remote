<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	$sys_name = 'policy';
	$tb_name  = _DB_PREFIX_TABLE.'policy';
	$tb_name_sub = _DB_PREFIX_TABLE.'policy_detail';
/*----------connect DB--------------*/
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
	if (!empty($_POST[action]))
	{	
		$arr_carrer = array('policy_id','policy_title_en','policy_title_jp','policy_title_short_en',
							'policy_title_short_jp','public','update_date','update_by'); 
		
		$arr_carrer_sub = array('policy_id','seq','policy_content_en','policy_content_jp');

			
		switch ($action)
		{
			case 'insert' :
				if (!empty($_POST[inp_title_en]))
				{
					
					$inp_title_en 		= $_POST[inp_title_en];
					$inp_title_jp 		= $_POST[inp_title_jp];
					$inp_title_short_en = $_POST[inp_title_short_en];
					$inp_title_short_jp = $_POST[inp_title_short_jp];		
					
					$arrinp_content_en  = $_POST[inp_content_en];
					$arrinp_content_jp  = $_POST[inp_content_jp];
					

					$db->db_start();
					
					$policy_id = $db->db_newid('policy');
					
					$data      = array($policy_id, $inp_title_en , $inp_title_jp,$inp_title_short_en,$inp_title_short_jp,'1','datetime',$user_id);
					
					
					$db->set_insert($tb_name,$arr_carrer,$data);
					
					for ($i =0;$i<count($arrinp_content_jp); $i++)
					{	
						$data_sub = array($policy_id, ($i+1), $arrinp_content_en[$i],$arrinp_content_jp[$i]);
						$db->set_insert($tb_name_sub,$arr_carrer_sub,$data_sub);
					}
					
					
					$db->db_updateid($policy_id,'policy');
					
					
					$db->db_commit();
			
				}
			break;
			
			case 'update' :
				if (!empty($_POST[page_id]))
				{
					$inp_title_jp 		= $_POST[inp_title_jp];
					$inp_title_short_jp = $_POST[inp_title_short_jp];		
					
					$arrinp_content_en  = $_POST[inp_content_en];
					$arrinp_content_jp  = $_POST[inp_content_jp];
					
					
					$arr_carrer     = array('policy_title_jp','policy_title_short_jp','update_date','update_by'); 
					
					$db->db_start();
					
					$db->set_update($tb_name,$arr_carrer,
									array($inp_title_jp,$inp_title_short_jp,'datetime', $user_id),
									'policy_id',$page_id);	
					
					$db->set_delete($tb_name_sub,'policy_id',$page_id);
					
					for ($i =0;$i<count($arrinp_content_jp); $i++)
					{	
						$data_sub = array($page_id, ($i+1), $arrinp_content_en[$i],$arrinp_content_jp[$i]);
						$db->set_insert($tb_name_sub,$arr_carrer_sub,$data_sub);
					}
					
					$db->db_commit();
					

				}

			break;
				
			case 'delete' :
			
		
			$action_id = $_POST[action_id];
			$arr_id = str_to_arr($action_id);
			
			$db->db_start();
			$db->set_delete_array($tb_name,'policy_id',$arr_id);		
			$db->set_delete_array($tb_name_sub,'policy_id',$arr_id);	
			$db->db_commit();
			
			break;
		}
	}
	header("Location: ".$sys_name.".php?page=".$_GET[page]);
/*-----------------------------------process insert,update,delete-------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>