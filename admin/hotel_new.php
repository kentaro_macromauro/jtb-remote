<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Hotel : New';
	$sys_name = 'hotel';
	$data[2] = breadcamp( array('Home','Hotel','New'),array('index.php',$sys_name.'.php') ); //array name, array link
	
	
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'&page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	$page_submit = $data[0][0];
	$data[0][0]  = $page_submit;


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$record = $db->get_country();
	
	$arr_data  = $record[1];
	$arr_value = $record[0];	
	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$arr_country =  $db->get_country() ;
	
	$entrydate = Date('YmdHis');
	
	function upload_product($img,$entrydate)
	{
		
		return  '
				 	<p class="product-img'.$img.'"></p>
				 	<input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'">
				 	<input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />
				 ';
				
	}
	
	$req = '<span class="msg-req">*</span>';
	
	$data[3]['inp_country']  = input_selectbox('inp_country',$arr_country[1],$arr_country[0],'','-------Please Select-------','inp_cx1').$req ;
	$data[3]['inp_city'] 	 = '<p id="field_city"><select name="inp_city" class="inp_cx1"><option value="0" >-------Please Select-------</option></select><span class="msg-req">*</span></p>';
	$data[3]['inp_code']	 = input_textbox('inp_code','','inp_cx2').$req;
	$data[3]['inp_group_id']     = input_textbox('inp_group_id','','inp_cx2');
	
	$data[3]['inp_name_en']  = input_textbox('inp_name_en','','inp_cx3').$req;
	$data[3]['inp_name_jp']  = input_textbox('inp_name_jp','','inp_cx3').$req;
	$data[3]['inp_phone']    = input_textbox('inp_phone','','inp_cx2');
	
	$data[3]['inp_address1'] = input_textbox('inp_address1','','inp_cx3');
	$data[3]['inp_address2'] = input_textbox('inp_address2','','inp_cx3');
	
	$data[3]['inp_map']		 = input_memobox('inp_map','','','inp_cx3',5).input_hiddenbox('entrydate',$entrydate);
	
	$data[3]['inp_logo']	 = upload_product(1,$entrydate);


	$data[4] 	= array(0,'insert');	
	$data[5]    = $data[0][0];

	$themes = new c_themes("backend_input_hotel","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: index.php");
}
?>

