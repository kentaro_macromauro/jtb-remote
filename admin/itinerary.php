<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{	
/*----------connect DB--------------*/
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
/*----------connect DB--------------*/

/*------------------------------------view table-------------------------------------------------------------------*/
	$db->db_connect();
	$data[0] = 'Itinerary';	/*----set h1 name-------*/
	$data[5] = 'itinerary';  /* system page name */
	$data[1] = breadcamp( array('Home','Itinerary'),array('index.php') ); /*---create breadcamp---*/
	
	/*-----------set header table-------------*/
	$data[2] = array(tb_head('Itinerary Name En','295'),
					 tb_head('Itinerary Name Jp','295'),
					 tb_head('Update Date','70'),
					 tb_head('Update By','70')) ;
	/*-----------set header table-------------*/

	/*-----------find now page----------------*/
	$page_num = 20;
	$page_count = $db->page_count("itinerary_obj");	
	$page_now   = pagenavi_start($page_count,$page_num,$_GET[page]);
	$page_first = pagenavi_first($page_now,$page_num);
	/*-----------find now page----------------*/

	/*-----------create main table data-------*/
	$record = $db->fetch_itinerary($page_first,$page_num);		  
	$rec_linkprefix = $data[5].'_edit.php?page='.$page_now.'&id=';
	
	
	for ($i=0;$i< count($record['itiner_id']); $i++)
	{
		$data[3][$i]  = tb_normal( 
		array(($page_first+$i+1),
		      checkbox($record['itiner_id'][$i]), $record['itiner_name_en'][$i], $record['itiner_name_jp'][$i], $record['update_date'][$i], $record['update_by'][$i]),
		array('','',
			  $rec_linkprefix.$record['itiner_id'][$i], $rec_linkprefix.$record['itiner_id'][$i], $rec_linkprefix.$record['itiner_id'][$i], $rec_linkprefix.$record['itiner_id'][$i],$rec_linkprefix.$record['itiner_id'][$i] ),
		array(2,1,0,0,0,0,2));
	}
	/*-----------create main table data-------*/

	/*----set namepage for action check all and delete item-----*/
	foreach ($record['itiner_id'] as $raw_id) 
	{
		$data[6] .= $raw_id.',';	
	}
	$data[7] = $data[5].'_action.php?page='.$page_now;
	/*----set namepage for action check all and delete item-----*/
	
	/*----create footer navigation----*/
	$data[4] = pagenavi( $page_now, $page_count ,$data[5].'.php?page=',$page_num); 
	/*----create footer navigation----*/
	
	/*----send all data to themespage-------*/
	$themes = new c_themes("backend_table","../content/themes_backend/");
	$themes->pbody($data);
	/*----send all data to themespage-------*/
/*------------------------------------view table-------------------------------------------------------------------*/
}
else
{
	header("Location: index.php");
}
?>