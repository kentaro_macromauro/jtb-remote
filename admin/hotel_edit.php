<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Hotel : Update';
	$sys_name = 'hotel';

	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_hotel($obj_id);
	
	$arr_country =  $db->get_country();
	
	$entrydate = Date('YmdHis');

	
	$req = '<span class="msg-req">*</span>';
	
	$data[3]['inp_country']  = $raw_data['country_iso3'].input_hiddenbox('inp_country',$raw_data['country_iso3']) ;
	$data[3]['inp_city'] 	 = $raw_data['city_iso3'];
	$data[3]['inp_code']	 = input_textbox('inp_code',$raw_data['hotel_code'],'inp_cx2').$req;
	$data[3]['inp_group_id']     = input_textbox('inp_group_id',$raw_data['group_id'],'inp_cx2');	
	
	$data[3]['inp_name_en']  = input_textbox('inp_name_en',$raw_data['hotel_name_en'],'inp_cx3').$req;
	$data[3]['inp_name_jp']  = input_textbox('inp_name_jp',$raw_data['hotel_name_jp'],'inp_cx3').$req;
	$data[3]['inp_phone']    = input_textbox('inp_phone',$raw_data['hotel_tel'],'inp_cx2');
	
	$data[3]['inp_address1'] = input_textbox('inp_address1',$raw_data['address1'],'inp_cx3');
	$data[3]['inp_address2'] = input_textbox('inp_address2',$raw_data['address2'],'inp_cx3');
	
	$data[3]['inp_map']		 = input_memobox('inp_map',$raw_data['gmap'],'','inp_cx3',5).input_hiddenbox('entrydate',$entrydate);
	
	
	
	function upload_product($img,$entrydate,$productimg)
	{
		
		return  '<p class="product-img'.$img.'">'.$productimg.'</p>
				 <input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
				 <input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />';
				
	}
	
	$path = $db->showpath_bycountry($raw_data[1]);
	
	$path_img  = '../'.$path.'/images/hotel/';
	$star_start = strlen($path_img);
	
	
	foreach (glob($path_img.'*') as $filename)
	{
		$file_select =  substr( $filename , $star_start , strlen( $filename )) ;
		
		$select = explode('-',$file_select);
		
				
		if ($obj_id == $select[0])
		{			
			
			$img_upload = '<img src="'.$filename.'?'.time().'"  height="100" /><div class="height5"></div>';
		}
	}
	
	$data[3]['inp_logo']	 = upload_product(1,$entrydate,$img_upload);
	
	$data[2] = breadcamp( array('Home','Hotel',$raw_data['hotel_name_jp']),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input_hotel","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>