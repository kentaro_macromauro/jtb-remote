<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'News content (CMS) : Update';
	$sys_name = 'country_news';
	$data[0] = array($sys_name.'_action.php',$sys_name);
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}

	
	$public = "";
	$status = "";
	

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_news($_GET[id]);
	
	
	if ($raw_data['public'] == '1')
	{
		$public = true;	
	}
	
	if ($raw_data['icon'] == '1')
	{
		$status  = true;	
	}
	
	if ( $raw_data[country_iso3 ] != $_SESSION[session_country])
	{
		header("Location: country_news.php");
	}
	
	
	
	$data[3][] = table_data('Country' , $db->view_country_jp($raw_data[country_iso3 ]),'','');
	$data[3][] = table_data('Subject',input_textbox('inp_subject',$raw_data[news_subject],'inp_cx2'),'','',1);
	$data[3][] = table_data('Content',input_memobox('inp_description',$raw_data[news_countent ],'id="tidybox"','inp_cx4',20),'','',1);
	$data[3][] = table_data('','<span class="btn_insertimg1">Insert Picture</span>');
	$data[3][] = open_tinymce();
	
	$data[3][] = table_data('Date', date_picker($raw_data[news_date]),'','',1 );
	$data[3][] = table_data('Publish', input_chkbox('inp_public',$public).'&nbsp;Yes');
	$data[3][] = table_data('Status', input_chkbox('inp_status',$status).'&nbsp;<img src="../images/icon_new.gif" alt="New" />');
		

	$data[2] = breadcamp( array('Home','News content (CMS)',$raw_data[news_subject]),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;

	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>