<?
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{

    $data[1] = 'OPT : Products : Detail';
    $sys_name = 'productopt_api';
    $data[0] = array($sys_name);

    if (!empty($_GET[id]))
    {
        $obj_id = $_GET[id];
    }
    else
    {
        header("Location: ".$data[0][0]);
    }

    $page_back = $data[0][0];


    $db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
    $db->db_connect();

    $raw_data = $db->view_product($obj_id);

    /* Select Theme */
    $buff_arr1 = explode(';',$raw_data['product_theme']);
    $sql  =  'SELECT theme_id,theme_name FROM ';
    $sql .=  '(SELECT IF(theme_id<100,IF (theme_id<10,CONCAT(\'00\',theme_id),
			  CONCAT(\'0\',theme_id)),theme_id) theme_id,theme_name FROM '._DB_PREFIX_TABLE.'theme ) AS a ';
    $sql .=  'WHERE ';
    foreach ($buff_arr1 as $text)
    {
        $sql .= '(theme_id  =  "'.$text.'") OR ';
    }
    $sql .= '(theme_id = "'.$buff_arr1[0].'") ORDER BY theme_name ';
    $record = $db->db_query($sql);
    $buffer_left = '';
    while ($rec = mysql_fetch_array($record))
    {
        $buffer_left .= $rec['theme_name'].','	;
    }
    $product_detail_link = get_jtb_product_url($raw_data['product_code'], '', $raw_data['country_iso3'], $raw_data['city_iso3']).'" target="_blank;"';
    $data[3][]  = $raw_data['country_name_jp'];
    $data[3][]  = $raw_data['city_name_jp'];
    $data[3][]  = '<a href="'.$product_detail_link . '>' .$raw_data['product_code'] .'</a>';

    $data[3][] = $raw_data['product_type'];

    $data[3][]  = $raw_data['product_name_en'];
    $data[3][]  = $raw_data['product_name_jp'];

    $data[3][] = $buffer_left;

    $data[3][] = $db->view_time($raw_data['status_time'])[0];
    $data[3]['option'] = $db->view_option($raw_data['status_book'])[0];

    $data[3][] = $raw_data['sign'].$raw_data['price_min'];
    $data[3][] = $raw_data['sign'].$raw_data['price_max'];

    $data[3][] = $raw_data['short_desc'];
    $data[3][] = '<img src="' . $raw_data['picture_url'].'?'.time().'" width="500px">';



    $data[2] = breadcamp( array('Home','OPT : API Products',$raw_data['product_name_en']),array('index.php',$sys_name.'.php') ); //array name, array link

    $data[4] 	= array($obj_id,'update');
    $data[5]    = $page_back;


    $themes = new c_themes("backend_product_detail","../content/themes_backend/");
    $themes->pbody($data);
}
else
{
    header("Location: login.php");
}
?>