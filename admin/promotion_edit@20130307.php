<? 
require_once("include/header.php");
require_once($path."class/c_action.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	$data[1] = 'Special Campaign Page (CMS) : Update';
	$sys_name = 'promotion';

	$data[0] = array($sys_name.'_action.php','');
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_promotion($obj_id);
		
	
	$entrydate = Date('YmdHis');

	
	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	
	$entrydate = Date('YmdHis');
	
	if ($_SESSION["session_country"] != 'ALL')
	{
	
		$arr_country = array(0 => array($_SESSION["session_country"]) , 1 => array( $db->view_country_jp($_SESSION["session_country"]) ));
	}
	else
	{
		$arr_country = array(0 => array('ALL') , 1 => array('ALL'));
	
		$result_country = $db->get_country();
	
		for ($i = 0; $i < count($result_country[0] ) ; $i++)
		{
			array_push($arr_country[0],$result_country[0][$i]);
			array_push($arr_country[1],$result_country[1][$i]);
		}
		
	}

	
	
	$data[3][inp_country] = input_selectbox('inp_country_promo',$arr_country[1],$arr_country[0],$raw_data[country_iso3],'-------Please Select-------','inp_cx1 selreq-country');
	
	//$data[3][inp_product_type] = input_selectbox('inp_type_promo',array("PKG","OPT"),array("PKG","OPT"),$raw_data[product_type],'Please Select','inp_cx0');
	
	$data[3][inp_product_type] = input_selectbox('inp_type_promo',array("OPT"),array("OPT"),$raw_data[product_type],'Please Select','inp_cx0');
	
	$data[3][inp_product_content] = input_memobox('inp_desc',$raw_data[promo_detail],'','inp_cx4',10);

	$arr_product_list =  explode(',',$raw_data[product_list]); 
	
	$buff_temp = '';
	
	$ck = 0;
	
	for ($i =0; $i < count($arr_product_list); $i++)
	{
		if (!empty( $arr_product_list[$i] ) )
		{		
			/*if ($raw_data[product_type] == "PKG")		
			{
				$result = $db->view_product_pkg($raw_data[country_iso3]);
				
				$buff_temp .= '<li>'.input_selectbox('inp_product[]',$result['data'],$result['value'],$arr_product_list[$i],'------------------------------------------Please Select------------------------------------------',"inp_cx4 selreq-product").'</li>';
			}*/
			
			if ($raw_data[product_type] == "OPT")
			{
				
				if ( $arr_product_list[$i] == 174)
				{
				$buff_temp .= '<li><input type="text" readonly="readonly" name="inp_product[]" value="174" /><span class="msg-req">* Custom Product</span></li>';
				}
				else
				{
				$result = $db->view_product_opt($raw_data[country_iso3]);
				
				$buff_temp .= '<li>'.input_selectbox('inp_product[]',$result['data'],$result['value'],$arr_product_list[$i],'------------------------------------------Please Select------------------------------------------',"inp_cx4 selreq-product").'</li>';
				}
				
				$ck++;
			}
		}
		

	}
	
	$data[3][inp_product_id]   = $buff_temp;	
	
	if ($ck == 0)
	{
		
		$result = $db->view_product_opt($raw_data[country_iso3]);
		$data[3][inp_product_id]   = '<li>'.input_selectbox('inp_product[]',$result['data'],$result['value'],$arr_product_list[$i],'------------------------------------------Please Select------------------------------------------',"inp_cx4 selreq-product").'</li>';		
		
	}



	
	function upload_product($img,$entrydate,$productimg)
	{
		
		return  '<p class="product-img'.$img.'">'.$productimg.'</p>
				 <input id="productToimg'.$img.'"  type="file" name="fileToUpload'.$img.'" />
				 <input type="button" id="buttonUpload" class="button " onclick="return product_img('.$img.','.$entrydate.');" value="Upload" />';
				
	}
	

	$data[3][inp_product_name] = input_textbox('inp_banner_name',$raw_data[promo_tittle],'inp_cx3').input_hiddenbox('entrydate',$entrydate);
	$data[3][inp_public]       = input_chkbox('inp_public',$raw_data[product_public]).' Yes';
 
 
 
 	$path = $db->showpath_bycountry($raw_data[country_iso3]);
	
	if ($raw_data[country_iso3] == "ALL")
	{
		$path = 'public';
	}
	
	$path_img  = '../'.$path.'/images/banner/';
	
	$star_start = strlen($path_img);
	
	
	foreach (glob($path_img.'*') as $filename)
	{
		$file_select =  substr( $filename , $star_start , strlen( $filename )) ;
		
		$select = explode('-',$file_select);
		
				
		if ($obj_id == $select[0])
		{			
			
			$img_upload = '<img src="'.$filename.'"  height="100" /><div class="height5"></div>'; 									
		}
	}
			
	$data[3][inp_banner] =  upload_product(1,$entrydate,$img_upload);
	
	
	$data[3]['script']   = '$(\'select[name$="inp_country_promo"],select[name$="inp_type_promo"]\').change(
								function()
								{					
									$.post(\'ajax_view_product_custom.php\',
									{ 
									  value1 : $(\'select[name$="inp_country_promo"]\').val(), 
									  value2 : $(\'select[name$="inp_type_promo"]\').val()  
									},
									function(data) 
									{
										
										$(\'select[name$="inp_product[]"]\').parent().html(data);
									});								
								}
							);
	
							
	
							$(\'input[name$="inp_addproduct"]\').click(
								function()
								{
									
									_list = ( $(this).parent().parent().find(\'ul li:first-child\').html())  ;
									
									
									$(this).parent().parent().find(\'ul\').append( \'<li>\' +_list + \'<\/li>\' );
								
								}
							);';
	
	
	$data[2] = breadcamp( array('Home','Special Campaign Page (CMS)',$raw_data[3]),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input_promotion","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>