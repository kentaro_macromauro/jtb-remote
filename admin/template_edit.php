<? 
require_once("include/header.php");
require_once($path."class/c_query_sub.php");
if ($status == true)
{
	
	$data[1] = 'Template Setting : Update';
	$sys_name = 'template';
	$data[0] = array($sys_name.'_action.php','');
	$base_url = ", document_base_url : 'http://127.0.0.1/my-bus/html/'";
	
	if (!empty($_GET[id]))
	{
		$obj_id = $_GET[id];
	}
	else
	{
		header("Location: ".$data[0][0]);
	}
	
	if (!empty($_GET[page]))
	{
		$page_back = $data[0][0].'?page='.$_GET[page];	
	}
	else
	{
		$page_back = $data[0][0];	
	}


	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$raw_data = $db->view_template($_GET[id]);
	

	$data[3][0] = table_data('Template Name',input_textbox('inp_tittle',$raw_data[0]));
	$data[3][1] = table_data('Template Detail',input_memobox('inp_detail',$raw_data[1],'id="tidybox"'));	
	$data[3][2] = open_tinymce($base_url);
	
	$data[2] = breadcamp( array('Home','Group Setting',$raw_data[0]),array('index.php',$sys_name.'.php') ); //array name, array link

	$data[4] 	= array($obj_id,'update');
	$data[5]    = $page_back;


	$themes = new c_themes("backend_input","../content/themes_backend/");
	$themes->pbody($data);
}
else
{
	header("Location: login.php");
}
?>