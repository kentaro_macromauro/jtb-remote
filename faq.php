<? 
	$code = $_REQUEST[code];
	
	switch ($code){
		case 'faq-sg' :  $code = 'sg'; break;	
		case 'faq-th'  :  $code = 'th'; break;
		case 'faq-tw' :  $code = 'tw'; break;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>よくある質問</title>
<link type="text/css" href="common/reset.css" rel="stylesheet" media="all" />
<link type="text/css" href="common/raku.css" rel="stylesheet" media="all" />
<style type="text/css">
.faq{padding:5px 0px 0px;}
.faq-q{background:url(images/lookjtb/faqq.jpg) left top no-repeat; padding-left:25px; line-height:22px; color:#C40000; font-weight:bold; cursor:pointer; padding-bottom:3px;}
.faq-a{background:url(images/lookjtb/faqa.jpg) left top no-repeat; padding-left:25px; line-height:22px; display:none;  }
.height10{height:10px; margin:0px; padding:0px; line-height:10px; font-size:10px;}
.border-bottom{border-bottom:#e7e7e7 solid 1px;}
</style>
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(
			function(){
					$('.faq-q').toggle(
						 function(){
							$(this).parent().find('.faq-a').show();	
						},
						function(){
							$(this).parent().find('.faq-a').hide();	
						});
		});
</script>

</head>
<body>

<!--conent-->
<div class="content">
	<!--header-->
	<div class="header">
		<h1 class="txtHeaderTop">よくある質問</h1>
		<a href="#" class="logoTop"><img src="images/lookjtb/logo.jpg" alt="ルックJTB×Mybusアジア・パシフィック"  /></a>
	</div>
	<!--header-->
	
	
	<!--content -->
	<div class="txtContent">
		
		<!--content form step1-->
		<div class="breakLine"></div>		
		<!--contentbottom-->
		<div class="txtContentBottom border-bottom">
		
			<div class="txtContentNoteHeader">よくある質問</div>
			
			<div class="height10"></div>
			
			<? if ($code=='tw'){ ?>
			
			<!--taiwan-->
			<ul class="faq">
				<li class="faq-q">気候・服装</li>
				<li class="faq-a">台北では冬季(12～3月)は気温が下がるのでコートやジャケットが必要です。高雄、台南は平均気温22度前後比較的すごしやすいです。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">物価（お水1本あたり目安）</li>
				<li class="faq-a">ミネラルウォーター500ミリリットル20‐30元、コンビニで手軽に購入できます。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">日曜・祝日はお店はほとんど閉まっているのか？店舗の営業状況。</li>
				<li class="faq-a">旧正月・国慶節は休みになる店舗が多いですが、それ以外は営業しているのがほとんどです。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">オプショナルツアーの支払いをカードで払えるか？</li>
				<li class="faq-a">JTBラウンジにてカードでのお支払いを取り扱っております。※使用可能なカード：VISA,マスター,JCB,AMEX,ダイナース</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">レストランでのドレスコード</li>
				<li class="faq-a">特にございませんが、スリッパや短パンは避けたほうが無難です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">両替は現地でできますか？</li>
				<li class="faq-a">現地空港にてガイドがお迎えした後に両替所にご案内します。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">インターネット環境（WIFI）についての詳細</li>
				<li class="faq-a">ほとんどのホテルで利用可能ですが、有料のところが多く、無料WIFI利用時でも申請や暗証番号の取得が必要です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">禁煙喫煙ルームの有無について</li>
				<li class="faq-a">全ホテル全室禁煙となっております。レストラン等を含め室内すべて禁煙となります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">変圧器の貸し出しの有無（有料・無料も含めて）</li>
				<li class="faq-a">日本製の電化製品はそのまま使用可能ですが、携帯やコンピューター等の海外利用可能な充電器をお持ちいただくことをお勧めいたします。また長時間の仕様は破損、火災等思わぬ災害の原因となる可能性もあるので、ご注意ください。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">空港送迎時のホテルピックアップ時間の目安</li>
				<li class="faq-a">桃園空港利用の場合は4時間20分～3時間30分前、松山空港利用の場合は3時間～2時間前にお迎えに上がります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">移動日のオプショナルツアーの申し込み可否</li>
				<li class="faq-a">到着フライトのお時間により夕刻発のツアーのお申し込みも可能です、都度確認が必要となります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテル、レストランでチップは必要ですか？</li>
				<li class="faq-a">台湾では基本的にチップは不要です、ホテル従業員に荷物の上げ下ろしなどを依頼した場合は、NT$50程度を目安にチップを渡すのが一般的です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテルに歯ブラシはありますか？</li>
				<li class="faq-a">ほとんどのホテルで用意がありますが、一部無い場合もあります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">RAKUなびから予約は受け付けられますか?</li>
				<li class="faq-a">オプショナルツアー、レストラン等すべての予約手配は出来かねます。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">タクシー初乗り料金はいくらですか？</li>
				<li class="faq-a">初乗りはNT$70です、当デスクから101まではNT$220前後、故宮博物院まではNT$260前後となります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">外国人旅客に対する特定商品営業税還付について</li>
				<li class="faq-a">特定商品還付マーク(TRS)の貼られた商店で同一日NT$3,000以上の商品を購入した際に税金還付を受けることができます。詳しくは財政部のホームページアドレスを参照ください：<a href="http://www.ntbsa.gov.tw/etwch/img/Tax_rebate_file09JP.jpg" target="_blank">http://www.ntbsa.gov.tw/etwch/img/Tax_rebate_file09JP.jpg</a></li>
			</ul>
			<!--taiwan-->
			<? } ?>
			<? if ($code == 'sg'){ ?>
			<!--Singapore-->
			<ul class="faq">
				<li class="faq-q">気候・服装</li>
				<li class="faq-a">1年を通して高温多湿で蒸し暑いですが、シンガポールは雨季と乾季に分かれており、10～3月の雨季は雨が多く、4～9月の乾季は雨が少なく、空気も乾燥しています。 服装は日本の真夏の服装でお過ごしいただけますが、建物内の冷房が強いので、カーディガンなどの長袖の羽織物をご持参いただくことをおすすめします。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">物価（お水1本あたり目安）</li>
				<li class="faq-a">500mlの飲料水で1シンガポールドル前後です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">日曜・祝日はお店はほとんど閉まっているのか？店舗の営業状況。</li>
				<li class="faq-a">旧正月時期はレストランやスーパーなどが休業するお店もありますが、それ以外は営業しています。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">オプショナルツアーの支払いをカードで払えるか？</li>
				<li class="faq-a">JTBラウンジにてカードでのお支払いを取り扱っております。 ※使用可能なカード：VISA,マスター,JCB,AMEX,ダイナース</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">レストランでのドレスコード</li>
				<li class="faq-a">ホテル内レストランや高級レストランではスマートカジュアルをおすすめします。 サンダルやハーフパンツでの入場は断られる場合があります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">両替について</li>
				<li class="faq-a">空港内両替所、ホテルフロント、市内銀行（両替所）など両替が可能です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">インターネット環境（WIFI）についての詳細</li>
				<li class="faq-a">ホテルの客室やビジネスセンターにてインターネットを使用することができます。 有料と無料の場合のいずれの場合もあります。 市内のDFS館内や、JTBラウンジではWIFIが無料で接続可能です。無料ＷＩＦＩ利用時でも申請や暗証番号の取得が必要です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">禁煙喫煙ルームの有無について</li>
				<li class="faq-a">ほとんどのホテルで禁煙喫煙ルームがありますが、ホテルによっては喫煙ルームがない場合もあります。 喫煙ルームのお部屋数が少なく、混雑時などご希望に添えない場合もあります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">変圧器の貸し出しの有無（有料・無料も含めて）</li>
				<li class="faq-a">各ホテルにて無料での貸し出しサービスがありますが、数に限りがあります。また、事前予約はできません。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">空港送迎時のホテルピックアップ時間の目安</li>
				<li class="faq-a">通常ご出発便の約2時間30分前となります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">移動日のオプショナルツアーの申し込み可否</li>
				<li class="faq-a">可能ですが、コースによってはお受けできない場合があります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテル、レストランでチップは必要ですか？</li>
				<li class="faq-a">基本的には不要です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテルに歯ブラシはありますか？</li>
				<li class="faq-a">ほとんどのホテルで用意があります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">タクシー所要時間と料金</li>
				<li class="faq-a">タクシー料金は、空港から市内は、S＄１５-２０で、マリーナ地区や、オーチャード界隈から各観光スポットへは、S＄１０～S＄２０程になりますが、郊外の動物園、ナイトサファリは、S＄３０程です。また朝昼晩の時間帯のより追加料金が発生する場合が御座います。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">お土産（お菓子類）</li>
				<li class="faq-a">各デパート、ショッピングセンターのスーパーマーケットにもございますが、JTBラウンジにもご宿泊ホテルまでお届けするお土産物の販売をさせて頂いております。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">朝食時に請求書にサインをしましたが、追加料金はありますか。</li>
				<li class="faq-a">ご朝食付きのプランをお申し込みの場合、ご朝食時にサインをレストランから求められる場合が御座いますが、お客様が朝食をお取りになったかの確認とご利用人数の確認ですので、追加料金は、発生致しません。</li>
			</ul>
			<!--Singapore-->
			<? } ?>
			<? if ($code == 'th'){ ?>
			<!--Thailand-->
			<ul class="faq">
				<li class="faq-q">気候・服装</li>
				<li class="faq-a">雨季（5月中旬～10月）、乾季（10月～2月）、暑期（2月中旬～5月）に別れ、一般的に乾季がベストシーズンと言われます。雨季でも1日中雨が降り続くわけではなく、気温も暑期よりは下がるので、比較的過ごしやすくなります。乾季は爽やかな晴天が続きます。暑期は湿度も上がり、朝～晩まで気温が下がらず、厳しい暑さが続きます。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">物価（お水1本あたり目安）</li>
				<li class="faq-a">500mlの飲料水でで10～30タイバーツほどです。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">日曜・祝日はお店はほとんど閉まっているのか？店舗の営業状況。</li>
				<li class="faq-a">タイ正月であるソンクラン（例年4月中旬）以外はほとんどが年中無休で営業しています。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">オプショナルツアーの支払いをカードで払えるか？</li>
				<li class="faq-a">市内店舗（エンポリウム店、シーロムコンプレックス店）へお越しいただければご使用になれます。※使用可能なカード：VISA,マスター,JCB,AMEX,ダイナース</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">レストランでのドレスコード</li>
				<li class="faq-a">欧米に比べるとそれほど厳しくありませんが、ホテルのレストランなど高級店ではスマートカジュアルをおすすめします。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">両替について</li>
				<li class="faq-a">空港内両替所、ホテルフロント、市内銀行（両替所）など両替（日本円からタイバーツ）が可能です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">インターネット環境（WIFI）についての詳細</li>
				<li class="faq-a">ホテルの客室やビジネスセンターにてインターネットを使用することができます。有料と無料の場合のいずれの場合もあります。無料ＷＩＦＩ利用時でも申請や暗証番号の取得が必要です。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">禁煙喫煙ルームの有無について</li>
				<li class="faq-a">ほとんどのホテルで禁煙喫煙ルームがあります。混みあう時期はご希望通りのお部屋タイプにならないことが多いです。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">変圧器の貸し出しの有無（有料・無料も含めて）</li>
				<li class="faq-a">多くのホテルにて無料での貸し出しサービスがあります。事前予約はできません。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">空港送迎時のホテルピックアップ時間の目安</li>
				<li class="faq-a">通常ご出発便の約3時間～4時間前となります。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">移動日のオプショナルツアーの申し込み可否</li>
				<li class="faq-a">交通渋滞が激しいため、原則的に移動日のオプショナルツアー（1日観光）お申込みは受け付けておりません。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテル、レストランでチップは必要ですか？</li>
				<li class="faq-a">ホテルではベルボーイやベッドメイキングに20タイバーツほど、スパやタイ古式マッサージでは100バーツほどのチップを支払う習慣があります。ホテル内レストランでは飲食代の10％程度が加算される事がほとんどです。</li>
			</ul>
			<ul class="faq">
				<li class="faq-q">ホテルに歯ブラシはありますか？</li>
				<li class="faq-a">事前に準備されているホテルもありますが、ない場合もリクエストいただければほとんどのホテルで無料で準備してくれます。</li>
			</ul>		
			<!--Thailand-->	
			<? } ?>
			<div class="height10"></div>
		</div>
		<!--contentbottom-->
		
	</div>
	<!--content -->
	
</div>
	
	
<!--footer-->
<div class="footer">
	<div class="footerContent">
		<p>Copyright © 2013 MyBus-Asia.com All rights reserved</p>
	</div>
</div>
<!--footer-->
</body>
</html>