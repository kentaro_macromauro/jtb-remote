<? 
$path = "../";
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Souvenir AXES Report</title>
<style type="text/css">
.table_result{background:#333;}
.table_result th{text-align:left; }
.table_result th.head{text-align:center; background:#CCC;}
.table_result th,.table_result td{background:#FFF;}
.line{border-bottom:#999 solid 1px; padding-top:10px;}
.height10{10px;}

.txtCenter{text-align:center;}
</style>
</head>
<? 	

function decodehtml($in){
	
	$txt = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($in));
	$txt = html_entity_decode($txt,null,'UTF-8');
	
	return $txt;
}

	$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
	$db->db_connect();
	
	$sql = 'SELECT id ,data,upd_date  FROM mbus_check_result_souvenir ORDER BY id DESC ';
	
	$result = $db->db_query($sql);
	
	$key  = array();
	$data = array();
	
	
	$tb = array('optional','order_code','currency','email','amount','user_id','telephone_no','result');
	
	
	$buffer = '<div>
		<p><strong class="txtCenter">Souvenir AXES Report</strong></p>
	
       	<table class="table_result" cellspacing="1" >';
        	
	$buffer .= '<tr>';		
			
	foreach ($tb as $th){
		$buffer .= '<th class="head">'.$th.'</th>';	
	}
	
	$buffer .= '</tr>';
	
	
	while ($record = mysql_fetch_array($result)){
		
		$id = $record[id];		
		$data = $record[data];
				
		$raw = jd_decode($data);
		
		$th = array();
		$td = array();
		
		preg_match_all("#<th>(.*?)</th>#",$raw,$th);
		preg_match_all("#<td>(.*?)</td>#",$raw,$td);
		
		
		
		$b = '';
		$e = 0;
		for ($i = 0; $i < count($th[1]) ;$i++){
			
		
			$td[0][$i] = decodehtml($td[0][$i]);
			
			
			switch ($th[1][$i]){
				case 'optional' :  $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'order_code' : $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'currency' :$e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'email' : $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'amount' : $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'user_id' :$e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'telephone_no' : $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;
				case 'result' : $e++;
					$b .= $td[0][$i]?$td[0][$i]:'<td>&nbsp;</td>';
				break;	
			}
		
		}
		
		if ($e == count($tb))
			
		$buffer .= '<tr>'.$b.'</tr>';
			
		
		
	}	
	
	$buffer .= '</table></div><div class="height10"></div><div class="line"></div>';
	
?>
<body>
<?=$buffer;?>
</body>
</html>