<?

function resize_image($filename,$newwidth,$newheight=0) 
{
	
	
	if (!file_exists($filename) || !in_array(preg_replace('/(.*)[.]([^.]+)/is','$2',basename($filename)),array('png','jpg','jpeg','gif','xbm','xpm','wbmp'))) 
	{
		return false;	
		
	} 
	else 
	{	
		$ext=preg_replace('/(.*)[.]([^.]+)/is','$2',basename($filename));
		if ($ext=='jpg') { $ext='jpeg'; }
		$ext='imagecreatefrom'.$ext;
		list($width, $height) = getimagesize($filename);
		
		if ($newheight == 0)	$newheight = floor(($newwidth/$width ) * $height); 
		
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = $ext($filename);
		
		if ($width <= $newwidth)
		{
			return $source;
		}
		else 
		{			
			 imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);	
			 return $thumb;
		}
	}
}

if (!empty( $_GET[root]) && !empty( $_GET[name]) && !empty( $_GET[width]))
{
	$arr_root = explode('-',$_GET[root]);
	$root = '';
	foreach ($arr_root as $now_root)
	{
		$root .= $now_root.'/';	
	}
		
	$name = $_GET[name];
	$width = $_GET[width];
	$height = $_GET[heigh];


	$img_name = $root.$_GET[name];
	
	
	
	header('Content-Type: image/jpeg');	
	
	
	if (!file_exists($img_name))
	{
		$img_name = '../images/img_notfound.jpg';	
	}
	
	imagejpeg(resize_image($img_name,$width),NULL,100);
	
	
}	
else
{
	header('Content-Type: image/jpeg');	
	$img_name = '../images/img_notfound.jpg';	
	
	if (!empty( $_GET[width]))
	{
		imagejpeg(resize_image($name  ,$_GET[width]),NULL,100);
	}
	else
	{
		imagejpeg(resize_image($img_name ,71),NULL,100);
	}
}
?>