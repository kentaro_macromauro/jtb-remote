<?
if ( $_SERVER["SERVER_PORT"] != 443 ){
//    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}
$path = '../';

require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');
require_once($path."class/phpmailer/class.phpmailer.php");

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

//!important!
$charge_info['type'] = $_POST['type'];
$charge_info['stripeToken'] = $_POST['stripeToken'];

$charge_info['country_iso3'] = $_POST['country_iso3'];
$country_iso3 = $charge_info['country_iso3'];

$charge_info['site_name'] = $_POST['site_name'];
$charge_info['site_code'] = $_POST['site_code'];
$charge_info['product_kind'] = $_POST['product_kind'];
$charge_info['order_id'] = $_POST['order_id'];
$charge_info['currency'] = $_POST['currency'];
$charge_info['amount'] = $_POST['amount'];

$charge_info['card_holder'] = $_POST['card_holder'];
$charge_info['tel_country_code'] = $_POST['tel_country_code'];
$charge_info['telephone_no'] = $_POST['telephone_no'];
$charge_info['email'] = $_POST['email'];

//important
$amount = fetch_stripe_amount($charge_info['currency'], $charge_info['amount']);

if(empty($charge_info['stripeToken'])){
    echo 'エラーが起きました。最初からやり直して下さい。';
    exit;
}
//branch info
$data = fetch_site_info($charge_info['country_iso3']);
$branch['company'] = $data['branch_name'];
$branch['optional']['time'] = $data['opt_time'];
$branch['optional']['tel'] = $data['opt_tel'];
$branch['optional']['email'] = $data['opt_email'];
$branch['souvenir']['company'] = $data['souv_company'];
$branch['souvenir']['time'] = $data['souv_time'];
$branch['souvenir']['tel'] = $data['souv_tel'];
$branch['souvenir']['email'] = $data['souv_email'];

require_once($path.'stripe-php/lib/Stripe.php');
//!important! when prod
Stripe::setApiKey(_STRIPE_TEST_SECRET_KEY_);
try {
    if($charge_info['product_kind'] == 'OPTIONAL_TOUR'){
        $sql = 'SELECT a.* ,b.* FROM '._DB_PREFIX_TABLE.'booking a
			LEFT JOIN '._DB_PREFIX_TABLE.'booking_detail b ON a.book_id = b.book_id
			WHERE a.book_id = "'.trim($charge_info['order_id']).'" LIMIT 0,1';
        $result = $db->db_query($sql);
        while ($record = mysql_fetch_array($result)){
            $product_code = $record['product_code'];
        }
    //SOUVENIR
    }else{
        $sql = 'SELECT a.* ,b.* FROM '._DB_PREFIX_TABLE.'souvenir_order a
			LEFT JOIN '._DB_PREFIX_TABLE.'souvenir_order_detail b ON a.order_id = b.order_id
			WHERE a.order_id = "'.trim($charge_info['order_id']).'" LIMIT 0,1';

        $result = $db->db_query($sql);
        while ($record = mysql_fetch_array($result)){
            $product_code = $record['souvenir_code'];
        }
    }
    $stripe_array = array(
        'source' => $charge_info['stripeToken'],
        'amount' => $amount,
        'currency' => $charge_info['currency'],
        'statement_descriptor' => 'JTB',
        'metadata' => array(
            'country_iso3' => $charge_info['country_iso3'],
            'product_type' => $charge_info['product_kind'],
            'order_id' => $charge_info['order_id'],
            'product_code' => $product_code,
            'site_name' => $charge_info['site_name'],
            'site_code' => $charge_info['site_code'],
            'name' => $charge_info['card_holder'],
            'email' => $charge_info['email'],
            'tel' => $charge_info['tel_country_code'].$charge_info['telephone_no']
        )
    );
    $charge = Stripe_Charge::create($stripe_array);
} catch (Stripe_Error $e) {
    $smarty = new Smarty;
    $config['documentroot'] = '../';
    $smarty->assign("config",$config);
    $smarty->assign("branch",$branch);
    $smarty->assign("country_iso3",$country_iso3);
    $smarty->display('custom_payment_form_fail.tpl');
    exit;
}
//メール送信
date_default_timezone_set('Asia/Tokyo');
$now = date('Y年m月d日 H時i分s秒');

$mail_tempage = '
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
決済確認メール
To: ('.$charge_info['email'].')
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
この度は、'.$charge_info['site_name'].'をご利用ありがとうございます。
今回のご利用分につきましては、お客様のカードご利用明細に「JTB」と表示されます。

お支払い内容
━━━━━━━━━━━━━━━━━━━━━
サイト名：My Bus
決済ID：'.$charge->id.'
ご購入日時：'.$now.'
ご購入金額：'.$charge_info['currency'].' '.$charge_info['amount'].'
━━━━━━━━━━━━━━━━━━━━━

ご購入商品・サービスに関するお問合せは、ご利用のサイトへお問合せ下さい。
　【オプショナルツアーのお問合せ】
　E-mail : '.$branch['optional']['email'].'
　TEL : '.$branch['optional']['tel'];
if($country_iso3 == 'SGP' || $country_iso3 == 'IDN' || $country_iso3 == 'THA' || $country_iso3 == 'TWN'){
    $mail_tempage .= '
　【おみやげのお問合せ】
　'.$branch['souvenir']['company'].'
　'.$branch['souvenir']['time'].'
　E-mail : '.$branch['souvenir']['email'].'
　TEL : '.$branch['souvenir']['tel'];
}
$mail_tempage .= '
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
決済サービス提供元 「Stripe」';

$html = nl2br($mail_tempage);

$mail = new PHPMailer();
$mail->AddAddress($charge_info['email']);
//Optional
if($charge_info['product_kind'] == 'OPTIONAL_TOUR'){
    $jtb_email = opt_email_by_country($country_iso3);
}else{
    //Souvenir
    $jtb_email = email_by_country($country_iso3);
}
$mail->AddAddress($jtb_email);

$mail->CharSet = 'UTF-8';
$mail->Subject =  "JTB MyBus 決済完了のお知らせ";

$mail->MsgHTML('<html><body><div style="width:600px; display:block;">'.$html.'</div></body></html>');
$mail->SetFrom($jtb_email);

if (!$mail->Send()){
    $result = $mail->ErrorInfo;
}

$payment = $db->paymentcode($charge_info['country_iso3']);
//データベース処理
if($charge->status == 'succeeded'){
    $charge_result = 'success';
}else{
    $charge_result = 'failure';
}

if($charge_info['product_kind'] == 'OPTIONAL_TOUR'){

    $db_field     = array('order_code','site_code','currency','amount','telephone_no','email','result','user_id','type','optional','update_date');
    $db_data      = array($charge_info['order_id'] ,$payment['sitecode'] ,$charge_info['currency'] ,$charge_info['amount'] ,$charge_info['tel_country_code'].$charge_info['telephone_no'] ,$charge_info['email'] ,$charge_result ,$charge_info['order_id'] ,'',$charge->id ,'datetime');
    $db->set_insert('mbus_payment_result',$db_field,$db_data);

    $sql = 'SELECT COUNT(*) dd_count FROM mbus_booking WHERE paid_type = "3" AND status_cms = "1" AND book_id = "'.trim($charge_info['order_id']).'" ';
    $result = $db->db_query($sql);

    $dd_count = 0;

    while ($record = mysql_fetch_array($result))
    {
        $dd_count = $record[dd_count];
    }
    if ($dd_count > 0)
    {
        $sql = 'UPDATE mbus_booking SET status_cms_paid = 2 WHERE book_id = "'.trim($charge_info['order_id']).'" ';
        $db->db_query($sql);
    }
    //SOUVENIR
}else{
    $db_field     = array('order_code','site_code','currency','amount','telephone_no','email','result','user_id','type','optional','update_date');
    $db_data      = array($charge_info['order_id'] ,$payment['sitecode'] ,$charge_info['currency'] ,$charge_info['amount'] ,$charge_info['tel_country_code'].$charge_info['telephone_no'] ,$charge_info['email'] ,$charge_result ,$charge_info['order_id'] ,'',$charge->id ,'datetime');
    $db->set_insert('mbus_payment_result_souvenir',$db_field,$db_data);
}

$smarty = new Smarty;
$config[documentroot] = '../';
$smarty->assign("config",$config);
$smarty->assign("branch",$branch);
$smarty->assign("country_iso3",$country_iso3);
$smarty->display('custom_payment_form_thank.tpl');
?>
