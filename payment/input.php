<?
if ( $_SERVER["SERVER_PORT"] != 443 ){
//    Header("Location: https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}
$path = '../';

require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();

$country_iso3 = $_GET['countryCode'];

if($country_iso3){
    $sql = 'SELECT count(*) dd_count FROM '._DB_PREFIX_TABLE.'country WHERE country_iso3 = UPPER("'.$country_iso3.'")';
    $result = $db->db_query($sql);

    while ($record = mysql_fetch_array($result)){
        $country_exist = $record['dd_count'];
    }
    if($country_exist < 1){
        echo 'URLが正しくありません。';
        exit;
    }
}else{
    echo 'URLが正しくありません。';
    exit;
}
//for currency
$payment = $db->paymentcode($country_iso3);
//Get website information
$data = fetch_site_info($country_iso3);
$branch['company'] = $data['branch_name'];
$branch['optional']['time'] = $data['opt_time'];
$branch['optional']['tel'] = $data['opt_tel'];
$branch['optional']['email'] = $data['opt_email'];
$branch['souvenir']['company'] = $data['souv_company'];
$branch['souvenir']['time'] = $data['souv_time'];
$branch['souvenir']['tel'] = $data['souv_tel'];
$branch['souvenir']['email'] = $data['souv_email'];
$branch['site_code'] = $data['site_code'];

$type = $_POST['type'];
if($type == 'confirm'){

    $smarty = new Smarty;
    $config[documentroot] = '../';
    $smarty->assign("config",$config);
    $smarty->assign("form_data",$_POST);
    $smarty->assign("branch",$branch);
    $smarty->assign("country_iso3",$country_iso3);
    $smarty->display('custom_payment_form_confirm.tpl');
    exit;
}

$smarty = new Smarty;
$config[documentroot] = '../';
$smarty->assign("config",$config);
$smarty->assign("branch",$branch);
$smarty->assign("country_iso3",$country_iso3);
$smarty->assign("currency",$payment['code']);
$smarty->assign("stripe_publish_key",_STRIPE_TEST_PUBLISH_KEY_);

$smarty->display('custom_payment_form.tpl');
?>