<? 
function set_cookie($code,$name)
{
	setcookie('_COOKIE_TYPE_CODE_',$code,time()+_LOGIN_TIME_);
	setcookie('_COOKIE_NAME_',$name,time()+_LOGIN_TIME_);
		
	define('_USER_TYPE_CODE_',$_COOKIE[_COOKIE_TYPE_CODE_]);
	define('_USER_NAME_',$_COOKIE[_COOKIE_NAME_]);
}
	
function unset_cookie($code,$name)
{
	setcookie('_COOKIE_TYPE_CODE_',$code, time()-(60*60*24*30*12));
	setcookie('_COOKIE_NAME_', $name, time()-(60*60*24*30*12));
		
	define('_USER_TYPE_CODE_',$_COOKIE[_COOKIE_TYPE_CODE_]);
	define('_USER_NAME_',$_COOKIE[_COOKIE_NAME_]);
}
?>