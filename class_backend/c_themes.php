<?
class c_themes
{
	protected $_path,$_root;
	
	public function __construct($_path,$root='content/themes_frontend/')
	{
		$this->_path = $_path;
		$this->_root = $root;
	}
	
	public function pbody($data = NULL)
	{		
	
		//echo "==>>".$this->_path; exit;
		switch ($this->_path)
		{					
			
			case "backend_login":include($this->_root."login.tpl"); break;
			case "backend_index":include($this->_root."index.tpl"); break;
			case "backend_table_approve":include($this->_root."tb_data_approve.tpl"); break;
			case "backend_table":include($this->_root."tb_data.tpl");  break;
			case "backend_input":include($this->_root."tb_input.tpl"); break;	
			
			//Tong =======================================
			case "backend_order_info_list":include($this->_root."tb_order_info_list.tpl");  break;			
			case "backend_order_country_list":include($this->_root."tb_order_country_list.tpl");  break;			
			case "backend_order_detail":include($this->_root."tb_order_detail.tpl");  break;			
			case "backend_order_mail":include($this->_root."tb_order_mail.tpl");  break;			
			case "backend_order_edit":include($this->_root."tb_order_edit.tpl");  break;			
			case "backend_order_edit_complete":include($this->_root."tb_order_edit_complete.tpl");  break;			
			case "backend_update_status":include($this->_root."tb_update_status.tpl");  break;			
			case "backend_error":include($this->_root."tb_error.tpl");  break;			
			
			case "backend_cus_info_list":include($this->_root."tb_cus_info_list.tpl");  break;
			case "backend_cus_country_list":include($this->_root."tb_cus_country_list.tpl");  break;						
			case "backend_cus_detail":include($this->_root."tb_cus_detail.tpl");  break;			
			case "backend_cus_delete_check":include($this->_root."tb_cus_delete_check.tpl");  break;			
			case "backend_cus_delete":include($this->_root."tb_cus_delete.tpl");  break;			
			case "backend_cus_edit":include($this->_root."tb_cus_edit.tpl");  break;
			case "backend_cus_edit_complete":include($this->_root."tb_cus_edit_complete.tpl");  break;			
			case "backend_cus_mail":include($this->_root."tb_cus_mail.tpl");  break;				
			case "backend_cus_mail_edit":include($this->_root."tb_cus_mail_edit.tpl");  break;				
			case "backend_admin_pow":include($this->_root."tb_admin_pow.tpl");  break;							
			case "backend_admin_insert":include($this->_root."tb_admin_insert.tpl");  break;
			case "backend_admin_edit":include($this->_root."tb_admin_edit.tpl");  break;									

			case "backend_report_booking":include($this->_root."tb_report_booking.tpl");  break;
			case "backend_axes_booking":include($this->_root."tb_axes_booking.tpl");  break;
			case "backend_axes_booking_c01":include($this->_root."tb_axes_booking1.tpl");  break;
			
			case "backend_cus_complete_vouchar":include($this->_root."tb_cus_complete_vouchar.tpl");  break;			
			
			
			case "backend_permis":include($this->_root."tb_admin_permis.tpl");  break;			
			case "backend_permis_input":include($this->_root."tb_admin_permis_input.tpl");  break;			
			//case "backend_permis_edit":include($this->_root."tb_permis.tpl");  break;			
			case "backend_insert_update_all":include($this->_root."tb_insert_update_all.tpl");  break;			//menu 3 level
			case "backend_insert_update_all2":include($this->_root."tb_insert_update_all2.tpl");  break;		//menu 2 level
			case "backend_cus_mail_edit_input":include($this->_root."tb_cus_mail_edit_input.tpl");  break;					
			case "backend_order_mail_edit":include($this->_root."tb_order_mail_edit.tpl");  break;				

			//============================================
		}
	}
		
}
?>