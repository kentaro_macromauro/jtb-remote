<?php
class auto_query extends c_db
{
	/* all unit */
	public function __construct($s_host,$s_name,$s_user,$s_pass = null)
	{
		parent::__construct($s_host,$s_name,$s_user,$s_pass);
	}
	/* all unit*/
	
	public function create_session()
	{
		$sql = 'INSERT INTO '._DB_PREFIX_TABLE.'session value(NULL); ';
		
		$insert_id = $db->db_query($sql);
		
		return $insert_id;
	}
	
	public function get_currency_rate()
	{
		$sql = 'SELECT rate FROM '._DB_PREFIX_TABLE.'currency WHERE text_type ="JA" ';
		

		$result = $this->db_query($sql);
		
		while ($rec = mysql_fetch_array($result))
		{
			$rate = $rec['rate'];
		}
		
		return $rate;
	}
		
	
	/* admin */
	public function admin_login($login,$pass)
	{
		/*
		$sql  = 'SELECT admin_id,admin_user,admin_type,pre_code,country_iso3,admin_level 
		FROM '._DB_PREFIX_TABLE.'administrators a,
				'._DB_PREFIX_TABLE.'permission b ';
		$sql .= 'WHERE admin_user = "'.mysql_real_escape_string($login).'" AND ';
		$sql .= 'admin_pass = SHA1("'.mysql_real_escape_string($pass).'") AND ' ; 
		$sql .= 'admin_type = pre_type ';
		$sql .= 'LIMIT 0,1';
		*/
		
		$sql  = 'SELECT admin_id,admin_user,admin_type,pre_code,admin_level 
		FROM '._DB_PREFIX_TABLE.'admin_backend a,
			 '._DB_PREFIX_TABLE.'permission b ';
		$sql .= 'WHERE admin_user = "'.mysql_real_escape_string($login).'" AND ';
		$sql .= 'admin_pass = SHA1("'.mysql_real_escape_string($pass).'") AND ' ; 
		$sql .= 'admin_type = pre_type ';
		$sql .= 'LIMIT 0,1';
						
		
		//echo $sql; exit;
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$pre_code 		= $record[pre_code];
			$admin_id 		= $record[admin_id];
			$admin_level 	= $record[admin_level];
			$level_name 	= $this->get_data("level_name_en","mbus_admin_level","level='".$admin_level."'");
			
		}
		
		//return array($pre_code,$admin_id,$country_iso3,$country_code,$admin_level);
		return array($pre_code,$admin_id,$admin_level,$level_name[0]);
	}
	
	/* admin */
	
	
	//Tong add ==================================================
	
	public function count_admin()
	{
		$sql = "SELECT COUNT(*) dd FROM "._DB_PREFIX_TABLE."admin_backend";
		
		//echo $sql; exit;
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
	}	
	
	public function fetch_admin($page_first,$page_num)
	{
		$sql ="SELECT admin_id,admin_user,admin_pass,admin_name,admin_type
					 ,admin_level,update_date,update_by 
				FROM "._DB_PREFIX_TABLE."admin_backend 
				WHERE admin_id !='0' ".$where." 
				ORDER BY admin_id desc 
				LIMIT ".$page_first.",".$page_num;		
				
		//echo $sql; //exit;
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record['admin_id'];
			$rec[1][$i] = $record['admin_user'];
			$rec[2][$i] = $record['admin_pass'];
			//$rec[2][$i] = sha1($record['admin_pass'];)
			$rec[3][$i] = $record['admin_name'];
			$rec[4][$i] = $record['admin_type'];
			//$rec[5][$i] = $record['admin_menu'];
			$rec[6][$i] = $record['admin_level'];
			//$rec[7][$i] = $record['country_iso3'];
			$rec[8][$i] = $record['update_date'];
			$rec[9][$i] = $record['update_by'];
			$i++;			
		
		}	
		return $rec;
	}
	
	public function count_permis()
	{
		$sql = "SELECT COUNT(*) dd FROM "._DB_PREFIX_TABLE."admin_level";
		
		//echo $sql; exit;
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
	}	
	
	public function fetch_permis($page_first,$page_num)
	{
		$sql ="SELECT * FROM "._DB_PREFIX_TABLE."admin_level 
				WHERE level_id !='0' ".$where." 
				ORDER BY level 
				LIMIT ".$page_first.",".$page_num;		
				
		//echo $sql; //exit;
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec['0'][$i] 		= $record['level_id'];
			$rec['1'][$i] 		= $record['level'];
			$rec['2'][$i] 		= $record['level_name'];
			$rec['3'][$i] 		= $record['level_name_en'];
			$rec['4'][$i] 		= $record['level_pow'];
			$rec['5'][$i] 		= $record['update_date'];
			$rec['6'][$i] 		= $record['update_by'];
			
			$i++;		
		}	
		return $rec;
	}	
	
	//===================
	
	public function tt_count_booking($where)
	{
		$sql = "SELECT COUNT(*) dd 
				FROM (SELECT * FROM "._DB_PREFIX_TABLE."booking) AS a
				LEFT JOIN "._DB_PREFIX_TABLE."booking_detail b ON b.book_id=a.book_id
				LEFT JOIN "._DB_PREFIX_TABLE."product p ON p.product_id=b.product_id 
				LEFT JOIN (SELECT * FROM "._DB_PREFIX_TABLE."payment_result WHERE result = 'success') AS x ON a.book_id = trim(x.order_code)  
				
				WHERE a.session_status ='1' ".$where;
		
		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];
		}
		
		return $dd;
	}		
	
	public function tt_fetch_booking_count($where)
	{
		$sql ="SELECT a.book_id,a.jtb_bookind_id,a.country_iso3,charge_type,
					IF (mailto_customer=1,'Pending',IF(mailto_customer=2,'Send',IF(mailto_customer=3,'Cancel',''))) mailto_customer
					,DATE(a.book_date) as book_date,a.book_date as book_date_time
					,a.reg_firstname,a.reg_lastname,a.reg_furi_firstname,a.reg_furi_lastname
					,a.transection_id,a.amount,a.qty_amount
					,a.paid_status,a.paid_type,a.allot,a.reg_arrgo,a.reg_arrgo_time
					,a.update_by,date(a.update_date) d_date,a.update_date as d_date_time
					,b.product_id,b.product_code,p.product_name_en,p.product_name_jp
					,a.book_type,a.status_cms,
					IF (a.book_id = TRIM(x.order_code), IF (TRIM(x.type)='Cancellation',3,2), a.status_cms_paid) status_cms_paid ,
					IF ( (( trim(reg_arrgo_place) <> '') and ( trim(reg_arrgo_time) <> '')) , 'Yes','No') place 
				FROM (SELECT * FROM "._DB_PREFIX_TABLE."booking ORDER BY book_id DESC) AS a
				LEFT JOIN "._DB_PREFIX_TABLE."booking_detail b ON b.book_id=a.book_id
				LEFT JOIN "._DB_PREFIX_TABLE."product p ON p.product_id=b.product_id 
				LEFT JOIN (SELECT * FROM "._DB_PREFIX_TABLE."payment_result WHERE result = 'success') AS x ON a.book_id = trim(x.order_code)  
				
				WHERE a.session_status ='1' ".$where;		
				
		$result = $this->db_query($sql);
		return mysql_num_rows($result);
	}
	
	public function tt_fetch_booking($page_first,$page_num,$where)
	{
		
		$sql ="SELECT a.book_id,a.jtb_bookind_id,a.country_iso3,charge_type,
					IF (mailto_customer=1,'Pending',IF(mailto_customer=2,'Send',IF(mailto_customer=3,'Cancel',''))) mailto_customer
					,DATE(a.book_date) as book_date,a.book_date as book_date_time
					,a.reg_firstname,a.reg_lastname,a.reg_furi_firstname,a.reg_furi_lastname
					,a.transection_id,a.amount,a.qty_amount
					,a.paid_status,a.paid_type,a.allot,a.reg_arrgo,a.reg_arrgo_time
					,a.update_by,date(a.update_date) d_date,a.update_date as d_date_time
					,b.product_id,b.product_code,p.product_name_en,p.product_name_jp
					,a.book_type,a.status_cms,
					IF (a.book_id = TRIM(x.order_code), IF (TRIM(x.type)='Cancellation',3,2), a.status_cms_paid) status_cms_paid ,
					IF ( (( trim(reg_arrgo_place) <> '') and ( trim(reg_arrgo_time) <> '')) , 'Yes','No') place 
				FROM (SELECT * FROM "._DB_PREFIX_TABLE."booking ORDER BY book_id DESC) AS a
				LEFT JOIN "._DB_PREFIX_TABLE."booking_detail b ON b.book_id=a.book_id
				LEFT JOIN "._DB_PREFIX_TABLE."product p ON p.product_id=b.product_id 
				LEFT JOIN (SELECT * FROM "._DB_PREFIX_TABLE."payment_result WHERE result = 'success') AS x ON a.book_id = trim(x.order_code)  
				
				WHERE a.session_status ='1' ".$where." 

				LIMIT ".$page_first.",".$page_num;		
				
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($record = mysql_fetch_array($result))
		{

			$rec['book_id'][$i] 		= $record['book_id'];
			$rec['jtb_bookind_id'][$i] 	= $record['jtb_bookind_id'];
			$rec['customer_name'][$i] 	= $record['customer_name'];
			$rec['paid_status'][$i] 	= $record['paid_status'];
			$rec['book_date'][$i] 		= $record['book_date'];
			$rec['reg_firstname'][$i] 	= $record['reg_lastname'].' '.$record['reg_firstname'];
			$rec['reg_furi_firstname'][$i] = $record['reg_furi_lastname'].' '.$record['reg_furi_firstname'];			
			$rec['product_id'][$i] 		= $record['product_id'];
			$rec['product_code'][$i] 	= $record['product_code'];			
			$rec['product_name_jp'][$i] = $record['product_name_jp'];		
			$rec['d_date'][$i] 			= $record['d_date'];				//10
			$rec['update_by'][$i] 		= $record['update_by'];			
			$rec['allot'][$i] 			= $record['allot'];
			$rec['amount'][$i] 			= $record['amount'];
			$rec['qty_amount'][$i] 		= $record['qty_amount'];
			$rec['paid_type'][$i] 		= $record['paid_type'];
			$rec['country_iso3'][$i] 	= $record['country_iso3'];			
			$rec['reg_arrgo'][$i] 		= $record['reg_arrgo'];
			$rec['reg_arrgo_time'][$i] 	= $record['reg_arrgo_time'];
			$rec['book_type'][$i] 		= $record['book_type'];
			$rec['status_cms'][$i] 		= $record['status_cms'];
			$rec['status_cms_paid'][$i] 		= $record['status_cms_paid'];			
			$rec['book_date_time'][$i] 		= $record['book_date_time'];			
			$rec['d_date_time'][$i] 		= $record['d_date_time'];	
			$rec['place'][$i]				= $record['place'];
			$rec['charge_type'][$i]			= $record['charge_type'];
			
			$rec['mailto_customer'][$i]   = $record[mailto_customer];
			
			/*if ($record[mailto_customer] == 1)
			{
				$rec['mailto_customer'][$i] = "YES";	
			}
			else
			{
				$rec['mailto_customer'][$i] = "NO";
			}*/
			
			$i++;
			
		}
		return $rec;
	}	
	
	public function tt_view_booking($book_id)
	{

		$sql ="SELECT a.book_id,a.country_iso3,DATE(a.book_date) as book_date
					,jtb_bookind_id,a.transection_id,a.amount,a.paid_status,a.customer_adults
					,a.customer_children,a.customer_name,a.customer_furigana,a.customer_sex,a.customer_email,a.customer_tel
					,a.customer_zipcode,a.customer_state,a.customer_city,a.customer_town,a.customer_address1
					,a.customer_address2,a.update_date,a.update_by 
					,a.c_comment,a.paid_amount,a.paid_type,a.c_staff,b.paid_name
					,date(a.paid_date)as paid_date,date(a.stamp_date)as stamp_date,date(a.deadline_date)as deadline_date
				FROM "._DB_PREFIX_TABLE."booking a
				LEFT JOIN mbus_paidstatus b on b.paid_id=a.paid_status
				WHERE a.book_id='".$book_id."'";		
		
		
		//echo $sql; exit;
		$result = $this->db_query($sql); $i=0;
		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0] = $rec[book_id];
			$record[1] = $rec[country_iso3];
			$record[2] = $rec[book_date];			
			$record[3] = $rec[amount];
			$record[4] = $rec[customer_adults];
			$record[5] = $rec[customer_children];
			$record[6] = $rec[customer_name];
			$record[7] = $rec[customer_sex];
			$record[8] = $rec[customer_email];
			$record[9] = $rec[customer_zipcode];
			$record[10] = $rec[customer_state];
			$record[11] = $rec[customer_city];
			$record[12] = $rec[customer_town];
			$record[13] = $rec[customer_address1];
			$record[14] = $rec[customer_address2];
			$record[15] = $rec[c_comment];
			$record[16] = $rec[paid_amount];
			$record[17] = $rec[paid_type];
			$record[18] = $rec[c_staff];
			$record[19] = $rec[paid_date];
			$record[20] = $rec[stamp_date];
			$record[21] = $rec[deadline_date];
			
			$record[22] = $rec[paid_status];
			$record[23] = $rec[paid_name];
			$record[24] = $rec[update_by];
			$record[25] = $rec[update_date];						
			$i++;	
		}		
		return $record;
	}
	
	public function tt_view_booking2($book_id)
	{

	$sql = "SELECT b.book_id,b.country_iso3,b.book_type,DATE(b.book_date) b_date,b.jtb_bookind_id,b.jtb_statuscode 
					,b.transection_id,b.currency ,b.paid_type,b.paid_status,b.jtb_statuscode,b.status_cms_paid 
					,b.qty_adults,b.qty_child,b.qty_infant,b.qty_amount 
					,b.reg_furi_firstname,b.reg_furi_lastname,b.reg_firstname,b.reg_lastname
					,b.reg_sex,b.reg_addrjp_zip1,b.reg_addrjp_zip2 
					,reg_addrjp_city,reg_addrjp_area,reg_addrjp_pref,reg_addrjp_building 
					,reg_tel1,reg_tel2,reg_tel3,reg_mobile1,reg_mobile2,reg_mobile3,reg_backup_addrovs1 
					,reg_backup_tel1,reg_backup_tel2,reg_backup_tel3,reg_birthday 
					,reg_hotel_id,reg_arrfrm,reg_arrto,reg_arrgo,reg_airno,reg_info,b.remark 
					,reg_hoteltype,reg_hotel_name,reg_hotel_tel,reg_hotel_addr1 
					,b.reg_email,d.product_id,b.reg_hotel_name,b.reg_arrgo,b.reg_arrgo_time,b.reg_airno 
					,d.amount_adult,d.amount_child,d.amount_infant,b.amount 
					,p.city_iso3,p.product_code,p.product_name_jp,p.product_name_en,b.remark 
					,b.reg_addrtype,b.reg_addrovs1,b.reg_addrovs2 ,b.reg_arrgo_place,b.tour_id
					,DATE(b.update_date) AS create_date,b.update_date create_date_time ,b.update_by
					,charge_type,charge_desc
					,DATE(b.update_date_cms) AS update_date_cms ,b.update_by_cms ,b.status_cms,
					CONCAT( DATE_FORMAT(b.book_date,'%Y年%c月%e日 ') ,CASE DATE_FORMAT(NOW(),'%w') WHEN '0' THEN '(日)' 
					WHEN '1' THEN '(月)' 
					WHEN '2' THEN '(火)' 
					WHEN '3' THEN '(水)' 
					WHEN '4' THEN '(木)' 
					WHEN '5' THEN '(金)' 
					WHEN '6' THEN '(土)' END) bookdate,
					DATE_FORMAT(b.book_date,'%e %b %Y (%a)') bookdate_en , 
					DATE_FORMAT(reg_birthday,'%Y年%c月%e日') regbirthday ,
					TIME_FORMAT(reg_arrgo_time,'%H:%i') regarrgotime,
					DATE_FORMAT(reg_arrfrm,'%Y年%c月%e日')  regarrfrm,
					DATE_FORMAT(reg_arrto,'%Y年%c月%e日') regarrto,
					DATE_FORMAT(reg_arrgo,'%Y年%c月%e日') regarrgo 
				FROM mbus_booking b
				LEFT JOIN mbus_booking_detail d ON b.book_id=d.book_id
				LEFT JOIN mbus_product p ON p.product_id=d.product_id
				WHERE b.book_id='".$book_id."'";
		//echo $sql; //exit;
		$result = $this->db_query($sql); 
		$i=0;		
		
		//while ($rec = mysql_fetch_array($result)){
		$rec = mysql_fetch_array($result);
				
			$record['bookdate']        = $rec[bookdate];	
			$record['bookdate_en']	   = $rec[bookdate_en];
			$record['regarrgotime']    = $rec[regarrgotime];
			
			$record['regbirthday']     = $rec[regbirthday];
			$record['regarrfrm']	   = $rec[regarrfrm];
			$record['regarrto']        = $rec[regarrto];
			$record['regarrgo']        = $rec[regarrgo];
			
			$record['book_id'] 			= $rec[book_id];
			$record['b_date'] 			= $rec[b_date];					//date
			$record['country_iso3'] 	= $rec[country_iso3];
			$record['city_iso3'] 	= $rec[city_iso3];
			$record['create_date'] 		= $rec[create_date];
			$record['create_date_time'] = $rec[create_date_time];
			
			$record['amount'] 			= $rec[amount];
			$record['currency'] 			= $rec[currency];
			$record['book_type'] 			= $rec[book_type];
			$record['jtb_bookind_id'] 			= $rec[jtb_bookind_id];
			$record['jtb_statuscode'] 			= $rec[jtb_statuscode];
			$record['transection_id'] 			= $rec[transection_id];
			$record['qty_adults'] 			= $rec[qty_adults];
			$record['qty_child'] 			= $rec[qty_child];
			$record['qty_infant'] 			= $rec[qty_infant];
			$record['qty_amount'] 			= $rec[qty_amount];
			$record['status_cms_paid'] 			= $rec[status_cms_paid];
			
			$record['reg_furi_firstname'] 	= $rec[reg_furi_firstname];
			$record['reg_furi_lastname'] 	= $rec[reg_furi_lastname];
			$record['reg_firstname'] 	= $rec[reg_firstname];
			$record['reg_lastname'] 	= $rec[reg_lastname];
			$record['reg_sex'] 			= $rec[reg_sex];
			$record['reg_addrjp_zip1'] 			= $rec[reg_addrjp_zip1];
			$record['reg_addrjp_zip2'] 			= $rec[reg_addrjp_zip2];
			$record['reg_airno'] 			= $rec[reg_airno];
			$record['reg_addrtype'] 			= $rec[reg_addrtype];
			$record['reg_addrjp_city'] 			= $rec[reg_addrjp_city];
			$record['reg_addrjp_area'] 			= $rec[reg_addrjp_area];
			$record['reg_addrjp_pref'] 			= $rec[reg_addrjp_pref];
			$record['reg_addrjp_building'] 			= $rec[reg_addrjp_building];
			$record['reg_tel1'] 			= $rec[reg_tel1];
			$record['reg_tel2'] 			= $rec[reg_tel2];
			$record['reg_tel3'] 			= $rec[reg_tel3];
			$record['reg_mobile1'] 			= $rec[reg_mobile1];
			$record['reg_mobile2'] 			= $rec[reg_mobile2];
			$record['reg_mobile3'] 			= $rec[reg_mobile3];
			$record['reg_backup_addrovs1'] 			= $rec[reg_backup_addrovs1];
			$record['reg_backup_tel1'] 			= $rec[reg_backup_tel1];
			$record['reg_backup_tel2'] 			= $rec[reg_backup_tel2];
			$record['reg_backup_tel3'] 			= $rec[reg_backup_tel3];
			$record['reg_birthday'] 			= $rec[reg_birthday];
			$record['reg_hotel_id'] 			= $rec[reg_hotel_id];
			$record['reg_arrfrm'] 			= $rec[reg_arrfrm];
			$record['reg_arrto'] 			= $rec[reg_arrto];
			$record['reg_arrgo'] 			= $rec[reg_arrgo];
			$record['reg_airno'] 			= $rec[reg_airno];
			$record['reg_info'] 			= $rec[reg_info];
			$record['remark'] 			= $rec[remark];
			$record['reg_hoteltype'] 			= $rec[reg_hoteltype];
			$record['reg_hotel_name'] 			= $rec[reg_hotel_name];
			$record['reg_hotel_tel'] 			= $rec[reg_hotel_tel];
			$record['reg_hotel_addr1'] 			= $rec[reg_hotel_addr1];
			$record['reg_email'] 			= $rec[reg_email];
			$record['product_id'] 			= $rec[product_id];
			$record['reg_hotel_name'] 			= $rec[reg_hotel_name];
			$record['reg_arrgo_time'] 			= $rec[reg_arrgo_time];
			$record['paid_type'] 				= $rec[paid_type];
			
			$record['amount_adult'] 			= $rec[amount_adult];
			$record['amount_child'] 			= $rec[amount_child];
			$record['amount_infant'] 			= $rec[amount_infant];
			$record['product_code'] 			= $rec[product_code];
			$record['product_name_jp'] 			= $rec[product_name_jp];
			$record['product_name_en'] 			= $rec[product_name_en];
			$record['reg_arrgo_place'] 			= $rec[reg_arrgo_place];
			$record['reg_addrovs1'] 			= $rec[reg_addrovs1];
			$record['reg_addrovs2'] 			= $rec[reg_addrovs2];
			$record['tour_id'] 			= $rec[tour_id];


			$record['paid_status'] = $rec[paid_status];
			$record['paid_name'] = $rec[paid_name];
			$record['update_by_cms'] = $rec[update_by_cms];
			$record['update_date_cms'] = $rec[update_date_cms];	
			
			$record['charge_type'] = $rec[charge_type];
			$record['charge_desc']    = $rec[charge_desc];
			

			$i++;	
		//}		
		return $record;
	}
	
	public function tt_get_product()
	{
		$raw = array(  'rec'  => array( 'product_id' , 'product_name_en', 'product_name_jp' ), 
					   'field' => _DB_PREFIX_TABLE.'product');
		
		
		$result = $this->fetch_select($raw);
		$i = 0;
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record[$raw[rec][0]];
			$rec[1][$i] = $record[$raw[rec][1]];	
			$rec[2][$i] = $record[$raw[rec][2]];	
			
			$i++;
		}
		return $rec;	
	}	
	
	public function tt_get_paid_status()
	{
		$raw = array(  'rec'  => array( 'paid_id' , 'paid_name'), 
					   'field' => _DB_PREFIX_TABLE.'paidstatus');
				
		$result = $this->fetch_select($raw);
		$i = 0;
	
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record[$raw[rec][0]];
			$rec[1][$i] = $record[$raw[rec][1]];	
			
			$i++;
		}
		return $rec;	
	}	
	
	public function tt_view_paid_status($paid_id)
	{
		$sql =  'SELECT * FROM '._DB_PREFIX_TABLE.'paidstatus a 
				WHERE paid_id ="'.$paid_id.'"';
		
		
		$result = $this->db_query($sql); 
		//echo $sql; exit;
		
		$i=0;		
		while ($rec = mysql_fetch_array($result))
		{
			
			$record[0] = $rec[paid_id];
			$record[1] = $rec[paid_name];
			$record[2] = $rec[paid_detail];			
			$i++;	
		}		
		return $record;
	}	
	
	public function tt_count_level()
	{
		$sql = "SELECT COUNT(*) dd FROM "._DB_PREFIX_TABLE."admin_level a 
				where a.level_id !='0' ";
		
		//echo $sql; exit;
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$dd = $record[dd];	
		}
		
		return $dd;
	}		
	
	public function tt_fetch_level($page_first,$page_num,$where)
	{
		
		$sql ="SELECT a.level_id,a.level,a.level_name,a.level_name_en
				FROM "._DB_PREFIX_TABLE."admin_level a
				WHERE a.level_id !='0' ".$where." 
				ORDER BY a.level_id 
				LIMIT ".$page_first.",".$page_num;		
				
		//echo $sql; //exit;
		$result = $this->db_query($sql);
		$i = 0;
		
		while ($record = mysql_fetch_array($result))
		{
			$rec[0][$i] = $record['level_id'];
			$rec[1][$i] = $record['level'];
			$rec[2][$i] = $record['level_name'];
			$rec[3][$i] = $record['level_name_en'];

			$i++;
			
		}
		return $rec;
	}
	
	public function fatch_product($filter="")
	{		
		
		$sql = 'SELECT product_id,
				b.country_iso3,a.city_iso3,country_name_en,country_name_jp,city_name_en,city_name_jp,
				product_code,product_name_en,product_name_jp,
				price_min,price_max,
				a.update_date,
				rev_status,rev_date  
				FROM 
				'._DB_PREFIX_TABLE.'product a 
				LEFT JOIN 
				'._DB_PREFIX_TABLE.'city b ON a.city_iso3 = b.city_iso3 AND a.country_iso3 = b.country_iso3   
				LEFT JOIN
				'._DB_PREFIX_TABLE.'country c ON b.country_iso3 = c.country_iso3 ';
		
		//$sql .= $where.=;
				
		$sql .=	'ORDER BY product_code DESC,country_iso3,city_iso3,product_name_en,product_name_jp';
				
		//echo $sql;
		$result = $this->db_query($sql);
		$i = 0;
						
		while ($rec = mysql_fetch_array($result))
		{			
			$record['id'][$i] 			   = $rec[product_id];
			$record['country_iso3'][$i]    = $rec[country_iso3];
			$record['city_iso3'][$i] 	   = $rec[city_iso3];
			$record['country_name_en'][$i] = $rec[country_name_en];
			$record['country_name_jp'][$i] = $rec[country_name_jp];
			$record['city_name_en'][$i]    = $rec[city_name_en];
			$record['city_name_jp'][$i]    = $rec[city_name_jp];
			$record['product_code'][$i]    = $rec[product_code];
			$record['product_name_en'][$i] = $rec[product_name_en];
			$record['product_name_jp'][$i] = $rec[product_name_jp];
			$record['price_min'][$i]       = $rec[price_min];
			$record['price_max'][$i]	   = $rec[price_max];	
			$record['rev_status'][$i]	   = $rec[rev_status];
			$record['rev_date'][$i]		   = $rec[rev_date];

			$i++;
		}
		
		return $record;	
	}	
	
	
	public function get_data($field,$table,$where)
	{
				
		$sql = "SELECT ".$field." FROM ".$table." where ".$where." ";		
		str_replace(',','',$field,$count_replace);
		
		//echo $sql; exit;
		$result = $this->db_query($sql);		
		$record = mysql_fetch_array($result);
				
		for($i=0; $i<=$count_replace; $i++)
		{
			$c_return[$i] = $record[$i];
		}
		
		return $c_return;
	}		
	
	//for test query
	public function get_data_test($field,$table,$where)
	{
				
		$sql = "SELECT ".$field." FROM ".$table." where ".$where." ";		
		str_replace(',','',$field,$count_replace);
		
		//echo $sql; exit;
		$result = $this->db_query($sql);		
		$record = mysql_fetch_array($result);
				
		for($i=0; $i<=$count_replace; $i++)
		{
			//echo '/'.$i.'='.$record[$i];
			$c_return[$i] = $record[$i];
		}
		
		return $c_return;
	}	
	//End Tong add ==============================================
	
	
	
	public function view_pref($page_id)
	{
		$sql = 'SELECT name FROM '._DB_PREFIX_TABLE.'pref ';
		$sql .= 'WHERE id = '.$page_id;

		
		$result = $this->db_query($sql);
		
		while ($record = mysql_fetch_array($result))
		{
			$name = $record[name];	
		}
		
		return $name;
	}
	
	public function view_japan_city()
	{
		$sql = 'SELECT id,name FROM '._DB_PREFIX_TABLE.'pref ORDER BY rank';
		$result = $this->db_query($sql);
		
		$i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[$i] = array( 'id' => $rec['id'], 'name' =>  $rec['name']);			
			$i++;
		}
		
		return $record;		
	}		
	
	public function view_japan_city2()
	{
		$sql = 'SELECT id,name FROM '._DB_PREFIX_TABLE.'pref ORDER BY rank';
		$result = $this->db_query($sql);
		
		$i = 0;
		while ($rec = mysql_fetch_array($result))
		{
			$record[id][$i] = $rec['id'];			
			$record[name][$i] = $rec['name'];			
			$i++;
		}		
		
		return $record;	
	}	

}

?>