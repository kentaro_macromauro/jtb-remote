<?

if ( $_SERVER["SERVER_PORT"] == 443 ){
    Header("Location: http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] );
}

$path = '';
require_once($path."www_config/setting.php");
require_once($path."class/include/c_query.php");
require_once($path."class/c_query_sub.php");
require_once($path."class/c_common.php");
require_once($path.'webapp/libs/Smarty.class.php');

$db = new auto_query(_DB_SERVER_,_DB_NAME_,_DB_USER_,_DB_PASSWD_);
$db->db_connect();
$rate = $db->get_currency_rate();


$banner_link = 'images/banner_opt/';
require_once($path."include/config.php");

$breadcamp    = '<ul class="bread-camp"><li><a href="index.php">TOP</a><span>&gt;</span></li><li>ホテル</li></ul>';		


/*Search Box*/
$record = $db->get_country();
$inp_country = input_selectbox('inp_country',$record[1],$record[0],'','---Please Select---','search-left-selectbox w150 top') ;

$record = $db->get_souvenir_theme();

$inp_theme   = input_selectbox('inp_theme',$record[1],$record[0],'','---Please Select---','search-left-selectbox w150 top');


$record = $db->get_souvenir_location();

$inp_location = input_selectbox('inp_location',$record[1],$record[0],'','-----','search-left-selectbox w90 top');

/*Search Box*/

/* souvenir path setting   */
$img  = 'product/images/souvenir/';
$link = 'souvenir.php?souvenir_id=';
/* souvenir path setting   */


$entry_year = Date('Y');  $entry_month = Date('n');

if ($entry_month == '1')
{
	$prv_month = '12';
	$prv_year  = ($entry_year-1);
}
else
{
	$prv_month = $entry_month -1;
	$prv_year  = $entry_year;
}
/* product slider1 */


$product_slider1 = $db->front_show_souvenir_photosnap_slider('ALL');

$slider1 = '';


for ($i=0;$i<count($product_slider1['souvenir_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="'.$product_slider1['path'][$i].'/souvenir_product.php?souvenir_id='.$product_slider1['souvenir_id'][$i].'">';

	$imgcheck =  trim( 'product/images/souvenir/'.$product_slider1['souvenir_id'][$i].'-1.jpg' ) ;
	
	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media2" alt="" src="images/img_notfound.jpg" width="100"  />';
	}
	else
	{
		
		$product_img = 'product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $product_slider1['souvenir_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media2" alt="'.$product_img.'" src="'.$product_img.'" width="100"  />';
	}
	
	$product_text = $product_slider1['souvenir_name'][$i];
	
	$rate = $db->show_rate($product_slider1['country_iso3'][$i]);

				
	$tempgae .= '</a>
				<div class="slider-media-text">
				<p>'.$product_slider1[country_name_jp][$i].'</p>
				<p>
				<a href="'.$product_slider1['path'][$i].'/souvenir_product.php?souvenir_id='.$product_slider1['souvenir_id'][$i].'" title="'.$product_text.'"><strong>'.str_len($product_text,20).'</strong></a>
				<br/>
				'.str_len($product_slider1['short_desc'][$i],30).'
				</p>
				';
				
		
				
		if ($product_slider1['souvenir_price'][$i] > 0)
		{
			$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">';
				
			$tempgae .= $product_slider1['affiliate'][$i]?show_jp_price($product_slider1['souvenir_price'][$i]):
			show_location_price( $product_slider1['souvenir_price'][$i] , 
				array('sign' => $product_slider1[currency_sign][$i],'rate' => $product_slider1[currency_rate][$i], 'show' => $rate )	);
				
			$tempgae .=	'</span>
				</div>' ;
		
		}
		$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider1 .= $tempgae;
}

/* product slider1 */


/* product slider2 */

$product_slider2 = $db->front_show_souvenir_photosnap_slider2('ALL');
$slider2 = '';

for ($i=0;$i<count($product_slider2['souvenir_id']);$i++)
{
	$tempgae = '<li class="slider-list">
				<div class="slider-group">
				<a href="'.$product_slider2['path'][$i].'/souvenir_product.php?souvenir_id='.$product_slider2['souvenir_id'][$i].'">';			

	$imgcheck =  trim( 'product/images/souvenir/'.$product_slider2['souvenir_id'][$i].'-1.jpg' ) ;

	if (!file_exists($imgcheck) )
	{
		$tempgae .= '<img class="slider-media2" alt="" src="images/img_notfound.jpg" width="170" height="70" />';
		
	}
	else
	{
		$product_img = 'product/images/index.php?root=souvenir&amp;width=170&amp;name='.trim( $product_slider2['souvenir_id'][$i].'-1.jpg') ;
		$tempgae .= '<img class="slider-media2" alt="'.$product_img.'" src="'.$product_img.'" height="90" />';
	}
	
	$product_text =  $product_slider2['souvenir_name'][$i];
	
	$rate = $db->show_rate($product_slider2['country_iso3'][$i]);
		
	$tempgae .= '</a>
				<div class="slider-media-text">
				<p>'.$product_slider2[country_name_jp][$i].'</p>
				<p>
				<a href="'.$product_slider2['path'][$i].'/souvenir_product.php?souvenir_id='.$product_slider2['souvenir_id'][$i].'" title="'.$product_text.'"><strong>'.str_len($product_text,20).'</strong></a>
				<br/>
				'.str_len($product_slider2['short_desc'][$i],30).'
				</p>
				';
				
	if ( $product_slider2['souvenir_price'][$i]  > 0)
	{
		
		
		
		
		$tempgae .= '<div class="price">
				<span class="txt-red-bold-price">';
				
			$tempgae .= $product_slider2['affiliate'][$i]?show_jp_price($product_slider2['souvenir_price'][$i]):show_location_price( $product_slider2['souvenir_price'][$i] , 
				array('sign' => $product_slider2[currency_sign][$i],'rate' => $product_slider2[currency_rate][$i], 'show' => $rate )	);
				
			$tempgae .=	'</span>
				</div>' ;
		
		
	}
		$tempgae .= '
				</div>
				<div class="clear"></div>
				</div>
				</li>';
				
	$slider2 .= $tempgae;
}
/* product slider2 */

/* include tempage */

$site_country = 'ALL';
include("include/souvenir_right_menu.php");
$mybus_logo = '';

include("include/jtb_navigation.php");

/* include tempage */


/* banner list */
$arr_record = $db->show_promotion_souvenir( 'ALL' );



$data_select = explode(',',$arr_record);

$banner0 = $db->view_promotion_souvenir_country($data_select[0]);
$banner1 = $db->view_promotion_souvenir_country($data_select[1]);
$banner2 = $db->view_promotion_souvenir_country($data_select[2]);

$banner = array ( 'banner_id' => array( $data_select[0],  $data_select[1],  $data_select[2]) ,
			  	  'banner_name' => array( $banner0[scampaign_tittle], $banner1[scampaign_tittle] , $banner2[scampaign_tittle]),
			      'banner_country' => array( $banner0[path], $banner1[path] , $banner2[path] )
			    );


$smarty->assign("banner_link1",$banner[banner_country][0].'/souvenir_campaign.php?id='.$banner['banner_id'][0]);

if ( empty( $banner[banner_country][0] ))
{
	$banner[banner_country][0]  = 'public';
}

$smarty->assign("banner_img1", $banner[banner_country][0].'/images/souvenir_banner/'.$banner['banner_id'][0].'-1.jpg');
$smarty->assign("banner_name1",$banner[banner_name][0]);

$smarty->assign("banner_link2",$banner[banner_country][1].'/souvenir_campaign.php?id='.$banner['banner_id'][1]);

if ( empty( $banner[banner_country][1] ))
{
	$banner[banner_country][1]  = 'public';
}

$smarty->assign("banner_img2", $banner[banner_country][1].'/images/souvenir_banner/'.$banner['banner_id'][1].'-1.jpg');
$smarty->assign("banner_name2",$banner[banner_name][1]);

$smarty->assign("banner_link3",$banner[banner_country][2].'/souvenir_campaign.php?id='.$banner['banner_id'][2]);

if ( empty( $banner[banner_country][2] ))
{
	$banner[banner_country][2]  = 'public';
}

$smarty->assign("banner_img3", $banner[banner_country][2].'/images/souvenir_banner/'.$banner['banner_id'][2].'-1.jpg');
$smarty->assign("banner_name3",$banner[banner_name][2]);
/* banner list */

/* search box */
$smarty->assign("inp_country",$inp_country);
$smarty->assign("inp_location",$inp_location);
$smarty->assign("inp_theme",$inp_theme);

/* search box */

$smarty->assign("productslider1",$slider1);
$smarty->assign("productslider2",$slider2);
$smarty->template_dir = 'webapp/templates';
$smarty->compile_dir  = 'webapp/templates_c';


/* select tempage */
$smarty->display('jtb_souvenir.tpl');
/* select tempage */

/* set tempage */

?>
