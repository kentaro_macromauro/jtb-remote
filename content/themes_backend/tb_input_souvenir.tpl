<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Page</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="common/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="common/js/table_mainpage<? echo $data[7]; ?>.js"></script>
<script type="text/javascript">
_nowpage =   '<? echo $data[5]; ?>';
$(document).ready(
function()
{
<?=$data[6]; ?>
});
</script>
<script type="text/javascript" src="common/js/jd_datamanager/ajaxfileupload.js"></script>
<script type="text/javascript" src="common/js/jd_datamanager/jd_datamanager.js"></script>
</head>
<body>
<div class="header">
  <div class="sub_header">Administration
    <div class="logout">
      <p><a href="logout.php" class="btnlogout">Logout</a></p>
      <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="box1024 ptop10"><?=$data[2]; ?>
  <div class="control"><span class="logo"><?=$data[1]; ?></span>
    <div class="panel_btn2"> <span class="btn" id="btn_save"><b>Save</b></span> <span class="btn" id="btn_close" ><b>Close</b></span> </div>
  </div>
  <div class="data">
    <div class="table">
      <!--<?  /*f_loop($data[3]);*/ ?>-->
      <form action="<? echo $data[0][0]; ?>" <?=$data[0][1]; ?> name="frmInput" id="frmInput"  method="post">
        <input type="hidden" name="page_id" value="<?=$data[4][0]; ?>" />
        <input type="hidden" name="action" value="<?=$data[4][1]; ?>" />
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
          <tr>
            <th valign="middle" width="120" align="left" >Country<span class="msg-req">*</span></th>
            <td width="210" colspan="4"><? echo $data[3][inp_country]; ?></td>
            <th style="display:none;" valign="middle" align="left" >City</th>
            <td style="display:none;"><?=$data[3][inp_city]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Product Code<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_souvenir_code]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Product Type<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_souvenir_type]; ?></td>
          </tr>
         
           <tr>
          	<th valign="middle" align="left">Affiliate</th>
            <td colspan="3"><?=$data[3][affiliate]; ?></td>
          </tr>
          
          <tr>
          	<th valign="middle" align="left">Affiliate Code</th>
            <td colspan="3"><?=$data[3][affiliate_code]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Affiliate URL</th>
            <td colspan="3"><?=$data[3][affiliate_url]; ?></td>
          </tr>
          
                    
          <tr>
            <th valign="middle" align="left">Product Name<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_souvenir_name]; ?></td>
          </tr>
          <tr>
          	<th valign="top" align="left">Sort Description</th>
            <td colspan="3"><?=$data[3][inp_short_description]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Theme</th>
            <td colspan="3"><?=$data[3][inp_theme]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Paid Type<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_paid_type]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Price<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_price]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Allotment<span class="msg-req">*</span></th>
            <td colspan="3"><?=$data[3][inp_allotment]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Title Description</th>
            <td colspan="3"><?=$data[3][inp_title_description]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Table Description</th>
            <td colspan="3"><?=$data[3][inp_table_description]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Footer Description</th>
            <td colspan="3"><?=$data[3][inp_footer_description]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Publish</th>
            <td colspan="3"><?=$data[3][inp_publish]; ?></td>
          </tr>
        </table>
        <div class="height10"></div>
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
          <tr>
            <th width="120" align="left" valign="top">Remark</th>
            <td ><?=$data[3][inp_remark]; ?></td>
          </tr>
        </table>
        <?=$data[3][entrydate]; ?>
      </form>
      <div class="height10"></div>
      <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
        <tr>
          <th valign="middle" width="120" align="left">Pictures 1</th>
          <td><? echo $data[3][inp_upload_img][0]; ?><span class=""> Image (Width:768px Height:574px) </span></td>
        </tr>
        <tr>
          <th valign="middle" align="left">Pictures 2</th>
          <td ><? echo $data[3][inp_upload_img][1]; ?><span class=""> Image (Width:768px Height:574px) </span></td>
        </tr>
        <tr>
          <th valign="middle" align="left">Pictures 3</th>
          <td ><? echo $data[3][inp_upload_img][2]; ?><span class=""> Image (Width:768px Height:574px) </span></td>
        </tr>
      </table>
    </div>
    <div class="comment-req"><span class="msg-req">* Mandatory fields</span></div>
    <div class="h5"></div>
  </div>
  <div class="foter-control">
    <div class="panel_btn2"> <span class="btn" id="btn_close2" >Close</span> <span class="btn" id="btn_save2">Save</span> </div>
  </div>
  <div class="footer">&nbsp;</div>
</div>
</body>
</html>