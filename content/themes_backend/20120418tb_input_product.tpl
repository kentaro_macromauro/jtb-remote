<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Page</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="common/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="common/js/table_mainpage<? echo $data[7]; ?>.js"></script>
<script type="text/javascript">
_nowpage =   '<? echo $data[5]; ?>';
$(document).ready(
function()
{
<? echo $data[6]; ?>
});
</script>
<script type="text/javascript" src="common/js/jd_datamanager/ajaxfileupload.js"></script>


<script type="text/javascript" src="common/js/jd_datamanager/jd_datamanager.js"></script>


</head>

<body>
<div class="header">
  <div class="sub_header">Administration <div class="logout"><p><a href="logout.php" class="btnlogout">Logout</a></p> 
 <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p></div></div>
</div>
<div class="clear"></div>

  <div class="box1024 ptop10">
    <? echo $data[2]; ?>
    <div class="control"><span class="logo"><? echo $data[1]; ?></span>
      <div class="panel_btn2"> <span class="btn" id="btn_save"><b>Save</b></span> <span class="btn" id="btn_close" ><b>Close</b></span> </div>
    </div>
    
    <div class="data">
      <div class="table">
      	
      <!--<?  /*f_loop($data[3]);*/ ?>-->
      <form action="<? echo $data[0][0]; ?>" <? echo $data[0][1]; ?> name="frmInput" id="frmInput"  method="post">
      	<input type="hidden" name="page_id" value="<? echo $data[4][0]; ?>" />
      <input type="hidden" name="action" value="<? echo $data[4][1]; ?>" />
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
          <tr>
          	<th valign="middle" width="120" align="left" >Country</th>
            <td width="200"><? echo $data[3][0]; ?></td>
            <th valign="middle" align="left" >City</th>
            <td><? echo $data[3][1]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Product Id</th><td colspan="3"><? echo $data[3][2]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Product Type</th><td colspan="3"><? echo $data[3][3]; ?></td>
          </tr>
          
          <tr>
          	<th valign="middle" align="left">Product Name(EN)</th>
            <td colspan="3"><? echo $data[3][4]; ?></td>
          </tr>
          <tr>
            <th valign="middle" align="left">Product Name(JP)</th>
            <td colspan="3"><? echo $data[3][5]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Theme</th>
            <td colspan="3"><? echo $data[3][6]; ?></td>
          </tr>
          <tr>
            <th valign="top" align="left">Time</th>
            <td colspan="3"><? echo $data[3][7]; ?></td>
          </tr>
          
          <tr>
            <th valign="top" align="left">Option</th>
            <td colspan="3"><? echo $data[3]['option']; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Status</th>
			<td colspan="3"><? echo $data[3][8]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Min Price</th>
            <td colspan="3"><? echo $data[3][9]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Max Price</th>
            <td colspan="3"><? echo $data[3][10]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Minimum Pax</th>
            <td colspan="3"><? echo $data[3]['inp_min_pax']; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Maximum Pax</th>
            <td colspan="3"><? echo $data[3]['inp_max_pax']; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">No of days</th>
            <td colspan="3"><? echo $data[3]['inp_day_quantity']; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Cut off day</th>
            <td colspan="3"><? echo $data[3]['inp_day_cutoff']; ?></td>
          </tr>
         
          <!--<tr>
          	<th valign="top" align="left">Description(EN) </th>
            <td colspan="3"></td>
          </tr>-->
          
          
          <tr>
          	<th valign="top" align="left">Short Description(JP) <div style="display:none;"><? echo $data[3]['description_en']; ?></div></th>
            <td colspan="3"><? echo $data[3][11]; ?></td>
          </tr>
          
          
          <tr>
          	<th valign="top" align="left">Description(JP)</th>
            <td colspan="3"><? echo $data[3][12]; ?></td>
          </tr>
          
          
          
          <tr>
          	<th valign="top" align="left">Publish</th>
            <td colspan="3"><? echo $data[3][inp_public]; ?></td>
          </tr>

        </table>
        
  	
        <div class="height10"></div>
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
         <!--<tr>
         	<th valign="top" width="120" align="left">Policy</th>
            <td>
            	<span style="float:left;padding:3px;">Tittle</span>
                <span style="float:left;padding:3px; padding-left:75px;">Description(EN)</span>
                <div class="clear"></div>
            	<ul class="list-style1">
            	<? echo $data[3]['inp_policy']; ?>
            	</ul>
                <div class="add-more"><input type="button" value="Add" name="add_policy" /></div>
                <script type="text/javascript">
					$('input[name$="add_policy"]').click(
					function()
					{
						
						$(this).parent().parent().find('ul').append('<li><? echo $data[3]['inp_policy_code']; ?></li>');
					}
					); 
				</script>
            </td>
         </tr>

         <tr>
         	<th valign="top" width="120" align="left">Itinerary</th>
            <td>
            	<span style="float:left;padding:3px;">Day</span>
                <span style="float:left;padding:3px; padding-left:20px;">Time(EN)</span>
                <span style="float:left;padding:3px; padding-left:85px;">Description(EN)</span>
                <div class="clear"></div>
            	<ul class="list-style1">
            	<? echo $data[3]['inp_itinerary']; ?>
                </ul>
                <div class="add-more" style="padding-left:510px;"><input type="button" value="Add" name="inp_itinerary" /></div>
                <script type="text/javascript">
					$('input[name$="inp_itinerary"]').click(
					function()
					{
						$(this).parent().parent().find('ul').append('<li><? echo $data[3]['inp_itinerary_code']; ?></li>');
					}
					);
				</script>
            </td>
         </tr>-->
         <tr>
         	<th valign="top" width="120" align="left">Itinerary Template</th><td><? echo $data[3][13]; ?></td>
         </tr>
          <tr>
          	<th valign="top" align="left">Keyword Searching</th>
            <td ><? echo $data[3][14]; ?></td>
          </tr>
          
          <tr>
          	<th valign="top" align="left">Remark</th>
            <td ><? echo $data[3][15]; ?></td>
          </tr>
        </table>
        <? echo $data[3][26]; ?>
        </form>
        <div class="height10"></div>
         <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
         <tr>
         	<th valign="middle" width="120" align="left">Pictures 1</th>
            <td>
            	<? echo $data[3][16]; ?>  
            </td>
         </tr>
         <tr>
          	<th valign="middle" align="left">Pictures 2</th>
            <td ><? echo $data[3][17]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Pictures 3</th>
            <td ><? echo $data[3][18]; ?></td>
          </tr>
           <tr>
         	<th valign="middle" align="left">Pictures 4</th>
            <td><? echo $data[3][19]; ?></td>
         </tr>
         <tr>
          	<th valign="middle" align="left">Pictures 5</th>
            <td ><? echo $data[3][20]; ?></td>
         </tr>
         
         <tr>
         	<th valign="middle" align="left">Pictures 6</th>
            <td><? echo $data[3][21]; ?></td>
         </tr>
         <tr>
          	<th valign="middle" align="left">Pictures 7</th>
            <td ><? echo $data[3][22]; ?></td>
          </tr>
          <tr>
          	<th valign="middle" align="left">Pictures 8</th>
            <td ><? echo $data[3][23]; ?></td>
          </tr>
           <tr>
         	<th valign="middle" align="left">Pictures 9</th>
            <td><? echo $data[3][24]; ?></td>
         </tr>
         <tr>
          	<th valign="middle" align="left">Pictures 10</th>
            <td ><? echo $data[3][25]; ?></td>
         </tr>
        </table>
        
        
      </div>
      <div class="comment-req"><span class="msg-req">* Mandatory fields</span></div>
      <div class="h5"></div>
    </div>
    
    <div class="foter-control">
      <div class="panel_btn2"> <span class="btn" id="btn_close2" >Close</span> <span class="btn" id="btn_save2">Save</span>  </div>
    </div>
    <div class="footer">&nbsp;</div>
  </div>




<div class="jed_ddedit">
	<div class="box_533">

		<div id="loading"style="display:none;">loading</div>

			<ul class="img_pocket">
            </ul>
        </div>
        <div class="inport_link">
        <div>
           <table class="w533" border="0" align="left" cellpadding="0" cellspacing="1">
                <tr>

                	<th width="136" align="left" valign="top">Image Link : </th>

                    <td width="391" align="left" valign="top"><input type="text" name="boxpiclink" class="pic_name" value="" size="60" /></td>
                </tr>
                <tr>
                    <th align="left" valign="top">Image Description : </th>
                    <td align="left" valign="top"><input type="text" name="boxpicdesc" class="pic_desc" value="" size="60" /></td>
                </tr>
                <tr>

                    <td>&nbsp;</td>

                    <td align="left" valign="top">
                    	<input type="button" id="btn_insert" value="Insert" />&nbsp;<input type="button" id="btn_del" value="Delete" /><input type="hidden" id="obj_parent_name" value="" />
                    </td>
                </tr>
                <tr>
                    <th align="left" valign="top">
                 		Upload to Server : 
                    </th>

                    <td align="left" valign="top">

                    	<form name="form" action="" method="POST" enctype="multipart/form-data">
                        	<input id="fileToUpload" type="file" name="fileToUpload" >
                            <input  type="button" id="buttonUpload" onClick="return ajaxFileUpload();" value="Upload" />
                        </form>
                        <div class="box_close"></div>
                     </td>
                 </tr>

            </table> 
    	</div>
		
	</div>
    
</div>


</body>
</html>
