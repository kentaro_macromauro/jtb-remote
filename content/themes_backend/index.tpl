<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>JTB Administration : Index</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet"  />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
</head>
<body>
<div class="header" >
  <div class="sub_header">
    <div class="logout">
      <p><a href="logout.php" class="btnlogout">Logout</a></p>
      <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="box1024 ptop10">
  <div class="header1" >
    <!--<div class="control" style=" margin:auto; margin-bottom:10px; width:940px;"> 
    	<span class="logo">Info Biz THAILAND : Back-end</span></div>-->
    <div style="margin:auto; width:940px; border:#CCC solid 1px;">
      <div style="float:left; padding-right:50px; padding-top:10px;"><img src="../../admin/images/top_admin.jpg" width="271" height="326" /> </div>
      <div style="float:left;">
        <ul style="padding-top:10px;" class="admin_menulist">
          
          <!--
          <li><strong>Hotel</strong>
          	<ul class="admin_menulist1">
            	<li><a href="hotel.php">Hotel</a></li>
              	<li><a href="photosnap_hotel.php">Hotel Slide bar</a></li>
              	<li><a href="hotel_theme.php">Hotel Theme</a></li>
            </ul>
          </ul>
          -->
          <li style="padding-top:20px;"><strong>Souvenir</strong>
          	<ul class="admin_menulist1">
              <li><a href="souvenir_theme.php">Souvenir Theme Setup</a></li>
              <li><a href="souvenir.php">Souvenir Products</a></li>
              <li><a href="export_souvenir.php" target="_blank">Export Data From Souvenir Products</a></li>
              <li><a href="photosnap_souvenir_path1.php">Souvenir Photo Snap Top</a></li>
              
              <?   if ($_SESSION["session_country"] == "ALL") { ?>
              <li><a href="photosnap_souvenir_path2.php">Souvenir Photo Snap Bottom</a></li>
              <? } ?>
              
              <li><a href="souvenir_campaign.php">Souvenir Special Campaign Page (CMS)</a></li>
              <li><a href="souvenir_campaign_photosnap.php">Souvenir Special Campaign Banner</a></li>
              
            </ul>
          </li>
          
          
          
          
          <? 
          if ($_SESSION["session_country"] != "ALL")
          {?>
          <li style="padding-top:10px;">
          	<ul class="admin_menulist1">
              <li><a href="photosnap_souvenir_country_path1.php">Souvenir Photosnap Receive on the spot</a></li>
              <li><a href="photosnap_souvenir_country_path2.php">Souvenir Photosnap Receive in Japan</a></li>          
              
            </ul>
          </li>
          <?
          }
         ?>
         
         
        <ul style="padding-top:20px;" class="admin_menulist">
          <li><strong>Souvenir Report</strong>
            
            <ul class="admin_menulist1">
              <li><a href="./souvenir_order_report.php" target="_blank">Souvenir Transaction Report</a></li>
              <!--<li><a href="/result_souvenir/report.php" target="_blank">Souvenir AXES Report</a></li>-->
            </ul>
          </li>
      	</ul>
         

          <li style="padding-top:20px;"><strong>Database Setup</strong>
            <ul class="admin_menulist1">          
              <li><a href="productopt.php">OPT : Products</a></li>
              <li><a href="export_tour.php" target="_blank">Export Data From OPT Products</a></li>
              <!--<li><a href="productpkg.php">PKG : Products</a></li>-->
              <li><a href="prorev.php"> Product Review</a></li>
              <!--<li><a href="hotel.php">Hotel</a></li>-->
              <!--<li><a href="hotelgroup.php">Hotel group set</a></li>-->
              <!--<li><a href="country.php">Country</a></li>-->
              <? 
              	if ($_SESSION["session_country"] == "ALL")
                {
              
              ?>
              <li><a href="country.php">Country</a></li>
              
              <? 
              }
              ?>
              <!--<li><a href="city.php">City</a></li>-->
              <li><a href="currency.php">Currency</a></li>
            </ul>
          </li>
        </ul>
          <li style="padding-top:20px;"><strong>API Database</strong>
              <ul class="admin_menulist1">
                  <li><a href="productopt_api.php">OPT : API Products</a></li>
                  <li><a href="export_api_tour.php" target="_blank">Export Data From API OPT Products</a></li>
                  <li><a href="api_result_report.php">API Result Report</a></li>
              </ul>

          <?
              if ($_SESSION["session_country"] != "ALL")
              {
              ?>
          <li style="padding-top:20px;"><strong>Ranking Management</strong>
              <ul class="admin_menulist1">
                  <li><a href="sales_ranking.php">OPT : Sales Ranking</a></li>
                  <li><a href="recommend_ranking.php" >OPT : Recommended Ranking</a></li>
              </ul>
          <?
                         }
        ?>
        <!--
        <li><a href="bannerlp.php">Banner LP</a></li>
        <li><a href="banneropt.php">Banner OPT</a></li>
        <li><a href="option2.php">Option LP</a></li>
        <li><a href="option.php">Option OPT</a></li>
        <li><a href="category.php">Product Setting Theme</a></li>
        <li><a href="category2.php">Product Setting Category</a></li>
        <li><a href="time.php">Product Setting [Time]</a></li>
        <li><a href="day.php">Product Setting [Day of stay]</a></li>
        -->
        
        <? 
        	 if ($_SESSION["session_country"] != "ALL")
             {
            ?>
        
        <ul style="padding-top:20px;" class="admin_menulist">
          <li><strong>Country Top Layout</strong>
            <ul class="admin_menulist1">
            
            
              <li><a href="photosnap_opt.php">OPT Slide bar</a></li>
              <!--<li><a href="photosnap_pkg.php">PKG Slide bar</a></li>-->
              <li><a href="country_news.php">News content (CMS)</a></li>
              <li><a href="branch_content.php">Special content (CMS)</a></li>
            </ul>
          </li>
        </ul>
        
      <?
             } 
              
             ?>
            <ul style="padding-top:20px;" class="admin_menulist">
                <li><strong>Banner Management</strong>
                    <ul class="admin_menulist1">
                          <li><a href="banner_top.php">TOP Banner</a></li>
                          <?
                    	 if ($_SESSION["session_country"] != "ALL")
                        {
                        ?>
                        <li><a href="banner_footer.php">Footer Banner</a></li>
                        <?
                         }

                        ?>
                      </ul>
                  </li>
              </ul>

        <ul style="padding-top:20px;" class="admin_menulist">
          <li><strong>JTB Top Layout</strong>
            <ul class="admin_menulist1">
             <? 
        	if ($_SESSION["session_country"] == "ALL")
            {
        	?>
              <li><a href="top_photosnap_opt.php">OPT Slide bar</a></li>
           <? 
        	}
       	 	?>
              <!--<li><a href="top_photosnap_pkg.php">PKG Slide bar</a></li>-->
              <li><a href="promotion.php">Special Campaign Page (CMS)</a></li>
              <li><a href="promotion_photosnap.php">Special Campaign Banner</a></li>
            </ul>
          </li>
        </ul>
        <ul style="padding-top:20px; padding-bottom:20px;" class="admin_menulist">
          <li><strong>Report</strong>
            
            <ul class="admin_menulist1">
              <li><a href="../admin_jtb/" target="_blank">Booking Management System</a></li>
              <!--<li><a href="booking_report.php" target="_blank">Booking Report</a></li>-->
            </ul>
          </li>
        </ul>
        <!--
        <li><a href="product.php">Product</a></li>
        <li><a href="prorev.php">Product Review</a></li>
        <li><a href="policy.php">Policy</a></li>    
        <li><a href="itinerary.php">Itinerary</a></li>
          -->
      </div>
      <div style="clear:both;"></div>
      

       
      
      
    </div>
  </div>
</div>
</body>
</html>
