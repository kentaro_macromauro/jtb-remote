<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>JTB Administration : Index</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet"  />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
</head>
<body>
<div class="header" >
  <div class="sub_header">
    <div class="logout">
      <p><a href="logout.php" class="btnlogout">Logout</a></p>
      <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="box1024 ptop10">
  <div class="header1" >
    <!--<div class="control" style=" margin:auto; margin-bottom:10px; width:940px;"> 
    	<span class="logo">Info Biz THAILAND : Back-end</span></div>-->
    <div style="margin:auto; width:940px; border:#CCC solid 1px;">
      <div style="float:left; padding-right:50px; padding-top:10px;"><img src="../../admin/images/top_admin.jpg" width="271" height="326" /> </div>
      <div style="float:left;">
        <ul style="padding-top:10px;" class="admin_menulist">
          <li><strong>Database Setup</strong>
            <ul class="admin_menulist1">
              <li><a href="productopt.php">OPT : Products</a></li>
              <!--<li><a href="productpkg.php">PKG : Products</a></li>-->
              <!--<li><a href="prorev.php"> Product Review</a></li>
              <li><a href="hotel.php">Hotel</a></li>-->
              <!--<li><a href="hotelgroup.php">Hotel group set</a></li>-->
              <!--<li><a href="country.php">Country</a></li>
              <li><a href="city.php">City</a></li>-->
              <li><a href="currency.php">Currency</a></li>
            </ul>
          </li>
        </ul>
        <!--
        <li><a href="bannerlp.php">Banner LP</a></li>
        <li><a href="banneropt.php">Banner OPT</a></li>
        <li><a href="option2.php">Option LP</a></li>
        <li><a href="option.php">Option OPT</a></li>
        <li><a href="category.php">Product Setting Theme</a></li>
        <li><a href="category2.php">Product Setting Category</a></li>
        <li><a href="time.php">Product Setting [Time]</a></li>
        <li><a href="day.php">Product Setting [Day of stay]</a></li>
        -->
        <ul style="padding-top:20px;" class="admin_menulist">
          <li><strong>Country Top Layout</strong>
            <ul class="admin_menulist1">
              <li><a href="photosnap_opt.php">OPT Slide bar</a></li>
              <li><a href="photosnap_pkg.php">PKG Slide bar</a></li>
              <li><a href="country_news.php">News content (CMS)</a></li>
              <li><a href="branch_content.php">Special content (CMS)</a></li>
            </ul>
          </li>
        </ul>
        <ul style="padding-top:20px; padding-bottom:20px;" class="admin_menulist">
          <li><strong>JTB Top Layout</strong>
            <ul class="admin_menulist1">
              <li><a href="top_photosnap_opt.php">OPT Slide bar</a></li>
              <li><a href="top_photosnap_pkg.php">PKG Slide bar</a></li>
              <li><a href="promotion_photosnap.php">Promotion Banner</a></li>
              <li><a href="promotion.php">Promotion Page (CMS)</a></li>
            </ul>
          </li>
        </ul>
        <!--
    	
        <li><a href="product.php">Product</a></li>
        <li><a href="prorev.php">Product Review</a></li>  
        
        <li><a href="policy.php">Policy</a></li>    
        <li><a href="itinerary.php">Itinerary</a></li>
        
        
          -->
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
</body>
</html>
