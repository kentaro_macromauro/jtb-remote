<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Administrator Page</title>
    <link type="text/css" href="common/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="common/js/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="common/js/ppc_datepicker.js"></script>
    <script type="text/javascript" src="common/js/table_mainpage<? echo $data[7]; ?>.js"></script>
    <script type="text/javascript">
_nowpage =   '<? echo $data[5]; ?>';
$(document).ready(
function()
{
<? echo $data[6]; ?>
});
</script>
    <script type="text/javascript" src="common/js/jd_datamanager/ajaxfileupload.js"></script>


    <script type="text/javascript" src="common/js/jd_datamanager/jd_datamanager.js"></script>


</head>

<body>
<div class="header">
    <div class="sub_header">Administration <div class="logout"><p><a href="logout.php" class="btnlogout">Logout</a></p>
            <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p></div></div>
</div>
<div class="clear"></div>

<div class="box1024 ptop10">
    <? echo $data[2]; ?>
    <div class="control"><span class="logo"><? echo $data[1]; ?></span>
        <!--      <div class="panel_btn2"> <span class="btn" id="btn_save"><b>Save</b></span> <span class="btn" id="btn_close" ><b>Close</b></span> </div> -->
    </div>

    <div class="data">
        <div class="table">

            <!--<?  /*f_loop($data[3]);*/ ?>-->
            <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
                <tr>
                    <th valign="middle" width="120" align="left" >Country</th>
                    <td width="210"><? echo $data[3][0]; ?></td>
                    <th valign="middle" align="left" >City</th>
                    <td><? echo $data[3][1]; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Product Id</th><td colspan="3"><? echo $data[3][2]; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Product Type</th><td colspan="3"><? echo $data[3][3]; ?></td>
                </tr>

                <tr>
                    <th valign="middle" align="left">Product Name(EN)</th>
                    <td colspan="3"><? echo $data[3][4]; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Product Name(JP)</th>
                    <td colspan="3"><? echo $data[3][5]; ?></td>
                </tr>
                <tr>
                    <th valign="top" align="left">Theme</th>
                    <td colspan="3"><? echo $data[3][6]; ?></td>
                </tr>
                <tr>
                    <th valign="top" align="left">Time</th>
                    <td colspan="3"><? echo $data[3][7]; ?></td>
                </tr>

                <tr>
                    <th valign="top" align="left">Option</th>
                    <td colspan="3"><? echo $data[3]['option']; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Min Price</th>
                    <td colspan="3"><? echo $data[3][8]; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Max Price</th>
                    <td colspan="3"><? echo $data[3][9]; ?></td>
                </tr>
                <tr>
                    <th valign="top" align="left">Description(JP)</th>
                    <td colspan="3"><? echo $data[3][10]; ?></td>
                </tr>
                <tr>
                    <th valign="middle" align="left">Image</th>
                    <td colspan="3"><? echo $data[3][11]; ?></td>
                </tr>


            </table>

        </div>

    </div>

</div>


</body>
</html>