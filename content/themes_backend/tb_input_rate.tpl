<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Page</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="common/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="common/js/table_mainpage<? echo $data[7]; ?>.js"></script>
<script type="text/javascript">
_nowpage =   '<? echo $data[5]; ?>';
_nowpage2 =   '<? echo $data[6]; ?>';
$(document).ready(
function()
{
<? echo $data[6]; ?>
});
</script>

</head>

<body>
<div class="header">
  <div class="sub_header">Administration <div class="logout"><p><a href="logout.php" class="btnlogout">Logout</a></p> 
 <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p></div></div>
</div>
<div class="clear"></div>
<form action="<? echo $data[0][0]; ?>" <? echo $data[0][1]; ?> name="frmInput" id="frmInput"  method="post">
  <div class="box1024 ptop10">
     <? echo $data[2]; ?>
    <div class="control"><span class="logo"><? echo $data[1]; ?></span>
      <div class="panel_btn2"> <span class="btn" id="btn_save"><b>Save</b></span> <span class="btn" id="btn_close_cc" ><b>Close</b></span> </div>
      <input type="hidden" name="page_id" value="<? echo $data[4][0]; ?>" />
      <input type="hidden" name="action" value="<? echo $data[4][1]; ?>" />
    </div>
    
    <div class="data">
      <div class="table">
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
          <tr>
          	<th valign="middle" width="120" align="center" class="tittle" >Country</th>
            <th valign="middle" align="center"  class="tittle">Currency Code</th>
            <th valign="middle" align="center"  class="tittle">Currency Sign</th>
            <th valign="middle" align="center"  class="tittle" >Currency Rate</th>
            <th valign="middle" align="center"  class="tittle">Show JPY</th>
          </tr>
          
         <? for ($i =0; $i < count($data[3]) ; $i++) { ?> 
          <tr>
          	<td valign="middle" align="left" ><? echo $data[3][$i][country_name_jp]; ?></td>
            <td valign="middle" align="left" ><input type="text" name="inp_code[]" value="<? echo $data[3][$i][code]; ?>" /><span class="msg-req">*</span></th>
            <td valign="middle" align="left" ><input type="text" name="inp_sign[]" value="<? echo $data[3][$i][sign]; ?>" /><span class="msg-req">*</span></th>
            <td valign="middle" align="left" ><input type="text" name="inp_rate[]" value="<? echo $data[3][$i][rate]; ?>" /><span class="msg-req">*</span>
            <input type="hidden" name="currency_id[]" value="<? echo $data[3][$i][currency_id]; ?>"  />
            </td>
            <td valign="middle" align="center">
            	<input type="checkbox" name="inp_showrate<? echo $i; ?>"  <? 
                if ($data[3][$i][show_rate] == 1) echo 'checked="checked"'; 
                ?> />  
            </td>
          </tr>
          <?  } ?>
        </table>
      	
      </div>
      	<? if (!empty($data[3][0][message]) )
           { 
        ?>
        	<div class="comment-req" style="background:#FFF;"><span class="msg-req"><? echo $data[3][0][message]; ?></span></div>
        <?	
            }
         ?>
      	<div class="comment-req"><span class="msg-req">* Mandatory fields</span></div>
      <div class="h5"></div>
    </div>
    
    
    <div class="footer">&nbsp;</div>
  </div>
</form>




</body>
</html>
