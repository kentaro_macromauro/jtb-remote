<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Back-end</title>
<link type="text/css" href="../admin/common/css/main.css" rel="stylesheet"  />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript">
_nowpage = '<? echo $data[5]; ?>';
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='article.php?com_select="+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
<script type="text/javascript" src="common/js/table_mainpage.js"></script>

</head>
<body>

<div class="header">
  <div class="sub_header">GM-ch Administration <div class="logout"><a href="logout.php">Logout</a></div></div>
</div>
<div class="clear"></div>

<div class="box1024 ptop10">

  <div class="control"><span class="logo"><? echo $data[0]; ?></span>
    <div class="panel_btn">
    	<span class="btn" id="btn_unactive"><b>Unpublish</b></span>
    	<span class="btn" id="btn_active"><b>Publish</b></span>
        
        <span class="btn" id="btn_new"><b>New</b></span>
        <span class="btn" id="btn_delete" ><b>Delete</b></span>
    </div>
    <form action="<? echo $data[7]; ?>" method="post" name="del_obj">
    <input type="hidden" name="action" value="delete" />
    <input type="hidden" name="action_id" id="action_id" value=""  />    
    </form>

    
    <form action="<? echo $data[7]; ?>" method="post" name="enabled_obj">
    <input type="hidden" name="action" value="active" />
    <input type="hidden" name="action_id" id="enabled_action_id" value=""  />    
    </form>
    
    
    <form action="<? echo $data[7]; ?>" method="post" name="disabled_obj">
    <input type="hidden" name="action" value="unactive" />
    <input type="hidden" name="action_id" id="disabled_action_id" value=""  />    
    </form>
	<input type="hidden" name="ref_id" id="ref_id" value="<? echo $data[6]; ?>" />
  </div>
  <!-- bread camp -->
   <ul class="fillter"><li>Category Fillter : </li><li style="padding-top:3px;">
  <form action="" method="get">  
  		<select name="com_select" id="jumpMenu" onchange="MM_jumpMenu('parent',this,0)" style="width:auto;"> 
        	<option value="">All</option>
    		<? echo $data[8]; ?>
        </select>
  </form>
  </li></ul>
  <? echo $data[1]; ?>
  <!-- bread camp -->
  <div class="data">
    <!--<? f_loop($data[2]); ?>-->
    <div class="table">
      <table width="100%" class="table_data" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
        <tr align="center">
          <th style="width:50px;">#</th>
          <th width="5"><input name="chk_all" type="checkbox" value="all" class="chkbox_all" /></th>
          <?  f_loop($data[2]); ?>
        </tr>
        <?  f_loop($data[3]); ?>
      </table>
    </div>
    <? echo $data[4]; ?>
  </div>
  <div class="footer">&nbsp;</div>
</div>
</body>
</html>