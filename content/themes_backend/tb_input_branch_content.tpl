<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Page</title>
<link type="text/css" href="common/css/main.css" rel="stylesheet" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="common/js/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="common/js/ppc_datepicker.js"></script>
<script type="text/javascript" src="common/js/table_mainpage<? echo $data[7]; ?>.js"></script>
<script type="text/javascript">
_nowpage =   '<? echo $data[5]; ?>';
$(document).ready(
function()
{
<? echo $data[6]; ?>
});
</script>
<script type="text/javascript" src="common/js/jd_datamanager/ajaxfileupload.js"></script>
<script type="text/javascript" src="common/js/jd_datamanager/jd_datamanager.js"></script>
</head>
<body>
<div class="header">
  <div class="sub_header">Administration
   <div class="logout"><p><a href="logout.php" class="btnlogout">Logout</a></p> 
 <p>User : <a href="user_setting.php"><? echo $_SESSION["session_name"]  ; ?></a></p></div>
  </div>
</div>
<div class="clear"></div>
<div class="box1024 ptop10"> <? echo $data[2]; ?>
  <div class="control"><span class="logo"><? echo $data[1]; ?></span>
    <div class="panel_btn2"> <span class="btn" id="btn_save"><b>Save</b></span> <span class="btn" id="btn_close" ><b>Close</b></span> </div>
  </div>
  <div class="data">
    <div class="table">
      <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
        <tr>
          <th valign="middle" width="97" align="left">Content Image</th><td><? echo $data[3][0]; ?> <span>Content Image (Width:400px Height: 300px)</span></td>
        </tr>
      </table>
      <div class="height10"></div>
      <form action="<? echo $data[0][0]; ?>" <? echo $data[0][1]; ?> name="frmInput" id="frmInput"  method="post">
        <input type="hidden" name="page_id" value="<? echo $data[4][0]; ?>" />
        <input type="hidden" name="action" value="<? echo $data[4][1]; ?>" />
        <table width="100%" class="table_data pb5" border="0" cellpadding="0" cellspacing="1" bgcolor="#CDCDCD">
          <?
          for ($i=1; $i<count($data[3]); $i++)
          {
          	  echo $data[3][$i];
          }
          ?>                    
        </table>
      </form>
    </div>
    <div class="comment-req"><span class="msg-req">* Mandatory fields</span></div>
    <div class="h5"></div>
  </div>
  <div class="footer">&nbsp;</div>
</div>
<div class="jed_ddedit">
  <div class="box_533">
    <div id="loading"style="display:none;">loading</div>
    <ul class="img_pocket">
    </ul>
  </div>
  <div class="inport_link">
    <div>
      <table class="w533" border="0" align="left" cellpadding="0" cellspacing="1">
        <tr>
          <th width="136" align="left" valign="top">Image Link : </th>
          <td width="391" align="left" valign="top"><input type="text" name="boxpiclink" class="pic_name" value="" size="60" /></td>
        </tr>
        <tr>
          <th align="left" valign="top">Image Description : </th>
          <td align="left" valign="top"><input type="text" name="boxpicdesc" class="pic_desc" value="" size="60" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="left" valign="top"><input type="button" id="btn_insert" value="Insert" />
            &nbsp;
            <input type="button" id="btn_del" value="Delete" />
            <input type="hidden" id="obj_parent_name" value="" /></td>
        </tr>
        <tr>
          <th align="left" valign="top"> Upload to Server : </th>
          <td align="left" valign="top"><form name="form" action="" method="POST" enctype="multipart/form-data">
              <input id="fileToUpload" type="file" name="fileToUpload" >
              <input type="button" id="buttonUpload" onClick="return ajaxFileUpload();" value="Upload" />
            </form>
            <div class="box_close"></div></td>
        </tr>
      </table>
    </div>
  </div>
</div>
</body>
</html>
