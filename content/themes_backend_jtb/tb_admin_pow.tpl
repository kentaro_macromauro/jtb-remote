<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator ID.Pass Management screen</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />

<style type="text/css">
	.err_msg{font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#C30; padding-top:10px;}	
		
	#username{
		padding:1px;
		/*font-size:18px;*/
		border:3px #CCC solid;
	}
	
	#tick{display:none}
	#cross{display:none}	
	#tick2{display:none}
	#cross2{display:none}
	
</style>

<script type="text/javascript" src="./common/js/jquery-1.5.1.min"></script>
<script type="text/javascript">
/*
function nidoosi2(form) {	
	var elements = form.elements;
	
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].type == 'submit') {
			elements[i].disabled = true;
		}
	}
} 

*/


$(document).ready(
	function()
	{
		
		$('#admin_name').keyup(admin_name_check);
		$('#admin_id').keyup(admin_user_check);
		
		
	function admin_name_check(){	
	
		var _admin_name = $('#admin_name').val();
		
		//if(_admin_name == "" || _admin_name.length < 4){
		if(_admin_name == ""){
				
			$('#admin_name').css('border', '2px #CCC solid');
			$('#tick').hide();
		}else{
		
			jQuery.ajax({
				   type: "POST",
				   //url: "check.php",
				   url: "check_admin_name.php",
				   data: 'admin_name='+_admin_name,
				   cache: false,
				   success: function(response){ //alert('response='+response);
				//if(response == 1){
				if(response > 0){
					//alert('111');
						$('#admin_name').css('border', '2px #C33 solid');	
						$('#tick').hide();
						$('#cross').fadeIn();
					}else{
					//alert('222');			
						$('#admin_name').css('border', '2px #090 solid');
						$('#cross').hide();
						$('#tick').fadeIn();
					}
				
				}
			});
		}
	}
	
	function admin_user_check(){	
	
		var _admin_user = $('#admin_id').val();
		
		//if(_admin_name == "" || _admin_name.length < 4){
		if(_admin_user == ""){
				
			$('#admin_id').css('border', '2px #CCC solid');
			$('#tick').hide();
		}else{
		
			jQuery.ajax({
				   type: "POST",
				   //url: "check.php",
				   url: "check_admin_user.php",
				   data: 'admin_user='+_admin_user,
				   cache: false,
				   success: function(response){ //alert('response='+response);
				   
				//if(response == 1){
				if(response > 0){
					//alert('111');
						$('#admin_id').css('border', '2px #C33 solid');	
						$('#tick2').hide();
						$('#cross2').fadeIn();
					}else{
					//alert('222');			
						$('#admin_id').css('border', '2px #090 solid');
						$('#cross2').hide();
						$('#tick2').fadeIn();
					}
				
				}
			});
		}
	}	
	//================================================================
				
		$('#btn_insert').click(
		function()
		{

			_admin_id = $('#admin_id').val(); 
			_admin_pass = $('#admin_pass').val(); 
			_msg ='';
		
			//alert('_admin_id='+_admin_id.length);
			//alert('_admin_pass='+_admin_pass.length);
			
			if($('#admin_name').val() ==''){
				_msg ='Please enter Name';
			}			
			if(_admin_id.length < 5){
				_msg +='\nPlease enter ID Minimum 5 characters';
			}
			if(_admin_pass.length < 5){
				_msg +='\nPlease enter Password Minimum 5 characters';
			}
			
			if(_msg != ""){
				alert(_msg);
				return false;
			}
			else{
				return true;	
			}
		});	
		
		$('.btn_del').click(
		function()
		{
			var msg = "Are you sure you want to Delete?";
			if ( confirm(msg) ){				
				return true;
			}
			else{
				return false;	
			}
			
		});			
		
	}
);

</script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" />
</head>
<body>

<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a> &gt; Administrator Management ID/PASS </li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
  <div class="sub_head">
    	<ul class="head_admin">
    	<li class="icon_admin">Administrator Management : Find <? echo $data[page_count]; ?> User</li>
    	</ul>           
                        
			<!--<form name="admin_save" method="post" action="./admin_save.php" onSubmit="return nidoosi2(this)">-->
			<form name="admin_save" method="post" action="./admin_save.php">		 
			<!--<div class="save">
                <input type="image" src="./common/images/save.jpg" name="button" id="button" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />
                </div>-->

                  <table width="100%">
                    <tr>
                      <th class="center_set">No</th>                                               
                      <th class="center_set">Name</th>
                      <th class="center_set">ID</th>
                      <th class="center_set">Password</th>
                      <th class="center_set">Authorize Type</th>
                      <th class="center_set">Edit</th>
                      <th class="center_set">Delete</th>
                    </tr>                    
                    <?                     
                    	f_loop($data[3]);                    
                    ?>                                              
                  </table>

  	      </form>

			<br />
 
		<!--<form name="insert" method="post" action="./admin_insert.php" onSubmit="return nidoosi2(this)">-->
        <form name="insert" method="post" action="./admin_action.php">		 

		<div id="table">
                  <table width="100%">
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>ID</th>
                      <th>Password</th>
                      <th>Authorize Type</th>
                      <th>Add</th>
                    </tr>    				
                    
                    <tr>
                      <td>-<br /></td>
                      <td><input name="admin_name" type="text" id="admin_name" size="30" placeholder="New Entry" /><img id="tick" src="./common/images/tick.png" width="16" height="16" /><img id="cross" src="./common/images/cross.png" width="16" height="16" /></td>
                      <td><input name="admin_id" type="text" id="admin_id" size="30" placeholder="New Entry" /><img id="tick2" src="./common/images/tick.png" width="16" height="16" /><img id="cross2" src="./common/images/cross.png" width="16" height="16" /></td>
                      <td><input name="admin_pass" type="text" id="admin_pass" size="30" placeholder="New Entry" /></td>
                      <td>
                        <select name="admin_level" id="admin_level">
                                    <option value="A">A</option>     
                                    <option value="B">B</option>     
                                    <option value="C">C</option>     
                                    <option value="D">D</option>     
                                    <option value="E">E</option>
                                    <option value="F">F</option>
                                    <option value="G">G</option>
                                    <option value="H">H</option>
                                    <option value="I">I</option>
                                    <option value="J">J</option>
                                    <option value="K">K</option>
                                    <option value="L">L</option>
                                    <option value="M">M</option>
                                    <option value="N">N</option>                                                
                                    <option value="O">O</option>
                                    <option value="P">P</option>
                                    <option value="Q">Q</option>
                        </select>
                      </td>
                      <td>
<input type="image" src="./common/images/new.jpg" name="btn_insert" id="btn_insert" value="Admin New Edit" border="0" onmouseover="this.src='./common/images/new_on.jpg'" onmouseout="this.src='./common/images/new.jpg'"/>
                      </td>
                    </tr>  
                  </table>
                  <!--table end-->
                  <input type="hidden" name="action" value="insert" />                
                  </div>

  	      </form> 
          
           <br />
           
                <div class="pager">
                <? echo $data[4]; ?>
                </div>

   </div>       
 </div>
 <!--end content-->
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
