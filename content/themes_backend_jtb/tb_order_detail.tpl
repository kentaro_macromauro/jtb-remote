<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 
</head>
<body>

<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>   >  <a href="./order_info_list.php">Order information List</a> > <? echo $data[b_id] ?></li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="order">
    	<li class="icon_order">Order Detail</li>
    	</ul>
         
         	<div id="content_left">
			
			<? include("o_left_content.tpl"); ?>
            
            </div>
            <!--content_left-->
         	<div id="content_right">
            <div id="title">
                    	<div class="title_left">
                        <!--<img src="./common/images/logo.jpg" title="My-bus" />-->
                </div>
                   	  <div class="title_right">
                   	    <h2>GUEST SHEET (<? echo $data[book_type]?>) </h2>
                        </div>
                    </div>
                	<div id="first" style="margin-top:40px;">
               		  <div class="date">DATE :&nbsp;<span class="font_bold"><? echo $data[create_date]; ?></span></div>
               		  <div class="no">BOOKING ID :&nbsp;<span class="font_bold"><? echo $data[jtb_bookind_id]; ?></span></div>
               		  <div class="tour_id">TOUR ID :&nbsp;<span class="font_bold"><? echo $data[tour_id]; ?></span></div>                      
                    </div>
                
                	<div id="second">
                   	  <div class="name">NAME :&nbsp;<span class="font_bold"><? echo $data[full_name] ?></span></div>
                   	  <div class="c_s_no">E-MAIL :&nbsp;<span class="font_bold"><? echo $data[reg_email] ?></span></div>
                    </div>
                    
                	<div id="third">
                        <div class="tel">TEL :&nbsp;<span class="font_bold"><? echo $data[reg_tel1]; ?></span></div>
                        <div class="fax">FAX :&nbsp;<span class="font_bold"><? echo $data[reg_fax]; ?></span></div>
                    </div>
            
      		<? echo $data['detail']; ?>
            </div>
            <!--content_left-->            
         
 </div>
 
 </div>
 <!--end content-->

 </div>
 </div>

<? include("footer.tpl"); ?>

</body>

</html>
