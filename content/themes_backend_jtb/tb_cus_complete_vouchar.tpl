<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Making Voucher</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css">
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css">


<LINK REL="SHORTCUT ICON" HREF="./ppp.icon"> 
</head>

<body>
<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  <a href="./customer_info_list.php">Customer Information List</a></a> > <? echo $data[c_booking_id]; ?></li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="head_customer">
    	<li class="icon_customer">Customer Making Voucher <? echo $data[type]; ?> </li>
    	</ul>		 
                
        <!--content_left-->
        <div id="content_left">			
        <? include("c_left_content.tpl"); ?>                                  
        </div>
        <!--content_left-->        

    <table width="40%">
    	<tr>
        	<!--<td colspan="3">{$result}</td>-->
            <td colspan="3"><? echo $data[result]; ?></td>
        </tr>
     </table>
    <br />

    <table width="40%">
		<tr>
        	<th>No</th>
        	<th>Booking Id</th>
        	<th>Download</th>
        </tr>

    	<tr>
        	<td>1</td>
        	<td>
       		<a href="<? echo strtolower($data[dir]).$data[c_booking_id]; ?>.pdf" target="_blank"><? echo $data[c_booking_id]; ?></a>     
            </td>
        	<td>            
            <a href="./download.php?path=pdf_<? echo strtolower($data[type]); ?>&name=<? echo $data[c_booking_id]; ?>" ><img src="./common/images/download.jpg" alt="download" border="0" onmouseover="this.src='./common/images/download_on.jpg'" onmouseout="this.src='./common/images/download.jpg'"></a>
            </td>
        </tr>

    </table>

 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
