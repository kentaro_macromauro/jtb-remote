<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>JTB Administration : Index</title>

<link type="text/css" href="common/css/main.css" rel="stylesheet"  />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
</head>

<?
		unset($_SESSION["session_start_date"]);
		unset($_SESSION["session_end_date"]);
		unset($_SESSION["session_c_keyword"]);
		unset($_SESSION["session_c_status"]);
		unset($_SESSION["session_order_status"]);
		unset($_SESSION["session_product_type"]);
        
        //print_r($_SESSION);
?>

<body>
<div class="header" >
  <div class="sub_header">
    <div class="logout">
      <p><a href="logout.php" class="btnlogout">Logout</a></p>
      <p>User : <? echo $_SESSION["session_name"]  ; ?> Level : <? echo $_SESSION["session_level_name"]  ; ?></p>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="box1024 ptop10">
  <div class="header1" >
  
    <div style="margin:auto; width:940px; border:#CCC solid 1px;">
      <div style="float:left; padding-right:50px; padding-top:10px;"><img src="../admin_jtb/images/top_admin.jpg" width="271" height="326" /> </div>
      <div style="float:left;">
            

        
        <ul style="padding-top:20px;" class="admin_menulist">
          <li><strong>Booking Management</strong>
            <ul class="admin_menulist1">
              <li><a href="order_info_list.php">Order information list</a></li>
              <li><a href="customer_info_list.php">Customer information list</a></li>
			  <li><a href="axes_report_payment.php">Credit Card Transaction Report</a></li>
            </ul>
          </li>
        </ul>         
        
        <ul style="padding-top:20px; padding-bottom:20px;" class="admin_menulist">
          <li><strong>Report</strong>
            <ul class="admin_menulist1">
              <li><a href="booking_report_select.php">Booking report</a></li>
              <li><a href="axes_payment.php">Axes Payment Result</a></li>
              <li><a href="axes_payment_souvenir.php">Axes Payment Result : Souvenir</a></li>
            </ul>
          </li>
        </ul>
		<?
			if  ( ($_SESSION["session_country"] == "ALL") ||  ($_SESSION["session_country"] == "SGP") ||  ($_SESSION["session_country"] == "THA")  ||   ($_SESSION["session_country"] == "TWN") )
			{
		?>
			<ul style=" padding-bottom:20px;" class="admin_menulist">
          <li><strong>Rakunavi Support</strong>
            <ul class="admin_menulist1">
              <li><a href="inquiry_list.php">Inquiry list</a></li>
            </ul>
          </li>
        </ul>
		<?
			}
        ?>
         <? 
        	if ($_SESSION["session_level"] == "A")
            {
        ?>
        <ul style="padding-bottom:20px;" class="admin_menulist">
          <li><strong>Administrator Management</strong>
            <ul class="admin_menulist1">
              <li><a href="admin_pow.php">Administrator Management</a></li>
              <li><a href="permis.php">Authorize</a></li>
            </ul>
          </li>
        </ul>        
        
         <? 
			}
        ?>                                      

      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
</body>
</html>
