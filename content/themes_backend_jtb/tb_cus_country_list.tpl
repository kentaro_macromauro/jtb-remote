<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />

<script type="text/javascript" src="./common/js/datetimepicker.js"></script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 
</head>
<body>

<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  Customer information list</li>
   	  </ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="order">
    	<li class="icon_order">Customer information list : Find <? echo $data[page_count] ?> Level</li>
    	</ul>                                   
            
            <br />                                         
            <div id="save" align="right">
   </div>              
        <div id="table">
        <table width="100%" border="1">
        <tr>
          <th class="center_set" width="10%">No</th>
          <th class="center_set" width="10%">Level</th>
          <th class="center_set" width="60%">項目/ item</th>
          <th class="center_set" width="20%"></th>          
        </tr>
        
        <?  f_loop($data[3]); ?>                                                                  
        </table>            
        
        </div>
        
        <!--table end-->                      
          <br />
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>

<!--{include file="footer.tpl"}-->
<? include("footer.tpl"); ?>

</body>

</html>
