<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer information</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 
</head>

<body>
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  Customer Information </li>
   	  </ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="order">
    	<li class="icon_order">Customer Information  : Find <? echo $data[page_count] ?> Customer</li>
    	</ul>

 				 <div class="list">
                 <form name="customer_search" method="post" action="./customer_info_list.php">
                            <input  type="image" src="./common/images/reset.jpg" name="reset" value="reset" border="0" onmouseover="this.src='./common/images/reset_on.jpg'" onmouseout="this.src='./common/images/reset.jpg'"/>
                            <input type="hidden" value="<? echo $data[c_keyword]; ?>" name="c_keyword" />
                            <input type="hidden" value="<? echo $data[c_status]; ?>" name="c_status" />
                            <input type="hidden" value="<? echo $data[c_booking_id]; ?>" name="c_booking_id" />
                            <input type="hidden" value="<? echo $data[start_date]; ?>" name="start_date" />
                            <input type="hidden" value="<? echo $data[end_date]; ?>" name="end_date" />
                            <input type="hidden" value="" name="c_keyrowd_2" />
                            <input type="hidden" value="0" name="c_status_2" />
                            <input type="hidden" value="" name="c_booking_id_2" />
                            <input type="hidden" value="" name="start_date_2" />
                            <input type="hidden" value="" name="end_date_2" />
                            <input type="hidden" value="reset" name="search_type" />                            
                            </form>
                            </div>                    
                      
<div id="search">
            
                        <form name="admin_save" method="post" action="./customer_info_list.php">
            
                    
                        <table width="100%">
                            <tr>
                                <th>Keywords</th>
                                <td class="set_left"><label>
                                  <input type="text" size="45" name="c_keyword" value="<? echo $data[c_keyword]; ?>" id="c_keyword" />
                              </label></td>
                                <th>Status</th>
                                <td class="set_left">
                                  <? echo $data[8]; ?>                     
                                </td>
                                                                
                                <td rowspan="2">
<input type="image" src="./common/images/search.jpg" name="button" id="button" value="Search" border="0" onmouseover="this.src='./common/images/search_on.jpg'" onmouseout="this.src='./common/images/search.jpg'" />                                
<input type="hidden" value="search" name="search_type" />
</td>
                            </tr>
                            <tr>
                                <th>Book Type</th>
                                <td class="set_left">     
                                  <? echo $data[9]; ?>                                                                      
                                </td>
                                <th>Date</th>
                                <td class="set_left"><!--<input type="text" size="45" name="c_entry_date" id="c_entry_date" />-->
                                  <input name="start_date" id="ifrom_date" class="textFieldSize60" type="text" value="<? echo $data[start_date]; ?>" readonly="readonly" />
      <a href="javascript:NewCssCal('ifrom_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0" alt="" /></a>&nbsp;-&nbsp;
      <input name="end_date" id="ito_date" class="textFieldSize60" type="text" value="<? echo $data[end_date]; ?>" readonly="readonly" />
      <a href="javascript:NewCssCal('ito_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0" alt="" /></a></td>
                            </tr>
                        </table>
                    
</form>
                    </div>
            
            <br />                                         
                       <div id="save" align="right">
   </div>              
        <div id="table">
        <table width="100%">
        <tr>
          <th class="center_set" width="5%">No</th>
          <th class="center_set" width="5%">Id</th>
          <th class="center_set" width="15%">Book No</th>
          <th class="center_set" width="25%">Name</th>
          <th class="center_set" width="15%">Status</th>
          <th class="center_set" width="15%">Date</th>
          <th class="center_set" width="8%">Edit</th>
          <th class="center_set" width="8%">Delete</th>
        </tr>
        
        <?  f_loop($data[3]); ?>                                                                  
        </table>            
        
        </div>
                              <!--table end-->
                              
                              
                              
                      <br />
                      <!--link-->
                        <div class="pager">
                        <!--{$link}-->
                        <? echo $data[4]; ?>
                        </div>
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
