<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>My Bus | Administrator Back-end</title>
<link type="text/css" rel="stylesheet" href="common2/css/template.css" media="all" />
<link type="text/css" rel="stylesheet" href="common2/css/login.css" media="all" />
<script type="text/javascript" src="common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript"> $(document).ready( function(){ $('input[name="inp_user"]').focus(); } ); </script>
<body>
<!-- header -->
<div class="header">
  <div class="header-content">
    <div class="header-user"></div>
    <div class="header-manual"></div>
    <div class="header-logout"></div>
  </div>
</div>
<!-- header -->
<!--detail-->
<div class="content">
  <div class="mid-container">
    <div class="bordered-table padding10 boxlogin">
      <h1 class="font-h1">Administration Login</h1>
      <p>Use a valid username and password to gain access to the Administrator Back-end. </p>
      <p><a href="/">Return to site Home Page</a></p>
      <form method="post" action="login.php">
        <div class="clear">
          <label class="xlInput">User Name</label>
          <div class="input">
            <input class="input-large" id="xlInput" name="inp_user" size="30" type="text">
          </div>
        </div>
        <div class="clear">
          <label class="xlInput">Password</label>
          <div class="input ">
            <input class="input-large " id="xlInput" name="inp_pass" size="30" type="password">
            <input type="hidden" name="loginkey" value="a2dx7a9121" />
          </div>
        </div>
        <? 
        	echo $data[0];
        ?>
        <div class="actions">
          <input type="submit" class="btn primary" value="Login &raquo;" />
        </div>
      </form>
    </div>
  </div>
</div>
<!--footer-->
<div class="footer">
  <div class="footer-content">
    <p class="footer-content-text1">Copyright © 2012 my-bus.com</p>
  </div>
</div>
<!--footer-->
</body>
</html>
