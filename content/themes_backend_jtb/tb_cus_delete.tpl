<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Refresh" CONTENT="1; URL=./customer_info_list.php">

<title>Customer information</title>
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css">
 
</head>
<body>
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  <a href="./customer_info_list.php">Customer information list</a>  >  Customer Delete Complete</li></ul>
 <!-- end breadcrumb --> 
 
 <div class="line"><img src="common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="head_customer">
    	<li class="icon_customer">Customer information List</li>
    	</ul>
 
		 <p>Delete Data Complete <? echo $data[b_id]; ?></p>         
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
