<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer information Detail -<? echo $data[product_type]?>-</title>

<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>
 
</head>
<body>
<!--header inclu6de-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  <a href="./customer_info_list.php">Customer Information List</a> > <? echo $data[jtb_bookind_id] ?></li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="order">
    	<li class="icon_order">Customer Detail</li>
    	</ul>
         
         	<div id="content_left">
			<!--{include file="o_left_content.tpl"}-->
            <? include("c_left_content.tpl"); ?>

            </div>
            <!--content_left-->
                 
            <form name="customer_edit" method="post" action="./customer_action.php">   
                 
         	<div id="content_right">
            		<div id="title">
                    	<div class="title_left">
                        <!--<img src="./common/images/logo.jpg" title="My-bus" />-->
                </div>
                     	  <div class="title_right">
                   	    <h2>GUEST SHEET (<? echo $data[book_type]?>) </h2>
                        </div>
                    </div>
<!--                	<div id="first">
               		  <div class="date">DATE :&nbsp;<span class="font_bold"><? echo $data[create_date]; ?></span></div>
               		  <div class="no">BOOKING ID :&nbsp;<span class="font_bold"><? echo $data[jtb_bookind_id]; ?></span></div>
                    </div>-->
                	<div id="first" style="margin-top:40px;">
               		  <div class="date">DATE :&nbsp;<span class="font_bold"><? echo $data[create_date]; ?></span></div>
               		  <div class="no">BOOKING ID :&nbsp;<span class="font_bold"><? echo $data[jtb_bookind_id]; ?></span></div>
               		  <div class="tour_id">TOUR ID : <input type="text" name="inp_tour_id" id="inp_tour_id" value="<? echo $data[tour_id]; ?>" class="inp_width70" /></div>
                    </div>
                                    
                	<div id="second">
                   	  <div class="name">NAME :&nbsp;<span class="font_bold"><? echo $data[full_name] ?></span></div>
                   	  <div class="c_s_no">E-MAIL :&nbsp;<span class="font_bold"><? echo $data[reg_email] ?></span></div>
                    </div>
                    
                	<div id="third">
                        <div class="tel">TEL :&nbsp;<span class="font_bold"><? echo $data[reg_tel1]; ?></span></div>
                        <div class="fax">FAX :&nbsp;<span class="font_bold"><? echo $data[reg_fax]; ?></span></div>                   
                    </div>                  

                <div style="clear:both;"></div>
                
                <div class="save">
<input type="image" src="./common/images/save.jpg" name="button" id="button" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />                </div>

      		<? echo $data['detail']; ?>              
                  
                <div class="save">
<input type="image" src="./common/images/save.jpg" name="button2" id="button2" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />                

                <input type="hidden" name="action" value="update" />
                <input type="hidden" name="b_id" value="<? echo $data[b_id]; ?>" />
                <input type="hidden" name="jtb_bookind" value="<? echo $data[jtb_bookind_id] ?>" />
</div>

      </div>
</form>
            <!--content_left-->
                                 
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>
<? include("footer.tpl"); ?>
</body>

</html>
