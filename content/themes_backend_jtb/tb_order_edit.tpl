<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information Detail -<? echo $data[0][product_type]?>-</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 

<script type="text/javascript" src="./common/js/datetimepicker.js"></script>

<script type="text/javascript">
/*
function nidoosi2(form) {
	var elements = form.elements;
	
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].type == 'submit') {
			elements[i].disabled = true;
		}
	}
} 
*/
</script>

</head>
<body>
<!--header inclu6de-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>   >  <a href="./order_country_list.php">Order information list</a> > <a href="./order_info_list.php"><? echo $data[level_name] ?></a> > <? echo $data[0][jtb_bookind_id] ?></li></ul>
 <!-- end breadcrumb -->   
 
 <div class="line"><img src="common/images/line.jpg" width="994" height="4" alt="" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="order">
    	<li class="icon_order">Order Detail</li>
    	</ul>
         
         	<div id="content_left">
			<!--{include file="o_left_content.tpl"}-->
            <? include("o_left_content.tpl"); ?>

            </div>
            <!--content_left-->
                    
         	<div id="content_right">
            		
                <div id="title">
                    	<div class="title_left">
                        <!--<img src="./common/images/logo.jpg" title="My-bus" alt="" />-->
                </div>
                     	  <div class="title_right">
                   	    <h2>GUEST SHEET (<? echo $data[0][book_type]?>) </h2>
                        </div>
                    </div>
                	<div id="first" style="margin-top:40px;">
               		  <div class="date">DATE :&nbsp;<span class="font_bold"><? echo $data[0][update_date]; ?></span></div>
               		  <div class="no">BOOKING ID :&nbsp;<span class="font_bold"><? echo $data[0][jtb_bookind_id]; ?></span></div>
                    </div>
                
                	<div id="second">
                   	  <div class="name">NAME :&nbsp;<span class="font_bold"><? echo $data[0][full_name] ?></span></div>
                   	  <div class="c_s_no">E-MAIL :&nbsp;<span class="font_bold"><? echo $data[0][reg_email] ?></span></div>
                    </div>
                    
                	<div id="third">
                        <div class="tel">TEL :&nbsp;<span class="font_bold"><? echo $data[0][reg_tel1]; ?></span></div>
                        <div class="fax">FAX :&nbsp;<span class="font_bold"><? echo $data[0][reg_fax]; ?></span></div>
                    </div>                    

				<form name="order_detail" method="post" action="./order_edit_complete.php" onSubmit="return nidoosi2(this)">
                
                <div class="save">
<input type="image" src="./common/images/save.jpg" name="button" id="button" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />                </div>

              <table width="100%" border="0">
						<tr align="center">
							<td width="30%">DETAIL</td>
							<td width="10%">DATE</td>
							<td width="10%">QTY</td>
							<td width="10%">PRICE</td>
						</tr>
						
                        <tr>
							<td class="set_left"><? echo $data[3][0]; ?></td>
							<td class="set_left">
<input name="inp_book_date[0]" id="inp_book_date[0]" class="textFieldSize60" type="text" value="<? if(!empty($data[0][book_date])) echo $data[0][book_date] ?>" readonly="readonly" size="10">
      <a href="javascript:NewCssCal('inp_book_date[0]','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" alt="" /></a>      
      </td>
							<td class="set_left">
							  <input type="text" name="inp_qty[0]" size="5" id="inp_qty[0]" value="<? echo $data[0][product_quantity] ?>" />							   
							</td>
							<td class="set_left">
							  <input type="text" name="inp_price[0]" size="5" id="inp_price[0]" value="<? echo $data[0][product_amount] ?>" />
							</td>
						</tr>    
                        <tr>
							<td class="set_left"><? echo $data[3][1]; ?></td>
							<td class="set_left">
<input name="inp_book_date[1]" id="inp_book_date[1]" class="textFieldSize60" type="text" value="<? if(!empty($data[1][book_date])) echo $data[0][book_date] ?>" readonly="readonly" size="10">
      <a href="javascript:NewCssCal('inp_book_date[1]','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" alt="" /></a>      
      </td>
							<td class="set_left">
							  <input type="text" name="inp_qty[1]" size="5" id="inp_qty[1]" value="<? echo $data[1][product_quantity] ?>" />							   
							</td>
							<td class="set_left">
							  <input type="text" name="inp_price[1]" size="5" id="inp_price[1]" value="<? echo $data[1][product_amount] ?>" />
							</td>
						</tr> 
                        <tr>
							<td class="set_left"><? echo $data[3][2]; ?></td>
							<td class="set_left">
<input name="inp_book_date[2]" id="inp_book_date[2]" class="textFieldSize60" type="text" value="<? if(!empty($data[2][book_date])) echo $data[0][book_date] ?>" readonly="readonly" size="10" />
      <a href="javascript:NewCssCal('inp_book_date[2]','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" /></a>      
      </td>
							<td class="set_left">
							  <input type="text" name="inp_qty[2]" size="5" id="inp_qty[2]" value="<? echo $data[2][product_quantity] ?>" />							   
							</td>
							<td class="set_left">
							  <input type="text" name="inp_price[2]" size="5" id="inp_price[2]" value="<? echo $data[2][product_amount] ?>" />
							</td>
						</tr> 
                        <tr>
							<td class="set_left"><? echo $data[3][3]; ?></td>
							<td class="set_left">
<input name="inp_book_date[3]" id="inp_book_date[3]" class="textFieldSize60" type="text" value="<? if(!empty($data[3][book_date])) echo $data[0][book_date] ?>" readonly="readonly" size="10" />
      <a href="javascript:NewCssCal('inp_book_date[3]','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" /></a>      
      </td>
							<td class="set_left">
							  <input type="text" name="inp_qty[3]" size="5" id="inp_qty[3]" value="<? echo $data[3][product_quantity] ?>" />							   
							</td>
							<td class="set_left">
							  <input type="text" name="inp_price[3]" size="5" id="inp_price[3]" value="<? echo $data[3][product_amount] ?>" />
							</td>
						</tr>
                        <tr>
                                <td colspan="4" class="set_left">COMMENT</td>
                              </tr>
                              <tr>
                                <td colspan="4" height="70px"><textarea name="inp_comment" id="inp_comment" cols="95%" rows="5"><? echo $data[0][remark] ?></textarea></td>
                      </tr>
                    </table>
                    
                            <table width="100%">
                                <tr>
                                    <td colspan="2">INCOME</td>
                                </tr>
                                <tr>
                                    <td width="50%" height="23" class="set_left">TOTAL:
                                  <input type="text" name="inp_total" id="c_total" size="20" value="<? echo $data[0][amount] ?>" />
                                  </td>
                                    <td class="set_left">JPN / DEPOSIT / TH / MEMBER<br />
                                    PAID TYPE:
                                      <input type="text" name="inp_paid_type" id="c_paid_type" size="20" value="<? echo $data[0][paid_type]; ?>" /></td>        
                                </tr>
                                <tr>
                                    <td class="set_left">DEAD LINE:
                                     <input name="inp_dead_date" id="dead_date" class="textFieldSize60" type="text" value="<? echo $data[0][deadline_date]; ?>" readonly="readonly" size="10" /> <a href="javascript:NewCssCal('dead_date','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" alt="" /></a>                                                                         
</td>
                                    <td class="set_left">PAID: <? echo $data[3][4]; ?>
                                    </td>        
                                </tr>    
                            </table>
                                                        
                            <table width="100%">
                              <tr>
                                    <td colspan="3" class="set_left">OUTGO</td>
                              </tr>
                                <tr>
                                    <td class="set_left" colspan="2">AMOUNT: <input type="text" name="inp_paid_amount" id="inp_paid_amount" size="15" value="<? echo $data[0][paid_amount] ?>" /></td>
                                    <td class="set_left" width="50%">STAMP:                              
                                     <input name="inp_stamp_date" id="stamp_date" class="textFieldSize60" type="text" value="<? echo  $data[0][stamp_date]; ?>" readonly="readonly" size="10" /> <a href="javascript:NewCssCal('stamp_date','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" alt="" /></a>       
                                    </td>        
                                </tr>
                                <tr>
                                    <td colspan="2" class="set_left">DATE of PAID:
                                     <input name="inp_paid_date" id="paid_date" class="textFieldSize60" type="text" value="<? echo $data[0][paid_date] ?>" readonly="readonly" size="10" /> <a href="javascript:NewCssCal('paid_date','yyyymmdd')"><img src="./common/js/images/cal.gif" border="0" alt="" /></a>                
                                  </td>
                                    <td class="set_left">ACC. ATTN.:                                    
                                    <input type="text" name="inp_staff" id="inp_staff" size="30" value="<? echo $data[0][c_staff]; ?>" />
                                    </td>        
                                </tr>    
  </table>
                            
<div id="five">
<div class="attn">ATTN.:<span class="font_bold"><? echo strtoupper($data[0][customer_name]) ?></span></div>
                  </div>
                <div class="save">
<input type="image" src="./common/images/save.jpg" name="button" id="button" value="SAVE" style="border:0px;" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />                
                <input type="hidden" name="type" value="<? echo $data[0][product_type]; ?>" />
                <input type="hidden" name="b_id" value="<? echo $data[0][book_id]; ?>" />
</div>
</form>
      </div>
            <!--content_left-->
                                 
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>
<? include("footer.tpl"); ?>
</body>

</html>
