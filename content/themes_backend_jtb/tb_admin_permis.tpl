<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Permissions</title>
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />

<script type="text/javascript" src="./common/js/jquery-1.5.1.min"></script>
<script type="text/javascript">
	
$(document).ready(
	function()
	{	
	//alert('1122');
	
		$('.btn_del').click(
		function()
		{
			var msg = "Are you sure you want to Delete?";
			if ( confirm(msg) ){				
				return true;
			}
			else{
				return false;	
			}
			
		});	
	
	});	
	
</script>


</head>
<body>

<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a> &gt; Authorize </li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
  <div class="sub_head">
    	<ul class="head_admin">
    	<li class="icon_admin">Authorize : Find <? echo $data[page_count]; ?> Type</li>
    	</ul>           
                        
			<form name="frm_new" method="post" action="<? echo $data[p_new]; ?>">		 			
            
            <div class="save" style="padding-right:15px;">            
            <input type="image" src="./common/images/new.jpg" name="btn_new" id="btn_new" value="SAVE" border="0" onmouseover="this.src='./common/images/new_on.jpg'" onmouseout="this.src='./common/images/new.jpg'" />
            </div>                         
                  <table width="100%">
                    <tr>
                      <th class="center_set" width="10%">No</th>                                               
                      <th class="center_set" width="20%">Authorize name</th>
                      <th class="center_set" width="15%">Authorize Type</th>
                      <th class="center_set" width="35%">Power</th>
                      <th class="center_set" width="10%">Edit</th>
                      <th class="center_set" width="10%">Delete</th>
                    </tr>                    
                    
                    <?                     
                    	f_loop($data[3]);                    
                    ?>                          
                    
                  </table>

  	      </form>

		<!--	<br />
 
        <form name="insert" method="post" action="./admin_action.php">		 

		<div id="table">
                  <table width="100%">
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Level</th>
                      <th>Add</th>
                    </tr>
    				
                    
                    <tr>
                      <td>-<br /></td>
                      <td><input name="inp_level_name" type="text" id="inp_level_name" size="30" placeholder="New Entry" /></td>
                      <td><input name="inp_level" type="text" id="inp_level" size="30" placeholder="New Entry" /></td>
                      <td>
<input type="image" src="./common/images/new.jpg" name="btn_insert" id="btn_insert" value="Admin New Edit" border="0" onmouseover="this.src='./common/images/new_on.jpg'" onmouseout="this.src='./common/images/new.jpg'"/>
                      </td>
                    </tr>  
                  </table>
                  <input type="hidden" name="action" value="insert" />                
                  </div>

  	      </form> 
          
           <br />-->
           
                <div class="pager">
                <? echo $data[4]; ?>
                </div>

   </div>       
 </div>
 <!--end content-->
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
