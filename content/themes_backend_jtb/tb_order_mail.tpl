<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information</title>
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />

<script type="text/javascript" src="./common/js/jquery-1.5.1.min.js"></script>
<!--<script type="text/javascript" src="./common/js/tiny_mce/jquery.tinymce.js"></script> -->

</head>
<body>
<!--header inclu6de-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>   >  <a href="./order_info_list.php">Order information list</a> > <? echo $data[book_id] ?></li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
        
    	<ul class="head_customer">
    	<li class="icon_customer">Order Send Mail : </li>
    	</ul>        
 
         	<div id="content_left">
			<? include("o_left_content.tpl"); ?>
            </div>
            
            <!--content_left-->

         	<div id="content_right">
            
            <? if($data[send] ==''){ ?>

    <form name="mail_check" id="mail_check" method="post" enctype="multipart/form-data" action="./order_mail.php">  
    
<table width="100%">
  <tr>
  	<td ><div class="tittle_table">Mail Type</div>
    <td colspan="4"><? echo $data[mail_format];  ?></td>
    </td>
  </tr>
	<!---<tr>    	
    		<td width="20%" align="center">Mail Type</td>
            <? echo $data[mail_format]; ?>
    </tr> -->
  <tr>
    <td width="20%" align="center" nowrap="nowrap">Mail Address</td>
    <td width="80%" colspan="4"> 
    <div class="padding-bottom2">
    <span class="span_mailtype">To...</span><input type="text" name="mail_address" id="mail_address" class="inp_address" value="<? echo $data[reg_email] ?>" />
    </div>
    <div class="padding-bottom2">
    <span class="span_mailtype">Cc...</span><input type="text" name="mail_cc" id="mail_cc" class="inp_address" value="" />
    </div>
    <div >
    <span class="span_mailtype">Bcc..</span><input type="text" name="mail_bcc" id="mail_bcc" class="inp_address" value="" />
    </div>						
    </td>
  </tr>
  <tr>
    <td align="center">Mail Title</td>
    <td colspan="4"><input type="text" name="mail_title" id="mail_title" class="inp_width700" value="<? echo $data[mail_title]; ?>" /></td>    
  </tr>

  <tr>
    <td align="center">Mail Body</td>
    <td colspan="4"><label>
      <textarea name="mail_body" id="mail_body" rows="20" cols="85%"><? echo $data[inp_mail]; ?></textarea>
    </label></td>
  </tr>   

  <tr>
    <td class="td_font_small">Files</td>
    <td class="set_left" colspan="4">1
    	<? if(!empty($data[attach_file_1])){ ?>
      		<a href="<? echo $data[dir].$data[attach_file_1]; ?>" target="_blank"><? echo $data[attach_file_1]; ?></a>
      	<? }else{ ?>
        	<input type="file" class="input_file" name="upfile_1" size="40" />        
        <? } ?>
        
      <br />2
      <input type="file" class="input_file" name="upfile_2" size="40" /><br />
      3
      <input type="file" class="input_file" name="upfile_3" size="40" /></td>                  
  </tr>  
  
  <tr>
    <td>&nbsp;</td>
    <td colspan="4"><label>
		<input type="submit" name="btn_send" id="btn_send" value="Mail Send" />
    </label>
    </td>
  </tr>
</table>

	<input type="hidden" name="bs_id" value="<? echo $data[b_id]; ?>" />
    <input type="hidden" name="bs_mail_id" value="<? echo $data[inp_mail_id]; ?>" />
    <input type="hidden" name="bs_mail_status" value="<? echo $data[inp_mail_status]; ?>" />
</form>

	<? }else{ ?>
    		
            <p style="margin-left:200px;"><? echo $data[status_sendmail]; ?></p>
    
    <? } ?>
	
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>            
         
	</div>
    <!--content end-->
<? include("footer.tpl"); ?>
</body>

</html>
