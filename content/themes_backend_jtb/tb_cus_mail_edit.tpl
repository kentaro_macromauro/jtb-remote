<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css">
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css">
<script type="text/javascript" src="./common/js/jquery.js"></script> 

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon"> 
</head>
<body>

<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a>  >  <a href="./customer_info_list.php">Customer Information List</a> > Mail Edit</li>
	  </ul>	
     <!-- end breadcrumb -->  
 
<div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
<!--start content-->
 <div class="content">
 <div class="sub_head">
 
    	<ul class="head_customer">        
    	<li class="icon_customer">Customer Mail Edit: </li>        
    	</ul>
         
          	<div id="content_left">
			
            <!--{include file="c_left_content.tpl"}  -->
			<? include("c_left_content.tpl"); ?>            
                      
            </div>
            <!--content_left-->           
         	<div id="content_right">

			<form name="mail_check" method="post" enctype="multipart/form-data" action="./customer_mail_edit_action.php">

    <!--{if $task != NULL}-->
    <? if(!empty($data[task])){ ?>
    <table width="100%">
    	<tr>
        	<td><font size="4" color="#FF9900"><? echo $data[task]; ?></font></td>
        <tr>
    </table>
    <!--{/if}-->
    <? } ?>

    <table width="100%">    
	<tr>    	
        <? echo $data[mail_format]; ?>
    </tr>    
	</table>   
    
    <table width="100%" border="0">
      <tr>
        <td class="td_font_small">Mail Title</td>
        <td class="set_left"><input type="text" name="mail_title" size="60%" id="mail_title" value="<? echo $data[mail_title]; ?>" /></td>
      </tr>
    
      <tr style="display:none;">
        <td>Mail Body</td>
        <td><label>
          <textarea name="mail_body" id="mail_body" rows="20" cols="85%"><? echo $data[inp_mail]; ?></textarea>
        </label></td>
      </tr>       
      <tr>
        <td>&nbsp;</td>
        <td>
        
<!--        <label>
            <input type="submit" name="btn_save" value="Save" />
        </label>-->
        
        <div class="save_center" style="float:left;">
<input type="image" src="./common/images/save.jpg" name="button" id="button" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" /></div>

        <input type="hidden" name="sb_id" value="<? echo $data[book_id]; ?>" />
        <input type="hidden" name="sbook_code" value="<? echo $data[jtb_bookind_id]; ?>" />
        <input type="hidden" name="smail_id" value="<? echo $data[inp_mail_id]; ?>" />
        </td>
      </tr>
    </table>

</form>

</div>
            <!--content_right-->            
 </div> 
 </div>
 
 <!--end content-->   



    </div>
  </div>

<? include("footer.tpl"); ?>
</body>

</html>
