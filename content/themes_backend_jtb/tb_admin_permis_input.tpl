<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator ID.Pass Management screen</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />

<style type="text/css">
	.err_msg{font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#C30; padding-top:10px;}	
		
	#username{
		padding:1px;
		/*font-size:18px;*/
		border:3px #CCC solid;
	}
	
	#tick{display:none}
	#cross{display:none}	
	#tick2{display:none}
	#cross2{display:none}
	
</style>

<script type="text/javascript" src="./common/js/jquery-1.5.1.min"></script>
<script type="text/javascript">

$(document).ready(function(){
	$('#inp_level_name').keyup(username_check);
	$('#inp_level').keyup(username_check2);
	
	
$('#btn_save').click(
  function()
  {      
   _msg ='';
   //_msg ='tttt';
   inp_level_name = $('#inp_level_name').val();
   inp_level = $('#inp_level').val();    
       
   if(inp_level_name==''){
	    _msg +='Please enter Permissions name'; 
   }
   else if(inp_level_name.length < 4){
		_msg +='\nPlease enter Permissions name minimum 4 characters';				
   }
   
   if(inp_level==''){
	   
		if(_msg!=''){_msg +='\n';}    
		_msg +='Please enter Level';
   }
   
	if (  ( jQuery.trim($('#chkid_0:checked').val())  == false) &&
		  ( jQuery.trim($('#chkid_1:checked').val())  == false) &&
		  ( jQuery.trim($('#chkid_2:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_3:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_4:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_5:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_6:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_7:checked').val())  == false) && 
		  ( jQuery.trim($('#chkid_8:checked').val())  == false) )
	{					
	
    	if(_msg!=''){_msg +='\n';}    
		_msg +='Please select Permissions';
		
	}  
		
	   if(_msg != ""){
			alert(_msg);		
			return false;
	   }
	   else{		   		   
			return true; 										
	   }
   
  });	
});
	
	function username_check(){	
	
		var username = $('#inp_level_name').val();
		
		if(username == "" || username.length < 4){
			$('#inp_level_name').css('border', '2px #CCC solid');
			$('#tick').hide();
		}else{
		
			jQuery.ajax({
				   type: "POST",
				   //url: "check.php",
				   url: "checkdata.php",
				   data: 'username='+ username,
				   cache: false,
				   success: function(response){ //alert('response='+response);
				//if(response == 1){
				if(response > 0){
					//alert('111');
						$('#inp_level_name').css('border', '2px #C33 solid');	
						$('#tick').hide();
						$('#cross').fadeIn();
					}else{
					//alert('222');			
						$('#inp_level_name').css('border', '2px #090 solid');
						$('#cross').hide();
						$('#tick').fadeIn();
					}
				
				}
			});
		}
	}

	function username_check2(){	
	
		var username = $('#inp_level').val();
		
		if(username == "" || username.length < 1){
			$('#inp_level').css('border', '2px #CCC solid');
			$('#tick2').hide();
		}else{
		
			jQuery.ajax({
				   type: "POST",
				   //url: "check.php",
				   url: "checkdata2.php",
				   data: 'username='+ username,
				   cache: false,
				   success: function(response){ //alert('response='+response);
				//if(response == 1){
				if(response > 0){
					//alert('111');
						$('#inp_level').css('border', '2px #C33 solid');	
						$('#tick2').hide();
						$('#cross2').fadeIn();
					}else{
					//alert('222');			
						$('#inp_level').css('border', '2px #090 solid');
						$('#cross2').hide();
						$('#tick2').fadeIn();
					}
				
				}
			});
		}
	}

</script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 
</head>
<body>

<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a> &gt; <a href="./permis.php">Authorize</a> &gt; <? echo $data[page]; ?> </li></ul>

 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
  <div class="sub_head">
    	<ul class="head_admin">
    	<li class="icon_admin">Authorize</li>
    	</ul>           
                        
			<!--<form name="admin_save" method="post" action="./permis_action.php">-->
            <form name="admin_save" method="post" action="./permis_action.php">		 
				 <div class="save">
<input type="image" src="./common/images/save.jpg" name="btn_save" id="btn_save" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" onclick="requestCustomerInfo()" />				 </div>

                  <table width="100%">
                    <!--<tr>
                      <th class="center_set" width="30%">No</th>                                               
                      <th class="center_set">Name</th>					onclick="requestCustomerInfo()"
                    </tr>  -->                  
                    <?                     
                    	f_loop($data[3]);                    
                    ?>                                              
                  </table>
                  <div id="regist-error" class="err_msg"><? echo $data[msg]; ?></div>
                  <input type="hidden" name="action" id="action" value="<? echo $data[4][1]; ?>" />
                  <input type="hidden" name="update_id" id="update_id" value="<? echo $data[4][0]; ?>" />
  	      </form>         
          
           <br />          

   </div>       
 </div>
 <!--end content-->
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
