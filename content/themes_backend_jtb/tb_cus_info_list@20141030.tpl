<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer information</title>
<link rel="stylesheet" type="text/css" media="all" href="./common/css2/base.css" />
<link rel="stylesheet" type="text/css" media="all" href="./common/css2/table.css" />
<link rel="stylesheet" type="text/css" media="all" href="./common/css2/pager.css" />
<script type="text/javascript" src="./common/js/jquery-1.5.1.min"></script>
<script type="text/javascript" src="./common/js/datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('.btn_del').click(
		function(){
			var msg = "Are you sure you want to Delete?";
			if ( confirm(msg) ){				
				return true;
			}
			else{
				return false;	
			}
		});				
	});
</script>
</head>
<body>
<? include("header.tpl"); ?>
<!-- start breadcrumb -->
<ul class="breadcamp">
  <li class="home"><a href="./">HOME</a> > Customer Information List</li>
</ul>
<!-- end breadcrumb -->
<div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
<!--start content-->
<div class="content">
  <div class="sub_head">
    <ul class="order">
      <li class="icon_order">Customer Information  : Find <? echo $data[page_count] ?> Customer</li>
    </ul>
    <div class="list">
      <form name="customer_search" method="post" action="./customer_info_list.php">
        <input  type="image" src="./common/images/reset.jpg" name="reset" value="reset" style="border:0px;" onmouseover="this.src='./common/images/reset_on.jpg'" onmouseout="this.src='./common/images/reset.jpg'"/>
        <input type="hidden" value="reset" name="search_type" />
      </form>
    </div>
    <div id="search">
      <form name="admin_save" method="post" action="./customer_info_list.php">
        <table width="100%" border="0" cellpadding="0" cellspacing="1">
          <tr>
            <th>Keywords</th>
            <td class="set_left" ><label>
                <input type="text" size="30" name="c_keyword" value="<? echo $_SESSION["session_c_keyword"]; ?>" id="c_keyword" />
              </label></td>
            <th>Paid Type</th>
            <td class="set_left"><? echo $data[8]; ?></td>
            <th>Paid Status</th>
            <td class="set_left"><? echo $data[10]; ?></td>
            <th>Action Status</th>
            <td class="set_left"><? echo $data[11]; ?></td>
            
            <td rowspan="2"><input type="image" src="./common/images/search.jpg" name="button" id="button" value="Search" style="border:0px;" onmouseover="this.src='./common/images/search_on.jpg'" onmouseout="this.src='./common/images/search.jpg'" />
              <input type="hidden" value="search" name="search_type" /></td>
          </tr>
          <tr>
            <th>Product Type</th>
            <td class="set_left"><? echo $data[9]; ?></td>
            <th>Check In Date</th>
            <td class="set_left" colspan="5"><input name="start_date" id="ifrom_date" class="textFieldSize60" type="text" value="<? echo $_SESSION["session_start_date"]; ?>" readonly="readonly" />
              <a href="javascript:NewCssCal('ifrom_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0" alt="" /></a>&nbsp;-&nbsp;
              <input name="end_date" id="ito_date" class="textFieldSize60" type="text" value="<? echo $_SESSION["session_end_date"]; ?>" readonly="readonly" />
              <a href="javascript:NewCssCal('ito_date','yyyymmdd')"><img src="./common/js/images/cal.gif"  border="0" alt="" /></a></td>
          </tr>
        </table>
      </form>
    </div>
    <br />
    <div id="save" align="right"> </div>
    <div id="table">
      <table width="100%" border="0" cellpadding="0" cellspacing="1">
        <tr>
          <th class="center_set" width="15%">User ID/<br/>Order Code</th>
          <th class="center_set" width="10%">Booking No</th>
          <th class="center_set" width="10%">Booking<br />
            Date</th>
          <th class="center_set" width="20%">Product name（JP）</th>
          <th class="center_set" width="12%">Cust. Name</th>
          <th class="center_set" width="5%">Guest</th>
          <th class="center_set" width="10%">Amount</th>
          <th class="center_set" width="10%">Check In<br />
            Date</th>
          <th class="center_set" width="10%">Paid Type</th>
          <th class="center_set" width="10%">Paid Status</th>
          <th class="center_set" width="3%">Pickup</th>
          <th class="center_set" width="7%">Action Status</th>
          <th class="center_set" width="10%">Edit</th>
          <th class="center_set" width="10%">Delete</th>
        </tr>
        <?  f_loop($data[3]); ?>
      </table>
    </div>
    <!--table end-->
    <br />
    <!--link-->
    <div class="pager">
      <!--{$link}-->
      <? echo $data[4]; ?> </div>
  </div>
</div>
<!--end content-->
</div>
</div>
<? include("footer.tpl"); ?>
</body>
</html>