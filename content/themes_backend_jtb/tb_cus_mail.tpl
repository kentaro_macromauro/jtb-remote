<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order information</title>
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<script type="text/javascript" src="./common/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="./common/js/tiny_mce/jquery.tinymce.js"></script>
</head>
<body>
<!--header include-->
<? include("header.tpl"); ?>
<!-- start breadcrumb -->
<ul class="breadcamp">
  <li class="home"><a href="./">HOME</a> &gt; <a href="./customer_info_list.php">Customer Information List</a> &gt; Customer Send Mail</li>
</ul>
<!-- end breadcrumb -->
<div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
<!--start content-->
<div class="content">
  <div class="sub_head">
    <ul class="head_customer">
      <li class="icon_customer">Customer Send Mail :
        <? if($data[send] ==''){ ?>
        <a href="./customer_mail_edit.php?type=<? echo $data[book_type]; ?>&b_id=<? echo $data[book_id]; ?>&mail_id=<? echo $_GET[mail_id]; ?>"><img src="./common/images/edit_email_format.jpg" alt="New" border="0" onmouseover="this.src='./common/images/edit_email_format_on.jpg'" onmouseout="this.src='./common/images/edit_email_format.jpg'" /></a>
        <? } ?>
      </li>
    </ul>
    <!--content_left-->
    <div id="content_left">
      <? include("c_left_content.tpl"); ?>
    </div>
    <!--content_left-->
    <div id="content_right">
      <? if($data[send] ==''){ ?>
      <form name="mail_check" method="post" enctype="multipart/form-data" action="./customer_mail.php">
        <table width="100%">
          <tr><? echo $data[mail_format]; ?></tr>
        </table>
        <table width="100%" border="0">
          <tr>
            <td class="td_font_small">Mail Address</td>
            <td class="set_left">
            
            	<div class="padding-bottom2">
                <span class="span_mailtype">To...</span><input type="text" name="mail_address" id="mail_address" class="inp_address" value="<? echo $data[reg_email]; ?>" />
                </div>
                <div class="padding-bottom2">
                <span class="span_mailtype">Cc...</span><input type="text" name="mail_cc" id="mail_cc" class="inp_address" value="" />
                </div>
                <div >
                <span class="span_mailtype">Bcc..</span><input type="text" name="mail_bcc" id="mail_bcc" class="inp_address" value="" />
                </div>		
            
             </td>
          </tr>
          <tr>
            <td class="td_font_small">Mail Title</td>
            <td class="set_left"><input type="text" name="mail_title" id="mail_title" class="inp_width700" value="<? echo $data[mail_title]; ?>" /></td>
          </tr>
          <tr>
            <td>Mail Body</td>
            <td><textarea name="mail_body" id="mail_body" class="tinymce" rows="20" cols="85%"><? echo $data[inp_mail]; ?></textarea></td>
          </tr>
          <tr>
            <td class="td_font_small">Files</td>
            <td class="set_left">
              <p>1 <input type="file" class="input_file" name="upfile_1" size="40" /></p>
              <p>2 <input type="file" class="input_file" name="upfile_2" size="40" /></p>
              <p>3 <input type="file" class="input_file" name="upfile_3" size="40" /></p>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
                <input type="submit" name="send" value="Mail Send" />
              </label>
              <!--20120712-->
              <? 
              if ( $_REQUEST[mail_id] == 3){
              ?>
              <input type="hidden" name="mail_type" value="2" />
              <?
              }
              else{
              ?>
              <input type="hidden" name="mail_type" value="<? echo $_REQUEST[mail_id]+1; ?>" />
              <? } ?>
              
              <!--20120712-->
              <input type="hidden" name="bs_id" value="<? echo $data[book_id]; ?>" /></td>
          </tr>
        </table>
      </form>
      <? }else{ ?>
      <p style="margin-left:200px;"><? echo $data[status_sendmail]; ?></p>
      <? } ?>
    </div>
    <!--content_right-->
  </div>
</div>
<!--end content-->
</div>
</div>
<? include("footer.tpl"); ?>
</body>
</html>
