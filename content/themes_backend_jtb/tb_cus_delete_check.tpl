<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer information</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css">
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css">

<script type="text/javascript">
function nidoosi2(form) {
	var elements = form.elements;
	
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].type == 'submit') {
			elements[i].disabled = true;
		}
	}
} 
</script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon"> 
</head>
<body>
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./"><a href="./">HOME</a>  >  <a href="./customer_info_list.php">Customer information list</a>  >  Customer Delete Check</li></ul>
 <!-- end breadcrumb -->  
 
 <div class="line"><img src="common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
 <div class="sub_head">
    	<ul class="head_customer">
    	<li class="icon_customer">Customer information List</li>
    	</ul>
			<form name="customer_delete.php" method="post" action="./customer_delete.php" onSubmit="return nidoosi2(this)">
                 <div id="table">
                  <table width="100%">
                    <tr>
                      <th class="center_set">NO</th>
                      <th class="center_set">Book No</th>
                      <th class="center_set">Name</th>
                      <th class="center_set">Status</th>
                      <th class="center_set">Date</th>
                    </tr>
        
                    <tr>
                      <td><? echo $data[1][0]; ?></td>
                      <td><? echo $data[1][0]; ?></td>
                      <td><? echo $data[1][18]; ?></td>
                      <td><? echo $data[status_name]; ?></td>
                      <td><? echo $data[1][2]; ?></td>
                    </tr>
                    <tr>
                      <td colspan="5">
					  <label>
<input type="image" src="./common/images/delete.jpg" name="button" id="button" value="Delete Customer" border="0" onmouseover="this.src='./common/images/delete_on.jpg'" onmouseout="this.src='./common/images/delete.jpg'" />						
<input type="hidden" name="b_id" value="<? echo $data[1][0]; ?>" />
<!--<input type="hidden" name="type" value="{$type}" />-->
                      </label>                      
                      </td>
                    </tr>
                  </table>                
</div>                            
    	      </form>
 </div>
 
 </div>
 <!--end content-->
    
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
