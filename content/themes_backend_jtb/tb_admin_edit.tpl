<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator ID.Pass Management screen</title>
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/base.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/table.css" />
<LINK rel="stylesheet" type="text/css" media="screen,projection" href="./common/css2/pager.css" />

<script type="text/javascript" src="./common/js/jquery-1.5.1.min"></script>
<script type="text/javascript" src="./common/js/sha1.js"></script>
<script type="text/javascript">
/*
function nidoosi2(form) {	
	var elements = form.elements;
	
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].type == 'submit') {
			elements[i].disabled = true;
		}
	}
} 
*/

$(document).ready(
	function()
	{
				
		$('#btn_save').click(
		function()
		{

			_msg ='';
			_user_pass = $('#user_pass').val();
			_inp_oldpass = $('#inp_oldpass').val();
			_inp_newpass = $('#inp_newpass').val(); 
			_inp_cfmpass = $('#inp_cfmpass').val(); 
			
			//var aa =hex_sha1('12345');
			//alert(_user_pass+'=='+hex_sha1(_inp_oldpass));
			
			if(_inp_oldpass!='' || _inp_newpass!='' || _inp_cfmpass!=''){
				//alert('in');
				
				if(_inp_oldpass==''){
					_msg +='\nPlease enter Old Password';	
				}
				else if(_inp_oldpass.length < 5){
					//_msg +='\nPlease enter Old Password Minimum 5 characters';
				}
				
				if(_inp_newpass==''){
					_msg +='\nPlease enter New Password';	
				}
				else if(_inp_newpass.length < 5){
					_msg +='\nPlease enter New Password Minimum 5 characters';
				}
				
				if(_inp_cfmpass==''){
					_msg +='\nPlease enter Confirm Password';	
				}				
				else if(_inp_cfmpass.length < 5){
					_msg +='\nPlease enter Confirm Password Minimum 5 characters';
				}
				
				if(_inp_newpass != _inp_cfmpass){
					_msg +='\nNew Password and Confirm Password not match';
				}
				else if(hex_sha1(_inp_oldpass) != _user_pass){
					_msg +='\nOld Password Invalid';				
				}
			}			
			
			if(_msg != ""){
				alert(_msg);
				return false;
			}
			else{
				return true;	
			}
		});	
		
	});

</script>

<LINK REL="SHORTCUT ICON" HREF="./ppp.icon" /> 
</head>
<body>

<!--header include-->
<? include("header.tpl"); ?>

     <!-- start breadcrumb -->
   	  <ul class="breadcamp">
      <li class="home"><a href="./">HOME</a> &gt; <a href="./admin_pow.php">Administrator Management ID/PASS</a> &gt; <? echo $data[4][2]; ?> </li></ul>

 <!-- end breadcrumb -->  
 
 <div class="line"><img src="./common/images/line.jpg" width="994" height="4" /></div>
 
 <!--start content-->
 <div class="content">
  <div class="sub_head">
    	<ul class="head_admin">
    	<li class="icon_admin">Administrator Management</li>
    	</ul>           
                        
			<form name="admin_save" method="post" action="./admin_action.php">		 
				 <div class="save">
<input type="image" src="./common/images/save.jpg" name="btn_save" id="btn_save" value="SAVE" border="0" onmouseover="this.src='./common/images/save_on.jpg'" onmouseout="this.src='./common/images/save.jpg'" />				 </div>

                  <table width="100%">
                    <!--<tr>
                      <th class="center_set" width="30%">No</th>                                               
                      <th class="center_set">Name</th>
                    </tr>-->                  
                    <?                     
                    	f_loop($data[3]);                    
                    ?>                                              
                  </table>
                  <input type="hidden" name="admin_id" id="admin_id" value="<? echo $data[4][0]; ?>" />
                  <input type="hidden" name="action" id="action" value="<? echo $data[4][1]; ?>" />
                  <input type="hidden" name="user_pass" id="user_pass" value="<? echo $data[4][3]; ?>" />
  	      </form>         
          
           <br />          

   </div>       
 </div>
 <!--end content-->
    </div>
  </div>

<? include("footer.tpl"); ?>

</body>

</html>
